/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.change.trace;

import org.eclipse.emf.ecore.EModelElement;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaClass;

public class ChangeTraceItem {
	private String op = null; // operation code {"", "add", "delete", "update"}
	private AnnotatedElement mmElement = null; // the element in its metabup version
	private EModelElement ecoreElement = null; // the element in its ECore version
	private String referredElementId = null; // the id of the operation's referred element	
	
	public ChangeTraceItem(String op, AnnotatedElement ae, EModelElement eme, String re){		
		this.setOp(op);
		this.setMmElement(ae);
		this.setEcoreElement(eme);
		this.setReferredElementId(re);
	}
	
	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		if(op.equals("") || op.equals("add") || op.equals("delete") || op.equals("update")) this.op = op;
		else this.op = "";
	}

	public AnnotatedElement getMmElement() {
		return mmElement;
	}
	
	public String getId(){
		return mmElement.getId();
	}
	
	public void setMmElement(AnnotatedElement mmElement) {
		this.mmElement = mmElement;
	}		

	public EModelElement getEcoreElement() {
		return ecoreElement;
	}

	public void setEcoreElement(EModelElement ecoreElement) {
		this.ecoreElement = ecoreElement;
	}

	public String getReferredElementId() {
		return referredElementId;
	}

	public void setReferredElementId(String referredElementId) {
		this.referredElementId = referredElementId;
	}
	
	public boolean isLoad(){
		if(this.op.equals("")) return true;
		else return false;
	}
	
	public boolean isAdd(){
		if(this.op.equals("add")) return true;
		else return false;
	}
	
	public boolean isDelete(){
		if(this.op.equals("delete")) return true;
		else return false;
	}
	
	public boolean isUpdate(){
		if(this.op.equals("update")) return true;
		else return false;
	}		
}
