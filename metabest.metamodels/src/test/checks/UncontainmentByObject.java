/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks;

import test.checks.expressions.ObjectExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uncontainment By Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.UncontainmentByObject#getContainer <em>Container</em>}</li>
 * </ul>
 *
 * @see test.checks.ChecksPackage#getUncontainmentByObject()
 * @model
 * @generated
 */
public interface UncontainmentByObject extends Uncontainment {
	/**
	 * Returns the value of the '<em><b>Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' containment reference.
	 * @see #setContainer(ObjectExpression)
	 * @see test.checks.ChecksPackage#getUncontainmentByObject_Container()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ObjectExpression getContainer();

	/**
	 * Sets the value of the '{@link test.checks.UncontainmentByObject#getContainer <em>Container</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' containment reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(ObjectExpression value);

} // UncontainmentByObject
