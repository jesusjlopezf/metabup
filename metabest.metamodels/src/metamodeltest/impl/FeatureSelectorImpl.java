/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.FeatureFilter;
import metamodeltest.FeatureSelector;
import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Selector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.FeatureSelectorImpl#getFeatureFilter <em>Feature Filter</em>}</li>
 *   <li>{@link metamodeltest.impl.FeatureSelectorImpl#getFeatureConditioner <em>Feature Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FeatureSelectorImpl extends SelectorImpl implements FeatureSelector {
	/**
	 * The cached value of the '{@link #getFeatureFilter() <em>Feature Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureFilter()
	 * @generated
	 * @ordered
	 */
	protected FeatureFilter featureFilter;

	/**
	 * The cached value of the '{@link #getFeatureConditioner() <em>Feature Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureConditioner()
	 * @generated
	 * @ordered
	 */
	protected FeatureFilter featureConditioner;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureSelectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.FEATURE_SELECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureFilter getFeatureFilter() {
		return featureFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureFilter(FeatureFilter newFeatureFilter, NotificationChain msgs) {
		FeatureFilter oldFeatureFilter = featureFilter;
		featureFilter = newFeatureFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_FILTER, oldFeatureFilter, newFeatureFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureFilter(FeatureFilter newFeatureFilter) {
		if (newFeatureFilter != featureFilter) {
			NotificationChain msgs = null;
			if (featureFilter != null)
				msgs = ((InternalEObject)featureFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_FILTER, null, msgs);
			if (newFeatureFilter != null)
				msgs = ((InternalEObject)newFeatureFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_FILTER, null, msgs);
			msgs = basicSetFeatureFilter(newFeatureFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_FILTER, newFeatureFilter, newFeatureFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureFilter getFeatureConditioner() {
		return featureConditioner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureConditioner(FeatureFilter newFeatureConditioner, NotificationChain msgs) {
		FeatureFilter oldFeatureConditioner = featureConditioner;
		featureConditioner = newFeatureConditioner;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_CONDITIONER, oldFeatureConditioner, newFeatureConditioner);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureConditioner(FeatureFilter newFeatureConditioner) {
		if (newFeatureConditioner != featureConditioner) {
			NotificationChain msgs = null;
			if (featureConditioner != null)
				msgs = ((InternalEObject)featureConditioner).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_CONDITIONER, null, msgs);
			if (newFeatureConditioner != null)
				msgs = ((InternalEObject)newFeatureConditioner).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_CONDITIONER, null, msgs);
			msgs = basicSetFeatureConditioner(newFeatureConditioner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_CONDITIONER, newFeatureConditioner, newFeatureConditioner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_FILTER:
				return basicSetFeatureFilter(null, msgs);
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_CONDITIONER:
				return basicSetFeatureConditioner(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_FILTER:
				return getFeatureFilter();
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_CONDITIONER:
				return getFeatureConditioner();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_FILTER:
				setFeatureFilter((FeatureFilter)newValue);
				return;
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_CONDITIONER:
				setFeatureConditioner((FeatureFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_FILTER:
				setFeatureFilter((FeatureFilter)null);
				return;
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_CONDITIONER:
				setFeatureConditioner((FeatureFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_FILTER:
				return featureFilter != null;
			case MetamodeltestPackage.FEATURE_SELECTOR__FEATURE_CONDITIONER:
				return featureConditioner != null;
		}
		return super.eIsSet(featureID);
	}

} //FeatureSelectorImpl
