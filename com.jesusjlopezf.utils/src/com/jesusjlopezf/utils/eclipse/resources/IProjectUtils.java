package com.jesusjlopezf.utils.eclipse.resources;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.LibraryLocation;
import org.eclipse.jdt.ui.PreferenceConstants;

public class IProjectUtils {
	public static void createJavaProject(String name) throws CoreException{
		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
	    IProject project = workspaceRoot.getProject(name);
	    if(!project.exists()) project.create(null);
	    if(!project.isOpen()) project.open(null);

	    addNatureToProject(project, JavaCore.NATURE_ID);

	    IJavaProject javaProject = JavaCore.create(project); 

	    IFolder binFolder = project.getFolder("bin");
	    if(!binFolder.exists())binFolder.create(false, true, null);
	    javaProject.setOutputLocation(binFolder.getFullPath(), null);

	    List<IClasspathEntry> entries = new ArrayList<IClasspathEntry>();

	    IVMInstall vmInstall = JavaRuntime.getDefaultVMInstall();
	    LibraryLocation[] locations = JavaRuntime.getLibraryLocations(vmInstall);
	    for (LibraryLocation element : locations) {
	        entries.add(JavaCore.newLibraryEntry(element.getSystemLibraryPath(), null, null));	        
	    }
	    
	    Set<IClasspathEntry> mySet = new HashSet<IClasspathEntry>(Arrays.asList(PreferenceConstants.getDefaultJRELibrary()));
	    entries.addAll(mySet);

	    javaProject.setRawClasspath(entries.toArray(new IClasspathEntry[entries.size()]), null);

	    IFolder sourceFolder = project.getFolder("src");
	    if(!sourceFolder.exists()) sourceFolder.create(false, true, null);

	    IPackageFragmentRoot packageRoot = javaProject.getPackageFragmentRoot(sourceFolder);
	    IClasspathEntry[] oldEntries = javaProject.getRawClasspath();
	    IClasspathEntry[] newEntries = new IClasspathEntry[oldEntries.length + 1];
	    System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);
	    newEntries[oldEntries.length] = JavaCore.newSourceEntry(packageRoot.getPath());
	    javaProject.setRawClasspath(newEntries, null);
	}
	
	public static void addNatureToProject(IProject project, String nature) throws CoreException{
		IProjectDescription description = project.getDescription();
		String[] natures = description.getNatureIds();
		String[] newNatures = new String[natures.length + 1];
		System.arraycopy(natures, 0, newNatures, 0, natures.length);
		
		newNatures[natures.length] = nature;
		description.setNatureIds(newNatures);
		project.setDescription(description, null);
	}
	
	/**
	 * Static method for turning an unformatted object into an IProject instance.
	 *
	 * @param object the object
	 * @return IFile	IFile instance / NULL if it's not possible.
	 */
	public static IProject convertObjectToIProject(Object object) {

		if (object instanceof IProject) {
			return (IProject) object;
		}

		if (object instanceof IAdaptable) {
			IAdaptable adaptable = (IAdaptable) object;
			IProject project = (IProject) adaptable.getAdapter(IProject.class);

			if (project != null) {
				return project;
			}
		}

		return null;
	}
	
	public static boolean isProjectNature(IProject project, String nature){
		if(project != null && nature != null){
			try {
				String[] natureids = project.getDescription().getNatureIds();
				if(natureids != null) for(int i=0; i<natureids.length; i++) if(natureids[i].equalsIgnoreCase(nature)) return true;
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				//TransformationTools.log(IStatus.ERROR, e.getMessage());
			}
		}	
		
		return false;
	}
}
