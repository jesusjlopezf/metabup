/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.popup.actions;




import metabest.ui.views.FragmentAssertionEvaluationView;
import metabest.ui.views.MetamodelAssertionEvaluationView;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;

import test.TestModel;

public class MetaBestFragmentValidationAction implements IObjectActionDelegate {

	private Shell shell;
	private ISelection selection;
	
	/**
	 * Constructor for Action1.
	 */
	public MetaBestFragmentValidationAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		
		if(selection instanceof IStructuredSelection){
			Object obj[] = ((IStructuredSelection) selection).toArray();
			IFile file = IFileUtils.convertObjectToIFile(obj[0]);
			ResourceSet rs = new ResourceSetImpl();
			
			org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(file.getLocationURI().toString());
			Resource r = rs.getResource(uri, true);
			org.eclipse.emf.common.util.TreeIterator<EObject> it = r.getAllContents();
			
			String metamodelUri = null;
			
			while(it.hasNext() && metamodelUri == null){
				EObject eo = it.next();
				if(eo instanceof TestModel){
					TestModel tm = (TestModel) eo;
					metamodelUri = tm.getImportURI();
				}
			}
			
			if(metamodelUri == null) return;
			
			try {								
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(FragmentAssertionEvaluationView.ID);
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				page.hideView(page.findView(MetamodelAssertionEvaluationView.ID));
				
				IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
				FragmentAssertionEvaluationView av = null;
				
				for (int i = 0; i < viewReferences.length; i++) {
					if (FragmentAssertionEvaluationView.ID.equals(viewReferences[i].getId())){
						av = (FragmentAssertionEvaluationView)viewReferences[i].getView(true);
						av.refresh(r, metamodelUri);
					}
				}												
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

}
