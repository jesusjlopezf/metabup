/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.refactoring;

import java.util.List;

import fragments.Attribute;
import metabup.annotations.extensionpoint.RefactoringAnnotation;
import metabup.annotations.general.Annotation;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.Reference;

public class InLine extends RefactoringAnnotation {
	
	public InLine(){
		super.setName("inline");
		super.setDescription("It turns a reference, typed with a class with no features, into an int type attribute equally named");
	}
	
	@Override
	public MetaModel perform() {
		if(element instanceof Reference){			
			MetaClass mc = (MetaClass) element.eContainer();						
			MetaClass refMC = ((Reference) element).getReference();			
			if(refMC == null) return null;
			
			if(refMC.getFeatures().isEmpty()){
				String name = ((Reference) element).getName();
				super.removeFeatureWithName(mc, ((Reference) element).getName());
				
				boolean found = false;
				for(Feature f : mc.getFeatures()){
					if(f instanceof Attribute){
						Attribute a = (Attribute)f;
						if(a.getType().equals("IntType") && a.getName().equals(name)) found = true;		
					}
				}
				
				if(!found){
					//System.out.println("attribute not found");
					metabup.metamodel.Attribute attr = MetamodelFactory.eINSTANCE.createAttribute();
					attr.setName(name);
					attr.setPrimitiveType("IntType");
					mc.getFeatures().add(attr);
				}
								
				// delete referred MetaClass, for now				
				super.removeMetaClass(refMC);
				
				return targetMetaModel;
			}else return null;
						
		}else return null;		
	}

}
