/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import fragments.impl.FragmentImpl;
import test.ComplementaryBlock;
import test.CompletionType;
import test.TestPackage;
import test.TestableFragment;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Testable Fragment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link test.impl.TestableFragmentImpl#getAssertionSet <em>Assertion Set</em>}</li>
 *   <li>{@link test.impl.TestableFragmentImpl#getCompletion <em>Completion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TestableFragmentImpl extends FragmentImpl implements TestableFragment {
	/**
	 * The cached value of the '{@link #getAssertionSet() <em>Assertion Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertionSet()
	 * @generated
	 * @ordered
	 */
	protected ComplementaryBlock assertionSet;

	/**
	 * The default value of the '{@link #getCompletion() <em>Completion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompletion()
	 * @generated
	 * @ordered
	 */
	protected static final CompletionType COMPLETION_EDEFAULT = CompletionType.IS_FRAGMENT;

	/**
	 * The cached value of the '{@link #getCompletion() <em>Completion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompletion()
	 * @generated
	 * @ordered
	 */
	protected CompletionType completion = COMPLETION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestableFragmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestPackage.Literals.TESTABLE_FRAGMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplementaryBlock getAssertionSet() {
		return assertionSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssertionSet(ComplementaryBlock newAssertionSet, NotificationChain msgs) {
		ComplementaryBlock oldAssertionSet = assertionSet;
		assertionSet = newAssertionSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TestPackage.TESTABLE_FRAGMENT__ASSERTION_SET, oldAssertionSet, newAssertionSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertionSet(ComplementaryBlock newAssertionSet) {
		if (newAssertionSet != assertionSet) {
			NotificationChain msgs = null;
			if (assertionSet != null)
				msgs = ((InternalEObject)assertionSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TestPackage.TESTABLE_FRAGMENT__ASSERTION_SET, null, msgs);
			if (newAssertionSet != null)
				msgs = ((InternalEObject)newAssertionSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TestPackage.TESTABLE_FRAGMENT__ASSERTION_SET, null, msgs);
			msgs = basicSetAssertionSet(newAssertionSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestPackage.TESTABLE_FRAGMENT__ASSERTION_SET, newAssertionSet, newAssertionSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompletionType getCompletion() {
		return completion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompletion(CompletionType newCompletion) {
		CompletionType oldCompletion = completion;
		completion = newCompletion == null ? COMPLETION_EDEFAULT : newCompletion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestPackage.TESTABLE_FRAGMENT__COMPLETION, oldCompletion, completion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestPackage.TESTABLE_FRAGMENT__ASSERTION_SET:
				return basicSetAssertionSet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestPackage.TESTABLE_FRAGMENT__ASSERTION_SET:
				return getAssertionSet();
			case TestPackage.TESTABLE_FRAGMENT__COMPLETION:
				return getCompletion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestPackage.TESTABLE_FRAGMENT__ASSERTION_SET:
				setAssertionSet((ComplementaryBlock)newValue);
				return;
			case TestPackage.TESTABLE_FRAGMENT__COMPLETION:
				setCompletion((CompletionType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestPackage.TESTABLE_FRAGMENT__ASSERTION_SET:
				setAssertionSet((ComplementaryBlock)null);
				return;
			case TestPackage.TESTABLE_FRAGMENT__COMPLETION:
				setCompletion(COMPLETION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestPackage.TESTABLE_FRAGMENT__ASSERTION_SET:
				return assertionSet != null;
			case TestPackage.TESTABLE_FRAGMENT__COMPLETION:
				return completion != COMPLETION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public String toString() {
		return this.getName();
	}

} //TestableFragmentImpl
