/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.jesusjlopezf.utils.eclipse.xtext;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.ISetup;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.serializer.impl.Serializer;
import org.eclipse.xtext.service.DefaultRuntimeModule;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.jesusjlopezf.utils.resources.FileUtils;


public class XtextUtils {
	public static String serialize(DefaultRuntimeModule runtimeModule, EObject eo){
		Injector injector = Guice.createInjector(runtimeModule);
		Serializer serializer = injector.getInstance(Serializer.class);
		return serializer.serialize(eo);
	}
	
	public static EObject loadXtextFile(File file, ISetup standaloneSetup, String extension) throws IOException{
		String textShell = FileUtils.readFile(file.getAbsolutePath().toString());		
		Injector injector = standaloneSetup.createInjectorAndDoEMFRegistration();
		XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		Resource resource = resourceSet.createResource(URI.createURI("dummy:/example." + extension));
		InputStream in = new ByteArrayInputStream(textShell.getBytes());
		resource.load(in, resourceSet.getLoadOptions());
		return resource.getContents().get(0);
	}
}
