/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.lexicon.preferences;

import metabup.lexicon.Activator;

import org.eclipse.jface.preference.IPreferenceStore;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String WORDNET_LOCATION = "wordnet_location";
	
	public static String getPreferenceStringValue(String preferenceName){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(preferenceName);
	}
	
	public static boolean getPreferenceBooleanValue(String preferenceName){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getBoolean(preferenceName);
	}
}
