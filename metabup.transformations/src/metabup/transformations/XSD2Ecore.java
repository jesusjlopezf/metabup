/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xsd.ecore.XSDEcoreBuilder;

public class XSD2Ecore {
	String xsdFile = null;
	
	public XSD2Ecore(String xsdFile){
		this.xsdFile = xsdFile;
	}
	
	public EObject execute(){
		if(xsdFile == null) return null;
				
		XSDEcoreBuilder xsdEcoreBuilder = new XSDEcoreBuilder();
	    ResourceSet resourceSet = new ResourceSetImpl();
	    Collection eCorePackages = xsdEcoreBuilder.generate(URI.createFileURI(xsdFile));
	    System.out.println("how many packages: " + eCorePackages.size());
	    EPackage oneEPackage = null;
	    
	    // register ecores
	    for (Iterator iter = eCorePackages.iterator(); iter.hasNext();) {
	    	EPackage element = (EPackage) iter.next();
	    	resourceSet.getPackageRegistry().put(element.getNsURI(), element);	
	    	// only one package for now...
	    	return element;
	    }

	    return null;
	}
}
