/*
 * generated by Xtext
 */
package metabest.test.fragments.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class MBFAntlrTokenFileProvider implements IAntlrTokenFileProvider {
	
	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
    	return classLoader.getResourceAsStream("metabest/test/fragments/parser/antlr/internal/InternalMBF.tokens");
	}
}
