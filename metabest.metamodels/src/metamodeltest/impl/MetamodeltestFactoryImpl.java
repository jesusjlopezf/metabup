/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.Abstract;
import metamodeltest.Abstraction;
import metamodeltest.Adjective;
import metamodeltest.AndQualifier;
import metamodeltest.Annotated;
import metamodeltest.Annotation;
import metamodeltest.Assertion;
import metamodeltest.AssertionDefinition;
import metamodeltest.AssertionLibrary;
import metamodeltest.AssertionTest;
import metamodeltest.AttributeContainment;
import metamodeltest.AttributeFilter;
import metamodeltest.AttributeSelector;
import metamodeltest.BooleanParameter;
import metamodeltest.Case;
import metamodeltest.ClassCollects;
import metamodeltest.ClassContainee;
import metamodeltest.ClassContainment;
import metamodeltest.ClassFilter;
import metamodeltest.ClassReachedBy;
import metamodeltest.ClassReaches;
import metamodeltest.ClassSelector;
import metamodeltest.CompositionLeaf;
import metamodeltest.ContLeaf;
import metamodeltest.ContRoot;
import metamodeltest.ContainmentPath;
import metamodeltest.ContainmentReference;
import metamodeltest.Contains;
import metamodeltest.CyclicPath;
import metamodeltest.DescribedElement;
import metamodeltest.ElementAssertion;
import metamodeltest.ElementFeaturement;
import metamodeltest.ElementFilter;
import metamodeltest.ElementRelation;
import metamodeltest.ElementSelector;
import metamodeltest.Every;
import metamodeltest.Existance;
import metamodeltest.FeatureContainment;
import metamodeltest.FeatureFilter;
import metamodeltest.FeatureMultiplicity;
import metamodeltest.FeatureSelector;
import metamodeltest.FixedBound;
import metamodeltest.GrammaticalNumber;
import metamodeltest.Import;
import metamodeltest.Infinite;
import metamodeltest.InhLeaf;
import metamodeltest.InhRoot;
import metamodeltest.InheritedFeature;
import metamodeltest.InheritedFeatureContainment;
import metamodeltest.IntegerParameter;
import metamodeltest.IsSubTo;
import metamodeltest.IsSuperTo;
import metamodeltest.Length;
import metamodeltest.LessThan;
import metamodeltest.LibraryAssertionCall;
import metamodeltest.Literal;
import metamodeltest.LowerCase;
import metamodeltest.MaxMultiplicity;
import metamodeltest.MetaModelTest;
import metamodeltest.MetamodeltestFactory;
import metamodeltest.MetamodeltestPackage;
import metamodeltest.MoreThan;
import metamodeltest.Name;
import metamodeltest.Named;
import metamodeltest.NamedAfter;
import metamodeltest.None;
import metamodeltest.Noun;
import metamodeltest.OCLExpression;
import metamodeltest.OppositeTo;
import metamodeltest.OrQualifier;
import metamodeltest.ParameterElementSetValue;
import metamodeltest.ParameterStringValue;
import metamodeltest.PathAnd;
import metamodeltest.PathFilter;
import metamodeltest.PathOr;
import metamodeltest.PathSelector;
import metamodeltest.PathSource;
import metamodeltest.PathTarget;
import metamodeltest.PhraseNamed;
import metamodeltest.Prefix;
import metamodeltest.ReferenceContainment;
import metamodeltest.ReferenceFilter;
import metamodeltest.ReferenceSelector;
import metamodeltest.SelfInheritance;
import metamodeltest.Some;
import metamodeltest.SourceClass;
import metamodeltest.StepClass;
import metamodeltest.StrictNumber;
import metamodeltest.StringParameter;
import metamodeltest.Suffix;
import metamodeltest.Synonym;
import metamodeltest.TargetClass;
import metamodeltest.TrivialString;
import metamodeltest.Typed;
import metamodeltest.UpperCase;
import metamodeltest.UserDefinedParameter;
import metamodeltest.Verb;
import metamodeltest.WithInheritancePath;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MetamodeltestFactoryImpl extends EFactoryImpl implements MetamodeltestFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MetamodeltestFactory init() {
		try {
			MetamodeltestFactory theMetamodeltestFactory = (MetamodeltestFactory)EPackage.Registry.INSTANCE.getEFactory(MetamodeltestPackage.eNS_URI);
			if (theMetamodeltestFactory != null) {
				return theMetamodeltestFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MetamodeltestFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetamodeltestFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MetamodeltestPackage.META_MODEL_TEST: return createMetaModelTest();
			case MetamodeltestPackage.ASSERTION: return createAssertion();
			case MetamodeltestPackage.NONE: return createNone();
			case MetamodeltestPackage.NUMBER: return createNumber();
			case MetamodeltestPackage.EVERY: return createEvery();
			case MetamodeltestPackage.LESS_THAN: return createLessThan();
			case MetamodeltestPackage.MORE_THAN: return createMoreThan();
			case MetamodeltestPackage.ELEMENT_ASSERTION: return createElementAssertion();
			case MetamodeltestPackage.NAME: return createName();
			case MetamodeltestPackage.ABSTRACT: return createAbstract();
			case MetamodeltestPackage.ELEMENT_FILTER: return createElementFilter();
			case MetamodeltestPackage.CLASS_FILTER: return createClassFilter();
			case MetamodeltestPackage.FEATURE_FILTER: return createFeatureFilter();
			case MetamodeltestPackage.ATTRIBUTE_FILTER: return createAttributeFilter();
			case MetamodeltestPackage.REFERENCE_FILTER: return createReferenceFilter();
			case MetamodeltestPackage.NAMED: return createNamed();
			case MetamodeltestPackage.ABSTRACTION: return createAbstraction();
			case MetamodeltestPackage.IS_SUPER_TO: return createIsSuperTo();
			case MetamodeltestPackage.IS_SUB_TO: return createIsSubTo();
			case MetamodeltestPackage.FEATURE_CONTAINMENT: return createFeatureContainment();
			case MetamodeltestPackage.INHERITED_FEATURE_CONTAINMENT: return createInheritedFeatureContainment();
			case MetamodeltestPackage.INH_LEAF: return createInhLeaf();
			case MetamodeltestPackage.INH_ROOT: return createInhRoot();
			case MetamodeltestPackage.COMPOSITION_LEAF: return createCompositionLeaf();
			case MetamodeltestPackage.MAX_MULTIPLICITY: return createMaxMultiplicity();
			case MetamodeltestPackage.FIXED_BOUND: return createFixedBound();
			case MetamodeltestPackage.INFINITE: return createInfinite();
			case MetamodeltestPackage.CLASS_CONTAINMENT: return createClassContainment();
			case MetamodeltestPackage.CLASS_CONTAINEE: return createClassContainee();
			case MetamodeltestPackage.TYPED: return createTyped();
			case MetamodeltestPackage.PREFIX: return createPrefix();
			case MetamodeltestPackage.SUFFIX: return createSuffix();
			case MetamodeltestPackage.CONTAINS: return createContains();
			case MetamodeltestPackage.STRICT_NUMBER: return createStrictNumber();
			case MetamodeltestPackage.FEATURE_MULTIPLICITY: return createFeatureMultiplicity();
			case MetamodeltestPackage.UPPER_CASE: return createUpperCase();
			case MetamodeltestPackage.LOWER_CASE: return createLowerCase();
			case MetamodeltestPackage.ASSERTION_LIBRARY: return createAssertionLibrary();
			case MetamodeltestPackage.ASSERTION_TEST: return createAssertionTest();
			case MetamodeltestPackage.ASSERTION_DEFINITION: return createAssertionDefinition();
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL: return createLibraryAssertionCall();
			case MetamodeltestPackage.BOOLEAN_PARAMETER: return createBooleanParameter();
			case MetamodeltestPackage.INTEGER_PARAMETER: return createIntegerParameter();
			case MetamodeltestPackage.STRING_PARAMETER: return createStringParameter();
			case MetamodeltestPackage.USER_DEFINED_PARAMETER: return createUserDefinedParameter();
			case MetamodeltestPackage.PATH_FILTER: return createPathFilter();
			case MetamodeltestPackage.LENGTH: return createLength();
			case MetamodeltestPackage.SOURCE_CLASS: return createSourceClass();
			case MetamodeltestPackage.TARGET_CLASS: return createTargetClass();
			case MetamodeltestPackage.CONTAINMENT_REFERENCE: return createContainmentReference();
			case MetamodeltestPackage.PATH_TARGET: return createPathTarget();
			case MetamodeltestPackage.PATH_SOURCE: return createPathSource();
			case MetamodeltestPackage.ELEMENT_FEATUREMENT: return createElementFeaturement();
			case MetamodeltestPackage.STEP_CLASS: return createStepClass();
			case MetamodeltestPackage.EXISTANCE: return createExistance();
			case MetamodeltestPackage.ELEMENT_SELECTOR: return createElementSelector();
			case MetamodeltestPackage.CLASS_SELECTOR: return createClassSelector();
			case MetamodeltestPackage.FEATURE_SELECTOR: return createFeatureSelector();
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR: return createAttributeSelector();
			case MetamodeltestPackage.REFERENCE_SELECTOR: return createReferenceSelector();
			case MetamodeltestPackage.PATH_SELECTOR: return createPathSelector();
			case MetamodeltestPackage.AND_QUALIFIER: return createAndQualifier();
			case MetamodeltestPackage.OR_QUALIFIER: return createOrQualifier();
			case MetamodeltestPackage.PATH_AND: return createPathAnd();
			case MetamodeltestPackage.PATH_OR: return createPathOr();
			case MetamodeltestPackage.ATTRIBUTE_CONTAINMENT: return createAttributeContainment();
			case MetamodeltestPackage.REFERENCE_CONTAINMENT: return createReferenceContainment();
			case MetamodeltestPackage.CONT_ROOT: return createContRoot();
			case MetamodeltestPackage.CONT_LEAF: return createContLeaf();
			case MetamodeltestPackage.ELEMENT_RELATION: return createElementRelation();
			case MetamodeltestPackage.CLASS_REACHES: return createClassReaches();
			case MetamodeltestPackage.CLASS_REACHED_BY: return createClassReachedBy();
			case MetamodeltestPackage.CONTAINMENT_PATH: return createContainmentPath();
			case MetamodeltestPackage.WITH_INHERITANCE_PATH: return createWithInheritancePath();
			case MetamodeltestPackage.CYCLIC_PATH: return createCyclicPath();
			case MetamodeltestPackage.SOME: return createSome();
			case MetamodeltestPackage.INHERITED_FEATURE: return createInheritedFeature();
			case MetamodeltestPackage.CLASS_COLLECTS: return createClassCollects();
			case MetamodeltestPackage.ANNOTATED: return createAnnotated();
			case MetamodeltestPackage.IMPORT: return createImport();
			case MetamodeltestPackage.SELF_INHERITANCE: return createSelfInheritance();
			case MetamodeltestPackage.ANNOTATION: return createAnnotation();
			case MetamodeltestPackage.DESCRIBED_ELEMENT: return createDescribedElement();
			case MetamodeltestPackage.SYNONYM: return createSynonym();
			case MetamodeltestPackage.VERB: return createVerb();
			case MetamodeltestPackage.NOUN: return createNoun();
			case MetamodeltestPackage.ADJECTIVE: return createAdjective();
			case MetamodeltestPackage.PHRASE_NAMED: return createPhraseNamed();
			case MetamodeltestPackage.LITERAL: return createLiteral();
			case MetamodeltestPackage.TRIVIAL_STRING: return createTrivialString();
			case MetamodeltestPackage.OPPOSITE_TO: return createOppositeTo();
			case MetamodeltestPackage.PARAMETER_STRING_VALUE: return createParameterStringValue();
			case MetamodeltestPackage.PARAMETER_ELEMENT_SET_VALUE: return createParameterElementSetValue();
			case MetamodeltestPackage.NAMED_AFTER: return createNamedAfter();
			case MetamodeltestPackage.OCL_EXPRESSION: return createOCLExpression();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MetamodeltestPackage.CASE:
				return createCaseFromString(eDataType, initialValue);
			case MetamodeltestPackage.GRAMMATICAL_NUMBER:
				return createGrammaticalNumberFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MetamodeltestPackage.CASE:
				return convertCaseToString(eDataType, instanceValue);
			case MetamodeltestPackage.GRAMMATICAL_NUMBER:
				return convertGrammaticalNumberToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaModelTest createMetaModelTest() {
		MetaModelTestImpl metaModelTest = new MetaModelTestImpl();
		return metaModelTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion createAssertion() {
		AssertionImpl assertion = new AssertionImpl();
		return assertion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public None createNone() {
		NoneImpl none = new NoneImpl();
		return none;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public metamodeltest.Number createNumber() {
		NumberImpl number = new NumberImpl();
		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Every createEvery() {
		EveryImpl every = new EveryImpl();
		return every;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LessThan createLessThan() {
		LessThanImpl lessThan = new LessThanImpl();
		return lessThan;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MoreThan createMoreThan() {
		MoreThanImpl moreThan = new MoreThanImpl();
		return moreThan;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementAssertion createElementAssertion() {
		ElementAssertionImpl elementAssertion = new ElementAssertionImpl();
		return elementAssertion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Name createName() {
		NameImpl name = new NameImpl();
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Abstract createAbstract() {
		AbstractImpl abstract_ = new AbstractImpl();
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementFilter createElementFilter() {
		ElementFilterImpl elementFilter = new ElementFilterImpl();
		return elementFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassFilter createClassFilter() {
		ClassFilterImpl classFilter = new ClassFilterImpl();
		return classFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureFilter createFeatureFilter() {
		FeatureFilterImpl featureFilter = new FeatureFilterImpl();
		return featureFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeFilter createAttributeFilter() {
		AttributeFilterImpl attributeFilter = new AttributeFilterImpl();
		return attributeFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceFilter createReferenceFilter() {
		ReferenceFilterImpl referenceFilter = new ReferenceFilterImpl();
		return referenceFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Named createNamed() {
		NamedImpl named = new NamedImpl();
		return named;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Abstraction createAbstraction() {
		AbstractionImpl abstraction = new AbstractionImpl();
		return abstraction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsSuperTo createIsSuperTo() {
		IsSuperToImpl isSuperTo = new IsSuperToImpl();
		return isSuperTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IsSubTo createIsSubTo() {
		IsSubToImpl isSubTo = new IsSubToImpl();
		return isSubTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureContainment createFeatureContainment() {
		FeatureContainmentImpl featureContainment = new FeatureContainmentImpl();
		return featureContainment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InheritedFeatureContainment createInheritedFeatureContainment() {
		InheritedFeatureContainmentImpl inheritedFeatureContainment = new InheritedFeatureContainmentImpl();
		return inheritedFeatureContainment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InhLeaf createInhLeaf() {
		InhLeafImpl inhLeaf = new InhLeafImpl();
		return inhLeaf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InhRoot createInhRoot() {
		InhRootImpl inhRoot = new InhRootImpl();
		return inhRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositionLeaf createCompositionLeaf() {
		CompositionLeafImpl compositionLeaf = new CompositionLeafImpl();
		return compositionLeaf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaxMultiplicity createMaxMultiplicity() {
		MaxMultiplicityImpl maxMultiplicity = new MaxMultiplicityImpl();
		return maxMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FixedBound createFixedBound() {
		FixedBoundImpl fixedBound = new FixedBoundImpl();
		return fixedBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Infinite createInfinite() {
		InfiniteImpl infinite = new InfiniteImpl();
		return infinite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassContainment createClassContainment() {
		ClassContainmentImpl classContainment = new ClassContainmentImpl();
		return classContainment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassContainee createClassContainee() {
		ClassContaineeImpl classContainee = new ClassContaineeImpl();
		return classContainee;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Typed createTyped() {
		TypedImpl typed = new TypedImpl();
		return typed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Prefix createPrefix() {
		PrefixImpl prefix = new PrefixImpl();
		return prefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Suffix createSuffix() {
		SuffixImpl suffix = new SuffixImpl();
		return suffix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Contains createContains() {
		ContainsImpl contains = new ContainsImpl();
		return contains;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StrictNumber createStrictNumber() {
		StrictNumberImpl strictNumber = new StrictNumberImpl();
		return strictNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMultiplicity createFeatureMultiplicity() {
		FeatureMultiplicityImpl featureMultiplicity = new FeatureMultiplicityImpl();
		return featureMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UpperCase createUpperCase() {
		UpperCaseImpl upperCase = new UpperCaseImpl();
		return upperCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LowerCase createLowerCase() {
		LowerCaseImpl lowerCase = new LowerCaseImpl();
		return lowerCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionLibrary createAssertionLibrary() {
		AssertionLibraryImpl assertionLibrary = new AssertionLibraryImpl();
		return assertionLibrary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionTest createAssertionTest() {
		AssertionTestImpl assertionTest = new AssertionTestImpl();
		return assertionTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionDefinition createAssertionDefinition() {
		AssertionDefinitionImpl assertionDefinition = new AssertionDefinitionImpl();
		return assertionDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibraryAssertionCall createLibraryAssertionCall() {
		LibraryAssertionCallImpl libraryAssertionCall = new LibraryAssertionCallImpl();
		return libraryAssertionCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameter createBooleanParameter() {
		BooleanParameterImpl booleanParameter = new BooleanParameterImpl();
		return booleanParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerParameter createIntegerParameter() {
		IntegerParameterImpl integerParameter = new IntegerParameterImpl();
		return integerParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringParameter createStringParameter() {
		StringParameterImpl stringParameter = new StringParameterImpl();
		return stringParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserDefinedParameter createUserDefinedParameter() {
		UserDefinedParameterImpl userDefinedParameter = new UserDefinedParameterImpl();
		return userDefinedParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathFilter createPathFilter() {
		PathFilterImpl pathFilter = new PathFilterImpl();
		return pathFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Length createLength() {
		LengthImpl length = new LengthImpl();
		return length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SourceClass createSourceClass() {
		SourceClassImpl sourceClass = new SourceClassImpl();
		return sourceClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TargetClass createTargetClass() {
		TargetClassImpl targetClass = new TargetClassImpl();
		return targetClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContainmentReference createContainmentReference() {
		ContainmentReferenceImpl containmentReference = new ContainmentReferenceImpl();
		return containmentReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathTarget createPathTarget() {
		PathTargetImpl pathTarget = new PathTargetImpl();
		return pathTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathSource createPathSource() {
		PathSourceImpl pathSource = new PathSourceImpl();
		return pathSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementFeaturement createElementFeaturement() {
		ElementFeaturementImpl elementFeaturement = new ElementFeaturementImpl();
		return elementFeaturement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StepClass createStepClass() {
		StepClassImpl stepClass = new StepClassImpl();
		return stepClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Existance createExistance() {
		ExistanceImpl existance = new ExistanceImpl();
		return existance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementSelector createElementSelector() {
		ElementSelectorImpl elementSelector = new ElementSelectorImpl();
		return elementSelector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSelector createClassSelector() {
		ClassSelectorImpl classSelector = new ClassSelectorImpl();
		return classSelector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureSelector createFeatureSelector() {
		FeatureSelectorImpl featureSelector = new FeatureSelectorImpl();
		return featureSelector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeSelector createAttributeSelector() {
		AttributeSelectorImpl attributeSelector = new AttributeSelectorImpl();
		return attributeSelector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceSelector createReferenceSelector() {
		ReferenceSelectorImpl referenceSelector = new ReferenceSelectorImpl();
		return referenceSelector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathSelector createPathSelector() {
		PathSelectorImpl pathSelector = new PathSelectorImpl();
		return pathSelector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AndQualifier createAndQualifier() {
		AndQualifierImpl andQualifier = new AndQualifierImpl();
		return andQualifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrQualifier createOrQualifier() {
		OrQualifierImpl orQualifier = new OrQualifierImpl();
		return orQualifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathAnd createPathAnd() {
		PathAndImpl pathAnd = new PathAndImpl();
		return pathAnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathOr createPathOr() {
		PathOrImpl pathOr = new PathOrImpl();
		return pathOr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeContainment createAttributeContainment() {
		AttributeContainmentImpl attributeContainment = new AttributeContainmentImpl();
		return attributeContainment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceContainment createReferenceContainment() {
		ReferenceContainmentImpl referenceContainment = new ReferenceContainmentImpl();
		return referenceContainment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContRoot createContRoot() {
		ContRootImpl contRoot = new ContRootImpl();
		return contRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContLeaf createContLeaf() {
		ContLeafImpl contLeaf = new ContLeafImpl();
		return contLeaf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementRelation createElementRelation() {
		ElementRelationImpl elementRelation = new ElementRelationImpl();
		return elementRelation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassReaches createClassReaches() {
		ClassReachesImpl classReaches = new ClassReachesImpl();
		return classReaches;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassReachedBy createClassReachedBy() {
		ClassReachedByImpl classReachedBy = new ClassReachedByImpl();
		return classReachedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContainmentPath createContainmentPath() {
		ContainmentPathImpl containmentPath = new ContainmentPathImpl();
		return containmentPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WithInheritancePath createWithInheritancePath() {
		WithInheritancePathImpl withInheritancePath = new WithInheritancePathImpl();
		return withInheritancePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CyclicPath createCyclicPath() {
		CyclicPathImpl cyclicPath = new CyclicPathImpl();
		return cyclicPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Some createSome() {
		SomeImpl some = new SomeImpl();
		return some;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InheritedFeature createInheritedFeature() {
		InheritedFeatureImpl inheritedFeature = new InheritedFeatureImpl();
		return inheritedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassCollects createClassCollects() {
		ClassCollectsImpl classCollects = new ClassCollectsImpl();
		return classCollects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotated createAnnotated() {
		AnnotatedImpl annotated = new AnnotatedImpl();
		return annotated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Import createImport() {
		ImportImpl import_ = new ImportImpl();
		return import_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelfInheritance createSelfInheritance() {
		SelfInheritanceImpl selfInheritance = new SelfInheritanceImpl();
		return selfInheritance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation createAnnotation() {
		AnnotationImpl annotation = new AnnotationImpl();
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DescribedElement createDescribedElement() {
		DescribedElementImpl describedElement = new DescribedElementImpl();
		return describedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Synonym createSynonym() {
		SynonymImpl synonym = new SynonymImpl();
		return synonym;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Verb createVerb() {
		VerbImpl verb = new VerbImpl();
		return verb;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Noun createNoun() {
		NounImpl noun = new NounImpl();
		return noun;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Adjective createAdjective() {
		AdjectiveImpl adjective = new AdjectiveImpl();
		return adjective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhraseNamed createPhraseNamed() {
		PhraseNamedImpl phraseNamed = new PhraseNamedImpl();
		return phraseNamed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Literal createLiteral() {
		LiteralImpl literal = new LiteralImpl();
		return literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrivialString createTrivialString() {
		TrivialStringImpl trivialString = new TrivialStringImpl();
		return trivialString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OppositeTo createOppositeTo() {
		OppositeToImpl oppositeTo = new OppositeToImpl();
		return oppositeTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterStringValue createParameterStringValue() {
		ParameterStringValueImpl parameterStringValue = new ParameterStringValueImpl();
		return parameterStringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterElementSetValue createParameterElementSetValue() {
		ParameterElementSetValueImpl parameterElementSetValue = new ParameterElementSetValueImpl();
		return parameterElementSetValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedAfter createNamedAfter() {
		NamedAfterImpl namedAfter = new NamedAfterImpl();
		return namedAfter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLExpression createOCLExpression() {
		OCLExpressionImpl oclExpression = new OCLExpressionImpl();
		return oclExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Case createCaseFromString(EDataType eDataType, String initialValue) {
		Case result = Case.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GrammaticalNumber createGrammaticalNumberFromString(EDataType eDataType, String initialValue) {
		GrammaticalNumber result = GrammaticalNumber.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGrammaticalNumberToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetamodeltestPackage getMetamodeltestPackage() {
		return (MetamodeltestPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MetamodeltestPackage getPackage() {
		return MetamodeltestPackage.eINSTANCE;
	}

} //MetamodeltestFactoryImpl
