/****************************************************************************
 * This demo file is part of the yFiles extension package ySVG-2.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * ySVG demo files exhibit ySVG functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a ySVG version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own ySVG powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the ySVG
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.svg;

import yext.svg.io.SVGIOHandler;
import yext.svg.io.SVGZIOHandler;

import y.io.SuffixFileFilter;
import y.option.ConstraintManager;
import y.option.OptionGroup;
import y.option.OptionHandler;
import y.util.D;
import y.view.Arrow;
import y.view.DefaultBackgroundRenderer;
import y.view.Graph2D;
import y.view.Graph2DView;
import y.view.YRenderingHints;

import java.awt.EventQueue;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JToolBar;

/** 
 *  Demonstrates how to export a graph to SVG or SVGZ. 
 */
public class SVGExportDemo extends ViewActionDemo
{
  /**
   * Initializes this demo.
   */
  public SVGExportDemo() {
    view.getGraph2D().getDefaultEdgeRealizer().setTargetArrow(Arrow.STANDARD);
  }

  /**
   * Creates a toolbar for this demo.
   */
  protected JToolBar createToolBar()
  {
    JToolBar jtb = super.createToolBar();
    jtb.addSeparator();
    jtb.add(new ExportAction(createSVGIOHandler(false), createSVGIOHandler(true)));
    return jtb;
  }
  
  /**
   * Creates and configures the SVG output handlers to be used.
   * @param svgz  <code>true</code> if the returned ouput handler should
   * compress the generated SVG documents; <code>false</code> otherwise.
   * @return an output handler that generates SVG documents.
   */
  protected SVGIOHandler createSVGIOHandler(boolean svgz)
  {
    return svgz ? new SVGZIOHandler() : new SVGIOHandler();
  }
    
  /**
   * Action that exports the given graph to SVG format
   */
  class ExportAction extends AbstractAction
  {
    SVGIOHandler svg;
    SVGIOHandler svgz;
    OptionHandler options;

    ExportAction( SVGIOHandler svg, SVGIOHandler svgz ) {
      final String name = "SVG Export";
      putValue(Action.NAME, name);
      putValue(Action.SHORT_DESCRIPTION, "Exports the displayed graph to a SVG document.");

      this.svg  = svg;
      this.svgz = svgz;

      options = new OptionHandler(name);
      final OptionGroup group = new OptionGroup();
      group.setAttribute(OptionGroup.ATTRIBUTE_TITLE, name + " Options");
      group.addItem(options.addBool("Compressed Output (SVGZ)", false));
      group.addItem(options.addEnum("Size", new String[]{"Use Original Size", "Use Custom Width", "Use Custom Height"}, 0));
      group.addItem(options.addInt("Custom Width",  500));
      group.addItem(options.addInt("Custom Height", 500));
      group.addItem(options.addInt("Empty Border Width", 10));
      group.addItem(options.addEnum("Clip Region", new String[]{"Graph","View"}, 0));
      group.addItem(options.addBool("Transparent Background", true));
      group.addItem(options.addBool("Paint Selection Marker", false));

      final ConstraintManager cm = new ConstraintManager(options);
      cm.setEnabledOnValueEquals("Size", "Use Custom Width", "Custom Width");
      cm.setEnabledOnValueEquals("Size", "Use Custom Height", "Custom Height");
    }


    void configureViewPort(Graph2DView viewPort)
    {
      Graph2D graph = view.getGraph2D();
      
      double width  = options.getInt("Custom Width");
      double height = options.getInt("Custom Height");
      Point viewPoint = viewPort.getViewPoint();
      double zoom = 1.0;
      
      if ("Graph".equals(options.get("Clip Region")))
      {       
        Rectangle box = graph.getBoundingBox();
        int border = options.getInt("Empty Border Width");
        box.width  += 2*border;
        box.height += 2*border;
        box.x -= border;
        box.y -= border;
        
        if ("Use Custom Height".equals(options.get("Size")))
        {
          width = height*box.getWidth()/box.getHeight();
        }
        else if ("Use Custom Width".equals(options.get("Size")))
        {
          height = width*box.getHeight()/box.getWidth();
        }
        else
        {
          width =  box.getWidth();
          height = box.getHeight();
        }
        zoom = width/box.getWidth();
        viewPoint = new Point(box.x, box.y);
      }
      else if ("View".equals(options.get("Clip Region")))
      {
        if ("Use Custom Height".equals(options.get("Size")))
        {
          width = height*view.getWidth()/view.getHeight();
        }
        else if ("Use Custom Width".equals(options.get("Size")))
        {
          height = width*view.getHeight()/view.getWidth();
        }
        else
        {
          width =  view.getWidth();
          height = view.getHeight();
        }
        viewPoint = view.getViewPoint();
        zoom = view.getZoom()*width/view.getWidth();
      }
      
      viewPort.setZoom(zoom);
      viewPort.setSize((int)width, (int)height);
      viewPort.setViewPoint(viewPoint.x, viewPoint.y);
    }
    
    public void actionPerformed(ActionEvent e)
    {
      if (!options.showEditor(view.getFrame())) {
        return;
      }
      
      SVGIOHandler ioh = options.getBool("Compressed Output (SVGZ)") ? svgz : svg;
      if (options.getBool("Paint Selection Marker")) {
        ioh.removeRenderingHint(YRenderingHints.KEY_SELECTION_PAINTING);
      } else {
        ioh.addRenderingHint(
                YRenderingHints.KEY_SELECTION_PAINTING,
                YRenderingHints.VALUE_SELECTION_PAINTING_OFF);
      }
      Graph2D graph = view.getGraph2D();
      Graph2DView viewPort = ioh.createDefaultGraph2DView(graph);
      configureViewPort(viewPort);
      if (options.getBool("Transparent Background")) {
        DefaultBackgroundRenderer dbr = new DefaultBackgroundRenderer(viewPort);
        dbr.setColor(null);
        viewPort.setBackgroundRenderer(dbr);
      }
      graph.setCurrentView(viewPort);
      
      export(ioh);
      
      graph.removeView(viewPort); //unregister viewport as view 
    }
    
    void export(SVGIOHandler ioh)
    {
      JFileChooser chooser = new JFileChooser();
      chooser.setAcceptAllFileFilterUsed(false);
      String fne = ioh.getFileNameExtension(); 
      chooser.setFileFilter(new SuffixFileFilter(
        fne, ioh.getFileFormatString()));
      
      if (chooser.showSaveDialog(SVGExportDemo.this) == JFileChooser.APPROVE_OPTION)
      {
        String name = chooser.getSelectedFile().toString();
        if (!name.endsWith(fne)) {
          name = name + '.' + fne;
        }
        
        try {
          double pdt = view.getPaintDetailThreshold();
          view.setPaintDetailThreshold(0.0);
          ioh.write( view.getGraph2D(), name );
          view.setPaintDetailThreshold(pdt);
        }
        catch(IOException ex) 
        {
          D.show(ex);
        }
      }
    }
  }
  
  /**
   * Launches this demo.
   * @param args ignored.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        initLnF();
        (new SVGExportDemo()).start();
      }
    });
  }
}