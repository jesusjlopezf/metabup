/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.expressions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Nature Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.expressions.FeatureNatureExpression#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @see test.checks.expressions.ExpressionsPackage#getFeatureNatureExpression()
 * @model
 * @generated
 */
public interface FeatureNatureExpression extends EObject {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' containment reference.
	 * @see #setFeature(FeatureWithinObjectExpression)
	 * @see test.checks.expressions.ExpressionsPackage#getFeatureNatureExpression_Feature()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FeatureWithinObjectExpression getFeature();

	/**
	 * Sets the value of the '{@link test.checks.expressions.FeatureNatureExpression#getFeature <em>Feature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' containment reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(FeatureWithinObjectExpression value);

} // FeatureNatureExpression
