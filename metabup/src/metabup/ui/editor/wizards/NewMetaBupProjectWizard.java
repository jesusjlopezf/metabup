/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.wizards;

import metabup.builder.MetaBupProjectNature;
import metabup.fragments.resources.FragmentFolder;
import metabup.sketches.resources.LegendFolder;
import org.eclipse.jface.viewers.IStructuredSelection;

import java.awt.*;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveRegistry;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.operation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.core.resources.*;

import java.io.*;

import org.eclipse.ui.*;
import org.eclipse.ui.ide.IDE;

// TODO: Auto-generated Javadoc
/**
 * The Class TransformNewWizard.
 */
public class NewMetaBupProjectWizard extends Wizard implements INewWizard {
	
	/** The page. */
	private NewMetaBupProjectWizardPage page1;
	private NewInteractiveMetaModelWizardPage page2;
	
	/** The selection. */
	private ISelection selection;

	/**
	 * Constructor for TransformNewWizard.
	 */
	public NewMetaBupProjectWizard() {
		super();
		setNeedsProgressMonitor(true);
	}
	
	/**
	 * Adding the page to the wizard.
	 */
	public void addPages() {
		page1 = new NewMetaBupProjectWizardPage(selection);
		page2 = new NewInteractiveMetaModelWizardPage(selection, false);
		addPage(page1);
		addPage(page2);
	}

	/**
	 * This method is called when 'Finish' button is pressed in
	 * the wizard. We will create an operation and run it
	 * using wizard as execution context.
	 *
	 * @return true, if successful
	 */
	public boolean performFinish() {
		//final String containerName = page.getContainerName();
		final String folderName = page1.getFolderName();
		final Object result[] = page1.getResult();	
		final String fileName = page2.getFileName();
		
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					try {
						doFinish(folderName, monitor, result, fileName);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (AWTException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		
		try {
			getContainer().run(true, false, op);
			
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * The worker method. It will find the container, create the
	 * file if it's missing or just replace its contents, and open
	 * the editor on the newly created file.
	 *
	 * @param folderName the folder name
	 * @param monitor the monitor
	 * @param result the result
	 * @throws CoreException the core exception
	 * @throws InterruptedException the interrupted exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws AWTException the aWT exception
	 */
	private void doFinish(String folderName, IProgressMonitor monitor, Object result[], String fileName)
		throws CoreException, InterruptedException, IOException, AWTException {
		// create a sample file
		monitor.beginTask("Creating " + folderName, 6);		
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();		
		IProject project  = root.getProject(folderName);
		
		project.create(monitor);
		project.open(monitor);
		
		// assigns metabup.project.nature to the new project
		IProjectDescription description = project.getDescription();		
		ArrayList<String> newIds = new ArrayList<String>();
		newIds.add(MetaBupProjectNature.NATURE_ID);
		
		description.setNatureIds((String[]) newIds.toArray(new String[newIds.size()]));
		try{
			project.setDescription(description, monitor);
		} catch(Exception e){
			System.out.println("metaBup project nature couldn't be assigned for some reason");
		}
		
		// create metabup.sketches.legend folder
		LegendFolder folder = new LegendFolder(project);
		folder.create(monitor);
		monitor.worked(1);
		
		FragmentFolder fFolder = new FragmentFolder(project);
		fFolder.create(monitor);
		monitor.worked(1);
		
		// refresh project so new files are present in workspace
		project.refreshLocal(IProject.DEPTH_INFINITE, monitor);
		
		getShell().getDisplay().asyncExec(new Runnable() {			
			public void run() {
				@SuppressWarnings("unused")
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			}			
		});			
		
		monitor.worked(1);
		
		final IFile file = project.getFile(new Path(fileName));
		
		try {
			InputStream stream = openContentStream();
			if (file.exists()) {
				file.setContents(stream, true, true, monitor);
			} else {
				file.create(stream, true, monitor);											
			}
			stream.close();
		} catch (IOException e) {
		}
		
		monitor.worked(1);
		monitor.setTaskName("Opening file for editing...");
		getShell().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbenchPage page =
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();								
				try {
					IDE.openEditor(page, file, true);
					IPerspectiveRegistry reg = PlatformUI.getWorkbench().getPerspectiveRegistry();
					IPerspectiveDescriptor per = reg.findPerspectiveWithId("metabup.perspective.MetaBup");
					if (per != null) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().setPerspective(per);
				} catch (PartInitException e) {
				}
			}
		});
		monitor.worked(1);
		
		monitor.done();
	}	
	
	
	/**
	 * We will initialize file contents with a sample text.
	 */

	private InputStream openContentStream() {
		String contents =
			"shell new_metamodel" +
			"\n" +
			"\nfragment f{" +
			"\n" +
			"\n}";
		return new ByteArrayInputStream(contents.getBytes());
	}

	/**
	 * We will accept the selection in the workbench to see if
	 * we can initialize from it.
	 *
	 * @param workbench the workbench
	 * @param selection the selection
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}