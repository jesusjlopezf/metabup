/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import java.util.Collection;
import metamodeltest.AssertionCall;
import metamodeltest.AssertionTest;
import metamodeltest.DescribedElement;
import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assertion Test</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.AssertionTestImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link metamodeltest.impl.AssertionTestImpl#getMetamodelURI <em>Metamodel URI</em>}</li>
 *   <li>{@link metamodeltest.impl.AssertionTestImpl#getAssertions <em>Assertions</em>}</li>
 *   <li>{@link metamodeltest.impl.AssertionTestImpl#getDirURI <em>Dir URI</em>}</li>
 *   <li>{@link metamodeltest.impl.AssertionTestImpl#getCorrespondingEcoreURI <em>Corresponding Ecore URI</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssertionTestImpl extends AnnotatedElementImpl implements AssertionTest {
	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getMetamodelURI() <em>Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetamodelURI()
	 * @generated
	 * @ordered
	 */
	protected static final String METAMODEL_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMetamodelURI() <em>Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetamodelURI()
	 * @generated
	 * @ordered
	 */
	protected String metamodelURI = METAMODEL_URI_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAssertions() <em>Assertions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertions()
	 * @generated
	 * @ordered
	 */
	protected EList<AssertionCall> assertions;

	/**
	 * The default value of the '{@link #getDirURI() <em>Dir URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirURI()
	 * @generated
	 * @ordered
	 */
	protected static final String DIR_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDirURI() <em>Dir URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirURI()
	 * @generated
	 * @ordered
	 */
	protected String dirURI = DIR_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getCorrespondingEcoreURI() <em>Corresponding Ecore URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondingEcoreURI()
	 * @generated
	 * @ordered
	 */
	protected static final String CORRESPONDING_ECORE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCorrespondingEcoreURI() <em>Corresponding Ecore URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrespondingEcoreURI()
	 * @generated
	 * @ordered
	 */
	protected String correspondingEcoreURI = CORRESPONDING_ECORE_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertionTestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.ASSERTION_TEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ASSERTION_TEST__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMetamodelURI() {
		return metamodelURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetamodelURI(String newMetamodelURI) {
		String oldMetamodelURI = metamodelURI;
		metamodelURI = newMetamodelURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ASSERTION_TEST__METAMODEL_URI, oldMetamodelURI, metamodelURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AssertionCall> getAssertions() {
		if (assertions == null) {
			assertions = new EObjectContainmentEList<AssertionCall>(AssertionCall.class, this, MetamodeltestPackage.ASSERTION_TEST__ASSERTIONS);
		}
		return assertions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDirURI() {
		return dirURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirURI(String newDirURI) {
		String oldDirURI = dirURI;
		dirURI = newDirURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ASSERTION_TEST__DIR_URI, oldDirURI, dirURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCorrespondingEcoreURI() {
		return correspondingEcoreURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCorrespondingEcoreURI(String newCorrespondingEcoreURI) {
		String oldCorrespondingEcoreURI = correspondingEcoreURI;
		correspondingEcoreURI = newCorrespondingEcoreURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ASSERTION_TEST__CORRESPONDING_ECORE_URI, oldCorrespondingEcoreURI, correspondingEcoreURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_TEST__ASSERTIONS:
				return ((InternalEList<?>)getAssertions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_TEST__DESCRIPTION:
				return getDescription();
			case MetamodeltestPackage.ASSERTION_TEST__METAMODEL_URI:
				return getMetamodelURI();
			case MetamodeltestPackage.ASSERTION_TEST__ASSERTIONS:
				return getAssertions();
			case MetamodeltestPackage.ASSERTION_TEST__DIR_URI:
				return getDirURI();
			case MetamodeltestPackage.ASSERTION_TEST__CORRESPONDING_ECORE_URI:
				return getCorrespondingEcoreURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_TEST__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case MetamodeltestPackage.ASSERTION_TEST__METAMODEL_URI:
				setMetamodelURI((String)newValue);
				return;
			case MetamodeltestPackage.ASSERTION_TEST__ASSERTIONS:
				getAssertions().clear();
				getAssertions().addAll((Collection<? extends AssertionCall>)newValue);
				return;
			case MetamodeltestPackage.ASSERTION_TEST__DIR_URI:
				setDirURI((String)newValue);
				return;
			case MetamodeltestPackage.ASSERTION_TEST__CORRESPONDING_ECORE_URI:
				setCorrespondingEcoreURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_TEST__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case MetamodeltestPackage.ASSERTION_TEST__METAMODEL_URI:
				setMetamodelURI(METAMODEL_URI_EDEFAULT);
				return;
			case MetamodeltestPackage.ASSERTION_TEST__ASSERTIONS:
				getAssertions().clear();
				return;
			case MetamodeltestPackage.ASSERTION_TEST__DIR_URI:
				setDirURI(DIR_URI_EDEFAULT);
				return;
			case MetamodeltestPackage.ASSERTION_TEST__CORRESPONDING_ECORE_URI:
				setCorrespondingEcoreURI(CORRESPONDING_ECORE_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_TEST__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case MetamodeltestPackage.ASSERTION_TEST__METAMODEL_URI:
				return METAMODEL_URI_EDEFAULT == null ? metamodelURI != null : !METAMODEL_URI_EDEFAULT.equals(metamodelURI);
			case MetamodeltestPackage.ASSERTION_TEST__ASSERTIONS:
				return assertions != null && !assertions.isEmpty();
			case MetamodeltestPackage.ASSERTION_TEST__DIR_URI:
				return DIR_URI_EDEFAULT == null ? dirURI != null : !DIR_URI_EDEFAULT.equals(dirURI);
			case MetamodeltestPackage.ASSERTION_TEST__CORRESPONDING_ECORE_URI:
				return CORRESPONDING_ECORE_URI_EDEFAULT == null ? correspondingEcoreURI != null : !CORRESPONDING_ECORE_URI_EDEFAULT.equals(correspondingEcoreURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == DescribedElement.class) {
			switch (derivedFeatureID) {
				case MetamodeltestPackage.ASSERTION_TEST__DESCRIPTION: return MetamodeltestPackage.DESCRIBED_ELEMENT__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == DescribedElement.class) {
			switch (baseFeatureID) {
				case MetamodeltestPackage.DESCRIBED_ELEMENT__DESCRIPTION: return MetamodeltestPackage.ASSERTION_TEST__DESCRIPTION;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	@Override
	public String toString() {
		if(this.getDescription() != null) return this.getDescription();
		else return "test (metamodel \"" + this.getMetamodelURI() + "\")";
	}

} //AssertionTestImpl
