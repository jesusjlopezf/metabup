/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.general;

import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.util.EcoreUtil;

import metabup.annotations.general.Annotation;
import metabup.assistance.ISolution;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.AnnotationParam;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.StringValue;

public class Solution implements ISolution {
	private String description;
	private List<AnnotatedElement> elements; 	

	private metabup.metamodel.Annotation annotation;
	private HashMap<String, String> annotationParams = new HashMap<String, String>();
	
	public Solution(String description, List<AnnotatedElement> elements, Annotation annotation){
		this.description = description;
		this.elements = elements;
		
		this.annotation = MetamodelFactory.eINSTANCE.createAnnotation();
		this.annotation.setName(annotation.getName());						
	}
	
	public List<AnnotatedElement> getElements() {
		return elements;
	}
	
	@Override
	public void addParam(String name, String value){
		if(annotation == null) return;
		annotationParams.put(name, value);		
		metabup.metamodel.AnnotationParam annParam = MetamodelFactory.eINSTANCE.createAnnotationParam();
		annParam.setName((String) name);
		StringValue sv = MetamodelFactory.eINSTANCE.createStringValue();
		sv.setValue(value);
		annParam.setValue(sv);
		this.annotation.getParams().add(annParam);
	}
	
	@Override
	public String getDescription(){
		return description;
	}

	public metabup.metamodel.Annotation getAnnotation() {
		metabup.metamodel.Annotation an = MetamodelFactory.eINSTANCE.createAnnotation();
		an.setName(annotation.getName());		
		for(AnnotationParam ap : annotation.getParams()) an.getParams().add(EcoreUtil.copy(ap));		
		return an;
	}
}
