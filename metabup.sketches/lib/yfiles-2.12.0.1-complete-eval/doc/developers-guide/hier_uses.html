<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   
      <title>Working with Graph Hierarchies</title>
      <link rel="stylesheet" href="ystyle.css" type="text/css">
      <meta name="generator" content="DocBook XSL Stylesheets V1.65.1">
      <link rel="home" href="index.html" title="yFiles for Java Developer's Guide">
      <link rel="up" href="view_hierarchy.html" title="Chapter&nbsp;7.&nbsp;Graph Hierarchies">
      <link rel="previous" href="view_hierarchy.html" title="Chapter&nbsp;7.&nbsp;Graph Hierarchies">
      <link rel="next" href="hier_mvc_model.html" title="Managing Graph Hierarchies">
      <link rel="stylesheet" href="jsdt/toc.css" type="text/css"><script type="text/javascript" src="jsdt/jquery.min.js"></script><script type="text/javascript" src="jsdt/toc.js"></script><link type="text/css" rel="stylesheet" href="jssh/SyntaxHighlighter.css"><script type="text/javascript" src="jssh/shCore.js"></script><script type="text/javascript" src="jssh/shBrushJava.js"></script><script type="text/javascript" src="jssh/shBrushXml.js"></script><script type="text/javascript">
  function sh() {

    dp.SyntaxHighlighter.HighlightAll('programlisting', false, false, false, null, false);
  }
  </script></head>
   <body onload="sh()" bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
      <div class="navheader">
         <table width="100%" summary="Navigation header">
            <tr>
               <th colspan="3" align="center">Working with Graph Hierarchies</th>
            </tr>
            <tr>
               <td width="20%" align="left"><a accesskey="p" href="view_hierarchy.html">Prev</a>&nbsp;
               </td>
               <th width="60%" align="center">Chapter&nbsp;7.&nbsp;Graph Hierarchies</th>
               <td width="20%" align="right">&nbsp;<a accesskey="n" href="hier_mvc_model.html">Next</a></td>
            </tr>
         </table>
         <div class="navline"></div>
         <div style="display:none"><img src="figures/navbg.jpg" alt=""><img src="figures/navline.jpg" alt=""></div>
      </div>
      <div class="section" lang="en">
         <div class="titlepage">
            <div>
               <div>
                  <h2 class="title" style="clear: both"><a name="hier_uses"></a>Working with Graph Hierarchies
                  </h2>
               </div>
            </div>
            <div></div>
         </div>
         <p>
            The possibilities that are offered by a grouped graph can conveniently be used to
            improve a user's perception of a graph structure. 
            
            For example, large data sets can be presented in a concise manner, and 
            inherently hierarchically structured data can be modeled naturally. 
            
         </p>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h3 class="title"><a name="hier_comparison"></a>Comparing the Concepts
                     </h3>
                  </div>
               </div>
               <div></div>
            </div><a class="indexterm" name="d0e32023"></a><a class="indexterm" name="d0e32028"></a><a class="indexterm" name="d0e32033"></a><p>
               Grouping is a means to visually cluster a graph structure and have nodes that 
               somehow belong together be combined within a group node. 
               
               Nesting, in contrast, performs a real clustering in the sense that a folder 
               node's inner graph no longer bloats the original graph structure, but is stored 
               as a separate graph. 
               
            </p>
            <p>
               <a href="hier_uses.html#fig_comparison" title="Figure&nbsp;7.2.&nbsp;Differences between grouping and nesting">Figure&nbsp;7.2, &#8220;Differences between grouping and nesting&#8221;</a> presents the two concepts applied to the same 
               sample graph side by side. 
               The apparent differences between grouped nodes and nested nodes can easily be 
               recognized at first sight. 
               
            </p>
            <p>
               Moreover, inter-edge behavior is also demonstrated. 
               While all edges connect to their original end nodes with grouped nodes, the 
               same edges are remodeled as inter-edges with nested nodes. 
               
               The inter-edges connect to the folder node that contains the inner graph where 
               their end nodes (the "real" nodes) reside in. 
               
            </p>
            <div class="figure"><a name="fig_comparison"></a><p class="title"><b>Figure&nbsp;7.2.&nbsp;Differences between grouping and nesting</b></p>
               <div class="informaltable">
                  <center>
                     <table border="0">
                        <colgroup>
                           <col width="33%">
                           <col width="33%">
                           <col width="34%">
                        </colgroup>
                        <tbody valign="bottom">
                           <tr valign="bottom">
                              <td valign="bottom">
                                 <div class="mediaobject" align="center"><img src="figures/group_folder_1.jpg" align="middle" alt="Example graph..."></div>
                              </td>
                              <td valign="bottom">
                                 <div class="mediaobject" align="center"><img src="figures/group_folder_2.jpg" align="middle" alt="... featuring a group node."></div>
                              </td>
                              <td valign="bottom">
                                 <div class="mediaobject" align="center"><img src="figures/group_folder_3.jpg" align="middle" alt="... featuring a folder node."></div>
                              </td>
                           </tr>
                           <tr align="center">
                              <td align="center">
                                 (a) Example graph... 
                                 
                              </td>
                              <td align="center">
                                 (b) ... featuring a group node.
                                 
                              </td>
                              <td align="center">
                                 (c) ... featuring a folder node.
                                 
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
         </div>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h3 class="title"><a name="hier_nesting_adv"></a>Nesting Characteristics
                     </h3>
                  </div>
               </div>
               <div></div>
            </div>
            <p>
               By nesting subgraphs within folder nodes a graph can easily be decomposed into 
               a reduced version of itself and a number of inner graphs that are nicely 
               organized. 
               
               This characteristic is a perfect means to take the complexity out of a graph 
               and present a more cleaned-up view to the user. 
               At the same time, computation time of many algorithms is also reduced, since 
               they actually handle less data. 
               
            </p>
            <p>
               <a href="hier_uses.html#fig_nesting" title="Figure&nbsp;7.3.&nbsp;Folder node and its inner graph">Figure&nbsp;7.3, &#8220;Folder node and its inner graph&#8221;</a> shows to the right the inner graph that, 
               conceptually, is nested within the folder node to the left. 
               
            </p>
            <div class="figure"><a name="fig_nesting"></a><p class="title"><b>Figure&nbsp;7.3.&nbsp;Folder node and its inner graph</b></p>
               <div class="informaltable">
                  <center>
                     <table border="0">
                        <colgroup>
                           <col width="50%">
                           <col width="50%">
                        </colgroup>
                        <tbody valign="bottom">
                           <tr valign="middle">
                              <td valign="middle">
                                 <div class="mediaobject" align="center"><img src="figures/group_folder_3.jpg" align="middle" alt="Example graph with folder node..."></div>
                              </td>
                              <td valign="middle">
                                 <div class="mediaobject" align="center"><img src="figures/group_folder_4.jpg" align="middle" alt="... and the folder node's inner graph."></div>
                              </td>
                           </tr>
                           <tr align="center">
                              <td align="center">
                                 (a) Example graph with folder node...
                                 
                              </td>
                              <td align="center">
                                 (b) ... and the folder node's inner graph.
                                 
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
         </div>
      </div>
      <table class="copyright" border="0" cellpadding="0" cellspacing="0" width="100%">
         <tbody>
            <tr>
               <td align="right">
                  <p class="copyright">Copyright &copy;2004-2015, yWorks GmbH. All rights reserved.</p>
               </td>
            </tr>
         </tbody>
      </table>
      <div class="navfooter">
         <div class="navline2"></div>
         <div style="display:none"><img src="figures/navline2.jpg" alt=""><img src="figures/navbg2.jpg" alt=""></div>
         <table width="100%" summary="Navigation footer">
            <tr>
               <td width="40%" align="left"><a accesskey="p" href="view_hierarchy.html">Prev</a>&nbsp;
               </td>
               <td width="20%" align="center"><a accesskey="u" href="view_hierarchy.html">Up</a></td>
               <td width="40%" align="right">&nbsp;<a accesskey="n" href="hier_mvc_model.html">Next</a></td>
            </tr>
            <tr>
               <td width="40%" align="left" valign="top">Chapter&nbsp;7.&nbsp;Graph Hierarchies&nbsp;</td>
               <td width="20%" align="center"><a accesskey="h" href="index.html" target="_top">Home</a></td>
               <td width="40%" align="right" valign="top">&nbsp;Managing Graph Hierarchies</td>
            </tr>
         </table>
      </div>
   </body>
</html>