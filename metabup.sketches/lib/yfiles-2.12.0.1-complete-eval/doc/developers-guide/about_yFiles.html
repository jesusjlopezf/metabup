<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   
      <title>About yFiles</title>
      <link rel="stylesheet" href="ystyle.css" type="text/css">
      <meta name="generator" content="DocBook XSL Stylesheets V1.65.1">
      <link rel="home" href="index.html" title="yFiles for Java Developer's Guide">
      <link rel="up" href="preface.html" title="Chapter&nbsp;1.&nbsp;Preface">
      <link rel="previous" href="tasks.html" title="Graph-related Problems">
      <link rel="next" href="application_domains.html" title="Application Domains">
      <link rel="stylesheet" href="jsdt/toc.css" type="text/css"><script type="text/javascript" src="jsdt/jquery.min.js"></script><script type="text/javascript" src="jsdt/toc.js"></script><link type="text/css" rel="stylesheet" href="jssh/SyntaxHighlighter.css"><script type="text/javascript" src="jssh/shCore.js"></script><script type="text/javascript" src="jssh/shBrushJava.js"></script><script type="text/javascript" src="jssh/shBrushXml.js"></script><script type="text/javascript">
  function sh() {

    dp.SyntaxHighlighter.HighlightAll('programlisting', false, false, false, null, false);
  }
  </script></head>
   <body onload="sh()" bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
      <div class="navheader">
         <table width="100%" summary="Navigation header">
            <tr>
               <th colspan="3" align="center">About yFiles</th>
            </tr>
            <tr>
               <td width="20%" align="left"><a accesskey="p" href="tasks.html">Prev</a>&nbsp;
               </td>
               <th width="60%" align="center">Chapter&nbsp;1.&nbsp;Preface</th>
               <td width="20%" align="right">&nbsp;<a accesskey="n" href="application_domains.html">Next</a></td>
            </tr>
         </table>
         <div class="navline"></div>
         <div style="display:none"><img src="figures/navbg.jpg" alt=""><img src="figures/navline.jpg" alt=""></div>
      </div>
      <div class="section" lang="en">
         <div class="titlepage">
            <div>
               <div>
                  <h2 class="title" style="clear: both"><a name="about_yFiles"></a>About yFiles
                  </h2>
               </div>
            </div>
            <div></div>
         </div>
         <p>
            yFiles is a Java class library that provides algorithms and components for 
            analyzing, viewing, and drawing graphs, diagrams, and networks. 
            Its intended use is for application developers that are working on projects 
            dealing with graph-related structures. 
            
         </p>
         <div class="note" style="margin-left: 0.5in; margin-right: 0.5in;">
            <h3 class="title">Note</h3>
         </div>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h3 class="title"><a name="yFiles_YES"></a>What it is
                     </h3>
                  </div>
               </div>
               <div></div>
            </div>
            <p>
               One of the main aspects of yFiles is the provision of sophisticated layout 
               algorithms to support automatic generation of high-quality graph drawings. 
               
               Take, for instance, the graph depicted in <a href="about_yFiles.html#raw" title="Figure&nbsp;1.2.&nbsp;Initial setup of some data">Figure&nbsp;1.2, &#8220;Initial setup of some data&#8221;</a>. 
               It shows a "raw" version of a graph where it seems that the nodes have been 
               placed randomly on the plane. 
               Suppose this "raw" graph represents an initial setup of some data. 
               
            </p>
            <div class="figure"><a name="raw"></a><p class="title"><b>Figure&nbsp;1.2.&nbsp;Initial setup of some data</b></p>
               <div class="informaltable">
                  <center>
                     <table border="0">
                        <colgroup>
                           <col width="100%">
                        </colgroup>
                        <tbody>
                           <tr>
                              <td>
                                 <div class="mediaobject" align="center"><img src="figures/raw.jpg" align="middle" alt="Initial setup of some data."></div>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
            <p>
               By using one of yFiles's layout algorithms the "raw" graph will be transformed 
               into the arrangement shown in <a href="about_yFiles.html#polished" title="Figure&nbsp;1.3.&nbsp;Automatically computed &#34;polished&#34; drawing">Figure&nbsp;1.3, &#8220;Automatically computed "polished" drawing&#8221;</a>. 
               Note that the actual process of computing the "polished" drawing is completely 
               automatic. 
               
            </p>
            <div class="figure"><a name="polished"></a><p class="title"><b>Figure&nbsp;1.3.&nbsp;Automatically computed "polished" drawing</b></p>
               <div class="informaltable">
                  <center>
                     <table border="0">
                        <colgroup>
                           <col width="100%">
                        </colgroup>
                        <tbody>
                           <tr>
                              <td>
                                 <div class="mediaobject" align="center"><img src="figures/polished_2.jpg" align="middle" alt="Automatically computed &#34;polished&#34; drawing."></div>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
            <div class="note" style="margin-left: 0.5in; margin-right: 0.5in;">
               <h3 class="title">Note</h3>
               <p>
                  In a real-world scenario the initial setup could be even worse.
                  
                  For instance, when a graph is created from data retrieved from a database, any 
                  node created to represent an entity from a column would not have 
                  <span class="emphasis"><em>any</em></span> positional information associated. 
                  
                  Hence, the created nodes would all lie at the origin of the Cartesian 
                  coordinate system, i.e., they would "pile up." 
                  
               </p>
            </div>
         </div>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h3 class="title"><a name="yFiles_NO"></a>What it's not
                     </h3>
                  </div>
               </div>
               <div></div>
            </div>
            <p>
               Even for mathematically inclined people the term 
               <span class="emphasis"><em>graph drawing</em></span> can be misleading as to what it exactly 
               should mean. 
               Also, the term <span class="emphasis"><em>visualization</em></span> is another popular occasion 
               for a misunderstanding of the purposes the yFiles library serves. 
               
            </p>
            <p>
               <a href="about_yFiles.html#not_ex" title="Figure&nbsp;1.4.&nbsp;Other forms of diagrams">Figure&nbsp;1.4, &#8220;Other forms of diagrams&#8221;</a> shows some examples of other forms of graphs, or more 
               generally diagrams, that yFiles does not support. 
               
               In particular, the yFiles graph visualization library does not provide any 
               functionality to draw pie charts, function plots, or anything the like. 
               
            </p>
            <div class="figure"><a name="not_ex"></a><p class="title"><b>Figure&nbsp;1.4.&nbsp;Other forms of diagrams</b></p>
               <div class="informaltable">
                  <center>
                     <table border="0">
                        <colgroup>
                           <col width="38%">
                           <col width="10%">
                           <col width="52%">
                        </colgroup>
                        <tbody>
                           <tr valign="middle">
                              <td valign="middle">
                                 <div class="mediaobject" align="center"><img src="figures/pie-chart-2a.jpg" align="middle" alt="A pie-chart."></div>
                              </td>
                              <td valign="middle">&nbsp;</td>
                              <td valign="middle">
                                 <div class="mediaobject" align="center"><img src="figures/function-plot-2a.jpg" align="middle" alt="A function plot."></div>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
         </div>
      </div>
      <table class="copyright" border="0" cellpadding="0" cellspacing="0" width="100%">
         <tbody>
            <tr>
               <td align="right">
                  <p class="copyright">Copyright &copy;2004-2015, yWorks GmbH. All rights reserved.</p>
               </td>
            </tr>
         </tbody>
      </table>
      <div class="navfooter">
         <div class="navline2"></div>
         <div style="display:none"><img src="figures/navline2.jpg" alt=""><img src="figures/navbg2.jpg" alt=""></div>
         <table width="100%" summary="Navigation footer">
            <tr>
               <td width="40%" align="left"><a accesskey="p" href="tasks.html">Prev</a>&nbsp;
               </td>
               <td width="20%" align="center"><a accesskey="u" href="preface.html">Up</a></td>
               <td width="40%" align="right">&nbsp;<a accesskey="n" href="application_domains.html">Next</a></td>
            </tr>
            <tr>
               <td width="40%" align="left" valign="top">Graph-related Problems&nbsp;</td>
               <td width="20%" align="center"><a accesskey="h" href="index.html" target="_top">Home</a></td>
               <td width="40%" align="right" valign="top">&nbsp;Application Domains</td>
            </tr>
         </table>
      </div>
   </body>
</html>