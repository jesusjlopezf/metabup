/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches.importers.dia;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import metabup.sketches.InvalidFormat;
import metabup.sketches.Sketch;
import metabup.sketches.SketchElement;
import metabup.sketches.SketchFactory;
import metabup.sketches.XMLBasedImporter;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DiaImporter extends XMLBasedImporter {
	public Sketch read(InputStream stream, String sketchName)
			throws SAXException, IOException, ParserConfigurationException {

		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(stream);

		Sketch sketch = SketchFactory.eINSTANCE.createSketch();
		sketch.setName(sketchName);

		NodeList layers = doc.getElementsByTagName("dia:layer");
		for (int i = 0; i < layers.getLength(); i++) {
			readLayerContents(layers.item(i), sketch);
		}

		setConnections(sketch);

		return sketch;
	}

	protected String extractRealType(String type) {
		String[] parts = type.split("-");
		if (parts.length > 0)
			return parts[parts.length - 1].trim();
		return type;
	}

	void setConnections(Sketch sketch) {
		Collection<ObjectKind> values = id2element.values();
		for (ObjectKind wrapper: values) {
			wrapper.setConnections();
		}
	}
	
	void readLayerContents(Node item, Sketch sketch) {
        for(int i = 0; i < item.getChildNodes().getLength(); i++) {
        	Node object = item.getChildNodes().item(i);
        	
        	if ( object.getNodeName().equals("dia:object") ) {
        		String type     = object.getAttributes().getNamedItem("type").getNodeValue();
        		ObjectKind kind = decideType(object, type);
        		if ( kind == null ) continue;
        		
        		SketchElement element = kind.getElement();
        		element.setSkechTypeInfo(extractRealType(type));
        		element.setToolSymbolName(type);
        		element.setDebugName(type);
        		
        		String text = extractText(object);
        		if ( text != null && ! text.trim().isEmpty() ) {
        			element.setText(text);
        		}
        		
        		sketch.getElements().add(element);
        		
        		String id = object.getAttributes().getNamedItem("id").getNodeValue();
        		assert(id != null);
        		id2element.put(id, kind);
        		// node2kind.put(object, kind);
        	}
        }
	}
	
	private String extractText(Node node) {
		Node attNode  = filterChildrenWithAttribute1(node, "dia:attribute", "name", "text");
		if ( attNode == null ) return null;
		Node compNode = filterChildrenWithAttribute1(attNode, "dia:composite", "type", "text");
		if ( compNode == null ) return null;
		Node strNode  = filterChildrenWithAttribute1(compNode, "dia:attribute", "name", "string");
		if ( strNode == null ) return null;
		String text = filterChildren1(strNode, "dia:string").getTextContent();
		
		return text.replaceAll("#", "");
	}

	class DiaObject extends OBJECT {
		public DiaObject(Node obj) {
			super(obj);
		}		
	}

	class DiaContainer extends CONTAINER {
		public DiaContainer(Node obj) {
			super(obj);
		}		
	}

	class DiaConnection extends CONNECTION {
		public DiaConnection(Node obj) {
			super(obj);
		}

		protected List<Node> findConnections() {
			// String id =
			// node.getAttributes().getNamedItem("id").getNodeValue();
			Node connection = filterChildren1(node, "dia:connections");
						
			List<Node> conns = filterChildren(connection, "dia:connection");
			if ( conns.size() != 2 ) {
				throw new InvalidFormat("Node " + node + " with " + conns.size() + " " + connections);
			}			
			return conns;
		}
		
		@Override
		protected String getStartId() {
			List<Node> conns = findConnections();
			String startId = conns.get(0).getAttributes().getNamedItem("to").getNodeValue();
			return startId;
		}

		@Override
		protected String getEndId() {
			List<Node> conns = findConnections();
			String endId   = conns.get(1).getAttributes().getNamedItem("to").getNodeValue();
			return endId;
		}
		
	}
	
	
	// TODO: Parameterize this with an external model(s)
	private static String[] connections = { "Standard - Line" };
	private static String[] objects     = { 
		"UML - Actor",
		"Istar - other",
		"Flowchart - Document",
		"Misc - Analog Clock",
		"Network - Hub",
		"Cisco - CSU/DSU",
		"Cisco - Channelized Pipe",
		"Cisco - Centri Firewall",
		"Flowchart - Box",
		"Cisco - Androgynous Person",
		"Cisco - Running man",		
		"Cisco - Man red",		
		"Cisco - Medium Building blue",		
		"Jackson - domain",
		"Flowchart - Ellipse"
		};
	private static String[] containers =  { "UML - LargePackage" };

	private static String[] filter =  { "Standard - Text", "UML - Note" };

	private ObjectKind decideType(Node obj, String type) {
		if ( Arrays.asList(connections).contains(type) ) return new DiaConnection(obj);
		if ( Arrays.asList(objects).contains(type) ) return new DiaObject(obj);		
		if ( Arrays.asList(containers).contains(type) ) return new DiaContainer(obj);		
		if ( Arrays.asList(filter).contains(type) ) return null;		
		
		throw new RuntimeException("Unsupported object type " + type);
	}
	

	// XML Utilities


}
