/**
 */
package graphicProperties.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import graphicProperties.GraphicProperties;
import graphicProperties.GraphicPropertiesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Graphic Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class GraphicPropertiesImpl extends MinimalEObjectImpl.Container implements GraphicProperties {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GraphicPropertiesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GraphicPropertiesPackage.Literals.GRAPHIC_PROPERTIES;
	}

} //GraphicPropertiesImpl
