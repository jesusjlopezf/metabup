/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fragments.Annotation;
import fragments.AnnotationParam;
import fragments.Attribute;
import fragments.BooleanValue;
import fragments.Fragment;
import fragments.FragmentModel;
import fragments.FragmentType;
import fragments.FragmentsFactory;
import fragments.FragmentsPackage;
import fragments.IntegerValue;
import fragments.ObjectValueAnnotationParam;
import fragments.PrimitiveValueAnnotationParam;
import fragments.Reference;
import fragments.StringValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FragmentsFactoryImpl extends EFactoryImpl implements FragmentsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FragmentsFactory init() {
		try {
			FragmentsFactory theFragmentsFactory = (FragmentsFactory)EPackage.Registry.INSTANCE.getEFactory(FragmentsPackage.eNS_URI);
			if (theFragmentsFactory != null) {
				return theFragmentsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FragmentsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FragmentsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FragmentsPackage.FRAGMENT_MODEL: return createFragmentModel();
			case FragmentsPackage.FRAGMENT: return createFragment();
			case FragmentsPackage.OBJECT: return createObject();
			case FragmentsPackage.ATTRIBUTE: return createAttribute();
			case FragmentsPackage.STRING_VALUE: return createStringValue();
			case FragmentsPackage.INTEGER_VALUE: return createIntegerValue();
			case FragmentsPackage.BOOLEAN_VALUE: return createBooleanValue();
			case FragmentsPackage.REFERENCE: return createReference();
			case FragmentsPackage.ANNOTATION: return createAnnotation();
			case FragmentsPackage.ANNOTATION_PARAM: return createAnnotationParam();
			case FragmentsPackage.PRIMITIVE_VALUE_ANNOTATION_PARAM: return createPrimitiveValueAnnotationParam();
			case FragmentsPackage.OBJECT_VALUE_ANNOTATION_PARAM: return createObjectValueAnnotationParam();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case FragmentsPackage.FRAGMENT_TYPE:
				return createFragmentTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case FragmentsPackage.FRAGMENT_TYPE:
				return convertFragmentTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FragmentModel createFragmentModel() {
		FragmentModelImpl fragmentModel = new FragmentModelImpl();
		return fragmentModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fragment createFragment() {
		FragmentImpl fragment = new FragmentImpl();
		return fragment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fragments.Object createObject() {
		ObjectImpl object = new ObjectImpl();
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute createAttribute() {
		AttributeImpl attribute = new AttributeImpl();
		return attribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringValue createStringValue() {
		StringValueImpl stringValue = new StringValueImpl();
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntegerValue createIntegerValue() {
		IntegerValueImpl integerValue = new IntegerValueImpl();
		return integerValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanValue createBooleanValue() {
		BooleanValueImpl booleanValue = new BooleanValueImpl();
		return booleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference createReference() {
		ReferenceImpl reference = new ReferenceImpl();
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation createAnnotation() {
		AnnotationImpl annotation = new AnnotationImpl();
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationParam createAnnotationParam() {
		AnnotationParamImpl annotationParam = new AnnotationParamImpl();
		return annotationParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveValueAnnotationParam createPrimitiveValueAnnotationParam() {
		PrimitiveValueAnnotationParamImpl primitiveValueAnnotationParam = new PrimitiveValueAnnotationParamImpl();
		return primitiveValueAnnotationParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectValueAnnotationParam createObjectValueAnnotationParam() {
		ObjectValueAnnotationParamImpl objectValueAnnotationParam = new ObjectValueAnnotationParamImpl();
		return objectValueAnnotationParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FragmentType createFragmentTypeFromString(EDataType eDataType, String initialValue) {
		FragmentType result = FragmentType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFragmentTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FragmentsPackage getFragmentsPackage() {
		return (FragmentsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FragmentsPackage getPackage() {
		return FragmentsPackage.eINSTANCE;
	}

} //FragmentsFactoryImpl
