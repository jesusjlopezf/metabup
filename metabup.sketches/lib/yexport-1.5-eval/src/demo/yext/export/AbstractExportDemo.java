/****************************************************************************
 * This demo file is part of the yFiles extension package yExport-1.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * yExport demo files exhibit yExport functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a yExport version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own yExport powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the yExport
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.export;

import y.io.GraphMLIOHandler;
import y.option.OptionHandler;
import y.option.OptionGroup;
import y.option.ConstraintManager;
import y.view.AreaZoomMode;
import y.view.Arrow;
import y.view.EditMode;
import y.view.Graph2D;
import y.view.Graph2DView;
import y.io.IOHandler;
import y.io.SuffixFileFilter;
import y.util.D;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.UIManager;

import y.view.ViewMode;
import y.view.YRenderingHints;
import y.view.hierarchy.HierarchyManager;
import yext.export.io.OutputHandler;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.Iterator;

/**
 * Provides a generic export action that is used in all the file format specific
 * export demos.
 * For the most part the code given here handles user interaction such as
 * setting up the view port and specifying a destination file for the graph
 * export.
 * The actual write process is handled by the last four lines of code in
 * {@link demo.yext.export.AbstractExportDemo.ExportAction#export(y.io.IOHandler)}!
 *
 */
public abstract class AbstractExportDemo extends ViewActionDemo
{
  static final String GRAPH = "Graph";
  static final String USE_CUSTOM_HEIGHT = "Use Custom Height";
  static final String USE_CUSTOM_WIDTH = "Use Custom Width";
  static final String VIEW = "View";
  static final String SIZE = "Size";
  static final String CUSTOM_WIDTH = "Custom Width";
  static final String CUSTOM_HEIGHT = "Custom Height";
  static final String EMPTY_BORDER_WIDTH = "Empty Border Width";
  static final String CLIP_REGION = "Clip Region";
  static final String SELECTION_PAINTING = "Paint Selection Marker";

  protected AbstractExportDemo()
  {
    // the grouping.graphml sample below requires hierarchy support
    new HierarchyManager(view.getGraph2D());

    Color color = UIManager.getColor("Panel.background");
    color = color != null ? color.darker() : Color.GRAY;
    view.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, color));

    view.getGraph2D().getDefaultEdgeRealizer().setTargetArrow(Arrow.STANDARD);

    final JPanel statusBar = createStatusBar();
    add(statusBar, BorderLayout.SOUTH);
  }

  /**
   * Creates a status bar for this demo which shows the current zoom level.
   */
  protected JPanel createStatusBar()
  {
    final NumberFormat nf = NumberFormat.getPercentInstance();
    nf.setMaximumFractionDigits(0);

    final JLabel label = new JLabel("Zoom Factor");
    final Font font = label.getFont().deriveFont(Font.PLAIN);
    label.setFont(font);
    final JLabel zoom = new JLabel();
    zoom.setFont(font);

    view.getCanvasComponent()
        .addPropertyChangeListener("Zoom", new PropertyChangeListener()
        {
          public void propertyChange(final PropertyChangeEvent e)
          {
            zoom.setText(nf.format(e.getNewValue()));
          }
        });
    zoom.setText(nf.format(view.getZoom()));

    final JPanel statusBar = new JPanel(new FlowLayout(FlowLayout.LEADING, 2, 2));
    statusBar.add(label);
    statusBar.add(zoom);

    return statusBar;
  }

  /**
   * Creates a toolbar for this demo.
   * @return the application toolbar.
   */
  protected JToolBar createToolBar()
  {
    JToolBar bar = new JToolBar();
    bar.add(new ClearGraphAction());
    bar.add(new DeleteSelection());
    bar.add(new Zoom(1.2));
    bar.add(new Zoom(0.8));
    bar.add(new ResetZoom());
    bar.add(new FitContent());
    bar.add(new ZoomArea());

    return bar;
  }

  /**
   * Create a menu bar for this demo.
   * @return the application menu bar.
   */
  protected JMenuBar createMenuBar()
  {
    JMenuBar bar = super.createMenuBar();

    JMenu menu = createSampleGraphMenu();
    if (menu != null) {
      bar.add(menu);
    }

    return bar;
  }

  /**
   * Creates a menu for choosing sample graphs.
   */
  protected JMenu createSampleGraphMenu()
  {
    String[] resources = {
        "resource/ygraph/problemsolving.graphml",
        "resource/ygraph/grouping.graphml",
        "resource/ygraph/visual_edges_fight2.graphml",
        "resource/ygraph/visual_nodes_yedxp.graphml",
        "resource/ygraph/visual_features.graphml",
    };
    return createSampleGraphMenu(resources);
  }

  /**
   * Creates a menu for choosing the specified sample graphs.
   */
  JMenu createSampleGraphMenu(final String[] resources)
  {
    JMenu menu = new JMenu("Sample Graphs");
    int resolvedResourceCount = 0;
    for (int i = 0; i < resources.length; ++i) {
      final URL resource = getClass().getResource(resources[i]);
      if (resource != null) {
        ++resolvedResourceCount;
        menu.add(new LoadResourceAction(resource));
      }
      else {
        System.err.println("Could not resolve sample resource " + resources[i]);
      }
    }
    if (resolvedResourceCount > 0) {
      return menu;
    }
    else {
      return null;
    }
  }

  /**
   * Loads a graph from a specified GraphML file.
   * @param resource the GraphML file to load.
   * @param updateView whether or not the view is updated after loading.
   */
  protected void loadGraph(final URL resource, final boolean updateView)
  {
    GraphMLIOHandler ioh = createGraphMLIOHandler();
    try {
      view.getGraph2D().clear();
      ioh.read(view.getGraph2D(), resource);
    } catch (IOException ioe) {
      D.show(ioe);
    }

    if (updateView) {
      view.fitContent();
      view.getGraph2D().updateViews();
    }
  }

  /**
   * Creates an icon from the specified <code>resource</code>.
   */
  static ImageIcon createIcon(final String resource)
  {
    final URL url = AbstractExportDemo.class.getResource(resource);
    if (url != null) {
      return new ImageIcon(url);
    }
    else {
      System.err.println("Could not resolve resource " + resource);
      return null;
    }
  }


  /**
   * Action that clears the graph to present an empty view.
   */
  class ClearGraphAction extends AbstractAction
  {
    ClearGraphAction()
    {
      super("Clear Graph");
      this.putValue(Action.SHORT_DESCRIPTION, "Clear Graph");
      URL imageURL = getClass().getResource("resource/New16.gif");
      if (imageURL != null) {
        this.putValue(Action.SMALL_ICON, new ImageIcon(imageURL));
      }
    }

    public void actionPerformed(ActionEvent e)
    {
      view.getGraph2D().clear();
      view.updateView();
    }
  }

  /**
   * Action that zooms the view to the selected box.
   */
  class ZoomArea extends AbstractAction
  {
    ZoomArea()
    {
      super("Zoom Area");
      this.putValue(Action.SHORT_DESCRIPTION, "Zoom Area");
      URL imageURL = getClass().getResource("resource/ZoomArea16.gif");
      if (imageURL != null) {
        this.putValue(Action.SMALL_ICON, new ImageIcon(imageURL));
      }
    }

    public void actionPerformed(ActionEvent e)
    {
      Iterator viewModes = view.getViewModes();
      while (viewModes.hasNext()) {
        ViewMode viewMode = (ViewMode) viewModes.next();
        if (viewMode instanceof EditMode) {
          EditMode editMode = (EditMode) viewMode;
          editMode.setChild(new AreaZoomMode(), null, null);
        }
      }
    }
  }

  /**
   * Action that loads a graph from a specified file resource in GraphML format.
   */
  class LoadResourceAction extends AbstractAction
  {
    URL resource;

    LoadResourceAction(URL resource)
    {
      this.resource = resource;
      String file = resource.getFile();
      putValue(Action.NAME, file.substring(file.lastIndexOf('/') + 1));
    }

    public void actionPerformed(ActionEvent ev)
    {
      loadGraph(resource, true);
    }
  }

  /**
   * Action that exports the given graph to a specific output format.
   */
  class ExportAction extends AbstractAction
  {
    OptionHandler options;
    OutputHandler outputHandler;

    ExportAction( String name, OutputHandler outputHandler )
    {
      putValue(Action.NAME, name);
      putValue(Action.SMALL_ICON, createIcon("resource/Export16.gif"));
      putValue(Action.SHORT_DESCRIPTION, name);


      this.outputHandler = outputHandler;


      final String[] sizes = {
              "Use Original Size", USE_CUSTOM_WIDTH, USE_CUSTOM_HEIGHT
      };
      final String[] clipRegions = {
              GRAPH, VIEW
      };

      // set up an OptionHandler to allow for user customizations
      options = new OptionHandler(name);
      final OptionGroup group = new OptionGroup();
      group.setAttribute(OptionGroup.ATTRIBUTE_TITLE, name + " Options");
      group.addItem(options.addEnum(SIZE, sizes, 0));
      group.addItem(options.addInt(CUSTOM_WIDTH,  500));
      group.addItem(options.addInt(CUSTOM_HEIGHT, 500));
      group.addItem(options.addInt(EMPTY_BORDER_WIDTH, 10));
      group.addItem(options.addEnum(CLIP_REGION, clipRegions, 0));
      group.addItem(options.addBool(SELECTION_PAINTING, false));

      final ConstraintManager cm = new ConstraintManager(options);
      cm.setEnabledOnValueEquals(SIZE, USE_CUSTOM_WIDTH, CUSTOM_WIDTH);
      cm.setEnabledOnValueEquals(SIZE, USE_CUSTOM_HEIGHT, CUSTOM_HEIGHT);
      cm.setEnabledOnValueEquals(CLIP_REGION, GRAPH, EMPTY_BORDER_WIDTH);
    }

    public void actionPerformed( final ActionEvent e )
    {
      // show our OptionHandler to allow the user to set up the export view
      // according to his or her wishes
      if (!options.showEditor(view.getFrame())) {
        return;
      }

      if (options.getBool(SELECTION_PAINTING)) {
        outputHandler.removeRenderingHint(YRenderingHints.KEY_SELECTION_PAINTING);
      } else {
        outputHandler.addRenderingHint(
                YRenderingHints.KEY_SELECTION_PAINTING,
                YRenderingHints.VALUE_SELECTION_PAINTING_OFF);
      }

      Graph2D graph = view.getGraph2D();
      Graph2DView viewPort = outputHandler.createDefaultGraph2DView(graph);

      // configure the export view according to the options specified by the
      // user
      configureViewPort(viewPort);
      graph.setCurrentView(viewPort);

      // ask for the destination file and finally write the graph to the chosen
      // destination
      export(outputHandler);

      graph.removeView(viewPort);
    }

    /**
     * Configures the view port used for writing according to user
     * customizations.
     */
    void configureViewPort( final Graph2DView viewPort )
    {
      Graph2D graph = view.getGraph2D();

      double width  = options.getInt(CUSTOM_WIDTH);
      double height = options.getInt(CUSTOM_HEIGHT);
      Point viewPoint = viewPort.getViewPoint();
      double zoom = 1.0;

      if (GRAPH.equals(options.get(CLIP_REGION))) {
        Rectangle box = graph.getBoundingBox();
        int border = options.getInt(EMPTY_BORDER_WIDTH);
        box.width  += 2*border;
        box.height += 2*border;
        box.x -= border;
        box.y -= border;

        if (USE_CUSTOM_HEIGHT.equals(options.get(SIZE))) {
          width = height*box.getWidth()/box.getHeight();
        } else if (USE_CUSTOM_WIDTH.equals(options.get(SIZE))) {
          height = width*box.getHeight()/box.getWidth();
        } else {
          width =  box.getWidth();
          height = box.getHeight();
        }
        zoom = width/box.getWidth();
        viewPoint = new Point(box.x, box.y);
      } else if (VIEW.equals(options.get(CLIP_REGION))) {
        if (USE_CUSTOM_HEIGHT.equals(options.get(SIZE))) {
          width = height*view.getWidth()/view.getHeight();
        } else if (USE_CUSTOM_WIDTH.equals(options.get(SIZE))) {
          height = width*view.getHeight()/view.getWidth();
        } else {
          width =  view.getWidth();
          height = view.getHeight();
        }
        viewPoint = view.getViewPoint();
        zoom = view.getZoom()*width/view.getWidth();
      }

      viewPort.setZoom(zoom);
      viewPort.setSize((int)width, (int)height);
      viewPort.setViewPoint(viewPoint.x, viewPoint.y);
    }

    /**
     * Asks for a destination file and writes the currently visible graph
     * to that destination.
     */
    void export( final IOHandler ioh)
    {
      // ask for a destination file
      JFileChooser chooser = new JFileChooser();
      chooser.setAcceptAllFileFilterUsed(false);
      String fne = ioh.getFileNameExtension();
      chooser.setFileFilter(new SuffixFileFilter(
        fne, ioh.getFileFormatString()));

      if (chooser.showSaveDialog(AbstractExportDemo.this) ==
          JFileChooser.APPROVE_OPTION) {
        String name = chooser.getSelectedFile().toString();
        if (!name.endsWith(fne)) {
          name = name + '.' + fne;
        }

        // the actual export
        // it's this simple with yExport (and yFiles in general)   :-D
        try {
          ioh.write(view.getGraph2D(), name);
        } catch(IOException ex) {
          D.show(ex);
        }
      }
    }
  }
}