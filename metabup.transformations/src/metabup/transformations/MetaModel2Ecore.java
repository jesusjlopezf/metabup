/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;

import metabup.annotations.IAnnotation;
import metabup.annotations.IAnnotationFactory;
import metabup.annotations.IConstraintAnnotation;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Annotation;
import metabup.metamodel.Attribute;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;
import metabup.transformations.models.WEcoreModel;

public class MetaModel2Ecore extends BaseTransformation {
	MetaModel mm;
	WEcoreModel ecore;
	protected IAnnotationFactory af;	
	protected HashMap<String, EClass> eclasses = new HashMap<String, EClass>();
	protected HashMap<String, EReference> erefs = new HashMap<String, EReference>();
	
	public MetaModel2Ecore(MetaModel mm, IAnnotationFactory af){
		this.mm = mm;
		this.af = af;
	}
	
	public WEcoreModel execute(WEcoreModel ecore, String name, String prefix, String uri){
		this.ecore = ecore;
		EPackage pkg = ecore.createPackage();
		pkg.setName(name);
		pkg.setNsPrefix(prefix);
		pkg.setNsURI(uri);				
		pkg.getEClassifiers().addAll( (Collection<? extends EClassifier>) transform(mm.getClasses()) );
		
		// set all the supers
		for(MetaClass mc : mm.getClasses()){
			EClass ec = (EClass) getTraceOf(mc);
			
			for(MetaClass superMC : mc.getSupers()){
				EClass ecSuper = (EClass) getTraceOf(superMC);
				ec.getESuperTypes().add(ecSuper);
			}
		}
		
		// in case there are pending targets to be assigned in references...
		for(Reference r : mm.getReferences()){
			String cName = ((MetaClass)r.eContainer()).getName();
			String rName = cName + "." + r.getName();
			if(erefs.get(rName) == null) continue;
			if(erefs.get(rName).getEType() == null) erefs.get(rName).setEType(eclasses.get(r.getReference().getName()));
		}
		
		return ecore;
	}
	
	public EClass transformMetaClass(final MetaClass m) {
		//System.out.println("Transforming " + m.getName());
		final EClass eclass = EcoreFactory.eINSTANCE.createEClass();
		eclass.setName( m.getName() );
		eclass.setAbstract(m.getIsAbstract());
		
		if(!m.getAnnotations().isEmpty()){
			addConstraints(eclass, m.getAnnotations());
			addAnnotations(eclass, m.getAnnotations());
		}
		
		setTrace(m, eclass);
		eclasses.put(m.getName(), eclass);		
		
		eclass.getEStructuralFeatures().addAll( (Collection<? extends EStructuralFeature>) transform(m.getFeatures()) );
		return eclass;
	}
	
	private void addAnnotations(ENamedElement ne, List<Annotation> anns){
		for(Annotation ann : anns){
			if(ann.getName().toLowerCase().equals("constraint") || ann.getName().toLowerCase().equals("composition")) continue;
			Class<? extends IAnnotation> annotationClass = af.getAnnotation(ann.getName().toLowerCase());
						
			if (annotationClass != null){				
				EAnnotation ea = ne.getEAnnotation(ann.getName());
						
				if(ea == null){
					ea = EcoreFactory.eINSTANCE.createEAnnotation();
					ea.setSource(ann.getName());
					ne.getEAnnotations().add(ea);
				}											
			}			
		}
	}
	
	private void addConstraints(ENamedElement ne, List<Annotation> anns){
		for(Annotation ann : anns){
			Class<? extends IConstraintAnnotation> annotationClass = af.getConstraintAnnotation(ann.getName());
			
			if (annotationClass != null){
				//System.out.println("MY ANNOTATION: " + annotationClass.getName());
				try {
					IConstraintAnnotation annotation = annotationClass.newInstance();
					
					if(annotation.getConstraints() != null){
						//System.out.println("reading constraints from: " + annotation.getName());
						EAnnotation ea = ne.getEAnnotation("OCL");
						
						if(ea == null){
							ea = EcoreFactory.eINSTANCE.createEAnnotation();
							ea.setSource("OCL");
						}
						
						Iterator it = annotation.getConstraints().entrySet().iterator();
						
						while (it.hasNext()) {
							Map.Entry e = (Map.Entry)it.next();
							ea.getDetails().put((String)e.getKey(), (String)e.getValue());
						}
						
						ne.getEAnnotations().add(ea);
					}
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
	}
	
	public EAttribute transformAttribute(final Attribute a) {
		EAttribute eattr = ecore.createEAtribute();
		eattr.setName( a.getName() );
		
		String type = a.getPrimitiveType();
		if ( type.startsWith("String") ) 
			eattr.setEType(EcorePackage.eINSTANCE.getEString());
		else if ( type.startsWith("Int") ) 
			eattr.setEType(EcorePackage.eINSTANCE.getEInt());
		else if ( type.startsWith("Boolean") ) 
			eattr.setEType(EcorePackage.eINSTANCE.getEBoolean());
		
		if(!a.getAnnotations().isEmpty()){
			addConstraints(eattr, a.getAnnotations());
			addAnnotations(eattr, a.getAnnotations());
		}
		
		setTrace(eattr, a);
		
		return eattr;
	}

	public EReference transformReference(final Reference r) {
		final EReference eref = ecore.createEReference();
		eref.setName( r.getName() );
		
		/* domain annotations appliance */
		if(r.isAnnotated("composition")) eref.setContainment(true);
		
		registerContinuationForEquivalent(r.getReference(), new AContinuation<EClass>(EClass.class) {
			@Override
			public void execute(EClass data) {
				eref.setEType(data);
			}						
		});
		
		if(!r.getAnnotations().isEmpty()){
			addConstraints(eref, r.getAnnotations());
			addAnnotations(eref, r.getAnnotations());		
		}
		
		eref.setLowerBound(r.getMin());
		eref.setUpperBound(r.getMax());
		
		setTrace(eref, r);
		this.erefs.put(((MetaClass)r.eContainer()).getName() + "." + r.getName(), eref);
		
		return eref;
	}
}
