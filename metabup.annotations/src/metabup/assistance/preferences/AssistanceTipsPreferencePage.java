/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.preferences;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.preference.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.IWorkbench;

import metabup.annotations.Activator;
import metabup.annotations.IAnnotation;
import metabup.annotations.general.AnnotationFactory;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.general.AssistanceTipsFactory;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class AssistanceTipsPreferencePage
	extends FieldEditorPreferencePage
	implements IWorkbenchPreferencePage {

	public AssistanceTipsPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Interactive-metamodeling assistent preferences");		
	}
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {		
		AssistanceTipsFactory af = new AssistanceTipsFactory();
		af.initializeFactory();
		createSpace();
		
		for(String at : af.listAllAssistanceTipNames()){
			Class<? extends IAssistanceTip> tipClass = af.getAssistanceTip(at);
			try {
				IAssistanceTip annotation = tipClass.newInstance();
				HashMap<String, Object> preferences = annotation.getPreferences();
				
				if(preferences != null && !preferences.isEmpty()){
					Label label= new Label(getFieldEditorParent(), SWT.BOLD);
					GridData gd= new GridData(GridData.HORIZONTAL_ALIGN_FILL);
					gd.horizontalSpan= 2;
					label.setLayoutData(gd);
					label.setText(annotation.getName());					
					Iterator it = preferences.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry e = (Map.Entry)it.next();
						if(e.getValue() instanceof String ) 
							addField(new StringFieldEditor((String) e.getKey(), (String) e.getKey(), getFieldEditorParent()));
						else if(e.getValue() instanceof Boolean )
							addField(new BooleanFieldEditor((String) e.getKey(), (String) e.getKey(), getFieldEditorParent()));
						else if(e.getValue() instanceof List<?> ){
							List<?> values = (List<?>)e.getValue();
							
							String strVals[][] = new String[values.size()][2];
							
							for(int i = 0; i<values.size(); i++){
								strVals[i][0] = values.get(i).toString();
								strVals[i][1] = values.get(i).toString();
							}
							
							addField(new RadioGroupFieldEditor((String) e.getKey(), (String) e.getKey(), 1, strVals, getFieldEditorParent()));
						}
							
						/* radio group (multi-option) preferences should be furtherly approached */
					}
												
					createSpace();
				}											
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}				
		}
		
		/*addField(new DirectoryFieldEditor(PreferenceConstants.P_PATH, 
				"&Directory preference:", getFieldEditorParent()));*/
		/*addField(
			new BooleanFieldEditor(
				PreferenceConstants.P_BOOLEAN,
				"&An example of a boolean preference",
				getFieldEditorParent()));*/

		/*addField(new RadioGroupFieldEditor(
				PreferenceConstants.P_CHOICE,
			"An example of a multiple-choice preference",
			1,
			new String[][] { { "&Choice 1", "choice1" }, {
				"C&hoice 2", "choice2" }
		}, getFieldEditorParent()));*/
		/*addField(
			new StringFieldEditor(PreferenceConstants.P_STRING, "A &text preference:", getFieldEditorParent()));*/
	}
	
	private void createSpace() {
		Label label= new Label(getFieldEditorParent(), SWT.NONE);
		GridData gd= new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gd.horizontalSpan= 3;
		label.setLayoutData(gd);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}