/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.general;

import org.eclipse.core.resources.IProject;

import metabup.annotations.general.Annotation;
import metabup.annotations.preferences.AnnotationsPluginPreferenceConstants;

public abstract class AnnotationHandler {
	public final static Annotation getAnnotation(IProject project, String name){		
		//rellenar. devuelve una anotación dado un proyecto
		
		return null;
	}
	
	public final static StringBuffer generateInstanceCode(StringBuffer buffer, String name, String description){
		// it should be approached in a more sophisticated style (like with JDT and AST), but still... 
		buffer.append("package " + AnnotationsPluginPreferenceConstants.P_ANNOTATIONS_PACKAGE + ";");
		buffer.append("\n\n");
		buffer.append("import " + AnnotationsPluginPreferenceConstants.P_LIB_NAMES + ";");
		buffer.append("\n\n");
		buffer.append("public class " + name + " extends " + Annotation.class.getSimpleName() + " {");
		buffer.append("\n\n\t");
		buffer.append("public " + name + "(){");
		buffer.append("\n\t\t");
		buffer.append("setName(\"" + name + "\");");
		buffer.append("\n\t\t");
		buffer.append("setDescription(\"" + description + "\");");
		buffer.append("\n\t");
		buffer.append("}");
		buffer.append("\n\n\t");
		buffer.append("public boolean when(){");
		buffer.append("\n\t\t");
		buffer.append("// fill with the conditions the annotated element ought to fulfill for the annotation to trigger");
		buffer.append("\n\t\t");
		buffer.append("return false;");
		buffer.append("\n\t");
		buffer.append("}");
		buffer.append("\n\n\t");
		buffer.append("public void perform(){");
		buffer.append("\n\t\t");
		buffer.append("// fill with the annotation's action over the meta-model");
		buffer.append("\n\t");
		buffer.append("}");
		buffer.append("\n");
		buffer.append("}");
		return buffer;
	}
}
