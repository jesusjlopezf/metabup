/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.export.transformations;

import java.awt.Color;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.jesusjlopezf.utils.eclipse.emf.EMFUtils;
import com.jesusjlopezf.utils.image.ImageUtils;

import metabup.annotations.catalog.geometry.Adjacency;
import metabup.annotations.catalog.geometry.Containment;
import metabup.annotations.catalog.geometry.Overlapping;
import metabup.sketches.resources.LegendFolder;
import graphicProperties.ArrowType;
import graphicProperties.EdgeProperties;
import graphicProperties.GraphicProperties;
import graphicProperties.LineType;
import graphic_representation.AbstractElement;
import graphic_representation.AffixedCompartmentElement;
import graphic_representation.AllElements;
import graphic_representation.DefaultLayer;
import graphic_representation.GRUserColorDef;
import graphic_representation.GraphicRepresentation;
import graphic_representation.Graphic_representationFactory;
import graphic_representation.IconElement;
import graphic_representation.LabelEAttribute;
import graphic_representation.MMGraphic_Representation;
import graphic_representation.Node;
import graphic_representation.Node_Element;
import graphic_representation.PaletteDescription;
import graphic_representation.PaletteDescriptionLink;
import graphic_representation.RepresentationDD;
import graphic_representation.Root;


public class EPackage2GraphicRepresentation {
	private EPackage pkg;
	private String extension;
	private String ERROR_MESSAGE = null;
	private LegendFolder legendFolder = null;
	private HashMap <String, GraphicProperties> graphicProperties = new HashMap<String, GraphicProperties>();
	
	public EPackage2GraphicRepresentation(EPackage pkg, String extension, LegendFolder folder, HashMap<String, GraphicProperties> gProp){
		this.pkg = pkg;
		this.extension = extension;
		this.legendFolder = folder;
		this.graphicProperties = gProp;
	}
	
	public GraphicRepresentation execute(){
		GraphicRepresentation graphicRepresentation = Graphic_representationFactory.eINSTANCE.createGraphicRepresentation();
		graphicRepresentation.setName(pkg.getName());
		
		MMGraphic_Representation graphicModel = Graphic_representationFactory.eINSTANCE.createMMGraphic_Representation();
		graphicModel.setExtension(extension);

		RepresentationDD representation = Graphic_representationFactory.eINSTANCE.createRepresentationDD();
		
		////
		
		Root root = Graphic_representationFactory.eINSTANCE.createRoot();		
		
		int cont = 0;
		
		for(EClassifier ec : pkg.getEClassifiers()){
			if(ec instanceof EClass){
				if(ec.getEAnnotation(metabup.annotations.catalog.constraint.Root.NAME) != null){
					root.setAnEClass((EClass)ec);
					cont++;
				}
			}			
		}
		
		if(root.getAnEClass() == null){
			this.setErrorMessage("A root class couldn't be found.");
			System.out.println("A root class couldn't be found.");
			return null;
		}
		
		if(cont > 1){
			this.setErrorMessage(cont + " root classes found.");
			System.out.println(cont + " root classes found.");
			return null;
		}
				
		DefaultLayer layer = Graphic_representationFactory.eINSTANCE.createDefaultLayer();
		layer.setName("Default Layer");
		
		// node creation
		for(EClassifier ecr : pkg.getEClassifiers()){
			legendFolder.refresh();
			
			if(ecr instanceof EClass){				
				if(ecr.getEAnnotation(metabup.annotations.catalog.constraint.Root.NAME) != null) continue;
				EClass ec = (EClass)ecr;
				AllElements node;
				if(!ec.isAbstract()) node = Graphic_representationFactory.eINSTANCE.createNode();
				else node = Graphic_representationFactory.eINSTANCE.createAbstractElement();								
				
				if(!ec.isAbstract()){
					PaletteDescription desc = Graphic_representationFactory.eINSTANCE.createPaletteDescription();
					desc.setPalette_name(ec.getName());
					
					//desc.setIcon_filepath(legendFolder.getElementPath(ec.getName(), LegendFolder.SVG_EXTENSION));				
					
					IconElement icon = Graphic_representationFactory.eINSTANCE.createIconElement();				
					icon.setFilepath(legendFolder.getElementPath(ec.getName(), LegendFolder.SVG_EXTENSION));
					((graphic_representation.Node) node).setNode_shape(icon);				
					((graphic_representation.Node) node).setDiag_palette(desc);
					((graphic_representation.Node) node).setAnEClass(ec);
					System.out.println("! " + ec.getName() + " created as Node");
				}else{
					((graphic_representation.AbstractElement) node).setAnEClass(ec);
					System.out.println("! " + ec.getName() + " created as AbstractElement");
				}
								
				layer.getElements().add(node);				
			}
		}
		
		
		// set EClasses features
		for(EClassifier ecr : pkg.getEClassifiers()){
			if(!(ecr instanceof EClass)) continue;
			EClass ec = (EClass)ecr;
			
			if(!ec.isAbstract()){				
				Node node = (Node)this.getNodeByEClass(layer, ec);				
				if(node == null) continue;

				//calculate container - WHAT IF THER'R MANY?
				if(EMFUtils.getContainingReferences(ec) != null){
					for(EReference eref : EMFUtils.getContainingReferences(ec)){
						if(!node.getContainerReference().contains(eref))
							node.getContainerReference().add(eref);
					}
				}
				else System.out.println("containing reference not found");
				
				Node_Element ne = Graphic_representationFactory.eINSTANCE.createNode_Element();				
								
				// set class attributes				
				for(EAttribute ea : ec.getEAllAttributes()){
					LabelEAttribute lea = Graphic_representationFactory.eINSTANCE.createLabelEAttribute();
					lea.setAnEAttribute(ea);
					lea.setShowIcon(false);
					ne.getLabelanEAttribute().add(lea);
				}
				
				for(EReference r : ((EClass) ecr).getEReferences()){
					if(r.getEAnnotation(Adjacency.NAME) == null && r.getEAnnotation(Overlapping.NAME) == null && r.getEAnnotation(Containment.NAME) == null){						 
						PaletteDescriptionLink link = Graphic_representationFactory.eINSTANCE.createPaletteDescriptionLink();
						link.setAnEReference(r);
						link.setPalette_name(r.getName());
						ne.getLinkPalette().add(link);
												
						List<EClass> subs = EMFUtils.getEClassSubs(r.getEReferenceType(), true);
						
						if(subs != null && !subs.isEmpty()){
							for(EClass sub : subs){
								Node tarNode = (Node) this.getNodeByEClass(layer, sub);
								if(tarNode != null) link.getAnDiagramElement().add(tarNode);
								else System.out.println("the reference target couldn't be assined");
							}
						}
						
						if(!r.getEReferenceType().isAbstract()){
							Node tarNode = (Node) this.getNodeByEClass(layer, r.getEReferenceType());
							if(tarNode != null) link.getAnDiagramElement().add(tarNode);
						}
						
						GraphicProperties prop = graphicProperties.get(ec.getName() + "." + r.getName());
						
						if(prop != null && prop instanceof EdgeProperties){
							EdgeProperties eProp = (EdgeProperties)prop;
							link.setDecoratorName(metaBup2SiriusDecorator(eProp.getTgtArrow()));
							link.setSourceDecoratorName(metaBup2SiriusDecorator(eProp.getSrcArrow()));
							link.setLineStyle(metaBup2SiriusLineType(eProp.getLineType()));
							link.setLineWidth(Integer.toString(eProp.getWidth()));							
							
							if(eProp.getColor() != null){
								Color color = ImageUtils.hex2Rgb(eProp.getColor());
								graphic_representation.RGBColor rgbColor = Graphic_representationFactory.eINSTANCE.createRGBColor();							
								rgbColor.setBlue(color.getBlue());
								rgbColor.setGreen(color.getGreen());
								rgbColor.setRed(color.getRed());
								rgbColor.setName(EcoreUtil.generateUUID());
																
								graphicRepresentation.getUserColors().add(rgbColor);
								GRUserColorDef rgb = Graphic_representationFactory.eINSTANCE.createGRUserColorDef();
								rgb.setColor(rgbColor);
								link.setColor(rgb);
							}								
						}

						
						// TODO:
						//link.setDecoratorName(null);
						//link.setIcon_filepath(arg0);
						//link.setPalette_name(arg0);						
					}else{
						AffixedCompartmentElement comp;
						if(r.getEAnnotation(Adjacency.NAME) == null) comp = Graphic_representationFactory.eINSTANCE.createCompartmentElement();
						else comp = Graphic_representationFactory.eINSTANCE.createAffixedElement();
						
						comp.setAnEReference(r);
						List<EClass> subs = EMFUtils.getEClassSubs(r.getEReferenceType(), true);
						
						if(subs != null && !subs.isEmpty()){
							for(EClass tarec : subs)
								comp.getNodes().add((Node)this.getNodeByEClass(layer, tarec));
						}
						
						if(!r.getEReferenceType().isAbstract())comp.getNodes().add((Node)this.getNodeByEClass(layer, r.getEReferenceType()));
						
						ne.getAffixedCompartmentElements().add(comp);
					}
				}
				
				node.setNode_elements(ne);
			}else{
				AbstractElement node = (AbstractElement)this.getNodeByEClass(layer, ec);				
				if(node == null) continue;
				
				Node_Element ne = Graphic_representationFactory.eINSTANCE.createNode_Element();				
				
				// set class attributes				
				for(EAttribute ea : ec.getEAllAttributes()){
					LabelEAttribute lea = Graphic_representationFactory.eINSTANCE.createLabelEAttribute();
					lea.setAnEAttribute(ea);
					lea.setShowIcon(false);
					ne.getLabelanEAttribute().add(lea);
				}
				
				for(EReference r : ((EClass) ecr).getEReferences()){
					if(r.getEAnnotation(Adjacency.NAME) == null && r.getEAnnotation(Overlapping.NAME) == null && r.getEAnnotation(Containment.NAME) == null){						 
						PaletteDescriptionLink link = Graphic_representationFactory.eINSTANCE.createPaletteDescriptionLink();
						link.setAnEReference(r);
						link.setPalette_name(r.getName());
						ne.getLinkPalette().add(link);
						
						List<EClass> subs = EMFUtils.getEClassSubs(r.getEReferenceType(), true);
						
						if(subs != null && !subs.isEmpty()){
							for(EClass sub : subs){
								Node tarNode = (Node) this.getNodeByEClass(layer, sub);
								if(tarNode != null) link.getAnDiagramElement().add(tarNode);
								else System.out.println("the reference target couldn't be assined");
							}
						}
						
						if(!r.getEReferenceType().isAbstract()){
							Node tarNode = (Node) this.getNodeByEClass(layer, r.getEReferenceType());
							if(tarNode != null) link.getAnDiagramElement().add(tarNode);
						}
						
						GraphicProperties prop = graphicProperties.get(ec.getName() + "." + r.getName());
						
						if(prop != null && prop instanceof EdgeProperties){
							EdgeProperties eProp = (EdgeProperties)prop;
							link.setDecoratorName(metaBup2SiriusDecorator(eProp.getTgtArrow()));
							link.setSourceDecoratorName(metaBup2SiriusDecorator(eProp.getSrcArrow()));
							link.setLineStyle(metaBup2SiriusLineType(eProp.getLineType()));
							link.setLineWidth(Integer.toString(eProp.getWidth()));							
							
							if(eProp.getColor() != null){
								Color color = ImageUtils.hex2Rgb(eProp.getColor());
								graphic_representation.RGBColor rgbColor = Graphic_representationFactory.eINSTANCE.createRGBColor();							
								rgbColor.setBlue(color.getBlue());
								rgbColor.setGreen(color.getGreen());
								rgbColor.setRed(color.getRed());
								rgbColor.setName(EcoreUtil.generateUUID());
																
								GRUserColorDef rgb = Graphic_representationFactory.eINSTANCE.createGRUserColorDef();
								rgb.setColor(rgbColor);
								link.setColor(rgb);
							}								
						}

						
						// TODO:
						//link.setDecoratorName(null);
						//link.setIcon_filepath(arg0);
						//link.setPalette_name(arg0);						
					}else{
						AffixedCompartmentElement comp;
						if(r.getEAnnotation(Adjacency.NAME) == null) comp = Graphic_representationFactory.eINSTANCE.createCompartmentElement();
						else comp = Graphic_representationFactory.eINSTANCE.createAffixedElement();
						
						comp.setAnEReference(r);
						for(EClass tarec : EMFUtils.getEClassSubs(r.getEReferenceType(), true)){
							comp.getNodes().add((Node)this.getNodeByEClass(layer, tarec));
						}
						
						ne.getAffixedCompartmentElements().add(comp);
					}
				}
				
				node.setNode_elements(ne);
			}
		}				
		
		root.setRootLayer(layer);
		representation.getLayers().add(layer);
		representation.setRoot(root);
		graphicModel.getListRepresentations().add(representation);
		graphicRepresentation.getAllGraphicRepresentation().add(graphicModel);
		
		return graphicRepresentation;
	}
	
	
	private String metaBup2SiriusDecorator(ArrowType type){
		switch(type){
			case NONE: return null;
			case CROWS_FOOT_MANY: return "OutputClosedArrow";
			case PLAIN: return "InputArrow";
			case WHITE_DELTA: return "InputClosedArrow";
			case DELTA: return "InputFillClosedArrow";
			case WHITE_DIAMOND: return "Diamond";
			case DIAMOND: return "FillDiamond";
			case STANDARD: return "InputFillClosedArrow";
			case SHORT: return "InputFillClosedArrow";
			
			default:
				this.setErrorMessage("Sirius doesn't support the type of edge decorator \'" + type.getLiteral() + "\'. A standard line will be placed instead.");
				return "InputFillClosedArrow";
		}
	}
	
	private String metaBup2SiriusLineType(LineType type){
		switch(type){
			case LINE: return "solid";
			case DASHED_DOTTED: return "dash_dot";
			case DOTTED: return "dot";
			
			default:
				this.setErrorMessage("Sirius doesn't support the line type \'" + type.getLiteral() + "\'. A solid line type will be placed instead.");
				return "solid";
		}
	}
	
	private AllElements getNodeByEClass(DefaultLayer layer, EClass ec){		
		for(AllElements de : layer.getElements()){
			if(de instanceof Node){
				if(((Node) de).getAnEClass().equals(ec)){
					System.out.println("this eclass: " + ec.getName() + " is Node");
					return (Node)de;
				}
			}else if(de instanceof AbstractElement){
				if(((AbstractElement) de).getAnEClass().equals(ec)){					
					System.out.println("this eclass: " + ec.getName() + " is AbstractElement");
					return (AbstractElement)de;
				}
			}
		}
		
		return null;
	}
	
	public String getErrorMessage(){
		return ERROR_MESSAGE;
	}
	
	private void setErrorMessage(String msg){
		this.ERROR_MESSAGE = msg;
	}
}
