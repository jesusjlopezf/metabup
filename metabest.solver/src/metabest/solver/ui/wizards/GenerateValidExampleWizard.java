/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards;

import metabest.solver.ConstraintSolver;
import metabest.solver.ui.wizards.pages.GenerateValidExampleWizardPage;
import metabest.transformations.Fragment2GraphML;
import metabup.metamodel.MetaModel;
import metabup.transformations.*;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.operation.*;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

import java.io.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import jdk.internal.org.xml.sax.SAXException;

import org.eclipse.ui.*;

import choco.ContradictionException;
import fragments.Fragment;

import com.google.inject.Injector;
import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;

public class GenerateValidExampleWizard extends Wizard implements INewWizard {
	private GenerateValidExampleWizardPage page;
	private ISelection selection;
	private IProject project;
	private MetaModel mm;

	/**
	 * Constructor for SampleNewWizard.
	 */
	public GenerateValidExampleWizard(IProject project, MetaModel mm) {
		super();
		setNeedsProgressMonitor(true);
		this.project = project;
		this.mm = mm;
	}
	
	/**
	 * Adding the page to the wizard.
	 */

	public void addPages() {
		page = new GenerateValidExampleWizardPage(selection, project);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in
	 * the wizard. We will create an operation and run it
	 * using wizard as execution context.
	 */
	public boolean performFinish() {
		final boolean generateMBF = page.generateMBF();
		final boolean generateGraphML = page.generateGraphML();
		final String legendFile = page.getLegendFile();
		final Fragment baseFragment = page.getBaseFragment();
	
		
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(project, monitor, generateMBF, generateGraphML, baseFragment, legendFile, mm);									
				} catch (CoreException e) {
					System.out.println("here1");
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			System.out.println("here2");
			return false;
		} catch (InvocationTargetException e) {
			/*System.out.println("here3");
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());*/
			return true;
		}
		return true;
	}
	
	/**
	 * The worker method. It will find the container, create the
	 * file if missing or just replace its contents, and open
	 * the editor on the newly created file.
	 */

	private void doFinish(IProject project, IProgressMonitor monitor, boolean generateMBF, boolean generateGraphML, Fragment baseFragment, String legendFile, MetaModel mm)
		throws CoreException {
		// create a sample file
		monitor.beginTask("Calculating example...", 4);
		
		ConstraintSolver cs;
		
		try {
			IFolder folder = ResourcesPlugin.getWorkspace().getRoot().getFolder(project.getLocation());
			cs = new ConstraintSolver(project, folder, mm, baseFragment);
			IFile file = cs.solve(false, null, true);
			
			monitor.worked(1);
			
			if(generateGraphML){
				
				/*final Injector injector = FragmentsActivator.getInstance().getInjector(FragmentsActivator.METABUP_FRAGMENTS);				
				XtextResourceSet resourceSet = (XtextResourceSet) injector.getInstance(XtextResourceSetProvider.class).get(file.getProject());
				resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
				Resource resource = resourceSet.getResource(URI.createURI(file.getLocationURI().toString()),true);			    	   
				TestModel tm = (TestModel)resource.getContents().get(0);
				Fragment newBaseFragment = tm.getTestableFragments().get(0); 

				LayoutSolver ls = new LayoutSolver(project, mm, newBaseFragment, new File(legendFile));
				IFile tempFile = ls.solve(false); // false de momento							
				System.out.println("out");*/
				
				Fragment2GraphML transformation = new Fragment2GraphML(IFileUtils.IFile2File(file), new File(legendFile), project, true);
				IFile graphFile = project.getFile(mm.getName().replaceFirst(".mbupf", "") + ".graphml");
				transformation.execute(IFileUtils.IFile2File(graphFile));	
			}
			
			monitor.worked(1);
			
			//if(!generateMBF) file.delete(true, monitor);
			
			monitor.worked(1);
			
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
			
			monitor.worked(1);
									
			if(file.exists()){
				// open in editor
			}
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (org.xml.sax.SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		monitor.worked(1);				
	}

	private void throwCoreException(String message) throws CoreException {
		IStatus status =
			new Status(IStatus.ERROR, "metabup.ui.editor", IStatus.OK, message, null);
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if
	 * we can initialize from it.
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}