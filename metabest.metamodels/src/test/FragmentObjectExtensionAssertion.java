/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test;

import test.extensions.FragmentObjectQualifier;
import test.extensions.FragmentObjectSelector;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fragment Object Extension Assertion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.FragmentObjectExtensionAssertion#getSelector <em>Selector</em>}</li>
 *   <li>{@link test.FragmentObjectExtensionAssertion#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @see test.TestPackage#getFragmentObjectExtensionAssertion()
 * @model
 * @generated
 */
public interface FragmentObjectExtensionAssertion extends ExtensionAssertion {
	/**
	 * Returns the value of the '<em><b>Selector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selector</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selector</em>' containment reference.
	 * @see #setSelector(FragmentObjectSelector)
	 * @see test.TestPackage#getFragmentObjectExtensionAssertion_Selector()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FragmentObjectSelector getSelector();

	/**
	 * Sets the value of the '{@link test.FragmentObjectExtensionAssertion#getSelector <em>Selector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selector</em>' containment reference.
	 * @see #getSelector()
	 * @generated
	 */
	void setSelector(FragmentObjectSelector value);

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(FragmentObjectQualifier)
	 * @see test.TestPackage#getFragmentObjectExtensionAssertion_Condition()
	 * @model containment="true"
	 * @generated
	 */
	FragmentObjectQualifier getCondition();

	/**
	 * Sets the value of the '{@link test.FragmentObjectExtensionAssertion#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(FragmentObjectQualifier value);

} // FragmentObjectExtensionAssertion
