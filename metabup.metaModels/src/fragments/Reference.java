/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import metabup.metamodel.MetaClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fragments.Reference#getReference <em>Reference</em>}</li>
 *   <li>{@link fragments.Reference#getGraphicProperties <em>Graphic Properties</em>}</li>
 * </ul>
 *
 * @see fragments.FragmentsPackage#getReference()
 * @model
 * @generated
 */
public interface Reference extends Feature {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference list.
	 * The list contents are of type {@link fragments.Object}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' reference list.
	 * @see fragments.FragmentsPackage#getReference_Reference()
	 * @model required="true"
	 * @generated
	 */
	EList<fragments.Object> getReference();
	/**
	 * Returns the value of the '<em><b>Graphic Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graphic Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graphic Properties</em>' containment reference.
	 * @see #setGraphicProperties(graphicProperties.EdgeProperties)
	 * @see fragments.FragmentsPackage#getReference_GraphicProperties()
	 * @model containment="true"
	 * @generated
	 */
	graphicProperties.EdgeProperties getGraphicProperties();
	/**
	 * Sets the value of the '{@link fragments.Reference#getGraphicProperties <em>Graphic Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graphic Properties</em>' containment reference.
	 * @see #getGraphicProperties()
	 * @generated
	 */
	void setGraphicProperties(graphicProperties.EdgeProperties value);
	List<metabup.metamodel.Reference> getAnalogousReferences(MetaClass src);

} // Reference
