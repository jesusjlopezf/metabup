/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package History;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link History.User#getVotes <em>Votes</em>}</li>
 *   <li>{@link History.User#getCollaborations <em>Collaborations</em>}</li>
 * </ul>
 * </p>
 *
 * @see History.HistoryPackage#getUser()
 * @model
 * @generated
 */
public interface User extends IdElement {
	/**
	 * Returns the value of the '<em><b>Votes</b></em>' reference list.
	 * The list contents are of type {@link History.Vote}.
	 * It is bidirectional and its opposite is '{@link History.Vote#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Votes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Votes</em>' reference list.
	 * @see History.HistoryPackage#getUser_Votes()
	 * @see History.Vote#getUser
	 * @model opposite="user"
	 * @generated
	 */
	EList<Vote> getVotes();

	/**
	 * Returns the value of the '<em><b>Collaborations</b></em>' reference list.
	 * The list contents are of type {@link History.Collaboration}.
	 * It is bidirectional and its opposite is '{@link History.Collaboration#getProposedBy <em>Proposed By</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collaborations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collaborations</em>' reference list.
	 * @see History.HistoryPackage#getUser_Collaborations()
	 * @see History.Collaboration#getProposedBy
	 * @model opposite="proposedBy"
	 * @generated
	 */
	EList<Collaboration> getCollaborations();

} // User
