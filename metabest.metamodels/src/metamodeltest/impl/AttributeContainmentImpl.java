/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import java.util.Collection;
import metamodeltest.AttributeContainment;
import metamodeltest.AttributeSelector;
import metamodeltest.MetamodeltestPackage;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Containment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.AttributeContainmentImpl#getAttSels <em>Att Sels</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeContainmentImpl extends ElementRelationImpl implements AttributeContainment {
	/**
	 * The cached value of the '{@link #getAttSels() <em>Att Sels</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttSels()
	 * @generated
	 * @ordered
	 */
	protected EList<AttributeSelector> attSels;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeContainmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.ATTRIBUTE_CONTAINMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttributeSelector> getAttSels() {
		if (attSels == null) {
			attSels = new EObjectContainmentEList<AttributeSelector>(AttributeSelector.class, this, MetamodeltestPackage.ATTRIBUTE_CONTAINMENT__ATT_SELS);
		}
		return attSels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_CONTAINMENT__ATT_SELS:
				return ((InternalEList<?>)getAttSels()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_CONTAINMENT__ATT_SELS:
				return getAttSels();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_CONTAINMENT__ATT_SELS:
				getAttSels().clear();
				getAttSels().addAll((Collection<? extends AttributeSelector>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_CONTAINMENT__ATT_SELS:
				getAttSels().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_CONTAINMENT__ATT_SELS:
				return attSels != null && !attSels.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AttributeContainmentImpl
