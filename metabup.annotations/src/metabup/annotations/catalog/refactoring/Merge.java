/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.refactoring;

import java.util.List;

import fragments.Attribute;
import metabup.annotations.extensionpoint.RefactoringAnnotation;
import metabup.annotations.general.Annotation;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.ParamValue;
import metabup.metamodel.Reference;
import metabup.metamodel.StringValue;

public class Merge extends RefactoringAnnotation {
	
	public Merge(){
		super.setName("merge");
		super.setDescription("It makes one of all the annotated MetaClasses");
	}
	
	@Override
	public MetaModel perform() {
		if(element instanceof MetaClass){			
			List<MetaClass> mcs = super.allMetaClassesWithAnnotation(this.getAnnotation());			
			
			if(mcs.size() > 1){				
				MetaClass newmc = sourceMetaModel.createMetaclass();
				this.targetMetaModel.getClasses().add(newmc);
				
				ParamValue namePV = super.getAnnotationParamValueByParamName("name");
				String name = null;
				
				if(namePV != null){
					if(namePV instanceof StringValue) name = ((StringValue)namePV).getValue();
					else{
						this.putError("A String parameter was expected for \"name\"");
						name = "";
					}
				}
				
				if(name == null) name = "";				
						
				newmc.setIsAbstract(true);
				
				for(MetaClass mc : mcs){
					name += mc.getName();
					if(!mc.getIsAbstract()) newmc.setIsAbstract(false);
					
					for(Feature f : mc.getFeatures()){										
						if(f instanceof metabup.metamodel.Attribute){
							metabup.metamodel.Attribute newatt = MetamodelFactory.eINSTANCE.createAttribute();
							
							newatt.setMax(f.getMax());
							newatt.setMin(f.getMin());							
							newatt.setName(f.getName());
							newatt.setPrimitiveType(((metabup.metamodel.Attribute)f).getPrimitiveType());
							newmc.getFeatures().add(newatt);
						} else if(f instanceof metabup.metamodel.Reference){
							if(!mcs.contains(((metabup.metamodel.Reference)f).getReference())){
								metabup.metamodel.Reference newref = MetamodelFactory.eINSTANCE.createReference();
								
								newref.setMax(f.getMax());
								newref.setMin(f.getMin());
								newref.setName(f.getName());
								newref.setReference(((metabup.metamodel.Reference)f).getReference());
								newmc.getFeatures().add(newref);
							}
						}
					}
					
					for(MetaClass supermc : mc.getSupers()) newmc.getSupers().add(supermc);
					for(MetaClass submc : mc.getSubclasses()) newmc.getSubclasses().add(submc);	
					
					for(MetaClass othermc : targetMetaModel.getClasses()){
						for(Feature f : othermc.getFeatures()){
							if(f instanceof Reference){
								Reference r = (Reference)f;
								if(r.getReference().equals(mc)) r.setReference(newmc);
							}
						}
						
						if(othermc.getSupers().contains(mc)){
							othermc.getSupers().remove(mc);
							othermc.getSupers().add(newmc);
						}
						
						if(othermc.getSubclasses().contains(mc)){
							othermc.getSubclasses().remove(mc);
							othermc.getSubclasses().add(newmc);
						}
					}
				}
				
				newmc.setName(name);
				
				for(MetaClass mc : mcs){					
					super.removeMetaClass(mc);
				}
				
				if(newmc.getSupers().contains(newmc)){
					newmc.getSupers().remove(newmc);
					newmc.getSubclasses().remove(newmc);
				}
				super.removeReplicatedFeatures(newmc);
				
			}
			
			return targetMetaModel;									
		}else return null;		
	}

}
