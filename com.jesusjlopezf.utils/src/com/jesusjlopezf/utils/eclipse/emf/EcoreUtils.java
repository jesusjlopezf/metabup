package com.jesusjlopezf.utils.eclipse.emf;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;

public class EcoreUtils {
	public static List<EClass> getESubTypes(EClass parent){
		List<EClassifier> allClassifiers = parent.getEPackage().getEClassifiers();
		List<EClass> subs = new ArrayList<EClass>();
		
		for(EClassifier ecf : allClassifiers){
			if(ecf instanceof EClass){			
				List<EClass> superclasses;
				superclasses = ((EClass) ecf).getESuperTypes();
				
				for(EClass superclass : superclasses){
					if(superclass.equals(parent)){
						subs.add((EClass)ecf);
					}
				}
			}
		}
		
		return subs;
	}
	
	public static List<EClass> getEAllSubTypes(EClass parent){
		List<EClassifier> allClassifiers = parent.getEPackage().getEClassifiers();
		List<EClass> subs = new ArrayList<EClass>();
		
		for(EClassifier ecf : allClassifiers){
			if(ecf instanceof EClass){			
				List<EClass> superclasses;
				superclasses = ((EClass) ecf).getEAllSuperTypes();
				
				for(EClass superclass : superclasses){
					if(superclass.equals(parent)){
						subs.add((EClass)ecf);
					}
				}
			}
		}
		
		return subs;
	}
}
