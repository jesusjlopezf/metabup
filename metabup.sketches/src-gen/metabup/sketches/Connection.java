/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.sketches;

import graphicProperties.EdgeProperties;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metabup.sketches.Connection#getSrc <em>Src</em>}</li>
 *   <li>{@link metabup.sketches.Connection#getTgt <em>Tgt</em>}</li>
 *   <li>{@link metabup.sketches.Connection#getGraphicProperties <em>Graphic Properties</em>}</li>
 * </ul>
 *
 * @see metabup.sketches.sketchesPackage#getConnection()
 * @model
 * @generated
 */
public interface Connection extends SketchElement {
	/**
	 * Returns the value of the '<em><b>Src</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link metabup.sketches.SketchElement#getSrcOf <em>Src Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src</em>' reference.
	 * @see #setSrc(SketchElement)
	 * @see metabup.sketches.sketchesPackage#getConnection_Src()
	 * @see metabup.sketches.SketchElement#getSrcOf
	 * @model opposite="srcOf" required="true"
	 * @generated
	 */
	SketchElement getSrc();

	/**
	 * Sets the value of the '{@link metabup.sketches.Connection#getSrc <em>Src</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src</em>' reference.
	 * @see #getSrc()
	 * @generated
	 */
	void setSrc(SketchElement value);

	/**
	 * Returns the value of the '<em><b>Tgt</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link metabup.sketches.SketchElement#getTgtOf <em>Tgt Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgt</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tgt</em>' reference.
	 * @see #setTgt(SketchElement)
	 * @see metabup.sketches.sketchesPackage#getConnection_Tgt()
	 * @see metabup.sketches.SketchElement#getTgtOf
	 * @model opposite="tgtOf" required="true"
	 * @generated
	 */
	SketchElement getTgt();

	/**
	 * Sets the value of the '{@link metabup.sketches.Connection#getTgt <em>Tgt</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tgt</em>' reference.
	 * @see #getTgt()
	 * @generated
	 */
	void setTgt(SketchElement value);

	/**
	 * Returns the value of the '<em><b>Graphic Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graphic Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graphic Properties</em>' containment reference.
	 * @see #setGraphicProperties(EdgeProperties)
	 * @see metabup.sketches.sketchesPackage#getConnection_GraphicProperties()
	 * @model containment="true"
	 * @generated
	 */
	EdgeProperties getGraphicProperties();

	/**
	 * Sets the value of the '{@link metabup.sketches.Connection#getGraphicProperties <em>Graphic Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graphic Properties</em>' containment reference.
	 * @see #getGraphicProperties()
	 * @generated
	 */
	void setGraphicProperties(EdgeProperties value);

} // Connection
