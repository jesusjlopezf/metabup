/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.actions;

import java.io.IOException;

import metabup.SessionState;
import metabup.annotations.general.AnnotationFactory;
import metabup.extensionpoints.MetaBupMetaModelExportActionContribution;
import metabup.ui.editor.popup.actions.MetaBupEditorAction;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.preference.IPreferenceStore;

import metabest.transformations.MetaModel2Use;

public class GenerateUseModelAction extends MetaBupMetaModelExportActionContribution {
	public void run() {
		SessionState session = editor.getSession();		
		if(session.getMetamodelRoot() == null) return;
		
		if(session.getMmFile() == null) return;
		IProject project = session.getMmFile().getProject();				
		
		AnnotationFactory af = new AnnotationFactory();
		af.initializeFactory();
		
		MetaModel2Use transformation = new MetaModel2Use(session.getMetamodelRoot(), project, af, null);

		try {
			transformation.execute(null, 0, 0, true, true, true, false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
