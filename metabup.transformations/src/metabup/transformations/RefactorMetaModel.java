/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import metabup.annotations.IAnnotation;
import metabup.annotations.IAnnotationFactory;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Annotation;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.transformations.models.WMetamodelModel;

import org.eclipse.emf.ecore.EObject;
//import metabup.annotations.lib.general.Annotation;
//import metabup.annotations.lib.general.AnnotationFactory;

public class RefactorMetaModel extends BaseTransformation {
	private WMetamodelModel metaModel;

	private HashMap<EObject, List<String>> applied = new HashMap<EObject, List<String>>();		// A simple trace
	
	// Variables of the transformation
	private MetaModel targetMetaModel = null;

	private IAnnotationFactory af;
	
	public RefactorMetaModel(WMetamodelModel metaModel, MetaModel tarMetaModel, IAnnotationFactory factory) {
		this.metaModel = metaModel;
		this.targetMetaModel = tarMetaModel;
		this.af     = factory;
		//af.initializeFactory();				
	}
	
	public boolean hasBeenApplied(EObject r, String refac) {
		if (this.applied.containsKey(r)) {
			return this.applied.get(r).contains(refac);		// we need a list in the value if 
		}
		return false;
	}
	
	public void addTrace(EObject r, String refac) {
		if (!this.applied.containsKey(r)) {
			List<String> ls = new ArrayList<String>();
			ls.add(refac);
			this.applied.put(r, ls);
		} 
		else this.applied.get(r).add(refac);
	}
	
	public boolean triggerRefactorByAnnotation(metabup.metamodel.Annotation ann, boolean active){		
		if(ann == null || ann.eContainer() == null) return false;						
		Class<? extends IAnnotation> annotationClass = af.getAnnotation(ann.getName());
		
		if (annotationClass == null){
			return false;
		}
		
		try {
			IAnnotation annotation = annotationClass.newInstance();
			
			if(ann.eContainer() instanceof AnnotatedElement){
				annotation.setElement((AnnotatedElement)ann.eContainer());
				annotation.setSourceMetaModel(this.metaModel);
				annotation.setTargetMetaModel(this.targetMetaModel);
				annotation.run(active);
				this.targetMetaModel = annotation.getTargetMetaModel();
			}					
		} catch (InstantiationException e) {
			System.out.println("InstantiationException");
			return false;
		} catch (IllegalAccessException e) {
			System.out.println("IllegalAccessException");
			return false;
		}				
		
		return true;
	}
	
	public void clearNonPersistentAnnotations(){
		List<MetaClass> mcs = targetMetaModel.getClasses();		
		if(mcs == null || mcs.isEmpty()) return;					
		
		for(MetaClass mc : mcs){		
			List<Annotation> anns = new ArrayList<Annotation>();
				
			for(Annotation ann : mc.getAnnotations()) if(af.existsAnnotation(ann.getName())){
				Class<? extends IAnnotation> annotationClass = af.getAnnotation(ann.getName());
				if (annotationClass != null){
					try {
						IAnnotation annotation = annotationClass.newInstance();						
						if(!annotation.isPersistent()) anns.add(ann);
					} catch (InstantiationException e) {
						// TODO Autlo-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}							
			}
			
			for(Annotation ann : anns) mc.getAnnotations().remove(ann);
			anns.clear();
			for(Feature f : mc.getFeatures()){
				for(Annotation ann : f.getAnnotations()){
					if(af.existsAnnotation(ann.getName())){
						Class<? extends IAnnotation> annotationClass = af.getAnnotation(ann.getName());
						if (annotationClass != null){
							try {
								IAnnotation annotation = annotationClass.newInstance();						
								if(!annotation.isPersistent()) anns.add(ann);
							} catch (InstantiationException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IllegalAccessException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
				
				for(Annotation ann : anns) f.getAnnotations().remove(ann);
			}
		}
	}
}
