#*******************************************************************************
# Copyright (c) 2015 IBM Corporation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
if {![package vsatisfies [package provide Tcl] 8.2]} {return}
package ifneeded msgcat 1.3.2 [list source [file join $dir msgcat.tcl]]
