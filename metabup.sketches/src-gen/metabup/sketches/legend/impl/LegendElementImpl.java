/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.sketches.legend.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import metabup.sketches.legend.LegendElement;
import metabup.sketches.legend.LegendPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link metabup.sketches.legend.impl.LegendElementImpl#getOriginalSymbolName <em>Original Symbol Name</em>}</li>
 *   <li>{@link metabup.sketches.legend.impl.LegendElementImpl#getNewSymbolName <em>New Symbol Name</em>}</li>
 *   <li>{@link metabup.sketches.legend.impl.LegendElementImpl#getDefaultWidth <em>Default Width</em>}</li>
 *   <li>{@link metabup.sketches.legend.impl.LegendElementImpl#getDefaultHeight <em>Default Height</em>}</li>
 *   <li>{@link metabup.sketches.legend.impl.LegendElementImpl#getColor <em>Color</em>}</li>
 *   <li>{@link metabup.sketches.legend.impl.LegendElementImpl#isTransparent <em>Transparent</em>}</li>
 *   <li>{@link metabup.sketches.legend.impl.LegendElementImpl#getFile <em>File</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LegendElementImpl extends MinimalEObjectImpl.Container implements LegendElement {
	/**
	 * The default value of the '{@link #getOriginalSymbolName() <em>Original Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalSymbolName()
	 * @generated
	 * @ordered
	 */
	protected static final String ORIGINAL_SYMBOL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOriginalSymbolName() <em>Original Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOriginalSymbolName()
	 * @generated
	 * @ordered
	 */
	protected String originalSymbolName = ORIGINAL_SYMBOL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getNewSymbolName() <em>New Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewSymbolName()
	 * @generated
	 * @ordered
	 */
	protected static final String NEW_SYMBOL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNewSymbolName() <em>New Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewSymbolName()
	 * @generated
	 * @ordered
	 */
	protected String newSymbolName = NEW_SYMBOL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefaultWidth() <em>Default Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultWidth()
	 * @generated
	 * @ordered
	 */
	protected static final float DEFAULT_WIDTH_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getDefaultWidth() <em>Default Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultWidth()
	 * @generated
	 * @ordered
	 */
	protected float defaultWidth = DEFAULT_WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefaultHeight() <em>Default Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultHeight()
	 * @generated
	 * @ordered
	 */
	protected static final float DEFAULT_HEIGHT_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getDefaultHeight() <em>Default Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultHeight()
	 * @generated
	 * @ordered
	 */
	protected float defaultHeight = DEFAULT_HEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final String COLOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected String color = COLOR_EDEFAULT;

	/**
	 * The default value of the '{@link #isTransparent() <em>Transparent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransparent()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TRANSPARENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTransparent() <em>Transparent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransparent()
	 * @generated
	 * @ordered
	 */
	protected boolean transparent = TRANSPARENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getFile() <em>File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFile()
	 * @generated
	 * @ordered
	 */
	protected static final String FILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFile() <em>File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFile()
	 * @generated
	 * @ordered
	 */
	protected String file = FILE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LegendElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LegendPackage.Literals.LEGEND_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOriginalSymbolName() {
		return originalSymbolName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOriginalSymbolName(String newOriginalSymbolName) {
		String oldOriginalSymbolName = originalSymbolName;
		originalSymbolName = newOriginalSymbolName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LegendPackage.LEGEND_ELEMENT__ORIGINAL_SYMBOL_NAME, oldOriginalSymbolName, originalSymbolName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNewSymbolName() {
		return newSymbolName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNewSymbolName(String newNewSymbolName) {
		String oldNewSymbolName = newSymbolName;
		newSymbolName = newNewSymbolName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LegendPackage.LEGEND_ELEMENT__NEW_SYMBOL_NAME, oldNewSymbolName, newSymbolName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getDefaultWidth() {
		return defaultWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultWidth(float newDefaultWidth) {
		float oldDefaultWidth = defaultWidth;
		defaultWidth = newDefaultWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LegendPackage.LEGEND_ELEMENT__DEFAULT_WIDTH, oldDefaultWidth, defaultWidth));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getDefaultHeight() {
		return defaultHeight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultHeight(float newDefaultHeight) {
		float oldDefaultHeight = defaultHeight;
		defaultHeight = newDefaultHeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LegendPackage.LEGEND_ELEMENT__DEFAULT_HEIGHT, oldDefaultHeight, defaultHeight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(String newColor) {
		String oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LegendPackage.LEGEND_ELEMENT__COLOR, oldColor, color));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTransparent() {
		return transparent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransparent(boolean newTransparent) {
		boolean oldTransparent = transparent;
		transparent = newTransparent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LegendPackage.LEGEND_ELEMENT__TRANSPARENT, oldTransparent, transparent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFile() {
		return file;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFile(String newFile) {
		String oldFile = file;
		file = newFile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LegendPackage.LEGEND_ELEMENT__FILE, oldFile, file));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LegendPackage.LEGEND_ELEMENT__ORIGINAL_SYMBOL_NAME:
				return getOriginalSymbolName();
			case LegendPackage.LEGEND_ELEMENT__NEW_SYMBOL_NAME:
				return getNewSymbolName();
			case LegendPackage.LEGEND_ELEMENT__DEFAULT_WIDTH:
				return getDefaultWidth();
			case LegendPackage.LEGEND_ELEMENT__DEFAULT_HEIGHT:
				return getDefaultHeight();
			case LegendPackage.LEGEND_ELEMENT__COLOR:
				return getColor();
			case LegendPackage.LEGEND_ELEMENT__TRANSPARENT:
				return isTransparent();
			case LegendPackage.LEGEND_ELEMENT__FILE:
				return getFile();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LegendPackage.LEGEND_ELEMENT__ORIGINAL_SYMBOL_NAME:
				setOriginalSymbolName((String)newValue);
				return;
			case LegendPackage.LEGEND_ELEMENT__NEW_SYMBOL_NAME:
				setNewSymbolName((String)newValue);
				return;
			case LegendPackage.LEGEND_ELEMENT__DEFAULT_WIDTH:
				setDefaultWidth((Float)newValue);
				return;
			case LegendPackage.LEGEND_ELEMENT__DEFAULT_HEIGHT:
				setDefaultHeight((Float)newValue);
				return;
			case LegendPackage.LEGEND_ELEMENT__COLOR:
				setColor((String)newValue);
				return;
			case LegendPackage.LEGEND_ELEMENT__TRANSPARENT:
				setTransparent((Boolean)newValue);
				return;
			case LegendPackage.LEGEND_ELEMENT__FILE:
				setFile((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LegendPackage.LEGEND_ELEMENT__ORIGINAL_SYMBOL_NAME:
				setOriginalSymbolName(ORIGINAL_SYMBOL_NAME_EDEFAULT);
				return;
			case LegendPackage.LEGEND_ELEMENT__NEW_SYMBOL_NAME:
				setNewSymbolName(NEW_SYMBOL_NAME_EDEFAULT);
				return;
			case LegendPackage.LEGEND_ELEMENT__DEFAULT_WIDTH:
				setDefaultWidth(DEFAULT_WIDTH_EDEFAULT);
				return;
			case LegendPackage.LEGEND_ELEMENT__DEFAULT_HEIGHT:
				setDefaultHeight(DEFAULT_HEIGHT_EDEFAULT);
				return;
			case LegendPackage.LEGEND_ELEMENT__COLOR:
				setColor(COLOR_EDEFAULT);
				return;
			case LegendPackage.LEGEND_ELEMENT__TRANSPARENT:
				setTransparent(TRANSPARENT_EDEFAULT);
				return;
			case LegendPackage.LEGEND_ELEMENT__FILE:
				setFile(FILE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LegendPackage.LEGEND_ELEMENT__ORIGINAL_SYMBOL_NAME:
				return ORIGINAL_SYMBOL_NAME_EDEFAULT == null ? originalSymbolName != null : !ORIGINAL_SYMBOL_NAME_EDEFAULT.equals(originalSymbolName);
			case LegendPackage.LEGEND_ELEMENT__NEW_SYMBOL_NAME:
				return NEW_SYMBOL_NAME_EDEFAULT == null ? newSymbolName != null : !NEW_SYMBOL_NAME_EDEFAULT.equals(newSymbolName);
			case LegendPackage.LEGEND_ELEMENT__DEFAULT_WIDTH:
				return defaultWidth != DEFAULT_WIDTH_EDEFAULT;
			case LegendPackage.LEGEND_ELEMENT__DEFAULT_HEIGHT:
				return defaultHeight != DEFAULT_HEIGHT_EDEFAULT;
			case LegendPackage.LEGEND_ELEMENT__COLOR:
				return COLOR_EDEFAULT == null ? color != null : !COLOR_EDEFAULT.equals(color);
			case LegendPackage.LEGEND_ELEMENT__TRANSPARENT:
				return transparent != TRANSPARENT_EDEFAULT;
			case LegendPackage.LEGEND_ELEMENT__FILE:
				return FILE_EDEFAULT == null ? file != null : !FILE_EDEFAULT.equals(file);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (originalSymbolName: ");
		result.append(originalSymbolName);
		result.append(", newSymbolName: ");
		result.append(newSymbolName);
		result.append(", defaultWidth: ");
		result.append(defaultWidth);
		result.append(", defaultHeight: ");
		result.append(defaultHeight);
		result.append(", color: ");
		result.append(color);
		result.append(", transparent: ");
		result.append(transparent);
		result.append(", file: ");
		result.append(file);
		result.append(')');
		return result.toString();
	}

} //LegendElementImpl
