/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.MetamodeltestPackage;
import metamodeltest.ReferenceFilter;
import metamodeltest.ReferenceSelector;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference Selector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.ReferenceSelectorImpl#getReferenceFilter <em>Reference Filter</em>}</li>
 *   <li>{@link metamodeltest.impl.ReferenceSelectorImpl#getReferenceConditioner <em>Reference Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReferenceSelectorImpl extends SelectorImpl implements ReferenceSelector {
	/**
	 * The cached value of the '{@link #getReferenceFilter() <em>Reference Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceFilter()
	 * @generated
	 * @ordered
	 */
	protected ReferenceFilter referenceFilter;

	/**
	 * The cached value of the '{@link #getReferenceConditioner() <em>Reference Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceConditioner()
	 * @generated
	 * @ordered
	 */
	protected ReferenceFilter referenceConditioner;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferenceSelectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.REFERENCE_SELECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceFilter getReferenceFilter() {
		return referenceFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReferenceFilter(ReferenceFilter newReferenceFilter, NotificationChain msgs) {
		ReferenceFilter oldReferenceFilter = referenceFilter;
		referenceFilter = newReferenceFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_FILTER, oldReferenceFilter, newReferenceFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceFilter(ReferenceFilter newReferenceFilter) {
		if (newReferenceFilter != referenceFilter) {
			NotificationChain msgs = null;
			if (referenceFilter != null)
				msgs = ((InternalEObject)referenceFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_FILTER, null, msgs);
			if (newReferenceFilter != null)
				msgs = ((InternalEObject)newReferenceFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_FILTER, null, msgs);
			msgs = basicSetReferenceFilter(newReferenceFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_FILTER, newReferenceFilter, newReferenceFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceFilter getReferenceConditioner() {
		return referenceConditioner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetReferenceConditioner(ReferenceFilter newReferenceConditioner, NotificationChain msgs) {
		ReferenceFilter oldReferenceConditioner = referenceConditioner;
		referenceConditioner = newReferenceConditioner;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_CONDITIONER, oldReferenceConditioner, newReferenceConditioner);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenceConditioner(ReferenceFilter newReferenceConditioner) {
		if (newReferenceConditioner != referenceConditioner) {
			NotificationChain msgs = null;
			if (referenceConditioner != null)
				msgs = ((InternalEObject)referenceConditioner).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_CONDITIONER, null, msgs);
			if (newReferenceConditioner != null)
				msgs = ((InternalEObject)newReferenceConditioner).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_CONDITIONER, null, msgs);
			msgs = basicSetReferenceConditioner(newReferenceConditioner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_CONDITIONER, newReferenceConditioner, newReferenceConditioner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_FILTER:
				return basicSetReferenceFilter(null, msgs);
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_CONDITIONER:
				return basicSetReferenceConditioner(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_FILTER:
				return getReferenceFilter();
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_CONDITIONER:
				return getReferenceConditioner();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_FILTER:
				setReferenceFilter((ReferenceFilter)newValue);
				return;
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_CONDITIONER:
				setReferenceConditioner((ReferenceFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_FILTER:
				setReferenceFilter((ReferenceFilter)null);
				return;
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_CONDITIONER:
				setReferenceConditioner((ReferenceFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_FILTER:
				return referenceFilter != null;
			case MetamodeltestPackage.REFERENCE_SELECTOR__REFERENCE_CONDITIONER:
				return referenceConditioner != null;
		}
		return super.eIsSet(featureID);
	}

} //ReferenceSelectorImpl
