/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import metabest.solver.ui.preferences.PreferenceConstants;
import metabest.test.fragments.MBFStandaloneSetup;
import metabup.annotations.catalog.design.Composition;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Attribute;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;
import metamodeltest.IntegerParameter;
import metamodeltest.MetamodeltestFactory;
import metamodeltest.Some;
import test.AssertionSet;
import test.ExtensionAssertion;
import test.ExtensionAssertionSet;
import test.MetaClassExtensionAssertion;
import test.TestFactory;
import test.TestModel;
import test.extensions.AttributeValue;
import test.extensions.Contains;
import test.extensions.ExtensionsFactory;
import test.extensions.MetaClassQualifier;
import test.extensions.MetaClassSelector;
import test.extensions.Reach;

public class CoverageCriteriaConstraintGenerator {
	private MetaModel mm;
	private List<AnnotatedElement> elements;
	private Iterator<AssertionSet> iterator;
	private List<ExtensionAssertionSet> assertionSetList = new ArrayList<ExtensionAssertionSet>();
	private int coverageCriteria = SIMPLE_COVERAGE_CRITERIA;
	
	public final static int SIMPLE_COVERAGE_CRITERIA = 0;
	public final static int CLASS_BY_CLASS_CRITERIA = 1;
	
	
	public CoverageCriteriaConstraintGenerator(MetaModel mm, List<AnnotatedElement> elements, int coverageCriteria){
		this.mm = mm;
		this.elements = elements;
		
		if(coverageCriteria == SIMPLE_COVERAGE_CRITERIA 
				|| coverageCriteria == CLASS_BY_CLASS_CRITERIA){
			this.coverageCriteria = coverageCriteria;
		}
		
		assertionSetList = calculateAssertionSets(elements);
	}
	
	private List<ExtensionAssertionSet> calculateAssertionSets(List<AnnotatedElement> elements){
		switch(coverageCriteria){
			case SIMPLE_COVERAGE_CRITERIA:
				for(AnnotatedElement element : elements){
					if(element instanceof Attribute) assertionSetList.addAll(this.calculateAttributeAssertions((Attribute)element));
					else if(element instanceof Reference) assertionSetList.addAll(this.calculateReferenceAssertions((Reference)element));
					else if(element instanceof MetaClass) assertionSetList.addAll(this.calculateMetaClassAssertions((MetaClass)element));
				}
			break;
			
			case CLASS_BY_CLASS_CRITERIA:
			break;
		}
		
		return assertionSetList;
	}

	private List<ExtensionAssertionSet> calculateAttributeAssertions(Attribute attribute){
		List<ExtensionAssertionSet> assertionSets = new ArrayList<ExtensionAssertionSet>();
		MetaClass srcmc = (MetaClass)attribute.eContainer();
		if(srcmc == null) return assertionSets;
		
		switch(attribute.getPrimitiveType()){
			case "IntType":
				Random generator = new Random(); 
				Integer intRange[] = {0,1, generator.nextInt((Integer.MAX_VALUE - 2) + 1) + 2};
				ExtensionAssertionSet altogetherIntSet = TestFactory.eINSTANCE.createExtensionAssertionSet();
				
				Set<Integer> rangeSet = new HashSet<Integer>(Arrays.asList(intRange));
				
				for(int bound : rangeSet){
					ExtensionAssertionSet assertionSet = TestFactory.eINSTANCE.createExtensionAssertionSet();
					MetaClassExtensionAssertion assertion = TestFactory.eINSTANCE.createMetaClassExtensionAssertion();
					MetaClassSelector selector = ExtensionsFactory.eINSTANCE.createMetaClassSelector();
					Some quantifier = MetamodeltestFactory.eINSTANCE.createSome();
					selector.setQuantifier(quantifier);
					selector.setMetaclassName(srcmc.getName());
					AttributeValue attValue = ExtensionsFactory.eINSTANCE.createAttributeValue();
					attValue.setAttributeName(attribute.getName());
					attValue.setIntValue(bound);
					assertion.setSelector(selector);
					assertion.setCondition(attValue);
					assertionSet.getAssertions().add(assertion);
					
					MetaClassExtensionAssertion assertion2 = EcoreUtil.copy(assertion);
					altogetherIntSet.getAssertions().add(assertion2);
					
					assertionSets.add(assertionSet);
				}
				
				if(rangeSet.size() > 1) assertionSets.add(altogetherIntSet);
				
			break;
			
			case "BooleanType":
				Boolean boolRange[] = {true, false};
				ExtensionAssertionSet altogetherBoolSet = TestFactory.eINSTANCE.createExtensionAssertionSet();
								
				for(Boolean bound : boolRange){
					ExtensionAssertionSet assertionSet = TestFactory.eINSTANCE.createExtensionAssertionSet();
					MetaClassExtensionAssertion assertion = TestFactory.eINSTANCE.createMetaClassExtensionAssertion();
					MetaClassSelector selector = ExtensionsFactory.eINSTANCE.createMetaClassSelector();
					Some quantifier = MetamodeltestFactory.eINSTANCE.createSome();
					selector.setQuantifier(quantifier);
					selector.setMetaclassName(srcmc.getName());
					AttributeValue attValue = ExtensionsFactory.eINSTANCE.createAttributeValue();
					attValue.setAttributeName(attribute.getName());
					attValue.setBooleanValue(bound.toString());
					assertion.setSelector(selector);
					assertion.setCondition(attValue);
					assertionSet.getAssertions().add(assertion);
					
					MetaClassExtensionAssertion assertion2 = EcoreUtil.copy(assertion);
					altogetherBoolSet.getAssertions().add(assertion2);
					
					assertionSets.add(assertionSet);
				}
				
				if(boolRange.length > 1) assertionSets.add(altogetherBoolSet);
				
			break;
		}
		
		return assertionSets;
	}
	
	private List<ExtensionAssertionSet> calculateReferenceAssertions(Reference reference) {
		List<ExtensionAssertionSet> assertionSets = new ArrayList<ExtensionAssertionSet>();
		MetaClass srcmc = (MetaClass)reference.eContainer();
		if(srcmc == null) return assertionSets;
		MetaClass tarmc = (MetaClass)reference.getReference();
		if(tarmc == null) return assertionSets; 
		
		Integer multiplicity[] = {Integer.max(reference.getMin(), PreferenceConstants.getPreferenceIntegerValue(PreferenceConstants.P_MIN_DEFAULT_REFERENCE_CARDINALITY)), 
				1, 
				Integer.min(reference.getMax(), PreferenceConstants.getPreferenceIntegerValue(PreferenceConstants.P_MAX_DEFAULT_REFERENCE_CARDINALITY))};
		if(multiplicity[2] < 0) multiplicity[2] = PreferenceConstants.getPreferenceIntegerValue(PreferenceConstants.P_MAX_DEFAULT_REFERENCE_CARDINALITY);
		
		ExtensionAssertionSet altogetherRefSet = TestFactory.eINSTANCE.createExtensionAssertionSet();
		
		Set<Integer> multiplicitySet = new HashSet<Integer>(Arrays.asList(multiplicity));
		
		for(int bound : multiplicitySet){
			ExtensionAssertionSet assertionSet = TestFactory.eINSTANCE.createExtensionAssertionSet();
			MetaClassExtensionAssertion assertion = TestFactory.eINSTANCE.createMetaClassExtensionAssertion();
			MetaClassSelector selector = ExtensionsFactory.eINSTANCE.createMetaClassSelector();
			Some quantifier = MetamodeltestFactory.eINSTANCE.createSome();
			selector.setQuantifier(quantifier);
			selector.setMetaclassName(srcmc.getName());
			
			Reach condition = null;
			
			if(reference.isAnnotated(Composition.NAME)){
				condition = ExtensionsFactory.eINSTANCE.createContains();
				MetaClassSelector tarSelector = ExtensionsFactory.eINSTANCE.createMetaClassSelector();
				metamodeltest.StrictNumber tarQuantifier = MetamodeltestFactory.eINSTANCE.createStrictNumber();
				IntegerParameter integer = MetamodeltestFactory.eINSTANCE.createIntegerParameter();
				integer.setValue(bound);
				tarQuantifier.setN(integer);
				tarSelector.setQuantifier(tarQuantifier);
				tarSelector.setMetaclassName(tarmc.getName());
				condition.setReachSelector(tarSelector);	
				assertion.setCondition(condition);
			}
			else{
				condition = ExtensionsFactory.eINSTANCE.createReach();
				MetaClassSelector tarSelector = ExtensionsFactory.eINSTANCE.createMetaClassSelector();
				metamodeltest.StrictNumber tarQuantifier = MetamodeltestFactory.eINSTANCE.createStrictNumber();
				IntegerParameter integer = MetamodeltestFactory.eINSTANCE.createIntegerParameter();
				integer.setValue(bound);
				tarQuantifier.setN(integer);
				tarSelector.setQuantifier(tarQuantifier);
				tarSelector.setMetaclassName(tarmc.getName());
				condition.setReachSelector(tarSelector);
				assertion.setCondition(condition);
			}
			
			assertion.setSelector(selector);
			assertion.setCondition(condition);
			assertionSet.getAssertions().add(assertion);
			
			MetaClassExtensionAssertion assertion2 = EcoreUtil.copy(assertion);
			altogetherRefSet.getAssertions().add(assertion2);
			
			assertionSets.add(assertionSet);
		}
		
		if(multiplicitySet.size() > 1) assertionSets.add(altogetherRefSet);
		
		return assertionSets;
	}

	

	private List<ExtensionAssertionSet> calculateMetaClassAssertions(MetaClass metaclass){
		List<ExtensionAssertionSet> assertionSets = new ArrayList<ExtensionAssertionSet>();
		ExtensionAssertionSet assertionSet = TestFactory.eINSTANCE.createExtensionAssertionSet();
		MetaClassExtensionAssertion assertion = TestFactory.eINSTANCE.createMetaClassExtensionAssertion();
		MetaClassSelector selector = ExtensionsFactory.eINSTANCE.createMetaClassSelector();
		Some quantifier = MetamodeltestFactory.eINSTANCE.createSome();
		selector.setQuantifier(quantifier);
		selector.setMetaclassName(metaclass.getName());
		assertion.setSelector(selector);
		assertionSet.getAssertions().add(assertion);
		assertionSets.add(assertionSet);
		return assertionSets;
	}

	public List<ExtensionAssertionSet> getAssertionSetList() {
		return assertionSetList;
	}
	
	
	
}
