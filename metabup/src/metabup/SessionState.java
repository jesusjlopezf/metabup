/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import metabup.annotations.general.AnnotationFactory;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.ISolution;
import metabup.assistance.general.AssistanceTip;
import metabup.assistance.general.AssistanceTipsFactory;
import metabup.change.collaboro.CollaboroChangeTrace;
import metabup.change.trace.ChangeTrace;
import metabup.fragments.resources.FragmentFolder;
import metabup.issues.general.IOpenIssue;
import metabup.issues.general.OpenIssue;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Annotation;
import metabup.metamodel.MetaModel;
import metabup.perspective.views.AssistanceView;
import metabup.perspective.views.OpenIssuesView;
import metabup.shell.Command;
import metabup.shell.FragmentCommand;
import metabup.shell.ShellFactory;
import metabup.shell.ShellModel;
import metabup.shell.ShellPackage;
import metabup.shell.impl.ShellModelImpl;
import metabup.transformations.Ecore2MetaModel;
import metabup.transformations.InduceMetaModel;
import metabup.transformations.RefactorMetaModel;
import metabup.transformations.XSD2Ecore;
import metabup.transformations.models.WEcoreModel;
import metabup.transformations.models.WFragmentModel;
import metabup.transformations.models.WMetamodelModel;
import metabup.ui.editor.history.rename.RenameHistory;
import metabup.ui.internal.FragmentsActivator;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.xsd.util.XSDResourceImpl;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.ui.editor.model.XtextDocument;
import org.eclipse.xtext.ui.resource.XtextResourceSetProvider;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;

import com.google.inject.Injector;
import java.util.Map;
import java.util.Map.Entry;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.FragmentsFactory;
import fragments.FragmentsPackage;

//import metabup.annotations.assistance.lib.general.AssistanceTip;
//import metabup.annotations.assistance.lib.general.AssistanceTipsFactory;

/**
 * Class to handle the state a meta-modeling session. Each session is 
 * registered in the SessionManager class.
 * 
 * 
 * @author jesus
 * @param <SessionState>
 *
 */

public class SessionState {
	
	protected static final String ECORE_FILE_EXTENSION = "ecore"; //$NON-NLS-1$
	
	private IFile mmFile;
	private Resource metamodelResource;	
	private IFile shellFile;
	private XtextDocument shellDocument;
	private IFile historyFile;
	private XtextDocument historyDocument;
	private List<IAssistanceTip> tips = new ArrayList<IAssistanceTip>();
	private List<IOpenIssue> issues = new ArrayList<IOpenIssue>();
	private IProject project;

	private HashMap<String, Resource> deriveMetamodels = new HashMap<String, Resource>();
	private CollaboroChangeTrace changeTrace = new CollaboroChangeTrace();

	private RenameHistory renameHistory = new RenameHistory();
	
	public SessionState(IFile fragFile, IFile historyFile, IFile mmFile, Resource metamodelResource) {
		this.shellFile = fragFile;
		this.historyFile = historyFile;
		this.mmFile = mmFile;
		this.metamodelResource = metamodelResource;
		if(mmFile != null) this.setProject(mmFile.getProject());
	}
	
	public Resource getMetamodelResource() {
		return metamodelResource;
	}
	
	public IFile getFragmentFile() {
		return shellFile;
	}
	
	public IFile getHistoryFile() {
		return historyFile;
	}
	
	public IFile getMmFile(){
		return mmFile;
	}

	public MetaModel getMetamodelRoot() {
		return getMetamodelModel().rootMetamodel();
	}

	public WMetamodelModel getMetamodelModel() {
		return new WMetamodelModel(metamodelResource);
	}
	
	public void setFile(IFile mmFile){
		this.mmFile = mmFile;
	}
	
	public List<IAssistanceTip> getTips() {
		return tips;
	}
	
	public List<OpenIssue> getIssues(Class<?> clazz) {
		List<OpenIssue> retIssues = new ArrayList<OpenIssue>();
		for(IOpenIssue i : this.issues) if(clazz.isInstance(i)) retIssues.add((OpenIssue)i);		
		return retIssues;
	}
	
	public List<IOpenIssue> getIssues() {
		return this.issues;
	}
	
	public ChangeTrace getChangeTrace(){
		return this.changeTrace;
	}
	
	public void updateAssistance(){
		////System.out.println("Updating assistent");
		this.tips.clear();
		AssistanceTipsFactory atf = new AssistanceTipsFactory();
		atf.initializeFactory();
		
		for(Class<? extends AssistanceTip> atc : atf.listAllAssitanceTips()){
			////System.out.println("! checking " + atc.getName());
			try {
				AssistanceTip at = atc.newInstance();				
				at.setSourceMetaModel(this.getMetamodelModel());		
				if(at.run()) this.tips.add(at);
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}					
		
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if (AssistanceView.ID.equals(viewReferences[i].getId())){
				AssistanceView av = (AssistanceView)viewReferences[i].getView(true);
				av.refresh(this);
			}
		}
		
		if(this.tips.size() > 0){
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(AssistanceView.ID);
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	private void updateIssues(List<IOpenIssue> list){
		this.issues.clear();
		if(list != null) this.issues.addAll(list);
		
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (int i = 0; i < viewReferences.length; i++) {
			if (OpenIssuesView.ID.equals(viewReferences[i].getId())){				
				OpenIssuesView aiv = (OpenIssuesView)viewReferences[i].getView(true);
				aiv.refresh(this);
			}
		}
		
		if(this.issues.size() > 0){
			try {
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(OpenIssuesView.ID);				
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean saveChanges(){
		try {
			metamodelResource.save(null);
			mmFile.refreshLocal(1, null);	
			this.updateAssistance();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			return true;
		}
		
		return true;
	}
	
	// TODO: Check the resource has no errors!!
	public boolean updateMetamodel(final String fragmentName) {
		
		return shellDocument.modify(new IUnitOfWork<Boolean, XtextResource>() {

			@Override
			public Boolean exec(XtextResource underlyingResource) throws Exception {				
				final ShellModel shell = (ShellModel) underlyingResource.getContents().get(0);
				FragmentModel model = extractFragmentModel(shell);
				if ( model.getFragments().size() == 0 )
					return false;
				
				ResourceImpl frgResource = new ResourceImpl();					
				frgResource.getContents().add(model);
				ResourceSet res = new ResourceSetImpl();
				res.getResources().add(frgResource);
				
				
				AnnotationFactory af = new AnnotationFactory();
				af.initializeFactory();				
				
				InduceMetaModel induce = new InduceMetaModel(new WFragmentModel(frgResource), new WMetamodelModel(metamodelResource), changeTrace, af);				
				induce.execute(true);	
								
				metamodelResource.save(null);				
				mmFile.refreshLocal(1, null);				
								
				updateIssues(induce.getIssues());
				updateAssistance();
				WFragmentModel wfm = new WFragmentModel(frgResource);
				updateFragmentFolder(wfm.rootFragmentModel());
				
				FragmentsPackage.eINSTANCE.eClass();
				ResourceSet rs = new ResourceSetImpl();
				Resource rsc = rs.getResource(URI.createURI(FragmentsFactory.eINSTANCE.getEPackage().getNsURI()), true);
				rsc.load(null);
				EcoreUtil2.resolveAll(rsc);								
				
				// Now, modify the metabup.collaboro.metamodels.history
				/*historyDocument.modify(new IUnitOfWork<Boolean, XtextResource>() {
					@Override
					public Boolean exec(XtextResource historyResource) throws Exception {
						// List<Fragment> fragments = extractFragmentsFromCommands(shell);
						ShellModel history = (ShellModel) historyResource.getContents().get(0);
						history.getCommands().addAll(EcoreUtil2.copyAll(shell.getCommands()));
						
						// Make the metabup.collaboro.metamodels.history looks like a stack
						int shellSize   = shell.getCommands().size();
						int historySize = history.getCommands().size();
						for(int i = 0; i < shellSize; i++) {
							history.getCommands().move(i, historySize - i - 1);
						}
						shell.getCommands().clear();
						
						return true;
					}					
				});*/
				
				return true;
			}		
					
		});	
	}
	
	private void updateFragmentFolder(FragmentModel fm) {
		// TODO Auto-generated method stub
		FragmentFolder folder = new FragmentFolder(this.getProject());
		if(!folder.exists()) folder.create(null);
		folder.addFragment(fm);
	}	
	
	public boolean updateMetamodelByAssistanceTip(final String assistanceTipDescription) {			
		return shellDocument.modify(new IUnitOfWork<Boolean, XtextResource>() {

		@Override
		public Boolean exec(XtextResource underlyingResource) throws Exception {				
			final ShellModel shell = (ShellModel) underlyingResource.getContents().get(0);									
			
			boolean found = false;
			
			/* Refactoring by assistance tip appliance. 
			 * I'm not really happy with the way this is done, but it works so far */
			for(int i = 0; i<tips.size() && !found; i++){
				AssistanceTip at = (AssistanceTip)tips.get(i);				
				ISolution solution = at.getSolution(assistanceTipDescription);
				
				if(solution != null){
					List<AnnotatedElement> origElements = solution.getElements();
					if (origElements == null) return false;					
					List<Annotation> annotations = new ArrayList<Annotation>();
					List<AnnotatedElement> elements = new ArrayList<AnnotatedElement>();
					
					TreeIterator<EObject> it = getMetamodelRoot().eAllContents();
					
					while(it.hasNext()){
						EObject eo = it.next();
						
						if(eo instanceof AnnotatedElement){
							AnnotatedElement ae = (AnnotatedElement)eo;
							
							for(AnnotatedElement oldE : origElements) 
								if(ae.getId().equals(oldE.getId()))
									elements.add(ae);
						}
					}
					
					if(elements != null && !elements.isEmpty()){										
						found = true;
						AnnotationFactory af = new AnnotationFactory();
						af.initializeFactory();															
						
						for(AnnotatedElement e : elements){
							Annotation an = solution.getAnnotation();
							e.getAnnotations().add(an);
							annotations.add(an);						
						}
						
						RefactorMetaModel rm = new RefactorMetaModel(new WMetamodelModel(metamodelResource), getMetamodelRoot(), af);
						
						for(Annotation a : annotations) rm.triggerRefactorByAnnotation(a, true);
						rm.clearNonPersistentAnnotations();						
					}
				}																
			}
						
				metamodelResource.save(null);				
				mmFile.refreshLocal(1, null);

				updateAssistance();
						
				// Now, modify the metabup.collaboro.metamodels.history
				historyDocument.modify(new IUnitOfWork<Boolean, XtextResource>() {
				
					@Override
					public Boolean exec(XtextResource historyResource) throws Exception {
						// List<Fragment> fragments = extractFragmentsFromCommands(shell);
						ShellModel history = (ShellModel) historyResource.getContents().get(0);
						history.getCommands().addAll(EcoreUtil2.copyAll(shell.getCommands()));
									
						// Make the metabup.collaboro.metamodels.history looks like a stack
						int shellSize   = shell.getCommands().size();
						int historySize = history.getCommands().size();
						for(int i = 0; i < shellSize; i++) history.getCommands().move(i, historySize - i - 1);			
				
						shell.getCommands().clear();										
						return true;
					}
				});
					
				return true;
			}								
		});	
	}
	
	public boolean updateMetamodelByOpenIssue(final String openIssueDescription) {			
		return shellDocument.modify(new IUnitOfWork<Boolean, XtextResource>() {

		@Override
		public Boolean exec(XtextResource underlyingResource) throws Exception {				
			final ShellModel shell = (ShellModel) underlyingResource.getContents().get(0);									
			
			boolean found = false;
			
			for(int i = 0; i<issues.size() && !found; i++){
				if(issues.get(i).getDescription().equals(openIssueDescription)){
					issues.get(i).run();
					issues.remove(i);
				}				
			}				
						
			metamodelResource.save(null);				
			mmFile.refreshLocal(1, null);

			// changeTrace = induce.getChangeTrace();
			updateAssistance();
						
			// Now, modify the metabup.collaboro.metamodels.history
		/*	historyDocument.modify(new IUnitOfWork<Boolean, XtextResource>() {				
				@Override
				public Boolean exec(XtextResource historyResource) throws Exception {
					// List<Fragment> fragments = extractFragmentsFromCommands(shell);
					ShellModel history = (ShellModel) historyResource.getContents().get(0);
					history.getCommands().addAll(EcoreUtil2.copyAll(shell.getCommands()));
									
					// Make the metabup.collaboro.metamodels.history looks like a stack
					int shellSize   = shell.getCommands().size();
					int historySize = history.getCommands().size();
					for(int i = 0; i < shellSize; i++) history.getCommands().move(i, historySize - i - 1);			
				
					shell.getCommands().clear();										
					return true;
				}
			});
					*/
		return true;
	}								
	});	
}
	
	public boolean generateCollaboroHistory(){
		if(mmFile != null){
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot(); 
			IFile file = root.getFile(mmFile.getFullPath().removeFileExtension().addFileExtension("history"));
			changeTrace.exportToCollaboro(file);
			changeTrace.printIndex();
			return true;
		}
		
		return false;
	}
	
	public boolean loadMetamodel(final File file) {			
		return shellDocument.modify(new IUnitOfWork<Boolean, XtextResource>() {

			@Override
			public Boolean exec(XtextResource underlyingResource) throws Exception {				
				final ShellModel shell = (ShellModel) underlyingResource.getContents().get(0);					
				
				if(file.getName().toLowerCase().endsWith("xsd")){
					XSD2Ecore transformation1 = new XSD2Ecore(file.getAbsolutePath());
					EObject ecore = transformation1.execute();
					
					if(ecore != null){
						Ecore2MetaModel importOp = new Ecore2MetaModel(ecore, new WMetamodelModel(metamodelResource));
						importOp.execute();
					}
				}
				else if(file.getName().toLowerCase().endsWith("ecore") || file.getName().toLowerCase().endsWith("diagram")){
					Ecore2MetaModel importOp = new Ecore2MetaModel(file, new WMetamodelModel(metamodelResource));
					importOp.execute();
				}
				
				metamodelResource.save(null);
				mmFile.refreshLocal(1, null);					
				// changeTrace = importOp.getChangeTrace();
				updateAssistance();
				return true;
			}						
		});		
	}
		
	
	public void setMetamodel(WMetamodelModel mm){
		this.metamodelResource = mm.getResource();
	}
	
	public void updateShell(final Collection<Fragment> fragments) {		
		shellDocument.modify(new IUnitOfWork<Boolean, XtextResource>() {
			@Override
			public Boolean exec(XtextResource shellResource) throws Exception {
				ShellModel shell = (ShellModel) shellResource.getContents().get(0);
				if(shell.getName() == null) shell.setName("importedFragment");			
				
				for (Fragment fragment : fragments) {
					FragmentCommand command = ShellFactory.eINSTANCE.createFragmentCommand();
					command.setFragment(fragment);
					shell.getCommands().add(command);										
				}
				
				return true;
			}
		});		
	}
	
	public void setDocument(XtextDocument doc) {
		this.shellDocument = doc;
	}

	public void setHistoryDocument(XtextDocument doc) {
		this.historyDocument = doc;
	}	


	// TODO: Do the other way around, applying bunch of related commands (e.g., all fragment commands in a row), then a refactoring, then a fragment, etc.
	private FragmentModel extractFragmentModel(EObject eObject) {
		ShellModel shell    = (ShellModel) eObject;
		FragmentModel model = FragmentsFactory.eINSTANCE.createFragmentModel(); 
		model.setName(shell.getName());
			
		model.getFragments().addAll( (Collection<? extends Fragment>) EcoreUtil.copyAll( extractFragmentsFromCommands(shell)) );
		
		return model;
	}
	
	public List<Fragment> getCurrentFragmentModel() {	
		final Injector injector = FragmentsActivator.getInstance().getInjector(FragmentsActivator.METABUP_FRAGMENTS);
		
		XtextResourceSet resourceSet = (XtextResourceSet) injector.getInstance(XtextResourceSetProvider.class).get(shellFile.getProject());
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		Resource resource = resourceSet.getResource(URI.createURI(shellFile.getLocationURI().toString()),true);
		ShellModelImpl smi = (ShellModelImpl) resource.getContents().get(0);
			
		if(smi == null ) return null;
			
		List<Fragment> fragments = new ArrayList<Fragment>();
			
		for(Command c : smi.getCommands()){
			if (c instanceof FragmentCommand){
				FragmentCommand fc = (FragmentCommand) c;
				fragments.add(fc.getFragment());
			}
		}			
			
		return fragments;
	}
	
	private List<Fragment> extractFragmentsFromCommands(ShellModel shell) {
		LinkedList<Fragment> result = new LinkedList<Fragment>();
		EList<Command> commands = shell.getCommands();
		for (Command command : commands) {
			if ( command instanceof FragmentCommand ) {
				result.add( ((FragmentCommand) command).getFragment() );					
			}
		}
		return result;
		
	}

	/**
	 * Creates a copy of the current meta-model to be used as a platform-specific
	 * metamodel. The new meta-model is given an id.
	 * @param id The id of the new metamodel
	 */
	public void deriveCompiledMetamodel(String id) {
		//EcoreUtil.Copier copier = new EcoreUtil.Copier();
		//copier.c
		//EcoreUtil2.clone(target, source)
		MetaModel newRoot = EcoreUtil.copy(getMetamodelRoot());
		Resource resource = new ResourceImpl();
		resource.getContents().add(newRoot);
		this.deriveMetamodels.put(id, resource);
	}

	public WMetamodelModel getDerivedMetamodel(String id) {
		return new WMetamodelModel(this.deriveMetamodels.get(id));
	}

	public void updateMetamodelModel(IUpdateWork<WMetamodelModel> work) {
		work.update(getMetamodelModel());
		
		try {
			metamodelResource.save(null);
			mmFile.refreshLocal(1, null);		
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (CoreException e) {
			throw new RuntimeException(e);
		}
	}

	public WEcoreModel createEcoreMetamodel() {
		return new WEcoreModel(
				new EcoreResourceFactoryImpl().createResource(
						URI.createURI( mmFile.getFullPath().removeFileExtension().addFileExtension("ecore").toString() ))
			);
	}

	public IProject getProject() {
		return project;
	}

	private void setProject(IProject project) {
		this.project = project;
	}

}
