/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.ui.views;

import java.util.ArrayList;
import java.util.List;

import metabest.Activator;
import metabest.validation.FragmentAssertionEvaluationOutput;
import metabest.validation.MetamodelAssertionEvaluationOutput;
import metabest.validation.ValidateFragment;
import metabup.metamodel.MetaModel;
import metabup.ui.editor.editors.MetamodelInteractiveEditor;
import metamodeltest.AssertionTest;
import test.Assertion;
import test.AssertionSet;
import test.TestableFragment;
import test.impl.TestModelImpl;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.ViewPart;

public class FragmentAssertionEvaluationView extends ViewPart {
	  public static final String ID = "metabest.ui.views.FragmentAssertionEvaluationView";
	  private TreeViewer viewer;

	  private static Resource modelResource;
	  private static String metamodelUri;
		
		private TreeParent invisibleRoot;
			
		class TreeObject implements IAdaptable {
			protected String name;
			protected Object object;
			private TreeParent parent;
			
			public TreeObject(Object o) {
				this.object = o;
				this.name = o.toString();
			}
			public String getName() {
				return name;
			}		
			public Object getObject(){
				return object;
			}
			public void setParent(TreeParent parent) {
				this.parent = parent;
			}
			public TreeParent getParent() {
				return parent;
			}
			public String toString() {
				return getName();
			}
			public Object getAdapter(Class key) {
				return null;
			}
		}
		
		class TreeParent extends TreeObject {			
			private ArrayList children;
			public TreeParent(Object obj) {
				super(obj);
				children = new ArrayList();
			}
			public void addChild(TreeObject child) {
				children.add(child);
				child.setParent(this);
			}
			public void removeChild(TreeObject child) {
				children.remove(child);
				child.setParent(null);
			}
			public TreeObject [] getChildren() {
				return (TreeObject [])children.toArray(new TreeObject[children.size()]);
			}
			public boolean hasChildren() {
				return children.size()>0;
			}
		}
		
		class FragmentTreeParent extends TreeParent {
			int validChildren = 0;
			public FragmentTreeParent(Object obj) {
				super(obj);
			}
			
			public void addChild(TreeObject child){
				if(child.getObject() instanceof FragmentAssertionEvaluationOutput){
					super.addChild(child);
					if(((FragmentAssertionEvaluationOutput)child.getObject()).isTrue()) validChildren++;
					super.name = object.toString() + " (" + round(new Double(((float)validChildren/this.getChildren().length)*100)) + "%) ";
				}else super.addChild(child);
			}
			
			private double round(double num){
				return Math.rint(num*100)/100;
			}
		}
		
		class ViewContentProvider implements IStructuredContentProvider, ITreeContentProvider{
			public void inputChanged(Viewer v, Object oldInput, Object newInput) {
			}
			public void dispose() {
			}
			public Object[] getElements(Object parent) {
				if (parent.equals(getViewSite())) {
					if (invisibleRoot!=null) refresh(modelResource, metamodelUri);
					return getChildren(invisibleRoot);
				}
				return getChildren(parent);
			}
			
			public Object getParent(Object child) {
				if (child instanceof TreeObject) {
					return ((TreeObject)child).getParent();
				}
				return null;
			}
			public Object [] getChildren(Object parent) {
				if (parent instanceof TreeParent) {
					return ((TreeParent)parent).getChildren();
				}
				return new Object[0];
			}
			public boolean hasChildren(Object parent) {
				if (parent instanceof TreeParent)
					return ((TreeParent)parent).hasChildren();
				return false;
			}				
		}
		
		class ViewLabelProvider extends LabelProvider {
			public String getText(Object obj) {
				return obj.toString();
			}
			
			public Image getImage(Object obj) {
				ImageDescriptor desc;
				
				if(((TreeObject)obj).getObject() instanceof FragmentAssertionEvaluationOutput){
					FragmentAssertionEvaluationOutput faeo = (FragmentAssertionEvaluationOutput)((TreeObject)obj).getObject();				
					
					if(!faeo.isTrue()){
						desc = Activator.getImageDescriptor("icons/signed_no.gif");				
						if ( desc != null ) return desc.createImage();
					}
					else{
						desc = Activator.getImageDescriptor("icons/signed_yes.gif");				
						if ( desc != null ) return desc.createImage();
					}
				}else{
					if(((TreeObject) obj).getObject() instanceof TestableFragment){
						TreeParent tf = ((TreeParent)obj);
						
						boolean valid = true;
						
						for(int i = 0; i<tf.getChildren().length && valid; i++){
							TreeObject to = tf.getChildren()[i];
							
							if(to.getObject() instanceof FragmentAssertionEvaluationOutput){
								FragmentAssertionEvaluationOutput faeo = (FragmentAssertionEvaluationOutput)to.getObject();
								if(!faeo.isTrue()) valid = false;
							}
						}
						
						if(valid) desc = Activator.getImageDescriptor("icons/testok.png");
						else desc = Activator.getImageDescriptor("icons/testfail.png");								
						if ( desc != null ) return desc.createImage();
					}else{
						if(((TreeObject) obj).getObject() instanceof String){
							desc = Activator.getImageDescriptor("icons/info.png");
							if ( desc != null ) return desc.createImage();
						}
					}
				}
				return null;
			}
		}
		
		class NameSorter extends ViewerSorter { }	

		public FragmentAssertionEvaluationView() {
			// TODO Auto-generated constructor stub
		}
		
		private List<TestableFragment> listTestableFragments(Resource resource){
			TestModelImpl tmi = (TestModelImpl) resource.getContents().get(0);
				
			if(tmi == null ) return null;		
			return tmi.getTestableFragments();
		}
		
		public void refresh(Resource modelResource, String metamodelUri){
			if(modelResource == null) return;
			this.modelResource = modelResource;
			this.metamodelUri = metamodelUri;
			
			/* MetaModel path calculation */ 
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
		    IPath p = new Path(metamodelUri.replace(".mbup", ".mm.xmi"));
		    IFile mmFile = workspace.getRoot().getFile(p);
		    if(!mmFile.exists()) return;
		    		
		    ResourceSet rs = new ResourceSetImpl();
		    org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(mmFile.getLocationURI().toString());
			Resource r = rs.getResource(uri, true);
			org.eclipse.emf.common.util.TreeIterator<EObject> it = r.getAllContents();
			
			MetaModel mm = null;
			
			while(it.hasNext() && mm == null){
				EObject eo = it.next();
				if(eo instanceof MetaModel){
					mm = (MetaModel)eo;
				}
			}		    
			
			if(mm == null) return;
			
			List<FragmentTreeParent> fragments = new ArrayList<FragmentTreeParent>();
			
			for(TestableFragment tf : this.listTestableFragments(modelResource)){
				FragmentTreeParent tftp = new FragmentTreeParent(tf);				
				
				if(tf.getAssertionSet() != null && tf.getAssertionSet() instanceof AssertionSet){
					AssertionSet assSet = (AssertionSet)tf.getAssertionSet();
					
					for(Assertion a : assSet.getAssertions()){
						FragmentAssertionEvaluationOutput ev = new FragmentAssertionEvaluationOutput(a);					
						ev.evaluate(mm);
						TreeParent evtp = new TreeParent(ev);
						evtp.addChild(new TreeObject(ev.getRationale()));
						tftp.addChild(evtp);
					}
					
					ValidateFragment vf = new ValidateFragment(EcoreUtil.copy(tf));
					
					if(!vf.validate(mm)){
						TreeParent moreTp = new TreeParent("More errors found");						
						for(String error : vf.getExplanation()) moreTp.addChild(new TreeObject(error));						
						tftp.addChild(moreTp);
					}
					
					fragments.add(tftp);					
				}				
			}			
			
			invisibleRoot = new TreeParent("");
			for(TreeParent t : fragments) invisibleRoot.addChild(t);
			viewer.refresh();
			viewer.expandToLevel(2);
			
			return;
		}
		
		private void outlineOutput(){
			  ISelection selection = viewer.getSelection();
			  Object obj = ((IStructuredSelection)selection).getFirstElement();
			  
			  TreeObject to = (TreeObject)obj;		  
			  while(!(to.getObject() instanceof MetamodelAssertionEvaluationOutput) && to != null) to = to.getParent();		  
			  if(to == null) return;
			  
			  MetamodelAssertionEvaluationOutput maeo = (MetamodelAssertionEvaluationOutput)to.getObject();
			  
			  TreeParent tp = to.getParent();		  
			  AssertionTest at = (AssertionTest)tp.getObject();
			  			  
			  IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			  IResource resource = root.findMember(new Path(at.getMetamodelURI()));
				
			  IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			  
			  try {
				  IDE.openEditor(page, (IFile)resource, true);
				  
				  if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor() instanceof MetamodelInteractiveEditor){
					  MetamodelInteractiveEditor editor = (MetamodelInteractiveEditor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();					
					  IWorkspace workspace = ResourcesPlugin.getWorkspace();
					  IPath p = new Path(at.getMetamodelURI().replace(".mbup", ".mm.xmi"));
					  IFile mmFile = workspace.getRoot().getFile(p);
					  if(!mmFile.exists()) return;
					    		
					  ResourceSet rs = new ResourceSetImpl();
					  org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(mmFile.getLocationURI().toString());
					  Resource r = rs.getResource(uri, true);
					  MetaModel mm = (MetaModel)r.getContents().get(0);
					  if(maeo.isWarning()) editor.addAssertionViewer(mm, maeo.getMatches(), maeo.getErrors(), null);
					  else editor.addAssertionViewer(mm, maeo.getMatches(), null, maeo.getErrors());
				  }

			  } catch (PartInitException e) {
				  return;
			  }
		}
		
		/**
		 * This is a callback that will allow us
		 * to create the viewer and initialize it.
		 */
		public void createPartControl(Composite parent) {					
			FormToolkit toolkit = new FormToolkit(parent.getDisplay());
			
			// Creating the Screen
			Section section = toolkit.createSection(parent, Section.DESCRIPTION | Section.TITLE_BAR);
			section.setText("Test Fragment Evaluation"); //$NON-NLS-1$
			//section.setDescription("MetaModel validation output");
			
			// Composite for storing the data
			Composite client = toolkit.createComposite(section, SWT.WRAP);
			GridLayout layout = new GridLayout();
			layout.numColumns = 1;
			layout.marginWidth = 2;
			layout.marginHeight = 2;
			client.setLayout(layout);		  		    						
			
			Tree t = toolkit.createTree(client, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);    						
			    
			GridData gd = new GridData(GridData.FILL_BOTH);
			gd.heightHint = 50;
			gd.widthHint = 50;
			t.setLayoutData(gd);		    		    		    		  		    		    		    
			toolkit.paintBordersFor(client);
			    
			section.setClient(client);
			viewer = new TreeViewer(t);
			viewer.setContentProvider(new ViewContentProvider());
			viewer.setLabelProvider(new ViewLabelProvider());
			//viewer.setSorter(new NameSorter());
			viewer.setInput(getViewSite());			

			// Create the help context id for the viewer's control
			PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(), "metabup.perspective.viewer");
			//makeActions();
			hookContextMenu();
			hookDoubleClickAction();
			contributeToActionBars();
				 
			viewer.setInput(getViewSite());
		}
		
		private void hookContextMenu() {
			MenuManager menuMgr = new MenuManager("#PopupMenu");
			menuMgr.setRemoveAllWhenShown(true);
			menuMgr.addMenuListener(new IMenuListener() {
				public void menuAboutToShow(IMenuManager manager) {
					FragmentAssertionEvaluationView.this.fillContextMenu(manager);
				}
			});
			Menu menu = menuMgr.createContextMenu(viewer.getControl());
			viewer.getControl().setMenu(menu);
			getSite().registerContextMenu(menuMgr, viewer);
		}

		private void contributeToActionBars() {
			IActionBars bars = getViewSite().getActionBars();
			fillLocalPullDown(bars.getMenuManager());
			fillLocalToolBar(bars.getToolBarManager());
		}

		private void fillLocalPullDown(IMenuManager manager) {
			//manager.add(outlineMatchesAction);
			//manager.add(new Separator());
			//manager.add(action2);
		}

		private void fillContextMenu(IMenuManager manager) {
			//manager.add(outlineMatchesAction);
			//manager.add(action2);
			// Other plug-ins can contribute there actions here
			//manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		}
		
		private void fillLocalToolBar(IToolBarManager manager) {
			//manager.add(outlineMatchesAction);
			//manager.add(action2);
		}
		
		private void hookDoubleClickAction() {
			/*viewer.addDoubleClickListener(new IDoubleClickListener() {
				public void doubleClick(DoubleClickEvent event) {
					outlineMatchesAction.run();
				}
			});*/
		}
			
		private void showMessage(String message) {
			MessageDialog.openInformation(
				viewer.getControl().getShell(),
				"Validation View",
				message);
		}
			
		/**
		 * Passing the focus request to the viewer's control.
		 */
		public void setFocus() {
			viewer.getControl().setFocus();
		}
			
	}