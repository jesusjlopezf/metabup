/****************************************************************************
 * This demo file is part of the yFiles extension package yExport-1.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * yExport demo files exhibit yExport functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a yExport version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own yExport powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the yExport
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.export;


import javax.swing.JButton;
import javax.swing.JToolBar;

import y.base.Node;
import y.io.LinkInfo;
import y.io.LinkMap;
import y.option.OptionHandler;
import y.view.EditMode;
import y.view.HitInfo;
import y.view.ViewMode;

import yext.export.io.SWFOutputHandler;


/**
 * Demonstrates how to export a graph to SWF format
 * and how to add hyperlinks to the exported swf. 
 */
public class SWFExportDemo extends AbstractExportDemo
{
  private static final String SWF_URL = "swf_url";
  private static final String SWF_TARGET = "swf_target";

  //Map used to store URLs for nodes, edges and labels.
  LinkMap linkMap;


  public SWFExportDemo() {
    //add right click handler
    view.addViewMode(new RightClickMode());
  }

  /**
   * Creates a toolbar for this demo.
   */
  protected JToolBar createToolBar() {
    JToolBar jtb = super.createToolBar();
    jtb.addSeparator();
    // the link map will be used for storing hyperlinks for graph items
    // and will be queried for hyperlinks by the output handler
    linkMap = new LinkMap();
    jtb.add(new JButton(new ExportAction("SWF Export", new SWFOutputHandler(linkMap))));
    return jtb;
  }

  /**
   * Create an edit mode that displays the URL
   * of a node as tool tip.
   */
  protected EditMode createEditMode()
  {
    EditMode em = new EditMode() {
      public String getNodeTip(Node v) {
        String url = null;
        LinkInfo info = linkMap.get(v);
        if( null != info ) {
          url = info.getAttribute(SWF_URL);
        }
        if(url != null)
        {
          return "<html>URL: <b>" + url + "</b></html>";
        }
        else
        {
          return "<html>No URL assigned to node. <br><b>Right click on node</b></html>";
        }
      }
    };
    em.showNodeTips(true);
    return em;
  }


  /**
   * ViewMode responsible for processing a right mouse click.
   * Right clicking on nodes, edges amd labels will bring up
   * an option handler that allows to enter a URL for each item.
   * Hyperlinks to these URLs will be integrated in the SVG output.
   */
  class RightClickMode extends ViewMode {
    public void mouseReleasedRight(double x, double y)
    {
      HitInfo hitInfo = getGraph2D().getHitInfo(x,y, false);

      if(hitInfo.getHitNodeLabel() != null)
      {
        editLinkInfo(hitInfo.getHitNodeLabel(), "node label");
      }
      else if(hitInfo.getHitEdgeLabel() != null)
      {
        editLinkInfo(hitInfo.getHitEdgeLabel(), "edge label");
      }
      else if(hitInfo.getHitNode() != null)
      {
        editLinkInfo(hitInfo.getHitNode(), "node");
      }
      else if(hitInfo.getHitEdge() != null)
      {
        editLinkInfo(hitInfo.getHitEdge(), "edge");
      }

    }

    void editLinkInfo(Object key, String type)
    {
      String url = null;
      OptionHandler op = new OptionHandler("Hyperlink for " + type);
      LinkInfo info = linkMap.get(key);
      if( null != info ) {
        url = info.getAttribute(SWF_URL);
      }
      if(url != null)
      {
        op.addString("Hyperlink URL",url);
      }
      else
      {
        op.addString("Hyperlink URL","");
      }
      if(op.showEditor(view.getFrame()))
      {
        url = op.getString("Hyperlink URL");
        if(url.length() > 0)
        {
          LinkInfo newInfo = new LinkInfo();
          newInfo.setAttribute(SWF_URL,url);
          newInfo.setAttribute(SWF_TARGET,"_self");
          linkMap.put(key, newInfo);
        }
        else
        {
          linkMap.remove(key);
        }
      }
    }
  }

  /**
   * Launches this demo.
   */
  public static void main(String[] args) {
    initLnF();
    (new SWFExportDemo()).start();
  }
}