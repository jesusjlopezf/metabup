/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.pages;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import metabup.shell.impl.ShellModelImpl;
import metabup.ui.internal.FragmentsActivator;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.ui.resource.XtextResourceSetProvider;

import test.TestModel;

import com.google.inject.Injector;

import fragments.Fragment;


/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one (mbup).
 */

public class GenerateValidExampleWizardPage extends WizardPage {

	private Text fileText;
	private ISelection selection;
	private Fragment baseFragment;
	private boolean generateFragment = true;
	private boolean generateGraphML = true;
	
	private Text fragmentLegendText;
	
	private Button b1, b2;
	
	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param pageName
	 */
	public GenerateValidExampleWizardPage(ISelection selection, IProject project) {
		super("wizardPage");
		setTitle("Select a base fragment");
		setDescription("Please select a base fragment from which metaBup shall generate a valid meta-model example");
		this.selection = selection;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		final Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		
		Label label = new Label(container, SWT.NULL);
		label.setText("&Fragment file:");

		fileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		fileText.setLayoutData(gd);
		fileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		
		Button button = new Button(container, SWT.PUSH);
		button.setText("Browse...");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					handleBrowse();
				} catch (CoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		Group g = new Group(container, 0);
	    //g.setSize(400, 100);
	    g.setText("Generate:");
	    
	    b1 = new Button(g, SWT.CHECK);
	    b2 = new Button(g, SWT.CHECK);
	    
	    b1.setBounds(10, 20, 300, 25);	    
	    b1.setText("MBF fragment");
	    b1.setSelection(generateFragment); // default selection

	    b2.setBounds(10, 45, 300, 25);
	    b2.setText("GraphML fragment representation");	 
	    b2.setSelection(generateGraphML); // default selection
		
	    label = new Label(container, SWT.NULL);
		label.setText("");
		label = new Label(container, SWT.NULL);
		label.setText("");
		
		b1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){				
				generateFragment = getB1Selection(); 
			}
		});
		
		b2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){				
				generateGraphML = getB2Selection(); 
			}
		});
		
		label = new Label(container, SWT.NULL);
		label.setText("Legend file:");
	    
	    fragmentLegendText = new Text(container, SWT.BORDER | SWT.SINGLE);
		fragmentLegendText.setLayoutData(gd);		
		fragmentLegendText.setEnabled(true);
				
		final Button button4 = new Button(container, SWT.PUSH);
		button4.setText("Browse...");
		button4.setEnabled(true);
		button4.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				handleFragmentLegendBrowse(container);
			}
		});
		
		initialize();
		dialogChanged();
		setControl(container);
	}
	
	private boolean getB1Selection(){
		return b1.getSelection();
	}
	
	private boolean getB2Selection(){
		return b2.getSelection();
	}
	
	protected void handleFragmentLegendBrowse(Composite container) {
		FileDialog dialog = new FileDialog(container.getShell());
		dialog.setText("Select a metabup.sketches.legend file...");
		dialog.setFilterExtensions(new String[] { "*.graphml" } );
		String fileName = dialog.open();
		
		if(fileName != null){
			Path path = new Path(fileName);
			fragmentLegendText.setText(path.toOSString());
		}			
	}
	
	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */

	private void initialize() {
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			
			if (obj instanceof IFile)
				if(((IFile)obj).getFullPath().toOSString().endsWith(".mbf"))
					fileText.setText(((IFile)obj).getFullPath().toOSString());
		}
		fileText.setText("");
	}

	/**
	 * Uses the standard container selection dialog to choose the new value for
	 * the container field.
	 * @throws CoreException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */

	private void handleBrowse() throws CoreException, FileNotFoundException, IOException {
		/*System.out.println("PROJECT: " + project.getName());
		IResource files[] = new IResource[project.members().length];
		int i = 0;
		
		for(IResource ir : project.members()){
			if(ir instanceof IFile)
				if(ir.getFileExtension().equals("mbf")){
					files[i] = ir;
					i++;
					System.out.println("adding... " + files[i-1].getName());
				}
					
		}
		
		if(files.length > 0){		
			ResourceListSelectionDialog dialog = new ResourceListSelectionDialog(null, files);
			dialog.setTitle("Resource Selection");
			
			if(dialog.open() == ResourceListSelectionDialog.OK){
				if(dialog.getResult() != null){
					String s = ((IFile)(dialog.getResult()[0])).getFullPath().toOSString();
					fileText.setText(s);
				}
			}
		}*/
		
		FileDialog dialog = new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		String str = dialog.open();
		
		if(str != null){
			fileText.setText(str);
			
			File file = new File(fileText.getText());
			IWorkspace workspace= ResourcesPlugin.getWorkspace();    
			IPath location= Path.fromOSString(file.getAbsolutePath()); 
			IFile ifile= workspace.getRoot().getFileForLocation(location);
			
			if(ifile.exists()){
				final Injector injector = FragmentsActivator.getInstance().getInjector(FragmentsActivator.METABUP_FRAGMENTS);
				
				XtextResourceSet resourceSet = (XtextResourceSet) injector.getInstance(XtextResourceSetProvider.class).get(ifile.getProject());
				resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
				Resource resource = resourceSet.getResource(URI.createURI(ifile.getLocationURI().toString()),true);
			    	    
				TestModel tm = (TestModel)resource.getContents().get(0);
				baseFragment = tm.getTestableFragments().get(0); 
			}
		}
	}

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {			
		String fileName = getFileName();
		
		if (fileName.length() == 0) {
			//updateStatus("File name must be specified");
			baseFragment = null;
			return;
		}
		/*if (fileName.replace('\\', '/').indexOf('/', 1) > 0) {
			updateStatus("File name must be valid");
			return;
		}*/
		int dotLoc = fileName.lastIndexOf('.');
		if (dotLoc != -1) {
			String ext = fileName.substring(dotLoc + 1);
			if (ext.equalsIgnoreCase("mbf") == false) {
				updateStatus("File extension must be \"mbf\"");
				return;
			}
		}
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	public String getFileName() {
		return fileText.getText();
	}
	
	public Fragment getBaseFragment(){
		return baseFragment;
	}
	
	public boolean generateMBF(){
		return this.generateFragment;
	}
	
	public boolean generateGraphML(){
		return this.generateGraphML;
	}
	
	public String getLegendFile(){
		return fragmentLegendText.getText();
	}
}