/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.Parameter#getParameterDefinition <em>Parameter Definition</em>}</li>
 *   <li>{@link metamodeltest.Parameter#getParameterReuse <em>Parameter Reuse</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getParameter()
 * @model abstract="true"
 * @generated
 */
public interface Parameter extends EObject {
	/**
	 * Returns the value of the '<em><b>Parameter Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Definition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Definition</em>' containment reference.
	 * @see #setParameterDefinition(UserDefinedParameter)
	 * @see metamodeltest.MetamodeltestPackage#getParameter_ParameterDefinition()
	 * @model containment="true"
	 * @generated
	 */
	UserDefinedParameter getParameterDefinition();

	/**
	 * Sets the value of the '{@link metamodeltest.Parameter#getParameterDefinition <em>Parameter Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Definition</em>' containment reference.
	 * @see #getParameterDefinition()
	 * @generated
	 */
	void setParameterDefinition(UserDefinedParameter value);

	/**
	 * Returns the value of the '<em><b>Parameter Reuse</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Reuse</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Reuse</em>' reference.
	 * @see #setParameterReuse(UserDefinedParameter)
	 * @see metamodeltest.MetamodeltestPackage#getParameter_ParameterReuse()
	 * @model
	 * @generated
	 */
	UserDefinedParameter getParameterReuse();

	/**
	 * Sets the value of the '{@link metamodeltest.Parameter#getParameterReuse <em>Parameter Reuse</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Reuse</em>' reference.
	 * @see #getParameterReuse()
	 * @generated
	 */
	void setParameterReuse(UserDefinedParameter value);

} // Parameter
