/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.design;

import metabup.annotations.extensionpoint.DesignAnnotation;

public class Composition extends DesignAnnotation {
	public final static String NAME = "composition";
	
	public Composition (){
		this.setName(NAME);
		this.setDescription("It marks up a reference as composition");
	}
}
