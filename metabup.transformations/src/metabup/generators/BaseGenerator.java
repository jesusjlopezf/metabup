/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.generators;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.eclipse.emf.common.util.URI;

import metabup.transformations.models.BaseModel;

public abstract class BaseGenerator {
	protected BaseModel 	model;		// model from which we will generate code
	protected String	  	targetUri;	// URI where we want to generate the meta-model
	protected Properties	config;		// Configuration...
	protected String		configPath;
	
	public BaseGenerator (BaseModel m, String targetUri, String propFile) {
		this.model = m;
		this.targetUri = targetUri;
		this.config = new Properties();
		this.configPath = propFile;
		FileReader fr;
		try {
			fr = new FileReader(propFile);
			this.config.load(fr);
		} catch (FileNotFoundException e) {			
		} catch (IOException e) {						
		}		
	}
	
	public abstract void generate();
	
	/*
	 * These three static methods are in metabup.util.Util, but I get a cyclic dependency between projects
	 */
	
	public static URI createURI(String path) {		
		return createURI(path, null);
	}
	
	public static URI createURI(String path, String cwd) {
		String uriString = path;
		if ( uriString.startsWith("platform:/resource/") ) {
			//this option depends on org.eclipse.resources
			//return URI.createPlatformResourceURI(ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(uriString)), true);
			return URI.createPlatformResourceURI(extract(uriString, "platform:/resource/"), true);
		}
		else if ( uriString.startsWith("platform:/plugin/") ) {
			return URI.createPlatformPluginURI(extract(uriString, "platform:/plugin/"), true);
		}
		else if ( uriString.startsWith("http:/") ) { // to allow loading http:/www.eclipse.org/emf/2002/Ecore, but this is not the general case
			return URI.createURI(path);
		}
	
		if ( cwd == null ) {
			return URI.createURI(path);
		} else {
			URI uri = URI.createFileURI(new File(path).getAbsolutePath());
			return uri;
		}
		//return URI.createURI(path).resolve(cwdURI(cwd));
	}
	
	private static String extract(String s, String extract) {
		return s.replaceFirst("^" + extract, "");
	}	
}
