/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complementary Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.ComplementaryBlock#isAtLeast <em>At Least</em>}</li>
 *   <li>{@link test.ComplementaryBlock#getDescription <em>Description</em>}</li>
 * </ul>
 *
 * @see test.TestPackage#getComplementaryBlock()
 * @model abstract="true"
 * @generated
 */
public interface ComplementaryBlock extends EObject {
	/**
	 * Returns the value of the '<em><b>At Least</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>At Least</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>At Least</em>' attribute.
	 * @see #setAtLeast(boolean)
	 * @see test.TestPackage#getComplementaryBlock_AtLeast()
	 * @model
	 * @generated
	 */
	boolean isAtLeast();

	/**
	 * Sets the value of the '{@link test.ComplementaryBlock#isAtLeast <em>At Least</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>At Least</em>' attribute.
	 * @see #isAtLeast()
	 * @generated
	 */
	void setAtLeast(boolean value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see test.TestPackage#getComplementaryBlock_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link test.ComplementaryBlock#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

} // ComplementaryBlock
