package metabup.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import metabup.services.MetamodelingShellGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMetamodelingShellParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'shell'", "'fragment'", "'['", "']'", "'{'", "'}'", "':'", "';'", "'@'", "'('", "','", "')'", "'->'", "'.'", "'='", "'attr'", "'ref'", "'rel'", "'style'", "'color'", "'width'", "'line'", "'source'", "'target'", "'true'", "'false'", "'positive'", "'negative'", "'dashed'", "'dashed-dotted'", "'dotted'", "'circle'", "'concave'", "'convex'", "'crows-foot-many'", "'crows-foot-many-mandatory'", "'crows-foot-many-optional'", "'crows-foot-one'", "'crows-foot-one-mandatory'", "'crows-foot-one-optional'", "'dash'", "'delta'", "'diamond'", "'none'", "'plain'", "'short'", "'skewed-dash'", "'standard'", "'transparent-circle'", "'t-shape'", "'white-delta'", "'white-diamond'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__62=62;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalMetamodelingShellParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMetamodelingShellParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMetamodelingShellParser.tokenNames; }
    public String getGrammarFileName() { return "../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g"; }



     	private MetamodelingShellGrammarAccess grammarAccess;
     	
        public InternalMetamodelingShellParser(TokenStream input, MetamodelingShellGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "ShellModel";	
       	}
       	
       	@Override
       	protected MetamodelingShellGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleShellModel"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:68:1: entryRuleShellModel returns [EObject current=null] : iv_ruleShellModel= ruleShellModel EOF ;
    public final EObject entryRuleShellModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleShellModel = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:69:2: (iv_ruleShellModel= ruleShellModel EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:70:2: iv_ruleShellModel= ruleShellModel EOF
            {
             newCompositeNode(grammarAccess.getShellModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleShellModel_in_entryRuleShellModel75);
            iv_ruleShellModel=ruleShellModel();

            state._fsp--;

             current =iv_ruleShellModel; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleShellModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleShellModel"


    // $ANTLR start "ruleShellModel"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:77:1: ruleShellModel returns [EObject current=null] : ( () otherlv_1= 'shell' ( (lv_name_2_0= ruleEString ) ) ( (lv_commands_3_0= ruleCommand ) )* ) ;
    public final EObject ruleShellModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_commands_3_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:80:28: ( ( () otherlv_1= 'shell' ( (lv_name_2_0= ruleEString ) ) ( (lv_commands_3_0= ruleCommand ) )* ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:81:1: ( () otherlv_1= 'shell' ( (lv_name_2_0= ruleEString ) ) ( (lv_commands_3_0= ruleCommand ) )* )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:81:1: ( () otherlv_1= 'shell' ( (lv_name_2_0= ruleEString ) ) ( (lv_commands_3_0= ruleCommand ) )* )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:81:2: () otherlv_1= 'shell' ( (lv_name_2_0= ruleEString ) ) ( (lv_commands_3_0= ruleCommand ) )*
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:81:2: ()
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:82:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getShellModelAccess().getShellModelAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleShellModel131); 

                	newLeafNode(otherlv_1, grammarAccess.getShellModelAccess().getShellKeyword_1());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:91:1: ( (lv_name_2_0= ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:92:1: (lv_name_2_0= ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:92:1: (lv_name_2_0= ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:93:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getShellModelAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleShellModel152);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getShellModelRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:109:2: ( (lv_commands_3_0= ruleCommand ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12||LA1_0==19||(LA1_0>=37 && LA1_0<=38)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:110:1: (lv_commands_3_0= ruleCommand )
            	    {
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:110:1: (lv_commands_3_0= ruleCommand )
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:111:3: lv_commands_3_0= ruleCommand
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getShellModelAccess().getCommandsCommandParserRuleCall_3_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleCommand_in_ruleShellModel173);
            	    lv_commands_3_0=ruleCommand();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getShellModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"commands",
            	            		lv_commands_3_0, 
            	            		"Command");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleShellModel"


    // $ANTLR start "entryRuleCommand"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:135:1: entryRuleCommand returns [EObject current=null] : iv_ruleCommand= ruleCommand EOF ;
    public final EObject entryRuleCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCommand = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:136:2: (iv_ruleCommand= ruleCommand EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:137:2: iv_ruleCommand= ruleCommand EOF
            {
             newCompositeNode(grammarAccess.getCommandRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleCommand_in_entryRuleCommand210);
            iv_ruleCommand=ruleCommand();

            state._fsp--;

             current =iv_ruleCommand; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleCommand220); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommand"


    // $ANTLR start "ruleCommand"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:144:1: ruleCommand returns [EObject current=null] : this_FragmentCommand_0= ruleFragmentCommand ;
    public final EObject ruleCommand() throws RecognitionException {
        EObject current = null;

        EObject this_FragmentCommand_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:147:28: (this_FragmentCommand_0= ruleFragmentCommand )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:149:5: this_FragmentCommand_0= ruleFragmentCommand
            {
             
                    newCompositeNode(grammarAccess.getCommandAccess().getFragmentCommandParserRuleCall()); 
                
            pushFollow(FollowSets000.FOLLOW_ruleFragmentCommand_in_ruleCommand266);
            this_FragmentCommand_0=ruleFragmentCommand();

            state._fsp--;

             
                    current = this_FragmentCommand_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommand"


    // $ANTLR start "entryRuleFragmentCommand"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:165:1: entryRuleFragmentCommand returns [EObject current=null] : iv_ruleFragmentCommand= ruleFragmentCommand EOF ;
    public final EObject entryRuleFragmentCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragmentCommand = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:166:2: (iv_ruleFragmentCommand= ruleFragmentCommand EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:167:2: iv_ruleFragmentCommand= ruleFragmentCommand EOF
            {
             newCompositeNode(grammarAccess.getFragmentCommandRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentCommand_in_entryRuleFragmentCommand300);
            iv_ruleFragmentCommand=ruleFragmentCommand();

            state._fsp--;

             current =iv_ruleFragmentCommand; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentCommand310); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragmentCommand"


    // $ANTLR start "ruleFragmentCommand"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:174:1: ruleFragmentCommand returns [EObject current=null] : ( (lv_fragment_0_0= ruleFragment ) ) ;
    public final EObject ruleFragmentCommand() throws RecognitionException {
        EObject current = null;

        EObject lv_fragment_0_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:177:28: ( ( (lv_fragment_0_0= ruleFragment ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:178:1: ( (lv_fragment_0_0= ruleFragment ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:178:1: ( (lv_fragment_0_0= ruleFragment ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:179:1: (lv_fragment_0_0= ruleFragment )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:179:1: (lv_fragment_0_0= ruleFragment )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:180:3: lv_fragment_0_0= ruleFragment
            {
             
            	        newCompositeNode(grammarAccess.getFragmentCommandAccess().getFragmentFragmentParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFragment_in_ruleFragmentCommand355);
            lv_fragment_0_0=ruleFragment();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentCommandRule());
            	        }
                   		set(
                   			current, 
                   			"fragment",
                    		lv_fragment_0_0, 
                    		"Fragment");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentCommand"


    // $ANTLR start "entryRuleFragment"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:204:1: entryRuleFragment returns [EObject current=null] : iv_ruleFragment= ruleFragment EOF ;
    public final EObject entryRuleFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragment = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:205:2: (iv_ruleFragment= ruleFragment EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:206:2: iv_ruleFragment= ruleFragment EOF
            {
             newCompositeNode(grammarAccess.getFragmentRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragment_in_entryRuleFragment390);
            iv_ruleFragment=ruleFragment();

            state._fsp--;

             current =iv_ruleFragment; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragment400); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragment"


    // $ANTLR start "ruleFragment"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:213:1: ruleFragment returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}' ) ;
    public final EObject ruleFragment() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_12=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        Enumerator lv_ftype_3_0 = null;

        AntlrDatatypeRuleToken lv_type_6_0 = null;

        AntlrDatatypeRuleToken lv_name_8_0 = null;

        EObject lv_objects_10_0 = null;

        EObject lv_objects_11_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:216:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}' ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:217:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}' )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:217:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}' )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:217:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}'
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:217:2: ()
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:218:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getFragmentAccess().getFragmentAction_0(),
                        current);
                

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:223:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==19) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:223:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:223:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:224:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:224:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:225:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleFragment456);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:241:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==19) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:242:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:242:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:243:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleFragment477);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:259:5: ( (lv_ftype_3_0= ruleFragmentType ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>=37 && LA4_0<=38)) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:260:1: (lv_ftype_3_0= ruleFragmentType )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:260:1: (lv_ftype_3_0= ruleFragmentType )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:261:3: lv_ftype_3_0= ruleFragmentType
                    {
                     
                    	        newCompositeNode(grammarAccess.getFragmentAccess().getFtypeFragmentTypeEnumRuleCall_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFragmentType_in_ruleFragment501);
                    lv_ftype_3_0=ruleFragmentType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
                    	        }
                           		set(
                           			current, 
                           			"ftype",
                            		lv_ftype_3_0, 
                            		"FragmentType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleFragment514); 

                	newLeafNode(otherlv_4, grammarAccess.getFragmentAccess().getFragmentKeyword_3());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:281:1: (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==13) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:281:3: otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']'
                    {
                    otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleFragment527); 

                        	newLeafNode(otherlv_5, grammarAccess.getFragmentAccess().getLeftSquareBracketKeyword_4_0());
                        
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:285:1: ( (lv_type_6_0= ruleEString ) )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:286:1: (lv_type_6_0= ruleEString )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:286:1: (lv_type_6_0= ruleEString )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:287:3: lv_type_6_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getFragmentAccess().getTypeEStringParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFragment548);
                    lv_type_6_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
                    	        }
                           		set(
                           			current, 
                           			"type",
                            		lv_type_6_0, 
                            		"EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_7=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleFragment560); 

                        	newLeafNode(otherlv_7, grammarAccess.getFragmentAccess().getRightSquareBracketKeyword_4_2());
                        

                    }
                    break;

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:307:3: ( (lv_name_8_0= ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:308:1: (lv_name_8_0= ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:308:1: (lv_name_8_0= ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:309:3: lv_name_8_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getFragmentAccess().getNameEStringParserRuleCall_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFragment583);
            lv_name_8_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_8_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_9=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleFragment595); 

                	newLeafNode(otherlv_9, grammarAccess.getFragmentAccess().getLeftCurlyBracketKeyword_6());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:329:1: ( (lv_objects_10_0= ruleObject ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:330:1: (lv_objects_10_0= ruleObject )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:330:1: (lv_objects_10_0= ruleObject )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:331:3: lv_objects_10_0= ruleObject
            {
             
            	        newCompositeNode(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_7_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_ruleFragment616);
            lv_objects_10_0=ruleObject();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentRule());
            	        }
                   		add(
                   			current, 
                   			"objects",
                    		lv_objects_10_0, 
                    		"Object");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:347:2: ( (lv_objects_11_0= ruleObject ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=RULE_STRING && LA6_0<=RULE_ID)||LA6_0==19) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:348:1: (lv_objects_11_0= ruleObject )
            	    {
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:348:1: (lv_objects_11_0= ruleObject )
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:349:3: lv_objects_11_0= ruleObject
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_8_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleObject_in_ruleFragment637);
            	    lv_objects_11_0=ruleObject();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"objects",
            	            		lv_objects_11_0, 
            	            		"Object");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            otherlv_12=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleFragment650); 

                	newLeafNode(otherlv_12, grammarAccess.getFragmentAccess().getRightCurlyBracketKeyword_9());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragment"


    // $ANTLR start "entryRuleFeature"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:377:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:378:2: (iv_ruleFeature= ruleFeature EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:379:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_entryRuleFeature686);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeature696); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:386:1: ruleFeature returns [EObject current=null] : (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        EObject this_Attribute_0 = null;

        EObject this_Reference_1 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:389:28: ( (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:390:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:390:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )
            int alt7=2;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:391:5: this_Attribute_0= ruleAttribute
                    {
                     
                            newCompositeNode(grammarAccess.getFeatureAccess().getAttributeParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_ruleFeature743);
                    this_Attribute_0=ruleAttribute();

                    state._fsp--;

                     
                            current = this_Attribute_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:401:5: this_Reference_1= ruleReference
                    {
                     
                            newCompositeNode(grammarAccess.getFeatureAccess().getReferenceParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleReference_in_ruleFeature770);
                    this_Reference_1=ruleReference();

                    state._fsp--;

                     
                            current = this_Reference_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleEString"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:417:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:418:2: (iv_ruleEString= ruleEString EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:419:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString806);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString817); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:426:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:429:28: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:430:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:430:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_STRING) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_ID) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:430:6: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleEString857); 

                    		current.merge(this_STRING_0);
                        
                     
                        newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:438:10: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleEString883); 

                    		current.merge(this_ID_1);
                        
                     
                        newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleObject"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:453:1: entryRuleObject returns [EObject current=null] : iv_ruleObject= ruleObject EOF ;
    public final EObject entryRuleObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObject = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:454:2: (iv_ruleObject= ruleObject EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:455:2: iv_ruleObject= ruleObject EOF
            {
             newCompositeNode(grammarAccess.getObjectRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_entryRuleObject928);
            iv_ruleObject=ruleObject();

            state._fsp--;

             current =iv_ruleObject; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObject938); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObject"


    // $ANTLR start "ruleObject"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:462:1: ruleObject returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' ) ;
    public final EObject ruleObject() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        AntlrDatatypeRuleToken lv_name_3_0 = null;

        AntlrDatatypeRuleToken lv_type_5_0 = null;

        EObject lv_features_7_0 = null;

        EObject lv_features_9_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:465:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:466:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:466:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:466:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}'
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:466:2: ()
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:467:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getObjectAccess().getObjectAction_0(),
                        current);
                

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:472:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==19) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:472:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:472:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:473:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:473:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:474:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleObject994);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:490:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==19) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:491:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:491:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:492:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleObject1015);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:508:5: ( (lv_name_3_0= ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:509:1: (lv_name_3_0= ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:509:1: (lv_name_3_0= ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:510:3: lv_name_3_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getObjectAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObject1039);
            lv_name_3_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getObjectRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_3_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleObject1051); 

                	newLeafNode(otherlv_4, grammarAccess.getObjectAccess().getColonKeyword_3());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:530:1: ( (lv_type_5_0= ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:531:1: (lv_type_5_0= ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:531:1: (lv_type_5_0= ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:532:3: lv_type_5_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getObjectAccess().getTypeEStringParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObject1072);
            lv_type_5_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getObjectRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_5_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleObject1084); 

                	newLeafNode(otherlv_6, grammarAccess.getObjectAccess().getLeftCurlyBracketKeyword_5());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:552:1: ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==19||(LA14_0>=26 && LA14_0<=28)) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:552:2: ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )*
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:552:2: ( (lv_features_7_0= ruleFeature ) )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:553:1: (lv_features_7_0= ruleFeature )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:553:1: (lv_features_7_0= ruleFeature )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:554:3: lv_features_7_0= ruleFeature
                    {
                     
                    	        newCompositeNode(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleObject1106);
                    lv_features_7_0=ruleFeature();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	        }
                           		add(
                           			current, 
                           			"features",
                            		lv_features_7_0, 
                            		"Feature");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:570:2: (otherlv_8= ';' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0==18) ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:570:4: otherlv_8= ';'
                            {
                            otherlv_8=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleObject1119); 

                                	newLeafNode(otherlv_8, grammarAccess.getObjectAccess().getSemicolonKeyword_6_1());
                                

                            }
                            break;

                    }

                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:574:3: ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==19||(LA13_0>=26 && LA13_0<=28)) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:574:4: ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )?
                    	    {
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:574:4: ( (lv_features_9_0= ruleFeature ) )
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:575:1: (lv_features_9_0= ruleFeature )
                    	    {
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:575:1: (lv_features_9_0= ruleFeature )
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:576:3: lv_features_9_0= ruleFeature
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_2_0_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleObject1143);
                    	    lv_features_9_0=ruleFeature();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"features",
                    	            		lv_features_9_0, 
                    	            		"Feature");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }

                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:592:2: (otherlv_10= ';' )?
                    	    int alt12=2;
                    	    int LA12_0 = input.LA(1);

                    	    if ( (LA12_0==18) ) {
                    	        alt12=1;
                    	    }
                    	    switch (alt12) {
                    	        case 1 :
                    	            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:592:4: otherlv_10= ';'
                    	            {
                    	            otherlv_10=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleObject1156); 

                    	                	newLeafNode(otherlv_10, grammarAccess.getObjectAccess().getSemicolonKeyword_6_2_1());
                    	                

                    	            }
                    	            break;

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_11=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleObject1174); 

                	newLeafNode(otherlv_11, grammarAccess.getObjectAccess().getRightCurlyBracketKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObject"


    // $ANTLR start "entryRuleAnnotation"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:608:1: entryRuleAnnotation returns [EObject current=null] : iv_ruleAnnotation= ruleAnnotation EOF ;
    public final EObject entryRuleAnnotation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotation = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:609:2: (iv_ruleAnnotation= ruleAnnotation EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:610:2: iv_ruleAnnotation= ruleAnnotation EOF
            {
             newCompositeNode(grammarAccess.getAnnotationRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_entryRuleAnnotation1210);
            iv_ruleAnnotation=ruleAnnotation();

            state._fsp--;

             current =iv_ruleAnnotation; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotation1220); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotation"


    // $ANTLR start "ruleAnnotation"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:617:1: ruleAnnotation returns [EObject current=null] : (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? ) ;
    public final EObject ruleAnnotation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_params_3_0 = null;

        EObject lv_params_5_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:620:28: ( (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:621:1: (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:621:1: (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:621:3: otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )?
            {
            otherlv_0=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleAnnotation1257); 

                	newLeafNode(otherlv_0, grammarAccess.getAnnotationAccess().getCommercialAtKeyword_0());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:625:1: ( (lv_name_1_0= ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:626:1: (lv_name_1_0= ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:626:1: (lv_name_1_0= ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:627:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAnnotationAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAnnotation1278);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:643:2: (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==20) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:643:4: otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')'
                    {
                    otherlv_2=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleAnnotation1291); 

                        	newLeafNode(otherlv_2, grammarAccess.getAnnotationAccess().getLeftParenthesisKeyword_2_0());
                        
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:647:1: ( (lv_params_3_0= ruleAnnotationParam ) )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:648:1: (lv_params_3_0= ruleAnnotationParam )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:648:1: (lv_params_3_0= ruleAnnotationParam )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:649:3: lv_params_3_0= ruleAnnotationParam
                    {
                     
                    	        newCompositeNode(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_ruleAnnotation1312);
                    lv_params_3_0=ruleAnnotationParam();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
                    	        }
                           		add(
                           			current, 
                           			"params",
                            		lv_params_3_0, 
                            		"AnnotationParam");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:665:2: (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==21) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:665:4: otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) )
                    	    {
                    	    otherlv_4=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleAnnotation1325); 

                    	        	newLeafNode(otherlv_4, grammarAccess.getAnnotationAccess().getCommaKeyword_2_2_0());
                    	        
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:669:1: ( (lv_params_5_0= ruleAnnotationParam ) )
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:670:1: (lv_params_5_0= ruleAnnotationParam )
                    	    {
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:670:1: (lv_params_5_0= ruleAnnotationParam )
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:671:3: lv_params_5_0= ruleAnnotationParam
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_2_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_ruleAnnotation1346);
                    	    lv_params_5_0=ruleAnnotationParam();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"params",
                    	            		lv_params_5_0, 
                    	            		"AnnotationParam");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleAnnotation1360); 

                        	newLeafNode(otherlv_6, grammarAccess.getAnnotationAccess().getRightParenthesisKeyword_2_3());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotation"


    // $ANTLR start "entryRuleAnnotationParam"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:699:1: entryRuleAnnotationParam returns [EObject current=null] : iv_ruleAnnotationParam= ruleAnnotationParam EOF ;
    public final EObject entryRuleAnnotationParam() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotationParam = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:700:2: (iv_ruleAnnotationParam= ruleAnnotationParam EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:701:2: iv_ruleAnnotationParam= ruleAnnotationParam EOF
            {
             newCompositeNode(grammarAccess.getAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam1398);
            iv_ruleAnnotationParam=ruleAnnotationParam();

            state._fsp--;

             current =iv_ruleAnnotationParam; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotationParam1408); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotationParam"


    // $ANTLR start "ruleAnnotationParam"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:708:1: ruleAnnotationParam returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) ) ;
    public final EObject ruleAnnotationParam() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;

        EObject lv_paramValue_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:711:28: ( ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:712:1: ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:712:1: ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:712:2: ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:712:2: ( (lv_name_0_0= ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:713:1: (lv_name_0_0= ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:713:1: (lv_name_0_0= ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:714:3: lv_name_0_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAnnotationParamAccess().getNameEStringParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAnnotationParam1454);
            lv_name_0_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAnnotationParamRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:730:2: ( (lv_paramValue_1_0= ruleParamValue ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:731:1: (lv_paramValue_1_0= ruleParamValue )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:731:1: (lv_paramValue_1_0= ruleParamValue )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:732:3: lv_paramValue_1_0= ruleParamValue
            {
             
            	        newCompositeNode(grammarAccess.getAnnotationParamAccess().getParamValueParamValueParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_ruleAnnotationParam1475);
            lv_paramValue_1_0=ruleParamValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAnnotationParamRule());
            	        }
                   		set(
                   			current, 
                   			"paramValue",
                    		lv_paramValue_1_0, 
                    		"ParamValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotationParam"


    // $ANTLR start "entryRuleParamValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:756:1: entryRuleParamValue returns [EObject current=null] : iv_ruleParamValue= ruleParamValue EOF ;
    public final EObject entryRuleParamValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParamValue = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:757:2: (iv_ruleParamValue= ruleParamValue EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:758:2: iv_ruleParamValue= ruleParamValue EOF
            {
             newCompositeNode(grammarAccess.getParamValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_entryRuleParamValue1511);
            iv_ruleParamValue=ruleParamValue();

            state._fsp--;

             current =iv_ruleParamValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleParamValue1521); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParamValue"


    // $ANTLR start "ruleParamValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:765:1: ruleParamValue returns [EObject current=null] : (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam ) ;
    public final EObject ruleParamValue() throws RecognitionException {
        EObject current = null;

        EObject this_PrimitiveValueAnnotationParam_0 = null;

        EObject this_ObjectValueAnnotationParam_1 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:768:28: ( (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:769:1: (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:769:1: (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==25) ) {
                alt17=1;
            }
            else if ( (LA17_0==23) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:770:5: this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam
                    {
                     
                            newCompositeNode(grammarAccess.getParamValueAccess().getPrimitiveValueAnnotationParamParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_ruleParamValue1568);
                    this_PrimitiveValueAnnotationParam_0=rulePrimitiveValueAnnotationParam();

                    state._fsp--;

                     
                            current = this_PrimitiveValueAnnotationParam_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:780:5: this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam
                    {
                     
                            newCompositeNode(grammarAccess.getParamValueAccess().getObjectValueAnnotationParamParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_ruleParamValue1595);
                    this_ObjectValueAnnotationParam_1=ruleObjectValueAnnotationParam();

                    state._fsp--;

                     
                            current = this_ObjectValueAnnotationParam_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParamValue"


    // $ANTLR start "entryRuleObjectValueAnnotationParam"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:796:1: entryRuleObjectValueAnnotationParam returns [EObject current=null] : iv_ruleObjectValueAnnotationParam= ruleObjectValueAnnotationParam EOF ;
    public final EObject entryRuleObjectValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectValueAnnotationParam = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:797:2: (iv_ruleObjectValueAnnotationParam= ruleObjectValueAnnotationParam EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:798:2: iv_ruleObjectValueAnnotationParam= ruleObjectValueAnnotationParam EOF
            {
             newCompositeNode(grammarAccess.getObjectValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam1630);
            iv_ruleObjectValueAnnotationParam=ruleObjectValueAnnotationParam();

            state._fsp--;

             current =iv_ruleObjectValueAnnotationParam; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam1640); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectValueAnnotationParam"


    // $ANTLR start "ruleObjectValueAnnotationParam"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:805:1: ruleObjectValueAnnotationParam returns [EObject current=null] : (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? ) ;
    public final EObject ruleObjectValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:808:28: ( (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:809:1: (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:809:1: (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:809:3: otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )?
            {
            otherlv_0=(Token)match(input,23,FollowSets000.FOLLOW_23_in_ruleObjectValueAnnotationParam1677); 

                	newLeafNode(otherlv_0, grammarAccess.getObjectValueAnnotationParamAccess().getHyphenMinusGreaterThanSignKeyword_0());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:813:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:814:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:814:1: ( ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:815:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getObjectValueAnnotationParamRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectCrossReference_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam1700);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:828:2: (otherlv_2= '.' ( ( ruleEString ) ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==24) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:828:4: otherlv_2= '.' ( ( ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,24,FollowSets000.FOLLOW_24_in_ruleObjectValueAnnotationParam1713); 

                        	newLeafNode(otherlv_2, grammarAccess.getObjectValueAnnotationParamAccess().getFullStopKeyword_2_0());
                        
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:832:1: ( ( ruleEString ) )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:833:1: ( ruleEString )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:833:1: ( ruleEString )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:834:3: ruleEString
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectValueAnnotationParamRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureCrossReference_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam1736);
                    ruleEString();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectValueAnnotationParam"


    // $ANTLR start "entryRulePrimitiveValueAnnotationParam"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:855:1: entryRulePrimitiveValueAnnotationParam returns [EObject current=null] : iv_rulePrimitiveValueAnnotationParam= rulePrimitiveValueAnnotationParam EOF ;
    public final EObject entryRulePrimitiveValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveValueAnnotationParam = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:856:2: (iv_rulePrimitiveValueAnnotationParam= rulePrimitiveValueAnnotationParam EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:857:2: iv_rulePrimitiveValueAnnotationParam= rulePrimitiveValueAnnotationParam EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam1774);
            iv_rulePrimitiveValueAnnotationParam=rulePrimitiveValueAnnotationParam();

            state._fsp--;

             current =iv_rulePrimitiveValueAnnotationParam; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam1784); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveValueAnnotationParam"


    // $ANTLR start "rulePrimitiveValueAnnotationParam"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:864:1: rulePrimitiveValueAnnotationParam returns [EObject current=null] : (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) ) ;
    public final EObject rulePrimitiveValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:867:28: ( (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:868:1: (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:868:1: (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:868:3: otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) )
            {
            otherlv_0=(Token)match(input,25,FollowSets000.FOLLOW_25_in_rulePrimitiveValueAnnotationParam1821); 

                	newLeafNode(otherlv_0, grammarAccess.getPrimitiveValueAnnotationParamAccess().getEqualsSignKeyword_0());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:872:1: ( (lv_value_1_0= rulePrimitiveValue ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:873:1: (lv_value_1_0= rulePrimitiveValue )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:873:1: (lv_value_1_0= rulePrimitiveValue )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:874:3: lv_value_1_0= rulePrimitiveValue
            {
             
            	        newCompositeNode(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValuePrimitiveValueParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_rulePrimitiveValueAnnotationParam1842);
            lv_value_1_0=rulePrimitiveValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPrimitiveValueAnnotationParamRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"PrimitiveValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveValueAnnotationParam"


    // $ANTLR start "entryRuleAttribute"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:898:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:899:2: (iv_ruleAttribute= ruleAttribute EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:900:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_entryRuleAttribute1878);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttribute1888); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:907:1: ruleAttribute returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        AntlrDatatypeRuleToken lv_name_4_0 = null;

        EObject lv_values_6_0 = null;

        EObject lv_values_8_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:910:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:911:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:911:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:911:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )*
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:911:2: ()
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:912:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAttributeAccess().getAttributeAction_0(),
                        current);
                

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:917:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==19) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:917:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:917:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:918:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:918:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:919:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleAttribute1944);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:935:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==19) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:936:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:936:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:937:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleAttribute1965);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop19;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_3=(Token)match(input,26,FollowSets000.FOLLOW_26_in_ruleAttribute1980); 

                	newLeafNode(otherlv_3, grammarAccess.getAttributeAccess().getAttrKeyword_2());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:957:1: ( (lv_name_4_0= ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:958:1: (lv_name_4_0= ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:958:1: (lv_name_4_0= ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:959:3: lv_name_4_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAttribute2001);
            lv_name_4_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_4_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_5=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleAttribute2013); 

                	newLeafNode(otherlv_5, grammarAccess.getAttributeAccess().getEqualsSignKeyword_4());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:979:1: ( (lv_values_6_0= rulePrimitiveValue ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:980:1: (lv_values_6_0= rulePrimitiveValue )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:980:1: (lv_values_6_0= rulePrimitiveValue )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:981:3: lv_values_6_0= rulePrimitiveValue
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_ruleAttribute2034);
            lv_values_6_0=rulePrimitiveValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		add(
                   			current, 
                   			"values",
                    		lv_values_6_0, 
                    		"PrimitiveValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:997:2: (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==21) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:997:4: otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) )
            	    {
            	    otherlv_7=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleAttribute2047); 

            	        	newLeafNode(otherlv_7, grammarAccess.getAttributeAccess().getCommaKeyword_6_0());
            	        
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1001:1: ( (lv_values_8_0= rulePrimitiveValue ) )
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1002:1: (lv_values_8_0= rulePrimitiveValue )
            	    {
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1002:1: (lv_values_8_0= rulePrimitiveValue )
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1003:3: lv_values_8_0= rulePrimitiveValue
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_6_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_ruleAttribute2068);
            	    lv_values_8_0=rulePrimitiveValue();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"values",
            	            		lv_values_8_0, 
            	            		"PrimitiveValue");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRulePrimitiveValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1027:1: entryRulePrimitiveValue returns [EObject current=null] : iv_rulePrimitiveValue= rulePrimitiveValue EOF ;
    public final EObject entryRulePrimitiveValue() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveValue = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1028:2: (iv_rulePrimitiveValue= rulePrimitiveValue EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1029:2: iv_rulePrimitiveValue= rulePrimitiveValue EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveValueRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue2106);
            iv_rulePrimitiveValue=rulePrimitiveValue();

            state._fsp--;

             current =iv_rulePrimitiveValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValue2116); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveValue"


    // $ANTLR start "rulePrimitiveValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1036:1: rulePrimitiveValue returns [EObject current=null] : (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue ) ;
    public final EObject rulePrimitiveValue() throws RecognitionException {
        EObject current = null;

        EObject this_StringValue_0 = null;

        EObject this_IntegerValue_1 = null;

        EObject this_BooleanValue_2 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1039:28: ( (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1040:1: (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1040:1: (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue )
            int alt22=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt22=1;
                }
                break;
            case RULE_INT:
                {
                alt22=2;
                }
                break;
            case 35:
            case 36:
                {
                alt22=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1041:5: this_StringValue_0= ruleStringValue
                    {
                     
                            newCompositeNode(grammarAccess.getPrimitiveValueAccess().getStringValueParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_rulePrimitiveValue2163);
                    this_StringValue_0=ruleStringValue();

                    state._fsp--;

                     
                            current = this_StringValue_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1051:5: this_IntegerValue_1= ruleIntegerValue
                    {
                     
                            newCompositeNode(grammarAccess.getPrimitiveValueAccess().getIntegerValueParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_rulePrimitiveValue2190);
                    this_IntegerValue_1=ruleIntegerValue();

                    state._fsp--;

                     
                            current = this_IntegerValue_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1061:5: this_BooleanValue_2= ruleBooleanValue
                    {
                     
                            newCompositeNode(grammarAccess.getPrimitiveValueAccess().getBooleanValueParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_rulePrimitiveValue2217);
                    this_BooleanValue_2=ruleBooleanValue();

                    state._fsp--;

                     
                            current = this_BooleanValue_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveValue"


    // $ANTLR start "entryRuleIntegerValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1077:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1078:2: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1079:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
             newCompositeNode(grammarAccess.getIntegerValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue2252);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;

             current =iv_ruleIntegerValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntegerValue2262); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1086:1: ruleIntegerValue returns [EObject current=null] : ( (lv_value_0_0= RULE_INT ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1089:28: ( ( (lv_value_0_0= RULE_INT ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1090:1: ( (lv_value_0_0= RULE_INT ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1090:1: ( (lv_value_0_0= RULE_INT ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1091:1: (lv_value_0_0= RULE_INT )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1091:1: (lv_value_0_0= RULE_INT )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1092:3: lv_value_0_0= RULE_INT
            {
            lv_value_0_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleIntegerValue2303); 

            			newLeafNode(lv_value_0_0, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getIntegerValueRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"INT");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleStringValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1116:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1117:2: (iv_ruleStringValue= ruleStringValue EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1118:2: iv_ruleStringValue= ruleStringValue EOF
            {
             newCompositeNode(grammarAccess.getStringValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_entryRuleStringValue2343);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;

             current =iv_ruleStringValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringValue2353); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1125:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1128:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1129:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1129:1: ( (lv_value_0_0= RULE_STRING ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1130:1: (lv_value_0_0= RULE_STRING )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1130:1: (lv_value_0_0= RULE_STRING )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1131:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleStringValue2394); 

            			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getStringValueRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"STRING");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleBooleanValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1155:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1156:2: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1157:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
             newCompositeNode(grammarAccess.getBooleanValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue2434);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;

             current =iv_ruleBooleanValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValue2444); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1164:1: ruleBooleanValue returns [EObject current=null] : ( (lv_value_0_0= ruleBooleanValueTerminal ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1167:28: ( ( (lv_value_0_0= ruleBooleanValueTerminal ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1168:1: ( (lv_value_0_0= ruleBooleanValueTerminal ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1168:1: ( (lv_value_0_0= ruleBooleanValueTerminal ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1169:1: (lv_value_0_0= ruleBooleanValueTerminal )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1169:1: (lv_value_0_0= ruleBooleanValueTerminal )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1170:3: lv_value_0_0= ruleBooleanValueTerminal
            {
             
            	        newCompositeNode(grammarAccess.getBooleanValueAccess().getValueBooleanValueTerminalParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_ruleBooleanValue2489);
            lv_value_0_0=ruleBooleanValueTerminal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanValueRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"BooleanValueTerminal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleReference"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1194:1: entryRuleReference returns [EObject current=null] : iv_ruleReference= ruleReference EOF ;
    public final EObject entryRuleReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReference = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1195:2: (iv_ruleReference= ruleReference EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1196:2: iv_ruleReference= ruleReference EOF
            {
             newCompositeNode(grammarAccess.getReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReference_in_entryRuleReference2524);
            iv_ruleReference=ruleReference();

            state._fsp--;

             current =iv_ruleReference; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReference2534); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1203:1: ruleReference returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* ) ;
    public final EObject ruleReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        EObject lv_graphicProperties_3_0 = null;

        AntlrDatatypeRuleToken lv_name_6_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1206:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1207:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1207:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1207:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )*
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1207:2: ()
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1208:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getReferenceAccess().getReferenceAction_0(),
                        current);
                

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1213:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==19) ) {
                int LA24_1 = input.LA(2);

                if ( ((LA24_1>=RULE_STRING && LA24_1<=RULE_ID)) ) {
                    alt24=1;
                }
            }
            switch (alt24) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1213:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1213:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1214:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1214:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1215:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleReference2590);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReferenceRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1231:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop23:
                    do {
                        int alt23=2;
                        int LA23_0 = input.LA(1);

                        if ( (LA23_0==19) ) {
                            int LA23_1 = input.LA(2);

                            if ( ((LA23_1>=RULE_STRING && LA23_1<=RULE_ID)) ) {
                                alt23=1;
                            }


                        }


                        switch (alt23) {
                    	case 1 :
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1232:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1232:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1233:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleReference2611);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getReferenceRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop23;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1249:5: ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==19) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1250:1: (lv_graphicProperties_3_0= ruleEdgeProperties )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1250:1: (lv_graphicProperties_3_0= ruleEdgeProperties )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1251:3: lv_graphicProperties_3_0= ruleEdgeProperties
                    {
                     
                    	        newCompositeNode(grammarAccess.getReferenceAccess().getGraphicPropertiesEdgePropertiesParserRuleCall_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEdgeProperties_in_ruleReference2635);
                    lv_graphicProperties_3_0=ruleEdgeProperties();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReferenceRule());
                    	        }
                           		set(
                           			current, 
                           			"graphicProperties",
                            		lv_graphicProperties_3_0, 
                            		"EdgeProperties");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1267:3: (otherlv_4= 'ref' | otherlv_5= 'rel' )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==27) ) {
                alt26=1;
            }
            else if ( (LA26_0==28) ) {
                alt26=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }
            switch (alt26) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1267:5: otherlv_4= 'ref'
                    {
                    otherlv_4=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleReference2649); 

                        	newLeafNode(otherlv_4, grammarAccess.getReferenceAccess().getRefKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1272:7: otherlv_5= 'rel'
                    {
                    otherlv_5=(Token)match(input,28,FollowSets000.FOLLOW_28_in_ruleReference2667); 

                        	newLeafNode(otherlv_5, grammarAccess.getReferenceAccess().getRelKeyword_3_1());
                        

                    }
                    break;

            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1276:2: ( (lv_name_6_0= ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1277:1: (lv_name_6_0= ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1277:1: (lv_name_6_0= ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1278:3: lv_name_6_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference2689);
            lv_name_6_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReferenceRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_6_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_7=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleReference2701); 

                	newLeafNode(otherlv_7, grammarAccess.getReferenceAccess().getEqualsSignKeyword_5());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1298:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1299:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1299:1: ( ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1300:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getReferenceRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_6_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference2724);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1313:2: (otherlv_9= ',' ( ( ruleEString ) ) )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==21) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1313:4: otherlv_9= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_9=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleReference2737); 

            	        	newLeafNode(otherlv_9, grammarAccess.getReferenceAccess().getCommaKeyword_7_0());
            	        
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1317:1: ( ( ruleEString ) )
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1318:1: ( ruleEString )
            	    {
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1318:1: ( ruleEString )
            	    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1319:3: ruleEString
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getReferenceRule());
            	    	        }
            	            
            	     
            	    	        newCompositeNode(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_7_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference2760);
            	    ruleEString();

            	    state._fsp--;

            	     
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleEdgeProperties"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1340:1: entryRuleEdgeProperties returns [EObject current=null] : iv_ruleEdgeProperties= ruleEdgeProperties EOF ;
    public final EObject entryRuleEdgeProperties() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEdgeProperties = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1341:2: (iv_ruleEdgeProperties= ruleEdgeProperties EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1342:2: iv_ruleEdgeProperties= ruleEdgeProperties EOF
            {
             newCompositeNode(grammarAccess.getEdgePropertiesRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEdgeProperties_in_entryRuleEdgeProperties2798);
            iv_ruleEdgeProperties=ruleEdgeProperties();

            state._fsp--;

             current =iv_ruleEdgeProperties; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEdgeProperties2808); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEdgeProperties"


    // $ANTLR start "ruleEdgeProperties"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1349:1: ruleEdgeProperties returns [EObject current=null] : ( () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')' ) ;
    public final EObject ruleEdgeProperties() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token lv_width_10_0=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        AntlrDatatypeRuleToken lv_color_6_0 = null;

        Enumerator lv_lineType_14_0 = null;

        Enumerator lv_srcArrow_18_0 = null;

        Enumerator lv_tgtArrow_22_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1352:28: ( ( () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')' ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1353:1: ( () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')' )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1353:1: ( () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')' )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1353:2: () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')'
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1353:2: ()
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1354:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getEdgePropertiesAccess().getEdgePropertiesAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleEdgeProperties2854); 

                	newLeafNode(otherlv_1, grammarAccess.getEdgePropertiesAccess().getCommercialAtKeyword_1());
                
            otherlv_2=(Token)match(input,29,FollowSets000.FOLLOW_29_in_ruleEdgeProperties2866); 

                	newLeafNode(otherlv_2, grammarAccess.getEdgePropertiesAccess().getStyleKeyword_2());
                
            otherlv_3=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleEdgeProperties2878); 

                	newLeafNode(otherlv_3, grammarAccess.getEdgePropertiesAccess().getLeftParenthesisKeyword_3());
                
            otherlv_4=(Token)match(input,30,FollowSets000.FOLLOW_30_in_ruleEdgeProperties2890); 

                	newLeafNode(otherlv_4, grammarAccess.getEdgePropertiesAccess().getColorKeyword_4());
                
            otherlv_5=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties2902); 

                	newLeafNode(otherlv_5, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_5());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1379:1: ( (lv_color_6_0= ruleEString ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1380:1: (lv_color_6_0= ruleEString )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1380:1: (lv_color_6_0= ruleEString )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1381:3: lv_color_6_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getEdgePropertiesAccess().getColorEStringParserRuleCall_6_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleEdgeProperties2923);
            lv_color_6_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEdgePropertiesRule());
            	        }
                   		set(
                   			current, 
                   			"color",
                    		lv_color_6_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_7=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleEdgeProperties2935); 

                	newLeafNode(otherlv_7, grammarAccess.getEdgePropertiesAccess().getCommaKeyword_7());
                
            otherlv_8=(Token)match(input,31,FollowSets000.FOLLOW_31_in_ruleEdgeProperties2947); 

                	newLeafNode(otherlv_8, grammarAccess.getEdgePropertiesAccess().getWidthKeyword_8());
                
            otherlv_9=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties2959); 

                	newLeafNode(otherlv_9, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_9());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1409:1: ( (lv_width_10_0= RULE_INT ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1410:1: (lv_width_10_0= RULE_INT )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1410:1: (lv_width_10_0= RULE_INT )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1411:3: lv_width_10_0= RULE_INT
            {
            lv_width_10_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleEdgeProperties2976); 

            			newLeafNode(lv_width_10_0, grammarAccess.getEdgePropertiesAccess().getWidthINTTerminalRuleCall_10_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEdgePropertiesRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"width",
                    		lv_width_10_0, 
                    		"INT");
            	    

            }


            }

            otherlv_11=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleEdgeProperties2993); 

                	newLeafNode(otherlv_11, grammarAccess.getEdgePropertiesAccess().getCommaKeyword_11());
                
            otherlv_12=(Token)match(input,32,FollowSets000.FOLLOW_32_in_ruleEdgeProperties3005); 

                	newLeafNode(otherlv_12, grammarAccess.getEdgePropertiesAccess().getLineKeyword_12());
                
            otherlv_13=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties3017); 

                	newLeafNode(otherlv_13, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_13());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1439:1: ( (lv_lineType_14_0= ruleLineType ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1440:1: (lv_lineType_14_0= ruleLineType )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1440:1: (lv_lineType_14_0= ruleLineType )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1441:3: lv_lineType_14_0= ruleLineType
            {
             
            	        newCompositeNode(grammarAccess.getEdgePropertiesAccess().getLineTypeLineTypeEnumRuleCall_14_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleLineType_in_ruleEdgeProperties3038);
            lv_lineType_14_0=ruleLineType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEdgePropertiesRule());
            	        }
                   		set(
                   			current, 
                   			"lineType",
                    		lv_lineType_14_0, 
                    		"LineType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_15=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleEdgeProperties3050); 

                	newLeafNode(otherlv_15, grammarAccess.getEdgePropertiesAccess().getCommaKeyword_15());
                
            otherlv_16=(Token)match(input,33,FollowSets000.FOLLOW_33_in_ruleEdgeProperties3062); 

                	newLeafNode(otherlv_16, grammarAccess.getEdgePropertiesAccess().getSourceKeyword_16());
                
            otherlv_17=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties3074); 

                	newLeafNode(otherlv_17, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_17());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1469:1: ( (lv_srcArrow_18_0= ruleArrowType ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1470:1: (lv_srcArrow_18_0= ruleArrowType )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1470:1: (lv_srcArrow_18_0= ruleArrowType )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1471:3: lv_srcArrow_18_0= ruleArrowType
            {
             
            	        newCompositeNode(grammarAccess.getEdgePropertiesAccess().getSrcArrowArrowTypeEnumRuleCall_18_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleArrowType_in_ruleEdgeProperties3095);
            lv_srcArrow_18_0=ruleArrowType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEdgePropertiesRule());
            	        }
                   		set(
                   			current, 
                   			"srcArrow",
                    		lv_srcArrow_18_0, 
                    		"ArrowType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_19=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleEdgeProperties3107); 

                	newLeafNode(otherlv_19, grammarAccess.getEdgePropertiesAccess().getCommaKeyword_19());
                
            otherlv_20=(Token)match(input,34,FollowSets000.FOLLOW_34_in_ruleEdgeProperties3119); 

                	newLeafNode(otherlv_20, grammarAccess.getEdgePropertiesAccess().getTargetKeyword_20());
                
            otherlv_21=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties3131); 

                	newLeafNode(otherlv_21, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_21());
                
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1499:1: ( (lv_tgtArrow_22_0= ruleArrowType ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1500:1: (lv_tgtArrow_22_0= ruleArrowType )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1500:1: (lv_tgtArrow_22_0= ruleArrowType )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1501:3: lv_tgtArrow_22_0= ruleArrowType
            {
             
            	        newCompositeNode(grammarAccess.getEdgePropertiesAccess().getTgtArrowArrowTypeEnumRuleCall_22_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleArrowType_in_ruleEdgeProperties3152);
            lv_tgtArrow_22_0=ruleArrowType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEdgePropertiesRule());
            	        }
                   		set(
                   			current, 
                   			"tgtArrow",
                    		lv_tgtArrow_22_0, 
                    		"ArrowType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_23=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleEdgeProperties3164); 

                	newLeafNode(otherlv_23, grammarAccess.getEdgePropertiesAccess().getRightParenthesisKeyword_23());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEdgeProperties"


    // $ANTLR start "entryRuleBooleanValueTerminal"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1529:1: entryRuleBooleanValueTerminal returns [String current=null] : iv_ruleBooleanValueTerminal= ruleBooleanValueTerminal EOF ;
    public final String entryRuleBooleanValueTerminal() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBooleanValueTerminal = null;


        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1530:2: (iv_ruleBooleanValueTerminal= ruleBooleanValueTerminal EOF )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1531:2: iv_ruleBooleanValueTerminal= ruleBooleanValueTerminal EOF
            {
             newCompositeNode(grammarAccess.getBooleanValueTerminalRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal3201);
            iv_ruleBooleanValueTerminal=ruleBooleanValueTerminal();

            state._fsp--;

             current =iv_ruleBooleanValueTerminal.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValueTerminal3212); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValueTerminal"


    // $ANTLR start "ruleBooleanValueTerminal"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1538:1: ruleBooleanValueTerminal returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleBooleanValueTerminal() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1541:28: ( (kw= 'true' | kw= 'false' ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1542:1: (kw= 'true' | kw= 'false' )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1542:1: (kw= 'true' | kw= 'false' )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==35) ) {
                alt28=1;
            }
            else if ( (LA28_0==36) ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1543:2: kw= 'true'
                    {
                    kw=(Token)match(input,35,FollowSets000.FOLLOW_35_in_ruleBooleanValueTerminal3250); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getBooleanValueTerminalAccess().getTrueKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1550:2: kw= 'false'
                    {
                    kw=(Token)match(input,36,FollowSets000.FOLLOW_36_in_ruleBooleanValueTerminal3269); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getBooleanValueTerminalAccess().getFalseKeyword_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValueTerminal"


    // $ANTLR start "ruleFragmentType"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1563:1: ruleFragmentType returns [Enumerator current=null] : ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) ) ;
    public final Enumerator ruleFragmentType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1565:28: ( ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1566:1: ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1566:1: ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==37) ) {
                alt29=1;
            }
            else if ( (LA29_0==38) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1566:2: (enumLiteral_0= 'positive' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1566:2: (enumLiteral_0= 'positive' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1566:4: enumLiteral_0= 'positive'
                    {
                    enumLiteral_0=(Token)match(input,37,FollowSets000.FOLLOW_37_in_ruleFragmentType3323); 

                            current = grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1572:6: (enumLiteral_1= 'negative' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1572:6: (enumLiteral_1= 'negative' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1572:8: enumLiteral_1= 'negative'
                    {
                    enumLiteral_1=(Token)match(input,38,FollowSets000.FOLLOW_38_in_ruleFragmentType3340); 

                            current = grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentType"


    // $ANTLR start "ruleLineType"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1582:1: ruleLineType returns [Enumerator current=null] : ( (enumLiteral_0= 'line' ) | (enumLiteral_1= 'dashed' ) | (enumLiteral_2= 'dashed-dotted' ) | (enumLiteral_3= 'dotted' ) ) ;
    public final Enumerator ruleLineType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1584:28: ( ( (enumLiteral_0= 'line' ) | (enumLiteral_1= 'dashed' ) | (enumLiteral_2= 'dashed-dotted' ) | (enumLiteral_3= 'dotted' ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1585:1: ( (enumLiteral_0= 'line' ) | (enumLiteral_1= 'dashed' ) | (enumLiteral_2= 'dashed-dotted' ) | (enumLiteral_3= 'dotted' ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1585:1: ( (enumLiteral_0= 'line' ) | (enumLiteral_1= 'dashed' ) | (enumLiteral_2= 'dashed-dotted' ) | (enumLiteral_3= 'dotted' ) )
            int alt30=4;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt30=1;
                }
                break;
            case 39:
                {
                alt30=2;
                }
                break;
            case 40:
                {
                alt30=3;
                }
                break;
            case 41:
                {
                alt30=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1585:2: (enumLiteral_0= 'line' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1585:2: (enumLiteral_0= 'line' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1585:4: enumLiteral_0= 'line'
                    {
                    enumLiteral_0=(Token)match(input,32,FollowSets000.FOLLOW_32_in_ruleLineType3385); 

                            current = grammarAccess.getLineTypeAccess().getLineEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getLineTypeAccess().getLineEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1591:6: (enumLiteral_1= 'dashed' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1591:6: (enumLiteral_1= 'dashed' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1591:8: enumLiteral_1= 'dashed'
                    {
                    enumLiteral_1=(Token)match(input,39,FollowSets000.FOLLOW_39_in_ruleLineType3402); 

                            current = grammarAccess.getLineTypeAccess().getDashedEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getLineTypeAccess().getDashedEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1597:6: (enumLiteral_2= 'dashed-dotted' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1597:6: (enumLiteral_2= 'dashed-dotted' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1597:8: enumLiteral_2= 'dashed-dotted'
                    {
                    enumLiteral_2=(Token)match(input,40,FollowSets000.FOLLOW_40_in_ruleLineType3419); 

                            current = grammarAccess.getLineTypeAccess().getDashedDottedEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getLineTypeAccess().getDashedDottedEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1603:6: (enumLiteral_3= 'dotted' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1603:6: (enumLiteral_3= 'dotted' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1603:8: enumLiteral_3= 'dotted'
                    {
                    enumLiteral_3=(Token)match(input,41,FollowSets000.FOLLOW_41_in_ruleLineType3436); 

                            current = grammarAccess.getLineTypeAccess().getDottedEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getLineTypeAccess().getDottedEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLineType"


    // $ANTLR start "ruleArrowType"
    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1613:1: ruleArrowType returns [Enumerator current=null] : ( (enumLiteral_0= 'circle' ) | (enumLiteral_1= 'concave' ) | (enumLiteral_2= 'convex' ) | (enumLiteral_3= 'crows-foot-many' ) | (enumLiteral_4= 'crows-foot-many-mandatory' ) | (enumLiteral_5= 'crows-foot-many-optional' ) | (enumLiteral_6= 'crows-foot-one' ) | (enumLiteral_7= 'crows-foot-one-mandatory' ) | (enumLiteral_8= 'crows-foot-one-optional' ) | (enumLiteral_9= 'dash' ) | (enumLiteral_10= 'delta' ) | (enumLiteral_11= 'diamond' ) | (enumLiteral_12= 'none' ) | (enumLiteral_13= 'plain' ) | (enumLiteral_14= 'short' ) | (enumLiteral_15= 'skewed-dash' ) | (enumLiteral_16= 'standard' ) | (enumLiteral_17= 'transparent-circle' ) | (enumLiteral_18= 't-shape' ) | (enumLiteral_19= 'white-delta' ) | (enumLiteral_20= 'white-diamond' ) ) ;
    public final Enumerator ruleArrowType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;
        Token enumLiteral_10=null;
        Token enumLiteral_11=null;
        Token enumLiteral_12=null;
        Token enumLiteral_13=null;
        Token enumLiteral_14=null;
        Token enumLiteral_15=null;
        Token enumLiteral_16=null;
        Token enumLiteral_17=null;
        Token enumLiteral_18=null;
        Token enumLiteral_19=null;
        Token enumLiteral_20=null;

         enterRule(); 
        try {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1615:28: ( ( (enumLiteral_0= 'circle' ) | (enumLiteral_1= 'concave' ) | (enumLiteral_2= 'convex' ) | (enumLiteral_3= 'crows-foot-many' ) | (enumLiteral_4= 'crows-foot-many-mandatory' ) | (enumLiteral_5= 'crows-foot-many-optional' ) | (enumLiteral_6= 'crows-foot-one' ) | (enumLiteral_7= 'crows-foot-one-mandatory' ) | (enumLiteral_8= 'crows-foot-one-optional' ) | (enumLiteral_9= 'dash' ) | (enumLiteral_10= 'delta' ) | (enumLiteral_11= 'diamond' ) | (enumLiteral_12= 'none' ) | (enumLiteral_13= 'plain' ) | (enumLiteral_14= 'short' ) | (enumLiteral_15= 'skewed-dash' ) | (enumLiteral_16= 'standard' ) | (enumLiteral_17= 'transparent-circle' ) | (enumLiteral_18= 't-shape' ) | (enumLiteral_19= 'white-delta' ) | (enumLiteral_20= 'white-diamond' ) ) )
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1616:1: ( (enumLiteral_0= 'circle' ) | (enumLiteral_1= 'concave' ) | (enumLiteral_2= 'convex' ) | (enumLiteral_3= 'crows-foot-many' ) | (enumLiteral_4= 'crows-foot-many-mandatory' ) | (enumLiteral_5= 'crows-foot-many-optional' ) | (enumLiteral_6= 'crows-foot-one' ) | (enumLiteral_7= 'crows-foot-one-mandatory' ) | (enumLiteral_8= 'crows-foot-one-optional' ) | (enumLiteral_9= 'dash' ) | (enumLiteral_10= 'delta' ) | (enumLiteral_11= 'diamond' ) | (enumLiteral_12= 'none' ) | (enumLiteral_13= 'plain' ) | (enumLiteral_14= 'short' ) | (enumLiteral_15= 'skewed-dash' ) | (enumLiteral_16= 'standard' ) | (enumLiteral_17= 'transparent-circle' ) | (enumLiteral_18= 't-shape' ) | (enumLiteral_19= 'white-delta' ) | (enumLiteral_20= 'white-diamond' ) )
            {
            // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1616:1: ( (enumLiteral_0= 'circle' ) | (enumLiteral_1= 'concave' ) | (enumLiteral_2= 'convex' ) | (enumLiteral_3= 'crows-foot-many' ) | (enumLiteral_4= 'crows-foot-many-mandatory' ) | (enumLiteral_5= 'crows-foot-many-optional' ) | (enumLiteral_6= 'crows-foot-one' ) | (enumLiteral_7= 'crows-foot-one-mandatory' ) | (enumLiteral_8= 'crows-foot-one-optional' ) | (enumLiteral_9= 'dash' ) | (enumLiteral_10= 'delta' ) | (enumLiteral_11= 'diamond' ) | (enumLiteral_12= 'none' ) | (enumLiteral_13= 'plain' ) | (enumLiteral_14= 'short' ) | (enumLiteral_15= 'skewed-dash' ) | (enumLiteral_16= 'standard' ) | (enumLiteral_17= 'transparent-circle' ) | (enumLiteral_18= 't-shape' ) | (enumLiteral_19= 'white-delta' ) | (enumLiteral_20= 'white-diamond' ) )
            int alt31=21;
            switch ( input.LA(1) ) {
            case 42:
                {
                alt31=1;
                }
                break;
            case 43:
                {
                alt31=2;
                }
                break;
            case 44:
                {
                alt31=3;
                }
                break;
            case 45:
                {
                alt31=4;
                }
                break;
            case 46:
                {
                alt31=5;
                }
                break;
            case 47:
                {
                alt31=6;
                }
                break;
            case 48:
                {
                alt31=7;
                }
                break;
            case 49:
                {
                alt31=8;
                }
                break;
            case 50:
                {
                alt31=9;
                }
                break;
            case 51:
                {
                alt31=10;
                }
                break;
            case 52:
                {
                alt31=11;
                }
                break;
            case 53:
                {
                alt31=12;
                }
                break;
            case 54:
                {
                alt31=13;
                }
                break;
            case 55:
                {
                alt31=14;
                }
                break;
            case 56:
                {
                alt31=15;
                }
                break;
            case 57:
                {
                alt31=16;
                }
                break;
            case 58:
                {
                alt31=17;
                }
                break;
            case 59:
                {
                alt31=18;
                }
                break;
            case 60:
                {
                alt31=19;
                }
                break;
            case 61:
                {
                alt31=20;
                }
                break;
            case 62:
                {
                alt31=21;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1616:2: (enumLiteral_0= 'circle' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1616:2: (enumLiteral_0= 'circle' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1616:4: enumLiteral_0= 'circle'
                    {
                    enumLiteral_0=(Token)match(input,42,FollowSets000.FOLLOW_42_in_ruleArrowType3481); 

                            current = grammarAccess.getArrowTypeAccess().getCircleEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getArrowTypeAccess().getCircleEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1622:6: (enumLiteral_1= 'concave' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1622:6: (enumLiteral_1= 'concave' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1622:8: enumLiteral_1= 'concave'
                    {
                    enumLiteral_1=(Token)match(input,43,FollowSets000.FOLLOW_43_in_ruleArrowType3498); 

                            current = grammarAccess.getArrowTypeAccess().getConcaveEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getArrowTypeAccess().getConcaveEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1628:6: (enumLiteral_2= 'convex' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1628:6: (enumLiteral_2= 'convex' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1628:8: enumLiteral_2= 'convex'
                    {
                    enumLiteral_2=(Token)match(input,44,FollowSets000.FOLLOW_44_in_ruleArrowType3515); 

                            current = grammarAccess.getArrowTypeAccess().getConvexEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getArrowTypeAccess().getConvexEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1634:6: (enumLiteral_3= 'crows-foot-many' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1634:6: (enumLiteral_3= 'crows-foot-many' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1634:8: enumLiteral_3= 'crows-foot-many'
                    {
                    enumLiteral_3=(Token)match(input,45,FollowSets000.FOLLOW_45_in_ruleArrowType3532); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootManyEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getArrowTypeAccess().getCrowsFootManyEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;
                case 5 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1640:6: (enumLiteral_4= 'crows-foot-many-mandatory' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1640:6: (enumLiteral_4= 'crows-foot-many-mandatory' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1640:8: enumLiteral_4= 'crows-foot-many-mandatory'
                    {
                    enumLiteral_4=(Token)match(input,46,FollowSets000.FOLLOW_46_in_ruleArrowType3549); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootManyMandatoryEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_4, grammarAccess.getArrowTypeAccess().getCrowsFootManyMandatoryEnumLiteralDeclaration_4()); 
                        

                    }


                    }
                    break;
                case 6 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1646:6: (enumLiteral_5= 'crows-foot-many-optional' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1646:6: (enumLiteral_5= 'crows-foot-many-optional' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1646:8: enumLiteral_5= 'crows-foot-many-optional'
                    {
                    enumLiteral_5=(Token)match(input,47,FollowSets000.FOLLOW_47_in_ruleArrowType3566); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootManyOptionalEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_5, grammarAccess.getArrowTypeAccess().getCrowsFootManyOptionalEnumLiteralDeclaration_5()); 
                        

                    }


                    }
                    break;
                case 7 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1652:6: (enumLiteral_6= 'crows-foot-one' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1652:6: (enumLiteral_6= 'crows-foot-one' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1652:8: enumLiteral_6= 'crows-foot-one'
                    {
                    enumLiteral_6=(Token)match(input,48,FollowSets000.FOLLOW_48_in_ruleArrowType3583); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootOneEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_6, grammarAccess.getArrowTypeAccess().getCrowsFootOneEnumLiteralDeclaration_6()); 
                        

                    }


                    }
                    break;
                case 8 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1658:6: (enumLiteral_7= 'crows-foot-one-mandatory' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1658:6: (enumLiteral_7= 'crows-foot-one-mandatory' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1658:8: enumLiteral_7= 'crows-foot-one-mandatory'
                    {
                    enumLiteral_7=(Token)match(input,49,FollowSets000.FOLLOW_49_in_ruleArrowType3600); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootOneMandatoryEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_7, grammarAccess.getArrowTypeAccess().getCrowsFootOneMandatoryEnumLiteralDeclaration_7()); 
                        

                    }


                    }
                    break;
                case 9 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1664:6: (enumLiteral_8= 'crows-foot-one-optional' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1664:6: (enumLiteral_8= 'crows-foot-one-optional' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1664:8: enumLiteral_8= 'crows-foot-one-optional'
                    {
                    enumLiteral_8=(Token)match(input,50,FollowSets000.FOLLOW_50_in_ruleArrowType3617); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootOneOptionalEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_8, grammarAccess.getArrowTypeAccess().getCrowsFootOneOptionalEnumLiteralDeclaration_8()); 
                        

                    }


                    }
                    break;
                case 10 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1670:6: (enumLiteral_9= 'dash' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1670:6: (enumLiteral_9= 'dash' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1670:8: enumLiteral_9= 'dash'
                    {
                    enumLiteral_9=(Token)match(input,51,FollowSets000.FOLLOW_51_in_ruleArrowType3634); 

                            current = grammarAccess.getArrowTypeAccess().getDashEnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_9, grammarAccess.getArrowTypeAccess().getDashEnumLiteralDeclaration_9()); 
                        

                    }


                    }
                    break;
                case 11 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1676:6: (enumLiteral_10= 'delta' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1676:6: (enumLiteral_10= 'delta' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1676:8: enumLiteral_10= 'delta'
                    {
                    enumLiteral_10=(Token)match(input,52,FollowSets000.FOLLOW_52_in_ruleArrowType3651); 

                            current = grammarAccess.getArrowTypeAccess().getDeltaEnumLiteralDeclaration_10().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_10, grammarAccess.getArrowTypeAccess().getDeltaEnumLiteralDeclaration_10()); 
                        

                    }


                    }
                    break;
                case 12 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1682:6: (enumLiteral_11= 'diamond' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1682:6: (enumLiteral_11= 'diamond' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1682:8: enumLiteral_11= 'diamond'
                    {
                    enumLiteral_11=(Token)match(input,53,FollowSets000.FOLLOW_53_in_ruleArrowType3668); 

                            current = grammarAccess.getArrowTypeAccess().getDiamondEnumLiteralDeclaration_11().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_11, grammarAccess.getArrowTypeAccess().getDiamondEnumLiteralDeclaration_11()); 
                        

                    }


                    }
                    break;
                case 13 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1688:6: (enumLiteral_12= 'none' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1688:6: (enumLiteral_12= 'none' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1688:8: enumLiteral_12= 'none'
                    {
                    enumLiteral_12=(Token)match(input,54,FollowSets000.FOLLOW_54_in_ruleArrowType3685); 

                            current = grammarAccess.getArrowTypeAccess().getNoneEnumLiteralDeclaration_12().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_12, grammarAccess.getArrowTypeAccess().getNoneEnumLiteralDeclaration_12()); 
                        

                    }


                    }
                    break;
                case 14 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1694:6: (enumLiteral_13= 'plain' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1694:6: (enumLiteral_13= 'plain' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1694:8: enumLiteral_13= 'plain'
                    {
                    enumLiteral_13=(Token)match(input,55,FollowSets000.FOLLOW_55_in_ruleArrowType3702); 

                            current = grammarAccess.getArrowTypeAccess().getPlainEnumLiteralDeclaration_13().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_13, grammarAccess.getArrowTypeAccess().getPlainEnumLiteralDeclaration_13()); 
                        

                    }


                    }
                    break;
                case 15 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1700:6: (enumLiteral_14= 'short' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1700:6: (enumLiteral_14= 'short' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1700:8: enumLiteral_14= 'short'
                    {
                    enumLiteral_14=(Token)match(input,56,FollowSets000.FOLLOW_56_in_ruleArrowType3719); 

                            current = grammarAccess.getArrowTypeAccess().getShortEnumLiteralDeclaration_14().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_14, grammarAccess.getArrowTypeAccess().getShortEnumLiteralDeclaration_14()); 
                        

                    }


                    }
                    break;
                case 16 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1706:6: (enumLiteral_15= 'skewed-dash' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1706:6: (enumLiteral_15= 'skewed-dash' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1706:8: enumLiteral_15= 'skewed-dash'
                    {
                    enumLiteral_15=(Token)match(input,57,FollowSets000.FOLLOW_57_in_ruleArrowType3736); 

                            current = grammarAccess.getArrowTypeAccess().getSkewedDashEnumLiteralDeclaration_15().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_15, grammarAccess.getArrowTypeAccess().getSkewedDashEnumLiteralDeclaration_15()); 
                        

                    }


                    }
                    break;
                case 17 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1712:6: (enumLiteral_16= 'standard' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1712:6: (enumLiteral_16= 'standard' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1712:8: enumLiteral_16= 'standard'
                    {
                    enumLiteral_16=(Token)match(input,58,FollowSets000.FOLLOW_58_in_ruleArrowType3753); 

                            current = grammarAccess.getArrowTypeAccess().getStandardEnumLiteralDeclaration_16().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_16, grammarAccess.getArrowTypeAccess().getStandardEnumLiteralDeclaration_16()); 
                        

                    }


                    }
                    break;
                case 18 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1718:6: (enumLiteral_17= 'transparent-circle' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1718:6: (enumLiteral_17= 'transparent-circle' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1718:8: enumLiteral_17= 'transparent-circle'
                    {
                    enumLiteral_17=(Token)match(input,59,FollowSets000.FOLLOW_59_in_ruleArrowType3770); 

                            current = grammarAccess.getArrowTypeAccess().getTransparentCircleEnumLiteralDeclaration_17().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_17, grammarAccess.getArrowTypeAccess().getTransparentCircleEnumLiteralDeclaration_17()); 
                        

                    }


                    }
                    break;
                case 19 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1724:6: (enumLiteral_18= 't-shape' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1724:6: (enumLiteral_18= 't-shape' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1724:8: enumLiteral_18= 't-shape'
                    {
                    enumLiteral_18=(Token)match(input,60,FollowSets000.FOLLOW_60_in_ruleArrowType3787); 

                            current = grammarAccess.getArrowTypeAccess().getTShapeEnumLiteralDeclaration_18().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_18, grammarAccess.getArrowTypeAccess().getTShapeEnumLiteralDeclaration_18()); 
                        

                    }


                    }
                    break;
                case 20 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1730:6: (enumLiteral_19= 'white-delta' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1730:6: (enumLiteral_19= 'white-delta' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1730:8: enumLiteral_19= 'white-delta'
                    {
                    enumLiteral_19=(Token)match(input,61,FollowSets000.FOLLOW_61_in_ruleArrowType3804); 

                            current = grammarAccess.getArrowTypeAccess().getWhiteDeltaEnumLiteralDeclaration_19().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_19, grammarAccess.getArrowTypeAccess().getWhiteDeltaEnumLiteralDeclaration_19()); 
                        

                    }


                    }
                    break;
                case 21 :
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1736:6: (enumLiteral_20= 'white-diamond' )
                    {
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1736:6: (enumLiteral_20= 'white-diamond' )
                    // ../metabup.shell.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelingShell.g:1736:8: enumLiteral_20= 'white-diamond'
                    {
                    enumLiteral_20=(Token)match(input,62,FollowSets000.FOLLOW_62_in_ruleArrowType3821); 

                            current = grammarAccess.getArrowTypeAccess().getWhiteDiamondEnumLiteralDeclaration_20().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_20, grammarAccess.getArrowTypeAccess().getWhiteDiamondEnumLiteralDeclaration_20()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrowType"

    // Delegated rules


    protected DFA7 dfa7 = new DFA7(this);
    static final String DFA7_eotS =
        "\103\uffff";
    static final String DFA7_eofS =
        "\103\uffff";
    static final String DFA7_minS =
        "\1\23\1\4\2\uffff\2\23\2\4\2\27\2\23\3\4\6\25\2\27\1\4\1\23\3\4"+
        "\2\27\10\25\3\4\1\23\1\4\6\25\2\27\2\25\3\4\10\25\1\4\2\25";
    static final String DFA7_maxS =
        "\1\34\1\35\2\uffff\2\34\1\5\1\35\2\31\2\34\1\44\2\5\4\26\2\30\2"+
        "\31\1\5\1\34\1\5\1\44\1\5\2\31\6\26\2\30\1\44\2\5\1\34\1\5\4\26"+
        "\2\30\2\31\2\26\1\5\1\44\1\5\6\26\2\30\1\5\2\26";
    static final String DFA7_acceptS =
        "\2\uffff\1\1\1\2\77\uffff";
    static final String DFA7_specialS =
        "\103\uffff}>";
    static final String[] DFA7_transitionS = {
            "\1\1\6\uffff\1\2\2\3",
            "\1\4\1\5\27\uffff\1\3",
            "",
            "",
            "\1\7\1\6\5\uffff\1\2\2\3",
            "\1\7\1\6\5\uffff\1\2\2\3",
            "\1\10\1\11",
            "\1\12\1\13\27\uffff\1\3",
            "\1\15\1\uffff\1\14",
            "\1\15\1\uffff\1\14",
            "\1\7\1\16\5\uffff\1\2\2\3",
            "\1\7\1\16\5\uffff\1\2\2\3",
            "\1\17\1\uffff\1\20\34\uffff\1\21\1\22",
            "\1\23\1\24",
            "\1\25\1\26",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30\1\uffff\1\31",
            "\1\27\1\30\1\uffff\1\31",
            "\1\33\1\uffff\1\32",
            "\1\33\1\uffff\1\32",
            "\1\34\1\35",
            "\1\7\6\uffff\1\2\2\3",
            "\1\36\1\37",
            "\1\40\1\uffff\1\41\34\uffff\1\42\1\43",
            "\1\44\1\45",
            "\1\47\1\uffff\1\46",
            "\1\47\1\uffff\1\46",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51\1\uffff\1\52",
            "\1\50\1\51\1\uffff\1\52",
            "\1\53\1\uffff\1\54\34\uffff\1\55\1\56",
            "\1\57\1\60",
            "\1\61\1\62",
            "\1\7\6\uffff\1\2\2\3",
            "\1\63\1\64",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30\1\uffff\1\65",
            "\1\27\1\30\1\uffff\1\65",
            "\1\67\1\uffff\1\66",
            "\1\67\1\uffff\1\66",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\70\1\71",
            "\1\72\1\uffff\1\73\34\uffff\1\74\1\75",
            "\1\76\1\77",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51\1\uffff\1\100",
            "\1\50\1\51\1\uffff\1\100",
            "\1\101\1\102",
            "\1\50\1\51",
            "\1\50\1\51"
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }
        public String getDescription() {
            return "390:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleShellModel_in_entryRuleShellModel75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleShellModel85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_ruleShellModel131 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleShellModel152 = new BitSet(new long[]{0x0000006000081002L});
        public static final BitSet FOLLOW_ruleCommand_in_ruleShellModel173 = new BitSet(new long[]{0x0000006000081002L});
        public static final BitSet FOLLOW_ruleCommand_in_entryRuleCommand210 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleCommand220 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentCommand_in_ruleCommand266 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentCommand_in_entryRuleFragmentCommand300 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentCommand310 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragment_in_ruleFragmentCommand355 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragment_in_entryRuleFragment390 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragment400 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleFragment456 = new BitSet(new long[]{0x0000006000081000L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleFragment477 = new BitSet(new long[]{0x0000006000081000L});
        public static final BitSet FOLLOW_ruleFragmentType_in_ruleFragment501 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleFragment514 = new BitSet(new long[]{0x0000000000002030L});
        public static final BitSet FOLLOW_13_in_ruleFragment527 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFragment548 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_14_in_ruleFragment560 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFragment583 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_ruleFragment595 = new BitSet(new long[]{0x0000000000080030L});
        public static final BitSet FOLLOW_ruleObject_in_ruleFragment616 = new BitSet(new long[]{0x0000000000090030L});
        public static final BitSet FOLLOW_ruleObject_in_ruleFragment637 = new BitSet(new long[]{0x0000000000090030L});
        public static final BitSet FOLLOW_16_in_ruleFragment650 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature686 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeature696 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_ruleFeature743 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_ruleFeature770 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString806 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString817 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleEString857 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleEString883 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObject_in_entryRuleObject928 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObject938 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleObject994 = new BitSet(new long[]{0x0000000000080030L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleObject1015 = new BitSet(new long[]{0x0000000000080030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObject1039 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleObject1051 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObject1072 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_ruleObject1084 = new BitSet(new long[]{0x000000001C090000L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleObject1106 = new BitSet(new long[]{0x000000001C0D0000L});
        public static final BitSet FOLLOW_18_in_ruleObject1119 = new BitSet(new long[]{0x000000001C090000L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleObject1143 = new BitSet(new long[]{0x000000001C0D0000L});
        public static final BitSet FOLLOW_18_in_ruleObject1156 = new BitSet(new long[]{0x000000001C090000L});
        public static final BitSet FOLLOW_16_in_ruleObject1174 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_entryRuleAnnotation1210 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotation1220 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_ruleAnnotation1257 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAnnotation1278 = new BitSet(new long[]{0x0000000000100002L});
        public static final BitSet FOLLOW_20_in_ruleAnnotation1291 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_ruleAnnotation1312 = new BitSet(new long[]{0x0000000000600000L});
        public static final BitSet FOLLOW_21_in_ruleAnnotation1325 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_ruleAnnotation1346 = new BitSet(new long[]{0x0000000000600000L});
        public static final BitSet FOLLOW_22_in_ruleAnnotation1360 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam1398 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotationParam1408 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAnnotationParam1454 = new BitSet(new long[]{0x0000000002800000L});
        public static final BitSet FOLLOW_ruleParamValue_in_ruleAnnotationParam1475 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParamValue_in_entryRuleParamValue1511 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleParamValue1521 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_ruleParamValue1568 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_ruleParamValue1595 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam1630 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam1640 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_ruleObjectValueAnnotationParam1677 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam1700 = new BitSet(new long[]{0x0000000001000002L});
        public static final BitSet FOLLOW_24_in_ruleObjectValueAnnotationParam1713 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam1736 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam1774 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam1784 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_rulePrimitiveValueAnnotationParam1821 = new BitSet(new long[]{0x0000001800000050L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_rulePrimitiveValueAnnotationParam1842 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_entryRuleAttribute1878 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttribute1888 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleAttribute1944 = new BitSet(new long[]{0x0000000004080000L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleAttribute1965 = new BitSet(new long[]{0x0000000004080000L});
        public static final BitSet FOLLOW_26_in_ruleAttribute1980 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAttribute2001 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleAttribute2013 = new BitSet(new long[]{0x0000001800000050L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_ruleAttribute2034 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_21_in_ruleAttribute2047 = new BitSet(new long[]{0x0000001800000050L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_ruleAttribute2068 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue2106 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValue2116 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_rulePrimitiveValue2163 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_rulePrimitiveValue2190 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_rulePrimitiveValue2217 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue2252 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntegerValue2262 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerValue2303 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_entryRuleStringValue2343 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringValue2353 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleStringValue2394 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue2434 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValue2444 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_ruleBooleanValue2489 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_entryRuleReference2524 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReference2534 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleReference2590 = new BitSet(new long[]{0x0000000018080000L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleReference2611 = new BitSet(new long[]{0x0000000018080000L});
        public static final BitSet FOLLOW_ruleEdgeProperties_in_ruleReference2635 = new BitSet(new long[]{0x0000000018000000L});
        public static final BitSet FOLLOW_27_in_ruleReference2649 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_28_in_ruleReference2667 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference2689 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleReference2701 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference2724 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_21_in_ruleReference2737 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference2760 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_ruleEdgeProperties_in_entryRuleEdgeProperties2798 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEdgeProperties2808 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_ruleEdgeProperties2854 = new BitSet(new long[]{0x0000000020000000L});
        public static final BitSet FOLLOW_29_in_ruleEdgeProperties2866 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_20_in_ruleEdgeProperties2878 = new BitSet(new long[]{0x0000000040000000L});
        public static final BitSet FOLLOW_30_in_ruleEdgeProperties2890 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties2902 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleEdgeProperties2923 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleEdgeProperties2935 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_31_in_ruleEdgeProperties2947 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties2959 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleEdgeProperties2976 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleEdgeProperties2993 = new BitSet(new long[]{0x0000000100000000L});
        public static final BitSet FOLLOW_32_in_ruleEdgeProperties3005 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties3017 = new BitSet(new long[]{0x0000038100000000L});
        public static final BitSet FOLLOW_ruleLineType_in_ruleEdgeProperties3038 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleEdgeProperties3050 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_33_in_ruleEdgeProperties3062 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties3074 = new BitSet(new long[]{0x7FFFFC0000000000L});
        public static final BitSet FOLLOW_ruleArrowType_in_ruleEdgeProperties3095 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleEdgeProperties3107 = new BitSet(new long[]{0x0000000400000000L});
        public static final BitSet FOLLOW_34_in_ruleEdgeProperties3119 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties3131 = new BitSet(new long[]{0x7FFFFC0000000000L});
        public static final BitSet FOLLOW_ruleArrowType_in_ruleEdgeProperties3152 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_22_in_ruleEdgeProperties3164 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal3201 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValueTerminal3212 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_35_in_ruleBooleanValueTerminal3250 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_ruleBooleanValueTerminal3269 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_ruleFragmentType3323 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_38_in_ruleFragmentType3340 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_ruleLineType3385 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_39_in_ruleLineType3402 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_40_in_ruleLineType3419 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_41_in_ruleLineType3436 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_42_in_ruleArrowType3481 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_43_in_ruleArrowType3498 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_44_in_ruleArrowType3515 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_45_in_ruleArrowType3532 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_46_in_ruleArrowType3549 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_47_in_ruleArrowType3566 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_48_in_ruleArrowType3583 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_49_in_ruleArrowType3600 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_50_in_ruleArrowType3617 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_51_in_ruleArrowType3634 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_52_in_ruleArrowType3651 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_ruleArrowType3668 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_54_in_ruleArrowType3685 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_55_in_ruleArrowType3702 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_ruleArrowType3719 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_57_in_ruleArrowType3736 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_58_in_ruleArrowType3753 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_59_in_ruleArrowType3770 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_60_in_ruleArrowType3787 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_61_in_ruleArrowType3804 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_62_in_ruleArrowType3821 = new BitSet(new long[]{0x0000000000000002L});
    }


}