/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package History;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link History.Solution#getChanges <em>Changes</em>}</li>
 *   <li>{@link History.Solution#getProposal <em>Proposal</em>}</li>
 * </ul>
 * </p>
 *
 * @see History.HistoryPackage#getSolution()
 * @model
 * @generated
 */
public interface Solution extends Collaboration {
	/**
	 * Returns the value of the '<em><b>Changes</b></em>' containment reference list.
	 * The list contents are of type {@link History.ModelChange}.
	 * It is bidirectional and its opposite is '{@link History.ModelChange#getSolution <em>Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Changes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Changes</em>' containment reference list.
	 * @see History.HistoryPackage#getSolution_Changes()
	 * @see History.ModelChange#getSolution
	 * @model opposite="solution" containment="true"
	 * @generated
	 */
	EList<ModelChange> getChanges();

	/**
	 * Returns the value of the '<em><b>Proposal</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link History.Proposal#getSols <em>Sols</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Proposal</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Proposal</em>' container reference.
	 * @see #setProposal(Proposal)
	 * @see History.HistoryPackage#getSolution_Proposal()
	 * @see History.Proposal#getSols
	 * @model opposite="sols" transient="false"
	 * @generated
	 */
	Proposal getProposal();

	/**
	 * Sets the value of the '{@link History.Solution#getProposal <em>Proposal</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Proposal</em>' container reference.
	 * @see #getProposal()
	 * @generated
	 */
	void setProposal(Proposal value);

} // Solution
