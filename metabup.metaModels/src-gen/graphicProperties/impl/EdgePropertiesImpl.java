/**
 */
package graphicProperties.impl;

import graphicProperties.ArrowType;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import graphicProperties.EdgeProperties;
import graphicProperties.GraphicPropertiesPackage;
import graphicProperties.LineType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Edge Properties</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link graphicProperties.impl.EdgePropertiesImpl#getColor <em>Color</em>}</li>
 *   <li>{@link graphicProperties.impl.EdgePropertiesImpl#getWidth <em>Width</em>}</li>
 *   <li>{@link graphicProperties.impl.EdgePropertiesImpl#getLineType <em>Line Type</em>}</li>
 *   <li>{@link graphicProperties.impl.EdgePropertiesImpl#getSrcArrow <em>Src Arrow</em>}</li>
 *   <li>{@link graphicProperties.impl.EdgePropertiesImpl#getTgtArrow <em>Tgt Arrow</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EdgePropertiesImpl extends GraphicPropertiesImpl implements EdgeProperties {
	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final String COLOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected String color = COLOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected int width = WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getLineType() <em>Line Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineType()
	 * @generated
	 * @ordered
	 */
	protected static final LineType LINE_TYPE_EDEFAULT = LineType.LINE;

	/**
	 * The cached value of the '{@link #getLineType() <em>Line Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineType()
	 * @generated
	 * @ordered
	 */
	protected LineType lineType = LINE_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSrcArrow() <em>Src Arrow</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcArrow()
	 * @generated
	 * @ordered
	 */
	protected static final ArrowType SRC_ARROW_EDEFAULT = ArrowType.NONE;

	/**
	 * The cached value of the '{@link #getSrcArrow() <em>Src Arrow</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcArrow()
	 * @generated
	 * @ordered
	 */
	protected ArrowType srcArrow = SRC_ARROW_EDEFAULT;

	/**
	 * The default value of the '{@link #getTgtArrow() <em>Tgt Arrow</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTgtArrow()
	 * @generated
	 * @ordered
	 */
	protected static final ArrowType TGT_ARROW_EDEFAULT = ArrowType.NONE;

	/**
	 * The cached value of the '{@link #getTgtArrow() <em>Tgt Arrow</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTgtArrow()
	 * @generated
	 * @ordered
	 */
	protected ArrowType tgtArrow = TGT_ARROW_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EdgePropertiesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GraphicPropertiesPackage.Literals.EDGE_PROPERTIES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(String newColor) {
		String oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GraphicPropertiesPackage.EDGE_PROPERTIES__COLOR, oldColor, color));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWidth(int newWidth) {
		int oldWidth = width;
		width = newWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GraphicPropertiesPackage.EDGE_PROPERTIES__WIDTH, oldWidth, width));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LineType getLineType() {
		return lineType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLineType(LineType newLineType) {
		LineType oldLineType = lineType;
		lineType = newLineType == null ? LINE_TYPE_EDEFAULT : newLineType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GraphicPropertiesPackage.EDGE_PROPERTIES__LINE_TYPE, oldLineType, lineType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrowType getSrcArrow() {
		return srcArrow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSrcArrow(ArrowType newSrcArrow) {
		ArrowType oldSrcArrow = srcArrow;
		srcArrow = newSrcArrow == null ? SRC_ARROW_EDEFAULT : newSrcArrow;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GraphicPropertiesPackage.EDGE_PROPERTIES__SRC_ARROW, oldSrcArrow, srcArrow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArrowType getTgtArrow() {
		return tgtArrow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTgtArrow(ArrowType newTgtArrow) {
		ArrowType oldTgtArrow = tgtArrow;
		tgtArrow = newTgtArrow == null ? TGT_ARROW_EDEFAULT : newTgtArrow;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GraphicPropertiesPackage.EDGE_PROPERTIES__TGT_ARROW, oldTgtArrow, tgtArrow));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GraphicPropertiesPackage.EDGE_PROPERTIES__COLOR:
				return getColor();
			case GraphicPropertiesPackage.EDGE_PROPERTIES__WIDTH:
				return getWidth();
			case GraphicPropertiesPackage.EDGE_PROPERTIES__LINE_TYPE:
				return getLineType();
			case GraphicPropertiesPackage.EDGE_PROPERTIES__SRC_ARROW:
				return getSrcArrow();
			case GraphicPropertiesPackage.EDGE_PROPERTIES__TGT_ARROW:
				return getTgtArrow();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GraphicPropertiesPackage.EDGE_PROPERTIES__COLOR:
				setColor((String)newValue);
				return;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__WIDTH:
				setWidth((Integer)newValue);
				return;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__LINE_TYPE:
				setLineType((LineType)newValue);
				return;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__SRC_ARROW:
				setSrcArrow((ArrowType)newValue);
				return;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__TGT_ARROW:
				setTgtArrow((ArrowType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GraphicPropertiesPackage.EDGE_PROPERTIES__COLOR:
				setColor(COLOR_EDEFAULT);
				return;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__WIDTH:
				setWidth(WIDTH_EDEFAULT);
				return;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__LINE_TYPE:
				setLineType(LINE_TYPE_EDEFAULT);
				return;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__SRC_ARROW:
				setSrcArrow(SRC_ARROW_EDEFAULT);
				return;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__TGT_ARROW:
				setTgtArrow(TGT_ARROW_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GraphicPropertiesPackage.EDGE_PROPERTIES__COLOR:
				return COLOR_EDEFAULT == null ? color != null : !COLOR_EDEFAULT.equals(color);
			case GraphicPropertiesPackage.EDGE_PROPERTIES__WIDTH:
				return width != WIDTH_EDEFAULT;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__LINE_TYPE:
				return lineType != LINE_TYPE_EDEFAULT;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__SRC_ARROW:
				return srcArrow != SRC_ARROW_EDEFAULT;
			case GraphicPropertiesPackage.EDGE_PROPERTIES__TGT_ARROW:
				return tgtArrow != TGT_ARROW_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();
		
		String generatedName = "";
		generatedName += color.toString();
		generatedName += "_";
		generatedName += width;
		generatedName += "_";
		generatedName += lineType;
		generatedName += "_";
		generatedName += srcArrow;
		generatedName += "_";
		generatedName += tgtArrow;
		
		return generatedName;
	}
	
	@Override
	public boolean equals(EdgeProperties prop) {
		if(this.getColor().toLowerCase().equals(prop.getColor().toLowerCase()))
			if(this.getWidth() == prop.getWidth())
				if(this.getLineType().equals(prop.getLineType()))
					if(this.getSrcArrow().equals(prop.getSrcArrow()))
						if(this.getTgtArrow().equals(prop.getTgtArrow()))
							return true;
				
		return false;
	}

} //EdgePropertiesImpl
