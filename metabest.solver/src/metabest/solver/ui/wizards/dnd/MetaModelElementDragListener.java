/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.dnd;


import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;
import org.eclipse.swt.dnd.TextTransfer;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;

public class MetaModelElementDragListener implements DragSourceListener {
	private final DragMetaModelElementViewer viewer;
	
	public MetaModelElementDragListener(DragMetaModelElementViewer viewer){
		this.viewer = viewer;
		
	}
	
	@Override
	public void dragStart(DragSourceEvent event) {
	}

	@Override
	public void dragSetData(DragSourceEvent event) {
		// Here you do the convertion to the type which is expected.
	    IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
	    
	    String data = ((AnnotatedElement)selection.getFirstElement()).getId();	    
	    	    
	    if (TextTransfer.getInstance().isSupportedType(event.dataType)) {
	      event.data = data;
	    }
	}

	@Override
	public void dragFinished(DragSourceEvent event) {
		
	}

}
