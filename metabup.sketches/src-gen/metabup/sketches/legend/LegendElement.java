/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.sketches.legend;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metabup.sketches.legend.LegendElement#getOriginalSymbolName <em>Original Symbol Name</em>}</li>
 *   <li>{@link metabup.sketches.legend.LegendElement#getNewSymbolName <em>New Symbol Name</em>}</li>
 *   <li>{@link metabup.sketches.legend.LegendElement#getDefaultWidth <em>Default Width</em>}</li>
 *   <li>{@link metabup.sketches.legend.LegendElement#getDefaultHeight <em>Default Height</em>}</li>
 *   <li>{@link metabup.sketches.legend.LegendElement#getColor <em>Color</em>}</li>
 *   <li>{@link metabup.sketches.legend.LegendElement#isTransparent <em>Transparent</em>}</li>
 *   <li>{@link metabup.sketches.legend.LegendElement#getFile <em>File</em>}</li>
 * </ul>
 *
 * @see metabup.sketches.legend.LegendPackage#getLegendElement()
 * @model
 * @generated
 */
public interface LegendElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Original Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Original Symbol Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Original Symbol Name</em>' attribute.
	 * @see #setOriginalSymbolName(String)
	 * @see metabup.sketches.legend.LegendPackage#getLegendElement_OriginalSymbolName()
	 * @model required="true"
	 * @generated
	 */
	String getOriginalSymbolName();

	/**
	 * Sets the value of the '{@link metabup.sketches.legend.LegendElement#getOriginalSymbolName <em>Original Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Original Symbol Name</em>' attribute.
	 * @see #getOriginalSymbolName()
	 * @generated
	 */
	void setOriginalSymbolName(String value);

	/**
	 * Returns the value of the '<em><b>New Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>New Symbol Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Symbol Name</em>' attribute.
	 * @see #setNewSymbolName(String)
	 * @see metabup.sketches.legend.LegendPackage#getLegendElement_NewSymbolName()
	 * @model required="true"
	 * @generated
	 */
	String getNewSymbolName();

	/**
	 * Sets the value of the '{@link metabup.sketches.legend.LegendElement#getNewSymbolName <em>New Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Symbol Name</em>' attribute.
	 * @see #getNewSymbolName()
	 * @generated
	 */
	void setNewSymbolName(String value);

	/**
	 * Returns the value of the '<em><b>Default Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Width</em>' attribute.
	 * @see #setDefaultWidth(float)
	 * @see metabup.sketches.legend.LegendPackage#getLegendElement_DefaultWidth()
	 * @model
	 * @generated
	 */
	float getDefaultWidth();

	/**
	 * Sets the value of the '{@link metabup.sketches.legend.LegendElement#getDefaultWidth <em>Default Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Width</em>' attribute.
	 * @see #getDefaultWidth()
	 * @generated
	 */
	void setDefaultWidth(float value);

	/**
	 * Returns the value of the '<em><b>Default Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Height</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Height</em>' attribute.
	 * @see #setDefaultHeight(float)
	 * @see metabup.sketches.legend.LegendPackage#getLegendElement_DefaultHeight()
	 * @model
	 * @generated
	 */
	float getDefaultHeight();

	/**
	 * Sets the value of the '{@link metabup.sketches.legend.LegendElement#getDefaultHeight <em>Default Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Height</em>' attribute.
	 * @see #getDefaultHeight()
	 * @generated
	 */
	void setDefaultHeight(float value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(String)
	 * @see metabup.sketches.legend.LegendPackage#getLegendElement_Color()
	 * @model
	 * @generated
	 */
	String getColor();

	/**
	 * Sets the value of the '{@link metabup.sketches.legend.LegendElement#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(String value);

	/**
	 * Returns the value of the '<em><b>Transparent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transparent</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transparent</em>' attribute.
	 * @see #setTransparent(boolean)
	 * @see metabup.sketches.legend.LegendPackage#getLegendElement_Transparent()
	 * @model
	 * @generated
	 */
	boolean isTransparent();

	/**
	 * Sets the value of the '{@link metabup.sketches.legend.LegendElement#isTransparent <em>Transparent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transparent</em>' attribute.
	 * @see #isTransparent()
	 * @generated
	 */
	void setTransparent(boolean value);

	/**
	 * Returns the value of the '<em><b>File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>File</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File</em>' attribute.
	 * @see #setFile(String)
	 * @see metabup.sketches.legend.LegendPackage#getLegendElement_File()
	 * @model
	 * @generated
	 */
	String getFile();

	/**
	 * Sets the value of the '{@link metabup.sketches.legend.LegendElement#getFile <em>File</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File</em>' attribute.
	 * @see #getFile()
	 * @generated
	 */
	void setFile(String value);

} // LegendElement
