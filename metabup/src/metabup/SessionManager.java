/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;


/**
 * Singleton class to register meta-modeling sessions.
 * 
 * @author jesus
 *
 */
@SuppressWarnings("restriction")
public class SessionManager {
	public static final SessionManager INSTANCE = new SessionManager();
	private HashMap<String, SessionState> states = new HashMap<String, SessionState>();
	
	/**
	 * Returns a session for a given mbupFile. It may create support files
	 * if needed.
	 * 
	 * @param mbupFile The 
	 * @return A session state
	 * @throws CoreException 
	 * @throws IOException 
	 */
	public SessionState sessionFor(IFile mbupFile) throws CoreException, IOException {
		SessionState state = null;
		String key = mbupFile.getLocation().toString();
		System.out.println("hi");
		String metamodelName = mbupFile.getName().substring(0, mbupFile.getName().length() - ".mbup".length()); // remove mbup
		
		if ( ! states.containsKey(key) ) { 		
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
				
			// Create the file used as shell
			IFile fragFile = root.getFile(mbupFile.getFullPath().removeFileExtension().addFileExtension("mbupf"));			
	        if ( ! fragFile.exists() ) {
	        	fragFile.create(new ByteArrayInputStream(new byte[] {}), false, null);
	        	fragFile.setHidden(true);
	        }
	        
	        // Create the file used as metabup.collaboro.metamodels.history
			IFile historyFile = root.getFile(mbupFile.getFullPath().removeFileExtension().addFileExtension("history.mmcomands"));			
	        if ( ! historyFile.exists() ) { 
	        	historyFile.create(new ByteArrayInputStream( ("shell " + metamodelName + "\n").getBytes() ), false, null);
	        	historyFile.setDerived(true, null);
	        	historyFile.setHidden(true);
	        }	        
	        
	        ResourceSet rs = new ResourceSetImpl();
        	rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
            	    Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
	        rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("mm.xmi", new XMIResourceFactoryImpl());
	        
	        /*Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
	        reg.getExtensionToFactoryMap().put("mm.xmi", new XMIResourceFactoryImpl());	*/        
	        
	        IFile mmFile = root.getFile(mbupFile.getFullPath().removeFileExtension().addFileExtension("mm.xmi"));
	        
	        Resource resource = new XMIResourceImpl(URI.createURI(mmFile.getLocationURI().toString()));
	        rs.getResources().add(resource);
	        
	        if ( ! mmFile.exists() ) {		        
		       	MetaModel metamodel = MetamodelFactory.eINSTANCE.createMetaModel();
		       	metamodel.setName(fragFile.getName());	        	
		       	resource.getContents().add(metamodel);	        	
		        
		       	mmFile.create(null, true, null);
		       	mmFile.refreshLocal(1, null);
		       	mmFile.setHidden(true);
		       	resource.save(null);
		    } else {
		    	try {    
		    		InputStream stream = new FileInputStream(IFileUtils.IFile2File(mmFile));
		    		resource.load( stream, null );
					} catch (IOException e) {
						/*System.out.println(e.getLocalizedMessage());
						System.out.println(e.getCause());
						throw new ResourceException(Status.ERROR, mmFile.getLocation(), e.getMessage(), e);		*/
						resource= rs.getResource(URI.createURI(mmFile.getLocationURI().toString()), true);
						resource.load(null);
					}        	
		        }        	        
        	
        	state = new SessionState(fragFile, historyFile, mmFile, resource);	
	        states.put(key, state);
		}
		
        return states.get(key);
	}

	
}
