/****************************************************************************
 * This demo file is part of the yFiles extension package ySVG-2.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * ySVG demo files exhibit ySVG functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a ySVG version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own ySVG powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the ySVG
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.svg;

import yext.svg.io.SVGIconSerializer;
import yext.svg.io.SVGNodeRealizerSerializer;

import y.io.GraphMLIOHandler;
import y.util.D;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;


/**
 * Demonstrates how to register {@link SVGNodeRealizerSerializer} to enable
 * serialization of graphs that use {@link yext.svg.view.SVGNodeRealizer}.
 *
 * @see <a href="http://docs.yworks.com/yfiles/doc/developers-guide/svg_graphml_support.html#svg_graphml_support">Section GraphML Support</a> in the yFiles for Java Developer's Guide
 */
public class GraphMLDemo extends SVGNodeRealizerDemo {

  /**
   * Factory method that creates an instance of <code>GraphMLIOHandler</code>
   * which is configured to support serialization of graphs that use
   * {@link yext.svg.view.SVGNodeRealizer}.
   *
   * @return an instance of <code>GraphMLIOHandler</code>
   * which is configured to support serialization of graphs that use
   * {@link yext.svg.view.SVGNodeRealizer}.
   */
  protected GraphMLIOHandler createIOHandler() {
    final GraphMLIOHandler ioh = new GraphMLIOHandler();

    // register a custom serializer to support SVG nodes
    ioh.addNodeRealizerSerializer(new SVGNodeRealizerSerializer());

    // register custom deserialization code for SVGIcons
//    ioh.getGraphMLHandler().addDeserializationHandler(new SVGIconSerializer());

    // register custom serialization code for SVGIcons
//    ioh.getGraphMLHandler().addSerializationHandler(new SVGIconSerializer());

    return ioh;
  }

  /**
   * Overwritten to allow deserialization from GraphML format only.
   */
  protected Action createLoadAction() {
    return new GraphMLLoadAction();
  }

  /**
   * Overwritten to allow serialization to GraphML format only.
   */
  protected Action createSaveAction() {
    return new GraphMLSaveAction();
  }


  /**
   * Launches this demo.
   * @param args ignored.
   */
  public static void main( String[] args ) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        initLnF();
        (new GraphMLDemo()).start();
      }
    });
  }


  /*
   * #####################################################################
   * inner types
   * #####################################################################
   */

  /**
   * Action that loads the current graph from a file in GraphML format.
   */
  private class GraphMLLoadAction extends AbstractAction {
    GraphMLIOHandler ioh;
    JFileChooser chooser;

    public GraphMLLoadAction() {
      super("Load...");
      ioh = null;
      chooser = null;
    }

    public void actionPerformed(ActionEvent e) {
      if (ioh == null) {
        ioh = createIOHandler();
      }
      if (chooser == null) {
        chooser = new JFileChooser();
        chooser.addChoosableFileFilter(new FileFilter() {
          public boolean accept(final File f) {
            return f.isDirectory() || f.getName().toLowerCase().endsWith(".graphml");
          }

          public String getDescription() {
            return "GraphML (*.graphml)";
          }
        });
      }
      if (chooser.showOpenDialog(GraphMLDemo.this) == JFileChooser.APPROVE_OPTION) {
        String name = chooser.getSelectedFile().toString();
        if (!name.endsWith(".graphml")) {
          name = name + ".graphml";
        }
        try {
          view.getGraph2D().clear();
          ioh.read(view.getGraph2D(), name);
        } catch (IOException ioe) {
          D.show(ioe);
        }
        //force redisplay of view contents
        view.fitContent();
        view.getGraph2D().updateViews();
      }
    }
  }

  /**
   * Action that saves the current graph to a file in GraphML format.
   */
  private class GraphMLSaveAction extends AbstractAction {
    GraphMLIOHandler ioh;
    JFileChooser chooser;

    public GraphMLSaveAction() {
      super("Save...");
      ioh = null;
      chooser = null;
    }

    public void actionPerformed(final ActionEvent e) {
      if (ioh == null) {
        ioh = createIOHandler();
      }
      if (chooser == null) {
        chooser = new JFileChooser();
        chooser.addChoosableFileFilter(new FileFilter() {
          public boolean accept(final File f) {
            return f.isDirectory() || f.getName().toLowerCase().endsWith(".graphml");
          }

          public String getDescription() {
            return "GraphML (*.graphml)";
          }
        });
      }
      if (chooser.showSaveDialog(GraphMLDemo.this) == JFileChooser.APPROVE_OPTION) {
        String name = chooser.getSelectedFile().toString();
        if (!name.endsWith(".graphml")) {
          name = name + ".graphml";
        }
        try {
          ioh.write(view.getGraph2D(), name);
        } catch (IOException ioe) {
          D.show(ioe);
        }
      }
    }
  }
}