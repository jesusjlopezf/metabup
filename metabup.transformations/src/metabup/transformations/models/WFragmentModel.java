/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations.models;

import java.util.Collection;

import org.eclipse.emf.ecore.resource.Resource;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.FragmentsPackage;

public class WFragmentModel extends BaseModel {

	public WFragmentModel(Resource resource) {
		super(resource);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Fragment> fragments() {
		return (Collection<Fragment>) allObjectsOf(FragmentsPackage.eINSTANCE.getFragment());
	}

	public FragmentModel rootFragmentModel() {
		if(resource.getContents() != null && !resource.getContents().isEmpty()) return (FragmentModel) resource.getContents().get(0);
		else return null;
	}

	@SuppressWarnings("unchecked")
	public Collection<fragments.Object> allFragmentObjects() {
		return (Collection<fragments.Object>) allObjectsOf(FragmentsPackage.eINSTANCE.getObject());		
	}

	/*
	public void forAllFragmentModel(IClosure<?> closure) {
		applyForAllObjectsOf(FragmentsPackage.eINSTANCE.getFragmentModel(), closure);		
	}
	*/

}
