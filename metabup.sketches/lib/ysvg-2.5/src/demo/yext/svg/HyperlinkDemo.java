/****************************************************************************
 * This demo file is part of the yFiles extension package ySVG-2.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * ySVG demo files exhibit ySVG functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a ySVG version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own ySVG powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the ySVG
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.svg;

import yext.svg.io.SVGDOMEnhancer;
import yext.svg.io.SVGIOHandler;

import y.base.Edge;
import y.base.Node;
import y.option.OptionHandler;
import y.view.EdgeLabel;
import y.view.EdgeRealizer;
import y.view.EditMode;
import y.view.HitInfo;
import y.view.NodeLabel;
import y.view.NodeRealizer;
import y.view.ViewMode;

import java.awt.EventQueue;
import java.util.HashMap;
import java.util.Map;

/** 
 * Demonstrates how to write SVG documents with hyperlinks for nodes, edges, and
 * labels. 
 * <p>
 * Basic Usage: After nodes, edges, and labels have been created in the editor,
 * it is possible to associate an URL with these objects by right clicking on
 * them.
 * </p>
 * @see <a href="http://docs.yworks.com/yfiles/doc/developers-guide/svg_customization.html#svg_customization">Section Customizing SVG Content</a> in the yFiles for Java Developer's Guide
 */
public class HyperlinkDemo extends SVGExportDemo
{
  //Map used to store URLs for nodes, edges and labels.
  Map linkMap;
  
  /**
   * Initializes a new instance of <code>HyperlinkDemo</code>.
   */
  public HyperlinkDemo()
  {
    //add right click handler
    view.addViewMode(new RightClickMode());
    
    //configure default edge
    EdgeRealizer er = view.getGraph2D().getDefaultEdgeRealizer();
    EdgeLabel el = er.getLabel();
    el.setText("EDGE LABEL");

    //configure default node
    NodeRealizer nr = view.getGraph2D().getDefaultNodeRealizer();
    nr.setSize(120, 40);
    NodeLabel nl = nr.getLabel();
    nl.setText("LABEL");
    linkMap = new HashMap();
  }
  
  /**
   * Create an edit mode that displays the URL
   * of a node as tooltip.
   */
  protected EditMode createEditMode()
  {
    EditMode editMode = new EditMode() {
      public String getNodeTip(Node v) {
        String url = (String)linkMap.get(v);
        if(url != null)
        {
          return "<html>URL: <b>" + url + "</b></html>";
        }
        else
        {
          return "<html>No URL assigned to node. <br><b>Right click on node</b></html>";
        }
      }
    };
    editMode.showNodeTips(true);
    return editMode;
  }
  
  /**
   * Creates and configures the SVGIOHandlers to be used.
   * This method will add a specialized SVGDOMEnhancer to
   * the IOHandler.
   */
  protected SVGIOHandler createSVGIOHandler(boolean svgz)
  {
    SVGIOHandler ioh = super.createSVGIOHandler(svgz);
    SVGDOMEnhancer enhancer = new HttpLinksEnhancer();
    ioh.setSVGGraph2DRenderer(enhancer);
    return ioh;
  }
  
  /**
   * A custom SVGDOMEnhancer. This class will add hyperlink statements 
   * around each node, node label, edge and
   * edge label for which the user has given a URL.
   */
  class HttpLinksEnhancer extends SVGDOMEnhancer
  {
    protected void initializeDOM() {
      final String function =
              "function yhref(evt) {\n" +
              "  if (evt.charAt(0) != '#') {\n" +
              "    window.parent.location.href = evt;\n" +
              "  } else {\n" +
              "    window.parent.location.hash = evt;\n" +
              "  }\n" +
              "}";
      org.w3c.dom.Element script = createElement("script");
      script.setAttribute("type", "text/ecmascript");
      script.appendChild(createCDATASection(function));
      addToSVGDefinition(script);
    }

    /**
     * Override a callback method. Hyperlink the given node.
     */
    protected void nodeAddedToDOM(Node yNode, org.w3c.dom.Element element)
    {
      insertLink(yNode, element, true);
    }
    
    /**
     * Override a callback method. Hyperlink the given edge.
     */
    protected void edgeAddedToDOM(Edge yEdge, org.w3c.dom.Element element)
    {
      insertLink(yEdge, element, true);
    }
    
    
    /**
     * Override a callback method. Hyperlink the given node label.
     */
    protected void nodeLabelAddedToDOM(NodeLabel yNodeLabel, org.w3c.dom.Element element)
    {
      insertLink(yNodeLabel, element, false);
    }

    /**
     * Override a callback method. Hyperlink the given edge label.
     */
    protected void edgeLabelAddedToDOM(EdgeLabel yEdgeLabel, org.w3c.dom.Element element)
    {
      insertLink(yEdgeLabel, element, false);
    }
    
    /**
     * Inserts a new hyperlink element in the dom tree. The URL of the hyperlink
     * will be looked up in <code>linkMap</code>.
     * @param item the graph item (node, node label, edge, or edge label) for
     * which a hyperlink is created.
     * @param element the DOM element representing the specified graph item in
     * the generated SVG document.
     * @param notALabel <code>true</code> if the specified graph element is
     * neither a node label nor an edge label; <code>false</code> otherwise.
     */   
    private void insertLink(Object item, org.w3c.dom.Element element, boolean notALabel)
    {
      String url = (String)linkMap.get(item);
      if(url != null) {
        if (notALabel) {
          org.w3c.dom.Element a = createElement("a");
          element.setAttribute("onclick", "yhref('" + url + "')");
          insertNodeBelow(a, element);
        } else {
          if (!"text".equals(element.getLocalName())) {
            org.w3c.dom.NodeList text = element.getElementsByTagName("text");
            element = text.getLength() > 0 ? (org.w3c.dom.Element)text.item(0) : null;
          }
          if (element != null) {
            element.setAttribute("onclick", "yhref('" + url + "')");
          }
        }
      }
    }
  }
  
  /**
   * ViewMode reponsible for processing a right mouse click.
   * Right clicking on nodes, edges and labels will bring up
   * an option handler that allows to enter a URL for each item.
   * Hyperlinks to these URLs will be integrated in the SVG output.
   */   
  class RightClickMode extends ViewMode
  {
    public void mouseReleasedRight(double x, double y)
    {
      HitInfo hitInfo = new HitInfo(view, x, y, true);

      if(hitInfo.getHitNodeLabel() != null)
      {
        editLinkInfo(hitInfo.getHitNodeLabel(), "node label");
      }
      else if(hitInfo.getHitEdgeLabel() != null)
      {
        editLinkInfo(hitInfo.getHitEdgeLabel(), "edge label");
      }
      else if(hitInfo.getHitNode() != null)
      {
        editLinkInfo(hitInfo.getHitNode(), "node");
      }
      else if(hitInfo.getHitEdge() != null)
      {
        editLinkInfo(hitInfo.getHitEdge(), "edge");
      }
    }
    
    void editLinkInfo(Object key, String type)
    {
      OptionHandler op = new OptionHandler("Hyperlink for " + type);
      String url = (String)linkMap.get(key);
      if(url != null)
      {
        op.addString("Hyperlink URL",url);
      }
      else
      {
        op.addString("Hyperlink URL","");
      }
      if(op.showEditor(view.getFrame()))
      {
        url = op.getString("Hyperlink URL");
        if(url.length() > 0)
        {
          linkMap.put(key, url);
        }
        else
        {
          linkMap.remove(key);
        }
      } 
    }
  }
  
  /**
   * Launches this demo.
   * @param args ignored.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        initLnF();
        (new HyperlinkDemo()).start();
      }
    });
  }
}