/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.wizards;

import org.eclipse.core.resources.IResource;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

// TODO: Auto-generated Javadoc
/**
 * The Class TransformNewWizardPage.
 */
public class NewMetaBupProjectWizardPage extends WizardPage {
	
	/** The editor. */
	protected FileFieldEditor editor;
	
	/** The folder text. */
	private Text folderText;	
	
	/** The import cont text. */
	private Text importContText;
	
	/** The selection. */
	@SuppressWarnings("unused")
	private ISelection selection;
	
	/** The result. */
	private Object result[];

	/**
	 * Instantiates a new transform new wizard page.
	 *
	 * @param selection the selection
	 */
	public NewMetaBupProjectWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("New MetaBup Project");
		setDescription("Create a new MetaBup project, featuring an interactive metamodel and a MetaBest testcase package.");
		this.selection = selection;
	}

	/**
	 * Creates the control.
	 *
	 * @param parent the parent
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;			
		
		Label label = new Label(container, SWT.NULL);
		label.setText("&Project name:");

		folderText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		folderText.setLayoutData(gd);
		folderText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		
		label = new Label(container, SWT.NULL);
		label.setText("");
		
		initialize();
		dialogChanged();
		setControl(container);
	}

	/**
	 * Initialize.
	 */
	private void initialize() {
	}

	/**
	 * Dialog changed.
	 */
	private void dialogChanged() {
		//IResource container = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(getContainerName()));
		//String folderName = getFolderName();
		String folderName = folderText.getText();
		
		if (folderName.length() == 0) {
			updateStatus("A name for the project must be specified");
			return;
		}
		
		/*if (container == null
				|| (container.getType() & (IResource.PROJECT | IResource.FOLDER )) == 0) {
			updateStatus("Folder container must exist");
			return;
		}*/
		/*if (!container.isAccessible()) {
			updateStatus("Folder must be writable");
			return;
		}*/
		/*if (folderName.length() == 0) {
			updateStatus("Folder name must be specified");
			return;
		}*/
		/*if (folderName.replace('\\', '/').indexOf('/', 1) > 0) {
			updateStatus("Folder name must be valid");
			return;
		}*/

		updateStatus(null);
	}

	/**
	 * Update status.
	 *
	 * @param message the message
	 */
	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	/**
	 * Gets the folder name.
	 *
	 * @return the folder name
	 */
	public String getFolderName() {
		return folderText.getText();
	}

	// tan solo contiene las rutas de los items seleccionados, concatenadas, y separadas por ;
	/**
	 * Gets the import cont name.
	 *
	 * @return the import cont name
	 */
	public String getImportContName() {
		return importContText.getText();
	}
	
	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public Object[] getResult(){
		return result;
	}
	

	/**
	 * This method checks the correctness of every selected item (only query and xmi files allowed).
	 *
	 * @return true, if successful
	 */
	public boolean checkResultExtensions(){
		if(result.length>0){
			for(int i=0; i<result.length; i++){
				IResource obj = (IResource) result[i];
				String ext = obj.getFileExtension().toString();
				
				// aqui iran todos los formatos admisibles
				if(( !ext.equals("xmi")  )
				  & ( !ext.equals("xq")  )
				  ){
					return false;  
				}
			}
		}
		
		return true;
	}
}