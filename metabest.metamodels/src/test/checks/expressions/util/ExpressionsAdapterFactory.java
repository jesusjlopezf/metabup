/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.expressions.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import test.checks.expressions.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see test.checks.expressions.ExpressionsPackage
 * @generated
 */
public class ExpressionsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ExpressionsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ExpressionsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionsSwitch<Adapter> modelSwitch =
		new ExpressionsSwitch<Adapter>() {
			@Override
			public Adapter caseTypeExpression(TypeExpression object) {
				return createTypeExpressionAdapter();
			}
			@Override
			public Adapter caseObjectTypeExpression(ObjectTypeExpression object) {
				return createObjectTypeExpressionAdapter();
			}
			@Override
			public Adapter caseFeatureTypeExpression(FeatureTypeExpression object) {
				return createFeatureTypeExpressionAdapter();
			}
			@Override
			public Adapter caseMultiplicityExpression(MultiplicityExpression object) {
				return createMultiplicityExpressionAdapter();
			}
			@Override
			public Adapter caseFeatureNatureExpression(FeatureNatureExpression object) {
				return createFeatureNatureExpressionAdapter();
			}
			@Override
			public Adapter caseFeatureWithinObjectExpression(FeatureWithinObjectExpression object) {
				return createFeatureWithinObjectExpressionAdapter();
			}
			@Override
			public Adapter caseObjectExpression(ObjectExpression object) {
				return createObjectExpressionAdapter();
			}
			@Override
			public Adapter caseFragmentElementExpression(FragmentElementExpression object) {
				return createFragmentElementExpressionAdapter();
			}
			@Override
			public Adapter caseMetaClassExpression(MetaClassExpression object) {
				return createMetaClassExpressionAdapter();
			}
			@Override
			public Adapter caseObjectOrMetaClassExpression(ObjectOrMetaClassExpression object) {
				return createObjectOrMetaClassExpressionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.TypeExpression <em>Type Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.TypeExpression
	 * @generated
	 */
	public Adapter createTypeExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.ObjectTypeExpression <em>Object Type Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.ObjectTypeExpression
	 * @generated
	 */
	public Adapter createObjectTypeExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.FeatureTypeExpression <em>Feature Type Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.FeatureTypeExpression
	 * @generated
	 */
	public Adapter createFeatureTypeExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.MultiplicityExpression <em>Multiplicity Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.MultiplicityExpression
	 * @generated
	 */
	public Adapter createMultiplicityExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.FeatureNatureExpression <em>Feature Nature Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.FeatureNatureExpression
	 * @generated
	 */
	public Adapter createFeatureNatureExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.FeatureWithinObjectExpression <em>Feature Within Object Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.FeatureWithinObjectExpression
	 * @generated
	 */
	public Adapter createFeatureWithinObjectExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.ObjectExpression <em>Object Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.ObjectExpression
	 * @generated
	 */
	public Adapter createObjectExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.FragmentElementExpression <em>Fragment Element Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.FragmentElementExpression
	 * @generated
	 */
	public Adapter createFragmentElementExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.MetaClassExpression <em>Meta Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.MetaClassExpression
	 * @generated
	 */
	public Adapter createMetaClassExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.expressions.ObjectOrMetaClassExpression <em>Object Or Meta Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.expressions.ObjectOrMetaClassExpression
	 * @generated
	 */
	public Adapter createObjectOrMetaClassExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ExpressionsAdapterFactory
