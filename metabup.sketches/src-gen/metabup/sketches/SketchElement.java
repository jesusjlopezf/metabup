/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.sketches;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metabup.sketches.SketchElement#getDebugName <em>Debug Name</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getToolSymbolName <em>Tool Symbol Name</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getText <em>Text</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getSkechTypeInfo <em>Skech Type Info</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getX <em>X</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getY <em>Y</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getWidth <em>Width</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getHeight <em>Height</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getContainer <em>Container</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getImplicitlyContainedElements <em>Implicitly Contained Elements</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getSrcOf <em>Src Of</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getTgtOf <em>Tgt Of</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getLabels <em>Labels</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getColor <em>Color</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#isTransparent <em>Transparent</em>}</li>
 *   <li>{@link metabup.sketches.SketchElement#getSourceCode <em>Source Code</em>}</li>
 * </ul>
 *
 * @see metabup.sketches.sketchesPackage#getSketchElement()
 * @model abstract="true"
 * @generated
 */
public interface SketchElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Debug Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Debug Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Debug Name</em>' attribute.
	 * @see #setDebugName(String)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_DebugName()
	 * @model
	 * @generated
	 */
	String getDebugName();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getDebugName <em>Debug Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Debug Name</em>' attribute.
	 * @see #getDebugName()
	 * @generated
	 */
	void setDebugName(String value);

	/**
	 * Returns the value of the '<em><b>Tool Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tool Symbol Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tool Symbol Name</em>' attribute.
	 * @see #setToolSymbolName(String)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_ToolSymbolName()
	 * @model required="true"
	 * @generated
	 */
	String getToolSymbolName();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getToolSymbolName <em>Tool Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tool Symbol Name</em>' attribute.
	 * @see #getToolSymbolName()
	 * @generated
	 */
	void setToolSymbolName(String value);

	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_Text()
	 * @model
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

	/**
	 * Returns the value of the '<em><b>Skech Type Info</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Skech Type Info</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Skech Type Info</em>' attribute.
	 * @see #setSkechTypeInfo(String)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_SkechTypeInfo()
	 * @model required="true"
	 * @generated
	 */
	String getSkechTypeInfo();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getSkechTypeInfo <em>Skech Type Info</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Skech Type Info</em>' attribute.
	 * @see #getSkechTypeInfo()
	 * @generated
	 */
	void setSkechTypeInfo(String value);

	/**
	 * Returns the value of the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' attribute.
	 * @see #setX(Float)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_X()
	 * @model
	 * @generated
	 */
	Float getX();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getX <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' attribute.
	 * @see #getX()
	 * @generated
	 */
	void setX(Float value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' attribute.
	 * @see #setY(Float)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_Y()
	 * @model
	 * @generated
	 */
	Float getY();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getY <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' attribute.
	 * @see #getY()
	 * @generated
	 */
	void setY(Float value);

	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #setWidth(Float)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_Width()
	 * @model
	 * @generated
	 */
	Float getWidth();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(Float value);

	/**
	 * Returns the value of the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Height</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Height</em>' attribute.
	 * @see #setHeight(Float)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_Height()
	 * @model
	 * @generated
	 */
	Float getHeight();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getHeight <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Height</em>' attribute.
	 * @see #getHeight()
	 * @generated
	 */
	void setHeight(Float value);

	/**
	 * Returns the value of the '<em><b>Container</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link metabup.sketches.SketchElement#getImplicitlyContainedElements <em>Implicitly Contained Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' reference.
	 * @see #setContainer(SketchElement)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_Container()
	 * @see metabup.sketches.SketchElement#getImplicitlyContainedElements
	 * @model opposite="implicitlyContainedElements"
	 * @generated
	 */
	SketchElement getContainer();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getContainer <em>Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(SketchElement value);

	/**
	 * Returns the value of the '<em><b>Implicitly Contained Elements</b></em>' reference list.
	 * The list contents are of type {@link metabup.sketches.SketchElement}.
	 * It is bidirectional and its opposite is '{@link metabup.sketches.SketchElement#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implicitly Contained Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implicitly Contained Elements</em>' reference list.
	 * @see metabup.sketches.sketchesPackage#getSketchElement_ImplicitlyContainedElements()
	 * @see metabup.sketches.SketchElement#getContainer
	 * @model opposite="container"
	 * @generated
	 */
	EList<SketchElement> getImplicitlyContainedElements();

	/**
	 * Returns the value of the '<em><b>Src Of</b></em>' reference list.
	 * The list contents are of type {@link metabup.sketches.Connection}.
	 * It is bidirectional and its opposite is '{@link metabup.sketches.Connection#getSrc <em>Src</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src Of</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src Of</em>' reference list.
	 * @see metabup.sketches.sketchesPackage#getSketchElement_SrcOf()
	 * @see metabup.sketches.Connection#getSrc
	 * @model opposite="src"
	 * @generated
	 */
	EList<Connection> getSrcOf();

	/**
	 * Returns the value of the '<em><b>Tgt Of</b></em>' reference list.
	 * The list contents are of type {@link metabup.sketches.Connection}.
	 * It is bidirectional and its opposite is '{@link metabup.sketches.Connection#getTgt <em>Tgt</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgt Of</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tgt Of</em>' reference list.
	 * @see metabup.sketches.sketchesPackage#getSketchElement_TgtOf()
	 * @see metabup.sketches.Connection#getTgt
	 * @model opposite="tgt"
	 * @generated
	 */
	EList<Connection> getTgtOf();

	/**
	 * Returns the value of the '<em><b>Labels</b></em>' containment reference list.
	 * The list contents are of type {@link metabup.sketches.Label}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Labels</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Labels</em>' containment reference list.
	 * @see metabup.sketches.sketchesPackage#getSketchElement_Labels()
	 * @model containment="true"
	 * @generated
	 */
	EList<Label> getLabels();
	
	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(String)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_Color()
	 * @model
	 * @generated
	 */
	String getColor();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(String value);

	/**
	 * Returns the value of the '<em><b>Transparent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transparent</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transparent</em>' attribute.
	 * @see #setTransparent(boolean)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_Transparent()
	 * @model
	 * @generated
	 */
	boolean isTransparent();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#isTransparent <em>Transparent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transparent</em>' attribute.
	 * @see #isTransparent()
	 * @generated
	 */
	void setTransparent(boolean value);

	/**
	 * Returns the value of the '<em><b>Source Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Code</em>' attribute.
	 * @see #setSourceCode(String)
	 * @see metabup.sketches.sketchesPackage#getSketchElement_SourceCode()
	 * @model
	 * @generated
	 */
	String getSourceCode();

	/**
	 * Sets the value of the '{@link metabup.sketches.SketchElement#getSourceCode <em>Source Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Code</em>' attribute.
	 * @see #getSourceCode()
	 * @generated
	 */
	void setSourceCode(String value);

	boolean isOverlapping(SketchElement element, boolean extCall);
	boolean isAdjacent(SketchElement element, int maxDistance, boolean extCall);
	int getAdjacencySide(SketchElement element, int maxDistance, boolean extCall);
	int getAlignmentSide(SketchElement element, int maxDistance, boolean extCall);
	boolean contains(SketchElement element);
} // SketchElement
