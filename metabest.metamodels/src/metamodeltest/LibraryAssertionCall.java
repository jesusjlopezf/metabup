/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import java.util.List;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Library Assertion Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.LibraryAssertionCall#getReferencedAssertion <em>Referenced Assertion</em>}</li>
 *   <li>{@link metamodeltest.LibraryAssertionCall#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getLibraryAssertionCall()
 * @model
 * @generated
 */
public interface LibraryAssertionCall extends AssertionCall {
	/**
	 * Returns the value of the '<em><b>Referenced Assertion</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Assertion</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Assertion</em>' reference.
	 * @see #setReferencedAssertion(AssertionDefinition)
	 * @see metamodeltest.MetamodeltestPackage#getLibraryAssertionCall_ReferencedAssertion()
	 * @model required="true"
	 * @generated
	 */
	AssertionDefinition getReferencedAssertion();

	/**
	 * Sets the value of the '{@link metamodeltest.LibraryAssertionCall#getReferencedAssertion <em>Referenced Assertion</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Assertion</em>' reference.
	 * @see #getReferencedAssertion()
	 * @generated
	 */
	void setReferencedAssertion(AssertionDefinition value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link metamodeltest.ParameterValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see metamodeltest.MetamodeltestPackage#getLibraryAssertionCall_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterValue> getParameters();
	public String toString();
	
	public List<ParameterElementSetValue> getPermutedParameters();
} // LibraryAssertionCall
