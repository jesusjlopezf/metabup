/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package fragments;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Value Annotation Param</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fragments.ObjectValueAnnotationParam#getObject <em>Object</em>}</li>
 *   <li>{@link fragments.ObjectValueAnnotationParam#getFeature <em>Feature</em>}</li>
 * </ul>
 *
 * @see fragments.FragmentsPackage#getObjectValueAnnotationParam()
 * @model
 * @generated
 */
public interface ObjectValueAnnotationParam extends ParamValue {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' reference.
	 * @see #setObject(fragments.Object)
	 * @see fragments.FragmentsPackage#getObjectValueAnnotationParam_Object()
	 * @model required="true"
	 * @generated
	 */
	fragments.Object getObject();

	/**
	 * Sets the value of the '{@link fragments.ObjectValueAnnotationParam#getObject <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(fragments.Object value);

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(Feature)
	 * @see fragments.FragmentsPackage#getObjectValueAnnotationParam_Feature()
	 * @model
	 * @generated
	 */
	Feature getFeature();

	/**
	 * Sets the value of the '{@link fragments.ObjectValueAnnotationParam#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(Feature value);

} // ObjectValueAnnotationParam
