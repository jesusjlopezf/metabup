/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.metamodel.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;
import metabup.metamodel.impl.ReferenceImpl;

public class PathCalculator {
	int i = 0;
	Stack<String> path = null;
	SET<String> onPath = null;
	List<Path> paths = null;
	MetaModel mm = null;
	List<MetaClass> srcCandidates = new ArrayList<MetaClass>();
	List<MetaClass> tgtCandidates = new ArrayList<MetaClass>();
	Stack<PathEnumerator> enumerators = new Stack<PathEnumerator>();
	PathCalculatorMainThread mainThread;
	boolean end = false;
	
	int pivot1, pivot2;
	
	public PathCalculator(MetaModel metamodel){
		this.mm = metamodel;
	}
	
	public List<Path> getPaths(MetaClass source, MetaClass target, int refSort, boolean byInheritance){
		path  = new Stack<String>();   // the current path
	    onPath  = new SET<String>();     // the set of vertices on the path
	    paths = new ArrayList<Path>();
	    
	    enumerate(source, target, null, refSort, byInheritance);	    	    
	    
		return paths;
	}
	
	public List<Path> getPaths(MetaClass source, MetaClass target){
		path  = new Stack<String>();   // the current path
	    onPath  = new SET<String>();     // the set of vertices on the path
	    paths = new ArrayList<Path>();
	    
	    enumerate(source, target, null, ReferenceImpl.IRRELEVANT, true);	    	    
	    
		return paths;
	}
	
	public List<Path> getAllPaths(List<MetaClass> precandidates){	
		path  = new Stack<String>();   // the current path
	    onPath  = new SET<String>();     // the set of vertices on the path
	    paths = new ArrayList<Path>();
	    srcCandidates = precandidates;
	    tgtCandidates = precandidates;
	    
	    for(MetaClass mc : precandidates){
	    	if(mc.getReferences(true, ReferenceImpl.IRRELEVANT) != null && !mc.getReferences(true, ReferenceImpl.IRRELEVANT).isEmpty()) srcCandidates.add(mc);
	    	else if(mc.getIncomingRefs(true, true, ReferenceImpl.IRRELEVANT) != null && !mc.getIncomingRefs(true, true, ReferenceImpl.IRRELEVANT).isEmpty()) tgtCandidates.add(mc);
	    }
	    
	    for(MetaClass mc1 : srcCandidates)
			for(MetaClass mc2 : tgtCandidates)
				enumerate(mc1, mc2, null, ReferenceImpl.IRRELEVANT, true);
	    
	    return paths;
	}
	
	public boolean calculateAllPaths(List<MetaClass> srcPrecandidates, List<MetaClass> tgtPrecandidates){
		/*if(srcPrecandidates == null || srcPrecandidates.isEmpty()){
			for(MetaClass mc : mm.getClasses())
		    	if(mc.getReferences(true, ReferenceImpl.IRRELEVANT) != null && !mc.getReferences(true, ReferenceImpl.IRRELEVANT).isEmpty()) srcCandidates.add(mc);
		}
		
		if(tgtPrecandidates == null || tgtPrecandidates.isEmpty()){
			for(MetaClass mc : mm.getClasses())
				if(mc.getIncomingRefs(true, true, ReferenceImpl.IRRELEVANT) != null && !mc.getIncomingRefs(true, true, ReferenceImpl.IRRELEVANT).isEmpty()) tgtCandidates.add(mc);
		}*/
		
		if(srcCandidates.isEmpty() || tgtCandidates.isEmpty()) return false;
		
		path  = new Stack<String>();   // the current path
	    onPath  = new SET<String>();     // the set of vertices on the path
	    paths = new ArrayList<Path>();	    	    	    		    	    	    
		
	  /*  PathEnumeratorMemoryHandler handler = new PathEnumeratorMemoryHandler();
	    handler.start();*/
	    
	    PathCalculatorMainThread mainThread = new PathCalculatorMainThread();
	    mainThread.start();
	    
	    return true;
	}
	
	public Path next() {
		if(srcCandidates == null || srcCandidates.isEmpty()) return null;			
		if(tgtCandidates == null || tgtCandidates.isEmpty()) return null;					
		if(isOver() && paths.isEmpty()) return null;
			
		if(!paths.isEmpty()) {	
			synchronized(paths){
				//System.out.println("Returning path: " + paths.get(0));
				return paths.remove(0);
			}
		}else{
			try {
				Thread.sleep(1000);
				return next();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				return next();
			}			
		}
	}
	
	public boolean isOver(){
		if(end){
			synchronized(enumerators){
				for(PathEnumerator pe : enumerators){
					if(pe.isAlive() || pe.running) return false;
				}
				
				return true;
			}
		}else return false;
	}
	
	/*private boolean enoughMemory(){		
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		long maxMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();
		
		System.out.println("Total Memory: " + maxMemory);
		System.out.println("Free Memory: " + freeMemory);
		
		if(freeMemory >= (maxMemory/10)){
			System.out.println("enough memory: "  + freeMemory / maxMemory);
			return true;
		}
		else{
			System.out.println("not enough memory: "  + freeMemory / maxMemory);
			return false;
		} return true;
	}*/
	
	public void killCalculation(){
		synchronized(enumerators){
			for(PathEnumerator t : enumerators) t.shutdown();
		}
		
		System.runFinalization();
	}
	
	class PathCalculatorMainThread extends Thread{
		public void run(){
			// System.out.println("pivot1 = " + pivot1 + " / pivot2 = " + pivot2);
			// System.out.println("class1 = " + candidates.get(pivot1) + " / class2 = " + candidates.get(pivot2));
			
		    for(pivot1 = 0; pivot1 < srcCandidates.size(); pivot1++){
		    	for(pivot2 = 0; pivot2 < tgtCandidates.size(); pivot2++){
		    		if(srcCandidates.get(pivot1).references(tgtCandidates.get(pivot2), 0, -1, 0, ReferenceImpl.IRRELEVANT, true, false, null)){
		    			PathEnumerator pe = new PathEnumerator(srcCandidates.get(pivot1), tgtCandidates.get(pivot2),ReferenceImpl.IRRELEVANT, true);
						synchronized(enumerators) {enumerators.add(pe);}
						pe.start();
						//System.out.println("launched p1 " + pivot1 + " p2 " + pivot2 + " / "+ enumerators.size() + " threads");
		    		}		    		
		    	}
		    }
		    
		    end = true;
		}
	}
	
	/*class PathEnumeratorMemoryHandler extends Thread{
		public void run(){
			while(true){
				synchronized(enumerators){
					for(PathEnumerator pe : enumerators){
						if(!pe.running) pe.interrupt();
						else if(pe.isAlive() && enoughMemory()) pe.notify();
					}
				}
			}
	    }
	}*/
	
	class PathEnumerator extends Thread
	{
		LinkedList<MetaClass> visited = null;
		MetaClass source, target;
		int refSort;
		boolean byInheritance;
		
		//variable for safely shutting the thread down
		boolean running = true;

	    public PathEnumerator(MetaClass source, MetaClass target, int refSort, boolean byInheritance)
	    {
	        this.source = source;
	        this.target = target;
	        this.refSort = refSort;
	        this.byInheritance = byInheritance;
	    }

	    public void run()
	    {
	        try {
				enumerate(source, target, visited, refSort, byInheritance);				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        this.running = false;
	        this.shutdown();
	    }
	    
	    private void enumerate(MetaClass source, MetaClass target, LinkedList<MetaClass> visited, int refSort, boolean byInheritance) throws InterruptedException{						
			if(!running) return;

			if(visited == null){
				visited = new LinkedList<MetaClass>();
				visited.add(source);
			}
			
			List<Reference> references = visited.getLast().getReferences(byInheritance, refSort);
			List<MetaClass> nodes = new ArrayList<MetaClass>();		
			for(Reference r : references) nodes.add(r.getReference());
			
			for(MetaClass node : nodes){
				if(visited.contains(node) && node != source){
					continue;
				}
				if(node.equals(target)){
					visited.add(node);
					MetaClass mcs[] = new MetaClass[visited.size()];
					Path p = new metabup.metamodel.util.Path(visited.toArray(mcs), refSort, byInheritance);
					
					synchronized(paths){
						paths.add(p);
						//System.out.println("adding: " + p);
					}
					
					//System.out.println("adding path: " + p);
					visited.removeLast();
					break;
				}
			}
			
			for(MetaClass node: nodes){
				if(visited.contains(node) || node.equals(target)){
					continue;
				}
				visited.addLast(node);
				//if(!enoughMemory()) this.wait();
				enumerate(source,target,visited,refSort,byInheritance);
				visited.removeLast();
			}
		}
	    
	    public void shutdown(){
	    	if(this.running) running = false;	    	
	    	this.interrupt();
	    }
	}
	
	
	public void enumerate(MetaClass source, MetaClass target, LinkedList<MetaClass> visited, int refSort, boolean byInheritance){						
		if(visited == null){
			visited = new LinkedList<MetaClass>();
			visited.add(source);
		}
		//System.out.println("slow enumeration");
		List<Reference> references = visited.getLast().getReferences(byInheritance, refSort);
		List<MetaClass> nodes = new ArrayList<MetaClass>();		
		for(Reference r : references) nodes.add(r.getReference());
		
		for(MetaClass node : nodes){
			if(visited.contains(node)){
				continue;
			}
			if(node.equals(target)){
				visited.add(node);
				MetaClass mcs[] = new MetaClass[visited.size()];
				Path p = new metabup.metamodel.util.Path(visited.toArray(mcs), refSort, byInheritance);
				
				synchronized(p){
					paths.add(p);
				}
				
				//System.out.println("adding path: " + p);
				visited.removeLast();
				break;
			}
		}
		
		for(MetaClass node: nodes){
			if(visited.contains(node) || node.equals(target)){
				continue;
			}
			visited.addLast(node);
			enumerate(source,target,visited,refSort,byInheritance);
			visited.removeLast();
		}
	}
}
