/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.ui.views;

import org.eclipse.core.runtime.IAdaptable;

public class MetamodelAssertionEvaluationViewTreeObject /*extends TreeObject*/ implements IAdaptable {
	protected String name;
	protected Object object;
	protected MetamodelAssertionEvaluationViewTreeParent parent;
	
	public MetamodelAssertionEvaluationViewTreeObject(Object o) {
		if(o != null) this.name = o.toString();
		this.object = o;
	}
	
	public String getName() {
		return name;
	}		
	
	public String toString() {
		return getName();
	}
	
	public Object getAdapter(Class key) {
		return null;
	}
	
	public Object getObject(){
		return object;
	}
	
	public void setParent(MetamodelAssertionEvaluationViewTreeParent parent) {
		this.parent = parent;
	}
	public MetamodelAssertionEvaluationViewTreeParent getParent() {
		return parent;
	}
	
}
