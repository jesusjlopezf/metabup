/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.jesusjlopezf.utils.primitive;

import java.math.BigInteger;
import java.util.List;

public class NumericUtils {
	
	/* M�ximo Com�n Divisor */
	public static int getGCD(List<Integer> numbers) {		          
        int smallest = numbers.get(0);
 
        for (int i = 1; i < numbers.size(); i++)
            if (numbers.get(i) < smallest) 
            	smallest = numbers.get(i);
 
        while (smallest > 1) {            
            int counter = 0;
            int modTot = 0;
             
            while (counter < numbers.size()) {
                modTot += numbers.get(counter) % smallest;
                counter++;
            }
 
            if (modTot == 0)return smallest;
            smallest--;
        }

        return 1;
    }
}
