/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.sketches.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import com.jesusjlopezf.utils.geometry.GeometricObject;
import com.jesusjlopezf.utils.geometry.ObjectRelativePosition;

import metabup.sketches.Connection;
import metabup.sketches.Label;
import metabup.sketches.SketchElement;
import metabup.sketches.sketchesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getDebugName <em>Debug Name</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getToolSymbolName <em>Tool Symbol Name</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getText <em>Text</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getSkechTypeInfo <em>Skech Type Info</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getX <em>X</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getY <em>Y</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getWidth <em>Width</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getHeight <em>Height</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getContainer <em>Container</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getImplicitlyContainedElements <em>Implicitly Contained Elements</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getSrcOf <em>Src Of</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getTgtOf <em>Tgt Of</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getLabels <em>Labels</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getColor <em>Color</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#isTransparent <em>Transparent</em>}</li>
 *   <li>{@link metabup.sketches.impl.SketchElementImpl#getSourceCode <em>Source Code</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class SketchElementImpl extends MinimalEObjectImpl.Container implements SketchElement {
	/**
	 * The default value of the '{@link #getDebugName() <em>Debug Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDebugName()
	 * @generated
	 * @ordered
	 */
	protected static final String DEBUG_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDebugName() <em>Debug Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDebugName()
	 * @generated
	 * @ordered
	 */
	protected String debugName = DEBUG_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getToolSymbolName() <em>Tool Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToolSymbolName()
	 * @generated
	 * @ordered
	 */
	protected static final String TOOL_SYMBOL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToolSymbolName() <em>Tool Symbol Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToolSymbolName()
	 * @generated
	 * @ordered
	 */
	protected String toolSymbolName = TOOL_SYMBOL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getText() <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getText()
	 * @generated
	 * @ordered
	 */
	protected static final String TEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getText() <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getText()
	 * @generated
	 * @ordered
	 */
	protected String text = TEXT_EDEFAULT;

	/**
	 * The default value of the '{@link #getSkechTypeInfo() <em>Skech Type Info</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSkechTypeInfo()
	 * @generated
	 * @ordered
	 */
	protected static final String SKECH_TYPE_INFO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSkechTypeInfo() <em>Skech Type Info</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSkechTypeInfo()
	 * @generated
	 * @ordered
	 */
	protected String skechTypeInfo = SKECH_TYPE_INFO_EDEFAULT;

	/**
	 * The default value of the '{@link #getX() <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected static final Float X_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getX() <em>X</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX()
	 * @generated
	 * @ordered
	 */
	protected Float x = X_EDEFAULT;

	/**
	 * The default value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected static final Float Y_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getY() <em>Y</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getY()
	 * @generated
	 * @ordered
	 */
	protected Float y = Y_EDEFAULT;

	/**
	 * The default value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected static final Float WIDTH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected Float width = WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getHeight() <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeight()
	 * @generated
	 * @ordered
	 */
	protected static final Float HEIGHT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHeight() <em>Height</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeight()
	 * @generated
	 * @ordered
	 */
	protected Float height = HEIGHT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContainer() <em>Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainer()
	 * @generated
	 * @ordered
	 */
	protected SketchElement container;

	/**
	 * The cached value of the '{@link #getImplicitlyContainedElements() <em>Implicitly Contained Elements</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplicitlyContainedElements()
	 * @generated
	 * @ordered
	 */
	protected EList<SketchElement> implicitlyContainedElements;

	/**
	 * The cached value of the '{@link #getSrcOf() <em>Src Of</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcOf()
	 * @generated
	 * @ordered
	 */
	protected EList<Connection> srcOf;

	/**
	 * The cached value of the '{@link #getTgtOf() <em>Tgt Of</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTgtOf()
	 * @generated
	 * @ordered
	 */
	protected EList<Connection> tgtOf;

	/**
	 * The cached value of the '{@link #getLabels() <em>Labels</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabels()
	 * @generated
	 * @ordered
	 */
	protected EList<Label> labels;

	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final String COLOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected String color = COLOR_EDEFAULT;

	/**
	 * The default value of the '{@link #isTransparent() <em>Transparent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransparent()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TRANSPARENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isTransparent() <em>Transparent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isTransparent()
	 * @generated
	 * @ordered
	 */
	protected boolean transparent = TRANSPARENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceCode() <em>Source Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceCode()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceCode() <em>Source Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceCode()
	 * @generated
	 * @ordered
	 */
	protected String sourceCode = SOURCE_CODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SketchElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return sketchesPackage.Literals.SKETCH_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDebugName() {
		return debugName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDebugName(String newDebugName) {
		String oldDebugName = debugName;
		debugName = newDebugName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__DEBUG_NAME, oldDebugName, debugName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToolSymbolName() {
		return toolSymbolName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToolSymbolName(String newToolSymbolName) {
		String oldToolSymbolName = toolSymbolName;
		toolSymbolName = newToolSymbolName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__TOOL_SYMBOL_NAME, oldToolSymbolName, toolSymbolName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText() {
		return text;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setText(String newText) {
		String oldText = text;
		text = newText;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__TEXT, oldText, text));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSkechTypeInfo() {
		return skechTypeInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSkechTypeInfo(String newSkechTypeInfo) {
		String oldSkechTypeInfo = skechTypeInfo;
		skechTypeInfo = newSkechTypeInfo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__SKECH_TYPE_INFO, oldSkechTypeInfo, skechTypeInfo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float getX() {
		return x;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX(Float newX) {
		Float oldX = x;
		x = newX;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__X, oldX, x));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float getY() {
		return y;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setY(Float newY) {
		Float oldY = y;
		y = newY;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__Y, oldY, y));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float getWidth() {
		return width;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWidth(Float newWidth) {
		Float oldWidth = width;
		width = newWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__WIDTH, oldWidth, width));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float getHeight() {
		return height;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeight(Float newHeight) {
		Float oldHeight = height;
		height = newHeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__HEIGHT, oldHeight, height));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SketchElement getContainer() {
		if (container != null && container.eIsProxy()) {
			InternalEObject oldContainer = (InternalEObject)container;
			container = (SketchElement)eResolveProxy(oldContainer);
			if (container != oldContainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, sketchesPackage.SKETCH_ELEMENT__CONTAINER, oldContainer, container));
			}
		}
		return container;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SketchElement basicGetContainer() {
		return container;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainer(SketchElement newContainer, NotificationChain msgs) {
		SketchElement oldContainer = container;
		container = newContainer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__CONTAINER, oldContainer, newContainer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainer(SketchElement newContainer) {
		if (newContainer != container) {
			NotificationChain msgs = null;
			if (container != null)
				msgs = ((InternalEObject)container).eInverseRemove(this, sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS, SketchElement.class, msgs);
			if (newContainer != null)
				msgs = ((InternalEObject)newContainer).eInverseAdd(this, sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS, SketchElement.class, msgs);
			msgs = basicSetContainer(newContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__CONTAINER, newContainer, newContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SketchElement> getImplicitlyContainedElements() {
		if (implicitlyContainedElements == null) {
			implicitlyContainedElements = new EObjectWithInverseResolvingEList<SketchElement>(SketchElement.class, this, sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS, sketchesPackage.SKETCH_ELEMENT__CONTAINER);
		}
		return implicitlyContainedElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Connection> getSrcOf() {
		if (srcOf == null) {
			srcOf = new EObjectWithInverseResolvingEList<Connection>(Connection.class, this, sketchesPackage.SKETCH_ELEMENT__SRC_OF, sketchesPackage.CONNECTION__SRC);
		}
		return srcOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Connection> getTgtOf() {
		if (tgtOf == null) {
			tgtOf = new EObjectWithInverseResolvingEList<Connection>(Connection.class, this, sketchesPackage.SKETCH_ELEMENT__TGT_OF, sketchesPackage.CONNECTION__TGT);
		}
		return tgtOf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Label> getLabels() {
		if (labels == null) {
			labels = new EObjectContainmentEList<Label>(Label.class, this, sketchesPackage.SKETCH_ELEMENT__LABELS);
		}
		return labels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(String newColor) {
		String oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__COLOR, oldColor, color));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isTransparent() {
		return transparent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransparent(boolean newTransparent) {
		boolean oldTransparent = transparent;
		transparent = newTransparent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__TRANSPARENT, oldTransparent, transparent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceCode() {
		return sourceCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceCode(String newSourceCode) {
		String oldSourceCode = sourceCode;
		sourceCode = newSourceCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.SKETCH_ELEMENT__SOURCE_CODE, oldSourceCode, sourceCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case sketchesPackage.SKETCH_ELEMENT__CONTAINER:
				if (container != null)
					msgs = ((InternalEObject)container).eInverseRemove(this, sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS, SketchElement.class, msgs);
				return basicSetContainer((SketchElement)otherEnd, msgs);
			case sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getImplicitlyContainedElements()).basicAdd(otherEnd, msgs);
			case sketchesPackage.SKETCH_ELEMENT__SRC_OF:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSrcOf()).basicAdd(otherEnd, msgs);
			case sketchesPackage.SKETCH_ELEMENT__TGT_OF:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTgtOf()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case sketchesPackage.SKETCH_ELEMENT__CONTAINER:
				return basicSetContainer(null, msgs);
			case sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS:
				return ((InternalEList<?>)getImplicitlyContainedElements()).basicRemove(otherEnd, msgs);
			case sketchesPackage.SKETCH_ELEMENT__SRC_OF:
				return ((InternalEList<?>)getSrcOf()).basicRemove(otherEnd, msgs);
			case sketchesPackage.SKETCH_ELEMENT__TGT_OF:
				return ((InternalEList<?>)getTgtOf()).basicRemove(otherEnd, msgs);
			case sketchesPackage.SKETCH_ELEMENT__LABELS:
				return ((InternalEList<?>)getLabels()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case sketchesPackage.SKETCH_ELEMENT__DEBUG_NAME:
				return getDebugName();
			case sketchesPackage.SKETCH_ELEMENT__TOOL_SYMBOL_NAME:
				return getToolSymbolName();
			case sketchesPackage.SKETCH_ELEMENT__TEXT:
				return getText();
			case sketchesPackage.SKETCH_ELEMENT__SKECH_TYPE_INFO:
				return getSkechTypeInfo();
			case sketchesPackage.SKETCH_ELEMENT__X:
				return getX();
			case sketchesPackage.SKETCH_ELEMENT__Y:
				return getY();
			case sketchesPackage.SKETCH_ELEMENT__WIDTH:
				return getWidth();
			case sketchesPackage.SKETCH_ELEMENT__HEIGHT:
				return getHeight();
			case sketchesPackage.SKETCH_ELEMENT__CONTAINER:
				if (resolve) return getContainer();
				return basicGetContainer();
			case sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS:
				return getImplicitlyContainedElements();
			case sketchesPackage.SKETCH_ELEMENT__SRC_OF:
				return getSrcOf();
			case sketchesPackage.SKETCH_ELEMENT__TGT_OF:
				return getTgtOf();
			case sketchesPackage.SKETCH_ELEMENT__LABELS:
				return getLabels();
			case sketchesPackage.SKETCH_ELEMENT__COLOR:
				return getColor();
			case sketchesPackage.SKETCH_ELEMENT__TRANSPARENT:
				return isTransparent();
			case sketchesPackage.SKETCH_ELEMENT__SOURCE_CODE:
				return getSourceCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case sketchesPackage.SKETCH_ELEMENT__DEBUG_NAME:
				setDebugName((String)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__TOOL_SYMBOL_NAME:
				setToolSymbolName((String)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__TEXT:
				setText((String)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__SKECH_TYPE_INFO:
				setSkechTypeInfo((String)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__X:
				setX((Float)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__Y:
				setY((Float)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__WIDTH:
				setWidth((Float)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__HEIGHT:
				setHeight((Float)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__CONTAINER:
				setContainer((SketchElement)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS:
				getImplicitlyContainedElements().clear();
				getImplicitlyContainedElements().addAll((Collection<? extends SketchElement>)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__SRC_OF:
				getSrcOf().clear();
				getSrcOf().addAll((Collection<? extends Connection>)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__TGT_OF:
				getTgtOf().clear();
				getTgtOf().addAll((Collection<? extends Connection>)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__LABELS:
				getLabels().clear();
				getLabels().addAll((Collection<? extends Label>)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__COLOR:
				setColor((String)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__TRANSPARENT:
				setTransparent((Boolean)newValue);
				return;
			case sketchesPackage.SKETCH_ELEMENT__SOURCE_CODE:
				setSourceCode((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case sketchesPackage.SKETCH_ELEMENT__DEBUG_NAME:
				setDebugName(DEBUG_NAME_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__TOOL_SYMBOL_NAME:
				setToolSymbolName(TOOL_SYMBOL_NAME_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__TEXT:
				setText(TEXT_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__SKECH_TYPE_INFO:
				setSkechTypeInfo(SKECH_TYPE_INFO_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__X:
				setX(X_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__Y:
				setY(Y_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__WIDTH:
				setWidth(WIDTH_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__HEIGHT:
				setHeight(HEIGHT_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__CONTAINER:
				setContainer((SketchElement)null);
				return;
			case sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS:
				getImplicitlyContainedElements().clear();
				return;
			case sketchesPackage.SKETCH_ELEMENT__SRC_OF:
				getSrcOf().clear();
				return;
			case sketchesPackage.SKETCH_ELEMENT__TGT_OF:
				getTgtOf().clear();
				return;
			case sketchesPackage.SKETCH_ELEMENT__LABELS:
				getLabels().clear();
				return;
			case sketchesPackage.SKETCH_ELEMENT__COLOR:
				setColor(COLOR_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__TRANSPARENT:
				setTransparent(TRANSPARENT_EDEFAULT);
				return;
			case sketchesPackage.SKETCH_ELEMENT__SOURCE_CODE:
				setSourceCode(SOURCE_CODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case sketchesPackage.SKETCH_ELEMENT__DEBUG_NAME:
				return DEBUG_NAME_EDEFAULT == null ? debugName != null : !DEBUG_NAME_EDEFAULT.equals(debugName);
			case sketchesPackage.SKETCH_ELEMENT__TOOL_SYMBOL_NAME:
				return TOOL_SYMBOL_NAME_EDEFAULT == null ? toolSymbolName != null : !TOOL_SYMBOL_NAME_EDEFAULT.equals(toolSymbolName);
			case sketchesPackage.SKETCH_ELEMENT__TEXT:
				return TEXT_EDEFAULT == null ? text != null : !TEXT_EDEFAULT.equals(text);
			case sketchesPackage.SKETCH_ELEMENT__SKECH_TYPE_INFO:
				return SKECH_TYPE_INFO_EDEFAULT == null ? skechTypeInfo != null : !SKECH_TYPE_INFO_EDEFAULT.equals(skechTypeInfo);
			case sketchesPackage.SKETCH_ELEMENT__X:
				return X_EDEFAULT == null ? x != null : !X_EDEFAULT.equals(x);
			case sketchesPackage.SKETCH_ELEMENT__Y:
				return Y_EDEFAULT == null ? y != null : !Y_EDEFAULT.equals(y);
			case sketchesPackage.SKETCH_ELEMENT__WIDTH:
				return WIDTH_EDEFAULT == null ? width != null : !WIDTH_EDEFAULT.equals(width);
			case sketchesPackage.SKETCH_ELEMENT__HEIGHT:
				return HEIGHT_EDEFAULT == null ? height != null : !HEIGHT_EDEFAULT.equals(height);
			case sketchesPackage.SKETCH_ELEMENT__CONTAINER:
				return container != null;
			case sketchesPackage.SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS:
				return implicitlyContainedElements != null && !implicitlyContainedElements.isEmpty();
			case sketchesPackage.SKETCH_ELEMENT__SRC_OF:
				return srcOf != null && !srcOf.isEmpty();
			case sketchesPackage.SKETCH_ELEMENT__TGT_OF:
				return tgtOf != null && !tgtOf.isEmpty();
			case sketchesPackage.SKETCH_ELEMENT__LABELS:
				return labels != null && !labels.isEmpty();
			case sketchesPackage.SKETCH_ELEMENT__COLOR:
				return COLOR_EDEFAULT == null ? color != null : !COLOR_EDEFAULT.equals(color);
			case sketchesPackage.SKETCH_ELEMENT__TRANSPARENT:
				return transparent != TRANSPARENT_EDEFAULT;
			case sketchesPackage.SKETCH_ELEMENT__SOURCE_CODE:
				return SOURCE_CODE_EDEFAULT == null ? sourceCode != null : !SOURCE_CODE_EDEFAULT.equals(sourceCode);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (debugName: ");
		result.append(debugName);
		result.append(", toolSymbolName: ");
		result.append(toolSymbolName);
		result.append(", text: ");
		result.append(text);
		result.append(", skechTypeInfo: ");
		result.append(skechTypeInfo);
		result.append(", x: ");
		result.append(x);
		result.append(", y: ");
		result.append(y);
		result.append(", width: ");
		result.append(width);
		result.append(", height: ");
		result.append(height);
		result.append(", color: ");
		result.append(color);
		result.append(", transparent: ");
		result.append(transparent);
		result.append(", sourceCode: ");
		result.append(sourceCode);
		result.append(')');
		return result.toString();
	}

	public boolean isOverlapping(SketchElement element, boolean extCall){		
		if(element == null || element.getX() == null || element.getY() == null || element.getWidth() == null || element.getHeight() == null) return false;		
		
		GeometricObject thisObj = new GeometricObject(Math.round(this.getX()), Math.round(this.getY()), Math.round(this.getWidth()), Math.round(this.getHeight()));
		GeometricObject otherObj = new GeometricObject(Math.round(element.getX()), Math.round(element.getY()), Math.round(element.getWidth()), Math.round(element.getHeight()));
		
		if(thisObj.contains(otherObj) || otherObj.contains(thisObj)) return false;	  
		if(thisObj.isOverlapping(otherObj, true)) return true;
				
		return false;
	}
	
	public boolean contains(SketchElement element){
		if(element == null || element.getX() == null || element.getY() == null || element.getWidth() == null || element.getHeight() == null) return false;		
		
		GeometricObject thisObj = new GeometricObject(Math.round(this.getX()), Math.round(this.getY()), Math.round(this.getWidth()), Math.round(this.getHeight()));
		GeometricObject otherObj = new GeometricObject(Math.round(element.getX()), Math.round(element.getY()), Math.round(element.getWidth()), Math.round(element.getHeight()));
		
		if(thisObj.contains(otherObj)) return true;
		
		return false;
	}
	
	public boolean isAdjacent(SketchElement element, int maxDistance, boolean extCall){
		if(element == null || element.getX() == null || element.getY() == null || element.getWidth() == null || element.getHeight() == null) return false;		
		
		GeometricObject thisObj = new GeometricObject(Math.round(this.getX()), Math.round(this.getY()), Math.round(this.getWidth()), Math.round(this.getHeight()));
		GeometricObject otherObj = new GeometricObject(Math.round(element.getX()), Math.round(element.getY()), Math.round(element.getWidth()), Math.round(element.getHeight()));
		
		if(thisObj.contains(otherObj) || otherObj.contains(thisObj)) return false;	  
		if(thisObj.isAdjacent(otherObj, maxDistance, true)) return true;
				
		return false;
	}
	
	public int getAdjacencySide(SketchElement element, int maxDistance, boolean extCall){
		if(element == null || element.getX() == null || element.getY() == null || element.getWidth() == null || element.getHeight() == null) return ObjectRelativePosition.UNIDENTIFIED;		
		
		GeometricObject thisObj = new GeometricObject(Math.round(this.getX()), Math.round(this.getY()), Math.round(this.getWidth()), Math.round(this.getHeight()));
		GeometricObject otherObj = new GeometricObject(Math.round(element.getX()), Math.round(element.getY()), Math.round(element.getWidth()), Math.round(element.getHeight()));
		
		if(thisObj.contains(otherObj) || otherObj.contains(thisObj)) return ObjectRelativePosition.NONE;	  
		if(thisObj.isAdjacent(otherObj, maxDistance, true)) return thisObj.getAdjacencySide(otherObj, maxDistance, true);
				
		return ObjectRelativePosition.UNIDENTIFIED;
	}
	
	public int getAlignmentSide(SketchElement element, int maxDistance, boolean extCall){
		if(element == null || element.getX() == null || element.getY() == null || element.getWidth() == null || element.getHeight() == null) return ObjectRelativePosition.UNIDENTIFIED;		
		
		GeometricObject thisObj = new GeometricObject(Math.round(this.getX()), Math.round(this.getY()), Math.round(this.getWidth()), Math.round(this.getHeight()));
		GeometricObject otherObj = new GeometricObject(Math.round(element.getX()), Math.round(element.getY()), Math.round(element.getWidth()), Math.round(element.getHeight()));
		
		if(thisObj.contains(otherObj) || otherObj.contains(thisObj)) return ObjectRelativePosition.NONE;	  
		if(thisObj.isAdjacent(otherObj, maxDistance, true)) return thisObj.getAlignmentSide(otherObj, maxDistance, true);
				
		return ObjectRelativePosition.UNIDENTIFIED;
	}
	
	
} //SketchElementImpl
