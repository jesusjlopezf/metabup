/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.validation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.ocl.examples.domain.values.SetValue;
import org.eclipse.ocl.examples.pivot.ExpressionInOCL;
import org.eclipse.ocl.examples.pivot.OCL;
import org.eclipse.ocl.examples.pivot.OCLExpression;
import org.eclipse.ocl.examples.pivot.ParserException;
import org.eclipse.ocl.examples.pivot.helper.OCLHelper;
import org.eclipse.ocl.examples.pivot.utilities.PivotEnvironmentFactory;
import org.eclipse.ocl.examples.xtext.essentialocl.EssentialOCLStandaloneSetup;

import com.jesusjlopezf.utils.primitive.StringUtils;

import metabest.preferences.MetaBestPreferenceConstants;
import metabup.lexicon.utils.LexicalInflector;
import metabup.lexicon.wordnet.Wordnet;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Annotation;
import metabup.metamodel.AnnotationParam;
import metabup.metamodel.Attribute;
import metabup.metamodel.ElementValue;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;
import metabup.metamodel.impl.ReferenceImpl;
import metabup.metamodel.util.Path;
import metabup.metamodel.util.PathCalculator;
import metamodeltest.Abstraction;
import metamodeltest.Adjective;
import metamodeltest.AndQualifier;
import metamodeltest.Annotated;
import metamodeltest.Assertion;
import metamodeltest.AttributeContainment;
import metamodeltest.AttributeSelector;
import metamodeltest.Case;
import metamodeltest.ClassCollects;
import metamodeltest.ClassContainee;
import metamodeltest.ClassReachedBy;
import metamodeltest.ClassReaches;
import metamodeltest.ClassSelector;
import metamodeltest.ContLeaf;
import metamodeltest.ContRoot;
import metamodeltest.ContainmentPath;
import metamodeltest.ContainmentReference;
import metamodeltest.Contains;
import metamodeltest.CyclicPath;
import metamodeltest.ElementQualifier;
import metamodeltest.ElementSelector;
import metamodeltest.Every;
import metamodeltest.Existance;
import metamodeltest.FeatureContainment;
import metamodeltest.FeatureMultiplicity;
import metamodeltest.FeatureQualifier;
import metamodeltest.FeatureSelector;
import metamodeltest.GrammaticalNumber;
import metamodeltest.InhLeaf;
import metamodeltest.InhRoot;
import metamodeltest.InheritedFeature;
import metamodeltest.IsSubTo;
import metamodeltest.IsSuperTo;
import metamodeltest.Length;
import metamodeltest.Literal;
import metamodeltest.NamePart;
import metamodeltest.Named;
import metamodeltest.NamedAfter;
import metamodeltest.None;
import metamodeltest.Noun;
import metamodeltest.OppositeTo;
import metamodeltest.OrQualifier;
import metamodeltest.PathAnd;
import metamodeltest.PathOr;
import metamodeltest.PathSelector;
import metamodeltest.PathSource;
import metamodeltest.PathTarget;
import metamodeltest.PhraseNamed;
import metamodeltest.Prefix;
import metamodeltest.Qualifier;
import metamodeltest.Quantifier;
import metamodeltest.ReferenceContainment;
import metamodeltest.ReferenceSelector;
import metamodeltest.Selector;
import metamodeltest.SelfInheritance;
import metamodeltest.SourceClass;
import metamodeltest.StepClass;
import metamodeltest.Suffix;
import metamodeltest.Synonym;
import metamodeltest.TargetClass;
import metamodeltest.Typed;
import metamodeltest.Verb;
import metamodeltest.WithInheritancePath;
import metamodeltest.WordNature;


public class MetamodelAssertionEvaluationOutput {
	protected Assertion assertion;
	protected MetaModel metamodel;
	protected String ecoreURI;
	protected boolean output = false;
	protected int length[] = null;
	protected String rationale;
	protected List<EObject> eoCandidates = null;
	protected List<EObject> eoPrecandidates = null;
	private List<AnnotatedElement> errors = null;
	private List<AnnotatedElement> matches = null;
	protected String name = null;
	private boolean warning = false;
	protected Boolean unexistance = false;
	protected long runningTime = 0;
	protected long memoryConsumption = 0;
	
	boolean fastcheck = MetaBestPreferenceConstants.getPreferenceBooleanValue(MetaBestPreferenceConstants.FAST_PERMUTATION_CHECK);
	
	//private String additionalInfo = null;
	
	public MetamodelAssertionEvaluationOutput(Assertion a, String name){
		this.assertion = a;
		this.name = name;
	}
	
	public MetamodelAssertionEvaluationOutput(Assertion a, String name, boolean warning){
		this.assertion = a;
		this.name = name;
		this.warning = warning;
	}
	
	public String toString(){
		if(name == null) return "unnamed";
		else return name;
	}
	
	public boolean isTrue(){
		return output;
	}
	
	public boolean isWarning(){
		return warning;
	}
	
	public MetaModel getMetaModel(){
		return this.metamodel;
	}
	
	public boolean evaluate(MetaModel mm) {	
		//System.out.println("About to evaluate " + mm.getName());
		long startingTime = System.currentTimeMillis();
		this.metamodel = mm;
		eoCandidates = new ArrayList<EObject>();
		List<Path> pathCandidates = new ArrayList<Path>();
		boolean pathSelector = false;
		rationale = "No rationale";
		output = false;
		
		eoPrecandidates = new ArrayList<EObject>();
		Selector s = this.assertion.getSelector();
		
		if(s instanceof ElementSelector){
			ElementSelector es = (ElementSelector)s;	
			Quantifier quant = es.getQuantifier();	
			
			if(es.getElementFilter() != null){
				eoPrecandidates = filterElements(mm.getAllElements(), es.getElementFilter().getQualifier());					
				length = quant.getQuantity(eoPrecandidates.size());	
			}else{
				eoPrecandidates = mm.getAllElements();
				length = quant.getQuantity(mm.getAllElements().size());
			}
			
			int precandidatesSize = eoPrecandidates.size();	
		
			if(es.getElementConditioner() == null) eoCandidates = eoPrecandidates;
			else eoCandidates = filterElements(eoPrecandidates, es.getElementConditioner().getQualifier());
			
			if(unexistance && precandidatesSize == 0) output = true;
			else output = inRange(eoCandidates, length[0], length[1]);
		}
		
		if(s instanceof ClassSelector){
			ClassSelector cs = (ClassSelector)s;
			Quantifier quant = cs.getQuantifier();	
			
			List<EObject> classes = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					classes.add(eo);
			
			if(cs.getClassFilter() != null) eoPrecandidates = filterClasses(classes, cs.getClassFilter().getQualifier());					
			else eoPrecandidates = classes;
			length = quant.getQuantity(eoPrecandidates.size());
			
			int precandidatesSize = eoPrecandidates.size();	
			
			if(cs.getClassConditioner() == null) eoCandidates = eoPrecandidates;
			else eoCandidates = filterClasses(eoPrecandidates, cs.getClassConditioner().getQualifier());
			
			if(unexistance && precandidatesSize == 0) output = true;
			else output = inRange(eoCandidates, length[0], length[1]);
		}
		
		if(s instanceof FeatureSelector){
			FeatureSelector fs = (FeatureSelector)s;
			Quantifier quant = fs.getQuantifier();	
			
			List<EObject> features = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses()){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					for(Feature f : mc.getFeatures()) features.add(f);
				}					
			}
			
			if(fs.getFeatureFilter() != null) eoPrecandidates = filterFeatures(features, fs.getFeatureFilter().getQualifier());					
			else eoPrecandidates = features;
			length = quant.getQuantity(eoPrecandidates.size());
			
			int precandidatesSize = eoPrecandidates.size();	
			
			if(fs.getFeatureConditioner() == null) eoCandidates = eoPrecandidates;
			else eoCandidates = filterFeatures(eoPrecandidates, fs.getFeatureConditioner().getQualifier());
			
			if(unexistance && precandidatesSize == 0) output = true;
			else output = inRange(eoCandidates, length[0], length[1]);
		}
		
		if(s instanceof AttributeSelector){
			AttributeSelector as = (AttributeSelector)s;
			Quantifier quant = as.getQuantifier();	
			
			List<EObject> attributes = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses()){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					for(Attribute a : mc.getAttributes()) attributes.add(a);
				}					
			}
			
			if(as.getAttributeFilter() != null) eoPrecandidates = filterAttributes(attributes, as.getAttributeFilter().getQualifier());
			else eoPrecandidates = attributes;			
			length = quant.getQuantity(eoPrecandidates.size());
						
			int precandidatesSize = eoPrecandidates.size();	
			
			if(as.getAttributeConditioner() == null) eoCandidates = eoPrecandidates;
			else eoCandidates = filterAttributes(eoPrecandidates, as.getAttributeConditioner().getQualifier());
			
			if(unexistance && precandidatesSize == 0) output = true;
			else output = inRange(eoCandidates, length[0], length[1]);
		}
		
		if(s instanceof ReferenceSelector){
			ReferenceSelector rs = (ReferenceSelector)s;
			Quantifier quant = rs.getQuantifier();	
			
			List<EObject> references = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses()){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					for(Reference r : mc.getReferences()) references.add(r);
				}					
			}
			
			if(rs.getReferenceFilter() != null) eoPrecandidates = filterReferences(references, rs.getReferenceFilter().getQualifier());					
			else eoPrecandidates = references;
			length = quant.getQuantity(eoPrecandidates.size());
			
			int precandidatesSize = eoPrecandidates.size();	
			
			if(rs.getReferenceConditioner() == null) eoCandidates = eoPrecandidates;
			else eoCandidates = filterReferences(eoPrecandidates, rs.getReferenceConditioner().getQualifier());
			
			if(unexistance && precandidatesSize == 0) output = true;
			else output = inRange(eoCandidates, length[0], length[1]);
		}
		
		if(s instanceof PathSelector){
			pathSelector = true;
			PathSelector ps = (PathSelector)s;	
			Quantifier quant = ps.getQuantifier();	
			
			List<Path> precandidates = new ArrayList<Path>();
			PathCalculator pc = new PathCalculator(mm);
			
			if(ps.getPathFilter() != null){		
				List<Path> allPaths = pc.getAllPaths(mm.getClasses());
				precandidates = filterPaths(allPaths, ps.getPathFilter().getPathQualifier());
				length = quant.getQuantity(precandidates.size());
				pathCandidates = filterPaths(precandidates, ps.getPathConditioner().getPathQualifier());			
				output = pathsInRange(pathCandidates, length[0], length[1]);
			}else{
				if(quant instanceof Every){
					length = quant.getQuantity(Integer.MAX_VALUE);
					pathCandidates = filterPaths(precandidates, ps.getPathConditioner().getPathQualifier());			
					output = pathsInRange(pathCandidates, length[0], length[1]);
				}else{
					//System.out.println("EVALUATING " + assertion.getDescription());
					length = quant.getQuantity(Integer.MAX_VALUE);
					
					if(mm.getReferences() == null || mm.getReferences().isEmpty()) output = false;
					else{
						// optimization for reducing the path calculating time
						
						TreeIterator<EObject> it = ps.getPathConditioner().getPathQualifier().eAllContents();
						
						List<MetaClass> candidateSrcs = new ArrayList<MetaClass>();
						List<MetaClass> candidateTgts = new ArrayList<MetaClass>();
						
						while(it.hasNext()){
							EObject eo = it.next();
							
							if(eo instanceof PathSource){
								PathSource pas = (PathSource)eo;
								List<EObject> eomcs = new ArrayList<EObject>();
								for(MetaClass mc : mm.getClasses()) eomcs.add(mc);
								List<MetaClass> eomcs2 = new ArrayList<MetaClass>();
								for(EObject eomc: filterClasses(eomcs, pas.getClassSels().getClassFilter().getQualifier())) eomcs2.add((MetaClass)eomc);	
								//System.out.println("CANDIDATE SRCS: " + eomcs2);
								candidateSrcs.addAll(eomcs2);
							}
							else if(eo instanceof PathTarget){
								PathTarget pat = (PathTarget)eo;
								List<EObject> eomcs = new ArrayList<EObject>();
								for(MetaClass mc : mm.getClasses()) eomcs.add(mc);
								List<MetaClass> eomcs2 = new ArrayList<MetaClass>();
								for(EObject eomc: filterClasses(eomcs, pat.getClassSels().getClassFilter().getQualifier())) eomcs2.add((MetaClass)eomc);
								//System.out.println("CANDIDATE TRGS: " + eomcs2);
								candidateTgts.addAll(eomcs2);
							}
						}
						
						if(!pc.calculateAllPaths(candidateSrcs, candidateTgts)) output = pathsInRange(pathCandidates, length[0], length[1]);
						else{
							Path p;
															
							while((p=pc.next()) == null && !pc.isOver());
							
							boolean tooMany = false;
							boolean enough = false;
							
							while(p != null && !tooMany && !enough){
								if(!precandidates.isEmpty()) precandidates.clear();
								precandidates.add(p);
								int i = pathCandidates.size();
								pathCandidates.addAll(filterPaths(precandidates, ps.getPathConditioner().getPathQualifier()));
								//if(pathCandidates.size() > i) System.out.println(p + " is valid");
								if(quant instanceof None && pathCandidates.size() > 0) tooMany = true;
								else if(length[1] == Integer.MAX_VALUE && pathCandidates.size() >= length[0]) enough = true;
								else if(pathCandidates.size() > length[1]) tooMany = true;
								p = pc.next();
							}
							
							//System.out.println("ended");
							
							pc.killCalculation();
							
							if(tooMany) output = false;
							else if(enough) output = true;
							else output = pathsInRange(pathCandidates, length[0], length[1]);						
						}
						
						//System.out.println("result: " + output);
					}
				}
			}				
		}
		
		if(!(s instanceof PathSelector)) this.calculateResults();
		
		rationale = "";
		
		if(!pathSelector){
			rationale += eoCandidates.size();
			if(s instanceof ElementSelector) rationale += " element";
			else if(s instanceof ClassSelector) rationale += " class";
			else if(s instanceof FeatureSelector) rationale += " feature";
			else if(s instanceof AttributeSelector) rationale += " attribute";
			else if(s instanceof ReferenceSelector) rationale += " reference";
			
			if(eoCandidates.size() != 1 && !(s instanceof ClassSelector)) rationale += "s";
			else if(eoCandidates.size() != 1 && (s instanceof ClassSelector)) rationale += "es";
			
			rationale += " match";
			if(eoCandidates.size() == 1) rationale += "es";		
		}else{
			rationale += pathCandidates.size() + " path";
			if(pathCandidates.size() != -1) rationale += "s";
			rationale += " match";
			if(pathCandidates.size() != -1) rationale += "es";
		}
		
		rationale += " (";
		
		if(length[0] == length[1]) rationale += length[0];
		else{
			if(length[1] == -1 || length[1] == Integer.MAX_VALUE || length[1] == 0) rationale += "more than " + length[0];
			else rationale += length[0] + " to " + length[1];
		}
		
		rationale += " expected)";
		this.runningTime = System.currentTimeMillis() - startingTime;
		
		// measure memory consumption
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		this.memoryConsumption = (runtime.totalMemory() - runtime.freeMemory()) / (1024L * 1024L);
		
		return output;
	}
	
	private void calculateResults(){
		this.matches = new ArrayList<AnnotatedElement>();
		this.errors = new ArrayList<AnnotatedElement>();
		
		if(length[0] == 0 && length[1] == 0){
			for(EObject eo : this.eoPrecandidates){
				if(this.eoCandidates.contains(eo)) this.errors.add((AnnotatedElement)eo);
				else this.matches.add((AnnotatedElement)eo);
			}
		}else{		
			for(EObject eo : this.eoPrecandidates){
				if(this.eoCandidates.contains(eo)) this.matches.add((AnnotatedElement)eo);
				else this.errors.add((AnnotatedElement)eo);
			}
		}
	}
	
	public List<AnnotatedElement> getMatches(){
		return this.matches;
	}
	
	public List<AnnotatedElement> getErrors(){
		return this.errors;
	}
	
	private boolean inRange(List<EObject> elements, int min, int max){
		int size;
		if(elements == null) size = 0;
		else size = elements.size();
		
		if(size >= min && (size <= max || max < 0)) return true;
		
		return false;
	}
	
	private boolean pathsInRange(List<Path> elements, int min, int max){
		int size;
		if(elements == null) size = 0;
		else size = elements.size();
		
		if(size >= min && (size <= max || max < 0)) return true;
		
		return false;
	}
	
	private void addElement(List<EObject> list, EObject element){
		if(!list.contains(element)) list.add(element);
	}
	
	private void addPath(List<Path> list, Path path){
		if(!list.contains(path)) list.add(path);
	}
	
	private List<Path> filterPaths(List<Path> paths, Qualifier q){
		List<Path> outPaths = new ArrayList<Path>();
		if(paths == null || paths.isEmpty()) return outPaths = paths;
		if(q == null) return outPaths = paths;
		boolean positive = true;
		if(q.isNegative()) positive = false;
		
		if(q instanceof Existance){
			if(positive) return outPaths = paths;
			if(!positive){
				outPaths.clear();
				return outPaths;
			}
		}
		
		if(q instanceof PathSource){
			PathSource ps = (PathSource)q;
			
			ClassSelector cs = ps.getClassSels();
			Quantifier quant = cs.getQuantifier();			
			int length[];
			
			MetaModel mm = (MetaModel)paths.get(0).getStartingPoint().eContainer();
			
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
			
			length = quant.getQuantity(allClasses.size());
			
			if(length[0] <= 1){
				List<EObject> candidateStarts;
				if(cs.getClassFilter() != null) candidateStarts = filterClasses(allClasses, cs.getClassFilter().getQualifier());
				else candidateStarts = allClasses;				

				for(Path p : paths){
					MetaClass start = p.getStartingPoint();
					if(positive && candidateStarts.contains(start) && length[0] == 1) this.addPath(outPaths, p);
					else if(positive && !candidateStarts.contains(start) && length[1] == 0) this.addPath(outPaths, p);
					else if(!positive && !candidateStarts.contains(start) && length[0] == 1) this.addPath(outPaths, p);
					else if(!positive && candidateStarts.contains(start) && length[1] == 0) this.addPath(outPaths, p);
				}
			}						
		}
		
		if(q instanceof PathTarget){
			PathTarget pt = (PathTarget)q;
			
			ClassSelector cs = pt.getClassSels();
			Quantifier quant = cs.getQuantifier();			
			int length[];
			
			MetaModel mm = (MetaModel)paths.get(0).getStartingPoint().eContainer();
			
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
			
			length = quant.getQuantity(allClasses.size());
			
			if(length[0] <= 1){
				List<EObject> candidateStarts;
				if(cs.getClassFilter() != null) candidateStarts = filterClasses(allClasses, cs.getClassFilter().getQualifier());
				else candidateStarts = allClasses;				

				for(Path p : paths){
					MetaClass end = p.getEndingPoint();
					if(positive && candidateStarts.contains(end) && length[0] == 1) this.addPath(outPaths, p);
					else if(positive && !candidateStarts.contains(end) && length[1] == 0) this.addPath(outPaths, p);
					else if(!positive && !candidateStarts.contains(end) && length[0] == 1) this.addPath(outPaths, p);
					else if(!positive && candidateStarts.contains(end) && length[1] == 0) this.addPath(outPaths, p);
				}
			}	
		}
		
		if(q instanceof StepClass){
			StepClass pt = (StepClass)q;
			
			ClassSelector cs = pt.getClassSels();
			Quantifier quant = cs.getQuantifier();			
			int length[];
			
			MetaModel mm = (MetaModel)paths.get(0).getStartingPoint().eContainer();
			
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
			
			length = quant.getQuantity(allClasses.size());
			
			List<EObject> candidateStarts;
			if(cs.getClassFilter() != null) candidateStarts = filterClasses(allClasses, cs.getClassFilter().getQualifier());
			else candidateStarts = allClasses;				

			for(Path p : paths){
				List<EObject> matchingClasses = new ArrayList<EObject>();
				List<MetaClass> stepClasses = p.getStepPoints();
				
				for(MetaClass mc : stepClasses)
					if(candidateStarts.contains(mc))
						this.addElement(matchingClasses, mc);
				
				if(positive && this.inRange(matchingClasses, length[0], length[1])) this.addPath(outPaths, p);
				else if(!positive && !this.inRange(matchingClasses, length[0], length[1])) this.addPath(outPaths, p);
			}
		}
		
		if(q instanceof ContainmentPath){				
			for(Path p : paths){
				if(positive && p.isContainment()) this.addPath(outPaths, p);
				else if (!positive && !p.isContainment()) this.addPath(outPaths, p);
			}	
		}
		
		if(q instanceof CyclicPath){
			//System.out.println("5");
			for(Path p : paths){
				if(positive && p.isCyclic()) this.addPath(outPaths, p);
				else if (!positive && !p.isCyclic()) this.addPath(outPaths, p);
			}	
		}
		
		if(q instanceof WithInheritancePath){				
			for(Path p : paths){
				if(positive && p.anyInheritedReference()) this.addPath(outPaths, p);
				else if (!positive && !p.anyInheritedReference()) this.addPath(outPaths, p);
			}	
		}
		
		if(q instanceof PathAnd){
			PathAnd eand = (PathAnd) q;
			
			List<Path> andOutPaths = paths;
			
			for(Qualifier child : eand.getChildren()){
				outPaths = filterPaths(andOutPaths, child);
				andOutPaths = outPaths;
			}
			
			return outPaths;
		}
		
		if(q instanceof PathOr){
			PathOr eor = (PathOr) q;
			
			for(Qualifier child: eor.getChildren()){
				List<Path> orOutPaths = filterPaths(paths, child);				
				for(Path p : orOutPaths) this.addPath(outPaths, p);
			}
			
			return outPaths;
		}
		
		return outPaths;
	}
	
	protected List<EObject> filterFeatures(List<EObject> features, Qualifier eq){
		List<EObject> outFeatures = new ArrayList<EObject>();
		if(features == null || features.isEmpty()) return outFeatures = features;
		if(eq == null) return outFeatures = features;
		boolean positive = true;
		if(eq.isNegative()) positive = false;
		
		if(eq instanceof ElementQualifier) return outFeatures = filterElements(features, (ElementQualifier)eq);
		
		if(eq instanceof FeatureMultiplicity){
			FeatureMultiplicity fm = (FeatureMultiplicity)eq;
			int min[] = null, max[] = null;
			if(fm.getMin() != null) min = fm.getMin().getQuantity(-1);
			if(fm.getMax() != null) max = fm.getMax().getQuantity(-1);

			for(EObject eo : features){
				if(eo instanceof Feature){
					Feature f = (Feature)eo;
					Integer fMin = f.getMin();
					Integer fMax = f.getMax();					
					
					/*if(min != null) //System.out.println("checking min multiplicity for " + f.getName() + " (" + fMin + "), which is to be in the range: [" + min[0] + ", " + min[1] + "]");
					if(max != null) //System.out.println("checking max multiplicity for " + f.getName() + " (" + fMax + "), which is to be in the range: [" + max[0] + ", " + max[1] + "]");*/
					
					boolean matches = true;
					
					if(min != null)
						if(fMin < min[0] || (fMin > min[1] && min[1] != -1))
							matches = false;
					
					if(matches && max != null)
						if(fMax < max[0] || (fMax > max[1] && max[1] != -1)) matches = false;
					
					if(matches) this.addElement(outFeatures, f);
				}
			}
		}
		
		if(eq instanceof ClassContainee){
			ClassContainee cc = (ClassContainee)eq;
			ClassSelector cs = cc.getClassSel();
			Quantifier quant = cs.getQuantifier();
			boolean byInheritance = true; 
			if(cc.getByInheritance() != null) byInheritance = !cc.getByInheritance().isValue();
			
			MetaModel mm = (MetaModel)((MetaClass)features.get(0).eContainer()).eContainer();
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
			
			List<EObject> candidateConts;
			if(cs.getClassFilter() != null) candidateConts = filterClasses(allClasses, cs.getClassFilter().getQualifier());
			else candidateConts = allClasses;
			int length[] = quant.getQuantity(candidateConts.size());
			
			for(EObject eo : features){
				if(eo instanceof Feature){
					Feature f = (Feature)eo;
					List<EObject> contMcs = new ArrayList<EObject>();
					
					MetaClass strCont = (MetaClass)f.eContainer();
					if(candidateConts.contains(strCont)) this.addElement(contMcs, strCont);
					
					if(byInheritance){
						for(MetaClass mc : strCont.getAllSubs()){
							if(candidateConts.contains(mc)) this.addElement(contMcs, mc);
						}
					}
					
					if(positive && this.inRange(contMcs, length[0], length[1])) this.addElement(outFeatures, f);
					else if(!positive && !this.inRange(contMcs, length[0], length[1])) this.addElement(outFeatures, f);
				}
			}
		}
		
		if(eq instanceof InheritedFeature){
			InheritedFeature ife = (InheritedFeature)eq;
			
			if(ife.getClassSel() == null){
				for(EObject eo : features){
					Feature f = (Feature)eo;
					MetaClass mc = (MetaClass)f.eContainer();
					if(positive && (mc.getSubclasses() != null && !mc.getSubclasses().isEmpty())) this.addElement(outFeatures, f);
					if(!positive && (mc.getSubclasses() == null || mc.getSubclasses().isEmpty())) this.addElement(outFeatures, f);
				}
			}else{
				ClassSelector cs = ife.getClassSel();
				Quantifier quant = cs.getQuantifier();
				
				MetaModel mm = (MetaModel)((MetaClass)features.get(0).eContainer()).eContainer();
				List<EObject> allClasses = new ArrayList<EObject>();			
				
				for(EObject eo : mm.getClasses())
					if(eo instanceof MetaClass)
						allClasses.add(eo);
				
				List<EObject> candidateConts;
				if(cs.getClassFilter() != null) candidateConts = filterClasses(allClasses, cs.getClassFilter().getQualifier());
				else candidateConts = allClasses;
				int length[] = quant.getQuantity(candidateConts.size());
				
				for(EObject eo : features){
					List<EObject> supers = new ArrayList<EObject>();
					Feature f = (Feature)eo;
					MetaClass mc = (MetaClass)f.eContainer();
					if((mc.getSubclasses() != null && !mc.getSubclasses().isEmpty()) && candidateConts.contains(mc)){
						this.addElement(supers, mc);
						if(positive && this.inRange(supers, length[0], length[1])) this.addElement(outFeatures, f);
						else if(!positive && !this.inRange(supers, length[0], length[1])) this.addElement(outFeatures, f);
					}
				}
			}						
		}
		
		if(eq instanceof AndQualifier){
			AndQualifier eand = (AndQualifier) eq;
			
			List<EObject> andOutFeatures = features;
			
			for(Qualifier child : eand.getChildren()){
				outFeatures = filterFeatures(andOutFeatures, child);
				andOutFeatures = outFeatures;
			}
			
			return outFeatures;
		}
		
		if(eq instanceof OrQualifier){
			OrQualifier eor = (OrQualifier) eq;
			
			for(Qualifier child: eor.getChildren()){
				List<EObject> orOutFeatures = filterFeatures(features, child);				
				for(EObject eo : orOutFeatures) this.addElement(outFeatures, eo);
			}
			
			return outFeatures;
		}
		
		return outFeatures;
	}
	
	protected List<EObject> filterAttributes(List<EObject> attributes, Qualifier eq){
		List<EObject> outAttributes = new ArrayList<EObject>();
		//if(attributes == null || attributes.isEmpty()) return outAttributes = attributes;
		if(eq == null) return outAttributes = attributes;
		boolean positive = true;
		if(eq.isNegative()) positive = false;
		
		if(eq instanceof ElementQualifier) return outAttributes = filterElements(attributes, (ElementQualifier)eq);			
		if(eq instanceof FeatureQualifier) return outAttributes = filterFeatures(attributes, (FeatureQualifier)eq);		
		
		if(eq instanceof Typed){
			Typed t = (Typed)eq;
			String type = t.getType().getValue();

			for(EObject eo : attributes){
				if(eo instanceof Attribute){
					Attribute a = (Attribute)eo;
					if(positive && a.getPrimitiveType().equals(type)) this.addElement(outAttributes, a);
					else if(!positive && !a.getPrimitiveType().equals(type)) this.addElement(outAttributes, a);
				}
			}			
		}
		
		if(eq instanceof AndQualifier){
			AndQualifier eand = (AndQualifier) eq;
			
			List<EObject> andOutAttributes = attributes;
			
			for(Qualifier child : eand.getChildren()){
				outAttributes = filterAttributes(andOutAttributes, child);
				andOutAttributes = outAttributes;
			}
			
			return outAttributes;
		}
		
		if(eq instanceof OrQualifier){
			OrQualifier eor = (OrQualifier) eq;
			
			for(Qualifier child: eor.getChildren()){
				List<EObject> orOutAttributes = filterAttributes(attributes, child);				
				for(EObject eo : orOutAttributes) this.addElement(outAttributes, eo);
			}
			
			return outAttributes;
		}
		
		return outAttributes;
	}
	
	protected List<EObject> filterReferences(List<EObject> references, Qualifier eq){
		List<EObject> outReferences = new ArrayList<EObject>();
		if(references == null || references.isEmpty()) return outReferences = references;
		if(eq == null) return outReferences = references;
		boolean positive = true;
		if(eq.isNegative()) positive = false;
		
		if(eq instanceof ElementQualifier) return outReferences = filterElements(references, (ElementQualifier)eq);
		if(eq instanceof FeatureQualifier) return outReferences = filterFeatures(references, (FeatureQualifier)eq);	
		
		if(eq instanceof ContainmentReference){
			//ContainmentReference cr = (ContainmentReference)eq;

			for(EObject eo : references){
				if(eo instanceof Reference){
					Reference r = (Reference)eo;
					if(positive && r.isAnnotated("containment"))this.addElement(outReferences, r);
					else if(!positive && !r.isAnnotated("containment")) this.addElement(outReferences, r);
				}
			}
			
			return outReferences;
		}
		
		if(eq instanceof SourceClass){
			SourceClass sc = (SourceClass)eq;
			boolean byInheritance = true; 
			if(sc.getByInheritance() != null) byInheritance = !sc.getByInheritance().isValue();

			ClassSelector cs = sc.getClassSel();
			Quantifier quant = cs.getQuantifier();
			
			MetaModel mm = (MetaModel)((MetaClass)references.get(0).eContainer()).eContainer();
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
			
			List<EObject> candidateSrcs;
			if(cs.getClassFilter() != null) candidateSrcs = filterClasses(allClasses, cs.getClassFilter().getQualifier());
			else candidateSrcs = allClasses;
			int length[] = quant.getQuantity(candidateSrcs.size());
			
			for(EObject eo : references){
				if(eo instanceof Reference){
					List<EObject> sourceClasses = new ArrayList<EObject>();
					Reference r = (Reference)eo;
					
					for(EObject eomc : candidateSrcs){
						if(eomc instanceof MetaClass){
							MetaClass mc = (MetaClass)eomc;
							
							if(mc.equals((MetaClass)r.eContainer())) this.addElement(sourceClasses, mc);
							else if(byInheritance){
								for(MetaClass supermc : mc.getAllSupers())
									if(supermc.equals((MetaClass)r.eContainer())) 
										this.addElement(sourceClasses, mc);
							}
						}
					}			

					if(positive && this.inRange(sourceClasses, length[0], length[1])) this.addElement(outReferences, r);
					else if(!positive && this.inRange(sourceClasses, length[0], length[1])) this.addElement(outReferences, r);
				}
			}	
		}
		
		if(eq instanceof TargetClass){
			TargetClass tc = (TargetClass)eq;
			boolean byInheritance = true; 
			if(tc.getByInheritance() != null) byInheritance = !tc.getByInheritance().isValue();
			
			ClassSelector cs = tc.getClassSel();
			Quantifier quant = cs.getQuantifier();
			
			MetaModel mm = (MetaModel)((MetaClass)references.get(0).eContainer()).eContainer();
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
			
			List<EObject> candidateTars;
			if(cs.getClassFilter() != null) candidateTars = filterClasses(allClasses, cs.getClassFilter().getQualifier());
			else candidateTars = allClasses;
			int length[] = quant.getQuantity(candidateTars.size());
			
			for(EObject eo : references){
				if(eo instanceof Reference){
					List<EObject> targetClasses = new ArrayList<EObject>();
					Reference r = (Reference)eo;
					
					for(EObject eomc : candidateTars){
						if(eomc instanceof MetaClass){
							MetaClass mc = (MetaClass)eomc;
							
							if(mc.equals(r.getReference())) this.addElement(targetClasses, mc);
							else if(byInheritance){
								for(MetaClass supermc : mc.getAllSupers())
									if(supermc.equals(r.getReference())) 
										this.addElement(targetClasses, mc);
							}
						}
					}
					
					if(positive && this.inRange(targetClasses, length[0], length[1])) this.addElement(outReferences, r);
					else if(!positive && !this.inRange(targetClasses, length[0], length[1])) this.addElement(outReferences, r);
				}
			}
		}
		
		if(eq instanceof OppositeTo){
			OppositeTo ot = (OppositeTo)eq;
			
			ReferenceSelector rs = ot.getRefSel();
			Quantifier quant = rs.getQuantifier();
			
			MetaModel mm = (MetaModel)((MetaClass)references.get(0).eContainer()).eContainer();
			List<EObject> allRefs = new ArrayList<EObject>();			
			for(EObject eo : mm.getAllElements())
				if(eo instanceof Reference)
					allRefs.add(eo);
			
			List<EObject> candidateRefs;
			if(rs.getReferenceFilter() != null) candidateRefs = filterReferences(allRefs, rs.getReferenceFilter().getQualifier());
			else candidateRefs = allRefs;
			int length[] = quant.getQuantity(candidateRefs.size());
			
			for(EObject eo : references){
				if(eo instanceof Reference){
					Reference r = (Reference)eo;
					List<EObject> opposites = new ArrayList<EObject>();
					
					if(r.isAnnotated("opposite")){
						//System.out.println(((MetaClass)r.eContainer()).getName() + "." + r.getName() + " is annotated @opposite");
						
						for(Annotation a : r.getAnnotations()){
							if(a.getName().equals("opposite")){
								for(AnnotationParam ap : a.getParams()){
									//System.out.println("detected param named: " + ap.getName());
									
									if(ap.getName().equals("opp")){
										ElementValue ev = (ElementValue)ap.getValue();
										Reference opRef = (Reference)ev.getValue();										
										//System.out.println("opp = " + opRef.getName());
										if(candidateRefs.contains(opRef)) opposites.add(opRef);
									}
								}
							}
						}
					
						if(positive && this.inRange(opposites, length[0], length[1])) this.addElement(outReferences, r);
						else if(!positive && !this.inRange(opposites, length[0], length[1])) this.addElement(outReferences, r);
					}
				}					
			}	
		}
		
		if(eq instanceof AndQualifier){
			AndQualifier eand = (AndQualifier) eq;
			
			List<EObject> andOutReferences = references;
			
			for(Qualifier child : eand.getChildren()){
				outReferences = filterReferences(andOutReferences, child);
				andOutReferences = outReferences;
			}
			
			return outReferences;
		}
		
		if(eq instanceof OrQualifier){
			OrQualifier eor = (OrQualifier) eq;
			
			for(Qualifier child: eor.getChildren()){
				List<EObject> orOutElements = filterReferences(references, child);				
				for(EObject eo : orOutElements) this.addElement(outReferences, eo);
			}
			
			return outReferences;
		}
		
		return outReferences;
	}
	
	protected List<EObject> filterClasses(List<EObject> classes, Qualifier eq){
		List<EObject> outClasses = new ArrayList<EObject>();
		//if(classes == null || classes.isEmpty()) return outClasses = classes;		
		if(eq == null) return outClasses = classes;
		boolean positive = true;
		if(eq.isNegative()) positive = false;
		
		if(eq instanceof ElementQualifier) return outClasses = filterElements(classes, (ElementQualifier)eq);
		
		if(eq instanceof InhLeaf){
			/* hay algo que falla. revisar */
			for(EObject eo : classes){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;					
					
					if(positive && (mc.getSubclasses() == null || mc.getSubclasses().isEmpty()) && mc.getSupers() != null && !mc.getSupers().isEmpty()) this.addElement(outClasses, mc);
					else if(!positive && (mc.getSubclasses() != null && !mc.getSubclasses().isEmpty()) || (mc.getSupers() == null || mc.getSupers().isEmpty())) this.addElement(outClasses, mc);
				}
			}
			
			return outClasses;
		}
		
		if(eq instanceof InhRoot){
			for(EObject eo : classes){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;					
					
					if(positive && (mc.getSupers() == null || mc.getSupers().isEmpty()) && (mc.getSubclasses() != null && !mc.getSubclasses().isEmpty())) this.addElement(outClasses, mc);
					if(!positive && ((mc.getSupers() != null && !mc.getSupers().isEmpty()) || (mc.getSubclasses() == null || mc.getSubclasses().isEmpty()))) this.addElement(outClasses, mc);					
				}
			}
			
			return outClasses;
		}
		
		if(eq instanceof ContLeaf){
			for(EObject eo : classes){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;					
					
					int incomingRefs = mc.getIncomingRefs(true, false, ReferenceImpl.CONTAINMENT).size();
					int ownRefs = mc.getReferences(true, ReferenceImpl.CONTAINMENT).size();
					
					if(positive && incomingRefs > 0 && ownRefs == 0) this.addElement(outClasses, mc);
					if(!positive && (incomingRefs == 0 || ownRefs > 0)) this.addElement(outClasses, mc);					
				}
			}
			
			return outClasses;
		}
		
		if(eq instanceof ContRoot){
			ContRoot cr = (ContRoot)eq;			
			boolean absoluteRoot = false;
			
			if(cr.getAbsolute() != null){
				// lame, but yet...
				absoluteRoot = !cr.getAbsolute().isValue();
			}
			
			if(!absoluteRoot){
				for(EObject eo : classes){
					if(eo instanceof MetaClass){
						MetaClass mc = (MetaClass)eo;					
						
						int incomingRefs = mc.getIncomingRefs(true, false, ReferenceImpl.CONTAINMENT).size();
						int ownRefs = mc.getReferences(true, ReferenceImpl.CONTAINMENT).size();
						
						if(positive && incomingRefs == 0 && ownRefs > 0) this.addElement(outClasses, mc);
						if(!positive && (incomingRefs > 0 || ownRefs == 0)) this.addElement(outClasses, mc);					
					}
				}
			}else{
				if(classes.size() == 0) return outClasses = classes;
				List<MetaClass> allClasses = ((MetaModel)classes.get(0).eContainer()).getClasses();
				boolean found = false;
				MetaClass mc = null;
				
				if(allClasses.size() == 1){
					mc = allClasses.get(0);
					found = true;
				}
				
				for(int i = 0; i<classes.size() && !found; i++){
					if(classes.get(i) instanceof MetaClass){
						mc = (MetaClass)classes.get(i);	
						
						boolean candidate = true;
						
						for(MetaClass tarMC : allClasses){
							if(!tarMC.getId().equals(mc.getId()) && !tarMC.isSubTo(mc)){
								if(!tarMC.getIsAbstract() && !mc.references(tarMC, 1, -1, 0, ReferenceImpl.CONTAINMENT, true, true, null)) candidate = false;
							}
						}
						
						if(candidate) found = true;
					}
				}
				
				if(positive && found) this.addElement(outClasses, mc);
				if(!positive && found) for(EObject outmc : classes) if(!((MetaClass)outmc).getId().equals(mc.getId())) this.addElement(outClasses, outmc);
				if(!positive && !found) for(EObject outmc : classes) this.addElement(outClasses, outmc);
			}			
			
			return outClasses;
		}
		
		if(eq instanceof Abstraction){
			for(EObject eo : classes){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;					
					
					if(positive && mc.getIsAbstract()) this.addElement(outClasses, mc);
					if(!positive && !mc.getIsAbstract()) this.addElement(outClasses, mc);					
				}
			}
			
			return outClasses;
		}
		
		if(eq instanceof IsSuperTo){
			IsSuperTo ist = (IsSuperTo)eq;		
			if(ist.getClassSels() == null || ist.getClassSels() == null) return outClasses = classes;
			if(classes.size() == 0) return outClasses = classes;
				
			ClassSelector cs = ist.getClassSels();
			Quantifier quant = cs.getQuantifier();			
			int length[];
			
			MetaModel mm = (MetaModel)classes.get(0).eContainer();
			
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
			
			List<EObject> candidateSupers;
			if(cs.getClassFilter() != null) candidateSupers = filterClasses(allClasses, cs.getClassFilter().getQualifier());
			else candidateSupers = allClasses;
			length = quant.getQuantity(candidateSupers.size());
			
			// 'or-equal' condition
			boolean orEqual = false;
			
			if(ist.getOrEqual() != null){
				//lame, but yet...
				orEqual = !ist.getOrEqual().isValue();
			}
			
			List<EObject> precandidates = new ArrayList<EObject>();

			for(EObject eo : classes){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					
					for(EObject supeo : candidateSupers){
						if(orEqual){
							if(positive && mc.getId().equals(((MetaClass)supeo).getId())) precandidates.add(supeo);
							else if(!positive && !mc.getId().equals(((MetaClass)supeo).getId())) precandidates.add(supeo);
						}else{						
							boolean paths = true;
							int minHeight = 1;
							int maxHeight = -1;
							
							if(ist.getLength() != null){
								minHeight = ist.getLength().getMin().getValue();
								if(ist.getLength().getMax() != null) maxHeight = ist.getLength().getMax().getValue();
							}
							
							if(ist.getPaths() != null){
								int npaths = ((MetaClass)supeo).getInheritancePathsTo(mc, minHeight, maxHeight);
								if(npaths < ist.getPaths().getMin().getValue()) paths = false;
								else if(ist.getPaths().getMax() != null && 
										ist.getPaths().getMax().getValue() != -1 && 
										npaths > ist.getPaths().getMax().getValue()) paths = false;
							}
							
	
							if(positive && mc.isSuperTo((MetaClass)supeo, minHeight, maxHeight) && paths) precandidates.add(supeo);
							else if(!positive && (!mc.isSuperTo((MetaClass)supeo, minHeight, maxHeight) || !paths)) precandidates.add(supeo);
						}
					}
					
					if(inRange(precandidates, length[0], length[1])) this.addElement(outClasses, eo);
					precandidates.clear();
				}
			}
			
			return outClasses;
		}
		
		if(eq instanceof IsSubTo){
			IsSubTo ist = (IsSubTo)eq;		
			if(ist.getClassSels() == null || ist.getClassSels() == null || classes.size() == 0) return outClasses = classes;
			
			ClassSelector cs = ist.getClassSels();
			Quantifier quant = cs.getQuantifier();			
			int length[];
			
			MetaModel mm = (MetaModel)classes.get(0).eContainer();
			
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
			
			List<EObject> candidateSubs;
			if(cs.getClassFilter() != null) candidateSubs = filterClasses(allClasses, cs.getClassFilter().getQualifier());
			else candidateSubs = allClasses;
			length = quant.getQuantity(candidateSubs.size());
			
			// 'or-equal' condition
			boolean orEqual = false;
						
			if(ist.getOrEqual() != null){
				//lame, but yet...
				orEqual = !ist.getOrEqual().isValue();
			}
			
			List<EObject> precandidates = new ArrayList<EObject>();
			
			for(EObject eo : classes){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					
					for(EObject supeo : candidateSubs){
						if(orEqual){
							if(positive && mc.getId().equals(((MetaClass)supeo).getId())) precandidates.add(supeo);
							else if(!positive && !mc.getId().equals(((MetaClass)supeo).getId())) precandidates.add(supeo);
						}else{	
							boolean paths = true;
							int minHeight = 1;
							int maxHeight = -1;
							
							if(ist.getLength() != null){
								minHeight = ist.getLength().getMin().getValue();
								if(ist.getLength().getMax() != null) maxHeight = ist.getLength().getMax().getValue();
							}
							
							//System.out.println("min = " + minHeight);
							//System.out.println("max = " + maxHeight);
							
							if(ist.getPaths() != null){
								int npaths = mc.getInheritancePathsTo((MetaClass)supeo, minHeight, maxHeight);
								if(npaths < ist.getPaths().getMin().getValue()) paths = false;
								else if(ist.getPaths().getMax() != null && 
										ist.getPaths().getMax().getValue() != -1 && 
										npaths > ist.getPaths().getMax().getValue()) paths = false;
							}
							
							if(positive && mc.isSubTo((MetaClass)supeo, minHeight, maxHeight) && paths) precandidates.add(supeo);
							else if(!positive && (!mc.isSubTo((MetaClass)supeo, minHeight, maxHeight) || !paths)) precandidates.add(supeo);
						}
					}
					
					if(inRange(precandidates, length[0], length[1])) this.addElement(outClasses, eo);
					precandidates.clear();
				}
			}
			
			return outClasses;
		}
		
		if(eq instanceof SelfInheritance){
			SelfInheritance si = (SelfInheritance)eq;
			
			for(EObject eo : classes){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					
					boolean paths = true;
					int minHeight = 1;
					int maxHeight = -1;
					
					if(si.getLength() != null){
						minHeight = si.getLength().getMin().getValue();
						if(si.getLength().getMax() != null) maxHeight = si.getLength().getMax().getValue();
					}
					
					if(si.getPaths() != null){
						int npaths = mc.getInheritancePathsTo(mc, minHeight, maxHeight);
						if(npaths < si.getPaths().getMin().getValue()) paths = false;
						else if(si.getPaths().getMax() != null && 
								si.getPaths().getMax().getValue() != -1 && 
								npaths > si.getPaths().getMax().getValue()) paths = false;
					}
					
					if(positive && mc.isSubTo(mc, minHeight, maxHeight) && paths) this.addElement(outClasses, mc);
					else if(!positive && (!mc.isSubTo(mc, minHeight, maxHeight) || !paths)) this.addElement(outClasses, mc);
				}
			}
			
			return outClasses;
		}
		
		if(eq instanceof ClassReaches){
			ClassReaches cr = (ClassReaches)eq;		
			if(cr.getClassSels() == null || cr.getClassSels().isEmpty() || classes.size() == 0) return outClasses = classes;
			
			ClassSelector cs = cr.getClassSels().get(0);
			Quantifier quant = cs.getQuantifier();			
			int length[];
			
			MetaModel mm = (MetaModel)classes.get(0).eContainer();
			
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
			
			List<EObject> candidateTargets;
			boolean noFilter = false;
			
			if(cs.getClassFilter() != null) candidateTargets = filterClasses(allClasses, cs.getClassFilter().getQualifier());			
			else{
				candidateTargets = allClasses;				
				noFilter = true;
			}
			
			length = quant.getQuantity(candidateTargets.size());
									
			int refSort = ReferenceImpl.IRRELEVANT;
			
			if(cr.getContainment() != null){
				// lame, but yet...
				boolean containment = !cr.getContainment().isValue();
				if(containment) refSort = ReferenceImpl.CONTAINMENT;
				else refSort = ReferenceImpl.ASSOCIATION;		
			}			
			
			boolean inheritance = true; // default = true?
			if(cr.getByInheritance() != null) inheritance = !cr.getByInheritance().isValue();
			
			boolean takeSubs = true;			
			if(cr.getStrict() != null) takeSubs = cr.getStrict().isValue();
			
			// jumps
			int min = 1;
			int max = -1;
			
			Length pathLength = cr.getJumps();
			
			if(pathLength != null){
				if(pathLength.getMin() != null) min = pathLength.getMin().getValue();
				if(pathLength.getMax() != null) max = pathLength.getMax().getValue();
				else max = -1;
			}
			
			List<EObject> precandidates = new ArrayList<EObject>();
				
			for(int i = 0 ; i<classes.size(); i++){
				if(classes.get(i) instanceof MetaClass){
					MetaClass mc = (MetaClass)classes.get(i);
					
					for(int j = 0; j<candidateTargets.size(); j++){
						EObject supeo = candidateTargets.get(j);
						if(mc.references((MetaClass)supeo, min, max, 0, refSort, inheritance, takeSubs, null)){
							//System.out.println(mc.getName() + " references " + ((MetaClass)supeo).getName() + " in min " + min + " and max " + max);
							if(eq instanceof ClassCollects){
								// particular case in which the qualifier constraints the path's collected multiplicity
								ClassCollects cc = (ClassCollects)eq;
								PathCalculator pc = new PathCalculator(mm);
								List<Path> paths = pc.getPaths(mc, (MetaClass)supeo, refSort, inheritance);							
								
								if(!paths.isEmpty()){
									int expectedColl[] = cc.getCollectedMult().getQuantity(-1);	
									int currentColl[] = paths.get(0).getCollectedMultiplicity();
									
									for(int k=0; k<paths.size(); k++){
										int collection[] = paths.get(k).getCollectedMultiplicity();
										if(currentColl[0] < collection[0]) currentColl[0] = collection[0];
										if(currentColl[1] < collection[1] && currentColl[1] != -1) currentColl[1] = collection[1];
										else if(collection[1] == -1) currentColl[1] = -1; 
									}
									
									if(currentColl[0] == expectedColl[0] && currentColl[1] == expectedColl[1]) this.addElement(precandidates, supeo);
								}								
								
							}
							else this.addElement(precandidates, supeo);
						}
						
						// if the classes to reach have no filter, the condition is met once enough matches have been found
						if(noFilter){
							if(length[0] == 0 && length[1] == 0){
								if(!inRange(precandidates, length[0], length[1])){
									j = candidateTargets.size();
								}
							}else if(inRange(precandidates, length[0], length[1])){
								j = candidateTargets.size();
							}
						}
					}
					
					if(inRange(precandidates, length[0], length[1])) this.addElement(outClasses, mc);
					precandidates.clear();
				}
			}
			
			return outClasses;
		}
		
		if(eq instanceof ClassReachedBy){
			ClassReachedBy crb = (ClassReachedBy)eq;		
			if(crb.getClassSels() == null || crb.getClassSels().isEmpty()) return outClasses = classes;
			if(classes == null || classes.isEmpty()) return outClasses = classes;
			
			ClassSelector cs = crb.getClassSels().get(0);
			Quantifier quant = cs.getQuantifier();			
			int length[];
			
			MetaModel mm = (MetaModel)classes.get(0).eContainer();
			
			List<EObject> allClasses = new ArrayList<EObject>();			
			for(EObject eo : mm.getClasses())
				if(eo instanceof MetaClass)
					allClasses.add(eo);
						
			List<EObject> candidateSources;
			boolean noFilter = false;
			
			if(cs.getClassFilter() != null) candidateSources = filterClasses(allClasses, cs.getClassFilter().getQualifier());
			else{
				candidateSources = allClasses;				
				noFilter = true;
			}
			length = quant.getQuantity(candidateSources.size());
						
			int refSort = ReferenceImpl.IRRELEVANT;
			
			if(crb.getContainment() != null){
				// lame, but yet...
				boolean containment = !crb.getContainment().isValue();
				if(containment) refSort = ReferenceImpl.CONTAINMENT;
				else refSort = ReferenceImpl.ASSOCIATION;					
			}
			
			boolean inheritance = true; // default = true?
			if(crb.getByInheritance() != null) inheritance = !crb.getByInheritance().isValue();
			
			boolean takeSubs = true;			
			if(crb.getStrict() != null) takeSubs = crb.getStrict().isValue();
			
			// jumps
			int min = 1;
			int max = -1;
			
			Length pathLength = crb.getJumps();
			
			if(pathLength != null){
				if(pathLength.getMin() != null) min = pathLength.getMin().getValue();
				if(pathLength.getMax() != null) max = pathLength.getMax().getValue();
				else max = -1;
			}
			
			List<EObject> precandidates = new ArrayList<EObject>();
				
			for(EObject eo : classes){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					
					for(int i = 0; i<candidateSources.size(); i++){
						EObject supeo = candidateSources.get(i);
						
						if(mc.isReferencedFrom(((MetaClass)supeo), min, max, 0, refSort, inheritance, takeSubs, null))
							precandidates.add(supeo);
						
						if(noFilter){
							if(length[0] == 0 && length[1] == 0){
								if(!inRange(precandidates, length[0], length[1])){
									i = candidateSources.size();
								}
							}else if(inRange(precandidates, length[0], length[1])){
								i = candidateSources.size();
							}
						}
					}
					
					if(inRange(precandidates, length[0], length[1])) this.addElement(outClasses, eo);
					precandidates.clear();
				}
			}
			
			return outClasses;
		}
		
		if(eq instanceof FeatureContainment){
			FeatureContainment fc = (FeatureContainment)eq;
			if(fc.getFeatSels() == null || fc.getFeatSels().isEmpty()) return outClasses = classes;
			
			FeatureSelector fs = fc.getFeatSels().get(0);
			Quantifier quant = fs.getQuantifier();			
			int length[];
			
			boolean inheritance = true; // default = true?
			if(fc.getByInheritance() != null) inheritance = !fc.getByInheritance().isValue();
			
			for(EObject eo : classes){
				MetaClass mc = (MetaClass)eo;
				List<EObject> candidateFeatures = new ArrayList<EObject>();
				if(inheritance) for(Feature f : mc.getFeatures(true)) candidateFeatures.add(f);
				else for(Feature f : mc.getFeatures(false)) candidateFeatures.add(f);
				List<EObject> features;
				if(fs.getFeatureFilter() != null) features = filterFeatures(candidateFeatures, fs.getFeatureFilter().getQualifier());
				else features = candidateFeatures;
				length = quant.getQuantity(candidateFeatures.size());
				if(positive && this.inRange(features, length[0], length[1])) if(!outClasses.contains(mc)) this.addElement(outClasses, mc);
				if(!positive && !this.inRange(features, length[0], length[1])) if(!outClasses.contains(mc)) this.addElement(outClasses, mc);
			}
			
			return outClasses;
			
		}
		
		if(eq instanceof AttributeContainment){
			AttributeContainment ac = (AttributeContainment)eq;
			if(ac.getAttSels() == null || ac.getAttSels().isEmpty()) return outClasses = classes;
			
			AttributeSelector as = ac.getAttSels().get(0);
			Quantifier quant = as.getQuantifier();			
			int length[];
			
			boolean inheritance = true; // default = true?
			if(ac.getByInheritance() != null) inheritance = !ac.getByInheritance().isValue();
			
			for(EObject eo : classes){
				MetaClass mc = (MetaClass)eo;
				List<EObject> candidateAttributes = new ArrayList<EObject>();
				if(inheritance) for(Attribute a : mc.getAttributes(true)) candidateAttributes.add(a);
				else for(Attribute a : mc.getAttributes(false)) candidateAttributes.add(a);
				List<EObject> attributes;
				if(as.getAttributeFilter() != null) attributes = filterAttributes(candidateAttributes, as.getAttributeFilter().getQualifier());
				else attributes = candidateAttributes;
				length = quant.getQuantity(candidateAttributes.size());
				if(positive && this.inRange(attributes, length[0], length[1])) if(!outClasses.contains(mc)) this.addElement(outClasses, mc);
				if(!positive && !this.inRange(attributes, length[0], length[1])) if(!outClasses.contains(mc)) this.addElement(outClasses, mc);
			}
			
			return outClasses;
			
		}
		
		if(eq instanceof ReferenceContainment){
			ReferenceContainment rc = (ReferenceContainment)eq;
			if(rc.getRefSels() == null || rc.getRefSels().isEmpty()) return outClasses = classes;
			
			ReferenceSelector rs = rc.getRefSels().get(0);
			Quantifier quant = rs.getQuantifier();			
			int length[];
			
			boolean inheritance = true; // default = true?
			if(rc.getByInheritance() != null) inheritance = !rc.getByInheritance().isValue();
			
			for(EObject eo : classes){
				MetaClass mc = (MetaClass)eo;
				List<EObject> candidateReferences = new ArrayList<EObject>();
				if(inheritance) for(Reference r : mc.getReferences(true, ReferenceImpl.IRRELEVANT)) candidateReferences.add(r);
				else for(Reference r : mc.getReferences(false, ReferenceImpl.IRRELEVANT)) candidateReferences.add(r);
				List<EObject> references;
				if(rs.getReferenceFilter() != null) references = filterReferences(candidateReferences, rs.getReferenceFilter().getQualifier());
				else references = candidateReferences;
				length = quant.getQuantity(candidateReferences.size());
				if(positive && this.inRange(references, length[0], length[1])) if(!outClasses.contains(mc)) this.addElement(outClasses, mc);
				if(!positive && !this.inRange(references, length[0], length[1])) if(!outClasses.contains(mc)) this.addElement(outClasses, mc);
			}
			
			return outClasses;
			
		}
		
		if(eq instanceof AndQualifier){
			AndQualifier eand = (AndQualifier) eq;
			
			List<EObject> andOutClasses = classes;
			
			for(Qualifier child : eand.getChildren()){
				outClasses = filterClasses(andOutClasses, child);
				andOutClasses = outClasses;
			}
			
			return outClasses;
		}
		
		if(eq instanceof OrQualifier){
			OrQualifier eor = (OrQualifier) eq;
			
			for(Qualifier child: eor.getChildren()){
				List<EObject> orOutElements = filterClasses(classes, child);				
				for(EObject eo : orOutElements) this.addElement(outClasses, eo);
			}
			
			return outClasses;
		}
		
		return outClasses = classes;
	}
	
	protected List<EObject> filterElements(List<EObject> elements, Qualifier eq){
		List<EObject> outElements = new ArrayList<EObject>();
		if(elements == null || elements.isEmpty()) return outElements = elements;		
		if(eq == null) return outElements = elements;
		boolean positive = true;
		if(eq.isNegative()) positive = false;
		
		if(eq instanceof NamedAfter){
			NamedAfter na = (NamedAfter)eq;
			
			Selector s = na.getElementSels();
			Quantifier quant = s.getQuantifier();
			List<EObject> candidateElements = null;
			
			MetaModel mm = (MetaModel)((MetaClass)elements.get(0).eContainer()).eContainer();
			
			if(s instanceof ElementSelector){
				ElementSelector es = (ElementSelector)s;
			
				List<EObject> allElements = new ArrayList<EObject>();			
				allElements.addAll(mm.getAttributes());
				allElements.addAll(mm.getReferences());
				allElements.addAll(mm.getClasses());
				
				if(es.getElementFilter() != null) candidateElements = filterElements(allElements, es.getElementFilter().getQualifier());
				else candidateElements = allElements;								
				
			}else if(s instanceof ClassSelector){
				ClassSelector cs = (ClassSelector)s;
				
				List<EObject> allClasses = new ArrayList<EObject>();			
				
				for(EObject eo : mm.getClasses())
					if(eo instanceof MetaClass)
						allClasses.add(eo);
				
				if(cs.getClassFilter() != null) candidateElements = filterClasses(allClasses, cs.getClassFilter().getQualifier());
				else candidateElements = allClasses;
				
			}else if(s instanceof FeatureSelector){
				FeatureSelector fs = (FeatureSelector)s;
				
				List<EObject> allFeatures = new ArrayList<EObject>();			
				
				for(EObject eo : mm.getAttributes())
					if(eo instanceof Attribute)
						allFeatures.add(eo);
				
				for(EObject eo : mm.getReferences())
					if(eo instanceof Reference)
						allFeatures.add(eo);
				
				if(fs.getFeatureFilter() != null) candidateElements = filterFeatures(allFeatures, fs.getFeatureFilter().getQualifier());
				else candidateElements = allFeatures;
				
			}else if(s instanceof AttributeSelector){
				AttributeSelector as = (AttributeSelector)s;
				
				List<EObject> allAttributes = new ArrayList<EObject>();		
				
				for(EObject eo : mm.getAttributes())
					if(eo instanceof Attribute)
						allAttributes.add(eo);
				
				if(as.getAttributeFilter() != null) candidateElements = filterAttributes(allAttributes, as.getAttributeFilter().getQualifier());
				else candidateElements = allAttributes;
				
			}else if(s instanceof ReferenceSelector){
				ReferenceSelector rs = (ReferenceSelector)s;
				
				List<EObject> allReferences = new ArrayList<EObject>();		
				
				for(EObject eo : mm.getReferences())
					if(eo instanceof Reference)
						allReferences.add(eo);
				
				if(rs.getReferenceFilter() != null) candidateElements = filterReferences(allReferences, rs.getReferenceFilter().getQualifier());
				else candidateElements = allReferences;
			}
			
			int length[] = quant.getQuantity(candidateElements.size());
			List<EObject> precandidates = new ArrayList<EObject>();
			
			for(EObject eo : elements){
				boolean found = false;
				String name = null;
				
				if(eo instanceof MetaClass) name = ((MetaClass)eo).getName();
				else if(eo instanceof Feature) name = ((Feature)eo).getName();
				
				for(EObject eo2 : candidateElements){
					String name2 = null;
					if(eo2 instanceof MetaClass) name2 = ((MetaClass)eo2).getName();
					else if(eo2 instanceof Feature) name2 = ((Feature)eo2).getName();
					
					if(name.toLowerCase().equals(name2.toLowerCase())) found = true;
				}
				
				if(found) precandidates.add(eo);
			}
			
			if(positive && inRange(precandidates, length[0], length[1])) for(EObject eo : precandidates) this.addElement(outElements, eo);
			if(!positive && !inRange(precandidates, length[0], length[1])) for(EObject eo : precandidates) this.addElement(outElements, eo);
			
			return outElements;
		}
		
		if(eq instanceof PhraseNamed){
			PhraseNamed pn = (PhraseNamed)eq;
			
			// the only thing to check is whether the name is or isn't camelized
			if(pn.isPascal() && ((pn.getWords().isEmpty() || pn.getWords() == null)) && (pn.getStart() == null) && (pn.getEnd() == null)){
				for(EObject eo : elements){
					String name = null;
					if(eo instanceof MetaClass) name = ((MetaClass)eo).getName();
					else if(eo instanceof Feature) name = ((Feature)eo).getName();
					
					if(positive && StringUtils.startsUpperCase(name)){
						this.addElement(outElements, eo);
						/*boolean anotherUpperCaseFound = false;
						
						for(int i = 1; (i< name.length()-2) && !anotherUpperCaseFound; i++)
							if(name.substring(i,i+1).toUpperCase().equals(name.substring(i,i+1)))
								anotherUpperCaseFound = true;
						
						if(positive && anotherUpperCaseFound) this.addElement(outElements, eo);
						else if(!positive && !anotherUpperCaseFound) this.addElement(outElements, eo);*/
					}else if (!positive && !name.substring(0,1).toUpperCase().equals(name.substring(0,1))) this.addElement(outElements, eo);
				}
				
				return outElements;
			}
			
			// the only thing to check is whether the name is or isn't pascal
			if(pn.isCamelized() && ((pn.getWords().isEmpty() || pn.getWords() == null)) && (pn.getStart() == null) && (pn.getEnd() == null)){
				for(EObject eo : elements){
					String name = null;
					if(eo instanceof MetaClass) name = ((MetaClass)eo).getName();
					else if(eo instanceof Feature) name = ((Feature)eo).getName();
					
					if(positive && StringUtils.startsLowerCase(name)){
						this.addElement(outElements, eo);
						/*boolean anotherUpperCaseFound = false;
						
						for(int i = 1; (i<name.length()-2) && !anotherUpperCaseFound; i++)
							if(name.substring(i,i+1).toUpperCase().equals(name.substring(i,i+1)))
								anotherUpperCaseFound = true;
						
						if(positive && anotherUpperCaseFound) this.addElement(outElements, eo);
						else if(!positive && !anotherUpperCaseFound) this.addElement(outElements, eo);*/
					}else if (!positive && !name.substring(0,1).toUpperCase().equals(name.substring(0,1))) this.addElement(outElements, eo);
				}
				
				return outElements;
			}
			
			// name composition, provided that it is camelized
			if((pn.isCamelized() || pn.isPascal()) && pn.getWords() != null && !pn.getWords().isEmpty()){
				Wordnet wn = new Wordnet();
				
				for(EObject eo : elements){	
					boolean first = true;
					String remain = null;
					if(eo instanceof MetaClass) remain = ((MetaClass)eo).getName();
					else if(eo instanceof Feature) remain = ((Feature)eo).getName();
					
					if(!positive && pn.isPascal() && !StringUtils.startsUpperCase(remain)) this.addElement(outElements, eo);
					else if(!positive && pn.isCamelized() && !StringUtils.startsLowerCase(remain)) this.addElement(outElements, eo);
					else {
						boolean match = true;
						
						for(NamePart np : pn.getWords()){
							if(remain != null){
								String word;
								
								if(pn.isCamelized() && first){
									word = StringUtils.getFirstLowerCaseWord(remain);
									first = false;
								}else word = StringUtils.getFirstUpperCaseWord(remain);
								
								if(word != null){		
									if(np instanceof Literal){
										Literal l = (Literal)np;										
										if(!l.getLiteral().getValue().equals(word)) match = false;								
									}
									
									if(np instanceof Verb)
										if(!wn.isVerb(word)) 
											match = false;
									
									if(np instanceof Noun){
										Noun n = (Noun)np;
										
										if(!wn.isNoun(word)) match = false;
										else{
											if(n.getNum() != GrammaticalNumber.NULL){
												if(n.getNum() == GrammaticalNumber.PLURAL)
													if(!LexicalInflector.isPlural(word)) match = false;
												
												if(n.getNum() == GrammaticalNumber.SINGULAR)
													if(!LexicalInflector.isSingular(word)) match = false;
											}
										}
									}
									
									if(np instanceof Adjective)
										if(!wn.isAdjective(word)) 
											match = false;
									
									if(np instanceof Synonym){
										Synonym s = (Synonym)np;
										if(word == null || word.length() <= 1 || s.getName().getValue() == null){
											match = false;							
										}
										else{
											if(!wn.areSynonyms(word, s.getName().getValue())) match = false;
										}
									}
																											
									remain = StringUtils.removeFirstWordToUpper(remain);	
								}else match = false;
							}else match = false;
							
							if(!match) remain = null;
						}
						
						if(remain != null && remain.length() > 0) match = false;
						
						if(positive && match) this.addElement(outElements, eo);
						else if(!positive && !match) this.addElement(outElements, eo);
					}										
				}
				
				return outElements;
			}
			
			if((pn.isCamelized() || pn.isPascal()) && pn.getStart() != null){
				Wordnet wn = new Wordnet();				
				PhraseNamed start = pn.getStart();
				
				for(EObject eo : elements){	
					boolean first = true;
					String remain = null;
					if(eo instanceof MetaClass) remain = ((MetaClass)eo).getName();
					else if(eo instanceof Feature) remain = ((Feature)eo).getName();
					
					if(!positive && pn.isPascal() && !StringUtils.startsUpperCase(remain)) this.addElement(outElements, eo);
					else if(!positive && pn.isCamelized() && !StringUtils.startsLowerCase(remain)) this.addElement(outElements, eo);
					else {
						boolean match = true;
						
						for(NamePart np : start.getWords()){
							if(remain != null){
								String word;
								
								if(pn.isCamelized() && first){
									word = StringUtils.getFirstLowerCaseWord(remain);
									first = false;
								}else word = StringUtils.getFirstUpperCaseWord(remain);
								
								if(word != null){		
									if(np instanceof Literal){
										Literal l = (Literal)np;										
										if(!l.getLiteral().getValue().equals(word)) match = false;								
									}
									
									if(np instanceof Verb)
										if(!wn.isVerb(word)) 
											match = false;
									
									if(np instanceof Noun){
										Noun n = (Noun)np;
										
										if(!wn.isNoun(word)) match = false;
										else{
											if(n.getNum() != GrammaticalNumber.NULL){												
												if(n.getNum() == GrammaticalNumber.PLURAL)
													if(!LexicalInflector.isPlural(word)) match = false;
												
												if(n.getNum() == GrammaticalNumber.SINGULAR)
													if(!LexicalInflector.isSingular(word)) match = false;
											}
										}
									}
									
									if(np instanceof Adjective)
										if(!wn.isAdjective(word)) 
											match = false;
									
									if(np instanceof Synonym){
										Synonym s = (Synonym)np;
										if(word == null || word.length() <= 1 || s.getName().getValue() == null){
											match = false;							
										}
										else{
											if(!wn.areSynonyms(word, s.getName().getValue())) match = false;
										}
									}
																											
									remain = StringUtils.removeFirstWordToUpper(remain);	
								}else match = false;
							}else match = false;
							
							if(!match) remain = null;
						}
						
						if(positive && match){
							if(pn.getEnd() == null) this.addElement(outElements, eo);
							else{
								PhraseNamed end = (PhraseNamed)pn;
								remain = null;
								if(eo instanceof MetaClass) remain = ((MetaClass)eo).getName();
								else if(eo instanceof Feature) remain = ((Feature)eo).getName();
								
								ArrayList<String> newR = new ArrayList<String>();
								
								if(pn.isCamelized()){
									newR.add(StringUtils.getFirstLowerCaseWord(remain));
									remain = StringUtils.removeFirstWordToUpper(remain);
								}
								
								while(remain != null && !remain.equals("")){
									newR.add(StringUtils.getFirstUpperCaseWord(remain));
									remain = StringUtils.removeFirstWordToUpper(remain);
								}
								
								remain = "";
								
								for(int i = newR.size()-end.getWords().size(); i<newR.size(); i++) remain += newR.get(i);
								
								//if(!positive && !StringUtils.startsUpperCase(remain)) this.addElement(outElements, eo);
								//else {
								match = true;
								first = true;	
								
								for(NamePart np : end.getWords()){
									if(remain != null){										
										String word = null;
										
										if(pn.isCamelized() && first){
											word = StringUtils.getFirstLowerCaseWord(remain);
											first = false;											
										}					
										
										if(word == null) word = StringUtils.getFirstUpperCaseWord(remain);
										
										if(word != null){		
											if(np instanceof Literal){
												Literal l = (Literal)np;										
												if(!l.getLiteral().getValue().equals(word)) match = false;								
											}
											
											if(np instanceof Verb)
												if(!wn.isVerb(word)) 
													match = false;
											
											if(np instanceof Noun){
												Noun n = (Noun)np;
												
												if(!wn.isNoun(word)) match = false;
												else{
													if(n.getNum() != GrammaticalNumber.NULL){
														if(n.getNum() == GrammaticalNumber.PLURAL)
															if(!LexicalInflector.isPlural(word)) match = false;
														
														if(n.getNum() == GrammaticalNumber.SINGULAR)
															if(!LexicalInflector.isSingular(word)) match = false;
													}
												}
											}
											
											if(np instanceof Adjective)
												if(!wn.isAdjective(word)) 
													match = false;
											
											if(np instanceof Synonym){
												Synonym s = (Synonym)np;
												if(word == null || word.length() <= 1 || s.getName().getValue() == null){
													match = false;							
												}
												else{
													if(!wn.areSynonyms(word, s.getName().getValue())) match = false;
												}
											}
																													
											remain = StringUtils.removeFirstWordToUpper(remain);	
										}else match = false;
									}else match = false;
									
									if(!match) remain = null;
								}
									
								if(remain != null && remain.length() > 0) match = false;
									
								if(positive && match) this.addElement(outElements, eo);
								else if(!positive && !match) this.addElement(outElements, eo);
								//}
							}
						}
						else if(!positive && !match) this.addElement(outElements, eo);
					}										
				}
				
				return outElements;
			}
			
			if((pn.isCamelized() || pn.isPascal()) && pn.getEnd() != null){
				Wordnet wn = new Wordnet();				
				PhraseNamed end = pn.getEnd();

				for(EObject eo : elements){
					boolean first = true;
					String remain = null;
					if(eo instanceof MetaClass) remain = ((MetaClass)eo).getName();
					else if(eo instanceof Feature) remain = ((Feature)eo).getName();
					ArrayList<String> newR = new ArrayList<String>();
					
					if(pn.isCamelized()){
						newR.add(StringUtils.getFirstLowerCaseWord(remain));
						remain = StringUtils.removeFirstWordToUpper(remain);
					}
					
					while(remain != null && !remain.equals("")){
						newR.add(StringUtils.getFirstUpperCaseWord(remain));
						remain = StringUtils.removeFirstWordToUpper(remain);
					}
					
					remain = "";
					
					for(int i = newR.size()-end.getWords().size(); i<newR.size(); i++) remain += newR.get(i);
					if(!positive && !StringUtils.startsUpperCase(remain)) this.addElement(outElements, eo);
					else {
						boolean match = true;
						
						for(NamePart np : end.getWords()){
							if(remain != null){
								String word = null;
								
								if(pn.isCamelized() && first){
									word = StringUtils.getFirstLowerCaseWord(remain);
									first = false;											
								}					
								
								if(word == null) word = StringUtils.getFirstUpperCaseWord(remain);
								
								if(word != null){		
									if(np instanceof Literal){
										Literal l = (Literal)np;										
										if(!l.getLiteral().getValue().equals(word)) match = false;								
									}
									
									if(np instanceof Verb)
										if(!wn.isVerb(word)) 
											match = false;
									
									if(np instanceof Noun){
										Noun n = (Noun)np;
										
										if(!wn.isNoun(word)) match = false;
										else{
											if(n.getNum() != GrammaticalNumber.NULL){
												if(n.getNum() == GrammaticalNumber.PLURAL)
													if(!LexicalInflector.isPlural(word)) match = false;
												
												if(n.getNum() == GrammaticalNumber.SINGULAR){
													if(!LexicalInflector.isSingular(word)) match = false;
												}
											}
										}
									}
									
									if(np instanceof Adjective)
										if(!wn.isAdjective(word)) 
											match = false;
									
									if(np instanceof Synonym){
										Synonym s = (Synonym)np;
										if(word == null || word.length() <= 1 || s.getName().getValue() == null){
											match = false;							
										}
										else{
											if(!wn.areSynonyms(word, s.getName().getValue())) match = false;
										}
									}
																											
									remain = StringUtils.removeFirstWordToUpper(remain);	
								}else match = false;
							}else match = false;
							
							if(!match) remain = null;
						}
						
						if(remain != null && remain.length() > 0) match = false;
						
						if(positive && match) this.addElement(outElements, eo);
						else if(!positive && !match) this.addElement(outElements, eo);
					}
				}
				
				return outElements;
			}
			
			// name composition, provided that it is not camelized or pascal
			/* this algorithm still needs to be thought, and implemented */
			/*if(!pn.isCamelized() && pn.getWords() != null && !pn.getWords().isEmpty()){
				Wordnet wn = new Wordnet();
				
				for(EObject eo : elements){
					String remain = null;
					if(eo instanceof MetaClass) remain = ((MetaClass)eo).getName();
					else if(eo instanceof Feature) remain = ((Feature)eo).getName();
					
					List<String> combinations = new ArrayList<String>();
					String tarCombo = "";
					List<NamePart> words = new ArrayList<NamePart>();
					
					for(NamePart np : words){
						if(np instanceof Noun) tarCombo += "n";
						else if(np instanceof Verb) tarCombo += "v";
						else if(np instanceof Adjective) tarCombo += "a";
						else if(np instanceof Synonym) tarCombo += "s";
						else if(np instanceof TrivialString) tarCombo += "*";
					}
					
					boolean match = true;
										
					for(NamePart np : words){
						List<String> matchingWords = new ArrayList<String>();
						
						for(int i = 1; i<remain.length(); i++){
							if(np instanceof Noun){
								if(wn.isNoun(remain.substring(0, i))) matchingWords.add(remain.substring(0, i));
							}
							
							if(np instanceof Verb){
							
							}
							
							if(np instanceof Adjective){
								
							}
							
							if(np instanceof Synonym){

							}
							
							if(np instanceof TrivialString){

							}
						}
					}
				}
			}*/
		}
		
		if(eq instanceof Named){
			Named named = (Named)eq;
			
			if(named.getWordNature() != null){
				WordNature nature = named.getWordNature();
				
				if(nature instanceof Synonym){
					Synonym s = (Synonym) nature;
					
					if(s.getName() != null){
						Wordnet wn = new Wordnet();
						
						for(EObject eo : elements){
							String name = null;
							if(eo instanceof MetaClass) name = ((MetaClass)eo).getName();
							else if(eo instanceof Feature) name = ((Feature)eo).getName();
							if(positive && wn.areSynonyms(s.getName().getValue(), name)) this.addElement(outElements, eo);
							else if(!positive && !wn.areSynonyms(s.getName().getValue(), name)) this.addElement(outElements, eo);
						}
					}
					
					return outElements;
				}
				
				if(nature instanceof Noun){
					Wordnet wn = new Wordnet();
					Noun n = (Noun) nature;
						
					for(EObject eo : elements){
						String name = null;
						if(eo instanceof MetaClass) name = ((MetaClass)eo).getName();
						else if(eo instanceof Feature) name = ((Feature)eo).getName();
						if(positive && wn.isNoun(name)){

							if(n.getNum() == GrammaticalNumber.NULL) this.addElement(outElements, eo);		
							else{
								if(n.getNum() == GrammaticalNumber.PLURAL)
									if(LexicalInflector.isPlural(name))
										this.addElement(outElements, eo);
								
								if(n.getNum() == GrammaticalNumber.SINGULAR)
									if(LexicalInflector.isSingular(name))
										this.addElement(outElements, eo);
							}
						}
						
						if(!positive){
							if(!wn.isNoun(name)) this.addElement(outElements, eo);
							else{
								if(n.getNum() == GrammaticalNumber.PLURAL)
									if(!LexicalInflector.isPlural(name))
										this.addElement(outElements, eo);
								
								if(n.getNum() == GrammaticalNumber.SINGULAR)
									if(!LexicalInflector.isSingular(name))
										this.addElement(outElements, eo);
							}
						}
					}
					
					return outElements;
				}
				
				if(nature instanceof Verb){
					Wordnet wn = new Wordnet();
						
					for(EObject eo : elements){
						String name = null;
						if(eo instanceof MetaClass) name = ((MetaClass)eo).getName();
						else if(eo instanceof Feature) name = ((Feature)eo).getName();
						if(positive && wn.isVerb(name)) this.addElement(outElements, eo);		
						if(!positive && !wn.isVerb(name)) this.addElement(outElements, eo);
					}
					
					return outElements;
				}
				
				if(nature instanceof Adjective){
					Wordnet wn = new Wordnet();
						
					for(EObject eo : elements){
						String name = null;
						if(eo instanceof MetaClass) name = ((MetaClass)eo).getName();
						else if(eo instanceof Feature) name = ((Feature)eo).getName();
						if(positive && wn.isAdjective(name)) this.addElement(outElements, eo);		
						if(!positive && !wn.isAdjective(name)) this.addElement(outElements, eo);
					}
					
					return outElements;
				}
			}
			
			/* name start filter */
			if(named instanceof Prefix){
				/* substring prefix */
				if(named.getName() != null){
					String substring = named.getName().getValue().toLowerCase();
					
					for(EObject eo : elements){
						if(eo instanceof MetaClass){
							MetaClass mc = (MetaClass)eo;
							
							if(positive && mc.getName().toLowerCase().startsWith(substring)) this.addElement(outElements, mc);
							if(!positive && !mc.getName().toLowerCase().startsWith(substring)) this.addElement(outElements, mc);
							
						}else if(eo instanceof Feature){
							Feature f = (Feature)eo;
							
							if(positive && f.getName().toLowerCase().startsWith(substring)) this.addElement(outElements, f);							
							if(!positive && !f.getName().toLowerCase().startsWith(substring)) this.addElement(outElements, f);							
						}
					}
					
					return outElements;
				}
				
				/* lower/upper case prefix */
				if(named.getCase() != null){
					boolean upper = (named.getCase().getValue() == Case.UPPER_CASE_VALUE);
					
					for(EObject eo : elements){
						if(upper){
							if(eo instanceof MetaClass){
								MetaClass mc = (MetaClass)eo;
								String prefix = mc.getName().substring(0, 1);
								
								if(positive && prefix.toUpperCase().equals(prefix))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
								if(!positive && !prefix.toUpperCase().equals(prefix))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
							}else if(eo instanceof Feature){
								Feature f = (Feature)eo;
								String prefix = f.getName().substring(0, 1);
								
								if(positive && prefix.toUpperCase().equals(prefix))
									if(!outElements.contains(f))
										outElements.add(f);
								
								if(!positive && !prefix.toUpperCase().equals(prefix))
									if(!outElements.contains(f))
										outElements.add(f);
								
							}
						}else{
							/* lower */
							if(eo instanceof MetaClass){
								MetaClass mc = (MetaClass)eo;
								
								if(positive && mc.getName().toLowerCase().equals(mc.getName()))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
								if(!positive && !mc.getName().toLowerCase().equals(mc.getName()))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
							}else if(eo instanceof Feature){
								Feature f = (Feature)eo;
								
								if(positive && f.getName().toLowerCase().equals(f.getName()))
									if(!outElements.contains(f))
										outElements.add(f);
								
								if(!positive && !f.getName().toLowerCase().equals(f.getName()))
									if(!outElements.contains(f))
										outElements.add(f);
								
							}											
						}
					}
				
					return outElements;
				}
			}
			
			/* name ending filter */
			if(named instanceof Suffix){
				/* substring suffix */
				if(named.getName() != null){
					String substring = named.getName().getValue().toLowerCase();
					
					for(EObject eo : elements){
						if(eo instanceof MetaClass){
							MetaClass mc = (MetaClass)eo;
							
							if(positive && mc.getName().toLowerCase().endsWith(substring))
								if(!outElements.contains(mc))
									outElements.add(mc);
							
							if(!positive && !mc.getName().toLowerCase().endsWith(substring))
								if(!outElements.contains(mc))
									outElements.add(mc);
							
						}else if(eo instanceof Feature){
							Feature f = (Feature)eo;
							
							if(positive && f.getName().toLowerCase().endsWith(substring))
								if(!outElements.contains(f))
									outElements.add(f);
							
							if(!positive && !f.getName().toLowerCase().endsWith(substring))
								if(!outElements.contains(f))
									outElements.add(f);
							
						}
					}
					
					return outElements;
				}
				
				/* lower/upper case suffix */
				if(named.getCase() != null){
					boolean upper = (named.getCase().getValue() == Case.UPPER_CASE_VALUE);
					
					for(EObject eo : elements){
						if(upper){
							/* upper */
							if(eo instanceof MetaClass){
								MetaClass mc = (MetaClass)eo;
								String suffix = mc.getName().substring(mc.getName().length()-1, mc.getName().length());
								
								if(positive && suffix.toUpperCase().equals(suffix))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
								if(!positive && !suffix.toUpperCase().equals(suffix))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
							}else if(eo instanceof Feature){
								Feature f = (Feature)eo;
								String suffix = f.getName().substring(f.getName().length()-1, f.getName().length());
								
								if(positive && suffix.toUpperCase().equals(suffix))
									if(!outElements.contains(f))
										outElements.add(f);
								
								if(!positive && !suffix.toUpperCase().equals(suffix))
									if(!outElements.contains(f))
										outElements.add(f);
								
							}
						}else{
							/* lower */
							if(eo instanceof MetaClass){
								MetaClass mc = (MetaClass)eo;
								String suffix = mc.getName().substring(mc.getName().length()-1, mc.getName().length());
								
								if(positive && suffix.toLowerCase().equals(suffix))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
								if(!positive && !suffix.toLowerCase().equals(suffix))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
							}else if(eo instanceof Feature){
								Feature f = (Feature)eo;
								String suffix = f.getName().substring(f.getName().length()-1, f.getName().length());
								
								if(positive && suffix.toLowerCase().equals(suffix))
									if(!outElements.contains(f))
										outElements.add(f);
								
								if(!positive && !suffix.toLowerCase().equals(suffix))
									if(!outElements.contains(f))
										outElements.add(f);
								
							}											
						}
					}
				
					return outElements;
				}
			}
			
			/* name substring containment filter */
			if(named instanceof Contains){
				/* substring containment */
				if(named.getName() != null){
					String substring = named.getName().getValue().toLowerCase();
					
					for(EObject eo : elements){
						if(eo instanceof MetaClass){
							MetaClass mc = (MetaClass)eo;
							
							if(positive && mc.getName().toLowerCase().contains(substring))
								if(!outElements.contains(mc))
									outElements.add(mc);
							
							if(!positive && !mc.getName().toLowerCase().contains(substring))
								if(!outElements.contains(mc))
									outElements.add(mc);
							
						}else if(eo instanceof Feature){
							Feature f = (Feature)eo;
							
							if(positive && f.getName().toLowerCase().contains(substring))
								if(!outElements.contains(f))
									outElements.add(f);
							
							if(!positive && !f.getName().toLowerCase().contains(substring))
								if(!outElements.contains(f))
									outElements.add(f);
							
						}
					}
					
					return outElements;
				}
				
				/* lower/upper case substring containment */
				if(named.getCase() != null){
					boolean upper = (named.getCase().getValue() == Case.UPPER_CASE_VALUE);
					
					for(EObject eo : elements){
						if(upper){
							/* upper */
							if(eo instanceof MetaClass){
								MetaClass mc = (MetaClass)eo;
								
								if(positive && this.containsUppercase(mc.getName()))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
								if(!positive && !this.containsUppercase(mc.getName()))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
							}else if(eo instanceof Feature){
								Feature f = (Feature)eo;
								
								if(positive && this.containsUppercase(f.getName()))
									if(!outElements.contains(f))
										outElements.add(f);
								
								if(!positive && !this.containsUppercase(f.getName()))
									if(!outElements.contains(f))
										outElements.add(f);
								
							}
						}else{
							/* lower */
							if(eo instanceof MetaClass){
								MetaClass mc = (MetaClass)eo;
								
								if(positive && this.containsLowercase(mc.getName()))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
								if(!positive && !this.containsLowercase(mc.getName()))
									if(!outElements.contains(mc))
										outElements.add(mc);
								
							}else if(eo instanceof Feature){
								Feature f = (Feature)eo;
								
								if(positive && this.containsLowercase(f.getName()))
									if(!outElements.contains(f))
										outElements.add(f);
								
								if(!positive && !this.containsLowercase(f.getName()))
									if(!outElements.contains(f))
										outElements.add(f);
								
							}											
						}
					}
				
					return outElements;
				}
			}
			
			/* namesize */
			if(named.getSize() != null){
				int min = Integer.MIN_VALUE;
				int max = Integer.MAX_VALUE;
				if(named.getSize().getMin() != null) min = named.getSize().getMin().getValue();
				if(named.getSize().getMax() != null) max = named.getSize().getMax().getValue();
				
				for(EObject eo : elements){
					if(eo instanceof MetaClass){
						MetaClass mc = (MetaClass)eo;
							
						if(positive && mc.getName().length() >= min && mc.getName().length() <= max)
							if(!outElements.contains(mc))
								outElements.add(mc);
						
						if(!positive && (mc.getName().length() < min || mc.getName().length() > max))
							if(!outElements.contains(mc))
								outElements.add(mc);							
						
					}else if(eo instanceof Feature){
						Feature f = (Feature)eo;
						
						if(positive && f.getName().length() >= min && f.getName().length() <= max)
							if(!outElements.contains(f))
								outElements.add(f);
						
						if(!positive && (f.getName().length() < min || f.getName().length() > max))
							if(!outElements.contains(f))
								outElements.add(f);
							
					}
				}
				
				return outElements;
			}
			
			/* plain name comparation */
			if(named.getName() != null){
				for(EObject eo : elements){
					if(eo instanceof MetaClass){
						MetaClass mc = (MetaClass)eo;
						
						if(positive && named.getName().getValue().toLowerCase().equals(mc.getName().toLowerCase()))
							if(!outElements.contains(mc))
								outElements.add(mc);
						
						if(!positive && !named.getName().getValue().toLowerCase().equals(mc.getName().toLowerCase()))
							if(!outElements.contains(mc))
								outElements.add(mc);
						
					}else if(eo instanceof Feature){
						Feature f = (Feature)eo;
						
						if(positive && named.getName().getValue().toLowerCase().equals(f.getName().toLowerCase()))
							if(!outElements.contains(f))
								outElements.add(f);
						
						if(!positive && !named.getName().getValue().toLowerCase().equals(f.getName().toLowerCase()))
							if(!outElements.contains(f))
								outElements.add(f);
						
					}
				}
				
				return outElements;
			}
			
			if(named.getCase() != null){
				boolean upper = (named.getCase().getValue() == Case.UPPER_CASE_VALUE);
				
				for(EObject eo : elements){
					if(upper){
						if(eo instanceof MetaClass){
							MetaClass mc = (MetaClass)eo;
							
							if(positive && mc.getName().toUpperCase().equals(mc.getName()))
								if(!outElements.contains(mc))
									outElements.add(mc);
							
							if(!positive && !mc.getName().toUpperCase().equals(mc.getName()))
								if(!outElements.contains(mc))
									outElements.add(mc);
							
						}else if(eo instanceof Feature){
							Feature f = (Feature)eo;
							
							if(positive && f.getName().toUpperCase().equals(f.getName()))
								if(!outElements.contains(f))
									outElements.add(f);
							
							if(!positive && !f.getName().toUpperCase().equals(f.getName()))
								if(!outElements.contains(f))
									outElements.add(f);
							
						}
					}else{
						/* lower */
						if(eo instanceof MetaClass){
							MetaClass mc = (MetaClass)eo;
							
							if(positive && mc.getName().toLowerCase().equals(mc.getName()))
								if(!outElements.contains(mc))
									outElements.add(mc);
							
							if(!positive && !mc.getName().toLowerCase().equals(mc.getName()))
								if(!outElements.contains(mc))
									outElements.add(mc);
							
						}else if(eo instanceof Feature){
							Feature f = (Feature)eo;
							
							if(positive && f.getName().toLowerCase().equals(f.getName()))
								if(!outElements.contains(f))
									outElements.add(f);
							
							if(!positive && !f.getName().toLowerCase().equals(f.getName()))
								if(!outElements.contains(f))
									outElements.add(f);
							
						}											
					}
				}
			
				return outElements;
			}
		}
		
		if(eq instanceof Existance){
			if(positive) return outElements = elements;
			else{
				unexistance = true;
				outElements.clear();
				return outElements;
			}
		}
		
		if(eq instanceof Annotated){
			Annotated a = (Annotated)eq;
			if(a.getName() != null){
				for(EObject eo : elements){
					AnnotatedElement ae = (AnnotatedElement)eo;
					if(positive && ae.isAnnotated(a.getName().getValue())) this.addElement(outElements, ae);
					else if(!positive && !ae.isAnnotated(a.getName().getValue())) this.addElement(outElements, ae);
				}
			}else{
				for(EObject eo : elements){
					AnnotatedElement ae = (AnnotatedElement)eo;
					if(positive && !ae.getAnnotations().isEmpty()) this.addElement(outElements, ae);
					else if(!positive && ae.getAnnotations().isEmpty()) this.addElement(outElements, ae);
				}
			}
			
			return outElements;
		}
		
		if(eq instanceof AndQualifier){
			AndQualifier eand = (AndQualifier) eq;
			
			List<EObject> andOutElements = elements;
			
			for(Qualifier child : eand.getChildren()){
				outElements = filterElements(andOutElements, ((ElementQualifier)child));
				andOutElements = outElements;
			}
			
			return outElements;
		}
		
		if(eq instanceof OrQualifier){
			OrQualifier eor = (OrQualifier) eq;
			
			for(Qualifier child: eor.getChildren()){
				List<EObject> orOutElements = filterElements(elements, (ElementQualifier)child);
				
				for(EObject eo : orOutElements)
					if(!outElements.contains(eo))
						outElements.add(eo);
			}
			
			return outElements;
		}		
		
		return outElements;
	}
	
	public String getRationale(){
		return this.rationale;
	}
	
	private boolean containsUppercase(String string){
		for(int i=0; i<string.length(); i++){
			String substring = string.substring(i, i+1);
			if(substring.toUpperCase().equals(substring)) return true;
		}
		return false;
	}
	
	private boolean containsLowercase(String string){
		for(int i=0; i<string.length(); i++){
			String substring = string.substring(i, i+1);
			if(substring.toLowerCase().equals(substring)) return true;
		}
		return false;
	}
	
	public long getTime(){
		return this.runningTime;
	}
	
	public long getMemoryConsumption(){
		return this.memoryConsumption;
	}
	
	public String getOclEvaluation() {
		if(this.assertion.getOCLcomparison() != null){
			try {
				// install compiled shareable form of /org.eclipse.ocl.examples.library/model/OCL-2.4.oclstdlib
				org.eclipse.ocl.examples.pivot.model.OCLstdlib.install();
				
				// necessary for standalone applications
				EssentialOCLStandaloneSetup.doSetup();
				
				// load ocl environment
				OCL       ocl    = OCL.newInstance(new PivotEnvironmentFactory());
				OCLHelper helper = ocl.createOCLHelper();
				
				// register resource factory for ecore files
				Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
				
	            // -----------------------------------------------------------------------------
				// EXAMPLE a class is a leaf of containment
				// metaBest--: some class => cont-leaf
				// ocl-------: EClass.allInstances()->select(class | not class.eAllReferences->exists(reference | reference.containment))
				
				// load ecore to analyse
				if(metamodel.getSrcURI().endsWith("ecore")){
					URI uri = URI.createFileURI(new File(metamodel.getSrcURI()).getAbsolutePath());
					ResourceSet resourceSet = new ResourceSetImpl();
					Resource resource = resourceSet.getResource(uri, true);
					EPackage root;     
					
					if(resource.getContents().size() > 1){
						root = EcoreFactory.eINSTANCE.createEPackage();
						
						for(EObject ep : resource.getContents())
							root.getESubpackages().add((EPackage)ep);

					}else root = (EPackage)resource.getContents().get(0);
					
					//System.out.println("SUBPACKAGES: " + root.getESubpackages().size());
					
					// create and evaluate ocl query
					helper.setContext(EcorePackage.Literals.EPACKAGE);
					
					//System.out.println("OCL expression: " + this.assertion.getOCLcomparison().getExpression());
					
					ExpressionInOCL query = helper.createQuery(this.assertion.getOCLcomparison().getExpression());					
					
					Object op = ocl.evaluate(root, query);
					
					if(op instanceof Boolean) return op.toString();
					
					SetValue result = (SetValue) op;

					//System.out.println("VALID ELEMENTS");
					for (Object cl : result.asCollection()) System.out.println(((ENamedElement)cl).getName());
					return Boolean.toString(!result.isInvalid());
				}				
			} 
			catch (ParserException e) { return e.getMessage(); }
		}
		
		return "unevaluated";
	}
	
	public long getOclTime(){
		if(this.assertion.getOCLcomparison() != null){
			try {
				// install compiled shareable form of /org.eclipse.ocl.examples.library/model/OCL-2.4.oclstdlib
				org.eclipse.ocl.examples.pivot.model.OCLstdlib.install();
				
				// necessary for standalone applications
				EssentialOCLStandaloneSetup.doSetup();
				
				// load ocl environment
				OCL       ocl    = OCL.newInstance(new PivotEnvironmentFactory());
				OCLHelper helper = ocl.createOCLHelper();
				
				// register resource factory for ecore files
				Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
				
	            // -----------------------------------------------------------------------------
				// EXAMPLE a class is a leaf of containment
				// metaBest--: some class => cont-leaf
				// ocl-------: EClass.allInstances()->select(class | not class.eAllReferences->exists(reference | reference.containment))
				
				// load ecore to analyse
				if(metamodel.getSrcURI().endsWith("ecore")){
					URI uri = URI.createFileURI(new File(metamodel.getSrcURI()).getAbsolutePath());
					ResourceSet resourceSet = new ResourceSetImpl();
					Resource resource = resourceSet.getResource(uri, true);
					EPackage root     = (EPackage)resource.getContents().get(0);
					
					// create and evaluate ocl query
					helper.setContext(EcorePackage.Literals.EPACKAGE);
					ExpressionInOCL query = helper.createQuery(this.assertion.getOCLcomparison().getExpression());
					
					long startingTime = System.currentTimeMillis();
					
					Object op = ocl.evaluate(root, query);
					
					long time = System.currentTimeMillis() - startingTime;
					
					return time;
				}				
			} 
			catch (ParserException e) { e.printStackTrace(); } 
		}
		
		return -1;
	}
	
	public long getOclMemoryConsumption() throws org.eclipse.ocl.examples.pivot.ParserException{
		if(this.assertion.getOCLcomparison() != null){
			try {
				// install compiled shareable form of /org.eclipse.ocl.examples.library/model/OCL-2.4.oclstdlib
				org.eclipse.ocl.examples.pivot.model.OCLstdlib.install();
				
				// necessary for standalone applications
				EssentialOCLStandaloneSetup.doSetup();
				
				// load ocl environment
				OCL       ocl    = OCL.newInstance(new PivotEnvironmentFactory());
				OCLHelper helper = ocl.createOCLHelper();
				
				// register resource factory for ecore files
				Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
				
	            // -----------------------------------------------------------------------------
				// EXAMPLE a class is a leaf of containment
				// metaBest--: some class => cont-leaf
				// ocl-------: EClass.allInstances()->select(class | not class.eAllReferences->exists(reference | reference.containment))
				
				// load ecore to analyse
				if(metamodel.getSrcURI().endsWith("ecore")){
					URI uri = URI.createFileURI(new File(metamodel.getSrcURI()).getAbsolutePath());
					ResourceSet resourceSet = new ResourceSetImpl();
					Resource resource = resourceSet.getResource(uri, true);
					EPackage root     = (EPackage)resource.getContents().get(0);
					
					// create and evaluate ocl query
					helper.setContext(EcorePackage.Literals.EPACKAGE);
					ExpressionInOCL query = helper.createQuery(this.assertion.getOCLcomparison().getExpression());										
					
					Object op = ocl.evaluate(root, query);
					
					Runtime runtime = Runtime.getRuntime();
					runtime.gc();
					
					return (runtime.totalMemory() - runtime.freeMemory()) / (1024L * 1024L);
				}
				
			} 
			catch (ParserException e) { e.printStackTrace(); }
		}
		
		return -1;
	}
}
