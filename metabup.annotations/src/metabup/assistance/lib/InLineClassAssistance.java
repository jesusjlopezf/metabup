/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.lib;

import java.util.ArrayList;
import java.util.List;

import metabup.annotations.catalog.refactoring.Merge;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.general.AssistanceTip;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;

public class InLineClassAssistance extends AssistanceTip implements IAssistanceTip {
	public InLineClassAssistance(){
		super();
		super.setName("InLinable class detection");
		this.setAnnotation(new Merge());
	}
	
	public boolean calculateSolutions() {		
		for(MetaClass mc : this.getTargetMetaModel().getClasses()){
			for(Feature f : mc.getFeatures()){				
				if(f instanceof Reference){
					Reference r = (Reference)f;
					if(r.getReference() != null){
						if(r.getReference().getFeatures() != null							
							&& r.getReference().getSubclasses().isEmpty()
							&& r.getReference().getSupers().isEmpty()
							//&& r.getMin() == 1
							&& r.getMax() == 1
							&& !super.isTargetforAnyReference(r.getReference(), mc)){
								if(!r.getReference().getFeatures().isEmpty()){
									boolean hasAttributes = false;
									
									for(Feature reff : r.getReference().getFeatures()) 
										if(reff instanceof metabup.metamodel.Attribute) hasAttributes = true;																														
									
									boolean hasReferences = false;
									
									for(Feature reff : r.getReference().getFeatures()) 
										if(reff instanceof metabup.metamodel.Reference) hasReferences = true;
									
									if(hasAttributes && !hasReferences && !super.isTargetforAnyReference(r.getReference(), mc)){
										//System.out.println("The reference matches the tip");
										List<AnnotatedElement> set = new ArrayList<AnnotatedElement>();
										set.add(mc);
										set.add(r.getReference());
										super.putSolution("The reference " + r.getName() + " might be in-lined as a set of features within " + mc.getName() , set, new Merge());
									}
								}								
						}
					}				
				}
			}
		}
		
		if(super.solutions.isEmpty()) return false;
		else return true;
	}


	
}
