/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.impl;

import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fragments.FragmentsPackage;
import test.TestPackage;
import test.checks.AbstractTypeInstance;
import test.checks.Check;
import test.checks.ChecksFactory;
import test.checks.ChecksPackage;
import test.checks.Existence;
import test.checks.FeatureExistence;
import test.checks.FeatureNatureMismatch;
import test.checks.Mismatch;
import test.checks.MissingAttribute;
import test.checks.MissingReference;
import test.checks.MultiplicityMismatch;
import test.checks.RestrictionViolation;
import test.checks.TypeExistence;
import test.checks.TypeMismatch;
import test.checks.Uncontainment;
import test.checks.UncontainmentByMetaClass;
import test.checks.UncontainmentByObject;
import test.checks.expressions.ExpressionsPackage;
import test.checks.expressions.impl.ExpressionsPackageImpl;
import test.extensions.ExtensionsPackage;
import test.extensions.impl.ExtensionsPackageImpl;
import test.impl.TestPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ChecksPackageImpl extends EPackageImpl implements ChecksPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass checkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass existenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureExistenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeExistenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractTypeInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass missingAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mismatchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplicityMismatchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeMismatchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureNatureMismatchEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass restrictionViolationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uncontainmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uncontainmentByObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uncontainmentByMetaClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass missingReferenceEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see test.checks.ChecksPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ChecksPackageImpl() {
		super(eNS_URI, ChecksFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ChecksPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ChecksPackage init() {
		if (isInited) return (ChecksPackage)EPackage.Registry.INSTANCE.getEPackage(ChecksPackage.eNS_URI);

		// Obtain or create and register package
		ChecksPackageImpl theChecksPackage = (ChecksPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ChecksPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ChecksPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		FragmentsPackage.eINSTANCE.eClass();
		MetamodeltestPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		TestPackageImpl theTestPackage = (TestPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TestPackage.eNS_URI) instanceof TestPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TestPackage.eNS_URI) : TestPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) : ExpressionsPackage.eINSTANCE);
		ExtensionsPackageImpl theExtensionsPackage = (ExtensionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExtensionsPackage.eNS_URI) instanceof ExtensionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExtensionsPackage.eNS_URI) : ExtensionsPackage.eINSTANCE);

		// Create package meta-data objects
		theChecksPackage.createPackageContents();
		theTestPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theExtensionsPackage.createPackageContents();

		// Initialize created meta-data
		theChecksPackage.initializePackageContents();
		theTestPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theExtensionsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theChecksPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ChecksPackage.eNS_URI, theChecksPackage);
		return theChecksPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCheck() {
		return checkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExistence() {
		return existenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureExistence() {
		return featureExistenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureExistence_Feature() {
		return (EReference)featureExistenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeExistence() {
		return typeExistenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeExistence_Type() {
		return (EReference)typeExistenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractTypeInstance() {
		return abstractTypeInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractTypeInstance_Type() {
		return (EReference)abstractTypeInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMissingAttribute() {
		return missingAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMissingAttribute_Name() {
		return (EAttribute)missingAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMissingAttribute_Object() {
		return (EReference)missingAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMismatch() {
		return mismatchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplicityMismatch() {
		return multiplicityMismatchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMultiplicityMismatch_FeatureMultiplicity() {
		return (EReference)multiplicityMismatchEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeMismatch() {
		return typeMismatchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeMismatch_FeatureType() {
		return (EReference)typeMismatchEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureNatureMismatch() {
		return featureNatureMismatchEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureNatureMismatch_FeatureNature() {
		return (EReference)featureNatureMismatchEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRestrictionViolation() {
		return restrictionViolationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRestrictionViolation_FragmentElement() {
		return (EReference)restrictionViolationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRestrictionViolation_RestrictionName() {
		return (EAttribute)restrictionViolationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUncontainment() {
		return uncontainmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUncontainment_Object() {
		return (EReference)uncontainmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUncontainmentByObject() {
		return uncontainmentByObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUncontainmentByObject_Container() {
		return (EReference)uncontainmentByObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUncontainmentByMetaClass() {
		return uncontainmentByMetaClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUncontainmentByMetaClass_Metaclass() {
		return (EReference)uncontainmentByMetaClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMissingReference() {
		return missingReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMissingReference_RefName() {
		return (EAttribute)missingReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMissingReference_From() {
		return (EReference)missingReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMissingReference_To() {
		return (EReference)missingReferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChecksFactory getChecksFactory() {
		return (ChecksFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		checkEClass = createEClass(CHECK);

		existenceEClass = createEClass(EXISTENCE);

		featureExistenceEClass = createEClass(FEATURE_EXISTENCE);
		createEReference(featureExistenceEClass, FEATURE_EXISTENCE__FEATURE);

		typeExistenceEClass = createEClass(TYPE_EXISTENCE);
		createEReference(typeExistenceEClass, TYPE_EXISTENCE__TYPE);

		abstractTypeInstanceEClass = createEClass(ABSTRACT_TYPE_INSTANCE);
		createEReference(abstractTypeInstanceEClass, ABSTRACT_TYPE_INSTANCE__TYPE);

		missingAttributeEClass = createEClass(MISSING_ATTRIBUTE);
		createEAttribute(missingAttributeEClass, MISSING_ATTRIBUTE__NAME);
		createEReference(missingAttributeEClass, MISSING_ATTRIBUTE__OBJECT);

		mismatchEClass = createEClass(MISMATCH);

		multiplicityMismatchEClass = createEClass(MULTIPLICITY_MISMATCH);
		createEReference(multiplicityMismatchEClass, MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY);

		typeMismatchEClass = createEClass(TYPE_MISMATCH);
		createEReference(typeMismatchEClass, TYPE_MISMATCH__FEATURE_TYPE);

		featureNatureMismatchEClass = createEClass(FEATURE_NATURE_MISMATCH);
		createEReference(featureNatureMismatchEClass, FEATURE_NATURE_MISMATCH__FEATURE_NATURE);

		restrictionViolationEClass = createEClass(RESTRICTION_VIOLATION);
		createEReference(restrictionViolationEClass, RESTRICTION_VIOLATION__FRAGMENT_ELEMENT);
		createEAttribute(restrictionViolationEClass, RESTRICTION_VIOLATION__RESTRICTION_NAME);

		uncontainmentEClass = createEClass(UNCONTAINMENT);
		createEReference(uncontainmentEClass, UNCONTAINMENT__OBJECT);

		uncontainmentByObjectEClass = createEClass(UNCONTAINMENT_BY_OBJECT);
		createEReference(uncontainmentByObjectEClass, UNCONTAINMENT_BY_OBJECT__CONTAINER);

		uncontainmentByMetaClassEClass = createEClass(UNCONTAINMENT_BY_META_CLASS);
		createEReference(uncontainmentByMetaClassEClass, UNCONTAINMENT_BY_META_CLASS__METACLASS);

		missingReferenceEClass = createEClass(MISSING_REFERENCE);
		createEAttribute(missingReferenceEClass, MISSING_REFERENCE__REF_NAME);
		createEReference(missingReferenceEClass, MISSING_REFERENCE__FROM);
		createEReference(missingReferenceEClass, MISSING_REFERENCE__TO);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage)EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI);
		FragmentsPackage theFragmentsPackage = (FragmentsPackage)EPackage.Registry.INSTANCE.getEPackage(FragmentsPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theExpressionsPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		existenceEClass.getESuperTypes().add(this.getCheck());
		featureExistenceEClass.getESuperTypes().add(this.getExistence());
		typeExistenceEClass.getESuperTypes().add(this.getExistence());
		abstractTypeInstanceEClass.getESuperTypes().add(this.getCheck());
		missingAttributeEClass.getESuperTypes().add(this.getCheck());
		mismatchEClass.getESuperTypes().add(this.getCheck());
		multiplicityMismatchEClass.getESuperTypes().add(this.getMismatch());
		typeMismatchEClass.getESuperTypes().add(this.getMismatch());
		featureNatureMismatchEClass.getESuperTypes().add(this.getMismatch());
		restrictionViolationEClass.getESuperTypes().add(this.getCheck());
		uncontainmentEClass.getESuperTypes().add(this.getCheck());
		uncontainmentByObjectEClass.getESuperTypes().add(this.getUncontainment());
		uncontainmentByMetaClassEClass.getESuperTypes().add(this.getUncontainment());
		missingReferenceEClass.getESuperTypes().add(this.getCheck());

		// Initialize classes, features, and operations; add parameters
		initEClass(checkEClass, Check.class, "Check", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(existenceEClass, Existence.class, "Existence", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(featureExistenceEClass, FeatureExistence.class, "FeatureExistence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureExistence_Feature(), theExpressionsPackage.getFeatureWithinObjectExpression(), null, "feature", null, 1, 1, FeatureExistence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typeExistenceEClass, TypeExistence.class, "TypeExistence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTypeExistence_Type(), theExpressionsPackage.getObjectTypeExpression(), null, "type", null, 1, 1, TypeExistence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractTypeInstanceEClass, AbstractTypeInstance.class, "AbstractTypeInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractTypeInstance_Type(), theExpressionsPackage.getObjectTypeExpression(), null, "type", null, 1, 1, AbstractTypeInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(missingAttributeEClass, MissingAttribute.class, "MissingAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMissingAttribute_Name(), ecorePackage.getEString(), "name", null, 1, 1, MissingAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMissingAttribute_Object(), theFragmentsPackage.getObject(), null, "object", null, 1, 1, MissingAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mismatchEClass, Mismatch.class, "Mismatch", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(multiplicityMismatchEClass, MultiplicityMismatch.class, "MultiplicityMismatch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMultiplicityMismatch_FeatureMultiplicity(), theExpressionsPackage.getMultiplicityExpression(), null, "featureMultiplicity", null, 1, 1, MultiplicityMismatch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typeMismatchEClass, TypeMismatch.class, "TypeMismatch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTypeMismatch_FeatureType(), theExpressionsPackage.getFeatureTypeExpression(), null, "featureType", null, 1, 1, TypeMismatch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureNatureMismatchEClass, FeatureNatureMismatch.class, "FeatureNatureMismatch", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureNatureMismatch_FeatureNature(), theExpressionsPackage.getFeatureNatureExpression(), null, "featureNature", null, 1, 1, FeatureNatureMismatch.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(restrictionViolationEClass, RestrictionViolation.class, "RestrictionViolation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRestrictionViolation_FragmentElement(), theExpressionsPackage.getFragmentElementExpression(), null, "fragmentElement", null, 1, 1, RestrictionViolation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRestrictionViolation_RestrictionName(), ecorePackage.getEString(), "restrictionName", null, 1, 1, RestrictionViolation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uncontainmentEClass, Uncontainment.class, "Uncontainment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUncontainment_Object(), theExpressionsPackage.getObjectExpression(), null, "object", null, 1, 1, Uncontainment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uncontainmentByObjectEClass, UncontainmentByObject.class, "UncontainmentByObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUncontainmentByObject_Container(), theExpressionsPackage.getObjectExpression(), null, "container", null, 1, 1, UncontainmentByObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uncontainmentByMetaClassEClass, UncontainmentByMetaClass.class, "UncontainmentByMetaClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUncontainmentByMetaClass_Metaclass(), theExpressionsPackage.getMetaClassExpression(), null, "metaclass", null, 1, 1, UncontainmentByMetaClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(missingReferenceEClass, MissingReference.class, "MissingReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMissingReference_RefName(), ecorePackage.getEString(), "refName", null, 0, 1, MissingReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMissingReference_From(), theExpressionsPackage.getObjectOrMetaClassExpression(), null, "from", null, 0, 1, MissingReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMissingReference_To(), theExpressionsPackage.getObjectOrMetaClassExpression(), null, "to", null, 0, 1, MissingReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //ChecksPackageImpl
