/**
 */
package graphicProperties.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import graphicProperties.ArrowType;
import graphicProperties.EdgeProperties;
import graphicProperties.GraphicProperties;
import graphicProperties.GraphicPropertiesFactory;
import graphicProperties.GraphicPropertiesPackage;
import graphicProperties.LineType;
import graphicProperties.NodeProperties;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class GraphicPropertiesPackageImpl extends EPackageImpl implements GraphicPropertiesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass graphicPropertiesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodePropertiesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass edgePropertiesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum lineTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum arrowTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see graphicProperties.GraphicPropertiesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private GraphicPropertiesPackageImpl() {
		super(eNS_URI, GraphicPropertiesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link GraphicPropertiesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static GraphicPropertiesPackage init() {
		if (isInited) return (GraphicPropertiesPackage)EPackage.Registry.INSTANCE.getEPackage(GraphicPropertiesPackage.eNS_URI);

		// Obtain or create and register package
		GraphicPropertiesPackageImpl theGraphicPropertiesPackage = (GraphicPropertiesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof GraphicPropertiesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new GraphicPropertiesPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theGraphicPropertiesPackage.createPackageContents();

		// Initialize created meta-data
		theGraphicPropertiesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theGraphicPropertiesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(GraphicPropertiesPackage.eNS_URI, theGraphicPropertiesPackage);
		return theGraphicPropertiesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGraphicProperties() {
		return graphicPropertiesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNodeProperties() {
		return nodePropertiesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEdgeProperties() {
		return edgePropertiesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdgeProperties_Color() {
		return (EAttribute)edgePropertiesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdgeProperties_Width() {
		return (EAttribute)edgePropertiesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdgeProperties_LineType() {
		return (EAttribute)edgePropertiesEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdgeProperties_SrcArrow() {
		return (EAttribute)edgePropertiesEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEdgeProperties_TgtArrow() {
		return (EAttribute)edgePropertiesEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLineType() {
		return lineTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getArrowType() {
		return arrowTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GraphicPropertiesFactory getGraphicPropertiesFactory() {
		return (GraphicPropertiesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		graphicPropertiesEClass = createEClass(GRAPHIC_PROPERTIES);

		nodePropertiesEClass = createEClass(NODE_PROPERTIES);

		edgePropertiesEClass = createEClass(EDGE_PROPERTIES);
		createEAttribute(edgePropertiesEClass, EDGE_PROPERTIES__COLOR);
		createEAttribute(edgePropertiesEClass, EDGE_PROPERTIES__WIDTH);
		createEAttribute(edgePropertiesEClass, EDGE_PROPERTIES__LINE_TYPE);
		createEAttribute(edgePropertiesEClass, EDGE_PROPERTIES__SRC_ARROW);
		createEAttribute(edgePropertiesEClass, EDGE_PROPERTIES__TGT_ARROW);

		// Create enums
		lineTypeEEnum = createEEnum(LINE_TYPE);
		arrowTypeEEnum = createEEnum(ARROW_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		nodePropertiesEClass.getESuperTypes().add(this.getGraphicProperties());
		edgePropertiesEClass.getESuperTypes().add(this.getGraphicProperties());

		// Initialize classes, features, and operations; add parameters
		initEClass(graphicPropertiesEClass, GraphicProperties.class, "GraphicProperties", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nodePropertiesEClass, NodeProperties.class, "NodeProperties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(edgePropertiesEClass, EdgeProperties.class, "EdgeProperties", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEdgeProperties_Color(), ecorePackage.getEString(), "color", null, 0, 1, EdgeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEdgeProperties_Width(), ecorePackage.getEInt(), "width", null, 0, 1, EdgeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEdgeProperties_LineType(), this.getLineType(), "lineType", null, 0, 1, EdgeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEdgeProperties_SrcArrow(), this.getArrowType(), "srcArrow", null, 0, 1, EdgeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEdgeProperties_TgtArrow(), this.getArrowType(), "tgtArrow", null, 0, 1, EdgeProperties.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(lineTypeEEnum, LineType.class, "LineType");
		addEEnumLiteral(lineTypeEEnum, LineType.LINE);
		addEEnumLiteral(lineTypeEEnum, LineType.DASHED);
		addEEnumLiteral(lineTypeEEnum, LineType.DOTTED);
		addEEnumLiteral(lineTypeEEnum, LineType.DASHED_DOTTED);

		initEEnum(arrowTypeEEnum, ArrowType.class, "ArrowType");
		addEEnumLiteral(arrowTypeEEnum, ArrowType.NONE);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.STANDARD);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.DELTA);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.WHITE_DELTA);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.DIAMOND);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.WHITE_DIAMOND);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.SHORT);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.PLAIN);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.CONCAVE);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.CONVEX);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.CIRCLE);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.TRANSPARENT_CIRCLE);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.DASH);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.SKEWED_DASH);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.TSHAPE);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.CROWS_FOOT_ONE_MANDATORY);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.CROWS_FOOT_MANY_MANDATORY);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.CROWS_FOOT_ONE_OPTIONAL);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.CROWS_FOOT_MANY_OPTIONAL);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.CROWS_FOOT_ONE);
		addEEnumLiteral(arrowTypeEEnum, ArrowType.CROWS_FOOT_MANY);

		// Create resource
		createResource(eNS_URI);
	}

} //GraphicPropertiesPackageImpl
