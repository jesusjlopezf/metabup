/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.compilers.xtext;

import org.eclipse.jface.wizard.WizardDialog;

import metabup.annotations.IAnnotationFactory;
import metabup.metamodel.MetaModel;
import metabup.transformations.MetaModel2Ecore;
import metabup.transformations.models.WEcoreModel;
import metabup.transformations.models.WMetamodelModel;
import metabup.transformations.ui.editor.wizards.EcoreGenerationParamsWizard;

/**
 * Base class for compilers from implementation-agostic metamodels to Ecore.
 * 
 * @author jesus
 *
 */
public abstract class MetamodelCompiler {

	protected WMetamodelModel input;
	protected WEcoreModel ecore;
	protected IAnnotationFactory af;
	
	public MetamodelCompiler(WMetamodelModel input, WEcoreModel ecore, IAnnotationFactory af) {		
		this.input = input;
		this.ecore = ecore;
		this.af = af;
	}

	public void execute() {				
		MetaModel metamodel = input.rootMetamodel();
		EcoreGenerationParamsWizard wizard = new EcoreGenerationParamsWizard(metamodel.getName());
		WizardDialog dialog = new WizardDialog(null, wizard);
		
		if(dialog.open() == WizardDialog.OK){
			MetaModel2Ecore transformer = new MetaModel2Ecore(metamodel, af);
			ecore = transformer.execute(ecore, wizard.getName(), wizard.getPrefix(), wizard.getUri());
		}				
	}	

}
