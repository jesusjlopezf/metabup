/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.editors;

import java.util.Iterator;
import java.util.List;

import metabup.SessionManager;
import metabup.SessionState;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaModel;
import metabup.visualization.mm.MetamodelLabelProvider;
import metabup.visualization.mm.MetamodelVisualizer;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gef4.graph.Node;
import org.eclipse.gef4.zest.fx.ui.jface.ZestContentViewer;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.model.XtextDocument;
import org.eclipse.xtext.ui.editor.model.XtextDocumentProvider;

import com.google.inject.Inject;

/**
 * A multipage editor to edit fragments and show the inferred metamodel.
 * <ul>
 * <li>page 0 contains a nested text editor.
 * <li>page 1 shows a visualization of the inferred metamodel
 * </ul>
 */
public class MetamodelInteractiveEditor extends MultiPageEditorPart implements IResourceChangeListener{

    @Inject
    protected XtextEditor  xtextEditor;
    @Inject
    protected HistoryDocumentEditor  xtextHistoryViewer;
    
    
    protected SessionState session;
	private ZestContentViewer viewer;
	
    //private SprayGeneratorPropertiesForm generatorPropertiesForm;
    //@Inject
    //private TextEditor                   generatorPropertiesEditor;

    //@Inject
    //private IWorkspaceRoot               root;

	public MetamodelInteractiveEditor() {
		super();		
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
		if(this.getSession() != null) this.getSession().updateAssistance();
	}
	
	public XtextEditor getXtextEditor() {
		return xtextEditor;
	}
	
	public SessionState getSession() {
		return session;
	}
	
	public ZestContentViewer getViewer() {
		if(this.getSession() != null){
			viewer.refresh();
			this.getSession().updateAssistance();
		}
		return viewer;
	}
	
	void createPage0(SessionState state) {
        try {
        	FileEditorInput input = new FileEditorInput(state.getFragmentFile());
        	addPage(xtextEditor, input);
        	setPageText(0, "Shell");        	
        	// xtextAction.getAction() --> Maybe it could be used to retarget the action...
        	XtextDocumentProvider provider = (XtextDocumentProvider) xtextEditor.getDocumentProvider();
        	XtextDocument doc = (XtextDocument) provider.getDocument(input);
        	session.setDocument(doc);        	
        } catch (PartInitException e) {
			ErrorDialog.openError(getSite().getShell(), "Error creating nested text editor", null,e.getStatus());
        } 
	}

	
	void createPage1(SessionState state) {
    	/* Creating a different editor is just like this:
    	 * 
        //Injector injector = Guice.createInjector(Modules2.mixin(new MetamodelDSLRuntimeModule(), new metabup.ui.MetamodelDSLUiModule(Activator.getDefault()),
        	//	new org.eclipse.xtext.ui.shared.SharedStateModule()));
        myNewEditor = injector.getInstance(XtextEditor.class);
        */

		try {
        	FileEditorInput input = new FileEditorInput(state.getHistoryFile());        	
           
        	addPage(xtextHistoryViewer, input);
        	setPageText(1, "History");        	        	                	
        	XtextDocumentProvider provider = (XtextDocumentProvider)xtextHistoryViewer.getDocumentProvider();        	        
        	XtextDocument doc = (XtextDocument) provider.getDocument(input);

        	session.setHistoryDocument(doc);
        } catch (PartInitException e) {
			ErrorDialog.openError(getSite().getShell(), "Error creating nested text editor", null,e.getStatus());
        } catch (CoreException e) {
        	ErrorDialog.openError(getSite().getShell(), "Error creating mfrag file", null,e.getStatus());           
        }
	}		
	
	
	void createPage2(SessionState state) {
		Composite composite = new Composite(getContainer(), SWT.NONE);
		composite.setLayout(new FillLayout(SWT.VERTICAL));	

		viewer = MetamodelVisualizer.createViewer(composite, state.getMetamodelRoot());
		viewer.refresh();
		
		int index = addPage(composite);
		setPageText(index, "Metamodel");
	}
	
	void createPage3(SessionState state) {
		Composite composite = new Composite(getContainer(), SWT.NONE);
		composite.setLayout(new FillLayout(SWT.VERTICAL));	
		
		int index = addPage(composite);
		setPageText(index, "Edit");
	}
	
	
	
	public void refreshViewer(){
		Composite composite = new Composite(getContainer(), SWT.NONE);
		composite.setLayout(new FillLayout(SWT.VERTICAL));	
		viewer.setLabelProvider(new MetamodelLabelProvider(session.getMetamodelRoot(), null, null, null));
		viewer.refresh();
	}
	
	protected void createPages() {
		try {
			IFile mbupFile = (IFile) getEditorInput().getAdapter(IFile.class);
			session = SessionManager.INSTANCE.sessionFor(mbupFile);

			createPage0(session);
			//createPage1(session);	
			createPage2(session);
			createPage3(session);
			
		} catch (Exception e) {
			ErrorDialog.openError( getSite().getShell(), "Error creating pages",
					null, new WorkbenchException(e.getMessage(), e).getStatus());
		}		
	}
	
	/**
	 * The <code>MultiPageEditorPart</code> implementation of this 
	 * <code>IWorkbenchPart</code> method disposes all nested editors.
	 * Subclasses may extend.
	 */
	/*public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		super.dispose();
	}*/
	/**
	 * Saves the multi-page editor's document.
	 */
	/*
	public void doSave(IProgressMonitor monitor) {
		getEditor(0).doSave(monitor);
	}
	*/
	
    @Override
    public void doSave(IProgressMonitor monitor) {
        if(getActiveEditor() != null)getActiveEditor().doSave(monitor);
    	if(xtextEditor != null) xtextEditor.doSave(monitor);
    	if(xtextHistoryViewer != null) xtextHistoryViewer.doSave(monitor);
    }
	
	/**
	 * Saves the multi-page editor's document as another file.
	 * Also updates the text for page 0's tab, and updates this multi-page editor's input
	 * to correspond to the nested editor's.
	 */
	public void doSaveAs() {
		/*
		IEditorPart editor = getEditor(0);
		editor.doSaveAs();
		setPageText(0, editor.getTitle());
		setInput(editor.getEditorInput());
		*/
	}
	
	/* (non-Javadoc)
	 * Method declared on IEditorPart
	 */
	/*
	public void gotoMarker(IMarker marker) {
		setActivePage(0);
		IDE.gotoMarker(getEditor(0), marker);
	}
	*/
	
	/**
	 * The <code>MultiPageEditorExample</code> implementation of this method
	 * checks that the input is an instance of <code>IFileEditorInput</code>.
	 */
	public void init(IEditorSite site, IEditorInput editorInput)
		throws PartInitException {
		if (!(editorInput instanceof IFileEditorInput))
			throw new PartInitException("Invalid Input: Must be IFileEditorInput");
		super.init(site, editorInput);
	}
	
	/* (non-Javadoc)
	 * Method declared on IEditorPart.
	 */
	public boolean isSaveAsAllowed() {
		return false;
	}
	
	
	/**
	 * Calculates the contents of page 2 when the it is activated.
	 */
	protected void pageChange(int newPageIndex) {
		// //System.out.println("----> page change");
		super.pageChange(newPageIndex);
		if (newPageIndex == 2) {
			// sortWords();
		}
	}

	/**
	 * Adds a new page to visualize a compiled metamodel
	 */
	public void addCompiledMetamodel(String id) {
		Composite composite = new Composite(getContainer(), SWT.NONE);
		composite.setLayout(new FillLayout(SWT.VERTICAL));
		
		int index = addPage(composite);
		setPageText(index, id);

		viewer = MetamodelVisualizer.createViewer(composite, session.getDerivedMetamodel(id).rootMetamodel());	
	}

	
	public void addAssertionViewer(MetaModel mm, List<AnnotatedElement> right, List<AnnotatedElement> warnings, List<AnnotatedElement> errors){
		Composite composite = new Composite(getContainer(), SWT.NONE);
		composite.setLayout(new FillLayout(SWT.VERTICAL));
		
		//super.removePage(2);
		
		String id = "Matches";
		int index;
		
		this.removeAssertionViewer();	
		index = addPage(composite);
		setPageText(index, id);
		
		viewer = MetamodelVisualizer.createViewer(composite, mm, right, warnings, errors);	
		this.setActivePage(3);
		
		/*viewer.addDoubleClickListener(new IDoubleClickListener() {			
			@Override
			public void doubleClick(DoubleClickEvent event) {
				//System.out.println( event.getSource() );
			}
		});*/
	}
	
	public void removeAssertionViewer(){
		if(super.getPageCount() == 4) super.removePage(3);
	}

	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	public AnnotatedElement getSelection(){
		System.out.println("this is your selection: " + viewer.getSelection());
		
		AnnotatedElement element = parseGraph4NodeString(viewer.getSelection().toString(), session.getMetamodelRoot());
		return element;
		/*Iterator<?> selectedElements = ((IStructuredSelection)viewer.getSelection()).iterator();
        
		if (selectedElements.hasNext()) {
			// single object selection for now
			Object selectedElement = selectedElements.next();
			System.out.println("this is the class: " + selectedElement.getClass());
					
        }*/
	}
	
	private AnnotatedElement parseGraph4NodeString(String nodeStr, MetaModel metamodel){
		String mangoMC = "[Node {MetaClass : true, label : ";
		
		if(nodeStr.startsWith(mangoMC)){
			/* [Node {MetaClass : true, label : UseCase, node-label-css-style : -fx-fill: rgb(0,0,0);-fx-font-family: "Arial";-fx-font-size: 15pt;-fx-font-weight: bold;, node-rect-css-style : -fx-fill: rgb(192,192,192);-fx-stroke: rgb(0,0,0);, object : UseCase, tooltip : UseCase
			[]}] */
			
			String mcName = nodeStr.replace(mangoMC, "");
			mcName = mcName.substring(0, mcName.indexOf(","));
			
			return metamodel.getClassByName(mcName);
		}
		
		return null;
	}
}
