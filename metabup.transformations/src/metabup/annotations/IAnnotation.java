/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations;

import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import fragments.FragmentModel;
import metabup.transformations.models.WMetamodelModel;
import metabup.issues.general.IOpenIssue;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.AnnotationParam;
import metabup.metamodel.MetaModel;

public interface IAnnotation {
	boolean let(AnnotatedElement element, WMetamodelModel sourceMetaModel, MetaModel targetMetaModel);
	boolean when();
	MetaModel perform();

	public void run(boolean activeMode);
	
	void setElement(AnnotatedElement element);
	void setSourceMetaModel(WMetamodelModel sourceMetaModel);
	public void setTargetMetaModel(MetaModel targetMetaModel);
	MetaModel getTargetMetaModel();
	public List<IOpenIssue> detectFragmentIssues(FragmentModel fm, AnnotatedElement ae);

	public String getName();
	public boolean isPersistent();
	public HashMap<String, Object> getPreferences();
	public Object getPreferenceValue(String name);	
}
