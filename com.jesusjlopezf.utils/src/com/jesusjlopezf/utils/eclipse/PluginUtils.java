package com.jesusjlopezf.utils.eclipse;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.osgi.framework.Bundle;

public class PluginUtils {
public static ImageDescriptor getImageDescriptor(String pluginId, String iconPath){
		
		if(pluginId != null && iconPath != null){
			Bundle bdl = Platform.getBundle(pluginId);
			if(bdl == null) return null;
	        IPath path = new Path(iconPath);
	        URL url = FileLocator.find(bdl, path, null);
	        ImageDescriptor desc = ImageDescriptor.createFromURL(url);
	        return desc;
		}
		
		return null;
	}
}
