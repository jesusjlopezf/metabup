/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import fragments.FragmentsPackage;
import fragments.Reference;
import graphicProperties.EdgeProperties;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fragments.impl.ReferenceImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link fragments.impl.ReferenceImpl#getGraphicProperties <em>Graphic Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReferenceImpl extends FeatureImpl implements Reference {
	/**
	 * The cached value of the '{@link #getReference() <em>Reference</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected EList<fragments.Object> reference;
	/**
	 * The cached value of the '{@link #getGraphicProperties() <em>Graphic Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphicProperties()
	 * @generated
	 * @ordered
	 */
	protected graphicProperties.EdgeProperties graphicProperties;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FragmentsPackage.Literals.REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<fragments.Object> getReference() {
		if (reference == null) {
			reference = new EObjectResolvingEList<fragments.Object>(fragments.Object.class, this, FragmentsPackage.REFERENCE__REFERENCE);
		}
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public graphicProperties.EdgeProperties getGraphicProperties() {
		return graphicProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraphicProperties(graphicProperties.EdgeProperties newGraphicProperties, NotificationChain msgs) {
		graphicProperties.EdgeProperties oldGraphicProperties = graphicProperties;
		graphicProperties = newGraphicProperties;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FragmentsPackage.REFERENCE__GRAPHIC_PROPERTIES, oldGraphicProperties, newGraphicProperties);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphicProperties(graphicProperties.EdgeProperties newGraphicProperties) {
		if (newGraphicProperties != graphicProperties) {
			NotificationChain msgs = null;
			if (graphicProperties != null)
				msgs = ((InternalEObject)graphicProperties).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FragmentsPackage.REFERENCE__GRAPHIC_PROPERTIES, null, msgs);
			if (newGraphicProperties != null)
				msgs = ((InternalEObject)newGraphicProperties).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FragmentsPackage.REFERENCE__GRAPHIC_PROPERTIES, null, msgs);
			msgs = basicSetGraphicProperties(newGraphicProperties, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FragmentsPackage.REFERENCE__GRAPHIC_PROPERTIES, newGraphicProperties, newGraphicProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FragmentsPackage.REFERENCE__GRAPHIC_PROPERTIES:
				return basicSetGraphicProperties(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FragmentsPackage.REFERENCE__REFERENCE:
				return getReference();
			case FragmentsPackage.REFERENCE__GRAPHIC_PROPERTIES:
				return getGraphicProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FragmentsPackage.REFERENCE__REFERENCE:
				getReference().clear();
				getReference().addAll((Collection<? extends fragments.Object>)newValue);
				return;
			case FragmentsPackage.REFERENCE__GRAPHIC_PROPERTIES:
				setGraphicProperties((graphicProperties.EdgeProperties)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FragmentsPackage.REFERENCE__REFERENCE:
				getReference().clear();
				return;
			case FragmentsPackage.REFERENCE__GRAPHIC_PROPERTIES:
				setGraphicProperties((graphicProperties.EdgeProperties)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FragmentsPackage.REFERENCE__REFERENCE:
				return reference != null && !reference.isEmpty();
			case FragmentsPackage.REFERENCE__GRAPHIC_PROPERTIES:
				return graphicProperties != null;
		}
		return super.eIsSet(featureID);
	}
	
	/** 
	 * @return The references from a meta-model that target compatible-type meta-classes
	 * 
	 * */
	@Override
	public List<metabup.metamodel.Reference> getAnalogousReferences(MetaClass src) {
		if(src == null) return null;
		MetaModel mm = (MetaModel)src.eContainer();
		if(mm == null) return null;
				
		List<metabup.metamodel.Reference> matchingRefs = new ArrayList<metabup.metamodel.Reference>();		
		List<MetaClass> candidateTars = new ArrayList<MetaClass>();
		
		for(fragments.Object tarO : this.getReference()){
			MetaClass tarMC = mm.getClassByName(tarO.getType());
			if(tarMC == null) break;
			candidateTars.add(tarMC);
			if(tarMC.getAllSubs() != null) candidateTars.addAll(tarMC.getAllSubs());
		}
		
		for(metabup.metamodel.Reference mmRef : src.getReferences(true, metabup.metamodel.impl.ReferenceImpl.IRRELEVANT)){
			if(candidateTars.contains(mmRef.getReference())){
				matchingRefs.add(mmRef);
			}
		}
		
		
		return matchingRefs;
	}

} //ReferenceImpl
