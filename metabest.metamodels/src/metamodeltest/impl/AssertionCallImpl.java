/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import java.util.Collection;
import metamodeltest.AnnotatedElement;
import metamodeltest.Annotation;
import metamodeltest.AssertionCall;
import metamodeltest.MetamodeltestPackage;
import metamodeltest.OCLExpression;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assertion Call</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.AssertionCallImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link metamodeltest.impl.AssertionCallImpl#getOCLcomparison <em>OC Lcomparison</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class AssertionCallImpl extends DescribedElementImpl implements AssertionCall {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The cached value of the '{@link #getOCLcomparison() <em>OC Lcomparison</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOCLcomparison()
	 * @generated
	 * @ordered
	 */
	protected OCLExpression ocLcomparison;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertionCallImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.ASSERTION_CALL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, MetamodeltestPackage.ASSERTION_CALL__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OCLExpression getOCLcomparison() {
		return ocLcomparison;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOCLcomparison(OCLExpression newOCLcomparison, NotificationChain msgs) {
		OCLExpression oldOCLcomparison = ocLcomparison;
		ocLcomparison = newOCLcomparison;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ASSERTION_CALL__OC_LCOMPARISON, oldOCLcomparison, newOCLcomparison);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOCLcomparison(OCLExpression newOCLcomparison) {
		if (newOCLcomparison != ocLcomparison) {
			NotificationChain msgs = null;
			if (ocLcomparison != null)
				msgs = ((InternalEObject)ocLcomparison).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ASSERTION_CALL__OC_LCOMPARISON, null, msgs);
			if (newOCLcomparison != null)
				msgs = ((InternalEObject)newOCLcomparison).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ASSERTION_CALL__OC_LCOMPARISON, null, msgs);
			msgs = basicSetOCLcomparison(newOCLcomparison, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ASSERTION_CALL__OC_LCOMPARISON, newOCLcomparison, newOCLcomparison));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_CALL__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case MetamodeltestPackage.ASSERTION_CALL__OC_LCOMPARISON:
				return basicSetOCLcomparison(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_CALL__ANNOTATIONS:
				return getAnnotations();
			case MetamodeltestPackage.ASSERTION_CALL__OC_LCOMPARISON:
				return getOCLcomparison();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_CALL__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case MetamodeltestPackage.ASSERTION_CALL__OC_LCOMPARISON:
				setOCLcomparison((OCLExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_CALL__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case MetamodeltestPackage.ASSERTION_CALL__OC_LCOMPARISON:
				setOCLcomparison((OCLExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION_CALL__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case MetamodeltestPackage.ASSERTION_CALL__OC_LCOMPARISON:
				return ocLcomparison != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (derivedFeatureID) {
				case MetamodeltestPackage.ASSERTION_CALL__ANNOTATIONS: return MetamodeltestPackage.ANNOTATED_ELEMENT__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AnnotatedElement.class) {
			switch (baseFeatureID) {
				case MetamodeltestPackage.ANNOTATED_ELEMENT__ANNOTATIONS: return MetamodeltestPackage.ASSERTION_CALL__ANNOTATIONS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //AssertionCallImpl
