/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.jesusjlopezf.utils.structures.tree;

import java.util.ArrayList;

public class TreeParent extends TreeObject {
	protected ArrayList children;
	
	public TreeParent(Object o) {
		super(o);
		children = new ArrayList();
	}
	
	public void addChild(TreeObject child) {
		if(children.size() == 0) children.add((TreeParent)child);
		else{	
			boolean added = false;
			
			for(int i = 0; i<children.size(); i++){
				TreeObject maevto = (TreeObject) children.get(i);
				
				if(child != maevto){
					children.add(i, (TreeParent)child);
					i = children.size();
					added = true;
				}				
			}
				
			if(!added) children.add(child);
		}		
		
		child.setParent(this);
	}
	
	public void removeChild(TreeObject child) {
		children.remove(child);
		child.setParent(null);
	}
	
	public boolean hasChildren() {
		return children.size()>0;
	}
	
	public TreeParent [] getChildren() {
		return (TreeParent [])children.toArray(new TreeObject[children.size()]);
	}
}
