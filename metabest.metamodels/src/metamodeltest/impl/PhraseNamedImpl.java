/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import java.util.Collection;

import metamodeltest.MetamodeltestPackage;
import metamodeltest.NamePart;
import metamodeltest.PhraseNamed;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Phrase Named</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.PhraseNamedImpl#isCamelized <em>Camelized</em>}</li>
 *   <li>{@link metamodeltest.impl.PhraseNamedImpl#getWords <em>Words</em>}</li>
 *   <li>{@link metamodeltest.impl.PhraseNamedImpl#getStart <em>Start</em>}</li>
 *   <li>{@link metamodeltest.impl.PhraseNamedImpl#getEnd <em>End</em>}</li>
 *   <li>{@link metamodeltest.impl.PhraseNamedImpl#isPascal <em>Pascal</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PhraseNamedImpl extends ElementQualifierImpl implements PhraseNamed {
	/**
	 * The default value of the '{@link #isCamelized() <em>Camelized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCamelized()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CAMELIZED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCamelized() <em>Camelized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCamelized()
	 * @generated
	 * @ordered
	 */
	protected boolean camelized = CAMELIZED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getWords() <em>Words</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWords()
	 * @generated
	 * @ordered
	 */
	protected EList<NamePart> words;

	/**
	 * The cached value of the '{@link #getStart() <em>Start</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected PhraseNamed start;

	/**
	 * The cached value of the '{@link #getEnd() <em>End</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected PhraseNamed end;

	/**
	 * The default value of the '{@link #isPascal() <em>Pascal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPascal()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PASCAL_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPascal() <em>Pascal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPascal()
	 * @generated
	 * @ordered
	 */
	protected boolean pascal = PASCAL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PhraseNamedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.PHRASE_NAMED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCamelized() {
		return camelized;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCamelized(boolean newCamelized) {
		boolean oldCamelized = camelized;
		camelized = newCamelized;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PHRASE_NAMED__CAMELIZED, oldCamelized, camelized));
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<NamePart> getWords() {
		if (words == null) {
			words = new EObjectContainmentEList<NamePart>(NamePart.class, this, MetamodeltestPackage.PHRASE_NAMED__WORDS);
		}
		return words;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhraseNamed getStart() {
		return start;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStart(PhraseNamed newStart, NotificationChain msgs) {
		PhraseNamed oldStart = start;
		start = newStart;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PHRASE_NAMED__START, oldStart, newStart);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	public void setStart(PhraseNamed newStart) {
		if (newStart != start) {
			NotificationChain msgs = null;
			if (start != null)
				msgs = ((InternalEObject)start).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PHRASE_NAMED__START, null, msgs);
			if (newStart != null)
				msgs = ((InternalEObject)newStart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PHRASE_NAMED__START, null, msgs);
			msgs = basicSetStart(newStart, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PHRASE_NAMED__START, newStart, newStart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PhraseNamed getEnd() {
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnd(PhraseNamed newEnd, NotificationChain msgs) {
		PhraseNamed oldEnd = end;
		end = newEnd;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PHRASE_NAMED__END, oldEnd, newEnd);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnd(PhraseNamed newEnd) {
		if (newEnd != end) {
			NotificationChain msgs = null;
			if (end != null)
				msgs = ((InternalEObject)end).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PHRASE_NAMED__END, null, msgs);
			if (newEnd != null)
				msgs = ((InternalEObject)newEnd).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PHRASE_NAMED__END, null, msgs);
			msgs = basicSetEnd(newEnd, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PHRASE_NAMED__END, newEnd, newEnd));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPascal() {
		return pascal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPascal(boolean newPascal) {
		boolean oldPascal = pascal;
		pascal = newPascal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PHRASE_NAMED__PASCAL, oldPascal, pascal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.PHRASE_NAMED__WORDS:
				return ((InternalEList<?>)getWords()).basicRemove(otherEnd, msgs);
			case MetamodeltestPackage.PHRASE_NAMED__START:
				return basicSetStart(null, msgs);
			case MetamodeltestPackage.PHRASE_NAMED__END:
				return basicSetEnd(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.PHRASE_NAMED__CAMELIZED:
				return isCamelized();
			case MetamodeltestPackage.PHRASE_NAMED__WORDS:
				return getWords();
			case MetamodeltestPackage.PHRASE_NAMED__START:
				return getStart();
			case MetamodeltestPackage.PHRASE_NAMED__END:
				return getEnd();
			case MetamodeltestPackage.PHRASE_NAMED__PASCAL:
				return isPascal();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.PHRASE_NAMED__CAMELIZED:
				setCamelized((Boolean)newValue);
				return;
			case MetamodeltestPackage.PHRASE_NAMED__WORDS:
				getWords().clear();
				getWords().addAll((Collection<? extends NamePart>)newValue);
				return;
			case MetamodeltestPackage.PHRASE_NAMED__START:
				setStart((PhraseNamed)newValue);
				return;
			case MetamodeltestPackage.PHRASE_NAMED__END:
				setEnd((PhraseNamed)newValue);
				return;
			case MetamodeltestPackage.PHRASE_NAMED__PASCAL:
				setPascal((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PHRASE_NAMED__CAMELIZED:
				setCamelized(CAMELIZED_EDEFAULT);
				return;
			case MetamodeltestPackage.PHRASE_NAMED__WORDS:
				getWords().clear();
				return;
			case MetamodeltestPackage.PHRASE_NAMED__START:
				setStart((PhraseNamed)null);
				return;
			case MetamodeltestPackage.PHRASE_NAMED__END:
				setEnd((PhraseNamed)null);
				return;
			case MetamodeltestPackage.PHRASE_NAMED__PASCAL:
				setPascal(PASCAL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PHRASE_NAMED__CAMELIZED:
				return camelized != CAMELIZED_EDEFAULT;
			case MetamodeltestPackage.PHRASE_NAMED__WORDS:
				return words != null && !words.isEmpty();
			case MetamodeltestPackage.PHRASE_NAMED__START:
				return start != null;
			case MetamodeltestPackage.PHRASE_NAMED__END:
				return end != null;
			case MetamodeltestPackage.PHRASE_NAMED__PASCAL:
				return pascal != PASCAL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (camelized: ");
		result.append(camelized);
		result.append(", pascal: ");
		result.append(pascal);
		result.append(')');
		return result.toString();
	}

} //PhraseNamedImpl
