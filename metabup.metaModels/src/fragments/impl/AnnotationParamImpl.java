/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import fragments.AnnotationParam;
import fragments.FragmentsPackage;
import fragments.ParamValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Annotation Param</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fragments.impl.AnnotationParamImpl#getName <em>Name</em>}</li>
 *   <li>{@link fragments.impl.AnnotationParamImpl#getParamValue <em>Param Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnnotationParamImpl extends MinimalEObjectImpl.Container implements AnnotationParam {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParamValue() <em>Param Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParamValue()
	 * @generated
	 * @ordered
	 */
	protected ParamValue paramValue;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnotationParamImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FragmentsPackage.Literals.ANNOTATION_PARAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FragmentsPackage.ANNOTATION_PARAM__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParamValue getParamValue() {
		return paramValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParamValue(ParamValue newParamValue, NotificationChain msgs) {
		ParamValue oldParamValue = paramValue;
		paramValue = newParamValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FragmentsPackage.ANNOTATION_PARAM__PARAM_VALUE, oldParamValue, newParamValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParamValue(ParamValue newParamValue) {
		if (newParamValue != paramValue) {
			NotificationChain msgs = null;
			if (paramValue != null)
				msgs = ((InternalEObject)paramValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FragmentsPackage.ANNOTATION_PARAM__PARAM_VALUE, null, msgs);
			if (newParamValue != null)
				msgs = ((InternalEObject)newParamValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FragmentsPackage.ANNOTATION_PARAM__PARAM_VALUE, null, msgs);
			msgs = basicSetParamValue(newParamValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FragmentsPackage.ANNOTATION_PARAM__PARAM_VALUE, newParamValue, newParamValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FragmentsPackage.ANNOTATION_PARAM__PARAM_VALUE:
				return basicSetParamValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FragmentsPackage.ANNOTATION_PARAM__NAME:
				return getName();
			case FragmentsPackage.ANNOTATION_PARAM__PARAM_VALUE:
				return getParamValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FragmentsPackage.ANNOTATION_PARAM__NAME:
				setName((String)newValue);
				return;
			case FragmentsPackage.ANNOTATION_PARAM__PARAM_VALUE:
				setParamValue((ParamValue)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FragmentsPackage.ANNOTATION_PARAM__NAME:
				setName(NAME_EDEFAULT);
				return;
			case FragmentsPackage.ANNOTATION_PARAM__PARAM_VALUE:
				setParamValue((ParamValue)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FragmentsPackage.ANNOTATION_PARAM__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case FragmentsPackage.ANNOTATION_PARAM__PARAM_VALUE:
				return paramValue != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //AnnotationParamImpl
