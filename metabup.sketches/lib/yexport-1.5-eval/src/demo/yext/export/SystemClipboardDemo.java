/****************************************************************************
 * This demo file is part of the yFiles extension package yExport-1.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * yExport demo files exhibit yExport functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a yExport version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own yExport powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the yExport
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.export;


import yext.export.datatransfer.VisualTransferable;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.event.ActionEvent;
import java.net.URL;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import y.view.AbstractSelectionBoxMode;
import y.view.Drawable;
import y.view.Graph2D;
import y.view.Graph2DView;


/**
 * Demonstrates how to copy a graph in EMF format to the system clipboard.
 * Copying to the system clipboard is enabled by creating a to-be-copied
 * selection using a custom view mode.
 * @see <a href="http://docs.yworks.com/yfiles/doc/developers-guide/export_usage.html#export_clipboard">Using the yExport Extension Package</a> in the yFiles for Java Developer's Guide
 */
public class SystemClipboardDemo extends AbstractExportDemo
{
  private final CopySelectionMarker copySelectionMarker;
  private CopyToClipboardAction copyToClipboardAction;

  public SystemClipboardDemo() {
    copySelectionMarker = new CopySelectionMarker();
    if (copyToClipboardAction == null) {
      copyToClipboardAction = new CopyToClipboardAction();
    }
    view.addDrawable(copySelectionMarker);
  }

  /**
   * Creates a toolbar for this demo.
   */
  protected JToolBar createToolBar() {
    JToolBar jtb = super.createToolBar();
    jtb.addSeparator();
    jtb.add(new JToggleButton(new ToggleSelectionModeAction()));
    if (copyToClipboardAction == null) {
      copyToClipboardAction = new CopyToClipboardAction();
    }
    jtb.add(new JButton(copyToClipboardAction));
    return jtb;
  }

  protected void loadGraph(URL resource, boolean updateView) {
    setCopySelectionBounds(new Rectangle());
    super.loadGraph(resource, updateView);
  }

  private void setCopySelectionBounds( final Rectangle r ) {
    copySelectionMarker.setBounds(r);
    copyToClipboardAction.setEnabled(!copySelectionMarker.isEmpty());
  }


  /**
   * Action that copies a graph in EMF format to the system clipboard.
   */
  class CopyToClipboardAction extends AbstractAction {
    CopyToClipboardAction() {
      final String name = "Copy To Clipboard";
      putValue(Action.NAME, name);
      putValue(Action.SMALL_ICON, createIcon("resource/copy.png"));
      putValue(Action.SHORT_DESCRIPTION, name);
      setEnabled(false);
    }

    public void actionPerformed( final ActionEvent e ) {
      // setting up a Graph2DView for data transfer
      final Rectangle cpSlctnBnds = copySelectionMarker.getBounds();
      final Graph2D graph = (Graph2D) view.getGraph2D().createCopy();
      final double z = view.getZoom();
      final Dimension s = new Dimension(
              (int)(cpSlctnBnds.width * z),
              (int)(cpSlctnBnds.height * z));
      final Graph2DView transferView = new Graph2DView(graph);
      transferView.setZoom(z);
      transferView.setPaintDetailThreshold(0.0);
      transferView.setSize(s);
      transferView.setPreferredSize(s);
      transferView.setViewPoint(cpSlctnBnds.x, cpSlctnBnds.y);
      transferView.setWorldRect(cpSlctnBnds.x, cpSlctnBnds.y,
                                cpSlctnBnds.width, cpSlctnBnds.height);

      // copy the contents of the previously set up view to the system clipboard
      // note: the transferable is configured to provide some image format based
      // fallback data flavors that will allow EMF unaware applications to
      // import our data as image content
      VisualTransferable transferable = new VisualTransferable(transferView, true);
      Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
      cb.setContents(transferable, transferable);
    }
  }

  /**
   * Action that switches between regular graph edit mode and a mode to create
   * a selection to be copied to the system clipboard.
   */
  class ToggleSelectionModeAction extends AbstractAction {
    private final CreateCopySelectionMode mode;
    private boolean active;

    ToggleSelectionModeAction() {
      final String name = "Select Copy Region";
      putValue(Action.NAME, name);
      putValue(Action.SMALL_ICON, createIcon("resource/SelectRectangle16.gif"));
      putValue(Action.SHORT_DESCRIPTION, name);
      mode = new CreateCopySelectionMode();
      active = false;
    }

    public void actionPerformed( final ActionEvent e ) {
      if(!active) {
        view.removeViewMode(editMode);
        view.addViewMode(mode);
        active = true;
      } else {
        setCopySelectionBounds(new Rectangle());
        view.removeViewMode(mode);
        view.addViewMode(editMode);
        active = false;
      }
    }
  }

  /**
   * An <code>AbstractSelectionBoxMode</code> that creates a rectangular
   * selection to be copied to the system clipboard.
   */
  class CreateCopySelectionMode extends AbstractSelectionBoxMode {
    protected void selectionBoxAction(
            final Rectangle sb, final boolean shiftMode
    ) {
      setCopySelectionBounds(sb);
      view.updateView();
    }
  }

  /**
   * A drawable implementation that provides a shaded overlay for all of the
   * currently visible portion of a <code>Graph2DView</code> but its current
   * <code>bounds</code>.
   */
  static final class CopySelectionMarker implements Drawable {
    private final Rectangle bounds;
    private final Rectangle clip;

    private final Color fill;
    private final Color draw;

    CopySelectionMarker() {
      this.bounds = new Rectangle(0, 0, -1, -1);
      this.clip = new Rectangle();
      this.fill = Color.LIGHT_GRAY;
      this.draw = Color.DARK_GRAY;
    }

    public void paint( final Graphics2D gfx ) {
      if (isEmpty()) {
        return;
      }

      final Composite oldComp = gfx.getComposite();
      final Color oldColor = gfx.getColor();

      gfx.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.35f));

      gfx.getClipBounds(clip);
      gfx.setColor(fill);
      // top
      if (clip.y < bounds.y) {
        gfx.fillRect(clip.x, clip.y, clip.width, bounds.y - clip.y);
      }
      // left
      if (clip.x < bounds.x) {
        gfx.fillRect(clip.x, bounds.y, bounds.x - clip.x, bounds.height);
      }
      // bottom
      final int bndsMxY = bounds.y + bounds.height;
      final int cbMxY = clip.y + clip.height;
      if (cbMxY > bndsMxY) {
        gfx.fillRect(clip.x, bndsMxY, clip.width, cbMxY - bndsMxY);
      }
      // right
      final int bndsMxX = bounds.x + bounds.width;
      final int cbMxX = clip.x + clip.width;
      if (cbMxX > bndsMxX) {
        gfx.fillRect(bndsMxX, bounds.y, cbMxX - bndsMxX, bounds.height);
      }

      gfx.setColor(draw);
      gfx.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);

      gfx.setColor(oldColor);
      gfx.setComposite(oldComp);
    }

    public Rectangle getBounds() {
      return bounds.getBounds();
    }

    void setBounds( final Rectangle r ) {
      if (r == null) {
        throw new IllegalArgumentException("null");
      }
      bounds.setBounds(r);
    }

    boolean isEmpty() {
      return bounds.width < 1 || bounds.height < 1;
    }
  }

  /**
   * Launches this demo.
   */
  public static void main(String[] args) {
    initLnF();
    (new SystemClipboardDemo()).start();
  }
}