/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.expressions;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see test.checks.expressions.ExpressionsPackage
 * @generated
 */
public interface ExpressionsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExpressionsFactory eINSTANCE = test.checks.expressions.impl.ExpressionsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Object Type Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object Type Expression</em>'.
	 * @generated
	 */
	ObjectTypeExpression createObjectTypeExpression();

	/**
	 * Returns a new object of class '<em>Feature Type Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Type Expression</em>'.
	 * @generated
	 */
	FeatureTypeExpression createFeatureTypeExpression();

	/**
	 * Returns a new object of class '<em>Multiplicity Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiplicity Expression</em>'.
	 * @generated
	 */
	MultiplicityExpression createMultiplicityExpression();

	/**
	 * Returns a new object of class '<em>Feature Nature Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Nature Expression</em>'.
	 * @generated
	 */
	FeatureNatureExpression createFeatureNatureExpression();

	/**
	 * Returns a new object of class '<em>Feature Within Object Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Within Object Expression</em>'.
	 * @generated
	 */
	FeatureWithinObjectExpression createFeatureWithinObjectExpression();

	/**
	 * Returns a new object of class '<em>Object Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Object Expression</em>'.
	 * @generated
	 */
	ObjectExpression createObjectExpression();

	/**
	 * Returns a new object of class '<em>Meta Class Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Meta Class Expression</em>'.
	 * @generated
	 */
	MetaClassExpression createMetaClassExpression();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ExpressionsPackage getExpressionsPackage();

} //ExpressionsFactory
