/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.expressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.expressions.ObjectExpression#getObject <em>Object</em>}</li>
 * </ul>
 *
 * @see test.checks.expressions.ExpressionsPackage#getObjectExpression()
 * @model
 * @generated
 */
public interface ObjectExpression extends FragmentElementExpression, ObjectOrMetaClassExpression {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' reference.
	 * @see #setObject(fragments.Object)
	 * @see test.checks.expressions.ExpressionsPackage#getObjectExpression_Object()
	 * @model required="true"
	 * @generated
	 */
	fragments.Object getObject();

	/**
	 * Sets the value of the '{@link test.checks.expressions.ObjectExpression#getObject <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(fragments.Object value);

} // ObjectExpression
