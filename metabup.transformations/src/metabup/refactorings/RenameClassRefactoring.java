/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.refactorings;

import metabup.metamodel.MetaClass;
import metabup.transformations.models.WMetamodelModel;

public class RenameClassRefactoring extends MetamodelRefactoring {
	
	private MetaClass superclass;
	private String newName;

	public RenameClassRefactoring(WMetamodelModel model, MetaClass superclass, String newName) {
		super(model);
		this.superclass = superclass;
		this.newName    = newName;
	}

	@Override
	public boolean execute() {
		superclass.setName(newName);
		return true;
	}
	
}
