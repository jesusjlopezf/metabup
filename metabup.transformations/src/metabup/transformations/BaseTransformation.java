/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

public abstract class BaseTransformation {
	private List<String> errors = new ArrayList<String>();
	private List<String> warnings = new ArrayList<String>();
	
	class ObjReference {
		private EObject owner;
		private String  reference;
		
		public ObjReference(EObject o, String r) {
			this.owner = o;
			this.reference = r;
		}
		
		public EObject getObject() {
			return this.owner;
		}
		
		public String getReference() {
			return this.reference;
		}
	}
	
	protected HashMap<EObject, EObject> trace = new HashMap<EObject, EObject>();				// For the moment a 1-to-1, "untyped" trace
	protected HashMap<EObject, ObjReference> pending = new HashMap<EObject, ObjReference>();	// Pending references...

	protected HashMap<EObject, AContinuation<?>> pendingContinuation = new HashMap<EObject, AContinuation<?>>();	// Pending continuations
		
	protected void setTrace(EObject src, EObject tar) {
		this.trace.put(src, tar);
		if (this.pending.containsKey(src)) {	// now we can resolve the pending things...
			ObjReference or = this.pending.get(src);
			this.assignRef(or.getObject(), or.getReference(), tar);
			//System.out.println("Resolved trace: "+src);
		}
		
		if ( this.pendingContinuation.containsKey(src) ) {
			AContinuation<?> c = this.pendingContinuation.get(src);
			c.executeObj( tar );
			//System.out.println("Resumed continuation for " + src);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected <T> void registerContinuationForEquivalent(EObject src, AContinuation<T> continuation) {
		EObject tar = null;
		if ( (tar=this.getTraceOf(src))!=null ) 
			continuation.execute( (T) tar );
		else
			this.pendingContinuation.put(src, continuation);
	}
	
	private void assignRef(EObject owner, String refName, EObject tar) {	// This method does not belong to this class!!
		List<EReference> refs = owner.eClass().getEAllReferences();
		for (EReference e: refs ) 
			if (e.getName().equals(refName)) {
				owner.eSet(e, tar);
				return;
			}
		// TODO: Something's wrong here... throw exception...
	}
	
	protected EObject getTraceOf(EObject src) {
		return this.trace.get(src);
	}
	
	protected EObject assignEquivalent(EObject owner, String refName, EObject src) {		// what we really need here is a "continuation" ;-)
		EObject tar = null;
		if ((tar=this.getTraceOf(src))!=null) 
			this.assignRef(owner, refName, tar);
		else
			this.pending.put(src, new ObjReference(owner, refName));
		return tar;		
	}	

	protected Collection<? extends EObject> transform(Collection<? extends EObject> source) {
		LinkedList<EObject> result = new LinkedList<EObject>();
		for (EObject eObject : source) {
			try {
				Method m = findMethod("transform" + eObject.eClass().getName());
				// Method m = this.getClass().getMethod("transform" + eObject.eClass().getName(), EObject.class);
				Object r = m.invoke(this, eObject);
				if ( r == null ) throw new TransformationException("Rule returned null");
				result.add((EObject) r);
			} catch (Exception e) {
				throw new TransformationException(e);
			}	
		}
		return result;
	}

	// TODO: Factorize!
	protected Collection<? extends EObject> transform2(Collection<? extends EObject> source, EObject param) {
		LinkedList<EObject> result = new LinkedList<EObject>();
		
		for (EObject eObject : source) {
			try {
				Method m = findMethod("transform" + eObject.eClass().getName());
				// Method m = this.getClass().getMethod("transform" + eObject.eClass().getName(), EObject.class);
				Object r = m.invoke(this, eObject, param);
				if ( r == null ) 
					continue;
				result.add((EObject) r);
			} catch (Exception e) {
				throw new TransformationException(e);
			}	
		}
		
		return result;
	}
	
	private Method findMethod(String name) {
		Method[] methods = this.getClass().getMethods();
		for (Method method : methods) {
			if ( method.getName().equals(name) ) return method;
		}
		throw new NoSuchMethodError("No method " + name + " defined");
	}
	
	protected void addError(String error){
		this.errors.add(error);
	}
	
	protected void addWarning(String warning){
		this.warnings.add(warning);
	}
	
	public List<String> getErrors(){
		if(this.errors.isEmpty()) return null;
		return this.errors;
	}
	
	public List<String> getWarnings(){
		if(this.warnings.isEmpty()) return null;
		return this.warnings;
	}
	
	protected class TransformationException extends RuntimeException {
		private static final long serialVersionUID = 8269761925124986798L;

		public TransformationException(Exception e) {
			super(e);
		}

		public TransformationException(String msg) {
			super(msg);
		}	
	}

}
