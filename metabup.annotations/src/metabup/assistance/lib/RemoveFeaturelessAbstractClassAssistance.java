/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.lib;

import java.util.ArrayList;
import java.util.List;

import metabup.annotations.catalog.refactoring.Remove;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.general.AssistanceTip;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaClass;

public class RemoveFeaturelessAbstractClassAssistance extends AssistanceTip implements IAssistanceTip {
	public RemoveFeaturelessAbstractClassAssistance(){
		super();
		super.setName("Remove featureless abstract class");
		this.setAnnotation(new Remove());
	}
	
	public boolean calculateSolutions() {		
		for(MetaClass mc : this.getTargetMetaModel().getClasses()){
			List<AnnotatedElement> set = new ArrayList<AnnotatedElement>();
			
			if( mc.getIsAbstract()
				&& (mc.getFeatures() == null || mc.getFeatures().isEmpty())
				&& !super.isTargetforAnyReference(mc, null)){
						set.add(mc);
						super.putSolution(mc.getName() + " might be removed, since it's abstract and featureless." , set, new Remove());
			}								
		}
		
		if(super.solutions.isEmpty()) return false;
		else return true;
	}	
}
