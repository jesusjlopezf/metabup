/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import test.checks.ChecksPackage;
import test.checks.FeatureNatureMismatch;

import test.checks.expressions.FeatureNatureExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Nature Mismatch</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link test.checks.impl.FeatureNatureMismatchImpl#getFeatureNature <em>Feature Nature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureNatureMismatchImpl extends MismatchImpl implements FeatureNatureMismatch {
	/**
	 * The cached value of the '{@link #getFeatureNature() <em>Feature Nature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureNature()
	 * @generated
	 * @ordered
	 */
	protected FeatureNatureExpression featureNature;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureNatureMismatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChecksPackage.Literals.FEATURE_NATURE_MISMATCH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureNatureExpression getFeatureNature() {
		return featureNature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureNature(FeatureNatureExpression newFeatureNature, NotificationChain msgs) {
		FeatureNatureExpression oldFeatureNature = featureNature;
		featureNature = newFeatureNature;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ChecksPackage.FEATURE_NATURE_MISMATCH__FEATURE_NATURE, oldFeatureNature, newFeatureNature);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureNature(FeatureNatureExpression newFeatureNature) {
		if (newFeatureNature != featureNature) {
			NotificationChain msgs = null;
			if (featureNature != null)
				msgs = ((InternalEObject)featureNature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.FEATURE_NATURE_MISMATCH__FEATURE_NATURE, null, msgs);
			if (newFeatureNature != null)
				msgs = ((InternalEObject)newFeatureNature).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.FEATURE_NATURE_MISMATCH__FEATURE_NATURE, null, msgs);
			msgs = basicSetFeatureNature(newFeatureNature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChecksPackage.FEATURE_NATURE_MISMATCH__FEATURE_NATURE, newFeatureNature, newFeatureNature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ChecksPackage.FEATURE_NATURE_MISMATCH__FEATURE_NATURE:
				return basicSetFeatureNature(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChecksPackage.FEATURE_NATURE_MISMATCH__FEATURE_NATURE:
				return getFeatureNature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChecksPackage.FEATURE_NATURE_MISMATCH__FEATURE_NATURE:
				setFeatureNature((FeatureNatureExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChecksPackage.FEATURE_NATURE_MISMATCH__FEATURE_NATURE:
				setFeatureNature((FeatureNatureExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChecksPackage.FEATURE_NATURE_MISMATCH__FEATURE_NATURE:
				return featureNature != null;
		}
		return super.eIsSet(featureID);
	}

} //FeatureNatureMismatchImpl
