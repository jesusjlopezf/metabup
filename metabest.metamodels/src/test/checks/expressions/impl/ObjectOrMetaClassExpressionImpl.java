/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.expressions.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import test.checks.expressions.ExpressionsPackage;
import test.checks.expressions.ObjectOrMetaClassExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Or Meta Class Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ObjectOrMetaClassExpressionImpl extends MinimalEObjectImpl.Container implements ObjectOrMetaClassExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectOrMetaClassExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.OBJECT_OR_META_CLASS_EXPRESSION;
	}

} //ObjectOrMetaClassExpressionImpl
