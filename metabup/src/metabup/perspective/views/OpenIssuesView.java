/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.perspective.views;

import java.util.List;

import metabup.issues.general.categories.Conflict;
import metabup.Activator;
import metabup.SessionState;
import metabup.issues.general.OpenIssue;
import metabup.issues.general.categories.Automatic;
import metabup.perspective.views.actions.UpdateMetamodelByOpenIssue;
import metabup.ui.editor.editors.MetamodelInteractiveEditor;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;


/**
 * Shows a list of suggested refactorings over the active metamodel
 * 
 * @author jesusl
 */
// See TODO: http://wiki.eclipse.org/FAQ_How_to_decorate_a_TableViewer_or_TreeViewer_with_Columns%3F
public class OpenIssuesView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "metabup.perspective.views.OpenIssuesView";
	private TreeViewer viewer;
	private Action doubleClickAction;
	private DrillDownAdapter drillDownAdapter;
	private int conflictCount = 0;
	private int automaticCount = 0;
	
	public int getConflictCount() {
		return conflictCount;
	}
	
	public int getAutomaticCount(){
		return automaticCount;
	}
	
	// The connection with the editor
	protected SessionState session;
	
	class Category<T> {
		private Class<T> clazz;

		public Category(Class<T> clazz) {
			this.clazz = clazz;
		}
		
		public String toString() {
			return clazz.getSimpleName();
		}

		public String getIcon() {
			return clazz.getSimpleName().toLowerCase() + ".gif";
		}
				
		public Object[] filterIssues() {			
			List<OpenIssue> issues = session.getIssues(clazz);					
			Object[] result    = new Object[issues.size()];
			for(int i = 0; i < issues.size(); i++) result[i] = issues.get(i).getDescription();									
			return result;
		}

		public boolean hasChildrenIssues() {
			return session.getIssues(clazz).size() > 0;
		}
	}
	
	class ViewLabelProvider extends LabelProvider {

		public String getText(Object obj) {
			if ( obj instanceof Category<?> ) return obj.toString();
			return obj.toString();
		}
		
		public Image getImage(Object obj) {
			if (obj instanceof Category<?>) {
				return getIconImage( ((Category<?>) obj).getIcon());
			}
			return getIconImage("issue.gif");
		}
	}
	
	private Image getIconImage(String iconName) {
		ImageDescriptor desc = Activator.getImageDescriptor("icons/" + iconName );				
		if ( desc != null ) return desc.createImage();
		return null;
	}
	
	class NameSorter extends ViewerSorter {
	}
	
	/*
	 * The content provider class is responsible for
	 * providing objects to the view. It can wrap
	 * existing objects in adapters or simply return
	 * objects as-is. These objects may be sensitive
	 * to the current input of the view, or ignore
	 * it and always show the same content 
	 * (like Task List, for example).
	 */			
	class ViewContentProvider implements IStructuredContentProvider, ITreeContentProvider {
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			if ( session == null ) return new Object[] {};						
			
			if (parent.equals(getViewSite())) {
				Category<Conflict>   c = new Category<Conflict>(Conflict.class);
				Category<Automatic>  a = new Category<Automatic>(Automatic.class);								
				
				return new Object[] { a, c };
			}						
			
			return getChildren(parent);
		}
		
		@Override
		public Object[] getChildren(Object parentElement) {
			if ( parentElement instanceof Category<?> ){
				Object children[] = ((Category<?>) parentElement).filterIssues();
				
				/*Category<?> cat = (Category<?>) parentElement;
				//System.out.println("toString returns: "+ cat.toString());
				if(cat.toString().equals("Conflict")) conflictCount = children.length;
				else if(cat.toString().equals("Automatic")) automaticCount = children.length;*/
				
				return children;
			}
			return null;
		}
		
		@Override
		public Object getParent(Object element) {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof Category<?>) return (((Category<?>) element).hasChildrenIssues());
			return false;
		}
	}
	
	/*class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			return getText(obj);
		}
		public Image getColumnImage(Object obj, int index) {
			return getImage(obj);
		}
		public Image getImage(Object obj) {
			return PlatformUI.getWorkbench().
					getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		}
	}*/

	public OpenIssuesView() { }

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		drillDownAdapter = new DrillDownAdapter(viewer);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());
		viewer.expandAll();

		// Create the help context id for the viewer's control
		PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(), "metabup.perspective.viewer");
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
		
		getSite().getWorkbenchWindow().getPartService().addPartListener(new AssistanceListener());
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				OpenIssuesView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		//manager.add(action1);
		//manager.add(new Separator());
		//manager.add(action2);
	}

	private void fillContextMenu(IMenuManager manager) {
		//manager.add(action1);
		//manager.add(action2);
		// Other plug-ins can contribute there actions here
		//manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void fillLocalToolBar(IToolBarManager manager) {
		//manager.add(action1);
		//manager.add(action2);
	}

	private void makeActions() {
		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection)selection).getFirstElement();
				UpdateMetamodelByOpenIssue action = new UpdateMetamodelByOpenIssue();
				action.setEditor((MetamodelInteractiveEditor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor());
				action.run((String)obj);
				refresh(session);
			}
		};
				
		doubleClickAction.setToolTipText("Apply this open issue's associated operation");		
	}
	
	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}
	private void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"Assistance View",
			message);
	}
	
	public void refresh(SessionState session){
		if(session != null){
			viewer.refresh();
			viewer.expandAll();			
		}
	}
	
	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	protected class AssistanceListener implements IPartListener {

			@Override
			public void partActivated(IWorkbenchPart part) {
				////System.out.println("Activated metamodel editor");
				if ( part instanceof MetamodelInteractiveEditor ) {
					MetamodelInteractiveEditor metamodelInteractiveEditor = (MetamodelInteractiveEditor) part;
					session = metamodelInteractiveEditor.getSession();
					viewer.refresh();
					viewer.expandAll();					
				}
			}

			@Override
			public void partBroughtToTop(IWorkbenchPart part) { }

			@Override
			public void partClosed(IWorkbenchPart part) { }

			@Override
			public void partDeactivated(IWorkbenchPart part) { }

			@Override
			public void partOpened(IWorkbenchPart part) { }		
		}
}