/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.refactoring;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.InputDialog;

import metabup.annotations.catalog.design.Abstract;
import metabup.annotations.extensionpoint.RefactoringAnnotation;
import metabup.metamodel.Annotation;
import metabup.metamodel.Attribute;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.ParamValue;
import metabup.metamodel.Reference;
import metabup.metamodel.StringValue;

public class General extends RefactoringAnnotation {
	public final String P_PROMPT_CLASS_NAME = "Prompt new general class name";

	public General(){
		super();
		name = "general";
		description = "Create a new abstract metaclass containing the annotated feature and make every class containing such feature subclasses of this new abstract metaclass.";
		
		/* preference storage */
		addPreference(P_PROMPT_CLASS_NAME, true);
		super.initializePreferences();
	}
	
	@Override
	public MetaModel perform() {		 		
		if(element instanceof Attribute) performGeneralForAttribute();
		else if(element instanceof Reference) performGeneralForReference();
		else if(element instanceof MetaClass) performGeneralForMetaClass();	
		
		
		
		return targetMetaModel;
	}
			
		
	private void performGeneralForAttribute(){			
		List<MetaClass> all = super.allMetaClassesWithFeatureName(((Feature) element).getName());
		
		if (all.size()>1) {	// there is some commonality						
			InputDialog id;
			/* create or locate the super metaclass */
			ParamValue namePV = super.getAnnotationParamValueByParamName("name");
			String name;
			
			if(namePV == null){
				name = super.induceCommonName(all);
				if(super.isActiveMode() && (Boolean) this.getPreferenceValue(P_PROMPT_CLASS_NAME)) name = super.promptClassName(all, name);				
			}else{
				if(namePV instanceof StringValue) name = ((StringValue)namePV).getValue();
				else{
					this.putError("A String parameter was expected for \"name\"");
					return;
				}
			}
			
			MetaClass mc;
			
			if(!super.existsMetaClassByName(name)){
				mc = sourceMetaModel.createMetaclass();
				this.targetMetaModel.getClasses().add(mc);							
				mc.setIsAbstract(true);
			}else mc = super.getMetaClassByName(name);
																	
			List<metabup.metamodel.Annotation> allAnnotations = new ArrayList<metabup.metamodel.Annotation>(); 						
			
			for (MetaClass cont : all) {
				if(!cont.getId().equals(mc.getId())){
					cont.getSupers().add(mc);
					mc.getSubclasses().add(cont);
					allAnnotations.addAll(super.removeFeatureWithName(cont, ((Feature)element).getName()));
				}				
			}
			
			mc.getSubclasses().addAll(all);
			
			// in case all the classes containing the feature share a common superclass, the new one is put in the middle
			List<MetaClass> mcs = super.getCommonSupers(all);
			mcs.remove(mc);
			
			if(mcs != null && mcs.size() > 0){
				for(MetaClass superMC : mcs){
					superMC.getSubclasses().removeAll(all);
					for(MetaClass subMC : all){
						subMC.getSupers().removeAll(mcs);
					}
				}
				
				mc.getSupers().addAll(mcs);
			}
			
			Annotation an = sourceMetaModel.createAnnotation();
			an.setName(Abstract.NAME);
			mc.getAnnotations().add(an);
			
			mc.setName(name);					
			mc.getFeatures().add((Feature)element);	
		}
	}
	
	private void performGeneralForReference(){			
		List<MetaClass> all = super.allMetaClassesWithFeatureName(((Feature) element).getName());
		List<MetaClass> allTars = new ArrayList<MetaClass>();
		
		// calculate all tars
		for(MetaClass mc : all){
			Reference r = mc.getReferenceByName(((Feature)element).getName(), false);
			if(!allTars.contains(r.getReference())) allTars.add(r.getReference());
		}
		
		if (all.size()>1) {	// there is some commonality						
			InputDialog id;
			/* create or locate the super metaclass */
			ParamValue namePV = super.getAnnotationParamValueByParamName("name");
			String name;
			
			if(namePV == null){
				name = super.induceCommonName(all);
				if(super.isActiveMode() && (Boolean) this.getPreferenceValue(P_PROMPT_CLASS_NAME)) name = super.promptClassName(all, name);				
			}else{
				if(namePV instanceof StringValue) name = ((StringValue)namePV).getValue();
				else{
					this.putError("A String parameter was expected for \"name\"");
					return;
				}
			}
			
			MetaClass mc;
			
			if(!super.existsMetaClassByName(name)){
				mc = sourceMetaModel.createMetaclass();
				this.targetMetaModel.getClasses().add(mc);							
				mc.setIsAbstract(true);
			}else mc = super.getMetaClassByName(name);
																	
			List<metabup.metamodel.Annotation> allAnnotations = new ArrayList<metabup.metamodel.Annotation>(); 						
			List<MetaClass> tarMCs = new ArrayList<MetaClass>();
			
			for (MetaClass cont : all) {
				if(!cont.getId().equals(mc.getId())){
					cont.getSupers().add(mc);
					mc.getSubclasses().add(cont);					
					allAnnotations.addAll(super.removeFeatureWithName(cont, ((Feature)element).getName()));
				}				
			}
			
			mc.getSubclasses().addAll(all);
			
			// in case all the classes containing the feature share a common superclass, the new one is put in the middle
			List<MetaClass> mcs = super.getCommonSupers(all);
			mcs.remove(mc);
			
			if(mcs != null && mcs.size() > 0){
				for(MetaClass superMC : mcs){
					superMC.getSubclasses().removeAll(all);
					for(MetaClass subMC : all){
						subMC.getSupers().removeAll(mcs);
					}
				}
				
				mc.getSupers().addAll(mcs);
			}
			
			Annotation an = sourceMetaModel.createAnnotation();
			an.setName(Abstract.NAME);
			mc.getAnnotations().add(an);
			
			mc.setName(name);	
			
			Reference r = (Reference)element;
			
			if(allTars.size() > 1){
				MetaClass newMC = sourceMetaModel.createMetaclass();
				mc.setName(super.induceCommonName(allTars));
				mc.setIsAbstract(true);
				r.setReference(newMC);
			}
			
			mc.getFeatures().add(r);	
		}
	}
	
	private void performGeneralForMetaClass(){	
		List<MetaClass> all = super.allMetaClassesWithAnnotation(annotation);				
		
		if(all.size() > 1){					
			/* check whether the refactoring has already been applied or  */
			if(super.getCommonFeatures(all).isEmpty()) return;			
			/* create or locate the super metaclass */
			ParamValue namePM = super.getAnnotationParamValueByParamName("name");
			String name;
			
			if(namePM == null){
				List<MetaClass> supers = super.getCommonSupers(all);
				if(supers!=null && !supers.isEmpty()) name = supers.get(0).getName();
				else name = super.induceCommonName(all);
				if(super.isActiveMode() && (Boolean) this.getPreferenceValue(P_PROMPT_CLASS_NAME)) name = super.promptClassName(all, name);							
			}else{
				if(namePM instanceof StringValue) name = ((StringValue)namePM).getValue();
				else{
					this.putError("A String parameter was expected for \"name\"");
					return;
				}
			}
			
			MetaClass superMC = null;
			
			if(!super.existsMetaClassByName(name)){
				superMC = sourceMetaModel.createMetaclass();				
				superMC.setName(name);
				superMC.setIsAbstract(true);		
				this.targetMetaModel.getClasses().add(superMC);
								
				Annotation an = sourceMetaModel.createAnnotation();
				an.setName("abstract");
				superMC.getAnnotations().add(an);				
			}else superMC = super.getMetaClassByName(name);			
			
			/* take every common feature and relocate it */				
			List<Feature> featList = super.getCommonFeatures(all);				
			List<String> featNames = new ArrayList<String>();
				
			for(Feature f : featList) featNames.add(f.getName());							
			superMC.getFeatures().addAll(featList);
			
			for(MetaClass m : all) for(String fn : featNames) super.removeFeatureWithName(m, fn);
			//for(MetaClass m : all) super.removeAnnotationWithName(m, annotation.getName());
			
			for(MetaClass m : all){
				if(!m.getId().equals(superMC.getId())){ 
					m.getSupers().add(superMC);
					superMC.getSubclasses().add(m);
					super.removeReplicatedFeatures(superMC);					
				}
			}	
			
			superMC.getSubclasses().addAll(all);
		}
							
		/*take every common super metaclass */
		// does it make any sense doin' it?
	}
	
	
}
