/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package metabup.shell;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see metabup.shell.ShellPackage#getCommand()
 * @model abstract="true"
 * @generated
 */
public interface Command extends EObject {
} // Command
