/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.constraint;

import java.util.ArrayList;
import java.util.List;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.Object;
import metabup.annotations.extensionpoint.ConstraintAnnotation;
import metabup.annotations.extensionpoint.DesignAnnotation;
import metabup.issues.general.IOpenIssue;
import metabup.issues.lib.UniqueMetaClassMultiInstanced;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaClass;

public class Unique extends ConstraintAnnotation {
	public Unique (){	
		this.setName("unique");
		this.setDescription("The annotated feature shall be instanced only once");
	}
	
	@Override
	public List<IOpenIssue> detectFragmentIssues(FragmentModel fm, AnnotatedElement ae){
		if(fm == null || ae == null || fm.getFragments() == null || fm.getFragments().isEmpty()) return null;
		List<IOpenIssue> issues = new ArrayList<IOpenIssue>();
		
		if(ae instanceof MetaClass){
			List<Object> objects = new ArrayList<Object>();
			MetaClass mc = (MetaClass)ae;
			
			for(Fragment f : fm.getFragments())
				for(Object o : f.getObjects())
					if(o.getType().equals(mc.getName())) objects.add(o);
			
			if(objects.size() > 1){
				UniqueMetaClassMultiInstanced umcmi = new UniqueMetaClassMultiInstanced(mc, objects);
				issues.add(umcmi);
				return issues;
			}
		}
		
		return null;
	}

	@Override
	public void updateConstraints() {
		if(element instanceof MetaClass){
			MetaClass mc = (MetaClass)element;
			String ELEMENT = mc.getName();
			
			super.addConstraint("unique", "context " + ELEMENT + " inv " + ELEMENT + "_unique : " + ELEMENT + ".allInstances()->size() = 1");
		}
	}
}
