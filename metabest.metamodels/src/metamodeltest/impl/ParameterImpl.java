/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.MetamodeltestPackage;
import metamodeltest.Parameter;
import metamodeltest.UserDefinedParameter;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.ParameterImpl#getParameterDefinition <em>Parameter Definition</em>}</li>
 *   <li>{@link metamodeltest.impl.ParameterImpl#getParameterReuse <em>Parameter Reuse</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ParameterImpl extends MinimalEObjectImpl.Container implements Parameter {
	/**
	 * The cached value of the '{@link #getParameterDefinition() <em>Parameter Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterDefinition()
	 * @generated
	 * @ordered
	 */
	protected UserDefinedParameter parameterDefinition;

	/**
	 * The cached value of the '{@link #getParameterReuse() <em>Parameter Reuse</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterReuse()
	 * @generated
	 * @ordered
	 */
	protected UserDefinedParameter parameterReuse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserDefinedParameter getParameterDefinition() {
		return parameterDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameterDefinition(UserDefinedParameter newParameterDefinition, NotificationChain msgs) {
		UserDefinedParameter oldParameterDefinition = parameterDefinition;
		parameterDefinition = newParameterDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PARAMETER__PARAMETER_DEFINITION, oldParameterDefinition, newParameterDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterDefinition(UserDefinedParameter newParameterDefinition) {
		if (newParameterDefinition != parameterDefinition) {
			NotificationChain msgs = null;
			if (parameterDefinition != null)
				msgs = ((InternalEObject)parameterDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PARAMETER__PARAMETER_DEFINITION, null, msgs);
			if (newParameterDefinition != null)
				msgs = ((InternalEObject)newParameterDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PARAMETER__PARAMETER_DEFINITION, null, msgs);
			msgs = basicSetParameterDefinition(newParameterDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PARAMETER__PARAMETER_DEFINITION, newParameterDefinition, newParameterDefinition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserDefinedParameter getParameterReuse() {
		if (parameterReuse != null && parameterReuse.eIsProxy()) {
			InternalEObject oldParameterReuse = (InternalEObject)parameterReuse;
			parameterReuse = (UserDefinedParameter)eResolveProxy(oldParameterReuse);
			if (parameterReuse != oldParameterReuse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MetamodeltestPackage.PARAMETER__PARAMETER_REUSE, oldParameterReuse, parameterReuse));
			}
		}
		return parameterReuse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserDefinedParameter basicGetParameterReuse() {
		return parameterReuse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParameterReuse(UserDefinedParameter newParameterReuse) {
		UserDefinedParameter oldParameterReuse = parameterReuse;
		parameterReuse = newParameterReuse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PARAMETER__PARAMETER_REUSE, oldParameterReuse, parameterReuse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.PARAMETER__PARAMETER_DEFINITION:
				return basicSetParameterDefinition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.PARAMETER__PARAMETER_DEFINITION:
				return getParameterDefinition();
			case MetamodeltestPackage.PARAMETER__PARAMETER_REUSE:
				if (resolve) return getParameterReuse();
				return basicGetParameterReuse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.PARAMETER__PARAMETER_DEFINITION:
				setParameterDefinition((UserDefinedParameter)newValue);
				return;
			case MetamodeltestPackage.PARAMETER__PARAMETER_REUSE:
				setParameterReuse((UserDefinedParameter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PARAMETER__PARAMETER_DEFINITION:
				setParameterDefinition((UserDefinedParameter)null);
				return;
			case MetamodeltestPackage.PARAMETER__PARAMETER_REUSE:
				setParameterReuse((UserDefinedParameter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PARAMETER__PARAMETER_DEFINITION:
				return parameterDefinition != null;
			case MetamodeltestPackage.PARAMETER__PARAMETER_REUSE:
				return parameterReuse != null;
		}
		return super.eIsSet(featureID);
	}

} //ParameterImpl
