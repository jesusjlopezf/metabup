/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.metamodel;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metabup.metamodel.MetaModel#getClasses <em>Classes</em>}</li>
 *   <li>{@link metabup.metamodel.MetaModel#getName <em>Name</em>}</li>
 *   <li>{@link metabup.metamodel.MetaModel#getSrcURI <em>Src URI</em>}</li>
 * </ul>
 *
 * @see metabup.metamodel.MetamodelPackage#getMetaModel()
 * @model
 * @generated
 */
public interface MetaModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link metabup.metamodel.MetaClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see metabup.metamodel.MetamodelPackage#getMetaModel_Classes()
	 * @model containment="true"
	 * @generated
	 */
	EList<MetaClass> getClasses();
	MetaClass getClassById(String id);
	AnnotatedElement getElementById(String id);
	MetaClass getClassByName(String name);
	List<Reference> getReferences();
	List<Attribute> getAttributes();
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see metabup.metamodel.MetamodelPackage#getMetaModel_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link metabup.metamodel.MetaModel#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);
	
	
	/**
	 * Returns the value of the '<em><b>Src URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src URI</em>' attribute.
	 * @see #setSrcURI(String)
	 * @see metabup.metamodel.MetamodelPackage#getMetaModel_SrcURI()
	 * @model
	 * @generated
	 */
	String getSrcURI();
	/**
	 * Sets the value of the '{@link metabup.metamodel.MetaModel#getSrcURI <em>Src URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src URI</em>' attribute.
	 * @see #getSrcURI()
	 * @generated
	 */
	void setSrcURI(String value);
	List<EObject> getAllElements();
} // MetaModel