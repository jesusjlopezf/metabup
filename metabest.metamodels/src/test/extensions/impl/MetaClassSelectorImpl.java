/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.extensions.impl;

import metamodeltest.Quantifier;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import test.extensions.ExtensionsPackage;
import test.extensions.MetaClassQualifier;
import test.extensions.MetaClassSelector;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Class Selector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link test.extensions.impl.MetaClassSelectorImpl#getFilter <em>Filter</em>}</li>
 *   <li>{@link test.extensions.impl.MetaClassSelectorImpl#isOnlyNewElements <em>Only New Elements</em>}</li>
 *   <li>{@link test.extensions.impl.MetaClassSelectorImpl#getQuantifier <em>Quantifier</em>}</li>
 *   <li>{@link test.extensions.impl.MetaClassSelectorImpl#getMetaclassName <em>Metaclass Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetaClassSelectorImpl extends SelectorImpl implements MetaClassSelector {
	/**
	 * The cached value of the '{@link #getFilter() <em>Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected MetaClassQualifier filter;

	/**
	 * The default value of the '{@link #isOnlyNewElements() <em>Only New Elements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnlyNewElements()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ONLY_NEW_ELEMENTS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isOnlyNewElements() <em>Only New Elements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOnlyNewElements()
	 * @generated
	 * @ordered
	 */
	protected boolean onlyNewElements = ONLY_NEW_ELEMENTS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getQuantifier() <em>Quantifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuantifier()
	 * @generated
	 * @ordered
	 */
	protected Quantifier quantifier;

	/**
	 * The default value of the '{@link #getMetaclassName() <em>Metaclass Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaclassName()
	 * @generated
	 * @ordered
	 */
	protected static final String METACLASS_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMetaclassName() <em>Metaclass Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaclassName()
	 * @generated
	 * @ordered
	 */
	protected String metaclassName = METACLASS_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetaClassSelectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExtensionsPackage.Literals.META_CLASS_SELECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaClassQualifier getFilter() {
		return filter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFilter(MetaClassQualifier newFilter, NotificationChain msgs) {
		MetaClassQualifier oldFilter = filter;
		filter = newFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExtensionsPackage.META_CLASS_SELECTOR__FILTER, oldFilter, newFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilter(MetaClassQualifier newFilter) {
		if (newFilter != filter) {
			NotificationChain msgs = null;
			if (filter != null)
				msgs = ((InternalEObject)filter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExtensionsPackage.META_CLASS_SELECTOR__FILTER, null, msgs);
			if (newFilter != null)
				msgs = ((InternalEObject)newFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExtensionsPackage.META_CLASS_SELECTOR__FILTER, null, msgs);
			msgs = basicSetFilter(newFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionsPackage.META_CLASS_SELECTOR__FILTER, newFilter, newFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOnlyNewElements() {
		return onlyNewElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnlyNewElements(boolean newOnlyNewElements) {
		boolean oldOnlyNewElements = onlyNewElements;
		onlyNewElements = newOnlyNewElements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionsPackage.META_CLASS_SELECTOR__ONLY_NEW_ELEMENTS, oldOnlyNewElements, onlyNewElements));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Quantifier getQuantifier() {
		return quantifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQuantifier(Quantifier newQuantifier, NotificationChain msgs) {
		Quantifier oldQuantifier = quantifier;
		quantifier = newQuantifier;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExtensionsPackage.META_CLASS_SELECTOR__QUANTIFIER, oldQuantifier, newQuantifier);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuantifier(Quantifier newQuantifier) {
		if (newQuantifier != quantifier) {
			NotificationChain msgs = null;
			if (quantifier != null)
				msgs = ((InternalEObject)quantifier).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExtensionsPackage.META_CLASS_SELECTOR__QUANTIFIER, null, msgs);
			if (newQuantifier != null)
				msgs = ((InternalEObject)newQuantifier).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExtensionsPackage.META_CLASS_SELECTOR__QUANTIFIER, null, msgs);
			msgs = basicSetQuantifier(newQuantifier, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionsPackage.META_CLASS_SELECTOR__QUANTIFIER, newQuantifier, newQuantifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMetaclassName() {
		return metaclassName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetaclassName(String newMetaclassName) {
		String oldMetaclassName = metaclassName;
		metaclassName = newMetaclassName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionsPackage.META_CLASS_SELECTOR__METACLASS_NAME, oldMetaclassName, metaclassName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ExtensionsPackage.META_CLASS_SELECTOR__FILTER:
				return basicSetFilter(null, msgs);
			case ExtensionsPackage.META_CLASS_SELECTOR__QUANTIFIER:
				return basicSetQuantifier(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExtensionsPackage.META_CLASS_SELECTOR__FILTER:
				return getFilter();
			case ExtensionsPackage.META_CLASS_SELECTOR__ONLY_NEW_ELEMENTS:
				return isOnlyNewElements();
			case ExtensionsPackage.META_CLASS_SELECTOR__QUANTIFIER:
				return getQuantifier();
			case ExtensionsPackage.META_CLASS_SELECTOR__METACLASS_NAME:
				return getMetaclassName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExtensionsPackage.META_CLASS_SELECTOR__FILTER:
				setFilter((MetaClassQualifier)newValue);
				return;
			case ExtensionsPackage.META_CLASS_SELECTOR__ONLY_NEW_ELEMENTS:
				setOnlyNewElements((Boolean)newValue);
				return;
			case ExtensionsPackage.META_CLASS_SELECTOR__QUANTIFIER:
				setQuantifier((Quantifier)newValue);
				return;
			case ExtensionsPackage.META_CLASS_SELECTOR__METACLASS_NAME:
				setMetaclassName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExtensionsPackage.META_CLASS_SELECTOR__FILTER:
				setFilter((MetaClassQualifier)null);
				return;
			case ExtensionsPackage.META_CLASS_SELECTOR__ONLY_NEW_ELEMENTS:
				setOnlyNewElements(ONLY_NEW_ELEMENTS_EDEFAULT);
				return;
			case ExtensionsPackage.META_CLASS_SELECTOR__QUANTIFIER:
				setQuantifier((Quantifier)null);
				return;
			case ExtensionsPackage.META_CLASS_SELECTOR__METACLASS_NAME:
				setMetaclassName(METACLASS_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExtensionsPackage.META_CLASS_SELECTOR__FILTER:
				return filter != null;
			case ExtensionsPackage.META_CLASS_SELECTOR__ONLY_NEW_ELEMENTS:
				return onlyNewElements != ONLY_NEW_ELEMENTS_EDEFAULT;
			case ExtensionsPackage.META_CLASS_SELECTOR__QUANTIFIER:
				return quantifier != null;
			case ExtensionsPackage.META_CLASS_SELECTOR__METACLASS_NAME:
				return METACLASS_NAME_EDEFAULT == null ? metaclassName != null : !METACLASS_NAME_EDEFAULT.equals(metaclassName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (onlyNewElements: ");
		result.append(onlyNewElements);
		result.append(", metaclassName: ");
		result.append(metaclassName);
		result.append(')');
		return result.toString();
	}

} //MetaClassSelectorImpl
