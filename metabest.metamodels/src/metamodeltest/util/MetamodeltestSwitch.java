/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.util;

import metamodeltest.Abstract;
import metamodeltest.Abstraction;
import metamodeltest.Adjective;
import metamodeltest.AndQualifier;
import metamodeltest.Annotated;
import metamodeltest.AnnotatedElement;
import metamodeltest.Annotation;
import metamodeltest.Assertion;
import metamodeltest.AssertionCall;
import metamodeltest.AssertionDefinition;
import metamodeltest.AssertionLibrary;
import metamodeltest.AssertionTest;
import metamodeltest.AttributeContainment;
import metamodeltest.AttributeFilter;
import metamodeltest.AttributeQualifier;
import metamodeltest.AttributeSelector;
import metamodeltest.BooleanParameter;
import metamodeltest.ClassCollects;
import metamodeltest.ClassContainee;
import metamodeltest.ClassContainment;
import metamodeltest.ClassFilter;
import metamodeltest.ClassQualifier;
import metamodeltest.ClassReachedBy;
import metamodeltest.ClassReaches;
import metamodeltest.ClassRelation;
import metamodeltest.ClassSelector;
import metamodeltest.CompositionLeaf;
import metamodeltest.CompoundPathQualifier;
import metamodeltest.CompoundQualifier;
import metamodeltest.ContLeaf;
import metamodeltest.ContRoot;
import metamodeltest.ContainmentPath;
import metamodeltest.ContainmentReference;
import metamodeltest.Contains;
import metamodeltest.CyclicPath;
import metamodeltest.DescribedElement;
import metamodeltest.ElementAssertion;
import metamodeltest.ElementFeaturement;
import metamodeltest.ElementFilter;
import metamodeltest.ElementQualifier;
import metamodeltest.ElementRelation;
import metamodeltest.ElementSelector;
import metamodeltest.Every;
import metamodeltest.Existance;
import metamodeltest.FeatureContainment;
import metamodeltest.FeatureFilter;
import metamodeltest.FeatureMultiplicity;
import metamodeltest.FeatureQualifier;
import metamodeltest.FeatureSelector;
import metamodeltest.Filter;
import metamodeltest.FixedBound;
import metamodeltest.FromTo;
import metamodeltest.Import;
import metamodeltest.Infinite;
import metamodeltest.InhLeaf;
import metamodeltest.InhRoot;
import metamodeltest.Inheritance;
import metamodeltest.InheritedFeature;
import metamodeltest.InheritedFeatureContainment;
import metamodeltest.IntegerParameter;
import metamodeltest.IsSubTo;
import metamodeltest.IsSuperTo;
import metamodeltest.Length;
import metamodeltest.LessThan;
import metamodeltest.LibraryAssertionCall;
import metamodeltest.Literal;
import metamodeltest.LowerCase;
import metamodeltest.MaxMultiplicity;
import metamodeltest.MetaModelTest;
import metamodeltest.MetamodeltestPackage;
import metamodeltest.MoreThan;
import metamodeltest.Name;
import metamodeltest.NamePart;
import metamodeltest.Named;
import metamodeltest.NamedAfter;
import metamodeltest.NamedElement;
import metamodeltest.None;
import metamodeltest.Noun;
import metamodeltest.OCLExpression;
import metamodeltest.OppositeTo;
import metamodeltest.OrQualifier;
import metamodeltest.Parameter;
import metamodeltest.ParameterElementSetValue;
import metamodeltest.ParameterStringValue;
import metamodeltest.ParameterValue;
import metamodeltest.PathAnd;
import metamodeltest.PathFilter;
import metamodeltest.PathOr;
import metamodeltest.PathQualifier;
import metamodeltest.PathSelector;
import metamodeltest.PathSide;
import metamodeltest.PathSource;
import metamodeltest.PathTarget;
import metamodeltest.PhraseNamed;
import metamodeltest.Prefix;
import metamodeltest.Qualifier;
import metamodeltest.Quantifier;
import metamodeltest.ReferenceContainment;
import metamodeltest.ReferenceFilter;
import metamodeltest.ReferenceQualifier;
import metamodeltest.ReferenceSelector;
import metamodeltest.Selector;
import metamodeltest.SelfInheritance;
import metamodeltest.SideClass;
import metamodeltest.Some;
import metamodeltest.SourceClass;
import metamodeltest.StepClass;
import metamodeltest.StrictNumber;
import metamodeltest.StringParameter;
import metamodeltest.Substring;
import metamodeltest.Suffix;
import metamodeltest.Synonym;
import metamodeltest.TargetClass;
import metamodeltest.TrivialString;
import metamodeltest.Typed;
import metamodeltest.UpperCase;
import metamodeltest.UserDefinedParameter;
import metamodeltest.Verb;
import metamodeltest.WithInheritancePath;
import metamodeltest.WordNature;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see metamodeltest.MetamodeltestPackage
 * @generated
 */
public class MetamodeltestSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MetamodeltestPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetamodeltestSwitch() {
		if (modelPackage == null) {
			modelPackage = MetamodeltestPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MetamodeltestPackage.META_MODEL_TEST: {
				MetaModelTest metaModelTest = (MetaModelTest)theEObject;
				T result = caseMetaModelTest(metaModelTest);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ASSERTION: {
				Assertion assertion = (Assertion)theEObject;
				T result = caseAssertion(assertion);
				if (result == null) result = caseAssertionCall(assertion);
				if (result == null) result = caseDescribedElement(assertion);
				if (result == null) result = caseAnnotatedElement(assertion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.QUANTIFIER: {
				Quantifier quantifier = (Quantifier)theEObject;
				T result = caseQuantifier(quantifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.NONE: {
				None none = (None)theEObject;
				T result = caseNone(none);
				if (result == null) result = caseQuantifier(none);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.NUMBER: {
				metamodeltest.Number number = (metamodeltest.Number)theEObject;
				T result = caseNumber(number);
				if (result == null) result = caseQuantifier(number);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.EVERY: {
				Every every = (Every)theEObject;
				T result = caseEvery(every);
				if (result == null) result = caseQuantifier(every);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.LESS_THAN: {
				LessThan lessThan = (LessThan)theEObject;
				T result = caseLessThan(lessThan);
				if (result == null) result = caseNumber(lessThan);
				if (result == null) result = caseQuantifier(lessThan);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.MORE_THAN: {
				MoreThan moreThan = (MoreThan)theEObject;
				T result = caseMoreThan(moreThan);
				if (result == null) result = caseNumber(moreThan);
				if (result == null) result = caseQuantifier(moreThan);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ELEMENT_ASSERTION: {
				ElementAssertion elementAssertion = (ElementAssertion)theEObject;
				T result = caseElementAssertion(elementAssertion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.NAME: {
				Name name = (Name)theEObject;
				T result = caseName(name);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ABSTRACT: {
				Abstract abstract_ = (Abstract)theEObject;
				T result = caseAbstract(abstract_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.FILTER: {
				Filter filter = (Filter)theEObject;
				T result = caseFilter(filter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ELEMENT_FILTER: {
				ElementFilter elementFilter = (ElementFilter)theEObject;
				T result = caseElementFilter(elementFilter);
				if (result == null) result = caseFilter(elementFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CLASS_FILTER: {
				ClassFilter classFilter = (ClassFilter)theEObject;
				T result = caseClassFilter(classFilter);
				if (result == null) result = caseElementFilter(classFilter);
				if (result == null) result = caseFilter(classFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.FEATURE_FILTER: {
				FeatureFilter featureFilter = (FeatureFilter)theEObject;
				T result = caseFeatureFilter(featureFilter);
				if (result == null) result = caseElementFilter(featureFilter);
				if (result == null) result = caseFilter(featureFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ATTRIBUTE_FILTER: {
				AttributeFilter attributeFilter = (AttributeFilter)theEObject;
				T result = caseAttributeFilter(attributeFilter);
				if (result == null) result = caseFeatureFilter(attributeFilter);
				if (result == null) result = caseElementFilter(attributeFilter);
				if (result == null) result = caseFilter(attributeFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.REFERENCE_FILTER: {
				ReferenceFilter referenceFilter = (ReferenceFilter)theEObject;
				T result = caseReferenceFilter(referenceFilter);
				if (result == null) result = caseFeatureFilter(referenceFilter);
				if (result == null) result = caseElementFilter(referenceFilter);
				if (result == null) result = caseFilter(referenceFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ELEMENT_QUALIFIER: {
				ElementQualifier elementQualifier = (ElementQualifier)theEObject;
				T result = caseElementQualifier(elementQualifier);
				if (result == null) result = caseQualifier(elementQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.QUALIFIER: {
				Qualifier qualifier = (Qualifier)theEObject;
				T result = caseQualifier(qualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.NAMED: {
				Named named = (Named)theEObject;
				T result = caseNamed(named);
				if (result == null) result = caseElementQualifier(named);
				if (result == null) result = caseQualifier(named);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CLASS_QUALIFIER: {
				ClassQualifier classQualifier = (ClassQualifier)theEObject;
				T result = caseClassQualifier(classQualifier);
				if (result == null) result = caseQualifier(classQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.FEATURE_QUALIFIER: {
				FeatureQualifier featureQualifier = (FeatureQualifier)theEObject;
				T result = caseFeatureQualifier(featureQualifier);
				if (result == null) result = caseQualifier(featureQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ATTRIBUTE_QUALIFIER: {
				AttributeQualifier attributeQualifier = (AttributeQualifier)theEObject;
				T result = caseAttributeQualifier(attributeQualifier);
				if (result == null) result = caseQualifier(attributeQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.REFERENCE_QUALIFIER: {
				ReferenceQualifier referenceQualifier = (ReferenceQualifier)theEObject;
				T result = caseReferenceQualifier(referenceQualifier);
				if (result == null) result = caseQualifier(referenceQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ABSTRACTION: {
				Abstraction abstraction = (Abstraction)theEObject;
				T result = caseAbstraction(abstraction);
				if (result == null) result = caseClassQualifier(abstraction);
				if (result == null) result = caseQualifier(abstraction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.IS_SUPER_TO: {
				IsSuperTo isSuperTo = (IsSuperTo)theEObject;
				T result = caseIsSuperTo(isSuperTo);
				if (result == null) result = caseInheritance(isSuperTo);
				if (result == null) result = caseClassQualifier(isSuperTo);
				if (result == null) result = caseQualifier(isSuperTo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.IS_SUB_TO: {
				IsSubTo isSubTo = (IsSubTo)theEObject;
				T result = caseIsSubTo(isSubTo);
				if (result == null) result = caseInheritance(isSubTo);
				if (result == null) result = caseClassQualifier(isSubTo);
				if (result == null) result = caseQualifier(isSubTo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CLASS_RELATION: {
				ClassRelation classRelation = (ClassRelation)theEObject;
				T result = caseClassRelation(classRelation);
				if (result == null) result = caseClassQualifier(classRelation);
				if (result == null) result = caseQualifier(classRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.FEATURE_CONTAINMENT: {
				FeatureContainment featureContainment = (FeatureContainment)theEObject;
				T result = caseFeatureContainment(featureContainment);
				if (result == null) result = caseElementRelation(featureContainment);
				if (result == null) result = caseClassQualifier(featureContainment);
				if (result == null) result = caseQualifier(featureContainment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.INHERITED_FEATURE_CONTAINMENT: {
				InheritedFeatureContainment inheritedFeatureContainment = (InheritedFeatureContainment)theEObject;
				T result = caseInheritedFeatureContainment(inheritedFeatureContainment);
				if (result == null) result = caseFeatureContainment(inheritedFeatureContainment);
				if (result == null) result = caseElementRelation(inheritedFeatureContainment);
				if (result == null) result = caseClassQualifier(inheritedFeatureContainment);
				if (result == null) result = caseQualifier(inheritedFeatureContainment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.INH_LEAF: {
				InhLeaf inhLeaf = (InhLeaf)theEObject;
				T result = caseInhLeaf(inhLeaf);
				if (result == null) result = caseClassQualifier(inhLeaf);
				if (result == null) result = caseQualifier(inhLeaf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.INH_ROOT: {
				InhRoot inhRoot = (InhRoot)theEObject;
				T result = caseInhRoot(inhRoot);
				if (result == null) result = caseClassQualifier(inhRoot);
				if (result == null) result = caseQualifier(inhRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.COMPOSITION_LEAF: {
				CompositionLeaf compositionLeaf = (CompositionLeaf)theEObject;
				T result = caseCompositionLeaf(compositionLeaf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.MAX_MULTIPLICITY: {
				MaxMultiplicity maxMultiplicity = (MaxMultiplicity)theEObject;
				T result = caseMaxMultiplicity(maxMultiplicity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.FIXED_BOUND: {
				FixedBound fixedBound = (FixedBound)theEObject;
				T result = caseFixedBound(fixedBound);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.INFINITE: {
				Infinite infinite = (Infinite)theEObject;
				T result = caseInfinite(infinite);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CLASS_CONTAINMENT: {
				ClassContainment classContainment = (ClassContainment)theEObject;
				T result = caseClassContainment(classContainment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CLASS_CONTAINEE: {
				ClassContainee classContainee = (ClassContainee)theEObject;
				T result = caseClassContainee(classContainee);
				if (result == null) result = caseFeatureQualifier(classContainee);
				if (result == null) result = caseQualifier(classContainee);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.TYPED: {
				Typed typed = (Typed)theEObject;
				T result = caseTyped(typed);
				if (result == null) result = caseAttributeQualifier(typed);
				if (result == null) result = caseQualifier(typed);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PREFIX: {
				Prefix prefix = (Prefix)theEObject;
				T result = casePrefix(prefix);
				if (result == null) result = caseSubstring(prefix);
				if (result == null) result = caseNamed(prefix);
				if (result == null) result = caseElementQualifier(prefix);
				if (result == null) result = caseQualifier(prefix);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.SUFFIX: {
				Suffix suffix = (Suffix)theEObject;
				T result = caseSuffix(suffix);
				if (result == null) result = caseSubstring(suffix);
				if (result == null) result = caseNamed(suffix);
				if (result == null) result = caseElementQualifier(suffix);
				if (result == null) result = caseQualifier(suffix);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CONTAINS: {
				Contains contains = (Contains)theEObject;
				T result = caseContains(contains);
				if (result == null) result = caseSubstring(contains);
				if (result == null) result = caseNamed(contains);
				if (result == null) result = caseElementQualifier(contains);
				if (result == null) result = caseQualifier(contains);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.SIDE_CLASS: {
				SideClass sideClass = (SideClass)theEObject;
				T result = caseSideClass(sideClass);
				if (result == null) result = caseReferenceQualifier(sideClass);
				if (result == null) result = caseQualifier(sideClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.FROM_TO: {
				FromTo fromTo = (FromTo)theEObject;
				T result = caseFromTo(fromTo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.STRICT_NUMBER: {
				StrictNumber strictNumber = (StrictNumber)theEObject;
				T result = caseStrictNumber(strictNumber);
				if (result == null) result = caseNumber(strictNumber);
				if (result == null) result = caseQuantifier(strictNumber);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.INHERITANCE: {
				Inheritance inheritance = (Inheritance)theEObject;
				T result = caseInheritance(inheritance);
				if (result == null) result = caseClassQualifier(inheritance);
				if (result == null) result = caseQualifier(inheritance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.FEATURE_MULTIPLICITY: {
				FeatureMultiplicity featureMultiplicity = (FeatureMultiplicity)theEObject;
				T result = caseFeatureMultiplicity(featureMultiplicity);
				if (result == null) result = caseFeatureQualifier(featureMultiplicity);
				if (result == null) result = caseQualifier(featureMultiplicity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.UPPER_CASE: {
				UpperCase upperCase = (UpperCase)theEObject;
				T result = caseUpperCase(upperCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.LOWER_CASE: {
				LowerCase lowerCase = (LowerCase)theEObject;
				T result = caseLowerCase(lowerCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.SUBSTRING: {
				Substring substring = (Substring)theEObject;
				T result = caseSubstring(substring);
				if (result == null) result = caseNamed(substring);
				if (result == null) result = caseElementQualifier(substring);
				if (result == null) result = caseQualifier(substring);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ASSERTION_LIBRARY: {
				AssertionLibrary assertionLibrary = (AssertionLibrary)theEObject;
				T result = caseAssertionLibrary(assertionLibrary);
				if (result == null) result = caseNamedElement(assertionLibrary);
				if (result == null) result = caseDescribedElement(assertionLibrary);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ASSERTION_TEST: {
				AssertionTest assertionTest = (AssertionTest)theEObject;
				T result = caseAssertionTest(assertionTest);
				if (result == null) result = caseAnnotatedElement(assertionTest);
				if (result == null) result = caseDescribedElement(assertionTest);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ASSERTION_DEFINITION: {
				AssertionDefinition assertionDefinition = (AssertionDefinition)theEObject;
				T result = caseAssertionDefinition(assertionDefinition);
				if (result == null) result = caseNamedElement(assertionDefinition);
				if (result == null) result = caseDescribedElement(assertionDefinition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL: {
				LibraryAssertionCall libraryAssertionCall = (LibraryAssertionCall)theEObject;
				T result = caseLibraryAssertionCall(libraryAssertionCall);
				if (result == null) result = caseAssertionCall(libraryAssertionCall);
				if (result == null) result = caseDescribedElement(libraryAssertionCall);
				if (result == null) result = caseAnnotatedElement(libraryAssertionCall);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ASSERTION_CALL: {
				AssertionCall assertionCall = (AssertionCall)theEObject;
				T result = caseAssertionCall(assertionCall);
				if (result == null) result = caseDescribedElement(assertionCall);
				if (result == null) result = caseAnnotatedElement(assertionCall);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PARAMETER: {
				Parameter parameter = (Parameter)theEObject;
				T result = caseParameter(parameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.BOOLEAN_PARAMETER: {
				BooleanParameter booleanParameter = (BooleanParameter)theEObject;
				T result = caseBooleanParameter(booleanParameter);
				if (result == null) result = caseParameter(booleanParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.INTEGER_PARAMETER: {
				IntegerParameter integerParameter = (IntegerParameter)theEObject;
				T result = caseIntegerParameter(integerParameter);
				if (result == null) result = caseParameter(integerParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.STRING_PARAMETER: {
				StringParameter stringParameter = (StringParameter)theEObject;
				T result = caseStringParameter(stringParameter);
				if (result == null) result = caseParameter(stringParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.USER_DEFINED_PARAMETER: {
				UserDefinedParameter userDefinedParameter = (UserDefinedParameter)theEObject;
				T result = caseUserDefinedParameter(userDefinedParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PATH_FILTER: {
				PathFilter pathFilter = (PathFilter)theEObject;
				T result = casePathFilter(pathFilter);
				if (result == null) result = caseFilter(pathFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.LENGTH: {
				Length length = (Length)theEObject;
				T result = caseLength(length);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.SOURCE_CLASS: {
				SourceClass sourceClass = (SourceClass)theEObject;
				T result = caseSourceClass(sourceClass);
				if (result == null) result = caseSideClass(sourceClass);
				if (result == null) result = caseReferenceQualifier(sourceClass);
				if (result == null) result = caseQualifier(sourceClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.TARGET_CLASS: {
				TargetClass targetClass = (TargetClass)theEObject;
				T result = caseTargetClass(targetClass);
				if (result == null) result = caseSideClass(targetClass);
				if (result == null) result = caseReferenceQualifier(targetClass);
				if (result == null) result = caseQualifier(targetClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CONTAINMENT_REFERENCE: {
				ContainmentReference containmentReference = (ContainmentReference)theEObject;
				T result = caseContainmentReference(containmentReference);
				if (result == null) result = caseReferenceQualifier(containmentReference);
				if (result == null) result = caseQualifier(containmentReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PATH_QUALIFIER: {
				PathQualifier pathQualifier = (PathQualifier)theEObject;
				T result = casePathQualifier(pathQualifier);
				if (result == null) result = caseQualifier(pathQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PATH_SIDE: {
				PathSide pathSide = (PathSide)theEObject;
				T result = casePathSide(pathSide);
				if (result == null) result = casePathQualifier(pathSide);
				if (result == null) result = caseQualifier(pathSide);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PATH_TARGET: {
				PathTarget pathTarget = (PathTarget)theEObject;
				T result = casePathTarget(pathTarget);
				if (result == null) result = casePathSide(pathTarget);
				if (result == null) result = casePathQualifier(pathTarget);
				if (result == null) result = caseQualifier(pathTarget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PATH_SOURCE: {
				PathSource pathSource = (PathSource)theEObject;
				T result = casePathSource(pathSource);
				if (result == null) result = casePathSide(pathSource);
				if (result == null) result = casePathQualifier(pathSource);
				if (result == null) result = caseQualifier(pathSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ELEMENT_FEATUREMENT: {
				ElementFeaturement elementFeaturement = (ElementFeaturement)theEObject;
				T result = caseElementFeaturement(elementFeaturement);
				if (result == null) result = casePathQualifier(elementFeaturement);
				if (result == null) result = caseQualifier(elementFeaturement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.STEP_CLASS: {
				StepClass stepClass = (StepClass)theEObject;
				T result = caseStepClass(stepClass);
				if (result == null) result = casePathSide(stepClass);
				if (result == null) result = casePathQualifier(stepClass);
				if (result == null) result = caseQualifier(stepClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.EXISTANCE: {
				Existance existance = (Existance)theEObject;
				T result = caseExistance(existance);
				if (result == null) result = caseElementQualifier(existance);
				if (result == null) result = casePathQualifier(existance);
				if (result == null) result = caseQualifier(existance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.SELECTOR: {
				Selector selector = (Selector)theEObject;
				T result = caseSelector(selector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ELEMENT_SELECTOR: {
				ElementSelector elementSelector = (ElementSelector)theEObject;
				T result = caseElementSelector(elementSelector);
				if (result == null) result = caseSelector(elementSelector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CLASS_SELECTOR: {
				ClassSelector classSelector = (ClassSelector)theEObject;
				T result = caseClassSelector(classSelector);
				if (result == null) result = caseSelector(classSelector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.FEATURE_SELECTOR: {
				FeatureSelector featureSelector = (FeatureSelector)theEObject;
				T result = caseFeatureSelector(featureSelector);
				if (result == null) result = caseSelector(featureSelector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR: {
				AttributeSelector attributeSelector = (AttributeSelector)theEObject;
				T result = caseAttributeSelector(attributeSelector);
				if (result == null) result = caseSelector(attributeSelector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.REFERENCE_SELECTOR: {
				ReferenceSelector referenceSelector = (ReferenceSelector)theEObject;
				T result = caseReferenceSelector(referenceSelector);
				if (result == null) result = caseSelector(referenceSelector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PATH_SELECTOR: {
				PathSelector pathSelector = (PathSelector)theEObject;
				T result = casePathSelector(pathSelector);
				if (result == null) result = caseSelector(pathSelector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.AND_QUALIFIER: {
				AndQualifier andQualifier = (AndQualifier)theEObject;
				T result = caseAndQualifier(andQualifier);
				if (result == null) result = caseCompoundQualifier(andQualifier);
				if (result == null) result = caseQualifier(andQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.OR_QUALIFIER: {
				OrQualifier orQualifier = (OrQualifier)theEObject;
				T result = caseOrQualifier(orQualifier);
				if (result == null) result = caseCompoundQualifier(orQualifier);
				if (result == null) result = caseQualifier(orQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.COMPOUND_QUALIFIER: {
				CompoundQualifier compoundQualifier = (CompoundQualifier)theEObject;
				T result = caseCompoundQualifier(compoundQualifier);
				if (result == null) result = caseQualifier(compoundQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.COMPOUND_PATH_QUALIFIER: {
				CompoundPathQualifier compoundPathQualifier = (CompoundPathQualifier)theEObject;
				T result = caseCompoundPathQualifier(compoundPathQualifier);
				if (result == null) result = casePathQualifier(compoundPathQualifier);
				if (result == null) result = caseQualifier(compoundPathQualifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PATH_AND: {
				PathAnd pathAnd = (PathAnd)theEObject;
				T result = casePathAnd(pathAnd);
				if (result == null) result = caseCompoundPathQualifier(pathAnd);
				if (result == null) result = casePathQualifier(pathAnd);
				if (result == null) result = caseQualifier(pathAnd);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PATH_OR: {
				PathOr pathOr = (PathOr)theEObject;
				T result = casePathOr(pathOr);
				if (result == null) result = caseCompoundPathQualifier(pathOr);
				if (result == null) result = casePathQualifier(pathOr);
				if (result == null) result = caseQualifier(pathOr);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ATTRIBUTE_CONTAINMENT: {
				AttributeContainment attributeContainment = (AttributeContainment)theEObject;
				T result = caseAttributeContainment(attributeContainment);
				if (result == null) result = caseElementRelation(attributeContainment);
				if (result == null) result = caseClassQualifier(attributeContainment);
				if (result == null) result = caseQualifier(attributeContainment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.REFERENCE_CONTAINMENT: {
				ReferenceContainment referenceContainment = (ReferenceContainment)theEObject;
				T result = caseReferenceContainment(referenceContainment);
				if (result == null) result = caseElementRelation(referenceContainment);
				if (result == null) result = caseClassQualifier(referenceContainment);
				if (result == null) result = caseQualifier(referenceContainment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CONT_ROOT: {
				ContRoot contRoot = (ContRoot)theEObject;
				T result = caseContRoot(contRoot);
				if (result == null) result = caseClassQualifier(contRoot);
				if (result == null) result = caseQualifier(contRoot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CONT_LEAF: {
				ContLeaf contLeaf = (ContLeaf)theEObject;
				T result = caseContLeaf(contLeaf);
				if (result == null) result = caseClassQualifier(contLeaf);
				if (result == null) result = caseQualifier(contLeaf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ELEMENT_RELATION: {
				ElementRelation elementRelation = (ElementRelation)theEObject;
				T result = caseElementRelation(elementRelation);
				if (result == null) result = caseClassQualifier(elementRelation);
				if (result == null) result = caseQualifier(elementRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CLASS_REACHES: {
				ClassReaches classReaches = (ClassReaches)theEObject;
				T result = caseClassReaches(classReaches);
				if (result == null) result = caseClassRelation(classReaches);
				if (result == null) result = caseClassQualifier(classReaches);
				if (result == null) result = caseQualifier(classReaches);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CLASS_REACHED_BY: {
				ClassReachedBy classReachedBy = (ClassReachedBy)theEObject;
				T result = caseClassReachedBy(classReachedBy);
				if (result == null) result = caseClassRelation(classReachedBy);
				if (result == null) result = caseClassQualifier(classReachedBy);
				if (result == null) result = caseQualifier(classReachedBy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CONTAINMENT_PATH: {
				ContainmentPath containmentPath = (ContainmentPath)theEObject;
				T result = caseContainmentPath(containmentPath);
				if (result == null) result = casePathQualifier(containmentPath);
				if (result == null) result = caseQualifier(containmentPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.WITH_INHERITANCE_PATH: {
				WithInheritancePath withInheritancePath = (WithInheritancePath)theEObject;
				T result = caseWithInheritancePath(withInheritancePath);
				if (result == null) result = casePathQualifier(withInheritancePath);
				if (result == null) result = caseQualifier(withInheritancePath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CYCLIC_PATH: {
				CyclicPath cyclicPath = (CyclicPath)theEObject;
				T result = caseCyclicPath(cyclicPath);
				if (result == null) result = casePathQualifier(cyclicPath);
				if (result == null) result = caseQualifier(cyclicPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.SOME: {
				Some some = (Some)theEObject;
				T result = caseSome(some);
				if (result == null) result = caseQuantifier(some);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.INHERITED_FEATURE: {
				InheritedFeature inheritedFeature = (InheritedFeature)theEObject;
				T result = caseInheritedFeature(inheritedFeature);
				if (result == null) result = caseFeatureQualifier(inheritedFeature);
				if (result == null) result = caseQualifier(inheritedFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.CLASS_COLLECTS: {
				ClassCollects classCollects = (ClassCollects)theEObject;
				T result = caseClassCollects(classCollects);
				if (result == null) result = caseClassReaches(classCollects);
				if (result == null) result = caseClassRelation(classCollects);
				if (result == null) result = caseClassQualifier(classCollects);
				if (result == null) result = caseQualifier(classCollects);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ANNOTATED: {
				Annotated annotated = (Annotated)theEObject;
				T result = caseAnnotated(annotated);
				if (result == null) result = caseElementQualifier(annotated);
				if (result == null) result = caseQualifier(annotated);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.IMPORT: {
				Import import_ = (Import)theEObject;
				T result = caseImport(import_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PARAMETER_VALUE: {
				ParameterValue parameterValue = (ParameterValue)theEObject;
				T result = caseParameterValue(parameterValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.SELF_INHERITANCE: {
				SelfInheritance selfInheritance = (SelfInheritance)theEObject;
				T result = caseSelfInheritance(selfInheritance);
				if (result == null) result = caseInheritance(selfInheritance);
				if (result == null) result = caseClassQualifier(selfInheritance);
				if (result == null) result = caseQualifier(selfInheritance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ANNOTATION: {
				Annotation annotation = (Annotation)theEObject;
				T result = caseAnnotation(annotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ANNOTATED_ELEMENT: {
				AnnotatedElement annotatedElement = (AnnotatedElement)theEObject;
				T result = caseAnnotatedElement(annotatedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.DESCRIBED_ELEMENT: {
				DescribedElement describedElement = (DescribedElement)theEObject;
				T result = caseDescribedElement(describedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.SYNONYM: {
				Synonym synonym = (Synonym)theEObject;
				T result = caseSynonym(synonym);
				if (result == null) result = caseWordNature(synonym);
				if (result == null) result = caseNamed(synonym);
				if (result == null) result = caseNamePart(synonym);
				if (result == null) result = caseElementQualifier(synonym);
				if (result == null) result = caseQualifier(synonym);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.VERB: {
				Verb verb = (Verb)theEObject;
				T result = caseVerb(verb);
				if (result == null) result = caseWordNature(verb);
				if (result == null) result = caseNamed(verb);
				if (result == null) result = caseNamePart(verb);
				if (result == null) result = caseElementQualifier(verb);
				if (result == null) result = caseQualifier(verb);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.NOUN: {
				Noun noun = (Noun)theEObject;
				T result = caseNoun(noun);
				if (result == null) result = caseWordNature(noun);
				if (result == null) result = caseNamed(noun);
				if (result == null) result = caseNamePart(noun);
				if (result == null) result = caseElementQualifier(noun);
				if (result == null) result = caseQualifier(noun);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.ADJECTIVE: {
				Adjective adjective = (Adjective)theEObject;
				T result = caseAdjective(adjective);
				if (result == null) result = caseWordNature(adjective);
				if (result == null) result = caseNamed(adjective);
				if (result == null) result = caseNamePart(adjective);
				if (result == null) result = caseElementQualifier(adjective);
				if (result == null) result = caseQualifier(adjective);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.WORD_NATURE: {
				WordNature wordNature = (WordNature)theEObject;
				T result = caseWordNature(wordNature);
				if (result == null) result = caseNamed(wordNature);
				if (result == null) result = caseNamePart(wordNature);
				if (result == null) result = caseElementQualifier(wordNature);
				if (result == null) result = caseQualifier(wordNature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PHRASE_NAMED: {
				PhraseNamed phraseNamed = (PhraseNamed)theEObject;
				T result = casePhraseNamed(phraseNamed);
				if (result == null) result = caseElementQualifier(phraseNamed);
				if (result == null) result = caseQualifier(phraseNamed);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.NAME_PART: {
				NamePart namePart = (NamePart)theEObject;
				T result = caseNamePart(namePart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.LITERAL: {
				Literal literal = (Literal)theEObject;
				T result = caseLiteral(literal);
				if (result == null) result = caseNamePart(literal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.TRIVIAL_STRING: {
				TrivialString trivialString = (TrivialString)theEObject;
				T result = caseTrivialString(trivialString);
				if (result == null) result = caseNamePart(trivialString);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.OPPOSITE_TO: {
				OppositeTo oppositeTo = (OppositeTo)theEObject;
				T result = caseOppositeTo(oppositeTo);
				if (result == null) result = caseReferenceQualifier(oppositeTo);
				if (result == null) result = caseQualifier(oppositeTo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PARAMETER_STRING_VALUE: {
				ParameterStringValue parameterStringValue = (ParameterStringValue)theEObject;
				T result = caseParameterStringValue(parameterStringValue);
				if (result == null) result = caseParameterValue(parameterStringValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.PARAMETER_ELEMENT_SET_VALUE: {
				ParameterElementSetValue parameterElementSetValue = (ParameterElementSetValue)theEObject;
				T result = caseParameterElementSetValue(parameterElementSetValue);
				if (result == null) result = caseParameterValue(parameterElementSetValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.NAMED_AFTER: {
				NamedAfter namedAfter = (NamedAfter)theEObject;
				T result = caseNamedAfter(namedAfter);
				if (result == null) result = caseElementQualifier(namedAfter);
				if (result == null) result = caseQualifier(namedAfter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetamodeltestPackage.OCL_EXPRESSION: {
				OCLExpression oclExpression = (OCLExpression)theEObject;
				T result = caseOCLExpression(oclExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Meta Model Test</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Meta Model Test</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetaModelTest(MetaModelTest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assertion</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assertion</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertion(Assertion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quantifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quantifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuantifier(Quantifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>None</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>None</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNone(None object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumber(metamodeltest.Number object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Every</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Every</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvery(Every object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Less Than</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Less Than</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLessThan(LessThan object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>More Than</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>More Than</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMoreThan(MoreThan object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Assertion</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Assertion</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementAssertion(ElementAssertion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseName(Name object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstract(Abstract object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFilter(Filter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementFilter(ElementFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassFilter(ClassFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureFilter(FeatureFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeFilter(AttributeFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceFilter(ReferenceFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementQualifier(ElementQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualifier(Qualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassQualifier(ClassQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureQualifier(FeatureQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeQualifier(AttributeQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceQualifier(ReferenceQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstraction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstraction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstraction(Abstraction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Super To</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Super To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsSuperTo(IsSuperTo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Is Sub To</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Is Sub To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIsSubTo(IsSubTo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassRelation(ClassRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Containment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Containment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureContainment(FeatureContainment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inherited Feature Containment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inherited Feature Containment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInheritedFeatureContainment(InheritedFeatureContainment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inh Leaf</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inh Leaf</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInhLeaf(InhLeaf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inh Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inh Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInhRoot(InhRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composition Leaf</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composition Leaf</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositionLeaf(CompositionLeaf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Max Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Max Multiplicity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMaxMultiplicity(MaxMultiplicity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fixed Bound</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fixed Bound</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixedBound(FixedBound object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Infinite</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Infinite</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInfinite(Infinite object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Containment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Containment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassContainment(ClassContainment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Containee</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Containee</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassContainee(ClassContainee object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Typed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Typed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTyped(Typed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Prefix</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Prefix</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrefix(Prefix object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Suffix</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Suffix</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSuffix(Suffix object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contains</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contains</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContains(Contains object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Side Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Side Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSideClass(SideClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>From To</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>From To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFromTo(FromTo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Strict Number</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Strict Number</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStrictNumber(StrictNumber object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inheritance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inheritance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInheritance(Inheritance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Multiplicity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureMultiplicity(FeatureMultiplicity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Upper Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Upper Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUpperCase(UpperCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lower Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lower Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLowerCase(LowerCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Substring</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Substring</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubstring(Substring object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assertion Library</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assertion Library</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertionLibrary(AssertionLibrary object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assertion Test</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assertion Test</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertionTest(AssertionTest object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assertion Definition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assertion Definition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertionDefinition(AssertionDefinition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Library Assertion Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Library Assertion Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLibraryAssertionCall(LibraryAssertionCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Assertion Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Assertion Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssertionCall(AssertionCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanParameter(BooleanParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Integer Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Integer Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntegerParameter(IntegerParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringParameter(StringParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Defined Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Defined Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserDefinedParameter(UserDefinedParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathFilter(PathFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Length</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Length</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLength(Length object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Source Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Source Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSourceClass(SourceClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Target Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Target Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTargetClass(TargetClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Containment Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Containment Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainmentReference(ContainmentReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathQualifier(PathQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Side</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Side</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathSide(PathSide object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathTarget(PathTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathSource(PathSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Featurement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Featurement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementFeaturement(ElementFeaturement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Step Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Step Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStepClass(StepClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Existance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Existance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExistance(Existance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Selector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Selector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelector(Selector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Selector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Selector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementSelector(ElementSelector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Selector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Selector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassSelector(ClassSelector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Selector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Selector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureSelector(FeatureSelector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Selector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Selector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeSelector(AttributeSelector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Selector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Selector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceSelector(ReferenceSelector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Selector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Selector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathSelector(PathSelector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>And Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAndQualifier(AndQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrQualifier(OrQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compound Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compound Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompoundQualifier(CompoundQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Compound Path Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Compound Path Qualifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompoundPathQualifier(CompoundPathQualifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path And</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path And</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathAnd(PathAnd object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Path Or</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Path Or</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePathOr(PathOr object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Containment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Containment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeContainment(AttributeContainment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Reference Containment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Reference Containment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReferenceContainment(ReferenceContainment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cont Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cont Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContRoot(ContRoot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cont Leaf</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cont Leaf</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContLeaf(ContLeaf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElementRelation(ElementRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Reaches</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Reaches</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassReaches(ClassReaches object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Reached By</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Reached By</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassReachedBy(ClassReachedBy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Containment Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Containment Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainmentPath(ContainmentPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>With Inheritance Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>With Inheritance Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWithInheritancePath(WithInheritancePath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cyclic Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cyclic Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCyclicPath(CyclicPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Some</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Some</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSome(Some object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Inherited Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Inherited Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInheritedFeature(InheritedFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Collects</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Collects</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassCollects(ClassCollects object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotated(Annotated object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImport(Import object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterValue(ParameterValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Self Inheritance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Self Inheritance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelfInheritance(SelfInheritance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotation(Annotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Described Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDescribedElement(DescribedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Synonym</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Synonym</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSynonym(Synonym object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Verb</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Verb</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVerb(Verb object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Noun</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Noun</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNoun(Noun object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Adjective</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Adjective</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAdjective(Adjective object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Word Nature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Word Nature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWordNature(WordNature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Phrase Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Phrase Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePhraseNamed(PhraseNamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Name Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Name Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamePart(NamePart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLiteral(Literal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trivial String</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trivial String</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTrivialString(TrivialString object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Opposite To</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Opposite To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOppositeTo(OppositeTo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter String Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter String Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterStringValue(ParameterStringValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Element Set Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Element Set Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterElementSetValue(ParameterElementSetValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named After</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named After</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedAfter(NamedAfter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OCL Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OCL Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOCLExpression(OCLExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MetamodeltestSwitch
