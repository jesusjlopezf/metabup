/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.popup.actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import metabest.test.metamodel.MBMRuntimeModule;
import metabest.validation.MetamodelAssertionEvaluationOutput;
import metabest.validation.MetamodelPermutedAssertionEvaluationOutput;
import metabup.metamodel.MetaModel;
import metamodeltest.Annotation;
import metamodeltest.Assertion;
import metamodeltest.AssertionCall;
import metamodeltest.AssertionTest;
import metamodeltest.LibraryAssertionCall;
import metamodeltest.MetaModelTest;
import metamodeltest.ParameterElementSetValue;
import metamodeltest.impl.AssertionImpl;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.xtext.serializer.impl.Serializer;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;


public class MetaBestMetamodelValidateAndExportAction extends MetaBestMetamodelValidationAction {
	Resource metamodelTestResource;
	IProject project;
	List<String> mmURIs = new ArrayList<String>();
	List<File> outputFiles = new ArrayList<File>();
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if(selection instanceof IStructuredSelection){
			Object obj[] = ((IStructuredSelection) selection).toArray();
			IFile file = IFileUtils.convertObjectToIFile(obj[0]);
			ResourceSet rs = new ResourceSetImpl();
			
			org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(file.getLocationURI().toString());
			metamodelTestResource = rs.getResource(uri, true);	
			project = ((IFile)((IStructuredSelection)selection).getFirstElement()).getProject();
			
			refreshJob.schedule();
		}

	}
	
	Job refreshJob = new Job("Performing tests"){
		@Override 
	    protected IStatus run(IProgressMonitor monitor) {
			long start = System.currentTimeMillis();
			
			if(metamodelTestResource.getAllContents().next() instanceof MetaModelTest){
				MetaModelTest mmt = (MetaModelTest) metamodelTestResource.getContents().get(0);
				if(mmt.getTests() == null || mmt.getTests().isEmpty()) return Status.CANCEL_STATUS;
				
				int totalTests = 0;				
				for(AssertionTest at : mmt.getTests()) totalTests++;
				
		        monitor.beginTask("Performing tests ...", totalTests);
		        
				for(AssertionTest at : mmt.getTests()){
					File file, timeFile, oclTimeFile;									
					BufferedWriter bw = null, tbw = null, otbw = null;

					try {
						IFolder testFilesFolder = project.getFolder("testfiles");
						if(!testFilesFolder.exists()) testFilesFolder.create(true, true, monitor);
						
						int i=0;
						
						do{						
							if(i == 0) file = new File(testFilesFolder.getFile(at.getDescription() + ".csv").getLocation().toString());
							else file = new File(testFilesFolder.getFile(at.getDescription() + i +".csv").getLocation().toString());
							i++;
						}while(file.exists());
						
						i=0;
						
						do{						
							if(i == 0) timeFile = new File(testFilesFolder.getFile(at.getDescription() + "Time" + ".csv").getLocation().toString());
							else timeFile = new File(testFilesFolder.getFile(at.getDescription() + "Time" + i +".csv").getLocation().toString());
							i++;
						}while(timeFile.exists());
						
						i=0;
						
						do{						
							if(i == 0) oclTimeFile = new File(testFilesFolder.getFile(at.getDescription() + "OclTime" + ".csv").getLocation().toString());
							else oclTimeFile = new File(testFilesFolder.getFile(at.getDescription() + "OclTime" + i +".csv").getLocation().toString());
							i++;
						}while(oclTimeFile.exists());
						
						if(!file.createNewFile() || !timeFile.createNewFile() || !oclTimeFile.createNewFile()) return Status.CANCEL_STATUS;
						
						FileWriter fw = new FileWriter(file.getAbsoluteFile());
						bw = new BufferedWriter(fw);
						
						FileWriter tfw = new FileWriter(timeFile.getAbsoluteFile());
						tbw = new BufferedWriter(tfw);
						
						FileWriter otfw = new FileWriter(oclTimeFile.getAbsoluteFile());
						otbw = new BufferedWriter(otfw);
						
						bw.write("Model;Classes;Attributes;References");
						tbw.write("Model;Classes;Attributes;References");
						otbw.write("Model;Classes;Attributes;References");
						
						for(AssertionCall ac : at.getAssertions()) bw.write(";\"" + ac.getDescription().replace(";", "") + "\"");
						for(AssertionCall ac : at.getAssertions()) tbw.write(";\"" + ac.getDescription().replace(";", "") + "\"");
						for(AssertionCall ac : at.getAssertions()) otbw.write(";\"" + ac.getDescription().replace(";", "") + "\"");
						
						bw.write("Runtime");
						tbw.write("Total");
						otbw.write("Total");
						
						mmURIs.clear();
						
						if(at.getMetamodelURI() != null) mmURIs.add(at.getMetamodelURI());
						else{
							IContainer dir;
							if(at.getDirURI() != null) dir = project.getFolder(at.getDirURI());
							else dir = project;
							
							for(IResource res : dir.members(false))
								if(res.getFileExtension() != null && res.getFileExtension().equals("mbup"))
									mmURIs.add(res.getFullPath().toOSString());														
						}
						
						for(String metamodelUri : mmURIs){
							String metamodelName = metamodelUri.substring(metamodelUri.lastIndexOf("\\")+1, metamodelUri.indexOf("."));
							bw.write("\n\"" + metamodelName + "\"");						
							tbw.write("\n\"" + metamodelName + "\"");
							otbw.write("\n\"" + metamodelName + "\"");
							
							//System.out.println("checking: " + metamodelUri);
							
							IWorkspace workspace = ResourcesPlugin.getWorkspace();
							IPath p = new Path(metamodelUri.replace(".mbup", ".mm.xmi"));
						    IFile mmFile = workspace.getRoot().getFile(p);
						    if(!mmFile.exists()) return Status.CANCEL_STATUS;						    						    
						    
						    ResourceSet rs = new ResourceSetImpl();
						    org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(mmFile.getLocationURI().toString());
							Resource r = rs.getResource(uri, true);
							org.eclipse.emf.common.util.TreeIterator<EObject> it = r.getAllContents();
							
							MetaModel mm = null;
							
							while(it.hasNext() && mm == null){
								EObject eo = it.next();
								if(eo instanceof MetaModel){
									mm = (MetaModel)eo;
								}
							}
							
							bw.write(";" + mm.getClasses().size());
							tbw.write(";" + mm.getClasses().size());
							otbw.write(";" + mm.getClasses().size());
							
							bw.write(";" + mm.getAttributes().size());
							tbw.write(";" + mm.getAttributes().size());
							otbw.write(";" + mm.getAttributes().size());
							
							bw.write(";" + mm.getReferences().size());
							tbw.write(";" + mm.getReferences().size());
							otbw.write(";" + mm.getReferences().size());
							
							long time = 0;
							
							for(AssertionCall ac : at.getAssertions()){
								// spread test annotations to test assertions
								ac.getAnnotations().addAll(EcoreUtil.copyAll(at.getAnnotations()));
								
								Assertion a = null;
								String name = null;
								List<ParameterElementSetValue> permuteParams = new ArrayList<ParameterElementSetValue>();
								
								if(ac instanceof LibraryAssertionCall){
									LibraryAssertionCall lac = (LibraryAssertionCall) ac;
									permuteParams = lac.getPermutedParameters();
									
									a = EcoreUtil.copy((AssertionImpl)(lac.getReferencedAssertion().getAssertion()));
									a.getAnnotations().addAll(EcoreUtil.copyAll(lac.getAnnotations()));
									if(lac.getDescription() != null) a.setDescription(lac.getDescription());
									List<String> output = null;
									
									if(lac.getParameters() != null && !lac.getParameters().isEmpty()){
										output = a.putValuesOnParameters(lac.getParameters());
										
										if(!output.isEmpty()){
											 final String PID = metabest.Activator.PLUGIN_ID;
											 MultiStatus info = new MultiStatus(PID, 1, ((LibraryAssertionCall) a).getReferencedAssertion().getName() + " call returned errors", null);
											 for(String msg : output) info.add(new Status(IStatus.ERROR, PID, 1, msg, null));
											 ErrorDialog.openError(null, "Error", null, info);
										}									
									}
									
									if(a.getDescription() != null) name = a.getDescription();
									else{
										Injector injector = Guice.createInjector(new MBMRuntimeModule());
										Serializer serializer = injector.getInstance(Serializer.class);
										name = serializer.serialize(a);
											
										if(name.startsWith("@")){
											name = name.substring(name.lastIndexOf("@"), name.length());
											String substring = name.substring(name.lastIndexOf("@"), name.indexOf(" "));
											name = name.substring(substring.length()+1);
										}
									}
								}else if(ac instanceof Assertion){
									a = (Assertion)ac;
									
									if(a.getDescription() != null) name = a.getDescription();
									else{
										Injector injector = Guice.createInjector(new MBMRuntimeModule());
										Serializer serializer = injector.getInstance(Serializer.class);
										name = serializer.serialize(a);
										
										if(name.startsWith("@")){
											name = name.substring(name.lastIndexOf("@"), name.length());
											String substring = name.substring(name.lastIndexOf("@"), name.indexOf(" "));
											name = name.substring(substring.length()+1);
										}
									}
								}
								
								boolean warning = false;
								for(Annotation ann : a.getAnnotations()) if(ann.getName().equals("warning")) warning = true;																					    				    								    
								
								if(mm != null){
									if(permuteParams.isEmpty() || permuteParams == null){
										MetamodelAssertionEvaluationOutput ev = new MetamodelAssertionEvaluationOutput(a, name, warning);
										
										//System.out.println("About to evaluate: " + a.getDescription());
										
										ev.evaluate(mm);
										time += ev.getTime();
										System.out.println("Finished evaluating: " + a.getDescription() + " for " + metamodelName + ". Time = " + Double.toString((double)(ev.getTime()*0.001)));
										System.out.println(ev.getOclEvaluation());
										
										if(ev.isTrue()) bw.write(";1");									
										else if(!ev.isTrue() && ev.isWarning()) bw.write(";0");
										else if(!ev.isTrue() && !ev.isWarning()) bw.write(";-1");
										
										tbw.write(";" + Double.toString(ev.getTime()).replace(".", ","));
										otbw.write(";" + Double.toString(ev.getOclTime()).replace(".", ","));										
									}else{
										//permuted evaluation
										MetamodelPermutedAssertionEvaluationOutput pev = new MetamodelPermutedAssertionEvaluationOutput(a, name, warning);
										
										//System.out.println("About to evaluate: " + a.getDescription());
										
										pev.evaluate(mm, permuteParams);
										time += pev.getTime();
										System.out.println("Finished evaluating: " + a.getDescription() + " for " + metamodelName + ". Time = " + Double.toString((double)(pev.getTime()*0.001)));
										
										if(pev.isTrue()) bw.write(";1");									
										else if(!pev.isTrue() && pev.isWarning()) bw.write(";0");
										else if(!pev.isTrue() && !pev.isWarning()) bw.write(";-1");
										
										tbw.write(";" + Double.toString(pev.getTime()).replace(".", ","));
										otbw.write(";" + Double.toString(pev.getOclTime()).replace(".", ","));										
									}
								}								
						    }
							
							double doubleTime = (double)(time*0.001);
							bw.write(";" + Double.toString(doubleTime));
							tbw.write("; ");
							otbw.write("; ");
							System.out.println(Double.toString(doubleTime));
						}
						
						bw.close();		
						tbw.close();
						otbw.close();

					} catch (IOException e) {
						if(bw != null)
							try {
								bw.close();
							} catch (IOException e1) {
								errorDialog();
								return Status.CANCEL_STATUS;
							}
						
						if(tbw != null)
							try {
								tbw.close();
							} catch (IOException e1) {
								errorDialog();
								return Status.CANCEL_STATUS;
							}
						
						if(otbw != null)
							try {
								otbw.close();
							} catch (IOException e1) {
								errorDialog();
								return Status.CANCEL_STATUS;
							}

						errorDialog();
						return Status.CANCEL_STATUS;
					} catch (CoreException e) {
						if(bw != null)
							try {
								bw.close();
							} catch (IOException e1) {
								errorDialog();
								return Status.CANCEL_STATUS;
							}		
						
						if(tbw != null)
							try {
								tbw.close();
							} catch (IOException e1) {
								errorDialog();
								return Status.CANCEL_STATUS;
							}
						
						if(otbw != null)
							try {
								otbw.close();
							} catch (IOException e1) {
								errorDialog();
								return Status.CANCEL_STATUS;
							}	
						
						errorDialog();
						return Status.CANCEL_STATUS;
					}	
					
					monitor.worked(1);	
				}
			}
			
	        monitor.done();      
	        okDialog(((double)System.currentTimeMillis()-start)*0.001);
	        return Status.OK_STATUS; 
	    } 		
	};
	
	private void okDialog(double seconds) {
		final double s = seconds;
	    Display.getDefault().asyncExec(new Runnable() {
	      public void run() {
	    	  MessageDialog.openInformation(null, "Output files generated", "The test output files were successfully generated (" + Double.toString(s) + " s.)");
	    	  try {
				project.refreshLocal(IResource.DEPTH_INFINITE, null);
	    	  } catch (CoreException e) {
	    	  }
	  		return;
	      }
	    });
	}
	
	private void errorDialog() {
	    Display.getDefault().asyncExec(new Runnable() {
	      public void run() {
	    	  MessageDialog.openError(null, "Error", "The evaluation couldn't be finished due to unexpected problems.");
	    	  try {
				project.refreshLocal(IResource.DEPTH_INFINITE, null);
	    	  } catch (CoreException e) {
	    	  }
	  		return;
	      }
	    });
	}

}

