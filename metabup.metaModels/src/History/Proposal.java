/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package History;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Proposal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link History.Proposal#getSols <em>Sols</em>}</li>
 *   <li>{@link History.Proposal#getSelected <em>Selected</em>}</li>
 *   <li>{@link History.Proposal#getVersion <em>Version</em>}</li>
 *   <li>{@link History.Proposal#isAccepted <em>Accepted</em>}</li>
 *   <li>{@link History.Proposal#getMeta <em>Meta</em>}</li>
 *   <li>{@link History.Proposal#getConflictWith <em>Conflict With</em>}</li>
 * </ul>
 * </p>
 *
 * @see History.HistoryPackage#getProposal()
 * @model
 * @generated
 */
public interface Proposal extends Collaboration {
	/**
	 * Returns the value of the '<em><b>Sols</b></em>' containment reference list.
	 * The list contents are of type {@link History.Solution}.
	 * It is bidirectional and its opposite is '{@link History.Solution#getProposal <em>Proposal</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sols</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sols</em>' containment reference list.
	 * @see History.HistoryPackage#getProposal_Sols()
	 * @see History.Solution#getProposal
	 * @model opposite="proposal" containment="true"
	 * @generated
	 */
	EList<Solution> getSols();

	/**
	 * Returns the value of the '<em><b>Selected</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected</em>' reference.
	 * @see #setSelected(Solution)
	 * @see History.HistoryPackage#getProposal_Selected()
	 * @model
	 * @generated
	 */
	Solution getSelected();

	/**
	 * Sets the value of the '{@link History.Proposal#getSelected <em>Selected</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selected</em>' reference.
	 * @see #getSelected()
	 * @generated
	 */
	void setSelected(Solution value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link History.Version#getProposals <em>Proposals</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' container reference.
	 * @see #setVersion(Version)
	 * @see History.HistoryPackage#getProposal_Version()
	 * @see History.Version#getProposals
	 * @model opposite="proposals" transient="false"
	 * @generated
	 */
	Version getVersion();

	/**
	 * Sets the value of the '{@link History.Proposal#getVersion <em>Version</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' container reference.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(Version value);

	/**
	 * Returns the value of the '<em><b>Accepted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accepted</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accepted</em>' attribute.
	 * @see #setAccepted(boolean)
	 * @see History.HistoryPackage#getProposal_Accepted()
	 * @model
	 * @generated
	 */
	boolean isAccepted();

	/**
	 * Sets the value of the '{@link History.Proposal#isAccepted <em>Accepted</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accepted</em>' attribute.
	 * @see #isAccepted()
	 * @generated
	 */
	void setAccepted(boolean value);

	/**
	 * Returns the value of the '<em><b>Meta</b></em>' containment reference list.
	 * The list contents are of type {@link History.MetaInfo}.
	 * It is bidirectional and its opposite is '{@link History.MetaInfo#getProposal <em>Proposal</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Meta</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Meta</em>' containment reference list.
	 * @see History.HistoryPackage#getProposal_Meta()
	 * @see History.MetaInfo#getProposal
	 * @model opposite="proposal" containment="true"
	 * @generated
	 */
	EList<MetaInfo> getMeta();

	/**
	 * Returns the value of the '<em><b>Conflict With</b></em>' reference list.
	 * The list contents are of type {@link History.Proposal}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conflict With</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conflict With</em>' reference list.
	 * @see History.HistoryPackage#getProposal_ConflictWith()
	 * @model
	 * @generated
	 */
	EList<Proposal> getConflictWith();

} // Proposal
