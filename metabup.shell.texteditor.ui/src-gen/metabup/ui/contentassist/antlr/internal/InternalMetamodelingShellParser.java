package metabup.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import metabup.services.MetamodelingShellGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMetamodelingShellParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'ref'", "'rel'", "'true'", "'false'", "'positive'", "'negative'", "'line'", "'dashed'", "'dashed-dotted'", "'dotted'", "'circle'", "'concave'", "'convex'", "'crows-foot-many'", "'crows-foot-many-mandatory'", "'crows-foot-many-optional'", "'crows-foot-one'", "'crows-foot-one-mandatory'", "'crows-foot-one-optional'", "'dash'", "'delta'", "'diamond'", "'none'", "'plain'", "'short'", "'skewed-dash'", "'standard'", "'transparent-circle'", "'t-shape'", "'white-delta'", "'white-diamond'", "'shell'", "'fragment'", "'{'", "'}'", "'['", "']'", "':'", "';'", "'@'", "'('", "')'", "','", "'->'", "'.'", "'='", "'attr'", "'style'", "'color'", "'width'", "'source'", "'target'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__62=62;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalMetamodelingShellParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMetamodelingShellParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMetamodelingShellParser.tokenNames; }
    public String getGrammarFileName() { return "../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g"; }


     
     	private MetamodelingShellGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(MetamodelingShellGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleShellModel"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:60:1: entryRuleShellModel : ruleShellModel EOF ;
    public final void entryRuleShellModel() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:61:1: ( ruleShellModel EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:62:1: ruleShellModel EOF
            {
             before(grammarAccess.getShellModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleShellModel_in_entryRuleShellModel61);
            ruleShellModel();

            state._fsp--;

             after(grammarAccess.getShellModelRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleShellModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleShellModel"


    // $ANTLR start "ruleShellModel"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:69:1: ruleShellModel : ( ( rule__ShellModel__Group__0 ) ) ;
    public final void ruleShellModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:73:2: ( ( ( rule__ShellModel__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:74:1: ( ( rule__ShellModel__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:74:1: ( ( rule__ShellModel__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:75:1: ( rule__ShellModel__Group__0 )
            {
             before(grammarAccess.getShellModelAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:76:1: ( rule__ShellModel__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:76:2: rule__ShellModel__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__ShellModel__Group__0_in_ruleShellModel94);
            rule__ShellModel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getShellModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleShellModel"


    // $ANTLR start "entryRuleCommand"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:88:1: entryRuleCommand : ruleCommand EOF ;
    public final void entryRuleCommand() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:89:1: ( ruleCommand EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:90:1: ruleCommand EOF
            {
             before(grammarAccess.getCommandRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleCommand_in_entryRuleCommand121);
            ruleCommand();

            state._fsp--;

             after(grammarAccess.getCommandRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleCommand128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCommand"


    // $ANTLR start "ruleCommand"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:97:1: ruleCommand : ( ruleFragmentCommand ) ;
    public final void ruleCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:101:2: ( ( ruleFragmentCommand ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:102:1: ( ruleFragmentCommand )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:102:1: ( ruleFragmentCommand )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:103:1: ruleFragmentCommand
            {
             before(grammarAccess.getCommandAccess().getFragmentCommandParserRuleCall()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentCommand_in_ruleCommand154);
            ruleFragmentCommand();

            state._fsp--;

             after(grammarAccess.getCommandAccess().getFragmentCommandParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCommand"


    // $ANTLR start "entryRuleFragmentCommand"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:116:1: entryRuleFragmentCommand : ruleFragmentCommand EOF ;
    public final void entryRuleFragmentCommand() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:117:1: ( ruleFragmentCommand EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:118:1: ruleFragmentCommand EOF
            {
             before(grammarAccess.getFragmentCommandRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentCommand_in_entryRuleFragmentCommand180);
            ruleFragmentCommand();

            state._fsp--;

             after(grammarAccess.getFragmentCommandRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentCommand187); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFragmentCommand"


    // $ANTLR start "ruleFragmentCommand"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:125:1: ruleFragmentCommand : ( ( rule__FragmentCommand__FragmentAssignment ) ) ;
    public final void ruleFragmentCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:129:2: ( ( ( rule__FragmentCommand__FragmentAssignment ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:130:1: ( ( rule__FragmentCommand__FragmentAssignment ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:130:1: ( ( rule__FragmentCommand__FragmentAssignment ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:131:1: ( rule__FragmentCommand__FragmentAssignment )
            {
             before(grammarAccess.getFragmentCommandAccess().getFragmentAssignment()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:132:1: ( rule__FragmentCommand__FragmentAssignment )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:132:2: rule__FragmentCommand__FragmentAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentCommand__FragmentAssignment_in_ruleFragmentCommand213);
            rule__FragmentCommand__FragmentAssignment();

            state._fsp--;


            }

             after(grammarAccess.getFragmentCommandAccess().getFragmentAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFragmentCommand"


    // $ANTLR start "entryRuleFragment"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:144:1: entryRuleFragment : ruleFragment EOF ;
    public final void entryRuleFragment() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:145:1: ( ruleFragment EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:146:1: ruleFragment EOF
            {
             before(grammarAccess.getFragmentRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragment_in_entryRuleFragment240);
            ruleFragment();

            state._fsp--;

             after(grammarAccess.getFragmentRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragment247); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFragment"


    // $ANTLR start "ruleFragment"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:153:1: ruleFragment : ( ( rule__Fragment__Group__0 ) ) ;
    public final void ruleFragment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:157:2: ( ( ( rule__Fragment__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:158:1: ( ( rule__Fragment__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:158:1: ( ( rule__Fragment__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:159:1: ( rule__Fragment__Group__0 )
            {
             before(grammarAccess.getFragmentAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:160:1: ( rule__Fragment__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:160:2: rule__Fragment__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__0_in_ruleFragment273);
            rule__Fragment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFragment"


    // $ANTLR start "entryRuleFeature"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:172:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:173:1: ( ruleFeature EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:174:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_entryRuleFeature300);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeature307); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:181:1: ruleFeature : ( ( rule__Feature__Alternatives ) ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:185:2: ( ( ( rule__Feature__Alternatives ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:186:1: ( ( rule__Feature__Alternatives ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:186:1: ( ( rule__Feature__Alternatives ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:187:1: ( rule__Feature__Alternatives )
            {
             before(grammarAccess.getFeatureAccess().getAlternatives()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:188:1: ( rule__Feature__Alternatives )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:188:2: rule__Feature__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Alternatives_in_ruleFeature333);
            rule__Feature__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleEString"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:200:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:201:1: ( ruleEString EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:202:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString360);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString367); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:209:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:213:2: ( ( ( rule__EString__Alternatives ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:214:1: ( ( rule__EString__Alternatives ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:214:1: ( ( rule__EString__Alternatives ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:215:1: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:216:1: ( rule__EString__Alternatives )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:216:2: rule__EString__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__EString__Alternatives_in_ruleEString393);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleObject"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:228:1: entryRuleObject : ruleObject EOF ;
    public final void entryRuleObject() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:229:1: ( ruleObject EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:230:1: ruleObject EOF
            {
             before(grammarAccess.getObjectRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_entryRuleObject420);
            ruleObject();

            state._fsp--;

             after(grammarAccess.getObjectRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObject427); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObject"


    // $ANTLR start "ruleObject"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:237:1: ruleObject : ( ( rule__Object__Group__0 ) ) ;
    public final void ruleObject() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:241:2: ( ( ( rule__Object__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:242:1: ( ( rule__Object__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:242:1: ( ( rule__Object__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:243:1: ( rule__Object__Group__0 )
            {
             before(grammarAccess.getObjectAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:244:1: ( rule__Object__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:244:2: rule__Object__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__0_in_ruleObject453);
            rule__Object__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObject"


    // $ANTLR start "entryRuleAnnotation"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:256:1: entryRuleAnnotation : ruleAnnotation EOF ;
    public final void entryRuleAnnotation() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:257:1: ( ruleAnnotation EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:258:1: ruleAnnotation EOF
            {
             before(grammarAccess.getAnnotationRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_entryRuleAnnotation480);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getAnnotationRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotation487); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnnotation"


    // $ANTLR start "ruleAnnotation"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:265:1: ruleAnnotation : ( ( rule__Annotation__Group__0 ) ) ;
    public final void ruleAnnotation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:269:2: ( ( ( rule__Annotation__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:270:1: ( ( rule__Annotation__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:270:1: ( ( rule__Annotation__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:271:1: ( rule__Annotation__Group__0 )
            {
             before(grammarAccess.getAnnotationAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:272:1: ( rule__Annotation__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:272:2: rule__Annotation__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__0_in_ruleAnnotation513);
            rule__Annotation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnnotation"


    // $ANTLR start "entryRuleAnnotationParam"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:284:1: entryRuleAnnotationParam : ruleAnnotationParam EOF ;
    public final void entryRuleAnnotationParam() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:285:1: ( ruleAnnotationParam EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:286:1: ruleAnnotationParam EOF
            {
             before(grammarAccess.getAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam540);
            ruleAnnotationParam();

            state._fsp--;

             after(grammarAccess.getAnnotationParamRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotationParam547); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnnotationParam"


    // $ANTLR start "ruleAnnotationParam"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:293:1: ruleAnnotationParam : ( ( rule__AnnotationParam__Group__0 ) ) ;
    public final void ruleAnnotationParam() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:297:2: ( ( ( rule__AnnotationParam__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:298:1: ( ( rule__AnnotationParam__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:298:1: ( ( rule__AnnotationParam__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:299:1: ( rule__AnnotationParam__Group__0 )
            {
             before(grammarAccess.getAnnotationParamAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:300:1: ( rule__AnnotationParam__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:300:2: rule__AnnotationParam__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__Group__0_in_ruleAnnotationParam573);
            rule__AnnotationParam__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationParamAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnnotationParam"


    // $ANTLR start "entryRuleParamValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:312:1: entryRuleParamValue : ruleParamValue EOF ;
    public final void entryRuleParamValue() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:313:1: ( ruleParamValue EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:314:1: ruleParamValue EOF
            {
             before(grammarAccess.getParamValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_entryRuleParamValue600);
            ruleParamValue();

            state._fsp--;

             after(grammarAccess.getParamValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleParamValue607); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParamValue"


    // $ANTLR start "ruleParamValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:321:1: ruleParamValue : ( ( rule__ParamValue__Alternatives ) ) ;
    public final void ruleParamValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:325:2: ( ( ( rule__ParamValue__Alternatives ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:326:1: ( ( rule__ParamValue__Alternatives ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:326:1: ( ( rule__ParamValue__Alternatives ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:327:1: ( rule__ParamValue__Alternatives )
            {
             before(grammarAccess.getParamValueAccess().getAlternatives()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:328:1: ( rule__ParamValue__Alternatives )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:328:2: rule__ParamValue__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__ParamValue__Alternatives_in_ruleParamValue633);
            rule__ParamValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getParamValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParamValue"


    // $ANTLR start "entryRuleObjectValueAnnotationParam"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:340:1: entryRuleObjectValueAnnotationParam : ruleObjectValueAnnotationParam EOF ;
    public final void entryRuleObjectValueAnnotationParam() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:341:1: ( ruleObjectValueAnnotationParam EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:342:1: ruleObjectValueAnnotationParam EOF
            {
             before(grammarAccess.getObjectValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam660);
            ruleObjectValueAnnotationParam();

            state._fsp--;

             after(grammarAccess.getObjectValueAnnotationParamRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam667); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObjectValueAnnotationParam"


    // $ANTLR start "ruleObjectValueAnnotationParam"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:349:1: ruleObjectValueAnnotationParam : ( ( rule__ObjectValueAnnotationParam__Group__0 ) ) ;
    public final void ruleObjectValueAnnotationParam() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:353:2: ( ( ( rule__ObjectValueAnnotationParam__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:354:1: ( ( rule__ObjectValueAnnotationParam__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:354:1: ( ( rule__ObjectValueAnnotationParam__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:355:1: ( rule__ObjectValueAnnotationParam__Group__0 )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:356:1: ( rule__ObjectValueAnnotationParam__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:356:2: rule__ObjectValueAnnotationParam__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__0_in_ruleObjectValueAnnotationParam693);
            rule__ObjectValueAnnotationParam__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjectValueAnnotationParam"


    // $ANTLR start "entryRulePrimitiveValueAnnotationParam"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:368:1: entryRulePrimitiveValueAnnotationParam : rulePrimitiveValueAnnotationParam EOF ;
    public final void entryRulePrimitiveValueAnnotationParam() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:369:1: ( rulePrimitiveValueAnnotationParam EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:370:1: rulePrimitiveValueAnnotationParam EOF
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam720);
            rulePrimitiveValueAnnotationParam();

            state._fsp--;

             after(grammarAccess.getPrimitiveValueAnnotationParamRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam727); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimitiveValueAnnotationParam"


    // $ANTLR start "rulePrimitiveValueAnnotationParam"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:377:1: rulePrimitiveValueAnnotationParam : ( ( rule__PrimitiveValueAnnotationParam__Group__0 ) ) ;
    public final void rulePrimitiveValueAnnotationParam() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:381:2: ( ( ( rule__PrimitiveValueAnnotationParam__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:382:1: ( ( rule__PrimitiveValueAnnotationParam__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:382:1: ( ( rule__PrimitiveValueAnnotationParam__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:383:1: ( rule__PrimitiveValueAnnotationParam__Group__0 )
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:384:1: ( rule__PrimitiveValueAnnotationParam__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:384:2: rule__PrimitiveValueAnnotationParam__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__Group__0_in_rulePrimitiveValueAnnotationParam753);
            rule__PrimitiveValueAnnotationParam__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPrimitiveValueAnnotationParamAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimitiveValueAnnotationParam"


    // $ANTLR start "entryRuleAttribute"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:396:1: entryRuleAttribute : ruleAttribute EOF ;
    public final void entryRuleAttribute() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:397:1: ( ruleAttribute EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:398:1: ruleAttribute EOF
            {
             before(grammarAccess.getAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_entryRuleAttribute780);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getAttributeRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttribute787); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:405:1: ruleAttribute : ( ( rule__Attribute__Group__0 ) ) ;
    public final void ruleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:409:2: ( ( ( rule__Attribute__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:410:1: ( ( rule__Attribute__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:410:1: ( ( rule__Attribute__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:411:1: ( rule__Attribute__Group__0 )
            {
             before(grammarAccess.getAttributeAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:412:1: ( rule__Attribute__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:412:2: rule__Attribute__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__0_in_ruleAttribute813);
            rule__Attribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRulePrimitiveValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:424:1: entryRulePrimitiveValue : rulePrimitiveValue EOF ;
    public final void entryRulePrimitiveValue() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:425:1: ( rulePrimitiveValue EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:426:1: rulePrimitiveValue EOF
            {
             before(grammarAccess.getPrimitiveValueRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue840);
            rulePrimitiveValue();

            state._fsp--;

             after(grammarAccess.getPrimitiveValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValue847); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimitiveValue"


    // $ANTLR start "rulePrimitiveValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:433:1: rulePrimitiveValue : ( ( rule__PrimitiveValue__Alternatives ) ) ;
    public final void rulePrimitiveValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:437:2: ( ( ( rule__PrimitiveValue__Alternatives ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:438:1: ( ( rule__PrimitiveValue__Alternatives ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:438:1: ( ( rule__PrimitiveValue__Alternatives ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:439:1: ( rule__PrimitiveValue__Alternatives )
            {
             before(grammarAccess.getPrimitiveValueAccess().getAlternatives()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:440:1: ( rule__PrimitiveValue__Alternatives )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:440:2: rule__PrimitiveValue__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValue__Alternatives_in_rulePrimitiveValue873);
            rule__PrimitiveValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimitiveValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimitiveValue"


    // $ANTLR start "entryRuleIntegerValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:452:1: entryRuleIntegerValue : ruleIntegerValue EOF ;
    public final void entryRuleIntegerValue() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:453:1: ( ruleIntegerValue EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:454:1: ruleIntegerValue EOF
            {
             before(grammarAccess.getIntegerValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue900);
            ruleIntegerValue();

            state._fsp--;

             after(grammarAccess.getIntegerValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntegerValue907); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:461:1: ruleIntegerValue : ( ( rule__IntegerValue__ValueAssignment ) ) ;
    public final void ruleIntegerValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:465:2: ( ( ( rule__IntegerValue__ValueAssignment ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:466:1: ( ( rule__IntegerValue__ValueAssignment ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:466:1: ( ( rule__IntegerValue__ValueAssignment ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:467:1: ( rule__IntegerValue__ValueAssignment )
            {
             before(grammarAccess.getIntegerValueAccess().getValueAssignment()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:468:1: ( rule__IntegerValue__ValueAssignment )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:468:2: rule__IntegerValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__IntegerValue__ValueAssignment_in_ruleIntegerValue933);
            rule__IntegerValue__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getIntegerValueAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleStringValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:480:1: entryRuleStringValue : ruleStringValue EOF ;
    public final void entryRuleStringValue() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:481:1: ( ruleStringValue EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:482:1: ruleStringValue EOF
            {
             before(grammarAccess.getStringValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_entryRuleStringValue960);
            ruleStringValue();

            state._fsp--;

             after(grammarAccess.getStringValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringValue967); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:489:1: ruleStringValue : ( ( rule__StringValue__ValueAssignment ) ) ;
    public final void ruleStringValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:493:2: ( ( ( rule__StringValue__ValueAssignment ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:494:1: ( ( rule__StringValue__ValueAssignment ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:494:1: ( ( rule__StringValue__ValueAssignment ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:495:1: ( rule__StringValue__ValueAssignment )
            {
             before(grammarAccess.getStringValueAccess().getValueAssignment()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:496:1: ( rule__StringValue__ValueAssignment )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:496:2: rule__StringValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__StringValue__ValueAssignment_in_ruleStringValue993);
            rule__StringValue__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getStringValueAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleBooleanValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:508:1: entryRuleBooleanValue : ruleBooleanValue EOF ;
    public final void entryRuleBooleanValue() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:509:1: ( ruleBooleanValue EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:510:1: ruleBooleanValue EOF
            {
             before(grammarAccess.getBooleanValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue1020);
            ruleBooleanValue();

            state._fsp--;

             after(grammarAccess.getBooleanValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValue1027); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:517:1: ruleBooleanValue : ( ( rule__BooleanValue__ValueAssignment ) ) ;
    public final void ruleBooleanValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:521:2: ( ( ( rule__BooleanValue__ValueAssignment ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:522:1: ( ( rule__BooleanValue__ValueAssignment ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:522:1: ( ( rule__BooleanValue__ValueAssignment ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:523:1: ( rule__BooleanValue__ValueAssignment )
            {
             before(grammarAccess.getBooleanValueAccess().getValueAssignment()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:524:1: ( rule__BooleanValue__ValueAssignment )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:524:2: rule__BooleanValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__BooleanValue__ValueAssignment_in_ruleBooleanValue1053);
            rule__BooleanValue__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBooleanValueAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleReference"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:536:1: entryRuleReference : ruleReference EOF ;
    public final void entryRuleReference() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:537:1: ( ruleReference EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:538:1: ruleReference EOF
            {
             before(grammarAccess.getReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReference_in_entryRuleReference1080);
            ruleReference();

            state._fsp--;

             after(grammarAccess.getReferenceRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReference1087); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:545:1: ruleReference : ( ( rule__Reference__Group__0 ) ) ;
    public final void ruleReference() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:549:2: ( ( ( rule__Reference__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:550:1: ( ( rule__Reference__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:550:1: ( ( rule__Reference__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:551:1: ( rule__Reference__Group__0 )
            {
             before(grammarAccess.getReferenceAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:552:1: ( rule__Reference__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:552:2: rule__Reference__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__0_in_ruleReference1113);
            rule__Reference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleEdgeProperties"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:564:1: entryRuleEdgeProperties : ruleEdgeProperties EOF ;
    public final void entryRuleEdgeProperties() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:565:1: ( ruleEdgeProperties EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:566:1: ruleEdgeProperties EOF
            {
             before(grammarAccess.getEdgePropertiesRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEdgeProperties_in_entryRuleEdgeProperties1140);
            ruleEdgeProperties();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEdgeProperties1147); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEdgeProperties"


    // $ANTLR start "ruleEdgeProperties"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:573:1: ruleEdgeProperties : ( ( rule__EdgeProperties__Group__0 ) ) ;
    public final void ruleEdgeProperties() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:577:2: ( ( ( rule__EdgeProperties__Group__0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:578:1: ( ( rule__EdgeProperties__Group__0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:578:1: ( ( rule__EdgeProperties__Group__0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:579:1: ( rule__EdgeProperties__Group__0 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getGroup()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:580:1: ( rule__EdgeProperties__Group__0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:580:2: rule__EdgeProperties__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__0_in_ruleEdgeProperties1173);
            rule__EdgeProperties__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEdgeProperties"


    // $ANTLR start "entryRuleBooleanValueTerminal"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:592:1: entryRuleBooleanValueTerminal : ruleBooleanValueTerminal EOF ;
    public final void entryRuleBooleanValueTerminal() throws RecognitionException {
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:593:1: ( ruleBooleanValueTerminal EOF )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:594:1: ruleBooleanValueTerminal EOF
            {
             before(grammarAccess.getBooleanValueTerminalRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal1200);
            ruleBooleanValueTerminal();

            state._fsp--;

             after(grammarAccess.getBooleanValueTerminalRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValueTerminal1207); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanValueTerminal"


    // $ANTLR start "ruleBooleanValueTerminal"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:601:1: ruleBooleanValueTerminal : ( ( rule__BooleanValueTerminal__Alternatives ) ) ;
    public final void ruleBooleanValueTerminal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:605:2: ( ( ( rule__BooleanValueTerminal__Alternatives ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:606:1: ( ( rule__BooleanValueTerminal__Alternatives ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:606:1: ( ( rule__BooleanValueTerminal__Alternatives ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:607:1: ( rule__BooleanValueTerminal__Alternatives )
            {
             before(grammarAccess.getBooleanValueTerminalAccess().getAlternatives()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:608:1: ( rule__BooleanValueTerminal__Alternatives )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:608:2: rule__BooleanValueTerminal__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__BooleanValueTerminal__Alternatives_in_ruleBooleanValueTerminal1233);
            rule__BooleanValueTerminal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBooleanValueTerminalAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanValueTerminal"


    // $ANTLR start "ruleFragmentType"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:621:1: ruleFragmentType : ( ( rule__FragmentType__Alternatives ) ) ;
    public final void ruleFragmentType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:625:1: ( ( ( rule__FragmentType__Alternatives ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:626:1: ( ( rule__FragmentType__Alternatives ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:626:1: ( ( rule__FragmentType__Alternatives ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:627:1: ( rule__FragmentType__Alternatives )
            {
             before(grammarAccess.getFragmentTypeAccess().getAlternatives()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:628:1: ( rule__FragmentType__Alternatives )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:628:2: rule__FragmentType__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentType__Alternatives_in_ruleFragmentType1270);
            rule__FragmentType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFragmentTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFragmentType"


    // $ANTLR start "ruleLineType"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:640:1: ruleLineType : ( ( rule__LineType__Alternatives ) ) ;
    public final void ruleLineType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:644:1: ( ( ( rule__LineType__Alternatives ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:645:1: ( ( rule__LineType__Alternatives ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:645:1: ( ( rule__LineType__Alternatives ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:646:1: ( rule__LineType__Alternatives )
            {
             before(grammarAccess.getLineTypeAccess().getAlternatives()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:647:1: ( rule__LineType__Alternatives )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:647:2: rule__LineType__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__LineType__Alternatives_in_ruleLineType1306);
            rule__LineType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLineTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLineType"


    // $ANTLR start "ruleArrowType"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:659:1: ruleArrowType : ( ( rule__ArrowType__Alternatives ) ) ;
    public final void ruleArrowType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:663:1: ( ( ( rule__ArrowType__Alternatives ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:664:1: ( ( rule__ArrowType__Alternatives ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:664:1: ( ( rule__ArrowType__Alternatives ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:665:1: ( rule__ArrowType__Alternatives )
            {
             before(grammarAccess.getArrowTypeAccess().getAlternatives()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:666:1: ( rule__ArrowType__Alternatives )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:666:2: rule__ArrowType__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__ArrowType__Alternatives_in_ruleArrowType1342);
            rule__ArrowType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getArrowTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArrowType"


    // $ANTLR start "rule__Feature__Alternatives"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:677:1: rule__Feature__Alternatives : ( ( ruleAttribute ) | ( ruleReference ) );
    public final void rule__Feature__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:681:1: ( ( ruleAttribute ) | ( ruleReference ) )
            int alt1=2;
            alt1 = dfa1.predict(input);
            switch (alt1) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:682:1: ( ruleAttribute )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:682:1: ( ruleAttribute )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:683:1: ruleAttribute
                    {
                     before(grammarAccess.getFeatureAccess().getAttributeParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_rule__Feature__Alternatives1377);
                    ruleAttribute();

                    state._fsp--;

                     after(grammarAccess.getFeatureAccess().getAttributeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:688:6: ( ruleReference )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:688:6: ( ruleReference )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:689:1: ruleReference
                    {
                     before(grammarAccess.getFeatureAccess().getReferenceParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleReference_in_rule__Feature__Alternatives1394);
                    ruleReference();

                    state._fsp--;

                     after(grammarAccess.getFeatureAccess().getReferenceParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:699:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:703:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:704:1: ( RULE_STRING )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:704:1: ( RULE_STRING )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:705:1: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__EString__Alternatives1426); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:710:6: ( RULE_ID )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:710:6: ( RULE_ID )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:711:1: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__EString__Alternatives1443); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__ParamValue__Alternatives"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:721:1: rule__ParamValue__Alternatives : ( ( rulePrimitiveValueAnnotationParam ) | ( ruleObjectValueAnnotationParam ) );
    public final void rule__ParamValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:725:1: ( ( rulePrimitiveValueAnnotationParam ) | ( ruleObjectValueAnnotationParam ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==56) ) {
                alt3=1;
            }
            else if ( (LA3_0==54) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:726:1: ( rulePrimitiveValueAnnotationParam )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:726:1: ( rulePrimitiveValueAnnotationParam )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:727:1: rulePrimitiveValueAnnotationParam
                    {
                     before(grammarAccess.getParamValueAccess().getPrimitiveValueAnnotationParamParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_rule__ParamValue__Alternatives1475);
                    rulePrimitiveValueAnnotationParam();

                    state._fsp--;

                     after(grammarAccess.getParamValueAccess().getPrimitiveValueAnnotationParamParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:732:6: ( ruleObjectValueAnnotationParam )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:732:6: ( ruleObjectValueAnnotationParam )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:733:1: ruleObjectValueAnnotationParam
                    {
                     before(grammarAccess.getParamValueAccess().getObjectValueAnnotationParamParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_rule__ParamValue__Alternatives1492);
                    ruleObjectValueAnnotationParam();

                    state._fsp--;

                     after(grammarAccess.getParamValueAccess().getObjectValueAnnotationParamParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamValue__Alternatives"


    // $ANTLR start "rule__PrimitiveValue__Alternatives"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:743:1: rule__PrimitiveValue__Alternatives : ( ( ruleStringValue ) | ( ruleIntegerValue ) | ( ruleBooleanValue ) );
    public final void rule__PrimitiveValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:747:1: ( ( ruleStringValue ) | ( ruleIntegerValue ) | ( ruleBooleanValue ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt4=1;
                }
                break;
            case RULE_INT:
                {
                alt4=2;
                }
                break;
            case 13:
            case 14:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:748:1: ( ruleStringValue )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:748:1: ( ruleStringValue )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:749:1: ruleStringValue
                    {
                     before(grammarAccess.getPrimitiveValueAccess().getStringValueParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_rule__PrimitiveValue__Alternatives1524);
                    ruleStringValue();

                    state._fsp--;

                     after(grammarAccess.getPrimitiveValueAccess().getStringValueParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:754:6: ( ruleIntegerValue )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:754:6: ( ruleIntegerValue )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:755:1: ruleIntegerValue
                    {
                     before(grammarAccess.getPrimitiveValueAccess().getIntegerValueParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_rule__PrimitiveValue__Alternatives1541);
                    ruleIntegerValue();

                    state._fsp--;

                     after(grammarAccess.getPrimitiveValueAccess().getIntegerValueParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:760:6: ( ruleBooleanValue )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:760:6: ( ruleBooleanValue )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:761:1: ruleBooleanValue
                    {
                     before(grammarAccess.getPrimitiveValueAccess().getBooleanValueParserRuleCall_2()); 
                    pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_rule__PrimitiveValue__Alternatives1558);
                    ruleBooleanValue();

                    state._fsp--;

                     after(grammarAccess.getPrimitiveValueAccess().getBooleanValueParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValue__Alternatives"


    // $ANTLR start "rule__Reference__Alternatives_3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:771:1: rule__Reference__Alternatives_3 : ( ( 'ref' ) | ( 'rel' ) );
    public final void rule__Reference__Alternatives_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:775:1: ( ( 'ref' ) | ( 'rel' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==11) ) {
                alt5=1;
            }
            else if ( (LA5_0==12) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:776:1: ( 'ref' )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:776:1: ( 'ref' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:777:1: 'ref'
                    {
                     before(grammarAccess.getReferenceAccess().getRefKeyword_3_0()); 
                    match(input,11,FollowSets000.FOLLOW_11_in_rule__Reference__Alternatives_31591); 
                     after(grammarAccess.getReferenceAccess().getRefKeyword_3_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:784:6: ( 'rel' )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:784:6: ( 'rel' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:785:1: 'rel'
                    {
                     before(grammarAccess.getReferenceAccess().getRelKeyword_3_1()); 
                    match(input,12,FollowSets000.FOLLOW_12_in_rule__Reference__Alternatives_31611); 
                     after(grammarAccess.getReferenceAccess().getRelKeyword_3_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Alternatives_3"


    // $ANTLR start "rule__BooleanValueTerminal__Alternatives"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:797:1: rule__BooleanValueTerminal__Alternatives : ( ( 'true' ) | ( 'false' ) );
    public final void rule__BooleanValueTerminal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:801:1: ( ( 'true' ) | ( 'false' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==13) ) {
                alt6=1;
            }
            else if ( (LA6_0==14) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:802:1: ( 'true' )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:802:1: ( 'true' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:803:1: 'true'
                    {
                     before(grammarAccess.getBooleanValueTerminalAccess().getTrueKeyword_0()); 
                    match(input,13,FollowSets000.FOLLOW_13_in_rule__BooleanValueTerminal__Alternatives1646); 
                     after(grammarAccess.getBooleanValueTerminalAccess().getTrueKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:810:6: ( 'false' )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:810:6: ( 'false' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:811:1: 'false'
                    {
                     before(grammarAccess.getBooleanValueTerminalAccess().getFalseKeyword_1()); 
                    match(input,14,FollowSets000.FOLLOW_14_in_rule__BooleanValueTerminal__Alternatives1666); 
                     after(grammarAccess.getBooleanValueTerminalAccess().getFalseKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanValueTerminal__Alternatives"


    // $ANTLR start "rule__FragmentType__Alternatives"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:823:1: rule__FragmentType__Alternatives : ( ( ( 'positive' ) ) | ( ( 'negative' ) ) );
    public final void rule__FragmentType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:827:1: ( ( ( 'positive' ) ) | ( ( 'negative' ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==15) ) {
                alt7=1;
            }
            else if ( (LA7_0==16) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:828:1: ( ( 'positive' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:828:1: ( ( 'positive' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:829:1: ( 'positive' )
                    {
                     before(grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:830:1: ( 'positive' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:830:3: 'positive'
                    {
                    match(input,15,FollowSets000.FOLLOW_15_in_rule__FragmentType__Alternatives1701); 

                    }

                     after(grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:835:6: ( ( 'negative' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:835:6: ( ( 'negative' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:836:1: ( 'negative' )
                    {
                     before(grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:837:1: ( 'negative' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:837:3: 'negative'
                    {
                    match(input,16,FollowSets000.FOLLOW_16_in_rule__FragmentType__Alternatives1722); 

                    }

                     after(grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentType__Alternatives"


    // $ANTLR start "rule__LineType__Alternatives"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:847:1: rule__LineType__Alternatives : ( ( ( 'line' ) ) | ( ( 'dashed' ) ) | ( ( 'dashed-dotted' ) ) | ( ( 'dotted' ) ) );
    public final void rule__LineType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:851:1: ( ( ( 'line' ) ) | ( ( 'dashed' ) ) | ( ( 'dashed-dotted' ) ) | ( ( 'dotted' ) ) )
            int alt8=4;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt8=1;
                }
                break;
            case 18:
                {
                alt8=2;
                }
                break;
            case 19:
                {
                alt8=3;
                }
                break;
            case 20:
                {
                alt8=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:852:1: ( ( 'line' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:852:1: ( ( 'line' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:853:1: ( 'line' )
                    {
                     before(grammarAccess.getLineTypeAccess().getLineEnumLiteralDeclaration_0()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:854:1: ( 'line' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:854:3: 'line'
                    {
                    match(input,17,FollowSets000.FOLLOW_17_in_rule__LineType__Alternatives1758); 

                    }

                     after(grammarAccess.getLineTypeAccess().getLineEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:859:6: ( ( 'dashed' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:859:6: ( ( 'dashed' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:860:1: ( 'dashed' )
                    {
                     before(grammarAccess.getLineTypeAccess().getDashedEnumLiteralDeclaration_1()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:861:1: ( 'dashed' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:861:3: 'dashed'
                    {
                    match(input,18,FollowSets000.FOLLOW_18_in_rule__LineType__Alternatives1779); 

                    }

                     after(grammarAccess.getLineTypeAccess().getDashedEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:866:6: ( ( 'dashed-dotted' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:866:6: ( ( 'dashed-dotted' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:867:1: ( 'dashed-dotted' )
                    {
                     before(grammarAccess.getLineTypeAccess().getDashedDottedEnumLiteralDeclaration_2()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:868:1: ( 'dashed-dotted' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:868:3: 'dashed-dotted'
                    {
                    match(input,19,FollowSets000.FOLLOW_19_in_rule__LineType__Alternatives1800); 

                    }

                     after(grammarAccess.getLineTypeAccess().getDashedDottedEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:873:6: ( ( 'dotted' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:873:6: ( ( 'dotted' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:874:1: ( 'dotted' )
                    {
                     before(grammarAccess.getLineTypeAccess().getDottedEnumLiteralDeclaration_3()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:875:1: ( 'dotted' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:875:3: 'dotted'
                    {
                    match(input,20,FollowSets000.FOLLOW_20_in_rule__LineType__Alternatives1821); 

                    }

                     after(grammarAccess.getLineTypeAccess().getDottedEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LineType__Alternatives"


    // $ANTLR start "rule__ArrowType__Alternatives"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:885:1: rule__ArrowType__Alternatives : ( ( ( 'circle' ) ) | ( ( 'concave' ) ) | ( ( 'convex' ) ) | ( ( 'crows-foot-many' ) ) | ( ( 'crows-foot-many-mandatory' ) ) | ( ( 'crows-foot-many-optional' ) ) | ( ( 'crows-foot-one' ) ) | ( ( 'crows-foot-one-mandatory' ) ) | ( ( 'crows-foot-one-optional' ) ) | ( ( 'dash' ) ) | ( ( 'delta' ) ) | ( ( 'diamond' ) ) | ( ( 'none' ) ) | ( ( 'plain' ) ) | ( ( 'short' ) ) | ( ( 'skewed-dash' ) ) | ( ( 'standard' ) ) | ( ( 'transparent-circle' ) ) | ( ( 't-shape' ) ) | ( ( 'white-delta' ) ) | ( ( 'white-diamond' ) ) );
    public final void rule__ArrowType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:889:1: ( ( ( 'circle' ) ) | ( ( 'concave' ) ) | ( ( 'convex' ) ) | ( ( 'crows-foot-many' ) ) | ( ( 'crows-foot-many-mandatory' ) ) | ( ( 'crows-foot-many-optional' ) ) | ( ( 'crows-foot-one' ) ) | ( ( 'crows-foot-one-mandatory' ) ) | ( ( 'crows-foot-one-optional' ) ) | ( ( 'dash' ) ) | ( ( 'delta' ) ) | ( ( 'diamond' ) ) | ( ( 'none' ) ) | ( ( 'plain' ) ) | ( ( 'short' ) ) | ( ( 'skewed-dash' ) ) | ( ( 'standard' ) ) | ( ( 'transparent-circle' ) ) | ( ( 't-shape' ) ) | ( ( 'white-delta' ) ) | ( ( 'white-diamond' ) ) )
            int alt9=21;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt9=1;
                }
                break;
            case 22:
                {
                alt9=2;
                }
                break;
            case 23:
                {
                alt9=3;
                }
                break;
            case 24:
                {
                alt9=4;
                }
                break;
            case 25:
                {
                alt9=5;
                }
                break;
            case 26:
                {
                alt9=6;
                }
                break;
            case 27:
                {
                alt9=7;
                }
                break;
            case 28:
                {
                alt9=8;
                }
                break;
            case 29:
                {
                alt9=9;
                }
                break;
            case 30:
                {
                alt9=10;
                }
                break;
            case 31:
                {
                alt9=11;
                }
                break;
            case 32:
                {
                alt9=12;
                }
                break;
            case 33:
                {
                alt9=13;
                }
                break;
            case 34:
                {
                alt9=14;
                }
                break;
            case 35:
                {
                alt9=15;
                }
                break;
            case 36:
                {
                alt9=16;
                }
                break;
            case 37:
                {
                alt9=17;
                }
                break;
            case 38:
                {
                alt9=18;
                }
                break;
            case 39:
                {
                alt9=19;
                }
                break;
            case 40:
                {
                alt9=20;
                }
                break;
            case 41:
                {
                alt9=21;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:890:1: ( ( 'circle' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:890:1: ( ( 'circle' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:891:1: ( 'circle' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCircleEnumLiteralDeclaration_0()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:892:1: ( 'circle' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:892:3: 'circle'
                    {
                    match(input,21,FollowSets000.FOLLOW_21_in_rule__ArrowType__Alternatives1857); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCircleEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:897:6: ( ( 'concave' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:897:6: ( ( 'concave' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:898:1: ( 'concave' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getConcaveEnumLiteralDeclaration_1()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:899:1: ( 'concave' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:899:3: 'concave'
                    {
                    match(input,22,FollowSets000.FOLLOW_22_in_rule__ArrowType__Alternatives1878); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getConcaveEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:904:6: ( ( 'convex' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:904:6: ( ( 'convex' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:905:1: ( 'convex' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getConvexEnumLiteralDeclaration_2()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:906:1: ( 'convex' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:906:3: 'convex'
                    {
                    match(input,23,FollowSets000.FOLLOW_23_in_rule__ArrowType__Alternatives1899); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getConvexEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:911:6: ( ( 'crows-foot-many' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:911:6: ( ( 'crows-foot-many' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:912:1: ( 'crows-foot-many' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootManyEnumLiteralDeclaration_3()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:913:1: ( 'crows-foot-many' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:913:3: 'crows-foot-many'
                    {
                    match(input,24,FollowSets000.FOLLOW_24_in_rule__ArrowType__Alternatives1920); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootManyEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:918:6: ( ( 'crows-foot-many-mandatory' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:918:6: ( ( 'crows-foot-many-mandatory' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:919:1: ( 'crows-foot-many-mandatory' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootManyMandatoryEnumLiteralDeclaration_4()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:920:1: ( 'crows-foot-many-mandatory' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:920:3: 'crows-foot-many-mandatory'
                    {
                    match(input,25,FollowSets000.FOLLOW_25_in_rule__ArrowType__Alternatives1941); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootManyMandatoryEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:925:6: ( ( 'crows-foot-many-optional' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:925:6: ( ( 'crows-foot-many-optional' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:926:1: ( 'crows-foot-many-optional' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootManyOptionalEnumLiteralDeclaration_5()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:927:1: ( 'crows-foot-many-optional' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:927:3: 'crows-foot-many-optional'
                    {
                    match(input,26,FollowSets000.FOLLOW_26_in_rule__ArrowType__Alternatives1962); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootManyOptionalEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:932:6: ( ( 'crows-foot-one' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:932:6: ( ( 'crows-foot-one' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:933:1: ( 'crows-foot-one' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootOneEnumLiteralDeclaration_6()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:934:1: ( 'crows-foot-one' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:934:3: 'crows-foot-one'
                    {
                    match(input,27,FollowSets000.FOLLOW_27_in_rule__ArrowType__Alternatives1983); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootOneEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:939:6: ( ( 'crows-foot-one-mandatory' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:939:6: ( ( 'crows-foot-one-mandatory' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:940:1: ( 'crows-foot-one-mandatory' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootOneMandatoryEnumLiteralDeclaration_7()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:941:1: ( 'crows-foot-one-mandatory' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:941:3: 'crows-foot-one-mandatory'
                    {
                    match(input,28,FollowSets000.FOLLOW_28_in_rule__ArrowType__Alternatives2004); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootOneMandatoryEnumLiteralDeclaration_7()); 

                    }


                    }
                    break;
                case 9 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:946:6: ( ( 'crows-foot-one-optional' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:946:6: ( ( 'crows-foot-one-optional' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:947:1: ( 'crows-foot-one-optional' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootOneOptionalEnumLiteralDeclaration_8()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:948:1: ( 'crows-foot-one-optional' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:948:3: 'crows-foot-one-optional'
                    {
                    match(input,29,FollowSets000.FOLLOW_29_in_rule__ArrowType__Alternatives2025); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootOneOptionalEnumLiteralDeclaration_8()); 

                    }


                    }
                    break;
                case 10 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:953:6: ( ( 'dash' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:953:6: ( ( 'dash' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:954:1: ( 'dash' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getDashEnumLiteralDeclaration_9()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:955:1: ( 'dash' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:955:3: 'dash'
                    {
                    match(input,30,FollowSets000.FOLLOW_30_in_rule__ArrowType__Alternatives2046); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getDashEnumLiteralDeclaration_9()); 

                    }


                    }
                    break;
                case 11 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:960:6: ( ( 'delta' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:960:6: ( ( 'delta' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:961:1: ( 'delta' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getDeltaEnumLiteralDeclaration_10()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:962:1: ( 'delta' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:962:3: 'delta'
                    {
                    match(input,31,FollowSets000.FOLLOW_31_in_rule__ArrowType__Alternatives2067); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getDeltaEnumLiteralDeclaration_10()); 

                    }


                    }
                    break;
                case 12 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:967:6: ( ( 'diamond' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:967:6: ( ( 'diamond' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:968:1: ( 'diamond' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getDiamondEnumLiteralDeclaration_11()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:969:1: ( 'diamond' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:969:3: 'diamond'
                    {
                    match(input,32,FollowSets000.FOLLOW_32_in_rule__ArrowType__Alternatives2088); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getDiamondEnumLiteralDeclaration_11()); 

                    }


                    }
                    break;
                case 13 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:974:6: ( ( 'none' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:974:6: ( ( 'none' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:975:1: ( 'none' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getNoneEnumLiteralDeclaration_12()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:976:1: ( 'none' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:976:3: 'none'
                    {
                    match(input,33,FollowSets000.FOLLOW_33_in_rule__ArrowType__Alternatives2109); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getNoneEnumLiteralDeclaration_12()); 

                    }


                    }
                    break;
                case 14 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:981:6: ( ( 'plain' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:981:6: ( ( 'plain' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:982:1: ( 'plain' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getPlainEnumLiteralDeclaration_13()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:983:1: ( 'plain' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:983:3: 'plain'
                    {
                    match(input,34,FollowSets000.FOLLOW_34_in_rule__ArrowType__Alternatives2130); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getPlainEnumLiteralDeclaration_13()); 

                    }


                    }
                    break;
                case 15 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:988:6: ( ( 'short' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:988:6: ( ( 'short' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:989:1: ( 'short' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getShortEnumLiteralDeclaration_14()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:990:1: ( 'short' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:990:3: 'short'
                    {
                    match(input,35,FollowSets000.FOLLOW_35_in_rule__ArrowType__Alternatives2151); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getShortEnumLiteralDeclaration_14()); 

                    }


                    }
                    break;
                case 16 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:995:6: ( ( 'skewed-dash' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:995:6: ( ( 'skewed-dash' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:996:1: ( 'skewed-dash' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getSkewedDashEnumLiteralDeclaration_15()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:997:1: ( 'skewed-dash' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:997:3: 'skewed-dash'
                    {
                    match(input,36,FollowSets000.FOLLOW_36_in_rule__ArrowType__Alternatives2172); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getSkewedDashEnumLiteralDeclaration_15()); 

                    }


                    }
                    break;
                case 17 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1002:6: ( ( 'standard' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1002:6: ( ( 'standard' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1003:1: ( 'standard' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getStandardEnumLiteralDeclaration_16()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1004:1: ( 'standard' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1004:3: 'standard'
                    {
                    match(input,37,FollowSets000.FOLLOW_37_in_rule__ArrowType__Alternatives2193); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getStandardEnumLiteralDeclaration_16()); 

                    }


                    }
                    break;
                case 18 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1009:6: ( ( 'transparent-circle' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1009:6: ( ( 'transparent-circle' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1010:1: ( 'transparent-circle' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getTransparentCircleEnumLiteralDeclaration_17()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1011:1: ( 'transparent-circle' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1011:3: 'transparent-circle'
                    {
                    match(input,38,FollowSets000.FOLLOW_38_in_rule__ArrowType__Alternatives2214); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getTransparentCircleEnumLiteralDeclaration_17()); 

                    }


                    }
                    break;
                case 19 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1016:6: ( ( 't-shape' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1016:6: ( ( 't-shape' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1017:1: ( 't-shape' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getTShapeEnumLiteralDeclaration_18()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1018:1: ( 't-shape' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1018:3: 't-shape'
                    {
                    match(input,39,FollowSets000.FOLLOW_39_in_rule__ArrowType__Alternatives2235); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getTShapeEnumLiteralDeclaration_18()); 

                    }


                    }
                    break;
                case 20 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1023:6: ( ( 'white-delta' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1023:6: ( ( 'white-delta' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1024:1: ( 'white-delta' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getWhiteDeltaEnumLiteralDeclaration_19()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1025:1: ( 'white-delta' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1025:3: 'white-delta'
                    {
                    match(input,40,FollowSets000.FOLLOW_40_in_rule__ArrowType__Alternatives2256); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getWhiteDeltaEnumLiteralDeclaration_19()); 

                    }


                    }
                    break;
                case 21 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1030:6: ( ( 'white-diamond' ) )
                    {
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1030:6: ( ( 'white-diamond' ) )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1031:1: ( 'white-diamond' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getWhiteDiamondEnumLiteralDeclaration_20()); 
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1032:1: ( 'white-diamond' )
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1032:3: 'white-diamond'
                    {
                    match(input,41,FollowSets000.FOLLOW_41_in_rule__ArrowType__Alternatives2277); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getWhiteDiamondEnumLiteralDeclaration_20()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArrowType__Alternatives"


    // $ANTLR start "rule__ShellModel__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1044:1: rule__ShellModel__Group__0 : rule__ShellModel__Group__0__Impl rule__ShellModel__Group__1 ;
    public final void rule__ShellModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1048:1: ( rule__ShellModel__Group__0__Impl rule__ShellModel__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1049:2: rule__ShellModel__Group__0__Impl rule__ShellModel__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ShellModel__Group__0__Impl_in_rule__ShellModel__Group__02310);
            rule__ShellModel__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ShellModel__Group__1_in_rule__ShellModel__Group__02313);
            rule__ShellModel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__Group__0"


    // $ANTLR start "rule__ShellModel__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1056:1: rule__ShellModel__Group__0__Impl : ( () ) ;
    public final void rule__ShellModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1060:1: ( ( () ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1061:1: ( () )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1061:1: ( () )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1062:1: ()
            {
             before(grammarAccess.getShellModelAccess().getShellModelAction_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1063:1: ()
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1065:1: 
            {
            }

             after(grammarAccess.getShellModelAccess().getShellModelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__Group__0__Impl"


    // $ANTLR start "rule__ShellModel__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1075:1: rule__ShellModel__Group__1 : rule__ShellModel__Group__1__Impl rule__ShellModel__Group__2 ;
    public final void rule__ShellModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1079:1: ( rule__ShellModel__Group__1__Impl rule__ShellModel__Group__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1080:2: rule__ShellModel__Group__1__Impl rule__ShellModel__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ShellModel__Group__1__Impl_in_rule__ShellModel__Group__12371);
            rule__ShellModel__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ShellModel__Group__2_in_rule__ShellModel__Group__12374);
            rule__ShellModel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__Group__1"


    // $ANTLR start "rule__ShellModel__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1087:1: rule__ShellModel__Group__1__Impl : ( 'shell' ) ;
    public final void rule__ShellModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1091:1: ( ( 'shell' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1092:1: ( 'shell' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1092:1: ( 'shell' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1093:1: 'shell'
            {
             before(grammarAccess.getShellModelAccess().getShellKeyword_1()); 
            match(input,42,FollowSets000.FOLLOW_42_in_rule__ShellModel__Group__1__Impl2402); 
             after(grammarAccess.getShellModelAccess().getShellKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__Group__1__Impl"


    // $ANTLR start "rule__ShellModel__Group__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1106:1: rule__ShellModel__Group__2 : rule__ShellModel__Group__2__Impl rule__ShellModel__Group__3 ;
    public final void rule__ShellModel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1110:1: ( rule__ShellModel__Group__2__Impl rule__ShellModel__Group__3 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1111:2: rule__ShellModel__Group__2__Impl rule__ShellModel__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__ShellModel__Group__2__Impl_in_rule__ShellModel__Group__22433);
            rule__ShellModel__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ShellModel__Group__3_in_rule__ShellModel__Group__22436);
            rule__ShellModel__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__Group__2"


    // $ANTLR start "rule__ShellModel__Group__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1118:1: rule__ShellModel__Group__2__Impl : ( ( rule__ShellModel__NameAssignment_2 ) ) ;
    public final void rule__ShellModel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1122:1: ( ( ( rule__ShellModel__NameAssignment_2 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1123:1: ( ( rule__ShellModel__NameAssignment_2 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1123:1: ( ( rule__ShellModel__NameAssignment_2 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1124:1: ( rule__ShellModel__NameAssignment_2 )
            {
             before(grammarAccess.getShellModelAccess().getNameAssignment_2()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1125:1: ( rule__ShellModel__NameAssignment_2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1125:2: rule__ShellModel__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ShellModel__NameAssignment_2_in_rule__ShellModel__Group__2__Impl2463);
            rule__ShellModel__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getShellModelAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__Group__2__Impl"


    // $ANTLR start "rule__ShellModel__Group__3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1135:1: rule__ShellModel__Group__3 : rule__ShellModel__Group__3__Impl ;
    public final void rule__ShellModel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1139:1: ( rule__ShellModel__Group__3__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1140:2: rule__ShellModel__Group__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ShellModel__Group__3__Impl_in_rule__ShellModel__Group__32493);
            rule__ShellModel__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__Group__3"


    // $ANTLR start "rule__ShellModel__Group__3__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1146:1: rule__ShellModel__Group__3__Impl : ( ( rule__ShellModel__CommandsAssignment_3 )* ) ;
    public final void rule__ShellModel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1150:1: ( ( ( rule__ShellModel__CommandsAssignment_3 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1151:1: ( ( rule__ShellModel__CommandsAssignment_3 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1151:1: ( ( rule__ShellModel__CommandsAssignment_3 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1152:1: ( rule__ShellModel__CommandsAssignment_3 )*
            {
             before(grammarAccess.getShellModelAccess().getCommandsAssignment_3()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1153:1: ( rule__ShellModel__CommandsAssignment_3 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>=15 && LA10_0<=16)||LA10_0==43||LA10_0==50) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1153:2: rule__ShellModel__CommandsAssignment_3
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__ShellModel__CommandsAssignment_3_in_rule__ShellModel__Group__3__Impl2520);
            	    rule__ShellModel__CommandsAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getShellModelAccess().getCommandsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__Group__3__Impl"


    // $ANTLR start "rule__Fragment__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1171:1: rule__Fragment__Group__0 : rule__Fragment__Group__0__Impl rule__Fragment__Group__1 ;
    public final void rule__Fragment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1175:1: ( rule__Fragment__Group__0__Impl rule__Fragment__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1176:2: rule__Fragment__Group__0__Impl rule__Fragment__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__0__Impl_in_rule__Fragment__Group__02559);
            rule__Fragment__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__1_in_rule__Fragment__Group__02562);
            rule__Fragment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__0"


    // $ANTLR start "rule__Fragment__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1183:1: rule__Fragment__Group__0__Impl : ( () ) ;
    public final void rule__Fragment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1187:1: ( ( () ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1188:1: ( () )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1188:1: ( () )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1189:1: ()
            {
             before(grammarAccess.getFragmentAccess().getFragmentAction_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1190:1: ()
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1192:1: 
            {
            }

             after(grammarAccess.getFragmentAccess().getFragmentAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__0__Impl"


    // $ANTLR start "rule__Fragment__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1202:1: rule__Fragment__Group__1 : rule__Fragment__Group__1__Impl rule__Fragment__Group__2 ;
    public final void rule__Fragment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1206:1: ( rule__Fragment__Group__1__Impl rule__Fragment__Group__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1207:2: rule__Fragment__Group__1__Impl rule__Fragment__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__1__Impl_in_rule__Fragment__Group__12620);
            rule__Fragment__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__2_in_rule__Fragment__Group__12623);
            rule__Fragment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__1"


    // $ANTLR start "rule__Fragment__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1214:1: rule__Fragment__Group__1__Impl : ( ( rule__Fragment__Group_1__0 )? ) ;
    public final void rule__Fragment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1218:1: ( ( ( rule__Fragment__Group_1__0 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1219:1: ( ( rule__Fragment__Group_1__0 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1219:1: ( ( rule__Fragment__Group_1__0 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1220:1: ( rule__Fragment__Group_1__0 )?
            {
             before(grammarAccess.getFragmentAccess().getGroup_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1221:1: ( rule__Fragment__Group_1__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==50) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1221:2: rule__Fragment__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_1__0_in_rule__Fragment__Group__1__Impl2650);
                    rule__Fragment__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFragmentAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__1__Impl"


    // $ANTLR start "rule__Fragment__Group__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1231:1: rule__Fragment__Group__2 : rule__Fragment__Group__2__Impl rule__Fragment__Group__3 ;
    public final void rule__Fragment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1235:1: ( rule__Fragment__Group__2__Impl rule__Fragment__Group__3 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1236:2: rule__Fragment__Group__2__Impl rule__Fragment__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__2__Impl_in_rule__Fragment__Group__22681);
            rule__Fragment__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__3_in_rule__Fragment__Group__22684);
            rule__Fragment__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__2"


    // $ANTLR start "rule__Fragment__Group__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1243:1: rule__Fragment__Group__2__Impl : ( ( rule__Fragment__FtypeAssignment_2 )? ) ;
    public final void rule__Fragment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1247:1: ( ( ( rule__Fragment__FtypeAssignment_2 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1248:1: ( ( rule__Fragment__FtypeAssignment_2 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1248:1: ( ( rule__Fragment__FtypeAssignment_2 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1249:1: ( rule__Fragment__FtypeAssignment_2 )?
            {
             before(grammarAccess.getFragmentAccess().getFtypeAssignment_2()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1250:1: ( rule__Fragment__FtypeAssignment_2 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=15 && LA12_0<=16)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1250:2: rule__Fragment__FtypeAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Fragment__FtypeAssignment_2_in_rule__Fragment__Group__2__Impl2711);
                    rule__Fragment__FtypeAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFragmentAccess().getFtypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__2__Impl"


    // $ANTLR start "rule__Fragment__Group__3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1260:1: rule__Fragment__Group__3 : rule__Fragment__Group__3__Impl rule__Fragment__Group__4 ;
    public final void rule__Fragment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1264:1: ( rule__Fragment__Group__3__Impl rule__Fragment__Group__4 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1265:2: rule__Fragment__Group__3__Impl rule__Fragment__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__3__Impl_in_rule__Fragment__Group__32742);
            rule__Fragment__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__4_in_rule__Fragment__Group__32745);
            rule__Fragment__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__3"


    // $ANTLR start "rule__Fragment__Group__3__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1272:1: rule__Fragment__Group__3__Impl : ( 'fragment' ) ;
    public final void rule__Fragment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1276:1: ( ( 'fragment' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1277:1: ( 'fragment' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1277:1: ( 'fragment' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1278:1: 'fragment'
            {
             before(grammarAccess.getFragmentAccess().getFragmentKeyword_3()); 
            match(input,43,FollowSets000.FOLLOW_43_in_rule__Fragment__Group__3__Impl2773); 
             after(grammarAccess.getFragmentAccess().getFragmentKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__3__Impl"


    // $ANTLR start "rule__Fragment__Group__4"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1291:1: rule__Fragment__Group__4 : rule__Fragment__Group__4__Impl rule__Fragment__Group__5 ;
    public final void rule__Fragment__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1295:1: ( rule__Fragment__Group__4__Impl rule__Fragment__Group__5 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1296:2: rule__Fragment__Group__4__Impl rule__Fragment__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__4__Impl_in_rule__Fragment__Group__42804);
            rule__Fragment__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__5_in_rule__Fragment__Group__42807);
            rule__Fragment__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__4"


    // $ANTLR start "rule__Fragment__Group__4__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1303:1: rule__Fragment__Group__4__Impl : ( ( rule__Fragment__Group_4__0 )? ) ;
    public final void rule__Fragment__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1307:1: ( ( ( rule__Fragment__Group_4__0 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1308:1: ( ( rule__Fragment__Group_4__0 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1308:1: ( ( rule__Fragment__Group_4__0 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1309:1: ( rule__Fragment__Group_4__0 )?
            {
             before(grammarAccess.getFragmentAccess().getGroup_4()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1310:1: ( rule__Fragment__Group_4__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==46) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1310:2: rule__Fragment__Group_4__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__0_in_rule__Fragment__Group__4__Impl2834);
                    rule__Fragment__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFragmentAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__4__Impl"


    // $ANTLR start "rule__Fragment__Group__5"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1320:1: rule__Fragment__Group__5 : rule__Fragment__Group__5__Impl rule__Fragment__Group__6 ;
    public final void rule__Fragment__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1324:1: ( rule__Fragment__Group__5__Impl rule__Fragment__Group__6 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1325:2: rule__Fragment__Group__5__Impl rule__Fragment__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__5__Impl_in_rule__Fragment__Group__52865);
            rule__Fragment__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__6_in_rule__Fragment__Group__52868);
            rule__Fragment__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__5"


    // $ANTLR start "rule__Fragment__Group__5__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1332:1: rule__Fragment__Group__5__Impl : ( ( rule__Fragment__NameAssignment_5 ) ) ;
    public final void rule__Fragment__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1336:1: ( ( ( rule__Fragment__NameAssignment_5 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1337:1: ( ( rule__Fragment__NameAssignment_5 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1337:1: ( ( rule__Fragment__NameAssignment_5 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1338:1: ( rule__Fragment__NameAssignment_5 )
            {
             before(grammarAccess.getFragmentAccess().getNameAssignment_5()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1339:1: ( rule__Fragment__NameAssignment_5 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1339:2: rule__Fragment__NameAssignment_5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__NameAssignment_5_in_rule__Fragment__Group__5__Impl2895);
            rule__Fragment__NameAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getNameAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__5__Impl"


    // $ANTLR start "rule__Fragment__Group__6"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1349:1: rule__Fragment__Group__6 : rule__Fragment__Group__6__Impl rule__Fragment__Group__7 ;
    public final void rule__Fragment__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1353:1: ( rule__Fragment__Group__6__Impl rule__Fragment__Group__7 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1354:2: rule__Fragment__Group__6__Impl rule__Fragment__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__6__Impl_in_rule__Fragment__Group__62925);
            rule__Fragment__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__7_in_rule__Fragment__Group__62928);
            rule__Fragment__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__6"


    // $ANTLR start "rule__Fragment__Group__6__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1361:1: rule__Fragment__Group__6__Impl : ( '{' ) ;
    public final void rule__Fragment__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1365:1: ( ( '{' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1366:1: ( '{' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1366:1: ( '{' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1367:1: '{'
            {
             before(grammarAccess.getFragmentAccess().getLeftCurlyBracketKeyword_6()); 
            match(input,44,FollowSets000.FOLLOW_44_in_rule__Fragment__Group__6__Impl2956); 
             after(grammarAccess.getFragmentAccess().getLeftCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__6__Impl"


    // $ANTLR start "rule__Fragment__Group__7"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1380:1: rule__Fragment__Group__7 : rule__Fragment__Group__7__Impl rule__Fragment__Group__8 ;
    public final void rule__Fragment__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1384:1: ( rule__Fragment__Group__7__Impl rule__Fragment__Group__8 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1385:2: rule__Fragment__Group__7__Impl rule__Fragment__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__7__Impl_in_rule__Fragment__Group__72987);
            rule__Fragment__Group__7__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__8_in_rule__Fragment__Group__72990);
            rule__Fragment__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__7"


    // $ANTLR start "rule__Fragment__Group__7__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1392:1: rule__Fragment__Group__7__Impl : ( ( rule__Fragment__ObjectsAssignment_7 ) ) ;
    public final void rule__Fragment__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1396:1: ( ( ( rule__Fragment__ObjectsAssignment_7 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1397:1: ( ( rule__Fragment__ObjectsAssignment_7 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1397:1: ( ( rule__Fragment__ObjectsAssignment_7 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1398:1: ( rule__Fragment__ObjectsAssignment_7 )
            {
             before(grammarAccess.getFragmentAccess().getObjectsAssignment_7()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1399:1: ( rule__Fragment__ObjectsAssignment_7 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1399:2: rule__Fragment__ObjectsAssignment_7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__ObjectsAssignment_7_in_rule__Fragment__Group__7__Impl3017);
            rule__Fragment__ObjectsAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getObjectsAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__7__Impl"


    // $ANTLR start "rule__Fragment__Group__8"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1409:1: rule__Fragment__Group__8 : rule__Fragment__Group__8__Impl rule__Fragment__Group__9 ;
    public final void rule__Fragment__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1413:1: ( rule__Fragment__Group__8__Impl rule__Fragment__Group__9 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1414:2: rule__Fragment__Group__8__Impl rule__Fragment__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__8__Impl_in_rule__Fragment__Group__83047);
            rule__Fragment__Group__8__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__9_in_rule__Fragment__Group__83050);
            rule__Fragment__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__8"


    // $ANTLR start "rule__Fragment__Group__8__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1421:1: rule__Fragment__Group__8__Impl : ( ( rule__Fragment__ObjectsAssignment_8 )* ) ;
    public final void rule__Fragment__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1425:1: ( ( ( rule__Fragment__ObjectsAssignment_8 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1426:1: ( ( rule__Fragment__ObjectsAssignment_8 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1426:1: ( ( rule__Fragment__ObjectsAssignment_8 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1427:1: ( rule__Fragment__ObjectsAssignment_8 )*
            {
             before(grammarAccess.getFragmentAccess().getObjectsAssignment_8()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1428:1: ( rule__Fragment__ObjectsAssignment_8 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=RULE_STRING && LA14_0<=RULE_ID)||LA14_0==50) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1428:2: rule__Fragment__ObjectsAssignment_8
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Fragment__ObjectsAssignment_8_in_rule__Fragment__Group__8__Impl3077);
            	    rule__Fragment__ObjectsAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getFragmentAccess().getObjectsAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__8__Impl"


    // $ANTLR start "rule__Fragment__Group__9"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1438:1: rule__Fragment__Group__9 : rule__Fragment__Group__9__Impl ;
    public final void rule__Fragment__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1442:1: ( rule__Fragment__Group__9__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1443:2: rule__Fragment__Group__9__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__9__Impl_in_rule__Fragment__Group__93108);
            rule__Fragment__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__9"


    // $ANTLR start "rule__Fragment__Group__9__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1449:1: rule__Fragment__Group__9__Impl : ( '}' ) ;
    public final void rule__Fragment__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1453:1: ( ( '}' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1454:1: ( '}' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1454:1: ( '}' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1455:1: '}'
            {
             before(grammarAccess.getFragmentAccess().getRightCurlyBracketKeyword_9()); 
            match(input,45,FollowSets000.FOLLOW_45_in_rule__Fragment__Group__9__Impl3136); 
             after(grammarAccess.getFragmentAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__9__Impl"


    // $ANTLR start "rule__Fragment__Group_1__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1488:1: rule__Fragment__Group_1__0 : rule__Fragment__Group_1__0__Impl rule__Fragment__Group_1__1 ;
    public final void rule__Fragment__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1492:1: ( rule__Fragment__Group_1__0__Impl rule__Fragment__Group_1__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1493:2: rule__Fragment__Group_1__0__Impl rule__Fragment__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_1__0__Impl_in_rule__Fragment__Group_1__03187);
            rule__Fragment__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_1__1_in_rule__Fragment__Group_1__03190);
            rule__Fragment__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_1__0"


    // $ANTLR start "rule__Fragment__Group_1__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1500:1: rule__Fragment__Group_1__0__Impl : ( ( rule__Fragment__AnnotationAssignment_1_0 ) ) ;
    public final void rule__Fragment__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1504:1: ( ( ( rule__Fragment__AnnotationAssignment_1_0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1505:1: ( ( rule__Fragment__AnnotationAssignment_1_0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1505:1: ( ( rule__Fragment__AnnotationAssignment_1_0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1506:1: ( rule__Fragment__AnnotationAssignment_1_0 )
            {
             before(grammarAccess.getFragmentAccess().getAnnotationAssignment_1_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1507:1: ( rule__Fragment__AnnotationAssignment_1_0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1507:2: rule__Fragment__AnnotationAssignment_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__AnnotationAssignment_1_0_in_rule__Fragment__Group_1__0__Impl3217);
            rule__Fragment__AnnotationAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getAnnotationAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_1__0__Impl"


    // $ANTLR start "rule__Fragment__Group_1__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1517:1: rule__Fragment__Group_1__1 : rule__Fragment__Group_1__1__Impl ;
    public final void rule__Fragment__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1521:1: ( rule__Fragment__Group_1__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1522:2: rule__Fragment__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_1__1__Impl_in_rule__Fragment__Group_1__13247);
            rule__Fragment__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_1__1"


    // $ANTLR start "rule__Fragment__Group_1__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1528:1: rule__Fragment__Group_1__1__Impl : ( ( rule__Fragment__AnnotationAssignment_1_1 )* ) ;
    public final void rule__Fragment__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1532:1: ( ( ( rule__Fragment__AnnotationAssignment_1_1 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1533:1: ( ( rule__Fragment__AnnotationAssignment_1_1 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1533:1: ( ( rule__Fragment__AnnotationAssignment_1_1 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1534:1: ( rule__Fragment__AnnotationAssignment_1_1 )*
            {
             before(grammarAccess.getFragmentAccess().getAnnotationAssignment_1_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1535:1: ( rule__Fragment__AnnotationAssignment_1_1 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==50) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1535:2: rule__Fragment__AnnotationAssignment_1_1
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Fragment__AnnotationAssignment_1_1_in_rule__Fragment__Group_1__1__Impl3274);
            	    rule__Fragment__AnnotationAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getFragmentAccess().getAnnotationAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_1__1__Impl"


    // $ANTLR start "rule__Fragment__Group_4__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1549:1: rule__Fragment__Group_4__0 : rule__Fragment__Group_4__0__Impl rule__Fragment__Group_4__1 ;
    public final void rule__Fragment__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1553:1: ( rule__Fragment__Group_4__0__Impl rule__Fragment__Group_4__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1554:2: rule__Fragment__Group_4__0__Impl rule__Fragment__Group_4__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__0__Impl_in_rule__Fragment__Group_4__03309);
            rule__Fragment__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__1_in_rule__Fragment__Group_4__03312);
            rule__Fragment__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__0"


    // $ANTLR start "rule__Fragment__Group_4__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1561:1: rule__Fragment__Group_4__0__Impl : ( '[' ) ;
    public final void rule__Fragment__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1565:1: ( ( '[' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1566:1: ( '[' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1566:1: ( '[' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1567:1: '['
            {
             before(grammarAccess.getFragmentAccess().getLeftSquareBracketKeyword_4_0()); 
            match(input,46,FollowSets000.FOLLOW_46_in_rule__Fragment__Group_4__0__Impl3340); 
             after(grammarAccess.getFragmentAccess().getLeftSquareBracketKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__0__Impl"


    // $ANTLR start "rule__Fragment__Group_4__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1580:1: rule__Fragment__Group_4__1 : rule__Fragment__Group_4__1__Impl rule__Fragment__Group_4__2 ;
    public final void rule__Fragment__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1584:1: ( rule__Fragment__Group_4__1__Impl rule__Fragment__Group_4__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1585:2: rule__Fragment__Group_4__1__Impl rule__Fragment__Group_4__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__1__Impl_in_rule__Fragment__Group_4__13371);
            rule__Fragment__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__2_in_rule__Fragment__Group_4__13374);
            rule__Fragment__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__1"


    // $ANTLR start "rule__Fragment__Group_4__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1592:1: rule__Fragment__Group_4__1__Impl : ( ( rule__Fragment__TypeAssignment_4_1 ) ) ;
    public final void rule__Fragment__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1596:1: ( ( ( rule__Fragment__TypeAssignment_4_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1597:1: ( ( rule__Fragment__TypeAssignment_4_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1597:1: ( ( rule__Fragment__TypeAssignment_4_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1598:1: ( rule__Fragment__TypeAssignment_4_1 )
            {
             before(grammarAccess.getFragmentAccess().getTypeAssignment_4_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1599:1: ( rule__Fragment__TypeAssignment_4_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1599:2: rule__Fragment__TypeAssignment_4_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__TypeAssignment_4_1_in_rule__Fragment__Group_4__1__Impl3401);
            rule__Fragment__TypeAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getTypeAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__1__Impl"


    // $ANTLR start "rule__Fragment__Group_4__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1609:1: rule__Fragment__Group_4__2 : rule__Fragment__Group_4__2__Impl ;
    public final void rule__Fragment__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1613:1: ( rule__Fragment__Group_4__2__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1614:2: rule__Fragment__Group_4__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__2__Impl_in_rule__Fragment__Group_4__23431);
            rule__Fragment__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__2"


    // $ANTLR start "rule__Fragment__Group_4__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1620:1: rule__Fragment__Group_4__2__Impl : ( ']' ) ;
    public final void rule__Fragment__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1624:1: ( ( ']' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1625:1: ( ']' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1625:1: ( ']' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1626:1: ']'
            {
             before(grammarAccess.getFragmentAccess().getRightSquareBracketKeyword_4_2()); 
            match(input,47,FollowSets000.FOLLOW_47_in_rule__Fragment__Group_4__2__Impl3459); 
             after(grammarAccess.getFragmentAccess().getRightSquareBracketKeyword_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__2__Impl"


    // $ANTLR start "rule__Object__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1645:1: rule__Object__Group__0 : rule__Object__Group__0__Impl rule__Object__Group__1 ;
    public final void rule__Object__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1649:1: ( rule__Object__Group__0__Impl rule__Object__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1650:2: rule__Object__Group__0__Impl rule__Object__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__0__Impl_in_rule__Object__Group__03496);
            rule__Object__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__1_in_rule__Object__Group__03499);
            rule__Object__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__0"


    // $ANTLR start "rule__Object__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1657:1: rule__Object__Group__0__Impl : ( () ) ;
    public final void rule__Object__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1661:1: ( ( () ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1662:1: ( () )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1662:1: ( () )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1663:1: ()
            {
             before(grammarAccess.getObjectAccess().getObjectAction_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1664:1: ()
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1666:1: 
            {
            }

             after(grammarAccess.getObjectAccess().getObjectAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__0__Impl"


    // $ANTLR start "rule__Object__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1676:1: rule__Object__Group__1 : rule__Object__Group__1__Impl rule__Object__Group__2 ;
    public final void rule__Object__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1680:1: ( rule__Object__Group__1__Impl rule__Object__Group__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1681:2: rule__Object__Group__1__Impl rule__Object__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__1__Impl_in_rule__Object__Group__13557);
            rule__Object__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__2_in_rule__Object__Group__13560);
            rule__Object__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__1"


    // $ANTLR start "rule__Object__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1688:1: rule__Object__Group__1__Impl : ( ( rule__Object__Group_1__0 )? ) ;
    public final void rule__Object__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1692:1: ( ( ( rule__Object__Group_1__0 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1693:1: ( ( rule__Object__Group_1__0 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1693:1: ( ( rule__Object__Group_1__0 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1694:1: ( rule__Object__Group_1__0 )?
            {
             before(grammarAccess.getObjectAccess().getGroup_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1695:1: ( rule__Object__Group_1__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==50) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1695:2: rule__Object__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Object__Group_1__0_in_rule__Object__Group__1__Impl3587);
                    rule__Object__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getObjectAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__1__Impl"


    // $ANTLR start "rule__Object__Group__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1705:1: rule__Object__Group__2 : rule__Object__Group__2__Impl rule__Object__Group__3 ;
    public final void rule__Object__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1709:1: ( rule__Object__Group__2__Impl rule__Object__Group__3 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1710:2: rule__Object__Group__2__Impl rule__Object__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__2__Impl_in_rule__Object__Group__23618);
            rule__Object__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__3_in_rule__Object__Group__23621);
            rule__Object__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__2"


    // $ANTLR start "rule__Object__Group__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1717:1: rule__Object__Group__2__Impl : ( ( rule__Object__NameAssignment_2 ) ) ;
    public final void rule__Object__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1721:1: ( ( ( rule__Object__NameAssignment_2 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1722:1: ( ( rule__Object__NameAssignment_2 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1722:1: ( ( rule__Object__NameAssignment_2 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1723:1: ( rule__Object__NameAssignment_2 )
            {
             before(grammarAccess.getObjectAccess().getNameAssignment_2()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1724:1: ( rule__Object__NameAssignment_2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1724:2: rule__Object__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__NameAssignment_2_in_rule__Object__Group__2__Impl3648);
            rule__Object__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__2__Impl"


    // $ANTLR start "rule__Object__Group__3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1734:1: rule__Object__Group__3 : rule__Object__Group__3__Impl rule__Object__Group__4 ;
    public final void rule__Object__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1738:1: ( rule__Object__Group__3__Impl rule__Object__Group__4 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1739:2: rule__Object__Group__3__Impl rule__Object__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__3__Impl_in_rule__Object__Group__33678);
            rule__Object__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__4_in_rule__Object__Group__33681);
            rule__Object__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__3"


    // $ANTLR start "rule__Object__Group__3__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1746:1: rule__Object__Group__3__Impl : ( ':' ) ;
    public final void rule__Object__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1750:1: ( ( ':' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1751:1: ( ':' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1751:1: ( ':' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1752:1: ':'
            {
             before(grammarAccess.getObjectAccess().getColonKeyword_3()); 
            match(input,48,FollowSets000.FOLLOW_48_in_rule__Object__Group__3__Impl3709); 
             after(grammarAccess.getObjectAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__3__Impl"


    // $ANTLR start "rule__Object__Group__4"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1765:1: rule__Object__Group__4 : rule__Object__Group__4__Impl rule__Object__Group__5 ;
    public final void rule__Object__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1769:1: ( rule__Object__Group__4__Impl rule__Object__Group__5 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1770:2: rule__Object__Group__4__Impl rule__Object__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__4__Impl_in_rule__Object__Group__43740);
            rule__Object__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__5_in_rule__Object__Group__43743);
            rule__Object__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__4"


    // $ANTLR start "rule__Object__Group__4__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1777:1: rule__Object__Group__4__Impl : ( ( rule__Object__TypeAssignment_4 ) ) ;
    public final void rule__Object__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1781:1: ( ( ( rule__Object__TypeAssignment_4 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1782:1: ( ( rule__Object__TypeAssignment_4 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1782:1: ( ( rule__Object__TypeAssignment_4 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1783:1: ( rule__Object__TypeAssignment_4 )
            {
             before(grammarAccess.getObjectAccess().getTypeAssignment_4()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1784:1: ( rule__Object__TypeAssignment_4 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1784:2: rule__Object__TypeAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__TypeAssignment_4_in_rule__Object__Group__4__Impl3770);
            rule__Object__TypeAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getTypeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__4__Impl"


    // $ANTLR start "rule__Object__Group__5"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1794:1: rule__Object__Group__5 : rule__Object__Group__5__Impl rule__Object__Group__6 ;
    public final void rule__Object__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1798:1: ( rule__Object__Group__5__Impl rule__Object__Group__6 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1799:2: rule__Object__Group__5__Impl rule__Object__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__5__Impl_in_rule__Object__Group__53800);
            rule__Object__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__6_in_rule__Object__Group__53803);
            rule__Object__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__5"


    // $ANTLR start "rule__Object__Group__5__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1806:1: rule__Object__Group__5__Impl : ( '{' ) ;
    public final void rule__Object__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1810:1: ( ( '{' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1811:1: ( '{' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1811:1: ( '{' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1812:1: '{'
            {
             before(grammarAccess.getObjectAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,44,FollowSets000.FOLLOW_44_in_rule__Object__Group__5__Impl3831); 
             after(grammarAccess.getObjectAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__5__Impl"


    // $ANTLR start "rule__Object__Group__6"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1825:1: rule__Object__Group__6 : rule__Object__Group__6__Impl rule__Object__Group__7 ;
    public final void rule__Object__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1829:1: ( rule__Object__Group__6__Impl rule__Object__Group__7 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1830:2: rule__Object__Group__6__Impl rule__Object__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__6__Impl_in_rule__Object__Group__63862);
            rule__Object__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__7_in_rule__Object__Group__63865);
            rule__Object__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__6"


    // $ANTLR start "rule__Object__Group__6__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1837:1: rule__Object__Group__6__Impl : ( ( rule__Object__Group_6__0 )? ) ;
    public final void rule__Object__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1841:1: ( ( ( rule__Object__Group_6__0 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1842:1: ( ( rule__Object__Group_6__0 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1842:1: ( ( rule__Object__Group_6__0 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1843:1: ( rule__Object__Group_6__0 )?
            {
             before(grammarAccess.getObjectAccess().getGroup_6()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1844:1: ( rule__Object__Group_6__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ((LA17_0>=11 && LA17_0<=12)||LA17_0==50||LA17_0==57) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1844:2: rule__Object__Group_6__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__0_in_rule__Object__Group__6__Impl3892);
                    rule__Object__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getObjectAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__6__Impl"


    // $ANTLR start "rule__Object__Group__7"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1854:1: rule__Object__Group__7 : rule__Object__Group__7__Impl ;
    public final void rule__Object__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1858:1: ( rule__Object__Group__7__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1859:2: rule__Object__Group__7__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__7__Impl_in_rule__Object__Group__73923);
            rule__Object__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__7"


    // $ANTLR start "rule__Object__Group__7__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1865:1: rule__Object__Group__7__Impl : ( '}' ) ;
    public final void rule__Object__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1869:1: ( ( '}' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1870:1: ( '}' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1870:1: ( '}' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1871:1: '}'
            {
             before(grammarAccess.getObjectAccess().getRightCurlyBracketKeyword_7()); 
            match(input,45,FollowSets000.FOLLOW_45_in_rule__Object__Group__7__Impl3951); 
             after(grammarAccess.getObjectAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__7__Impl"


    // $ANTLR start "rule__Object__Group_1__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1900:1: rule__Object__Group_1__0 : rule__Object__Group_1__0__Impl rule__Object__Group_1__1 ;
    public final void rule__Object__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1904:1: ( rule__Object__Group_1__0__Impl rule__Object__Group_1__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1905:2: rule__Object__Group_1__0__Impl rule__Object__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_1__0__Impl_in_rule__Object__Group_1__03998);
            rule__Object__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_1__1_in_rule__Object__Group_1__04001);
            rule__Object__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_1__0"


    // $ANTLR start "rule__Object__Group_1__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1912:1: rule__Object__Group_1__0__Impl : ( ( rule__Object__AnnotationAssignment_1_0 ) ) ;
    public final void rule__Object__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1916:1: ( ( ( rule__Object__AnnotationAssignment_1_0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1917:1: ( ( rule__Object__AnnotationAssignment_1_0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1917:1: ( ( rule__Object__AnnotationAssignment_1_0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1918:1: ( rule__Object__AnnotationAssignment_1_0 )
            {
             before(grammarAccess.getObjectAccess().getAnnotationAssignment_1_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1919:1: ( rule__Object__AnnotationAssignment_1_0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1919:2: rule__Object__AnnotationAssignment_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__AnnotationAssignment_1_0_in_rule__Object__Group_1__0__Impl4028);
            rule__Object__AnnotationAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getAnnotationAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_1__0__Impl"


    // $ANTLR start "rule__Object__Group_1__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1929:1: rule__Object__Group_1__1 : rule__Object__Group_1__1__Impl ;
    public final void rule__Object__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1933:1: ( rule__Object__Group_1__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1934:2: rule__Object__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_1__1__Impl_in_rule__Object__Group_1__14058);
            rule__Object__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_1__1"


    // $ANTLR start "rule__Object__Group_1__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1940:1: rule__Object__Group_1__1__Impl : ( ( rule__Object__AnnotationAssignment_1_1 )* ) ;
    public final void rule__Object__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1944:1: ( ( ( rule__Object__AnnotationAssignment_1_1 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1945:1: ( ( rule__Object__AnnotationAssignment_1_1 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1945:1: ( ( rule__Object__AnnotationAssignment_1_1 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1946:1: ( rule__Object__AnnotationAssignment_1_1 )*
            {
             before(grammarAccess.getObjectAccess().getAnnotationAssignment_1_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1947:1: ( rule__Object__AnnotationAssignment_1_1 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==50) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1947:2: rule__Object__AnnotationAssignment_1_1
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Object__AnnotationAssignment_1_1_in_rule__Object__Group_1__1__Impl4085);
            	    rule__Object__AnnotationAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getObjectAccess().getAnnotationAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_1__1__Impl"


    // $ANTLR start "rule__Object__Group_6__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1961:1: rule__Object__Group_6__0 : rule__Object__Group_6__0__Impl rule__Object__Group_6__1 ;
    public final void rule__Object__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1965:1: ( rule__Object__Group_6__0__Impl rule__Object__Group_6__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1966:2: rule__Object__Group_6__0__Impl rule__Object__Group_6__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__0__Impl_in_rule__Object__Group_6__04120);
            rule__Object__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__1_in_rule__Object__Group_6__04123);
            rule__Object__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__0"


    // $ANTLR start "rule__Object__Group_6__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1973:1: rule__Object__Group_6__0__Impl : ( ( rule__Object__FeaturesAssignment_6_0 ) ) ;
    public final void rule__Object__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1977:1: ( ( ( rule__Object__FeaturesAssignment_6_0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1978:1: ( ( rule__Object__FeaturesAssignment_6_0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1978:1: ( ( rule__Object__FeaturesAssignment_6_0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1979:1: ( rule__Object__FeaturesAssignment_6_0 )
            {
             before(grammarAccess.getObjectAccess().getFeaturesAssignment_6_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1980:1: ( rule__Object__FeaturesAssignment_6_0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1980:2: rule__Object__FeaturesAssignment_6_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__FeaturesAssignment_6_0_in_rule__Object__Group_6__0__Impl4150);
            rule__Object__FeaturesAssignment_6_0();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getFeaturesAssignment_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__0__Impl"


    // $ANTLR start "rule__Object__Group_6__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1990:1: rule__Object__Group_6__1 : rule__Object__Group_6__1__Impl rule__Object__Group_6__2 ;
    public final void rule__Object__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1994:1: ( rule__Object__Group_6__1__Impl rule__Object__Group_6__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:1995:2: rule__Object__Group_6__1__Impl rule__Object__Group_6__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__1__Impl_in_rule__Object__Group_6__14180);
            rule__Object__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__2_in_rule__Object__Group_6__14183);
            rule__Object__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__1"


    // $ANTLR start "rule__Object__Group_6__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2002:1: rule__Object__Group_6__1__Impl : ( ( ';' )? ) ;
    public final void rule__Object__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2006:1: ( ( ( ';' )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2007:1: ( ( ';' )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2007:1: ( ( ';' )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2008:1: ( ';' )?
            {
             before(grammarAccess.getObjectAccess().getSemicolonKeyword_6_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2009:1: ( ';' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==49) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2010:2: ';'
                    {
                    match(input,49,FollowSets000.FOLLOW_49_in_rule__Object__Group_6__1__Impl4212); 

                    }
                    break;

            }

             after(grammarAccess.getObjectAccess().getSemicolonKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__1__Impl"


    // $ANTLR start "rule__Object__Group_6__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2021:1: rule__Object__Group_6__2 : rule__Object__Group_6__2__Impl ;
    public final void rule__Object__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2025:1: ( rule__Object__Group_6__2__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2026:2: rule__Object__Group_6__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__2__Impl_in_rule__Object__Group_6__24245);
            rule__Object__Group_6__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__2"


    // $ANTLR start "rule__Object__Group_6__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2032:1: rule__Object__Group_6__2__Impl : ( ( rule__Object__Group_6_2__0 )* ) ;
    public final void rule__Object__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2036:1: ( ( ( rule__Object__Group_6_2__0 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2037:1: ( ( rule__Object__Group_6_2__0 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2037:1: ( ( rule__Object__Group_6_2__0 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2038:1: ( rule__Object__Group_6_2__0 )*
            {
             before(grammarAccess.getObjectAccess().getGroup_6_2()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2039:1: ( rule__Object__Group_6_2__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( ((LA20_0>=11 && LA20_0<=12)||LA20_0==50||LA20_0==57) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2039:2: rule__Object__Group_6_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6_2__0_in_rule__Object__Group_6__2__Impl4272);
            	    rule__Object__Group_6_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getObjectAccess().getGroup_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__2__Impl"


    // $ANTLR start "rule__Object__Group_6_2__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2055:1: rule__Object__Group_6_2__0 : rule__Object__Group_6_2__0__Impl rule__Object__Group_6_2__1 ;
    public final void rule__Object__Group_6_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2059:1: ( rule__Object__Group_6_2__0__Impl rule__Object__Group_6_2__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2060:2: rule__Object__Group_6_2__0__Impl rule__Object__Group_6_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6_2__0__Impl_in_rule__Object__Group_6_2__04309);
            rule__Object__Group_6_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6_2__1_in_rule__Object__Group_6_2__04312);
            rule__Object__Group_6_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6_2__0"


    // $ANTLR start "rule__Object__Group_6_2__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2067:1: rule__Object__Group_6_2__0__Impl : ( ( rule__Object__FeaturesAssignment_6_2_0 ) ) ;
    public final void rule__Object__Group_6_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2071:1: ( ( ( rule__Object__FeaturesAssignment_6_2_0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2072:1: ( ( rule__Object__FeaturesAssignment_6_2_0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2072:1: ( ( rule__Object__FeaturesAssignment_6_2_0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2073:1: ( rule__Object__FeaturesAssignment_6_2_0 )
            {
             before(grammarAccess.getObjectAccess().getFeaturesAssignment_6_2_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2074:1: ( rule__Object__FeaturesAssignment_6_2_0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2074:2: rule__Object__FeaturesAssignment_6_2_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__FeaturesAssignment_6_2_0_in_rule__Object__Group_6_2__0__Impl4339);
            rule__Object__FeaturesAssignment_6_2_0();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getFeaturesAssignment_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6_2__0__Impl"


    // $ANTLR start "rule__Object__Group_6_2__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2084:1: rule__Object__Group_6_2__1 : rule__Object__Group_6_2__1__Impl ;
    public final void rule__Object__Group_6_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2088:1: ( rule__Object__Group_6_2__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2089:2: rule__Object__Group_6_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6_2__1__Impl_in_rule__Object__Group_6_2__14369);
            rule__Object__Group_6_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6_2__1"


    // $ANTLR start "rule__Object__Group_6_2__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2095:1: rule__Object__Group_6_2__1__Impl : ( ( ';' )? ) ;
    public final void rule__Object__Group_6_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2099:1: ( ( ( ';' )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2100:1: ( ( ';' )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2100:1: ( ( ';' )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2101:1: ( ';' )?
            {
             before(grammarAccess.getObjectAccess().getSemicolonKeyword_6_2_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2102:1: ( ';' )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==49) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2103:2: ';'
                    {
                    match(input,49,FollowSets000.FOLLOW_49_in_rule__Object__Group_6_2__1__Impl4398); 

                    }
                    break;

            }

             after(grammarAccess.getObjectAccess().getSemicolonKeyword_6_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6_2__1__Impl"


    // $ANTLR start "rule__Annotation__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2118:1: rule__Annotation__Group__0 : rule__Annotation__Group__0__Impl rule__Annotation__Group__1 ;
    public final void rule__Annotation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2122:1: ( rule__Annotation__Group__0__Impl rule__Annotation__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2123:2: rule__Annotation__Group__0__Impl rule__Annotation__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__0__Impl_in_rule__Annotation__Group__04435);
            rule__Annotation__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__1_in_rule__Annotation__Group__04438);
            rule__Annotation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__0"


    // $ANTLR start "rule__Annotation__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2130:1: rule__Annotation__Group__0__Impl : ( '@' ) ;
    public final void rule__Annotation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2134:1: ( ( '@' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2135:1: ( '@' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2135:1: ( '@' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2136:1: '@'
            {
             before(grammarAccess.getAnnotationAccess().getCommercialAtKeyword_0()); 
            match(input,50,FollowSets000.FOLLOW_50_in_rule__Annotation__Group__0__Impl4466); 
             after(grammarAccess.getAnnotationAccess().getCommercialAtKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__0__Impl"


    // $ANTLR start "rule__Annotation__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2149:1: rule__Annotation__Group__1 : rule__Annotation__Group__1__Impl rule__Annotation__Group__2 ;
    public final void rule__Annotation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2153:1: ( rule__Annotation__Group__1__Impl rule__Annotation__Group__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2154:2: rule__Annotation__Group__1__Impl rule__Annotation__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__1__Impl_in_rule__Annotation__Group__14497);
            rule__Annotation__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__2_in_rule__Annotation__Group__14500);
            rule__Annotation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__1"


    // $ANTLR start "rule__Annotation__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2161:1: rule__Annotation__Group__1__Impl : ( ( rule__Annotation__NameAssignment_1 ) ) ;
    public final void rule__Annotation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2165:1: ( ( ( rule__Annotation__NameAssignment_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2166:1: ( ( rule__Annotation__NameAssignment_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2166:1: ( ( rule__Annotation__NameAssignment_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2167:1: ( rule__Annotation__NameAssignment_1 )
            {
             before(grammarAccess.getAnnotationAccess().getNameAssignment_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2168:1: ( rule__Annotation__NameAssignment_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2168:2: rule__Annotation__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__NameAssignment_1_in_rule__Annotation__Group__1__Impl4527);
            rule__Annotation__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__1__Impl"


    // $ANTLR start "rule__Annotation__Group__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2178:1: rule__Annotation__Group__2 : rule__Annotation__Group__2__Impl ;
    public final void rule__Annotation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2182:1: ( rule__Annotation__Group__2__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2183:2: rule__Annotation__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__2__Impl_in_rule__Annotation__Group__24557);
            rule__Annotation__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__2"


    // $ANTLR start "rule__Annotation__Group__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2189:1: rule__Annotation__Group__2__Impl : ( ( rule__Annotation__Group_2__0 )? ) ;
    public final void rule__Annotation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2193:1: ( ( ( rule__Annotation__Group_2__0 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2194:1: ( ( rule__Annotation__Group_2__0 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2194:1: ( ( rule__Annotation__Group_2__0 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2195:1: ( rule__Annotation__Group_2__0 )?
            {
             before(grammarAccess.getAnnotationAccess().getGroup_2()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2196:1: ( rule__Annotation__Group_2__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==51) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2196:2: rule__Annotation__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__0_in_rule__Annotation__Group__2__Impl4584);
                    rule__Annotation__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAnnotationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__2__Impl"


    // $ANTLR start "rule__Annotation__Group_2__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2212:1: rule__Annotation__Group_2__0 : rule__Annotation__Group_2__0__Impl rule__Annotation__Group_2__1 ;
    public final void rule__Annotation__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2216:1: ( rule__Annotation__Group_2__0__Impl rule__Annotation__Group_2__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2217:2: rule__Annotation__Group_2__0__Impl rule__Annotation__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__0__Impl_in_rule__Annotation__Group_2__04621);
            rule__Annotation__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__1_in_rule__Annotation__Group_2__04624);
            rule__Annotation__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__0"


    // $ANTLR start "rule__Annotation__Group_2__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2224:1: rule__Annotation__Group_2__0__Impl : ( '(' ) ;
    public final void rule__Annotation__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2228:1: ( ( '(' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2229:1: ( '(' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2229:1: ( '(' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2230:1: '('
            {
             before(grammarAccess.getAnnotationAccess().getLeftParenthesisKeyword_2_0()); 
            match(input,51,FollowSets000.FOLLOW_51_in_rule__Annotation__Group_2__0__Impl4652); 
             after(grammarAccess.getAnnotationAccess().getLeftParenthesisKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__0__Impl"


    // $ANTLR start "rule__Annotation__Group_2__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2243:1: rule__Annotation__Group_2__1 : rule__Annotation__Group_2__1__Impl rule__Annotation__Group_2__2 ;
    public final void rule__Annotation__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2247:1: ( rule__Annotation__Group_2__1__Impl rule__Annotation__Group_2__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2248:2: rule__Annotation__Group_2__1__Impl rule__Annotation__Group_2__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__1__Impl_in_rule__Annotation__Group_2__14683);
            rule__Annotation__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__2_in_rule__Annotation__Group_2__14686);
            rule__Annotation__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__1"


    // $ANTLR start "rule__Annotation__Group_2__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2255:1: rule__Annotation__Group_2__1__Impl : ( ( rule__Annotation__ParamsAssignment_2_1 ) ) ;
    public final void rule__Annotation__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2259:1: ( ( ( rule__Annotation__ParamsAssignment_2_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2260:1: ( ( rule__Annotation__ParamsAssignment_2_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2260:1: ( ( rule__Annotation__ParamsAssignment_2_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2261:1: ( rule__Annotation__ParamsAssignment_2_1 )
            {
             before(grammarAccess.getAnnotationAccess().getParamsAssignment_2_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2262:1: ( rule__Annotation__ParamsAssignment_2_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2262:2: rule__Annotation__ParamsAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__ParamsAssignment_2_1_in_rule__Annotation__Group_2__1__Impl4713);
            rule__Annotation__ParamsAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationAccess().getParamsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__1__Impl"


    // $ANTLR start "rule__Annotation__Group_2__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2272:1: rule__Annotation__Group_2__2 : rule__Annotation__Group_2__2__Impl rule__Annotation__Group_2__3 ;
    public final void rule__Annotation__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2276:1: ( rule__Annotation__Group_2__2__Impl rule__Annotation__Group_2__3 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2277:2: rule__Annotation__Group_2__2__Impl rule__Annotation__Group_2__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__2__Impl_in_rule__Annotation__Group_2__24743);
            rule__Annotation__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__3_in_rule__Annotation__Group_2__24746);
            rule__Annotation__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__2"


    // $ANTLR start "rule__Annotation__Group_2__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2284:1: rule__Annotation__Group_2__2__Impl : ( ( rule__Annotation__Group_2_2__0 )* ) ;
    public final void rule__Annotation__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2288:1: ( ( ( rule__Annotation__Group_2_2__0 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2289:1: ( ( rule__Annotation__Group_2_2__0 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2289:1: ( ( rule__Annotation__Group_2_2__0 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2290:1: ( rule__Annotation__Group_2_2__0 )*
            {
             before(grammarAccess.getAnnotationAccess().getGroup_2_2()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2291:1: ( rule__Annotation__Group_2_2__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==53) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2291:2: rule__Annotation__Group_2_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2_2__0_in_rule__Annotation__Group_2__2__Impl4773);
            	    rule__Annotation__Group_2_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getAnnotationAccess().getGroup_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__2__Impl"


    // $ANTLR start "rule__Annotation__Group_2__3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2301:1: rule__Annotation__Group_2__3 : rule__Annotation__Group_2__3__Impl ;
    public final void rule__Annotation__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2305:1: ( rule__Annotation__Group_2__3__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2306:2: rule__Annotation__Group_2__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__3__Impl_in_rule__Annotation__Group_2__34804);
            rule__Annotation__Group_2__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__3"


    // $ANTLR start "rule__Annotation__Group_2__3__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2312:1: rule__Annotation__Group_2__3__Impl : ( ')' ) ;
    public final void rule__Annotation__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2316:1: ( ( ')' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2317:1: ( ')' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2317:1: ( ')' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2318:1: ')'
            {
             before(grammarAccess.getAnnotationAccess().getRightParenthesisKeyword_2_3()); 
            match(input,52,FollowSets000.FOLLOW_52_in_rule__Annotation__Group_2__3__Impl4832); 
             after(grammarAccess.getAnnotationAccess().getRightParenthesisKeyword_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__3__Impl"


    // $ANTLR start "rule__Annotation__Group_2_2__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2339:1: rule__Annotation__Group_2_2__0 : rule__Annotation__Group_2_2__0__Impl rule__Annotation__Group_2_2__1 ;
    public final void rule__Annotation__Group_2_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2343:1: ( rule__Annotation__Group_2_2__0__Impl rule__Annotation__Group_2_2__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2344:2: rule__Annotation__Group_2_2__0__Impl rule__Annotation__Group_2_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2_2__0__Impl_in_rule__Annotation__Group_2_2__04871);
            rule__Annotation__Group_2_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2_2__1_in_rule__Annotation__Group_2_2__04874);
            rule__Annotation__Group_2_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2_2__0"


    // $ANTLR start "rule__Annotation__Group_2_2__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2351:1: rule__Annotation__Group_2_2__0__Impl : ( ',' ) ;
    public final void rule__Annotation__Group_2_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2355:1: ( ( ',' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2356:1: ( ',' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2356:1: ( ',' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2357:1: ','
            {
             before(grammarAccess.getAnnotationAccess().getCommaKeyword_2_2_0()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__Annotation__Group_2_2__0__Impl4902); 
             after(grammarAccess.getAnnotationAccess().getCommaKeyword_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2_2__0__Impl"


    // $ANTLR start "rule__Annotation__Group_2_2__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2370:1: rule__Annotation__Group_2_2__1 : rule__Annotation__Group_2_2__1__Impl ;
    public final void rule__Annotation__Group_2_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2374:1: ( rule__Annotation__Group_2_2__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2375:2: rule__Annotation__Group_2_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2_2__1__Impl_in_rule__Annotation__Group_2_2__14933);
            rule__Annotation__Group_2_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2_2__1"


    // $ANTLR start "rule__Annotation__Group_2_2__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2381:1: rule__Annotation__Group_2_2__1__Impl : ( ( rule__Annotation__ParamsAssignment_2_2_1 ) ) ;
    public final void rule__Annotation__Group_2_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2385:1: ( ( ( rule__Annotation__ParamsAssignment_2_2_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2386:1: ( ( rule__Annotation__ParamsAssignment_2_2_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2386:1: ( ( rule__Annotation__ParamsAssignment_2_2_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2387:1: ( rule__Annotation__ParamsAssignment_2_2_1 )
            {
             before(grammarAccess.getAnnotationAccess().getParamsAssignment_2_2_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2388:1: ( rule__Annotation__ParamsAssignment_2_2_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2388:2: rule__Annotation__ParamsAssignment_2_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__ParamsAssignment_2_2_1_in_rule__Annotation__Group_2_2__1__Impl4960);
            rule__Annotation__ParamsAssignment_2_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationAccess().getParamsAssignment_2_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2_2__1__Impl"


    // $ANTLR start "rule__AnnotationParam__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2402:1: rule__AnnotationParam__Group__0 : rule__AnnotationParam__Group__0__Impl rule__AnnotationParam__Group__1 ;
    public final void rule__AnnotationParam__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2406:1: ( rule__AnnotationParam__Group__0__Impl rule__AnnotationParam__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2407:2: rule__AnnotationParam__Group__0__Impl rule__AnnotationParam__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__Group__0__Impl_in_rule__AnnotationParam__Group__04994);
            rule__AnnotationParam__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__Group__1_in_rule__AnnotationParam__Group__04997);
            rule__AnnotationParam__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__Group__0"


    // $ANTLR start "rule__AnnotationParam__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2414:1: rule__AnnotationParam__Group__0__Impl : ( ( rule__AnnotationParam__NameAssignment_0 ) ) ;
    public final void rule__AnnotationParam__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2418:1: ( ( ( rule__AnnotationParam__NameAssignment_0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2419:1: ( ( rule__AnnotationParam__NameAssignment_0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2419:1: ( ( rule__AnnotationParam__NameAssignment_0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2420:1: ( rule__AnnotationParam__NameAssignment_0 )
            {
             before(grammarAccess.getAnnotationParamAccess().getNameAssignment_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2421:1: ( rule__AnnotationParam__NameAssignment_0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2421:2: rule__AnnotationParam__NameAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__NameAssignment_0_in_rule__AnnotationParam__Group__0__Impl5024);
            rule__AnnotationParam__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationParamAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__Group__0__Impl"


    // $ANTLR start "rule__AnnotationParam__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2431:1: rule__AnnotationParam__Group__1 : rule__AnnotationParam__Group__1__Impl ;
    public final void rule__AnnotationParam__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2435:1: ( rule__AnnotationParam__Group__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2436:2: rule__AnnotationParam__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__Group__1__Impl_in_rule__AnnotationParam__Group__15054);
            rule__AnnotationParam__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__Group__1"


    // $ANTLR start "rule__AnnotationParam__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2442:1: rule__AnnotationParam__Group__1__Impl : ( ( rule__AnnotationParam__ParamValueAssignment_1 ) ) ;
    public final void rule__AnnotationParam__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2446:1: ( ( ( rule__AnnotationParam__ParamValueAssignment_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2447:1: ( ( rule__AnnotationParam__ParamValueAssignment_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2447:1: ( ( rule__AnnotationParam__ParamValueAssignment_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2448:1: ( rule__AnnotationParam__ParamValueAssignment_1 )
            {
             before(grammarAccess.getAnnotationParamAccess().getParamValueAssignment_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2449:1: ( rule__AnnotationParam__ParamValueAssignment_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2449:2: rule__AnnotationParam__ParamValueAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__ParamValueAssignment_1_in_rule__AnnotationParam__Group__1__Impl5081);
            rule__AnnotationParam__ParamValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationParamAccess().getParamValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__Group__1__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2463:1: rule__ObjectValueAnnotationParam__Group__0 : rule__ObjectValueAnnotationParam__Group__0__Impl rule__ObjectValueAnnotationParam__Group__1 ;
    public final void rule__ObjectValueAnnotationParam__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2467:1: ( rule__ObjectValueAnnotationParam__Group__0__Impl rule__ObjectValueAnnotationParam__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2468:2: rule__ObjectValueAnnotationParam__Group__0__Impl rule__ObjectValueAnnotationParam__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__0__Impl_in_rule__ObjectValueAnnotationParam__Group__05115);
            rule__ObjectValueAnnotationParam__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__1_in_rule__ObjectValueAnnotationParam__Group__05118);
            rule__ObjectValueAnnotationParam__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__0"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2475:1: rule__ObjectValueAnnotationParam__Group__0__Impl : ( '->' ) ;
    public final void rule__ObjectValueAnnotationParam__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2479:1: ( ( '->' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2480:1: ( '->' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2480:1: ( '->' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2481:1: '->'
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getHyphenMinusGreaterThanSignKeyword_0()); 
            match(input,54,FollowSets000.FOLLOW_54_in_rule__ObjectValueAnnotationParam__Group__0__Impl5146); 
             after(grammarAccess.getObjectValueAnnotationParamAccess().getHyphenMinusGreaterThanSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__0__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2494:1: rule__ObjectValueAnnotationParam__Group__1 : rule__ObjectValueAnnotationParam__Group__1__Impl rule__ObjectValueAnnotationParam__Group__2 ;
    public final void rule__ObjectValueAnnotationParam__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2498:1: ( rule__ObjectValueAnnotationParam__Group__1__Impl rule__ObjectValueAnnotationParam__Group__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2499:2: rule__ObjectValueAnnotationParam__Group__1__Impl rule__ObjectValueAnnotationParam__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__1__Impl_in_rule__ObjectValueAnnotationParam__Group__15177);
            rule__ObjectValueAnnotationParam__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__2_in_rule__ObjectValueAnnotationParam__Group__15180);
            rule__ObjectValueAnnotationParam__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__1"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2506:1: rule__ObjectValueAnnotationParam__Group__1__Impl : ( ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 ) ) ;
    public final void rule__ObjectValueAnnotationParam__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2510:1: ( ( ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2511:1: ( ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2511:1: ( ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2512:1: ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getObjectAssignment_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2513:1: ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2513:2: rule__ObjectValueAnnotationParam__ObjectAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__ObjectAssignment_1_in_rule__ObjectValueAnnotationParam__Group__1__Impl5207);
            rule__ObjectValueAnnotationParam__ObjectAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getObjectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__1__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2523:1: rule__ObjectValueAnnotationParam__Group__2 : rule__ObjectValueAnnotationParam__Group__2__Impl ;
    public final void rule__ObjectValueAnnotationParam__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2527:1: ( rule__ObjectValueAnnotationParam__Group__2__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2528:2: rule__ObjectValueAnnotationParam__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__2__Impl_in_rule__ObjectValueAnnotationParam__Group__25237);
            rule__ObjectValueAnnotationParam__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__2"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2534:1: rule__ObjectValueAnnotationParam__Group__2__Impl : ( ( rule__ObjectValueAnnotationParam__Group_2__0 )? ) ;
    public final void rule__ObjectValueAnnotationParam__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2538:1: ( ( ( rule__ObjectValueAnnotationParam__Group_2__0 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2539:1: ( ( rule__ObjectValueAnnotationParam__Group_2__0 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2539:1: ( ( rule__ObjectValueAnnotationParam__Group_2__0 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2540:1: ( rule__ObjectValueAnnotationParam__Group_2__0 )?
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getGroup_2()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2541:1: ( rule__ObjectValueAnnotationParam__Group_2__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==55) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2541:2: rule__ObjectValueAnnotationParam__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group_2__0_in_rule__ObjectValueAnnotationParam__Group__2__Impl5264);
                    rule__ObjectValueAnnotationParam__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__2__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group_2__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2557:1: rule__ObjectValueAnnotationParam__Group_2__0 : rule__ObjectValueAnnotationParam__Group_2__0__Impl rule__ObjectValueAnnotationParam__Group_2__1 ;
    public final void rule__ObjectValueAnnotationParam__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2561:1: ( rule__ObjectValueAnnotationParam__Group_2__0__Impl rule__ObjectValueAnnotationParam__Group_2__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2562:2: rule__ObjectValueAnnotationParam__Group_2__0__Impl rule__ObjectValueAnnotationParam__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group_2__0__Impl_in_rule__ObjectValueAnnotationParam__Group_2__05301);
            rule__ObjectValueAnnotationParam__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group_2__1_in_rule__ObjectValueAnnotationParam__Group_2__05304);
            rule__ObjectValueAnnotationParam__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group_2__0"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group_2__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2569:1: rule__ObjectValueAnnotationParam__Group_2__0__Impl : ( '.' ) ;
    public final void rule__ObjectValueAnnotationParam__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2573:1: ( ( '.' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2574:1: ( '.' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2574:1: ( '.' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2575:1: '.'
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getFullStopKeyword_2_0()); 
            match(input,55,FollowSets000.FOLLOW_55_in_rule__ObjectValueAnnotationParam__Group_2__0__Impl5332); 
             after(grammarAccess.getObjectValueAnnotationParamAccess().getFullStopKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group_2__0__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group_2__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2588:1: rule__ObjectValueAnnotationParam__Group_2__1 : rule__ObjectValueAnnotationParam__Group_2__1__Impl ;
    public final void rule__ObjectValueAnnotationParam__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2592:1: ( rule__ObjectValueAnnotationParam__Group_2__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2593:2: rule__ObjectValueAnnotationParam__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group_2__1__Impl_in_rule__ObjectValueAnnotationParam__Group_2__15363);
            rule__ObjectValueAnnotationParam__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group_2__1"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group_2__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2599:1: rule__ObjectValueAnnotationParam__Group_2__1__Impl : ( ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 ) ) ;
    public final void rule__ObjectValueAnnotationParam__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2603:1: ( ( ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2604:1: ( ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2604:1: ( ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2605:1: ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureAssignment_2_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2606:1: ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2606:2: rule__ObjectValueAnnotationParam__FeatureAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__FeatureAssignment_2_1_in_rule__ObjectValueAnnotationParam__Group_2__1__Impl5390);
            rule__ObjectValueAnnotationParam__FeatureAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group_2__1__Impl"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2620:1: rule__PrimitiveValueAnnotationParam__Group__0 : rule__PrimitiveValueAnnotationParam__Group__0__Impl rule__PrimitiveValueAnnotationParam__Group__1 ;
    public final void rule__PrimitiveValueAnnotationParam__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2624:1: ( rule__PrimitiveValueAnnotationParam__Group__0__Impl rule__PrimitiveValueAnnotationParam__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2625:2: rule__PrimitiveValueAnnotationParam__Group__0__Impl rule__PrimitiveValueAnnotationParam__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__Group__0__Impl_in_rule__PrimitiveValueAnnotationParam__Group__05424);
            rule__PrimitiveValueAnnotationParam__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__Group__1_in_rule__PrimitiveValueAnnotationParam__Group__05427);
            rule__PrimitiveValueAnnotationParam__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__Group__0"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2632:1: rule__PrimitiveValueAnnotationParam__Group__0__Impl : ( '=' ) ;
    public final void rule__PrimitiveValueAnnotationParam__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2636:1: ( ( '=' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2637:1: ( '=' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2637:1: ( '=' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2638:1: '='
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamAccess().getEqualsSignKeyword_0()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__PrimitiveValueAnnotationParam__Group__0__Impl5455); 
             after(grammarAccess.getPrimitiveValueAnnotationParamAccess().getEqualsSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__Group__0__Impl"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2651:1: rule__PrimitiveValueAnnotationParam__Group__1 : rule__PrimitiveValueAnnotationParam__Group__1__Impl ;
    public final void rule__PrimitiveValueAnnotationParam__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2655:1: ( rule__PrimitiveValueAnnotationParam__Group__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2656:2: rule__PrimitiveValueAnnotationParam__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__Group__1__Impl_in_rule__PrimitiveValueAnnotationParam__Group__15486);
            rule__PrimitiveValueAnnotationParam__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__Group__1"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2662:1: rule__PrimitiveValueAnnotationParam__Group__1__Impl : ( ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 ) ) ;
    public final void rule__PrimitiveValueAnnotationParam__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2666:1: ( ( ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2667:1: ( ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2667:1: ( ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2668:1: ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 )
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValueAssignment_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2669:1: ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2669:2: rule__PrimitiveValueAnnotationParam__ValueAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__ValueAssignment_1_in_rule__PrimitiveValueAnnotationParam__Group__1__Impl5513);
            rule__PrimitiveValueAnnotationParam__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2683:1: rule__Attribute__Group__0 : rule__Attribute__Group__0__Impl rule__Attribute__Group__1 ;
    public final void rule__Attribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2687:1: ( rule__Attribute__Group__0__Impl rule__Attribute__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2688:2: rule__Attribute__Group__0__Impl rule__Attribute__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__0__Impl_in_rule__Attribute__Group__05547);
            rule__Attribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__1_in_rule__Attribute__Group__05550);
            rule__Attribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0"


    // $ANTLR start "rule__Attribute__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2695:1: rule__Attribute__Group__0__Impl : ( () ) ;
    public final void rule__Attribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2699:1: ( ( () ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2700:1: ( () )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2700:1: ( () )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2701:1: ()
            {
             before(grammarAccess.getAttributeAccess().getAttributeAction_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2702:1: ()
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2704:1: 
            {
            }

             after(grammarAccess.getAttributeAccess().getAttributeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0__Impl"


    // $ANTLR start "rule__Attribute__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2714:1: rule__Attribute__Group__1 : rule__Attribute__Group__1__Impl rule__Attribute__Group__2 ;
    public final void rule__Attribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2718:1: ( rule__Attribute__Group__1__Impl rule__Attribute__Group__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2719:2: rule__Attribute__Group__1__Impl rule__Attribute__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__1__Impl_in_rule__Attribute__Group__15608);
            rule__Attribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__2_in_rule__Attribute__Group__15611);
            rule__Attribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1"


    // $ANTLR start "rule__Attribute__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2726:1: rule__Attribute__Group__1__Impl : ( ( rule__Attribute__Group_1__0 )? ) ;
    public final void rule__Attribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2730:1: ( ( ( rule__Attribute__Group_1__0 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2731:1: ( ( rule__Attribute__Group_1__0 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2731:1: ( ( rule__Attribute__Group_1__0 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2732:1: ( rule__Attribute__Group_1__0 )?
            {
             before(grammarAccess.getAttributeAccess().getGroup_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2733:1: ( rule__Attribute__Group_1__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==50) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2733:2: rule__Attribute__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_1__0_in_rule__Attribute__Group__1__Impl5638);
                    rule__Attribute__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttributeAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2743:1: rule__Attribute__Group__2 : rule__Attribute__Group__2__Impl rule__Attribute__Group__3 ;
    public final void rule__Attribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2747:1: ( rule__Attribute__Group__2__Impl rule__Attribute__Group__3 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2748:2: rule__Attribute__Group__2__Impl rule__Attribute__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__2__Impl_in_rule__Attribute__Group__25669);
            rule__Attribute__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__3_in_rule__Attribute__Group__25672);
            rule__Attribute__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2"


    // $ANTLR start "rule__Attribute__Group__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2755:1: rule__Attribute__Group__2__Impl : ( 'attr' ) ;
    public final void rule__Attribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2759:1: ( ( 'attr' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2760:1: ( 'attr' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2760:1: ( 'attr' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2761:1: 'attr'
            {
             before(grammarAccess.getAttributeAccess().getAttrKeyword_2()); 
            match(input,57,FollowSets000.FOLLOW_57_in_rule__Attribute__Group__2__Impl5700); 
             after(grammarAccess.getAttributeAccess().getAttrKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2__Impl"


    // $ANTLR start "rule__Attribute__Group__3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2774:1: rule__Attribute__Group__3 : rule__Attribute__Group__3__Impl rule__Attribute__Group__4 ;
    public final void rule__Attribute__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2778:1: ( rule__Attribute__Group__3__Impl rule__Attribute__Group__4 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2779:2: rule__Attribute__Group__3__Impl rule__Attribute__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__3__Impl_in_rule__Attribute__Group__35731);
            rule__Attribute__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__4_in_rule__Attribute__Group__35734);
            rule__Attribute__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3"


    // $ANTLR start "rule__Attribute__Group__3__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2786:1: rule__Attribute__Group__3__Impl : ( ( rule__Attribute__NameAssignment_3 ) ) ;
    public final void rule__Attribute__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2790:1: ( ( ( rule__Attribute__NameAssignment_3 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2791:1: ( ( rule__Attribute__NameAssignment_3 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2791:1: ( ( rule__Attribute__NameAssignment_3 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2792:1: ( rule__Attribute__NameAssignment_3 )
            {
             before(grammarAccess.getAttributeAccess().getNameAssignment_3()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2793:1: ( rule__Attribute__NameAssignment_3 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2793:2: rule__Attribute__NameAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__NameAssignment_3_in_rule__Attribute__Group__3__Impl5761);
            rule__Attribute__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3__Impl"


    // $ANTLR start "rule__Attribute__Group__4"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2803:1: rule__Attribute__Group__4 : rule__Attribute__Group__4__Impl rule__Attribute__Group__5 ;
    public final void rule__Attribute__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2807:1: ( rule__Attribute__Group__4__Impl rule__Attribute__Group__5 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2808:2: rule__Attribute__Group__4__Impl rule__Attribute__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__4__Impl_in_rule__Attribute__Group__45791);
            rule__Attribute__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__5_in_rule__Attribute__Group__45794);
            rule__Attribute__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__4"


    // $ANTLR start "rule__Attribute__Group__4__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2815:1: rule__Attribute__Group__4__Impl : ( '=' ) ;
    public final void rule__Attribute__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2819:1: ( ( '=' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2820:1: ( '=' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2820:1: ( '=' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2821:1: '='
            {
             before(grammarAccess.getAttributeAccess().getEqualsSignKeyword_4()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__Attribute__Group__4__Impl5822); 
             after(grammarAccess.getAttributeAccess().getEqualsSignKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__4__Impl"


    // $ANTLR start "rule__Attribute__Group__5"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2834:1: rule__Attribute__Group__5 : rule__Attribute__Group__5__Impl rule__Attribute__Group__6 ;
    public final void rule__Attribute__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2838:1: ( rule__Attribute__Group__5__Impl rule__Attribute__Group__6 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2839:2: rule__Attribute__Group__5__Impl rule__Attribute__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__5__Impl_in_rule__Attribute__Group__55853);
            rule__Attribute__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__6_in_rule__Attribute__Group__55856);
            rule__Attribute__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__5"


    // $ANTLR start "rule__Attribute__Group__5__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2846:1: rule__Attribute__Group__5__Impl : ( ( rule__Attribute__ValuesAssignment_5 ) ) ;
    public final void rule__Attribute__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2850:1: ( ( ( rule__Attribute__ValuesAssignment_5 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2851:1: ( ( rule__Attribute__ValuesAssignment_5 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2851:1: ( ( rule__Attribute__ValuesAssignment_5 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2852:1: ( rule__Attribute__ValuesAssignment_5 )
            {
             before(grammarAccess.getAttributeAccess().getValuesAssignment_5()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2853:1: ( rule__Attribute__ValuesAssignment_5 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2853:2: rule__Attribute__ValuesAssignment_5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__ValuesAssignment_5_in_rule__Attribute__Group__5__Impl5883);
            rule__Attribute__ValuesAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getValuesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__5__Impl"


    // $ANTLR start "rule__Attribute__Group__6"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2863:1: rule__Attribute__Group__6 : rule__Attribute__Group__6__Impl ;
    public final void rule__Attribute__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2867:1: ( rule__Attribute__Group__6__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2868:2: rule__Attribute__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__6__Impl_in_rule__Attribute__Group__65913);
            rule__Attribute__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__6"


    // $ANTLR start "rule__Attribute__Group__6__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2874:1: rule__Attribute__Group__6__Impl : ( ( rule__Attribute__Group_6__0 )* ) ;
    public final void rule__Attribute__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2878:1: ( ( ( rule__Attribute__Group_6__0 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2879:1: ( ( rule__Attribute__Group_6__0 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2879:1: ( ( rule__Attribute__Group_6__0 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2880:1: ( rule__Attribute__Group_6__0 )*
            {
             before(grammarAccess.getAttributeAccess().getGroup_6()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2881:1: ( rule__Attribute__Group_6__0 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==53) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2881:2: rule__Attribute__Group_6__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_6__0_in_rule__Attribute__Group__6__Impl5940);
            	    rule__Attribute__Group_6__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getAttributeAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__6__Impl"


    // $ANTLR start "rule__Attribute__Group_1__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2905:1: rule__Attribute__Group_1__0 : rule__Attribute__Group_1__0__Impl rule__Attribute__Group_1__1 ;
    public final void rule__Attribute__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2909:1: ( rule__Attribute__Group_1__0__Impl rule__Attribute__Group_1__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2910:2: rule__Attribute__Group_1__0__Impl rule__Attribute__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_1__0__Impl_in_rule__Attribute__Group_1__05985);
            rule__Attribute__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_1__1_in_rule__Attribute__Group_1__05988);
            rule__Attribute__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_1__0"


    // $ANTLR start "rule__Attribute__Group_1__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2917:1: rule__Attribute__Group_1__0__Impl : ( ( rule__Attribute__AnnotationAssignment_1_0 ) ) ;
    public final void rule__Attribute__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2921:1: ( ( ( rule__Attribute__AnnotationAssignment_1_0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2922:1: ( ( rule__Attribute__AnnotationAssignment_1_0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2922:1: ( ( rule__Attribute__AnnotationAssignment_1_0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2923:1: ( rule__Attribute__AnnotationAssignment_1_0 )
            {
             before(grammarAccess.getAttributeAccess().getAnnotationAssignment_1_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2924:1: ( rule__Attribute__AnnotationAssignment_1_0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2924:2: rule__Attribute__AnnotationAssignment_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__AnnotationAssignment_1_0_in_rule__Attribute__Group_1__0__Impl6015);
            rule__Attribute__AnnotationAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getAnnotationAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_1__0__Impl"


    // $ANTLR start "rule__Attribute__Group_1__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2934:1: rule__Attribute__Group_1__1 : rule__Attribute__Group_1__1__Impl ;
    public final void rule__Attribute__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2938:1: ( rule__Attribute__Group_1__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2939:2: rule__Attribute__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_1__1__Impl_in_rule__Attribute__Group_1__16045);
            rule__Attribute__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_1__1"


    // $ANTLR start "rule__Attribute__Group_1__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2945:1: rule__Attribute__Group_1__1__Impl : ( ( rule__Attribute__AnnotationAssignment_1_1 )* ) ;
    public final void rule__Attribute__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2949:1: ( ( ( rule__Attribute__AnnotationAssignment_1_1 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2950:1: ( ( rule__Attribute__AnnotationAssignment_1_1 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2950:1: ( ( rule__Attribute__AnnotationAssignment_1_1 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2951:1: ( rule__Attribute__AnnotationAssignment_1_1 )*
            {
             before(grammarAccess.getAttributeAccess().getAnnotationAssignment_1_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2952:1: ( rule__Attribute__AnnotationAssignment_1_1 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==50) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2952:2: rule__Attribute__AnnotationAssignment_1_1
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Attribute__AnnotationAssignment_1_1_in_rule__Attribute__Group_1__1__Impl6072);
            	    rule__Attribute__AnnotationAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getAttributeAccess().getAnnotationAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_1__1__Impl"


    // $ANTLR start "rule__Attribute__Group_6__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2966:1: rule__Attribute__Group_6__0 : rule__Attribute__Group_6__0__Impl rule__Attribute__Group_6__1 ;
    public final void rule__Attribute__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2970:1: ( rule__Attribute__Group_6__0__Impl rule__Attribute__Group_6__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2971:2: rule__Attribute__Group_6__0__Impl rule__Attribute__Group_6__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_6__0__Impl_in_rule__Attribute__Group_6__06107);
            rule__Attribute__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_6__1_in_rule__Attribute__Group_6__06110);
            rule__Attribute__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_6__0"


    // $ANTLR start "rule__Attribute__Group_6__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2978:1: rule__Attribute__Group_6__0__Impl : ( ',' ) ;
    public final void rule__Attribute__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2982:1: ( ( ',' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2983:1: ( ',' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2983:1: ( ',' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2984:1: ','
            {
             before(grammarAccess.getAttributeAccess().getCommaKeyword_6_0()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__Attribute__Group_6__0__Impl6138); 
             after(grammarAccess.getAttributeAccess().getCommaKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_6__0__Impl"


    // $ANTLR start "rule__Attribute__Group_6__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:2997:1: rule__Attribute__Group_6__1 : rule__Attribute__Group_6__1__Impl ;
    public final void rule__Attribute__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3001:1: ( rule__Attribute__Group_6__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3002:2: rule__Attribute__Group_6__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_6__1__Impl_in_rule__Attribute__Group_6__16169);
            rule__Attribute__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_6__1"


    // $ANTLR start "rule__Attribute__Group_6__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3008:1: rule__Attribute__Group_6__1__Impl : ( ( rule__Attribute__ValuesAssignment_6_1 ) ) ;
    public final void rule__Attribute__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3012:1: ( ( ( rule__Attribute__ValuesAssignment_6_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3013:1: ( ( rule__Attribute__ValuesAssignment_6_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3013:1: ( ( rule__Attribute__ValuesAssignment_6_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3014:1: ( rule__Attribute__ValuesAssignment_6_1 )
            {
             before(grammarAccess.getAttributeAccess().getValuesAssignment_6_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3015:1: ( rule__Attribute__ValuesAssignment_6_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3015:2: rule__Attribute__ValuesAssignment_6_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__ValuesAssignment_6_1_in_rule__Attribute__Group_6__1__Impl6196);
            rule__Attribute__ValuesAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getValuesAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_6__1__Impl"


    // $ANTLR start "rule__Reference__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3029:1: rule__Reference__Group__0 : rule__Reference__Group__0__Impl rule__Reference__Group__1 ;
    public final void rule__Reference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3033:1: ( rule__Reference__Group__0__Impl rule__Reference__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3034:2: rule__Reference__Group__0__Impl rule__Reference__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__0__Impl_in_rule__Reference__Group__06230);
            rule__Reference__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__1_in_rule__Reference__Group__06233);
            rule__Reference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__0"


    // $ANTLR start "rule__Reference__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3041:1: rule__Reference__Group__0__Impl : ( () ) ;
    public final void rule__Reference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3045:1: ( ( () ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3046:1: ( () )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3046:1: ( () )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3047:1: ()
            {
             before(grammarAccess.getReferenceAccess().getReferenceAction_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3048:1: ()
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3050:1: 
            {
            }

             after(grammarAccess.getReferenceAccess().getReferenceAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__0__Impl"


    // $ANTLR start "rule__Reference__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3060:1: rule__Reference__Group__1 : rule__Reference__Group__1__Impl rule__Reference__Group__2 ;
    public final void rule__Reference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3064:1: ( rule__Reference__Group__1__Impl rule__Reference__Group__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3065:2: rule__Reference__Group__1__Impl rule__Reference__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__1__Impl_in_rule__Reference__Group__16291);
            rule__Reference__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__2_in_rule__Reference__Group__16294);
            rule__Reference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__1"


    // $ANTLR start "rule__Reference__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3072:1: rule__Reference__Group__1__Impl : ( ( rule__Reference__Group_1__0 )? ) ;
    public final void rule__Reference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3076:1: ( ( ( rule__Reference__Group_1__0 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3077:1: ( ( rule__Reference__Group_1__0 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3077:1: ( ( rule__Reference__Group_1__0 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3078:1: ( rule__Reference__Group_1__0 )?
            {
             before(grammarAccess.getReferenceAccess().getGroup_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3079:1: ( rule__Reference__Group_1__0 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==50) ) {
                int LA28_1 = input.LA(2);

                if ( ((LA28_1>=RULE_STRING && LA28_1<=RULE_ID)) ) {
                    alt28=1;
                }
            }
            switch (alt28) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3079:2: rule__Reference__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_1__0_in_rule__Reference__Group__1__Impl6321);
                    rule__Reference__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReferenceAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__1__Impl"


    // $ANTLR start "rule__Reference__Group__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3089:1: rule__Reference__Group__2 : rule__Reference__Group__2__Impl rule__Reference__Group__3 ;
    public final void rule__Reference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3093:1: ( rule__Reference__Group__2__Impl rule__Reference__Group__3 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3094:2: rule__Reference__Group__2__Impl rule__Reference__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__2__Impl_in_rule__Reference__Group__26352);
            rule__Reference__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__3_in_rule__Reference__Group__26355);
            rule__Reference__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__2"


    // $ANTLR start "rule__Reference__Group__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3101:1: rule__Reference__Group__2__Impl : ( ( rule__Reference__GraphicPropertiesAssignment_2 )? ) ;
    public final void rule__Reference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3105:1: ( ( ( rule__Reference__GraphicPropertiesAssignment_2 )? ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3106:1: ( ( rule__Reference__GraphicPropertiesAssignment_2 )? )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3106:1: ( ( rule__Reference__GraphicPropertiesAssignment_2 )? )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3107:1: ( rule__Reference__GraphicPropertiesAssignment_2 )?
            {
             before(grammarAccess.getReferenceAccess().getGraphicPropertiesAssignment_2()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3108:1: ( rule__Reference__GraphicPropertiesAssignment_2 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==50) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3108:2: rule__Reference__GraphicPropertiesAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Reference__GraphicPropertiesAssignment_2_in_rule__Reference__Group__2__Impl6382);
                    rule__Reference__GraphicPropertiesAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReferenceAccess().getGraphicPropertiesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__2__Impl"


    // $ANTLR start "rule__Reference__Group__3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3118:1: rule__Reference__Group__3 : rule__Reference__Group__3__Impl rule__Reference__Group__4 ;
    public final void rule__Reference__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3122:1: ( rule__Reference__Group__3__Impl rule__Reference__Group__4 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3123:2: rule__Reference__Group__3__Impl rule__Reference__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__3__Impl_in_rule__Reference__Group__36413);
            rule__Reference__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__4_in_rule__Reference__Group__36416);
            rule__Reference__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__3"


    // $ANTLR start "rule__Reference__Group__3__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3130:1: rule__Reference__Group__3__Impl : ( ( rule__Reference__Alternatives_3 ) ) ;
    public final void rule__Reference__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3134:1: ( ( ( rule__Reference__Alternatives_3 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3135:1: ( ( rule__Reference__Alternatives_3 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3135:1: ( ( rule__Reference__Alternatives_3 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3136:1: ( rule__Reference__Alternatives_3 )
            {
             before(grammarAccess.getReferenceAccess().getAlternatives_3()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3137:1: ( rule__Reference__Alternatives_3 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3137:2: rule__Reference__Alternatives_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Alternatives_3_in_rule__Reference__Group__3__Impl6443);
            rule__Reference__Alternatives_3();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getAlternatives_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__3__Impl"


    // $ANTLR start "rule__Reference__Group__4"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3147:1: rule__Reference__Group__4 : rule__Reference__Group__4__Impl rule__Reference__Group__5 ;
    public final void rule__Reference__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3151:1: ( rule__Reference__Group__4__Impl rule__Reference__Group__5 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3152:2: rule__Reference__Group__4__Impl rule__Reference__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__4__Impl_in_rule__Reference__Group__46473);
            rule__Reference__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__5_in_rule__Reference__Group__46476);
            rule__Reference__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__4"


    // $ANTLR start "rule__Reference__Group__4__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3159:1: rule__Reference__Group__4__Impl : ( ( rule__Reference__NameAssignment_4 ) ) ;
    public final void rule__Reference__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3163:1: ( ( ( rule__Reference__NameAssignment_4 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3164:1: ( ( rule__Reference__NameAssignment_4 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3164:1: ( ( rule__Reference__NameAssignment_4 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3165:1: ( rule__Reference__NameAssignment_4 )
            {
             before(grammarAccess.getReferenceAccess().getNameAssignment_4()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3166:1: ( rule__Reference__NameAssignment_4 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3166:2: rule__Reference__NameAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__NameAssignment_4_in_rule__Reference__Group__4__Impl6503);
            rule__Reference__NameAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getNameAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__4__Impl"


    // $ANTLR start "rule__Reference__Group__5"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3176:1: rule__Reference__Group__5 : rule__Reference__Group__5__Impl rule__Reference__Group__6 ;
    public final void rule__Reference__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3180:1: ( rule__Reference__Group__5__Impl rule__Reference__Group__6 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3181:2: rule__Reference__Group__5__Impl rule__Reference__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__5__Impl_in_rule__Reference__Group__56533);
            rule__Reference__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__6_in_rule__Reference__Group__56536);
            rule__Reference__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__5"


    // $ANTLR start "rule__Reference__Group__5__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3188:1: rule__Reference__Group__5__Impl : ( '=' ) ;
    public final void rule__Reference__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3192:1: ( ( '=' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3193:1: ( '=' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3193:1: ( '=' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3194:1: '='
            {
             before(grammarAccess.getReferenceAccess().getEqualsSignKeyword_5()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__Reference__Group__5__Impl6564); 
             after(grammarAccess.getReferenceAccess().getEqualsSignKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__5__Impl"


    // $ANTLR start "rule__Reference__Group__6"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3207:1: rule__Reference__Group__6 : rule__Reference__Group__6__Impl rule__Reference__Group__7 ;
    public final void rule__Reference__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3211:1: ( rule__Reference__Group__6__Impl rule__Reference__Group__7 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3212:2: rule__Reference__Group__6__Impl rule__Reference__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__6__Impl_in_rule__Reference__Group__66595);
            rule__Reference__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__7_in_rule__Reference__Group__66598);
            rule__Reference__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__6"


    // $ANTLR start "rule__Reference__Group__6__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3219:1: rule__Reference__Group__6__Impl : ( ( rule__Reference__ReferenceAssignment_6 ) ) ;
    public final void rule__Reference__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3223:1: ( ( ( rule__Reference__ReferenceAssignment_6 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3224:1: ( ( rule__Reference__ReferenceAssignment_6 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3224:1: ( ( rule__Reference__ReferenceAssignment_6 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3225:1: ( rule__Reference__ReferenceAssignment_6 )
            {
             before(grammarAccess.getReferenceAccess().getReferenceAssignment_6()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3226:1: ( rule__Reference__ReferenceAssignment_6 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3226:2: rule__Reference__ReferenceAssignment_6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__ReferenceAssignment_6_in_rule__Reference__Group__6__Impl6625);
            rule__Reference__ReferenceAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getReferenceAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__6__Impl"


    // $ANTLR start "rule__Reference__Group__7"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3236:1: rule__Reference__Group__7 : rule__Reference__Group__7__Impl ;
    public final void rule__Reference__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3240:1: ( rule__Reference__Group__7__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3241:2: rule__Reference__Group__7__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__7__Impl_in_rule__Reference__Group__76655);
            rule__Reference__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__7"


    // $ANTLR start "rule__Reference__Group__7__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3247:1: rule__Reference__Group__7__Impl : ( ( rule__Reference__Group_7__0 )* ) ;
    public final void rule__Reference__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3251:1: ( ( ( rule__Reference__Group_7__0 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3252:1: ( ( rule__Reference__Group_7__0 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3252:1: ( ( rule__Reference__Group_7__0 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3253:1: ( rule__Reference__Group_7__0 )*
            {
             before(grammarAccess.getReferenceAccess().getGroup_7()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3254:1: ( rule__Reference__Group_7__0 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==53) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3254:2: rule__Reference__Group_7__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_7__0_in_rule__Reference__Group__7__Impl6682);
            	    rule__Reference__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

             after(grammarAccess.getReferenceAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__7__Impl"


    // $ANTLR start "rule__Reference__Group_1__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3280:1: rule__Reference__Group_1__0 : rule__Reference__Group_1__0__Impl rule__Reference__Group_1__1 ;
    public final void rule__Reference__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3284:1: ( rule__Reference__Group_1__0__Impl rule__Reference__Group_1__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3285:2: rule__Reference__Group_1__0__Impl rule__Reference__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_1__0__Impl_in_rule__Reference__Group_1__06729);
            rule__Reference__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_1__1_in_rule__Reference__Group_1__06732);
            rule__Reference__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_1__0"


    // $ANTLR start "rule__Reference__Group_1__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3292:1: rule__Reference__Group_1__0__Impl : ( ( rule__Reference__AnnotationAssignment_1_0 ) ) ;
    public final void rule__Reference__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3296:1: ( ( ( rule__Reference__AnnotationAssignment_1_0 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3297:1: ( ( rule__Reference__AnnotationAssignment_1_0 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3297:1: ( ( rule__Reference__AnnotationAssignment_1_0 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3298:1: ( rule__Reference__AnnotationAssignment_1_0 )
            {
             before(grammarAccess.getReferenceAccess().getAnnotationAssignment_1_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3299:1: ( rule__Reference__AnnotationAssignment_1_0 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3299:2: rule__Reference__AnnotationAssignment_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__AnnotationAssignment_1_0_in_rule__Reference__Group_1__0__Impl6759);
            rule__Reference__AnnotationAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getAnnotationAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_1__0__Impl"


    // $ANTLR start "rule__Reference__Group_1__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3309:1: rule__Reference__Group_1__1 : rule__Reference__Group_1__1__Impl ;
    public final void rule__Reference__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3313:1: ( rule__Reference__Group_1__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3314:2: rule__Reference__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_1__1__Impl_in_rule__Reference__Group_1__16789);
            rule__Reference__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_1__1"


    // $ANTLR start "rule__Reference__Group_1__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3320:1: rule__Reference__Group_1__1__Impl : ( ( rule__Reference__AnnotationAssignment_1_1 )* ) ;
    public final void rule__Reference__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3324:1: ( ( ( rule__Reference__AnnotationAssignment_1_1 )* ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3325:1: ( ( rule__Reference__AnnotationAssignment_1_1 )* )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3325:1: ( ( rule__Reference__AnnotationAssignment_1_1 )* )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3326:1: ( rule__Reference__AnnotationAssignment_1_1 )*
            {
             before(grammarAccess.getReferenceAccess().getAnnotationAssignment_1_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3327:1: ( rule__Reference__AnnotationAssignment_1_1 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==50) ) {
                    int LA31_1 = input.LA(2);

                    if ( ((LA31_1>=RULE_STRING && LA31_1<=RULE_ID)) ) {
                        alt31=1;
                    }


                }


                switch (alt31) {
            	case 1 :
            	    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3327:2: rule__Reference__AnnotationAssignment_1_1
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Reference__AnnotationAssignment_1_1_in_rule__Reference__Group_1__1__Impl6816);
            	    rule__Reference__AnnotationAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getReferenceAccess().getAnnotationAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_1__1__Impl"


    // $ANTLR start "rule__Reference__Group_7__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3341:1: rule__Reference__Group_7__0 : rule__Reference__Group_7__0__Impl rule__Reference__Group_7__1 ;
    public final void rule__Reference__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3345:1: ( rule__Reference__Group_7__0__Impl rule__Reference__Group_7__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3346:2: rule__Reference__Group_7__0__Impl rule__Reference__Group_7__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_7__0__Impl_in_rule__Reference__Group_7__06851);
            rule__Reference__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_7__1_in_rule__Reference__Group_7__06854);
            rule__Reference__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_7__0"


    // $ANTLR start "rule__Reference__Group_7__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3353:1: rule__Reference__Group_7__0__Impl : ( ',' ) ;
    public final void rule__Reference__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3357:1: ( ( ',' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3358:1: ( ',' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3358:1: ( ',' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3359:1: ','
            {
             before(grammarAccess.getReferenceAccess().getCommaKeyword_7_0()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__Reference__Group_7__0__Impl6882); 
             after(grammarAccess.getReferenceAccess().getCommaKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_7__0__Impl"


    // $ANTLR start "rule__Reference__Group_7__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3372:1: rule__Reference__Group_7__1 : rule__Reference__Group_7__1__Impl ;
    public final void rule__Reference__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3376:1: ( rule__Reference__Group_7__1__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3377:2: rule__Reference__Group_7__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_7__1__Impl_in_rule__Reference__Group_7__16913);
            rule__Reference__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_7__1"


    // $ANTLR start "rule__Reference__Group_7__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3383:1: rule__Reference__Group_7__1__Impl : ( ( rule__Reference__ReferenceAssignment_7_1 ) ) ;
    public final void rule__Reference__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3387:1: ( ( ( rule__Reference__ReferenceAssignment_7_1 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3388:1: ( ( rule__Reference__ReferenceAssignment_7_1 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3388:1: ( ( rule__Reference__ReferenceAssignment_7_1 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3389:1: ( rule__Reference__ReferenceAssignment_7_1 )
            {
             before(grammarAccess.getReferenceAccess().getReferenceAssignment_7_1()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3390:1: ( rule__Reference__ReferenceAssignment_7_1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3390:2: rule__Reference__ReferenceAssignment_7_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__ReferenceAssignment_7_1_in_rule__Reference__Group_7__1__Impl6940);
            rule__Reference__ReferenceAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getReferenceAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_7__1__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3404:1: rule__EdgeProperties__Group__0 : rule__EdgeProperties__Group__0__Impl rule__EdgeProperties__Group__1 ;
    public final void rule__EdgeProperties__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3408:1: ( rule__EdgeProperties__Group__0__Impl rule__EdgeProperties__Group__1 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3409:2: rule__EdgeProperties__Group__0__Impl rule__EdgeProperties__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__0__Impl_in_rule__EdgeProperties__Group__06974);
            rule__EdgeProperties__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__1_in_rule__EdgeProperties__Group__06977);
            rule__EdgeProperties__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__0"


    // $ANTLR start "rule__EdgeProperties__Group__0__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3416:1: rule__EdgeProperties__Group__0__Impl : ( () ) ;
    public final void rule__EdgeProperties__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3420:1: ( ( () ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3421:1: ( () )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3421:1: ( () )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3422:1: ()
            {
             before(grammarAccess.getEdgePropertiesAccess().getEdgePropertiesAction_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3423:1: ()
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3425:1: 
            {
            }

             after(grammarAccess.getEdgePropertiesAccess().getEdgePropertiesAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__0__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3435:1: rule__EdgeProperties__Group__1 : rule__EdgeProperties__Group__1__Impl rule__EdgeProperties__Group__2 ;
    public final void rule__EdgeProperties__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3439:1: ( rule__EdgeProperties__Group__1__Impl rule__EdgeProperties__Group__2 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3440:2: rule__EdgeProperties__Group__1__Impl rule__EdgeProperties__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__1__Impl_in_rule__EdgeProperties__Group__17035);
            rule__EdgeProperties__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__2_in_rule__EdgeProperties__Group__17038);
            rule__EdgeProperties__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__1"


    // $ANTLR start "rule__EdgeProperties__Group__1__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3447:1: rule__EdgeProperties__Group__1__Impl : ( '@' ) ;
    public final void rule__EdgeProperties__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3451:1: ( ( '@' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3452:1: ( '@' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3452:1: ( '@' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3453:1: '@'
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommercialAtKeyword_1()); 
            match(input,50,FollowSets000.FOLLOW_50_in_rule__EdgeProperties__Group__1__Impl7066); 
             after(grammarAccess.getEdgePropertiesAccess().getCommercialAtKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__1__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3466:1: rule__EdgeProperties__Group__2 : rule__EdgeProperties__Group__2__Impl rule__EdgeProperties__Group__3 ;
    public final void rule__EdgeProperties__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3470:1: ( rule__EdgeProperties__Group__2__Impl rule__EdgeProperties__Group__3 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3471:2: rule__EdgeProperties__Group__2__Impl rule__EdgeProperties__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__2__Impl_in_rule__EdgeProperties__Group__27097);
            rule__EdgeProperties__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__3_in_rule__EdgeProperties__Group__27100);
            rule__EdgeProperties__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__2"


    // $ANTLR start "rule__EdgeProperties__Group__2__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3478:1: rule__EdgeProperties__Group__2__Impl : ( 'style' ) ;
    public final void rule__EdgeProperties__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3482:1: ( ( 'style' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3483:1: ( 'style' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3483:1: ( 'style' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3484:1: 'style'
            {
             before(grammarAccess.getEdgePropertiesAccess().getStyleKeyword_2()); 
            match(input,58,FollowSets000.FOLLOW_58_in_rule__EdgeProperties__Group__2__Impl7128); 
             after(grammarAccess.getEdgePropertiesAccess().getStyleKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__2__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3497:1: rule__EdgeProperties__Group__3 : rule__EdgeProperties__Group__3__Impl rule__EdgeProperties__Group__4 ;
    public final void rule__EdgeProperties__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3501:1: ( rule__EdgeProperties__Group__3__Impl rule__EdgeProperties__Group__4 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3502:2: rule__EdgeProperties__Group__3__Impl rule__EdgeProperties__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__3__Impl_in_rule__EdgeProperties__Group__37159);
            rule__EdgeProperties__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__4_in_rule__EdgeProperties__Group__37162);
            rule__EdgeProperties__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__3"


    // $ANTLR start "rule__EdgeProperties__Group__3__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3509:1: rule__EdgeProperties__Group__3__Impl : ( '(' ) ;
    public final void rule__EdgeProperties__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3513:1: ( ( '(' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3514:1: ( '(' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3514:1: ( '(' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3515:1: '('
            {
             before(grammarAccess.getEdgePropertiesAccess().getLeftParenthesisKeyword_3()); 
            match(input,51,FollowSets000.FOLLOW_51_in_rule__EdgeProperties__Group__3__Impl7190); 
             after(grammarAccess.getEdgePropertiesAccess().getLeftParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__3__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__4"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3528:1: rule__EdgeProperties__Group__4 : rule__EdgeProperties__Group__4__Impl rule__EdgeProperties__Group__5 ;
    public final void rule__EdgeProperties__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3532:1: ( rule__EdgeProperties__Group__4__Impl rule__EdgeProperties__Group__5 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3533:2: rule__EdgeProperties__Group__4__Impl rule__EdgeProperties__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__4__Impl_in_rule__EdgeProperties__Group__47221);
            rule__EdgeProperties__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__5_in_rule__EdgeProperties__Group__47224);
            rule__EdgeProperties__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__4"


    // $ANTLR start "rule__EdgeProperties__Group__4__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3540:1: rule__EdgeProperties__Group__4__Impl : ( 'color' ) ;
    public final void rule__EdgeProperties__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3544:1: ( ( 'color' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3545:1: ( 'color' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3545:1: ( 'color' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3546:1: 'color'
            {
             before(grammarAccess.getEdgePropertiesAccess().getColorKeyword_4()); 
            match(input,59,FollowSets000.FOLLOW_59_in_rule__EdgeProperties__Group__4__Impl7252); 
             after(grammarAccess.getEdgePropertiesAccess().getColorKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__4__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__5"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3559:1: rule__EdgeProperties__Group__5 : rule__EdgeProperties__Group__5__Impl rule__EdgeProperties__Group__6 ;
    public final void rule__EdgeProperties__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3563:1: ( rule__EdgeProperties__Group__5__Impl rule__EdgeProperties__Group__6 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3564:2: rule__EdgeProperties__Group__5__Impl rule__EdgeProperties__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__5__Impl_in_rule__EdgeProperties__Group__57283);
            rule__EdgeProperties__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__6_in_rule__EdgeProperties__Group__57286);
            rule__EdgeProperties__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__5"


    // $ANTLR start "rule__EdgeProperties__Group__5__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3571:1: rule__EdgeProperties__Group__5__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3575:1: ( ( '=' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3576:1: ( '=' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3576:1: ( '=' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3577:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_5()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__5__Impl7314); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__5__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__6"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3590:1: rule__EdgeProperties__Group__6 : rule__EdgeProperties__Group__6__Impl rule__EdgeProperties__Group__7 ;
    public final void rule__EdgeProperties__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3594:1: ( rule__EdgeProperties__Group__6__Impl rule__EdgeProperties__Group__7 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3595:2: rule__EdgeProperties__Group__6__Impl rule__EdgeProperties__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__6__Impl_in_rule__EdgeProperties__Group__67345);
            rule__EdgeProperties__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__7_in_rule__EdgeProperties__Group__67348);
            rule__EdgeProperties__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__6"


    // $ANTLR start "rule__EdgeProperties__Group__6__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3602:1: rule__EdgeProperties__Group__6__Impl : ( ( rule__EdgeProperties__ColorAssignment_6 ) ) ;
    public final void rule__EdgeProperties__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3606:1: ( ( ( rule__EdgeProperties__ColorAssignment_6 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3607:1: ( ( rule__EdgeProperties__ColorAssignment_6 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3607:1: ( ( rule__EdgeProperties__ColorAssignment_6 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3608:1: ( rule__EdgeProperties__ColorAssignment_6 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getColorAssignment_6()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3609:1: ( rule__EdgeProperties__ColorAssignment_6 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3609:2: rule__EdgeProperties__ColorAssignment_6
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__ColorAssignment_6_in_rule__EdgeProperties__Group__6__Impl7375);
            rule__EdgeProperties__ColorAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getColorAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__6__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__7"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3619:1: rule__EdgeProperties__Group__7 : rule__EdgeProperties__Group__7__Impl rule__EdgeProperties__Group__8 ;
    public final void rule__EdgeProperties__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3623:1: ( rule__EdgeProperties__Group__7__Impl rule__EdgeProperties__Group__8 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3624:2: rule__EdgeProperties__Group__7__Impl rule__EdgeProperties__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__7__Impl_in_rule__EdgeProperties__Group__77405);
            rule__EdgeProperties__Group__7__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__8_in_rule__EdgeProperties__Group__77408);
            rule__EdgeProperties__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__7"


    // $ANTLR start "rule__EdgeProperties__Group__7__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3631:1: rule__EdgeProperties__Group__7__Impl : ( ',' ) ;
    public final void rule__EdgeProperties__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3635:1: ( ( ',' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3636:1: ( ',' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3636:1: ( ',' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3637:1: ','
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_7()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__EdgeProperties__Group__7__Impl7436); 
             after(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__7__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__8"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3650:1: rule__EdgeProperties__Group__8 : rule__EdgeProperties__Group__8__Impl rule__EdgeProperties__Group__9 ;
    public final void rule__EdgeProperties__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3654:1: ( rule__EdgeProperties__Group__8__Impl rule__EdgeProperties__Group__9 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3655:2: rule__EdgeProperties__Group__8__Impl rule__EdgeProperties__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__8__Impl_in_rule__EdgeProperties__Group__87467);
            rule__EdgeProperties__Group__8__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__9_in_rule__EdgeProperties__Group__87470);
            rule__EdgeProperties__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__8"


    // $ANTLR start "rule__EdgeProperties__Group__8__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3662:1: rule__EdgeProperties__Group__8__Impl : ( 'width' ) ;
    public final void rule__EdgeProperties__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3666:1: ( ( 'width' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3667:1: ( 'width' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3667:1: ( 'width' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3668:1: 'width'
            {
             before(grammarAccess.getEdgePropertiesAccess().getWidthKeyword_8()); 
            match(input,60,FollowSets000.FOLLOW_60_in_rule__EdgeProperties__Group__8__Impl7498); 
             after(grammarAccess.getEdgePropertiesAccess().getWidthKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__8__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__9"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3681:1: rule__EdgeProperties__Group__9 : rule__EdgeProperties__Group__9__Impl rule__EdgeProperties__Group__10 ;
    public final void rule__EdgeProperties__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3685:1: ( rule__EdgeProperties__Group__9__Impl rule__EdgeProperties__Group__10 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3686:2: rule__EdgeProperties__Group__9__Impl rule__EdgeProperties__Group__10
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__9__Impl_in_rule__EdgeProperties__Group__97529);
            rule__EdgeProperties__Group__9__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__10_in_rule__EdgeProperties__Group__97532);
            rule__EdgeProperties__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__9"


    // $ANTLR start "rule__EdgeProperties__Group__9__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3693:1: rule__EdgeProperties__Group__9__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3697:1: ( ( '=' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3698:1: ( '=' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3698:1: ( '=' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3699:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_9()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__9__Impl7560); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__9__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__10"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3712:1: rule__EdgeProperties__Group__10 : rule__EdgeProperties__Group__10__Impl rule__EdgeProperties__Group__11 ;
    public final void rule__EdgeProperties__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3716:1: ( rule__EdgeProperties__Group__10__Impl rule__EdgeProperties__Group__11 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3717:2: rule__EdgeProperties__Group__10__Impl rule__EdgeProperties__Group__11
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__10__Impl_in_rule__EdgeProperties__Group__107591);
            rule__EdgeProperties__Group__10__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__11_in_rule__EdgeProperties__Group__107594);
            rule__EdgeProperties__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__10"


    // $ANTLR start "rule__EdgeProperties__Group__10__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3724:1: rule__EdgeProperties__Group__10__Impl : ( ( rule__EdgeProperties__WidthAssignment_10 ) ) ;
    public final void rule__EdgeProperties__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3728:1: ( ( ( rule__EdgeProperties__WidthAssignment_10 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3729:1: ( ( rule__EdgeProperties__WidthAssignment_10 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3729:1: ( ( rule__EdgeProperties__WidthAssignment_10 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3730:1: ( rule__EdgeProperties__WidthAssignment_10 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getWidthAssignment_10()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3731:1: ( rule__EdgeProperties__WidthAssignment_10 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3731:2: rule__EdgeProperties__WidthAssignment_10
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__WidthAssignment_10_in_rule__EdgeProperties__Group__10__Impl7621);
            rule__EdgeProperties__WidthAssignment_10();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getWidthAssignment_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__10__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__11"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3741:1: rule__EdgeProperties__Group__11 : rule__EdgeProperties__Group__11__Impl rule__EdgeProperties__Group__12 ;
    public final void rule__EdgeProperties__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3745:1: ( rule__EdgeProperties__Group__11__Impl rule__EdgeProperties__Group__12 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3746:2: rule__EdgeProperties__Group__11__Impl rule__EdgeProperties__Group__12
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__11__Impl_in_rule__EdgeProperties__Group__117651);
            rule__EdgeProperties__Group__11__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__12_in_rule__EdgeProperties__Group__117654);
            rule__EdgeProperties__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__11"


    // $ANTLR start "rule__EdgeProperties__Group__11__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3753:1: rule__EdgeProperties__Group__11__Impl : ( ',' ) ;
    public final void rule__EdgeProperties__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3757:1: ( ( ',' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3758:1: ( ',' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3758:1: ( ',' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3759:1: ','
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_11()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__EdgeProperties__Group__11__Impl7682); 
             after(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__11__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__12"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3772:1: rule__EdgeProperties__Group__12 : rule__EdgeProperties__Group__12__Impl rule__EdgeProperties__Group__13 ;
    public final void rule__EdgeProperties__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3776:1: ( rule__EdgeProperties__Group__12__Impl rule__EdgeProperties__Group__13 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3777:2: rule__EdgeProperties__Group__12__Impl rule__EdgeProperties__Group__13
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__12__Impl_in_rule__EdgeProperties__Group__127713);
            rule__EdgeProperties__Group__12__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__13_in_rule__EdgeProperties__Group__127716);
            rule__EdgeProperties__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__12"


    // $ANTLR start "rule__EdgeProperties__Group__12__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3784:1: rule__EdgeProperties__Group__12__Impl : ( 'line' ) ;
    public final void rule__EdgeProperties__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3788:1: ( ( 'line' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3789:1: ( 'line' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3789:1: ( 'line' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3790:1: 'line'
            {
             before(grammarAccess.getEdgePropertiesAccess().getLineKeyword_12()); 
            match(input,17,FollowSets000.FOLLOW_17_in_rule__EdgeProperties__Group__12__Impl7744); 
             after(grammarAccess.getEdgePropertiesAccess().getLineKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__12__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__13"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3803:1: rule__EdgeProperties__Group__13 : rule__EdgeProperties__Group__13__Impl rule__EdgeProperties__Group__14 ;
    public final void rule__EdgeProperties__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3807:1: ( rule__EdgeProperties__Group__13__Impl rule__EdgeProperties__Group__14 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3808:2: rule__EdgeProperties__Group__13__Impl rule__EdgeProperties__Group__14
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__13__Impl_in_rule__EdgeProperties__Group__137775);
            rule__EdgeProperties__Group__13__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__14_in_rule__EdgeProperties__Group__137778);
            rule__EdgeProperties__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__13"


    // $ANTLR start "rule__EdgeProperties__Group__13__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3815:1: rule__EdgeProperties__Group__13__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3819:1: ( ( '=' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3820:1: ( '=' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3820:1: ( '=' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3821:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_13()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__13__Impl7806); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__13__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__14"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3834:1: rule__EdgeProperties__Group__14 : rule__EdgeProperties__Group__14__Impl rule__EdgeProperties__Group__15 ;
    public final void rule__EdgeProperties__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3838:1: ( rule__EdgeProperties__Group__14__Impl rule__EdgeProperties__Group__15 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3839:2: rule__EdgeProperties__Group__14__Impl rule__EdgeProperties__Group__15
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__14__Impl_in_rule__EdgeProperties__Group__147837);
            rule__EdgeProperties__Group__14__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__15_in_rule__EdgeProperties__Group__147840);
            rule__EdgeProperties__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__14"


    // $ANTLR start "rule__EdgeProperties__Group__14__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3846:1: rule__EdgeProperties__Group__14__Impl : ( ( rule__EdgeProperties__LineTypeAssignment_14 ) ) ;
    public final void rule__EdgeProperties__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3850:1: ( ( ( rule__EdgeProperties__LineTypeAssignment_14 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3851:1: ( ( rule__EdgeProperties__LineTypeAssignment_14 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3851:1: ( ( rule__EdgeProperties__LineTypeAssignment_14 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3852:1: ( rule__EdgeProperties__LineTypeAssignment_14 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getLineTypeAssignment_14()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3853:1: ( rule__EdgeProperties__LineTypeAssignment_14 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3853:2: rule__EdgeProperties__LineTypeAssignment_14
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__LineTypeAssignment_14_in_rule__EdgeProperties__Group__14__Impl7867);
            rule__EdgeProperties__LineTypeAssignment_14();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getLineTypeAssignment_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__14__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__15"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3863:1: rule__EdgeProperties__Group__15 : rule__EdgeProperties__Group__15__Impl rule__EdgeProperties__Group__16 ;
    public final void rule__EdgeProperties__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3867:1: ( rule__EdgeProperties__Group__15__Impl rule__EdgeProperties__Group__16 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3868:2: rule__EdgeProperties__Group__15__Impl rule__EdgeProperties__Group__16
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__15__Impl_in_rule__EdgeProperties__Group__157897);
            rule__EdgeProperties__Group__15__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__16_in_rule__EdgeProperties__Group__157900);
            rule__EdgeProperties__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__15"


    // $ANTLR start "rule__EdgeProperties__Group__15__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3875:1: rule__EdgeProperties__Group__15__Impl : ( ',' ) ;
    public final void rule__EdgeProperties__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3879:1: ( ( ',' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3880:1: ( ',' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3880:1: ( ',' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3881:1: ','
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_15()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__EdgeProperties__Group__15__Impl7928); 
             after(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__15__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__16"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3894:1: rule__EdgeProperties__Group__16 : rule__EdgeProperties__Group__16__Impl rule__EdgeProperties__Group__17 ;
    public final void rule__EdgeProperties__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3898:1: ( rule__EdgeProperties__Group__16__Impl rule__EdgeProperties__Group__17 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3899:2: rule__EdgeProperties__Group__16__Impl rule__EdgeProperties__Group__17
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__16__Impl_in_rule__EdgeProperties__Group__167959);
            rule__EdgeProperties__Group__16__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__17_in_rule__EdgeProperties__Group__167962);
            rule__EdgeProperties__Group__17();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__16"


    // $ANTLR start "rule__EdgeProperties__Group__16__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3906:1: rule__EdgeProperties__Group__16__Impl : ( 'source' ) ;
    public final void rule__EdgeProperties__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3910:1: ( ( 'source' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3911:1: ( 'source' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3911:1: ( 'source' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3912:1: 'source'
            {
             before(grammarAccess.getEdgePropertiesAccess().getSourceKeyword_16()); 
            match(input,61,FollowSets000.FOLLOW_61_in_rule__EdgeProperties__Group__16__Impl7990); 
             after(grammarAccess.getEdgePropertiesAccess().getSourceKeyword_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__16__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__17"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3925:1: rule__EdgeProperties__Group__17 : rule__EdgeProperties__Group__17__Impl rule__EdgeProperties__Group__18 ;
    public final void rule__EdgeProperties__Group__17() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3929:1: ( rule__EdgeProperties__Group__17__Impl rule__EdgeProperties__Group__18 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3930:2: rule__EdgeProperties__Group__17__Impl rule__EdgeProperties__Group__18
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__17__Impl_in_rule__EdgeProperties__Group__178021);
            rule__EdgeProperties__Group__17__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__18_in_rule__EdgeProperties__Group__178024);
            rule__EdgeProperties__Group__18();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__17"


    // $ANTLR start "rule__EdgeProperties__Group__17__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3937:1: rule__EdgeProperties__Group__17__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__17__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3941:1: ( ( '=' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3942:1: ( '=' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3942:1: ( '=' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3943:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_17()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__17__Impl8052); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_17()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__17__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__18"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3956:1: rule__EdgeProperties__Group__18 : rule__EdgeProperties__Group__18__Impl rule__EdgeProperties__Group__19 ;
    public final void rule__EdgeProperties__Group__18() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3960:1: ( rule__EdgeProperties__Group__18__Impl rule__EdgeProperties__Group__19 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3961:2: rule__EdgeProperties__Group__18__Impl rule__EdgeProperties__Group__19
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__18__Impl_in_rule__EdgeProperties__Group__188083);
            rule__EdgeProperties__Group__18__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__19_in_rule__EdgeProperties__Group__188086);
            rule__EdgeProperties__Group__19();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__18"


    // $ANTLR start "rule__EdgeProperties__Group__18__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3968:1: rule__EdgeProperties__Group__18__Impl : ( ( rule__EdgeProperties__SrcArrowAssignment_18 ) ) ;
    public final void rule__EdgeProperties__Group__18__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3972:1: ( ( ( rule__EdgeProperties__SrcArrowAssignment_18 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3973:1: ( ( rule__EdgeProperties__SrcArrowAssignment_18 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3973:1: ( ( rule__EdgeProperties__SrcArrowAssignment_18 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3974:1: ( rule__EdgeProperties__SrcArrowAssignment_18 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getSrcArrowAssignment_18()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3975:1: ( rule__EdgeProperties__SrcArrowAssignment_18 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3975:2: rule__EdgeProperties__SrcArrowAssignment_18
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__SrcArrowAssignment_18_in_rule__EdgeProperties__Group__18__Impl8113);
            rule__EdgeProperties__SrcArrowAssignment_18();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getSrcArrowAssignment_18()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__18__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__19"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3985:1: rule__EdgeProperties__Group__19 : rule__EdgeProperties__Group__19__Impl rule__EdgeProperties__Group__20 ;
    public final void rule__EdgeProperties__Group__19() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3989:1: ( rule__EdgeProperties__Group__19__Impl rule__EdgeProperties__Group__20 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3990:2: rule__EdgeProperties__Group__19__Impl rule__EdgeProperties__Group__20
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__19__Impl_in_rule__EdgeProperties__Group__198143);
            rule__EdgeProperties__Group__19__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__20_in_rule__EdgeProperties__Group__198146);
            rule__EdgeProperties__Group__20();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__19"


    // $ANTLR start "rule__EdgeProperties__Group__19__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:3997:1: rule__EdgeProperties__Group__19__Impl : ( ',' ) ;
    public final void rule__EdgeProperties__Group__19__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4001:1: ( ( ',' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4002:1: ( ',' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4002:1: ( ',' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4003:1: ','
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_19()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__EdgeProperties__Group__19__Impl8174); 
             after(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_19()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__19__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__20"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4016:1: rule__EdgeProperties__Group__20 : rule__EdgeProperties__Group__20__Impl rule__EdgeProperties__Group__21 ;
    public final void rule__EdgeProperties__Group__20() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4020:1: ( rule__EdgeProperties__Group__20__Impl rule__EdgeProperties__Group__21 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4021:2: rule__EdgeProperties__Group__20__Impl rule__EdgeProperties__Group__21
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__20__Impl_in_rule__EdgeProperties__Group__208205);
            rule__EdgeProperties__Group__20__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__21_in_rule__EdgeProperties__Group__208208);
            rule__EdgeProperties__Group__21();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__20"


    // $ANTLR start "rule__EdgeProperties__Group__20__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4028:1: rule__EdgeProperties__Group__20__Impl : ( 'target' ) ;
    public final void rule__EdgeProperties__Group__20__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4032:1: ( ( 'target' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4033:1: ( 'target' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4033:1: ( 'target' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4034:1: 'target'
            {
             before(grammarAccess.getEdgePropertiesAccess().getTargetKeyword_20()); 
            match(input,62,FollowSets000.FOLLOW_62_in_rule__EdgeProperties__Group__20__Impl8236); 
             after(grammarAccess.getEdgePropertiesAccess().getTargetKeyword_20()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__20__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__21"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4047:1: rule__EdgeProperties__Group__21 : rule__EdgeProperties__Group__21__Impl rule__EdgeProperties__Group__22 ;
    public final void rule__EdgeProperties__Group__21() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4051:1: ( rule__EdgeProperties__Group__21__Impl rule__EdgeProperties__Group__22 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4052:2: rule__EdgeProperties__Group__21__Impl rule__EdgeProperties__Group__22
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__21__Impl_in_rule__EdgeProperties__Group__218267);
            rule__EdgeProperties__Group__21__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__22_in_rule__EdgeProperties__Group__218270);
            rule__EdgeProperties__Group__22();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__21"


    // $ANTLR start "rule__EdgeProperties__Group__21__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4059:1: rule__EdgeProperties__Group__21__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__21__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4063:1: ( ( '=' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4064:1: ( '=' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4064:1: ( '=' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4065:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_21()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__21__Impl8298); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_21()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__21__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__22"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4078:1: rule__EdgeProperties__Group__22 : rule__EdgeProperties__Group__22__Impl rule__EdgeProperties__Group__23 ;
    public final void rule__EdgeProperties__Group__22() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4082:1: ( rule__EdgeProperties__Group__22__Impl rule__EdgeProperties__Group__23 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4083:2: rule__EdgeProperties__Group__22__Impl rule__EdgeProperties__Group__23
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__22__Impl_in_rule__EdgeProperties__Group__228329);
            rule__EdgeProperties__Group__22__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__23_in_rule__EdgeProperties__Group__228332);
            rule__EdgeProperties__Group__23();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__22"


    // $ANTLR start "rule__EdgeProperties__Group__22__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4090:1: rule__EdgeProperties__Group__22__Impl : ( ( rule__EdgeProperties__TgtArrowAssignment_22 ) ) ;
    public final void rule__EdgeProperties__Group__22__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4094:1: ( ( ( rule__EdgeProperties__TgtArrowAssignment_22 ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4095:1: ( ( rule__EdgeProperties__TgtArrowAssignment_22 ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4095:1: ( ( rule__EdgeProperties__TgtArrowAssignment_22 ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4096:1: ( rule__EdgeProperties__TgtArrowAssignment_22 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getTgtArrowAssignment_22()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4097:1: ( rule__EdgeProperties__TgtArrowAssignment_22 )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4097:2: rule__EdgeProperties__TgtArrowAssignment_22
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__TgtArrowAssignment_22_in_rule__EdgeProperties__Group__22__Impl8359);
            rule__EdgeProperties__TgtArrowAssignment_22();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getTgtArrowAssignment_22()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__22__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__23"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4107:1: rule__EdgeProperties__Group__23 : rule__EdgeProperties__Group__23__Impl ;
    public final void rule__EdgeProperties__Group__23() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4111:1: ( rule__EdgeProperties__Group__23__Impl )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4112:2: rule__EdgeProperties__Group__23__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__23__Impl_in_rule__EdgeProperties__Group__238389);
            rule__EdgeProperties__Group__23__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__23"


    // $ANTLR start "rule__EdgeProperties__Group__23__Impl"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4118:1: rule__EdgeProperties__Group__23__Impl : ( ')' ) ;
    public final void rule__EdgeProperties__Group__23__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4122:1: ( ( ')' ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4123:1: ( ')' )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4123:1: ( ')' )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4124:1: ')'
            {
             before(grammarAccess.getEdgePropertiesAccess().getRightParenthesisKeyword_23()); 
            match(input,52,FollowSets000.FOLLOW_52_in_rule__EdgeProperties__Group__23__Impl8417); 
             after(grammarAccess.getEdgePropertiesAccess().getRightParenthesisKeyword_23()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__23__Impl"


    // $ANTLR start "rule__ShellModel__NameAssignment_2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4186:1: rule__ShellModel__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__ShellModel__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4190:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4191:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4191:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4192:1: ruleEString
            {
             before(grammarAccess.getShellModelAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__ShellModel__NameAssignment_28501);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getShellModelAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__NameAssignment_2"


    // $ANTLR start "rule__ShellModel__CommandsAssignment_3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4201:1: rule__ShellModel__CommandsAssignment_3 : ( ruleCommand ) ;
    public final void rule__ShellModel__CommandsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4205:1: ( ( ruleCommand ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4206:1: ( ruleCommand )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4206:1: ( ruleCommand )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4207:1: ruleCommand
            {
             before(grammarAccess.getShellModelAccess().getCommandsCommandParserRuleCall_3_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleCommand_in_rule__ShellModel__CommandsAssignment_38532);
            ruleCommand();

            state._fsp--;

             after(grammarAccess.getShellModelAccess().getCommandsCommandParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ShellModel__CommandsAssignment_3"


    // $ANTLR start "rule__FragmentCommand__FragmentAssignment"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4216:1: rule__FragmentCommand__FragmentAssignment : ( ruleFragment ) ;
    public final void rule__FragmentCommand__FragmentAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4220:1: ( ( ruleFragment ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4221:1: ( ruleFragment )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4221:1: ( ruleFragment )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4222:1: ruleFragment
            {
             before(grammarAccess.getFragmentCommandAccess().getFragmentFragmentParserRuleCall_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragment_in_rule__FragmentCommand__FragmentAssignment8563);
            ruleFragment();

            state._fsp--;

             after(grammarAccess.getFragmentCommandAccess().getFragmentFragmentParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentCommand__FragmentAssignment"


    // $ANTLR start "rule__Fragment__AnnotationAssignment_1_0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4231:1: rule__Fragment__AnnotationAssignment_1_0 : ( ruleAnnotation ) ;
    public final void rule__Fragment__AnnotationAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4235:1: ( ( ruleAnnotation ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4236:1: ( ruleAnnotation )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4236:1: ( ruleAnnotation )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4237:1: ruleAnnotation
            {
             before(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Fragment__AnnotationAssignment_1_08594);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__AnnotationAssignment_1_0"


    // $ANTLR start "rule__Fragment__AnnotationAssignment_1_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4246:1: rule__Fragment__AnnotationAssignment_1_1 : ( ruleAnnotation ) ;
    public final void rule__Fragment__AnnotationAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4250:1: ( ( ruleAnnotation ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4251:1: ( ruleAnnotation )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4251:1: ( ruleAnnotation )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4252:1: ruleAnnotation
            {
             before(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Fragment__AnnotationAssignment_1_18625);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__AnnotationAssignment_1_1"


    // $ANTLR start "rule__Fragment__FtypeAssignment_2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4261:1: rule__Fragment__FtypeAssignment_2 : ( ruleFragmentType ) ;
    public final void rule__Fragment__FtypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4265:1: ( ( ruleFragmentType ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4266:1: ( ruleFragmentType )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4266:1: ( ruleFragmentType )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4267:1: ruleFragmentType
            {
             before(grammarAccess.getFragmentAccess().getFtypeFragmentTypeEnumRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentType_in_rule__Fragment__FtypeAssignment_28656);
            ruleFragmentType();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getFtypeFragmentTypeEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__FtypeAssignment_2"


    // $ANTLR start "rule__Fragment__TypeAssignment_4_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4276:1: rule__Fragment__TypeAssignment_4_1 : ( ruleEString ) ;
    public final void rule__Fragment__TypeAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4280:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4281:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4281:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4282:1: ruleEString
            {
             before(grammarAccess.getFragmentAccess().getTypeEStringParserRuleCall_4_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Fragment__TypeAssignment_4_18687);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getTypeEStringParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__TypeAssignment_4_1"


    // $ANTLR start "rule__Fragment__NameAssignment_5"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4291:1: rule__Fragment__NameAssignment_5 : ( ruleEString ) ;
    public final void rule__Fragment__NameAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4295:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4296:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4296:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4297:1: ruleEString
            {
             before(grammarAccess.getFragmentAccess().getNameEStringParserRuleCall_5_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Fragment__NameAssignment_58718);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getNameEStringParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__NameAssignment_5"


    // $ANTLR start "rule__Fragment__ObjectsAssignment_7"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4306:1: rule__Fragment__ObjectsAssignment_7 : ( ruleObject ) ;
    public final void rule__Fragment__ObjectsAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4310:1: ( ( ruleObject ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4311:1: ( ruleObject )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4311:1: ( ruleObject )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4312:1: ruleObject
            {
             before(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_7_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_rule__Fragment__ObjectsAssignment_78749);
            ruleObject();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__ObjectsAssignment_7"


    // $ANTLR start "rule__Fragment__ObjectsAssignment_8"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4321:1: rule__Fragment__ObjectsAssignment_8 : ( ruleObject ) ;
    public final void rule__Fragment__ObjectsAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4325:1: ( ( ruleObject ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4326:1: ( ruleObject )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4326:1: ( ruleObject )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4327:1: ruleObject
            {
             before(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_8_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_rule__Fragment__ObjectsAssignment_88780);
            ruleObject();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__ObjectsAssignment_8"


    // $ANTLR start "rule__Object__AnnotationAssignment_1_0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4336:1: rule__Object__AnnotationAssignment_1_0 : ( ruleAnnotation ) ;
    public final void rule__Object__AnnotationAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4340:1: ( ( ruleAnnotation ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4341:1: ( ruleAnnotation )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4341:1: ( ruleAnnotation )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4342:1: ruleAnnotation
            {
             before(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Object__AnnotationAssignment_1_08811);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__AnnotationAssignment_1_0"


    // $ANTLR start "rule__Object__AnnotationAssignment_1_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4351:1: rule__Object__AnnotationAssignment_1_1 : ( ruleAnnotation ) ;
    public final void rule__Object__AnnotationAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4355:1: ( ( ruleAnnotation ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4356:1: ( ruleAnnotation )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4356:1: ( ruleAnnotation )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4357:1: ruleAnnotation
            {
             before(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Object__AnnotationAssignment_1_18842);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__AnnotationAssignment_1_1"


    // $ANTLR start "rule__Object__NameAssignment_2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4366:1: rule__Object__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Object__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4370:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4371:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4371:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4372:1: ruleEString
            {
             before(grammarAccess.getObjectAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Object__NameAssignment_28873);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__NameAssignment_2"


    // $ANTLR start "rule__Object__TypeAssignment_4"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4381:1: rule__Object__TypeAssignment_4 : ( ruleEString ) ;
    public final void rule__Object__TypeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4385:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4386:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4386:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4387:1: ruleEString
            {
             before(grammarAccess.getObjectAccess().getTypeEStringParserRuleCall_4_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Object__TypeAssignment_48904);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getTypeEStringParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__TypeAssignment_4"


    // $ANTLR start "rule__Object__FeaturesAssignment_6_0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4396:1: rule__Object__FeaturesAssignment_6_0 : ( ruleFeature ) ;
    public final void rule__Object__FeaturesAssignment_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4400:1: ( ( ruleFeature ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4401:1: ( ruleFeature )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4401:1: ( ruleFeature )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4402:1: ruleFeature
            {
             before(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_rule__Object__FeaturesAssignment_6_08935);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__FeaturesAssignment_6_0"


    // $ANTLR start "rule__Object__FeaturesAssignment_6_2_0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4411:1: rule__Object__FeaturesAssignment_6_2_0 : ( ruleFeature ) ;
    public final void rule__Object__FeaturesAssignment_6_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4415:1: ( ( ruleFeature ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4416:1: ( ruleFeature )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4416:1: ( ruleFeature )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4417:1: ruleFeature
            {
             before(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_2_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_rule__Object__FeaturesAssignment_6_2_08966);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__FeaturesAssignment_6_2_0"


    // $ANTLR start "rule__Annotation__NameAssignment_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4426:1: rule__Annotation__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Annotation__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4430:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4431:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4431:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4432:1: ruleEString
            {
             before(grammarAccess.getAnnotationAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Annotation__NameAssignment_18997);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAnnotationAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__NameAssignment_1"


    // $ANTLR start "rule__Annotation__ParamsAssignment_2_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4441:1: rule__Annotation__ParamsAssignment_2_1 : ( ruleAnnotationParam ) ;
    public final void rule__Annotation__ParamsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4445:1: ( ( ruleAnnotationParam ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4446:1: ( ruleAnnotationParam )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4446:1: ( ruleAnnotationParam )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4447:1: ruleAnnotationParam
            {
             before(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_rule__Annotation__ParamsAssignment_2_19028);
            ruleAnnotationParam();

            state._fsp--;

             after(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__ParamsAssignment_2_1"


    // $ANTLR start "rule__Annotation__ParamsAssignment_2_2_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4456:1: rule__Annotation__ParamsAssignment_2_2_1 : ( ruleAnnotationParam ) ;
    public final void rule__Annotation__ParamsAssignment_2_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4460:1: ( ( ruleAnnotationParam ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4461:1: ( ruleAnnotationParam )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4461:1: ( ruleAnnotationParam )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4462:1: ruleAnnotationParam
            {
             before(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_2_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_rule__Annotation__ParamsAssignment_2_2_19059);
            ruleAnnotationParam();

            state._fsp--;

             after(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__ParamsAssignment_2_2_1"


    // $ANTLR start "rule__AnnotationParam__NameAssignment_0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4471:1: rule__AnnotationParam__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__AnnotationParam__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4475:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4476:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4476:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4477:1: ruleEString
            {
             before(grammarAccess.getAnnotationParamAccess().getNameEStringParserRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__AnnotationParam__NameAssignment_09090);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAnnotationParamAccess().getNameEStringParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__NameAssignment_0"


    // $ANTLR start "rule__AnnotationParam__ParamValueAssignment_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4486:1: rule__AnnotationParam__ParamValueAssignment_1 : ( ruleParamValue ) ;
    public final void rule__AnnotationParam__ParamValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4490:1: ( ( ruleParamValue ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4491:1: ( ruleParamValue )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4491:1: ( ruleParamValue )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4492:1: ruleParamValue
            {
             before(grammarAccess.getAnnotationParamAccess().getParamValueParamValueParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_rule__AnnotationParam__ParamValueAssignment_19121);
            ruleParamValue();

            state._fsp--;

             after(grammarAccess.getAnnotationParamAccess().getParamValueParamValueParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__ParamValueAssignment_1"


    // $ANTLR start "rule__ObjectValueAnnotationParam__ObjectAssignment_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4501:1: rule__ObjectValueAnnotationParam__ObjectAssignment_1 : ( ( ruleEString ) ) ;
    public final void rule__ObjectValueAnnotationParam__ObjectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4505:1: ( ( ( ruleEString ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4506:1: ( ( ruleEString ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4506:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4507:1: ( ruleEString )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectCrossReference_1_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4508:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4509:1: ruleEString
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectEStringParserRuleCall_1_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__ObjectValueAnnotationParam__ObjectAssignment_19156);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectEStringParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__ObjectAssignment_1"


    // $ANTLR start "rule__ObjectValueAnnotationParam__FeatureAssignment_2_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4520:1: rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 : ( ( ruleEString ) ) ;
    public final void rule__ObjectValueAnnotationParam__FeatureAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4524:1: ( ( ( ruleEString ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4525:1: ( ( ruleEString ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4525:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4526:1: ( ruleEString )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureCrossReference_2_1_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4527:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4528:1: ruleEString
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureEStringParserRuleCall_2_1_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__ObjectValueAnnotationParam__FeatureAssignment_2_19195);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureEStringParserRuleCall_2_1_0_1()); 

            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureCrossReference_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__FeatureAssignment_2_1"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__ValueAssignment_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4539:1: rule__PrimitiveValueAnnotationParam__ValueAssignment_1 : ( rulePrimitiveValue ) ;
    public final void rule__PrimitiveValueAnnotationParam__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4543:1: ( ( rulePrimitiveValue ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4544:1: ( rulePrimitiveValue )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4544:1: ( rulePrimitiveValue )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4545:1: rulePrimitiveValue
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValuePrimitiveValueParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_rule__PrimitiveValueAnnotationParam__ValueAssignment_19230);
            rulePrimitiveValue();

            state._fsp--;

             after(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValuePrimitiveValueParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__ValueAssignment_1"


    // $ANTLR start "rule__Attribute__AnnotationAssignment_1_0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4554:1: rule__Attribute__AnnotationAssignment_1_0 : ( ruleAnnotation ) ;
    public final void rule__Attribute__AnnotationAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4558:1: ( ( ruleAnnotation ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4559:1: ( ruleAnnotation )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4559:1: ( ruleAnnotation )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4560:1: ruleAnnotation
            {
             before(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Attribute__AnnotationAssignment_1_09261);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__AnnotationAssignment_1_0"


    // $ANTLR start "rule__Attribute__AnnotationAssignment_1_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4569:1: rule__Attribute__AnnotationAssignment_1_1 : ( ruleAnnotation ) ;
    public final void rule__Attribute__AnnotationAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4573:1: ( ( ruleAnnotation ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4574:1: ( ruleAnnotation )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4574:1: ( ruleAnnotation )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4575:1: ruleAnnotation
            {
             before(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Attribute__AnnotationAssignment_1_19292);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__AnnotationAssignment_1_1"


    // $ANTLR start "rule__Attribute__NameAssignment_3"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4584:1: rule__Attribute__NameAssignment_3 : ( ruleEString ) ;
    public final void rule__Attribute__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4588:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4589:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4589:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4590:1: ruleEString
            {
             before(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_3_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Attribute__NameAssignment_39323);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__NameAssignment_3"


    // $ANTLR start "rule__Attribute__ValuesAssignment_5"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4599:1: rule__Attribute__ValuesAssignment_5 : ( rulePrimitiveValue ) ;
    public final void rule__Attribute__ValuesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4603:1: ( ( rulePrimitiveValue ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4604:1: ( rulePrimitiveValue )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4604:1: ( rulePrimitiveValue )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4605:1: rulePrimitiveValue
            {
             before(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_5_0()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_rule__Attribute__ValuesAssignment_59354);
            rulePrimitiveValue();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__ValuesAssignment_5"


    // $ANTLR start "rule__Attribute__ValuesAssignment_6_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4614:1: rule__Attribute__ValuesAssignment_6_1 : ( rulePrimitiveValue ) ;
    public final void rule__Attribute__ValuesAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4618:1: ( ( rulePrimitiveValue ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4619:1: ( rulePrimitiveValue )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4619:1: ( rulePrimitiveValue )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4620:1: rulePrimitiveValue
            {
             before(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_6_1_0()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_rule__Attribute__ValuesAssignment_6_19385);
            rulePrimitiveValue();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__ValuesAssignment_6_1"


    // $ANTLR start "rule__IntegerValue__ValueAssignment"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4629:1: rule__IntegerValue__ValueAssignment : ( RULE_INT ) ;
    public final void rule__IntegerValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4633:1: ( ( RULE_INT ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4634:1: ( RULE_INT )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4634:1: ( RULE_INT )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4635:1: RULE_INT
            {
             before(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__IntegerValue__ValueAssignment9416); 
             after(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerValue__ValueAssignment"


    // $ANTLR start "rule__StringValue__ValueAssignment"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4644:1: rule__StringValue__ValueAssignment : ( RULE_STRING ) ;
    public final void rule__StringValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4648:1: ( ( RULE_STRING ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4649:1: ( RULE_STRING )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4649:1: ( RULE_STRING )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4650:1: RULE_STRING
            {
             before(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__StringValue__ValueAssignment9447); 
             after(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringValue__ValueAssignment"


    // $ANTLR start "rule__BooleanValue__ValueAssignment"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4659:1: rule__BooleanValue__ValueAssignment : ( ruleBooleanValueTerminal ) ;
    public final void rule__BooleanValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4663:1: ( ( ruleBooleanValueTerminal ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4664:1: ( ruleBooleanValueTerminal )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4664:1: ( ruleBooleanValueTerminal )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4665:1: ruleBooleanValueTerminal
            {
             before(grammarAccess.getBooleanValueAccess().getValueBooleanValueTerminalParserRuleCall_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_rule__BooleanValue__ValueAssignment9478);
            ruleBooleanValueTerminal();

            state._fsp--;

             after(grammarAccess.getBooleanValueAccess().getValueBooleanValueTerminalParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanValue__ValueAssignment"


    // $ANTLR start "rule__Reference__AnnotationAssignment_1_0"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4674:1: rule__Reference__AnnotationAssignment_1_0 : ( ruleAnnotation ) ;
    public final void rule__Reference__AnnotationAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4678:1: ( ( ruleAnnotation ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4679:1: ( ruleAnnotation )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4679:1: ( ruleAnnotation )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4680:1: ruleAnnotation
            {
             before(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Reference__AnnotationAssignment_1_09509);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__AnnotationAssignment_1_0"


    // $ANTLR start "rule__Reference__AnnotationAssignment_1_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4689:1: rule__Reference__AnnotationAssignment_1_1 : ( ruleAnnotation ) ;
    public final void rule__Reference__AnnotationAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4693:1: ( ( ruleAnnotation ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4694:1: ( ruleAnnotation )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4694:1: ( ruleAnnotation )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4695:1: ruleAnnotation
            {
             before(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Reference__AnnotationAssignment_1_19540);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__AnnotationAssignment_1_1"


    // $ANTLR start "rule__Reference__GraphicPropertiesAssignment_2"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4704:1: rule__Reference__GraphicPropertiesAssignment_2 : ( ruleEdgeProperties ) ;
    public final void rule__Reference__GraphicPropertiesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4708:1: ( ( ruleEdgeProperties ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4709:1: ( ruleEdgeProperties )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4709:1: ( ruleEdgeProperties )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4710:1: ruleEdgeProperties
            {
             before(grammarAccess.getReferenceAccess().getGraphicPropertiesEdgePropertiesParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEdgeProperties_in_rule__Reference__GraphicPropertiesAssignment_29571);
            ruleEdgeProperties();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getGraphicPropertiesEdgePropertiesParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__GraphicPropertiesAssignment_2"


    // $ANTLR start "rule__Reference__NameAssignment_4"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4719:1: rule__Reference__NameAssignment_4 : ( ruleEString ) ;
    public final void rule__Reference__NameAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4723:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4724:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4724:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4725:1: ruleEString
            {
             before(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_4_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Reference__NameAssignment_49602);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__NameAssignment_4"


    // $ANTLR start "rule__Reference__ReferenceAssignment_6"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4734:1: rule__Reference__ReferenceAssignment_6 : ( ( ruleEString ) ) ;
    public final void rule__Reference__ReferenceAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4738:1: ( ( ( ruleEString ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4739:1: ( ( ruleEString ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4739:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4740:1: ( ruleEString )
            {
             before(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_6_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4741:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4742:1: ruleEString
            {
             before(grammarAccess.getReferenceAccess().getReferenceObjectEStringParserRuleCall_6_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Reference__ReferenceAssignment_69637);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getReferenceObjectEStringParserRuleCall_6_0_1()); 

            }

             after(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__ReferenceAssignment_6"


    // $ANTLR start "rule__Reference__ReferenceAssignment_7_1"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4753:1: rule__Reference__ReferenceAssignment_7_1 : ( ( ruleEString ) ) ;
    public final void rule__Reference__ReferenceAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4757:1: ( ( ( ruleEString ) ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4758:1: ( ( ruleEString ) )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4758:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4759:1: ( ruleEString )
            {
             before(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_7_1_0()); 
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4760:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4761:1: ruleEString
            {
             before(grammarAccess.getReferenceAccess().getReferenceObjectEStringParserRuleCall_7_1_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Reference__ReferenceAssignment_7_19676);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getReferenceObjectEStringParserRuleCall_7_1_0_1()); 

            }

             after(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__ReferenceAssignment_7_1"


    // $ANTLR start "rule__EdgeProperties__ColorAssignment_6"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4772:1: rule__EdgeProperties__ColorAssignment_6 : ( ruleEString ) ;
    public final void rule__EdgeProperties__ColorAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4776:1: ( ( ruleEString ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4777:1: ( ruleEString )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4777:1: ( ruleEString )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4778:1: ruleEString
            {
             before(grammarAccess.getEdgePropertiesAccess().getColorEStringParserRuleCall_6_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__EdgeProperties__ColorAssignment_69711);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesAccess().getColorEStringParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__ColorAssignment_6"


    // $ANTLR start "rule__EdgeProperties__WidthAssignment_10"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4787:1: rule__EdgeProperties__WidthAssignment_10 : ( RULE_INT ) ;
    public final void rule__EdgeProperties__WidthAssignment_10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4791:1: ( ( RULE_INT ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4792:1: ( RULE_INT )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4792:1: ( RULE_INT )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4793:1: RULE_INT
            {
             before(grammarAccess.getEdgePropertiesAccess().getWidthINTTerminalRuleCall_10_0()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__EdgeProperties__WidthAssignment_109742); 
             after(grammarAccess.getEdgePropertiesAccess().getWidthINTTerminalRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__WidthAssignment_10"


    // $ANTLR start "rule__EdgeProperties__LineTypeAssignment_14"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4802:1: rule__EdgeProperties__LineTypeAssignment_14 : ( ruleLineType ) ;
    public final void rule__EdgeProperties__LineTypeAssignment_14() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4806:1: ( ( ruleLineType ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4807:1: ( ruleLineType )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4807:1: ( ruleLineType )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4808:1: ruleLineType
            {
             before(grammarAccess.getEdgePropertiesAccess().getLineTypeLineTypeEnumRuleCall_14_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleLineType_in_rule__EdgeProperties__LineTypeAssignment_149773);
            ruleLineType();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesAccess().getLineTypeLineTypeEnumRuleCall_14_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__LineTypeAssignment_14"


    // $ANTLR start "rule__EdgeProperties__SrcArrowAssignment_18"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4817:1: rule__EdgeProperties__SrcArrowAssignment_18 : ( ruleArrowType ) ;
    public final void rule__EdgeProperties__SrcArrowAssignment_18() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4821:1: ( ( ruleArrowType ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4822:1: ( ruleArrowType )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4822:1: ( ruleArrowType )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4823:1: ruleArrowType
            {
             before(grammarAccess.getEdgePropertiesAccess().getSrcArrowArrowTypeEnumRuleCall_18_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleArrowType_in_rule__EdgeProperties__SrcArrowAssignment_189804);
            ruleArrowType();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesAccess().getSrcArrowArrowTypeEnumRuleCall_18_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__SrcArrowAssignment_18"


    // $ANTLR start "rule__EdgeProperties__TgtArrowAssignment_22"
    // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4832:1: rule__EdgeProperties__TgtArrowAssignment_22 : ( ruleArrowType ) ;
    public final void rule__EdgeProperties__TgtArrowAssignment_22() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4836:1: ( ( ruleArrowType ) )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4837:1: ( ruleArrowType )
            {
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4837:1: ( ruleArrowType )
            // ../metabup.shell.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelingShell.g:4838:1: ruleArrowType
            {
             before(grammarAccess.getEdgePropertiesAccess().getTgtArrowArrowTypeEnumRuleCall_22_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleArrowType_in_rule__EdgeProperties__TgtArrowAssignment_229835);
            ruleArrowType();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesAccess().getTgtArrowArrowTypeEnumRuleCall_22_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__TgtArrowAssignment_22"

    // Delegated rules


    protected DFA1 dfa1 = new DFA1(this);
    static final String DFA1_eotS =
        "\103\uffff";
    static final String DFA1_eofS =
        "\103\uffff";
    static final String DFA1_minS =
        "\1\13\1\4\2\uffff\2\13\2\4\2\66\2\13\3\4\6\64\2\66\1\4\1\13\3\4"+
        "\2\66\10\64\3\4\1\13\1\4\6\64\2\66\2\64\3\4\10\64\1\4\2\64";
    static final String DFA1_maxS =
        "\1\71\1\72\2\uffff\2\71\1\5\1\72\2\70\2\71\1\16\2\5\4\65\2\67\2"+
        "\70\1\5\1\71\1\5\1\16\1\5\2\70\6\65\2\67\1\16\2\5\1\71\1\5\4\65"+
        "\2\67\2\70\2\65\1\5\1\16\1\5\6\65\2\67\1\5\2\65";
    static final String DFA1_acceptS =
        "\2\uffff\1\1\1\2\77\uffff";
    static final String DFA1_specialS =
        "\103\uffff}>";
    static final String[] DFA1_transitionS = {
            "\2\3\45\uffff\1\1\6\uffff\1\2",
            "\1\4\1\5\64\uffff\1\3",
            "",
            "",
            "\2\3\45\uffff\1\7\1\6\5\uffff\1\2",
            "\2\3\45\uffff\1\7\1\6\5\uffff\1\2",
            "\1\10\1\11",
            "\1\12\1\13\64\uffff\1\3",
            "\1\15\1\uffff\1\14",
            "\1\15\1\uffff\1\14",
            "\2\3\45\uffff\1\7\1\16\5\uffff\1\2",
            "\2\3\45\uffff\1\7\1\16\5\uffff\1\2",
            "\1\17\1\uffff\1\20\6\uffff\1\21\1\22",
            "\1\23\1\24",
            "\1\25\1\26",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27\1\uffff\1\31",
            "\1\30\1\27\1\uffff\1\31",
            "\1\33\1\uffff\1\32",
            "\1\33\1\uffff\1\32",
            "\1\34\1\35",
            "\2\3\45\uffff\1\7\6\uffff\1\2",
            "\1\36\1\37",
            "\1\40\1\uffff\1\41\6\uffff\1\42\1\43",
            "\1\44\1\45",
            "\1\47\1\uffff\1\46",
            "\1\47\1\uffff\1\46",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50\1\uffff\1\52",
            "\1\51\1\50\1\uffff\1\52",
            "\1\53\1\uffff\1\54\6\uffff\1\55\1\56",
            "\1\57\1\60",
            "\1\61\1\62",
            "\2\3\45\uffff\1\7\6\uffff\1\2",
            "\1\63\1\64",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27\1\uffff\1\65",
            "\1\30\1\27\1\uffff\1\65",
            "\1\67\1\uffff\1\66",
            "\1\67\1\uffff\1\66",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\70\1\71",
            "\1\72\1\uffff\1\73\6\uffff\1\74\1\75",
            "\1\76\1\77",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50\1\uffff\1\100",
            "\1\51\1\50\1\uffff\1\100",
            "\1\101\1\102",
            "\1\51\1\50",
            "\1\51\1\50"
    };

    static final short[] DFA1_eot = DFA.unpackEncodedString(DFA1_eotS);
    static final short[] DFA1_eof = DFA.unpackEncodedString(DFA1_eofS);
    static final char[] DFA1_min = DFA.unpackEncodedStringToUnsignedChars(DFA1_minS);
    static final char[] DFA1_max = DFA.unpackEncodedStringToUnsignedChars(DFA1_maxS);
    static final short[] DFA1_accept = DFA.unpackEncodedString(DFA1_acceptS);
    static final short[] DFA1_special = DFA.unpackEncodedString(DFA1_specialS);
    static final short[][] DFA1_transition;

    static {
        int numStates = DFA1_transitionS.length;
        DFA1_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA1_transition[i] = DFA.unpackEncodedString(DFA1_transitionS[i]);
        }
    }

    class DFA1 extends DFA {

        public DFA1(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = DFA1_eot;
            this.eof = DFA1_eof;
            this.min = DFA1_min;
            this.max = DFA1_max;
            this.accept = DFA1_accept;
            this.special = DFA1_special;
            this.transition = DFA1_transition;
        }
        public String getDescription() {
            return "677:1: rule__Feature__Alternatives : ( ( ruleAttribute ) | ( ruleReference ) );";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleShellModel_in_entryRuleShellModel61 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleShellModel68 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ShellModel__Group__0_in_ruleShellModel94 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCommand_in_entryRuleCommand121 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleCommand128 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentCommand_in_ruleCommand154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentCommand_in_entryRuleFragmentCommand180 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentCommand187 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentCommand__FragmentAssignment_in_ruleFragmentCommand213 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragment_in_entryRuleFragment240 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragment247 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__0_in_ruleFragment273 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature300 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeature307 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Alternatives_in_ruleFeature333 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString360 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString367 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EString__Alternatives_in_ruleEString393 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObject_in_entryRuleObject420 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObject427 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__0_in_ruleObject453 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_entryRuleAnnotation480 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotation487 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group__0_in_ruleAnnotation513 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam540 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotationParam547 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__Group__0_in_ruleAnnotationParam573 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParamValue_in_entryRuleParamValue600 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleParamValue607 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ParamValue__Alternatives_in_ruleParamValue633 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam660 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam667 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__0_in_ruleObjectValueAnnotationParam693 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam720 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam727 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__Group__0_in_rulePrimitiveValueAnnotationParam753 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_entryRuleAttribute780 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttribute787 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__0_in_ruleAttribute813 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue840 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValue847 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValue__Alternatives_in_rulePrimitiveValue873 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue900 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntegerValue907 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__IntegerValue__ValueAssignment_in_ruleIntegerValue933 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_entryRuleStringValue960 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringValue967 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__StringValue__ValueAssignment_in_ruleStringValue993 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue1020 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValue1027 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BooleanValue__ValueAssignment_in_ruleBooleanValue1053 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_entryRuleReference1080 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReference1087 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__0_in_ruleReference1113 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEdgeProperties_in_entryRuleEdgeProperties1140 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEdgeProperties1147 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__0_in_ruleEdgeProperties1173 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal1200 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValueTerminal1207 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BooleanValueTerminal__Alternatives_in_ruleBooleanValueTerminal1233 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentType__Alternatives_in_ruleFragmentType1270 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__LineType__Alternatives_in_ruleLineType1306 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ArrowType__Alternatives_in_ruleArrowType1342 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_rule__Feature__Alternatives1377 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_rule__Feature__Alternatives1394 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__EString__Alternatives1426 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__EString__Alternatives1443 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_rule__ParamValue__Alternatives1475 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_rule__ParamValue__Alternatives1492 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_rule__PrimitiveValue__Alternatives1524 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_rule__PrimitiveValue__Alternatives1541 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_rule__PrimitiveValue__Alternatives1558 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_rule__Reference__Alternatives_31591 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__Reference__Alternatives_31611 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__BooleanValueTerminal__Alternatives1646 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__BooleanValueTerminal__Alternatives1666 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__FragmentType__Alternatives1701 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_rule__FragmentType__Alternatives1722 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__LineType__Alternatives1758 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_rule__LineType__Alternatives1779 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__LineType__Alternatives1800 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__LineType__Alternatives1821 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__ArrowType__Alternatives1857 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__ArrowType__Alternatives1878 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_rule__ArrowType__Alternatives1899 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_rule__ArrowType__Alternatives1920 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_rule__ArrowType__Alternatives1941 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__ArrowType__Alternatives1962 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__ArrowType__Alternatives1983 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_28_in_rule__ArrowType__Alternatives2004 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_rule__ArrowType__Alternatives2025 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_30_in_rule__ArrowType__Alternatives2046 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_31_in_rule__ArrowType__Alternatives2067 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_rule__ArrowType__Alternatives2088 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_33_in_rule__ArrowType__Alternatives2109 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_34_in_rule__ArrowType__Alternatives2130 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_35_in_rule__ArrowType__Alternatives2151 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_rule__ArrowType__Alternatives2172 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_rule__ArrowType__Alternatives2193 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_38_in_rule__ArrowType__Alternatives2214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_39_in_rule__ArrowType__Alternatives2235 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_40_in_rule__ArrowType__Alternatives2256 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_41_in_rule__ArrowType__Alternatives2277 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ShellModel__Group__0__Impl_in_rule__ShellModel__Group__02310 = new BitSet(new long[]{0x0000040000000000L});
        public static final BitSet FOLLOW_rule__ShellModel__Group__1_in_rule__ShellModel__Group__02313 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ShellModel__Group__1__Impl_in_rule__ShellModel__Group__12371 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__ShellModel__Group__2_in_rule__ShellModel__Group__12374 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_42_in_rule__ShellModel__Group__1__Impl2402 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ShellModel__Group__2__Impl_in_rule__ShellModel__Group__22433 = new BitSet(new long[]{0x0004080000018000L});
        public static final BitSet FOLLOW_rule__ShellModel__Group__3_in_rule__ShellModel__Group__22436 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ShellModel__NameAssignment_2_in_rule__ShellModel__Group__2__Impl2463 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ShellModel__Group__3__Impl_in_rule__ShellModel__Group__32493 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ShellModel__CommandsAssignment_3_in_rule__ShellModel__Group__3__Impl2520 = new BitSet(new long[]{0x0004080000018002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__0__Impl_in_rule__Fragment__Group__02559 = new BitSet(new long[]{0x0004080000018000L});
        public static final BitSet FOLLOW_rule__Fragment__Group__1_in_rule__Fragment__Group__02562 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__1__Impl_in_rule__Fragment__Group__12620 = new BitSet(new long[]{0x0004080000018000L});
        public static final BitSet FOLLOW_rule__Fragment__Group__2_in_rule__Fragment__Group__12623 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_1__0_in_rule__Fragment__Group__1__Impl2650 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__2__Impl_in_rule__Fragment__Group__22681 = new BitSet(new long[]{0x0004080000018000L});
        public static final BitSet FOLLOW_rule__Fragment__Group__3_in_rule__Fragment__Group__22684 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__FtypeAssignment_2_in_rule__Fragment__Group__2__Impl2711 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__3__Impl_in_rule__Fragment__Group__32742 = new BitSet(new long[]{0x0000400000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__4_in_rule__Fragment__Group__32745 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_43_in_rule__Fragment__Group__3__Impl2773 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__4__Impl_in_rule__Fragment__Group__42804 = new BitSet(new long[]{0x0000400000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__5_in_rule__Fragment__Group__42807 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__0_in_rule__Fragment__Group__4__Impl2834 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__5__Impl_in_rule__Fragment__Group__52865 = new BitSet(new long[]{0x0000100000000000L});
        public static final BitSet FOLLOW_rule__Fragment__Group__6_in_rule__Fragment__Group__52868 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__NameAssignment_5_in_rule__Fragment__Group__5__Impl2895 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__6__Impl_in_rule__Fragment__Group__62925 = new BitSet(new long[]{0x0004000000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__7_in_rule__Fragment__Group__62928 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_44_in_rule__Fragment__Group__6__Impl2956 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__7__Impl_in_rule__Fragment__Group__72987 = new BitSet(new long[]{0x0004200000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__8_in_rule__Fragment__Group__72990 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__ObjectsAssignment_7_in_rule__Fragment__Group__7__Impl3017 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__8__Impl_in_rule__Fragment__Group__83047 = new BitSet(new long[]{0x0004200000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__9_in_rule__Fragment__Group__83050 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__ObjectsAssignment_8_in_rule__Fragment__Group__8__Impl3077 = new BitSet(new long[]{0x0004000000000032L});
        public static final BitSet FOLLOW_rule__Fragment__Group__9__Impl_in_rule__Fragment__Group__93108 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_45_in_rule__Fragment__Group__9__Impl3136 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_1__0__Impl_in_rule__Fragment__Group_1__03187 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__Fragment__Group_1__1_in_rule__Fragment__Group_1__03190 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__AnnotationAssignment_1_0_in_rule__Fragment__Group_1__0__Impl3217 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_1__1__Impl_in_rule__Fragment__Group_1__13247 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__AnnotationAssignment_1_1_in_rule__Fragment__Group_1__1__Impl3274 = new BitSet(new long[]{0x0004000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__0__Impl_in_rule__Fragment__Group_4__03309 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__1_in_rule__Fragment__Group_4__03312 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_46_in_rule__Fragment__Group_4__0__Impl3340 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__1__Impl_in_rule__Fragment__Group_4__13371 = new BitSet(new long[]{0x0000800000000000L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__2_in_rule__Fragment__Group_4__13374 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__TypeAssignment_4_1_in_rule__Fragment__Group_4__1__Impl3401 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__2__Impl_in_rule__Fragment__Group_4__23431 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_47_in_rule__Fragment__Group_4__2__Impl3459 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__0__Impl_in_rule__Object__Group__03496 = new BitSet(new long[]{0x0004000000000030L});
        public static final BitSet FOLLOW_rule__Object__Group__1_in_rule__Object__Group__03499 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__1__Impl_in_rule__Object__Group__13557 = new BitSet(new long[]{0x0004000000000030L});
        public static final BitSet FOLLOW_rule__Object__Group__2_in_rule__Object__Group__13560 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_1__0_in_rule__Object__Group__1__Impl3587 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__2__Impl_in_rule__Object__Group__23618 = new BitSet(new long[]{0x0001000000000000L});
        public static final BitSet FOLLOW_rule__Object__Group__3_in_rule__Object__Group__23621 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__NameAssignment_2_in_rule__Object__Group__2__Impl3648 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__3__Impl_in_rule__Object__Group__33678 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Object__Group__4_in_rule__Object__Group__33681 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_48_in_rule__Object__Group__3__Impl3709 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__4__Impl_in_rule__Object__Group__43740 = new BitSet(new long[]{0x0000100000000000L});
        public static final BitSet FOLLOW_rule__Object__Group__5_in_rule__Object__Group__43743 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__TypeAssignment_4_in_rule__Object__Group__4__Impl3770 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__5__Impl_in_rule__Object__Group__53800 = new BitSet(new long[]{0x0204200000001800L});
        public static final BitSet FOLLOW_rule__Object__Group__6_in_rule__Object__Group__53803 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_44_in_rule__Object__Group__5__Impl3831 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__6__Impl_in_rule__Object__Group__63862 = new BitSet(new long[]{0x0204200000001800L});
        public static final BitSet FOLLOW_rule__Object__Group__7_in_rule__Object__Group__63865 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6__0_in_rule__Object__Group__6__Impl3892 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__7__Impl_in_rule__Object__Group__73923 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_45_in_rule__Object__Group__7__Impl3951 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_1__0__Impl_in_rule__Object__Group_1__03998 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__Object__Group_1__1_in_rule__Object__Group_1__04001 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__AnnotationAssignment_1_0_in_rule__Object__Group_1__0__Impl4028 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_1__1__Impl_in_rule__Object__Group_1__14058 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__AnnotationAssignment_1_1_in_rule__Object__Group_1__1__Impl4085 = new BitSet(new long[]{0x0004000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6__0__Impl_in_rule__Object__Group_6__04120 = new BitSet(new long[]{0x0206000000001800L});
        public static final BitSet FOLLOW_rule__Object__Group_6__1_in_rule__Object__Group_6__04123 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__FeaturesAssignment_6_0_in_rule__Object__Group_6__0__Impl4150 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6__1__Impl_in_rule__Object__Group_6__14180 = new BitSet(new long[]{0x0206000000001800L});
        public static final BitSet FOLLOW_rule__Object__Group_6__2_in_rule__Object__Group_6__14183 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_49_in_rule__Object__Group_6__1__Impl4212 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6__2__Impl_in_rule__Object__Group_6__24245 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6_2__0_in_rule__Object__Group_6__2__Impl4272 = new BitSet(new long[]{0x0204000000001802L});
        public static final BitSet FOLLOW_rule__Object__Group_6_2__0__Impl_in_rule__Object__Group_6_2__04309 = new BitSet(new long[]{0x0002000000000000L});
        public static final BitSet FOLLOW_rule__Object__Group_6_2__1_in_rule__Object__Group_6_2__04312 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__FeaturesAssignment_6_2_0_in_rule__Object__Group_6_2__0__Impl4339 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6_2__1__Impl_in_rule__Object__Group_6_2__14369 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_49_in_rule__Object__Group_6_2__1__Impl4398 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group__0__Impl_in_rule__Annotation__Group__04435 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Annotation__Group__1_in_rule__Annotation__Group__04438 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_50_in_rule__Annotation__Group__0__Impl4466 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group__1__Impl_in_rule__Annotation__Group__14497 = new BitSet(new long[]{0x0008000000000000L});
        public static final BitSet FOLLOW_rule__Annotation__Group__2_in_rule__Annotation__Group__14500 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__NameAssignment_1_in_rule__Annotation__Group__1__Impl4527 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group__2__Impl_in_rule__Annotation__Group__24557 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__0_in_rule__Annotation__Group__2__Impl4584 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__0__Impl_in_rule__Annotation__Group_2__04621 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__1_in_rule__Annotation__Group_2__04624 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_51_in_rule__Annotation__Group_2__0__Impl4652 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__1__Impl_in_rule__Annotation__Group_2__14683 = new BitSet(new long[]{0x0030000000000000L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__2_in_rule__Annotation__Group_2__14686 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__ParamsAssignment_2_1_in_rule__Annotation__Group_2__1__Impl4713 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__2__Impl_in_rule__Annotation__Group_2__24743 = new BitSet(new long[]{0x0030000000000000L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__3_in_rule__Annotation__Group_2__24746 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2_2__0_in_rule__Annotation__Group_2__2__Impl4773 = new BitSet(new long[]{0x0020000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__3__Impl_in_rule__Annotation__Group_2__34804 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_52_in_rule__Annotation__Group_2__3__Impl4832 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2_2__0__Impl_in_rule__Annotation__Group_2_2__04871 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2_2__1_in_rule__Annotation__Group_2_2__04874 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__Annotation__Group_2_2__0__Impl4902 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2_2__1__Impl_in_rule__Annotation__Group_2_2__14933 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__ParamsAssignment_2_2_1_in_rule__Annotation__Group_2_2__1__Impl4960 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__Group__0__Impl_in_rule__AnnotationParam__Group__04994 = new BitSet(new long[]{0x0140000000000000L});
        public static final BitSet FOLLOW_rule__AnnotationParam__Group__1_in_rule__AnnotationParam__Group__04997 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__NameAssignment_0_in_rule__AnnotationParam__Group__0__Impl5024 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__Group__1__Impl_in_rule__AnnotationParam__Group__15054 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__ParamValueAssignment_1_in_rule__AnnotationParam__Group__1__Impl5081 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__0__Impl_in_rule__ObjectValueAnnotationParam__Group__05115 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__1_in_rule__ObjectValueAnnotationParam__Group__05118 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_54_in_rule__ObjectValueAnnotationParam__Group__0__Impl5146 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__1__Impl_in_rule__ObjectValueAnnotationParam__Group__15177 = new BitSet(new long[]{0x0080000000000000L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__2_in_rule__ObjectValueAnnotationParam__Group__15180 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__ObjectAssignment_1_in_rule__ObjectValueAnnotationParam__Group__1__Impl5207 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__2__Impl_in_rule__ObjectValueAnnotationParam__Group__25237 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group_2__0_in_rule__ObjectValueAnnotationParam__Group__2__Impl5264 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group_2__0__Impl_in_rule__ObjectValueAnnotationParam__Group_2__05301 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group_2__1_in_rule__ObjectValueAnnotationParam__Group_2__05304 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_55_in_rule__ObjectValueAnnotationParam__Group_2__0__Impl5332 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group_2__1__Impl_in_rule__ObjectValueAnnotationParam__Group_2__15363 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__FeatureAssignment_2_1_in_rule__ObjectValueAnnotationParam__Group_2__1__Impl5390 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__Group__0__Impl_in_rule__PrimitiveValueAnnotationParam__Group__05424 = new BitSet(new long[]{0x0000000000006050L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__Group__1_in_rule__PrimitiveValueAnnotationParam__Group__05427 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__PrimitiveValueAnnotationParam__Group__0__Impl5455 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__Group__1__Impl_in_rule__PrimitiveValueAnnotationParam__Group__15486 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__ValueAssignment_1_in_rule__PrimitiveValueAnnotationParam__Group__1__Impl5513 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__0__Impl_in_rule__Attribute__Group__05547 = new BitSet(new long[]{0x0204000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__1_in_rule__Attribute__Group__05550 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__1__Impl_in_rule__Attribute__Group__15608 = new BitSet(new long[]{0x0204000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__2_in_rule__Attribute__Group__15611 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_1__0_in_rule__Attribute__Group__1__Impl5638 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__2__Impl_in_rule__Attribute__Group__25669 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Attribute__Group__3_in_rule__Attribute__Group__25672 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_57_in_rule__Attribute__Group__2__Impl5700 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__3__Impl_in_rule__Attribute__Group__35731 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__4_in_rule__Attribute__Group__35734 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__NameAssignment_3_in_rule__Attribute__Group__3__Impl5761 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__4__Impl_in_rule__Attribute__Group__45791 = new BitSet(new long[]{0x0000000000006050L});
        public static final BitSet FOLLOW_rule__Attribute__Group__5_in_rule__Attribute__Group__45794 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__Attribute__Group__4__Impl5822 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__5__Impl_in_rule__Attribute__Group__55853 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__6_in_rule__Attribute__Group__55856 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__ValuesAssignment_5_in_rule__Attribute__Group__5__Impl5883 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__6__Impl_in_rule__Attribute__Group__65913 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_6__0_in_rule__Attribute__Group__6__Impl5940 = new BitSet(new long[]{0x0020000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_1__0__Impl_in_rule__Attribute__Group_1__05985 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group_1__1_in_rule__Attribute__Group_1__05988 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__AnnotationAssignment_1_0_in_rule__Attribute__Group_1__0__Impl6015 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_1__1__Impl_in_rule__Attribute__Group_1__16045 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__AnnotationAssignment_1_1_in_rule__Attribute__Group_1__1__Impl6072 = new BitSet(new long[]{0x0004000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_6__0__Impl_in_rule__Attribute__Group_6__06107 = new BitSet(new long[]{0x0000000000006050L});
        public static final BitSet FOLLOW_rule__Attribute__Group_6__1_in_rule__Attribute__Group_6__06110 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__Attribute__Group_6__0__Impl6138 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_6__1__Impl_in_rule__Attribute__Group_6__16169 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__ValuesAssignment_6_1_in_rule__Attribute__Group_6__1__Impl6196 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__0__Impl_in_rule__Reference__Group__06230 = new BitSet(new long[]{0x0204000000001800L});
        public static final BitSet FOLLOW_rule__Reference__Group__1_in_rule__Reference__Group__06233 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__1__Impl_in_rule__Reference__Group__16291 = new BitSet(new long[]{0x0204000000001800L});
        public static final BitSet FOLLOW_rule__Reference__Group__2_in_rule__Reference__Group__16294 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_1__0_in_rule__Reference__Group__1__Impl6321 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__2__Impl_in_rule__Reference__Group__26352 = new BitSet(new long[]{0x0204000000001800L});
        public static final BitSet FOLLOW_rule__Reference__Group__3_in_rule__Reference__Group__26355 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__GraphicPropertiesAssignment_2_in_rule__Reference__Group__2__Impl6382 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__3__Impl_in_rule__Reference__Group__36413 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Reference__Group__4_in_rule__Reference__Group__36416 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Alternatives_3_in_rule__Reference__Group__3__Impl6443 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__4__Impl_in_rule__Reference__Group__46473 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__Reference__Group__5_in_rule__Reference__Group__46476 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__NameAssignment_4_in_rule__Reference__Group__4__Impl6503 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__5__Impl_in_rule__Reference__Group__56533 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Reference__Group__6_in_rule__Reference__Group__56536 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__Reference__Group__5__Impl6564 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__6__Impl_in_rule__Reference__Group__66595 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__Reference__Group__7_in_rule__Reference__Group__66598 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__ReferenceAssignment_6_in_rule__Reference__Group__6__Impl6625 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__7__Impl_in_rule__Reference__Group__76655 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_7__0_in_rule__Reference__Group__7__Impl6682 = new BitSet(new long[]{0x0020000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_1__0__Impl_in_rule__Reference__Group_1__06729 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__Reference__Group_1__1_in_rule__Reference__Group_1__06732 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__AnnotationAssignment_1_0_in_rule__Reference__Group_1__0__Impl6759 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_1__1__Impl_in_rule__Reference__Group_1__16789 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__AnnotationAssignment_1_1_in_rule__Reference__Group_1__1__Impl6816 = new BitSet(new long[]{0x0004000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_7__0__Impl_in_rule__Reference__Group_7__06851 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Reference__Group_7__1_in_rule__Reference__Group_7__06854 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__Reference__Group_7__0__Impl6882 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_7__1__Impl_in_rule__Reference__Group_7__16913 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__ReferenceAssignment_7_1_in_rule__Reference__Group_7__1__Impl6940 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__0__Impl_in_rule__EdgeProperties__Group__06974 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__1_in_rule__EdgeProperties__Group__06977 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__1__Impl_in_rule__EdgeProperties__Group__17035 = new BitSet(new long[]{0x0400000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__2_in_rule__EdgeProperties__Group__17038 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_50_in_rule__EdgeProperties__Group__1__Impl7066 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__2__Impl_in_rule__EdgeProperties__Group__27097 = new BitSet(new long[]{0x0008000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__3_in_rule__EdgeProperties__Group__27100 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_58_in_rule__EdgeProperties__Group__2__Impl7128 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__3__Impl_in_rule__EdgeProperties__Group__37159 = new BitSet(new long[]{0x0800000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__4_in_rule__EdgeProperties__Group__37162 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_51_in_rule__EdgeProperties__Group__3__Impl7190 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__4__Impl_in_rule__EdgeProperties__Group__47221 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__5_in_rule__EdgeProperties__Group__47224 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_59_in_rule__EdgeProperties__Group__4__Impl7252 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__5__Impl_in_rule__EdgeProperties__Group__57283 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__6_in_rule__EdgeProperties__Group__57286 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__5__Impl7314 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__6__Impl_in_rule__EdgeProperties__Group__67345 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__7_in_rule__EdgeProperties__Group__67348 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__ColorAssignment_6_in_rule__EdgeProperties__Group__6__Impl7375 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__7__Impl_in_rule__EdgeProperties__Group__77405 = new BitSet(new long[]{0x1000000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__8_in_rule__EdgeProperties__Group__77408 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__EdgeProperties__Group__7__Impl7436 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__8__Impl_in_rule__EdgeProperties__Group__87467 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__9_in_rule__EdgeProperties__Group__87470 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_60_in_rule__EdgeProperties__Group__8__Impl7498 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__9__Impl_in_rule__EdgeProperties__Group__97529 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__10_in_rule__EdgeProperties__Group__97532 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__9__Impl7560 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__10__Impl_in_rule__EdgeProperties__Group__107591 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__11_in_rule__EdgeProperties__Group__107594 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__WidthAssignment_10_in_rule__EdgeProperties__Group__10__Impl7621 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__11__Impl_in_rule__EdgeProperties__Group__117651 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__12_in_rule__EdgeProperties__Group__117654 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__EdgeProperties__Group__11__Impl7682 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__12__Impl_in_rule__EdgeProperties__Group__127713 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__13_in_rule__EdgeProperties__Group__127716 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__EdgeProperties__Group__12__Impl7744 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__13__Impl_in_rule__EdgeProperties__Group__137775 = new BitSet(new long[]{0x00000000001E0000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__14_in_rule__EdgeProperties__Group__137778 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__13__Impl7806 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__14__Impl_in_rule__EdgeProperties__Group__147837 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__15_in_rule__EdgeProperties__Group__147840 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__LineTypeAssignment_14_in_rule__EdgeProperties__Group__14__Impl7867 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__15__Impl_in_rule__EdgeProperties__Group__157897 = new BitSet(new long[]{0x2000000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__16_in_rule__EdgeProperties__Group__157900 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__EdgeProperties__Group__15__Impl7928 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__16__Impl_in_rule__EdgeProperties__Group__167959 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__17_in_rule__EdgeProperties__Group__167962 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_61_in_rule__EdgeProperties__Group__16__Impl7990 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__17__Impl_in_rule__EdgeProperties__Group__178021 = new BitSet(new long[]{0x000003FFFFE00000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__18_in_rule__EdgeProperties__Group__178024 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__17__Impl8052 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__18__Impl_in_rule__EdgeProperties__Group__188083 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__19_in_rule__EdgeProperties__Group__188086 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__SrcArrowAssignment_18_in_rule__EdgeProperties__Group__18__Impl8113 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__19__Impl_in_rule__EdgeProperties__Group__198143 = new BitSet(new long[]{0x4000000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__20_in_rule__EdgeProperties__Group__198146 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__EdgeProperties__Group__19__Impl8174 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__20__Impl_in_rule__EdgeProperties__Group__208205 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__21_in_rule__EdgeProperties__Group__208208 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_62_in_rule__EdgeProperties__Group__20__Impl8236 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__21__Impl_in_rule__EdgeProperties__Group__218267 = new BitSet(new long[]{0x000003FFFFE00000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__22_in_rule__EdgeProperties__Group__218270 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__21__Impl8298 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__22__Impl_in_rule__EdgeProperties__Group__228329 = new BitSet(new long[]{0x0010000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__23_in_rule__EdgeProperties__Group__228332 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__TgtArrowAssignment_22_in_rule__EdgeProperties__Group__22__Impl8359 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__23__Impl_in_rule__EdgeProperties__Group__238389 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_52_in_rule__EdgeProperties__Group__23__Impl8417 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__ShellModel__NameAssignment_28501 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCommand_in_rule__ShellModel__CommandsAssignment_38532 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragment_in_rule__FragmentCommand__FragmentAssignment8563 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Fragment__AnnotationAssignment_1_08594 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Fragment__AnnotationAssignment_1_18625 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentType_in_rule__Fragment__FtypeAssignment_28656 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Fragment__TypeAssignment_4_18687 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Fragment__NameAssignment_58718 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObject_in_rule__Fragment__ObjectsAssignment_78749 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObject_in_rule__Fragment__ObjectsAssignment_88780 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Object__AnnotationAssignment_1_08811 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Object__AnnotationAssignment_1_18842 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Object__NameAssignment_28873 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Object__TypeAssignment_48904 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_rule__Object__FeaturesAssignment_6_08935 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_rule__Object__FeaturesAssignment_6_2_08966 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Annotation__NameAssignment_18997 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_rule__Annotation__ParamsAssignment_2_19028 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_rule__Annotation__ParamsAssignment_2_2_19059 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__AnnotationParam__NameAssignment_09090 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParamValue_in_rule__AnnotationParam__ParamValueAssignment_19121 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__ObjectValueAnnotationParam__ObjectAssignment_19156 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__ObjectValueAnnotationParam__FeatureAssignment_2_19195 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_rule__PrimitiveValueAnnotationParam__ValueAssignment_19230 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Attribute__AnnotationAssignment_1_09261 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Attribute__AnnotationAssignment_1_19292 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Attribute__NameAssignment_39323 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_rule__Attribute__ValuesAssignment_59354 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_rule__Attribute__ValuesAssignment_6_19385 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__IntegerValue__ValueAssignment9416 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__StringValue__ValueAssignment9447 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_rule__BooleanValue__ValueAssignment9478 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Reference__AnnotationAssignment_1_09509 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Reference__AnnotationAssignment_1_19540 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEdgeProperties_in_rule__Reference__GraphicPropertiesAssignment_29571 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Reference__NameAssignment_49602 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Reference__ReferenceAssignment_69637 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Reference__ReferenceAssignment_7_19676 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__EdgeProperties__ColorAssignment_69711 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__EdgeProperties__WidthAssignment_109742 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleLineType_in_rule__EdgeProperties__LineTypeAssignment_149773 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleArrowType_in_rule__EdgeProperties__SrcArrowAssignment_189804 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleArrowType_in_rule__EdgeProperties__TgtArrowAssignment_229835 = new BitSet(new long[]{0x0000000000000002L});
    }


}