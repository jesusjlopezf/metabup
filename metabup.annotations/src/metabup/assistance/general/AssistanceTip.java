/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.general;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;

import metabup.annotations.Activator;
import metabup.annotations.IAnnotation;
import metabup.annotations.general.Annotation;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.ISolution;
import metabup.metamodel.*;
//import metabup.suggestions.extensions.ISuggestion;
import metabup.transformations.models.WMetamodelModel;

/**
 * @author Jesus
 *
 */
public abstract class AssistanceTip implements IAssistanceTip {
	//protected HashMap<String, List<AnnotatedElement>> solutions = new HashMap<String, List<AnnotatedElement>>();
	protected List<Solution> solutions = new ArrayList<Solution>();
	protected IAnnotation annotation = null;	
	protected WMetamodelModel sourceMetaModel = null;	
	protected MetaModel targetMetaModel = null; /* is it actually necessary? */
	
	protected String name = null;		

	protected List<String> errors = new ArrayList<String>();
	public HashMap<String, Object> preferences = new HashMap<String, Object>();
	
	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}
	
	public boolean run(){
		if(let(sourceMetaModel, annotation)) return calculateSolutions();		
		return false;
	}		
	
	public List<ISolution> getSolutions(){
		List<ISolution> isolutions = new ArrayList<ISolution>();
		for(Solution s : solutions) isolutions.add(s);
		return isolutions;
	}
	
	public ISolution getSolution(String description){
		if(this.solutions.isEmpty()) return null;		
		for(ISolution s : solutions) if(s.getDescription().equals(description)) return s;
		return null;
	}		
	
	protected Solution putSolution(String description, List<AnnotatedElement> elements, Annotation annotation){
		////System.out.println("putting " + description);
		if(description != null && elements != null && !elements.isEmpty() && annotation != null){
			Solution solution = new Solution(description, elements, annotation);
			solutions.add(solution);
			return solution;
		}
		
		return null;
	}
	
	public class PreferenceInitializer extends AbstractPreferenceInitializer {

		@Override
		public void initializeDefaultPreferences() {
			// TODO Auto-generated method stub
			IPreferenceStore store = Activator.getDefault().getPreferenceStore();

			Iterator it = preferences.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry)it.next();				
				if(e.getValue() instanceof String ) store.setDefault((String)e.getKey(), (String) e.getValue());
				else if(e.getValue() instanceof Boolean ) store.setDefault((String)e.getKey(), (Boolean) e.getValue());
				else if(e.getValue() instanceof List<?>){
					List<?> list = (List<?>)e.getValue();
					store.setDefault((String)e.getKey(), list.get(0).toString());
				}
			}				
		}		
	}
	
	public Object getPreferenceValue(String name){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		Object value = preferences.get(name);
		if(value == null) return null;
		if(value instanceof String) return store.getString(name);
		if(value instanceof Boolean) return store.getBoolean(name);
		if(value instanceof List<?>) return store.getString(name);
		return null;
	}
	
	protected void initializePreferences(){
		PreferenceInitializer pi = this.new PreferenceInitializer();
		pi.initializeDefaultPreferences();
	}
	
	/* Allowed preference types are: String, Boolean and List<?> */
	/* In case it is a List, the first position will be taken as defult */
	protected void addPreference(String name, Object defaultValue){
		if(defaultValue instanceof String || defaultValue instanceof Boolean || defaultValue instanceof List<?>) preferences.put(name, defaultValue);		
	}
	
	public HashMap<String, Object> getPreferences(){
		return preferences;
	}
	
	/* it annotates the specified solution's elements */
	/*public MetaModel getSolutionProposal(String description, MetaModel inMM){
		//System.out.println("gettin' in ");
		List<AnnotatedElement> solElements = this.getSolution(description);
		if(solElements == null || solElements.isEmpty()) return null;
		//System.out.println("inside");
		TreeIterator<EObject> ti = inMM.eAllContents();
		
		while(ti.hasNext()){
			EObject element = ti.next();
			
			if(element instanceof AnnotatedElement){
				AnnotatedElement ae = (AnnotatedElement) element;
				
				for(AnnotatedElement solAe : solElements){
					metabup.metaModel.Annotation an = MetaModelPackage.eINSTANCE.createAnnotation();
					an.setName(this.getAnnotation().getName());
					// parameters set missing here
					if(solAe.getId().equals(ae.getId())){
						ae.getAnnotations().add(an);					
					}
				}
			}			
		}
		
		return inMM;
	}*/
	
	@Override
	public boolean let(WMetamodelModel sourceMetaModel, IAnnotation annotation){
		if(sourceMetaModel == null) return false;	
		
		this.setAnnotation(annotation);
		this.setSourceMetaModel(sourceMetaModel);
		this.targetMetaModel = EcoreUtil.copy(this.getSourceMetaModel().rootMetamodel());
		this.setTargetMetaModel(this.getTargetMetaModel());		
		
		return true;		
	}
	
/*	public final MetaModel annotate(){		
		for(AnnotatedElement ae : elements){
			//ae.getAnnotations().add(this.getAnnotation());
		}
		
		return this.getTargetMetaModel();
	}
	*/
	public MetaModel getTargetMetaModel(){
		return targetMetaModel;
	}		
	
	/*public final void setElements(List<AnnotatedElement> elements) {
		this.elements = elements;
	}*/

	public void setSourceMetaModel(WMetamodelModel sourceMetaModel) {
		this.sourceMetaModel = sourceMetaModel;
	}

	public void setTargetMetaModel(MetaModel targetMetaModel) {
		this.targetMetaModel = targetMetaModel;
	}				
	
	public IAnnotation getAnnotation() {
		return annotation;
	}
	
	public void setAnnotation(IAnnotation annotation) {
		this.annotation = annotation;
	}

	public WMetamodelModel getSourceMetaModel() {
		return sourceMetaModel;
	}

/*	protected void setDescription() {
		this.description = "The elements: ";
		
		for(AnnotatedElement ae : this.getElements()){
			if(ae != null && ae instanceof MetaClass) this.description += ((MetaClass)ae).getName() + "(MetaClass), ";
			else if(ae != null && ae instanceof Reference) this.description += ((Reference)ae).getName() + "(Reference from " + ((MetaClass)((Reference)ae).eContainer()).getName() + "), ";
			else if(ae != null && ae instanceof Attribute) this.description += ((Attribute)ae).getName() + "(Attribute from " + ((MetaClass)((Reference)ae).eContainer()).getName() + "), ";
		}
		
		this.description = this.description.substring(0, this.description.length()-2) + "are suitable to be annotated with @" + this.annotation.getName();
	}*/
	
	protected void putError(String error){
		errors.add(error);
		MessageDialog.openError(null, "Error when applying annotation refactoring", error);
	}
	
	/*
	 * method library set
	 */
	
	/* 
	 * METACLASSES 
	 * 
	 * */
	
	protected List<MetaClass> allMetaClasses(){
		ArrayList<MetaClass> all = new ArrayList<MetaClass>();
		
		for ( MetaClass c : sourceMetaModel.allMetaClasses()) {
			all.add(c);
		}
		
		return all;
	}		
	
	protected List<MetaClass> allMetaClassesWithFeature(metabup.metamodel.Feature f) {
		ArrayList<MetaClass> all = new ArrayList<MetaClass>();
		
		for ( MetaClass c : sourceMetaModel.allMetaClasses()) {
			for (metabup.metamodel.Feature ff : c.getFeatures()) {
				if (ff.getName().equals(f.getName()) && this.isCompatibleType(f, ff)) {		// TODO: Check also the type...
					all.add(c);
					break;
				}
			}
		}
		
		return all;
	}
	
	protected List<MetaClass> allMetaClassesWithAnnotation(metabup.metamodel.Annotation a) {
		ArrayList<MetaClass> allMC = new ArrayList<MetaClass>();
		EList<AnnotationParam> annParams = a.getParams();
		
		for ( MetaClass mc : sourceMetaModel.allMetaClasses()) {			
			for (metabup.metamodel.Annotation ann : mc.getAnnotations()) {
				if (ann.getName().equals(a.getName())) {
					boolean ok = true;
					
					if(!a.getParams().isEmpty()){
						for(metabup.metamodel.AnnotationParam annParam : ann.getParams()){
							if(ok){
								for(AnnotationParam originalAnnParam : annParams){
									if(ok){
										if(!originalAnnParam.getName().equals(annParam.getName()) ||
										   !originalAnnParam.getValue().equals(annParam.getValue())){
											ok = false;
										}
									}
								}							
							}											
						}
						
						if(ok) allMC.add(mc);
					}else allMC.add(mc);
				}
			}
		}
		
		return allMC;
	}		
	
	protected boolean existsMetaClassByName(String name){
		ArrayList<MetaClass> all = new ArrayList<MetaClass>();
		
		for ( MetaClass mc : sourceMetaModel.allMetaClasses()) {
			if(mc.getName().equals(name)) return true;
		}
		
		return false;
	}
	
	protected MetaClass getMetaClassByName(String name){
		ArrayList<MetaClass> all = new ArrayList<MetaClass>();
		
		for ( MetaClass mc : sourceMetaModel.allMetaClasses()) {
			if(mc.getName().equals(name)) return mc;
		}
		
		return null;
	}
	
	protected boolean isTargetforAnyReference(MetaClass mc, MetaClass exception){
		if(mc == null) return false;
		
		for ( MetaClass srcMc : sourceMetaModel.allMetaClasses()) {
			if((exception != null && !srcMc.getId().equals(exception.getId())) || exception == null){
				for(Feature f : srcMc.getFeatures()){
					if(f instanceof Reference){
						Reference r = (Reference)f;
						if(r.getReference() != null && r.getReference().getId().equals(mc.getId())) return true;							
					}
				}
			}			
		}
		
		return false;
	}
	
	protected List<Feature> getCommonFeatures(List<MetaClass> mcs){
		if(mcs == null || mcs.size() == 0) return null;
		if(mcs.size() == 1) return null;
		
		List<Feature> ff = new ArrayList<Feature>();		 
		ff = mcs.get(0).getFeatures();
		
		List<Feature> commonff = new ArrayList<Feature>();		 
		//mcs.remove(0);
		
		if(ff != null){
			for(MetaClass mc : mcs){
				if(!mc.equals(mcs.get(0))){
					List<Feature> mcff = mc.getFeatures();
					
					for(Feature f : ff){
						boolean found = false;
						
						for(Feature mcf : mcff){
							if((f.getName().equals(mcf.getName())) && ((f instanceof Attribute && mcf instanceof Attribute) || (f instanceof Reference && mcf instanceof Reference))){
								if(f instanceof Attribute){
									if(((Attribute)f).getPrimitiveType().equals(((Attribute)mcf).getPrimitiveType())) found = true;								
								}else if(f instanceof Reference){
									if(((Reference)f).getReference().getId().equals(((Reference)mcf).getReference().getId())){										
										found = true;
										
										/* multiplicity calculation */
										
										/* min */
										int fMin = ((Reference)f).getMin();
										int mcfMin = ((Reference)f).getMin();
												
										if(fMin == 0 || mcfMin == 0) ((Reference)f).setMin(0);
										else if (fMin == 1 || mcfMin == 1) ((Reference)f).setMin(1);
										else ((Reference)f).setMin(0); // it's no sense setting min bound to "-1"
										
										/* max */
										int fMax = ((Reference)f).getMax();
										int mcfMax = ((Reference)f).getMax();
												
										if(fMax == -1 || mcfMax == -1) ((Reference)f).setMax(-1);
										else if (fMax == 1 || mcfMax == 1) ((Reference)f).setMax(1);
										else ((Reference)f).setMax(-1);
									}
								}
							}	
						}
																		
						if(found){			
							if( f instanceof Reference){
								Reference ref = (Reference) EcoreUtil.copy(f);
								ref.setId(EcoreUtil.generateUUID());
								commonff.add(ref);
							}else{
								if( f instanceof Attribute ){
									Attribute att = (Attribute) EcoreUtil.copy(f);
									att.setId(EcoreUtil.generateUUID());
									commonff.add(att);
								}
							}
							//f.setId(EcoreUtil.generateUUID());
							//commonff.add(f);
						}
					}
				}
			}
		}
		
		return commonff;
	}
	
	/*
	 * FEATURES
	 * 
	 */
	protected boolean existsFeatureInMetaClass(Feature f, MetaClass mc){
		List<Feature> mcff = mc.getFeatures();
		if(mcff.contains(f)) return true;
		else return false;
	}
	
	protected boolean isCompatibleType(metabup.metamodel.Feature f, metabup.metamodel.Feature ff) {
		if (f instanceof metabup.metamodel.Reference && ff instanceof metabup.metamodel.Reference) {
			if (((metabup.metamodel.Reference)f).getReference()!=null)	// Puede ser null si no se ha resuelto el target... no s� si habr�a que postponer los refactorings al final...
				return ((metabup.metamodel.Reference)f).getReference().equals(((metabup.metamodel.Reference)ff).getReference());
			return false;
		}
		if (f instanceof metabup.metamodel.Attribute && ff instanceof metabup.metamodel.Attribute) {
			return ((metabup.metamodel.Attribute)f).getPrimitiveType().equals(((metabup.metamodel.Attribute)ff).getPrimitiveType());
		}
		return false;
	}
	
	/*
	 * REFERENCES
	 * 
	 */
	
	protected List<Reference> allReferencesFromMetaClassWithAnnotation(metabup.metamodel.Annotation a, metabup.metamodel.MetaClass mc) {		
		EList<AnnotationParam> annParams = a.getParams();
		ArrayList<Reference> refs = new ArrayList<Reference>(); 
		
		for (Feature f : mc.getFeatures()) {
			if(f instanceof metabup.metamodel.Reference){				
				for (metabup.metamodel.Annotation ann : f.getAnnotations()) {
					if (ann.getName().equals(a.getName())) {
						boolean ok = true;
						
						if(!a.getParams().isEmpty()){
							for(metabup.metamodel.AnnotationParam annParam : ann.getParams()){
								if(ok){
									for(AnnotationParam originalAnnParam : annParams){
										if(ok){
											if(!originalAnnParam.getName().equals(annParam.getName()) ||
											   !originalAnnParam.getValue().equals(annParam.getValue())){
												ok = false;
											}
										}
									}								
								}											
							}
							
							if(ok) refs.add((Reference)f);
						}else refs.add((Reference)f);
					}
				}
			}
		}
		
		return refs;
	}
	
	protected List<Reference> allReferencesFromMetaClassWithName(String name, metabup.metamodel.MetaClass mc) {				
		ArrayList<Reference> refs = new ArrayList<Reference>(); 
		
		for (Feature f : mc.getFeatures()) {
			if(f instanceof metabup.metamodel.Reference){
				if(f.getName().equals(name)) refs.add((Reference)f);
			}
		}
		
		return refs;
	}
}
