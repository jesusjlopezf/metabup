/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.constraint;

import java.util.ArrayList;
import java.util.List;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.Object;
import metabup.annotations.extensionpoint.ConstraintAnnotation;
import metabup.annotations.extensionpoint.DesignAnnotation;
import metabup.issues.general.IOpenIssue;
import metabup.issues.lib.AcyclicReferenceCycle;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Annotation;
import metabup.metamodel.AnnotationParam;
import metabup.metamodel.ElementValue;
import metabup.metamodel.MetaClass;
import metabup.metamodel.ParamValue;
import metabup.metamodel.Reference;

public class CycleWith extends ConstraintAnnotation {
	public CycleWith (){
		this.setName("cycleWith");
		this.setDescription("The reference shall cycle with the indicated ones");
	}
	
	@Override
	public List<IOpenIssue> detectFragmentIssues(FragmentModel fm, AnnotatedElement ae){
		/*if(fm == null || ae == null || fm.getFragments() == null || fm.getFragments().isEmpty()) return null;
		List<IOpenIssue> issues = new ArrayList<IOpenIssue>();
		
		if(ae instanceof Reference){			
			Reference mmr = (Reference) ae;
			if(mmr.getReference() == null) return null;
			String name = mmr.getName();		
			
			List<metabup.fragments.Reference> references = new ArrayList<metabup.fragments.Reference>();
			boolean cyclic = false;
			
			for(Fragment f : fm.getFragments()){
				for(Object o : f.getObjects()){
					for(metabup.fragments.Reference fr : getReferences(o)){
						if(name.equals(fr.getName())){
							cyclic = isCyclic(fr, o);
							if(cyclic) references.add(fr);
						}
					}
				}
			}
			
			if(!references.isEmpty()){
				for(metabup.fragments.Reference r : references){
					AcyclicReferenceCycle arc = new AcyclicReferenceCycle(mmr, r, AcyclicReferenceCycle.DEPTH_INFINITE);
					issues.add(arc);
					return issues;
				}				
			}
		}*/
		
		return null;
	}
	
	private boolean isCyclic(fragments.Reference r, fragments.Object end) {
		/*if(r == null || end == null) return false;
		
		for(Object o : r.getReference()){			
			for(metabup.fragments.Reference or : getReferences(o)){
				if(or.getName().equals(r.getName())){
					//System.out.println(or.getName());
					//System.out.println(r.getName());
					
					for(metabup.fragments.Object oo : or.getReference()){
						if(oo.getType().equals(end.getType())) return true;
						else return isCyclic(or, end);
					}										
				}
			}
		}*/
		
		return false;
	}

	@Override
	public void updateConstraints() {
		// TODO Auto-generated method stub	
		if(!(element instanceof fragments.Reference)) return;
		List<ParamValue> params = new ArrayList<ParamValue>();
		int j = 1;
		
		Annotation ann = null;
		
		for(Annotation a : element.getAnnotations()){
			if(a.getName().toLowerCase().equals(this.getName().toLowerCase())){
				ann = a;
				break;
			}
		}
		
		ParamValue pv;
		
		while((pv = ann.getParamValueByParamName("ref" + j)) != null){
			params.add(pv);		
			j++;
		}
		
		if(params == null || params.size() < 1) return;
		
		String REFSRC = ((MetaClass)element.eContainer()).getName();
		String ELEMENT = ((Reference)element).getName();
		
		String constraint = "context " + REFSRC + " inv " + REFSRC + "_" + ELEMENT + "_cycleWith:";
		
		List<String> refs = new ArrayList<String>();
		
		for(int i = 0; i<params.size(); i++){		
			if(params.get(i) instanceof ElementValue){
				ElementValue ev = (ElementValue)params.get(i);
				
				if(ev.getValue() instanceof Reference){
					Reference r = (Reference)ev.getValue();
					MetaClass mc = (MetaClass)r.eContainer();
					refs.add(r.getName());
				}
			}
		}
		
		constraint += "\n\tself." + ELEMENT + " -> forAll(r1 |";
		
		for(int i = 0; i<refs.size()-1; i++)
			constraint += "\n\tr" + (i+1) + "." + refs.get(i) + " -> exists(r" + (i+2) + " | ";
		
		constraint += "\n\t\tr" + (refs.size()) + "." + refs.get(refs.size()-1) + " -> includes(self)";
		
		for(int i = 0; i<refs.size(); i++) constraint += ")";
		
		System.out.println(constraint);
		
		super.addConstraint("cycleWith", constraint);
	}

	/*private List<metabup.fragments.Reference> getReferences(Object o){
		List<metabup.fragments.Reference> references = new ArrayList<metabup.fragments.Reference>();
		
		for(metabup.fragments.Feature f : o.getFeatures())
			if(f instanceof metabup.fragments.Reference)
				references.add((metabup.fragments.Reference)f);
		
		return references;
	}*/
}
