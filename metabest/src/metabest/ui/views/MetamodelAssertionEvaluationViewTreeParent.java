/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.ui.views;

import java.util.ArrayList;

import org.eclipse.core.runtime.IAdaptable;

import metabup.metamodel.AnnotatedElement;

public class MetamodelAssertionEvaluationViewTreeParent extends MetamodelAssertionEvaluationViewTreeObject /*TreeParent*/ implements IAdaptable {
	protected String name;
	protected ArrayList<MetamodelAssertionEvaluationViewTreeParent> children = new ArrayList<>();
	
	public MetamodelAssertionEvaluationViewTreeParent(Object obj) {
		super(obj);
		if(obj != null)this.name = obj.toString();
	}
	
	public void addChild(MetamodelAssertionEvaluationViewTreeParent child) {
		System.out.println("adding " + child.toString() + " of type " + child.getObject().getClass());
		if(!(child.getObject() instanceof AnnotatedElement)) children.add(child);
		else{
			if(children.size() == 0) children.add(child);
			else{	
				boolean added = false;
				
				for(int i = 0; i<children.size(); i++){
					MetamodelAssertionEvaluationViewTreeObject maevto = (MetamodelAssertionEvaluationViewTreeObject) children.get(i);
					
					if(child != maevto){
						children.add(i, child);
						i = children.size();
						added = true;
					}				
				}
					
				if(!added) children.add(child);
			}		
		}
		
		child.setParent(this);
	}
	
	public MetamodelAssertionEvaluationViewTreeParent [] getChildren() {
		return (MetamodelAssertionEvaluationViewTreeParent [])children.toArray(new MetamodelAssertionEvaluationViewTreeParent[children.size()]);
	}
	
	public boolean hasChildren() {
		return children.size()>0;
	}
	
	public String getName() {
		return name;
	}		
	
	public String toString() {
		return getName();
	}
	
	public Object getAdapter(Class key) {
		return null;
	}
	
	public MetamodelAssertionEvaluationViewTreeParent getParent() {
		return (MetamodelAssertionEvaluationViewTreeParent)parent;
	}
}
