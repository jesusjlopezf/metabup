/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.pages;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

import metabest.solver.ui.wizards.dnd.DragMetaModelElementViewer;
import metabest.solver.ui.wizards.dnd.DropMetaModelElementTableTreeViewer;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaModel;


public class MetaModelElementSelectionWizardPage extends WizardPage{
	Composite containerArea;
	
	DragMetaModelElementViewer dragView;
	DropMetaModelElementTableTreeViewer dropView;
	
	private MetaModel metamodel;
	private IStructuredSelection selection;

	public MetaModelElementSelectionWizardPage(String pageName, IStructuredSelection selection, MetaModel metamodel) {
		super(pageName);
		setTitle(pageName);
		setDescription("Select the elements");
		this.metamodel = metamodel;
		this.selection = selection;
	}

	@Override
	public void createControl(Composite parent) {
		containerArea = new Composite(parent, SWT.NONE);
		FillLayout container = new FillLayout(SWT.HORIZONTAL);
		container.marginHeight = 0;
		container.marginWidth = 0;
		containerArea.setLayout(container);
		
		dragView = new DragMetaModelElementViewer(containerArea, metamodel);
		dropView = new DropMetaModelElementTableTreeViewer(containerArea, metamodel);
		dropView.refresh();
		
		containerArea.moveAbove(null);
		setControl(containerArea);
	}
	
	public Composite getContainerArea() {
		return containerArea;
	}

	public void setContainerArea(Composite containerArea) {
		this.containerArea = containerArea;
	}

	public void setDatamodel(MetaModel datamodel){
		this.metamodel = datamodel;
		dragView.refresh(metamodel);
		dropView.refresh(metamodel);
	}
	
	public List<AnnotatedElement> getSelectedElements(){
		return dropView.getSelectedElements();
	}
}
