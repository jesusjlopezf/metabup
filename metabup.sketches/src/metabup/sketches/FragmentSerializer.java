/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches;

import java.io.IOException;
import java.io.OutputStream;

import org.eclipse.emf.common.util.EList;

import fragments.Attribute;
import fragments.Feature;
import fragments.Fragment;
import fragments.FragmentModel;
import fragments.Object;
import fragments.Reference;

/**
 * This has been implemented because of my inability to make the standard 
 * xText serializer work.
 * 
 * 
 * @author jesusc
 */
public class FragmentSerializer {

	private int indent;

	public void serializeTo(OutputStream stream, FragmentModel model) throws IOException {		
		StringBuffer buffer = execute(model);
		stream.write(buffer.toString().getBytes());
	}
	
	public StringBuffer execute(FragmentModel model) {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("model " + model.getName() + "\n");
		serializerFragments(model.getFragments(), buffer);		
		
		return buffer;
	}

	private void serializerFragments(EList<Fragment> fragments, StringBuffer buffer) {
		for (Fragment fragment : fragments) {
			buffer.append("positive fragment " + fragment.getName() +  "{" + "\n");
			indent++;
			
			serializeObjects(fragment.getObjects(), buffer);
			
			indent--;
			buffer.append("}" + "\n\n");
		}
	}

	private void serializeObjects(EList<Object> objects, StringBuffer buffer) {
		for (Object object : objects) {
			
			indent(buffer); buffer.append(toId(object.getName()) + " : " + toId(object.getType()) + " {" + "\n");
			indent++;
			
			serializeFeatures(object.getFeatures(), buffer);
			
			indent--;			
			indent(buffer); buffer.append("}" + "\n\n");
		}
	}

	private void serializeFeatures(EList<Feature> features, StringBuffer buffer) {
		for (Feature feature : features) {
			if ( feature instanceof Attribute ) {
				throw new UnsupportedOperationException();
			} else {
				Reference ref = (Reference) feature;
				indent(buffer); buffer.append("ref " + ref.getName() + " = " );
				EList<Object> values = ref.getReference();
				for(int i = 0; i < values.size(); i++) {
					buffer.append(toId(values.get(i).getName()));
					if ( i + 1 < values.size() ) buffer.append(", ");
				}
				buffer.append("\n");
			}
		}
	}

	private String toId(String name) {
		boolean properId = name.matches("[a-zA-Z0-9_]+");
		if ( ! properId || name.contains(" ")) return '"' + name + '"';
		return name;
	}

	private void indent(StringBuffer buffer) {
		for(int i = 0; i < indent; i++) {
			buffer.append("\t");
		}
	}

}
