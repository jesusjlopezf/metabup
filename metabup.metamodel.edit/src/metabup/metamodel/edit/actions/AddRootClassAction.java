/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.metamodel.edit.actions;

import metabup.annotations.catalog.constraint.Root;
import metabup.annotations.catalog.design.Composition;
import metabup.extensionpoints.MetaBupMetaModelEditActionContribution;
import metabup.lexicon.wordnet.Wordnet;
import metabup.metamodel.Annotation;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.Reference;
import metabup.metamodel.impl.ReferenceImpl;

import org.apache.commons.lang.WordUtils;

import com.jesusjlopezf.utils.eclipse.emf.EMFUtils;

public class AddRootClassAction extends MetaBupMetaModelEditActionContribution {

	@Override
	public boolean checkConditions(MetaModel mm) {
		// a root-annotated class exists
		MetaClass root = null;
		
		for(MetaClass mc : mm.getClasses()){
			if(mc.isAnnotated(Root.NAME)){
				root = mc;
				break;
			}
		}				
		
		if(root == null) return true;
		
		// there's a class annotated as ROOT but...  is it really root?
		for(MetaClass mc : mm.getClasses()){
			if(!mc.isAnnotated(Root.NAME) && !mc.getIsAbstract()){
				if(mc.getIncomingRefs(true, true, ReferenceImpl.CONTAINMENT) == null) return true;
				if(mc.getIncomingRefs(true, true, ReferenceImpl.CONTAINMENT).isEmpty()) return true;
				// mmm... what if it's self-contained or in a containment cycle?
			}
		}
		
		return false;
	}

	@Override
	public MetaModel editMetaModel(MetaModel mm) {
		MetaClass root = null;
				
		for(MetaClass mc : mm.getClasses()){
			if(mc.isAnnotated(Root.NAME)){
				root = mc;
				break;
			}
		}
		
		if(root == null){
			// add it if there is no one
			root = MetamodelFactory.eINSTANCE.createMetaClass();
			root.annotate(Root.NAME);
			root.setName(WordUtils.capitalize(Root.NAME));
		}
		
		for(MetaClass tar : mm.getClasses()){
			if(tar != root){
				if(root.references(tar, 1, -1, 0, ReferenceImpl.CONTAINMENT, true, true, null)) continue;
				
				if(tar.isTop() && tar.getIncomingRefs(true, true, ReferenceImpl.CONTAINMENT).isEmpty()){
					//System.out.println(tar.getName() + " <- root" );
					Reference r = MetamodelFactory.eINSTANCE.createReference();
					r.setMin(0);
					r.setMax(-1);
					r.setName(tar.getName().toLowerCase());				
					r.annotate(Composition.NAME);				
					r.setReference(tar);
					root.getFeatures().add(r);				
				}
			}
		}
		
		// TODO - las clases ya contenidas, no necesitan volver a serlo
		
		//System.out.println(root.getReferences().size());
		mm.getClasses().add(root);
		
		return mm;
	}

}
