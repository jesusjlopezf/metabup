/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.ElementFilter;
import metamodeltest.ElementSelector;
import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Element Selector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.ElementSelectorImpl#getElementFilter <em>Element Filter</em>}</li>
 *   <li>{@link metamodeltest.impl.ElementSelectorImpl#getElementConditioner <em>Element Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ElementSelectorImpl extends SelectorImpl implements ElementSelector {
	/**
	 * The cached value of the '{@link #getElementFilter() <em>Element Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementFilter()
	 * @generated
	 * @ordered
	 */
	protected ElementFilter elementFilter;

	/**
	 * The cached value of the '{@link #getElementConditioner() <em>Element Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementConditioner()
	 * @generated
	 * @ordered
	 */
	protected ElementFilter elementConditioner;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElementSelectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.ELEMENT_SELECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementFilter getElementFilter() {
		return elementFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElementFilter(ElementFilter newElementFilter, NotificationChain msgs) {
		ElementFilter oldElementFilter = elementFilter;
		elementFilter = newElementFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_FILTER, oldElementFilter, newElementFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementFilter(ElementFilter newElementFilter) {
		if (newElementFilter != elementFilter) {
			NotificationChain msgs = null;
			if (elementFilter != null)
				msgs = ((InternalEObject)elementFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_FILTER, null, msgs);
			if (newElementFilter != null)
				msgs = ((InternalEObject)newElementFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_FILTER, null, msgs);
			msgs = basicSetElementFilter(newElementFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_FILTER, newElementFilter, newElementFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ElementFilter getElementConditioner() {
		return elementConditioner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElementConditioner(ElementFilter newElementConditioner, NotificationChain msgs) {
		ElementFilter oldElementConditioner = elementConditioner;
		elementConditioner = newElementConditioner;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_CONDITIONER, oldElementConditioner, newElementConditioner);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementConditioner(ElementFilter newElementConditioner) {
		if (newElementConditioner != elementConditioner) {
			NotificationChain msgs = null;
			if (elementConditioner != null)
				msgs = ((InternalEObject)elementConditioner).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_CONDITIONER, null, msgs);
			if (newElementConditioner != null)
				msgs = ((InternalEObject)newElementConditioner).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_CONDITIONER, null, msgs);
			msgs = basicSetElementConditioner(newElementConditioner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_CONDITIONER, newElementConditioner, newElementConditioner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_FILTER:
				return basicSetElementFilter(null, msgs);
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_CONDITIONER:
				return basicSetElementConditioner(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_FILTER:
				return getElementFilter();
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_CONDITIONER:
				return getElementConditioner();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_FILTER:
				setElementFilter((ElementFilter)newValue);
				return;
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_CONDITIONER:
				setElementConditioner((ElementFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_FILTER:
				setElementFilter((ElementFilter)null);
				return;
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_CONDITIONER:
				setElementConditioner((ElementFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_FILTER:
				return elementFilter != null;
			case MetamodeltestPackage.ELEMENT_SELECTOR__ELEMENT_CONDITIONER:
				return elementConditioner != null;
		}
		return super.eIsSet(featureID);
	}

} //ElementSelectorImpl
