/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.util;

import metamodeltest.Abstract;
import metamodeltest.Abstraction;
import metamodeltest.Adjective;
import metamodeltest.AndQualifier;
import metamodeltest.Annotated;
import metamodeltest.AnnotatedElement;
import metamodeltest.Annotation;
import metamodeltest.Assertion;
import metamodeltest.AssertionCall;
import metamodeltest.AssertionDefinition;
import metamodeltest.AssertionLibrary;
import metamodeltest.AssertionTest;
import metamodeltest.AttributeContainment;
import metamodeltest.AttributeFilter;
import metamodeltest.AttributeQualifier;
import metamodeltest.AttributeSelector;
import metamodeltest.BooleanParameter;
import metamodeltest.ClassCollects;
import metamodeltest.ClassContainee;
import metamodeltest.ClassContainment;
import metamodeltest.ClassFilter;
import metamodeltest.ClassQualifier;
import metamodeltest.ClassReachedBy;
import metamodeltest.ClassReaches;
import metamodeltest.ClassRelation;
import metamodeltest.ClassSelector;
import metamodeltest.CompositionLeaf;
import metamodeltest.CompoundPathQualifier;
import metamodeltest.CompoundQualifier;
import metamodeltest.ContLeaf;
import metamodeltest.ContRoot;
import metamodeltest.ContainmentPath;
import metamodeltest.ContainmentReference;
import metamodeltest.Contains;
import metamodeltest.CyclicPath;
import metamodeltest.DescribedElement;
import metamodeltest.ElementAssertion;
import metamodeltest.ElementFeaturement;
import metamodeltest.ElementFilter;
import metamodeltest.ElementQualifier;
import metamodeltest.ElementRelation;
import metamodeltest.ElementSelector;
import metamodeltest.Every;
import metamodeltest.Existance;
import metamodeltest.FeatureContainment;
import metamodeltest.FeatureFilter;
import metamodeltest.FeatureMultiplicity;
import metamodeltest.FeatureQualifier;
import metamodeltest.FeatureSelector;
import metamodeltest.Filter;
import metamodeltest.FixedBound;
import metamodeltest.FromTo;
import metamodeltest.Import;
import metamodeltest.Infinite;
import metamodeltest.InhLeaf;
import metamodeltest.InhRoot;
import metamodeltest.Inheritance;
import metamodeltest.InheritedFeature;
import metamodeltest.InheritedFeatureContainment;
import metamodeltest.IntegerParameter;
import metamodeltest.IsSubTo;
import metamodeltest.IsSuperTo;
import metamodeltest.Length;
import metamodeltest.LessThan;
import metamodeltest.LibraryAssertionCall;
import metamodeltest.Literal;
import metamodeltest.LowerCase;
import metamodeltest.MaxMultiplicity;
import metamodeltest.MetaModelTest;
import metamodeltest.MetamodeltestPackage;
import metamodeltest.MoreThan;
import metamodeltest.Name;
import metamodeltest.NamePart;
import metamodeltest.Named;
import metamodeltest.NamedAfter;
import metamodeltest.NamedElement;
import metamodeltest.None;
import metamodeltest.Noun;
import metamodeltest.OCLExpression;
import metamodeltest.OppositeTo;
import metamodeltest.OrQualifier;
import metamodeltest.Parameter;
import metamodeltest.ParameterElementSetValue;
import metamodeltest.ParameterStringValue;
import metamodeltest.ParameterValue;
import metamodeltest.PathAnd;
import metamodeltest.PathFilter;
import metamodeltest.PathOr;
import metamodeltest.PathQualifier;
import metamodeltest.PathSelector;
import metamodeltest.PathSide;
import metamodeltest.PathSource;
import metamodeltest.PathTarget;
import metamodeltest.PhraseNamed;
import metamodeltest.Prefix;
import metamodeltest.Qualifier;
import metamodeltest.Quantifier;
import metamodeltest.ReferenceContainment;
import metamodeltest.ReferenceFilter;
import metamodeltest.ReferenceQualifier;
import metamodeltest.ReferenceSelector;
import metamodeltest.Selector;
import metamodeltest.SelfInheritance;
import metamodeltest.SideClass;
import metamodeltest.Some;
import metamodeltest.SourceClass;
import metamodeltest.StepClass;
import metamodeltest.StrictNumber;
import metamodeltest.StringParameter;
import metamodeltest.Substring;
import metamodeltest.Suffix;
import metamodeltest.Synonym;
import metamodeltest.TargetClass;
import metamodeltest.TrivialString;
import metamodeltest.Typed;
import metamodeltest.UpperCase;
import metamodeltest.UserDefinedParameter;
import metamodeltest.Verb;
import metamodeltest.WithInheritancePath;
import metamodeltest.WordNature;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see metamodeltest.MetamodeltestPackage
 * @generated
 */
public class MetamodeltestAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MetamodeltestPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetamodeltestAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MetamodeltestPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetamodeltestSwitch<Adapter> modelSwitch =
		new MetamodeltestSwitch<Adapter>() {
			@Override
			public Adapter caseMetaModelTest(MetaModelTest object) {
				return createMetaModelTestAdapter();
			}
			@Override
			public Adapter caseAssertion(Assertion object) {
				return createAssertionAdapter();
			}
			@Override
			public Adapter caseQuantifier(Quantifier object) {
				return createQuantifierAdapter();
			}
			@Override
			public Adapter caseNone(None object) {
				return createNoneAdapter();
			}
			@Override
			public Adapter caseNumber(metamodeltest.Number object) {
				return createNumberAdapter();
			}
			@Override
			public Adapter caseEvery(Every object) {
				return createEveryAdapter();
			}
			@Override
			public Adapter caseLessThan(LessThan object) {
				return createLessThanAdapter();
			}
			@Override
			public Adapter caseMoreThan(MoreThan object) {
				return createMoreThanAdapter();
			}
			@Override
			public Adapter caseElementAssertion(ElementAssertion object) {
				return createElementAssertionAdapter();
			}
			@Override
			public Adapter caseName(Name object) {
				return createNameAdapter();
			}
			@Override
			public Adapter caseAbstract(Abstract object) {
				return createAbstractAdapter();
			}
			@Override
			public Adapter caseFilter(Filter object) {
				return createFilterAdapter();
			}
			@Override
			public Adapter caseElementFilter(ElementFilter object) {
				return createElementFilterAdapter();
			}
			@Override
			public Adapter caseClassFilter(ClassFilter object) {
				return createClassFilterAdapter();
			}
			@Override
			public Adapter caseFeatureFilter(FeatureFilter object) {
				return createFeatureFilterAdapter();
			}
			@Override
			public Adapter caseAttributeFilter(AttributeFilter object) {
				return createAttributeFilterAdapter();
			}
			@Override
			public Adapter caseReferenceFilter(ReferenceFilter object) {
				return createReferenceFilterAdapter();
			}
			@Override
			public Adapter caseElementQualifier(ElementQualifier object) {
				return createElementQualifierAdapter();
			}
			@Override
			public Adapter caseQualifier(Qualifier object) {
				return createQualifierAdapter();
			}
			@Override
			public Adapter caseNamed(Named object) {
				return createNamedAdapter();
			}
			@Override
			public Adapter caseClassQualifier(ClassQualifier object) {
				return createClassQualifierAdapter();
			}
			@Override
			public Adapter caseFeatureQualifier(FeatureQualifier object) {
				return createFeatureQualifierAdapter();
			}
			@Override
			public Adapter caseAttributeQualifier(AttributeQualifier object) {
				return createAttributeQualifierAdapter();
			}
			@Override
			public Adapter caseReferenceQualifier(ReferenceQualifier object) {
				return createReferenceQualifierAdapter();
			}
			@Override
			public Adapter caseAbstraction(Abstraction object) {
				return createAbstractionAdapter();
			}
			@Override
			public Adapter caseIsSuperTo(IsSuperTo object) {
				return createIsSuperToAdapter();
			}
			@Override
			public Adapter caseIsSubTo(IsSubTo object) {
				return createIsSubToAdapter();
			}
			@Override
			public Adapter caseClassRelation(ClassRelation object) {
				return createClassRelationAdapter();
			}
			@Override
			public Adapter caseFeatureContainment(FeatureContainment object) {
				return createFeatureContainmentAdapter();
			}
			@Override
			public Adapter caseInheritedFeatureContainment(InheritedFeatureContainment object) {
				return createInheritedFeatureContainmentAdapter();
			}
			@Override
			public Adapter caseInhLeaf(InhLeaf object) {
				return createInhLeafAdapter();
			}
			@Override
			public Adapter caseInhRoot(InhRoot object) {
				return createInhRootAdapter();
			}
			@Override
			public Adapter caseCompositionLeaf(CompositionLeaf object) {
				return createCompositionLeafAdapter();
			}
			@Override
			public Adapter caseMaxMultiplicity(MaxMultiplicity object) {
				return createMaxMultiplicityAdapter();
			}
			@Override
			public Adapter caseFixedBound(FixedBound object) {
				return createFixedBoundAdapter();
			}
			@Override
			public Adapter caseInfinite(Infinite object) {
				return createInfiniteAdapter();
			}
			@Override
			public Adapter caseClassContainment(ClassContainment object) {
				return createClassContainmentAdapter();
			}
			@Override
			public Adapter caseClassContainee(ClassContainee object) {
				return createClassContaineeAdapter();
			}
			@Override
			public Adapter caseTyped(Typed object) {
				return createTypedAdapter();
			}
			@Override
			public Adapter casePrefix(Prefix object) {
				return createPrefixAdapter();
			}
			@Override
			public Adapter caseSuffix(Suffix object) {
				return createSuffixAdapter();
			}
			@Override
			public Adapter caseContains(Contains object) {
				return createContainsAdapter();
			}
			@Override
			public Adapter caseSideClass(SideClass object) {
				return createSideClassAdapter();
			}
			@Override
			public Adapter caseFromTo(FromTo object) {
				return createFromToAdapter();
			}
			@Override
			public Adapter caseStrictNumber(StrictNumber object) {
				return createStrictNumberAdapter();
			}
			@Override
			public Adapter caseInheritance(Inheritance object) {
				return createInheritanceAdapter();
			}
			@Override
			public Adapter caseFeatureMultiplicity(FeatureMultiplicity object) {
				return createFeatureMultiplicityAdapter();
			}
			@Override
			public Adapter caseUpperCase(UpperCase object) {
				return createUpperCaseAdapter();
			}
			@Override
			public Adapter caseLowerCase(LowerCase object) {
				return createLowerCaseAdapter();
			}
			@Override
			public Adapter caseSubstring(Substring object) {
				return createSubstringAdapter();
			}
			@Override
			public Adapter caseAssertionLibrary(AssertionLibrary object) {
				return createAssertionLibraryAdapter();
			}
			@Override
			public Adapter caseAssertionTest(AssertionTest object) {
				return createAssertionTestAdapter();
			}
			@Override
			public Adapter caseAssertionDefinition(AssertionDefinition object) {
				return createAssertionDefinitionAdapter();
			}
			@Override
			public Adapter caseLibraryAssertionCall(LibraryAssertionCall object) {
				return createLibraryAssertionCallAdapter();
			}
			@Override
			public Adapter caseAssertionCall(AssertionCall object) {
				return createAssertionCallAdapter();
			}
			@Override
			public Adapter caseParameter(Parameter object) {
				return createParameterAdapter();
			}
			@Override
			public Adapter caseBooleanParameter(BooleanParameter object) {
				return createBooleanParameterAdapter();
			}
			@Override
			public Adapter caseIntegerParameter(IntegerParameter object) {
				return createIntegerParameterAdapter();
			}
			@Override
			public Adapter caseStringParameter(StringParameter object) {
				return createStringParameterAdapter();
			}
			@Override
			public Adapter caseUserDefinedParameter(UserDefinedParameter object) {
				return createUserDefinedParameterAdapter();
			}
			@Override
			public Adapter casePathFilter(PathFilter object) {
				return createPathFilterAdapter();
			}
			@Override
			public Adapter caseLength(Length object) {
				return createLengthAdapter();
			}
			@Override
			public Adapter caseSourceClass(SourceClass object) {
				return createSourceClassAdapter();
			}
			@Override
			public Adapter caseTargetClass(TargetClass object) {
				return createTargetClassAdapter();
			}
			@Override
			public Adapter caseContainmentReference(ContainmentReference object) {
				return createContainmentReferenceAdapter();
			}
			@Override
			public Adapter casePathQualifier(PathQualifier object) {
				return createPathQualifierAdapter();
			}
			@Override
			public Adapter casePathSide(PathSide object) {
				return createPathSideAdapter();
			}
			@Override
			public Adapter casePathTarget(PathTarget object) {
				return createPathTargetAdapter();
			}
			@Override
			public Adapter casePathSource(PathSource object) {
				return createPathSourceAdapter();
			}
			@Override
			public Adapter caseElementFeaturement(ElementFeaturement object) {
				return createElementFeaturementAdapter();
			}
			@Override
			public Adapter caseStepClass(StepClass object) {
				return createStepClassAdapter();
			}
			@Override
			public Adapter caseExistance(Existance object) {
				return createExistanceAdapter();
			}
			@Override
			public Adapter caseSelector(Selector object) {
				return createSelectorAdapter();
			}
			@Override
			public Adapter caseElementSelector(ElementSelector object) {
				return createElementSelectorAdapter();
			}
			@Override
			public Adapter caseClassSelector(ClassSelector object) {
				return createClassSelectorAdapter();
			}
			@Override
			public Adapter caseFeatureSelector(FeatureSelector object) {
				return createFeatureSelectorAdapter();
			}
			@Override
			public Adapter caseAttributeSelector(AttributeSelector object) {
				return createAttributeSelectorAdapter();
			}
			@Override
			public Adapter caseReferenceSelector(ReferenceSelector object) {
				return createReferenceSelectorAdapter();
			}
			@Override
			public Adapter casePathSelector(PathSelector object) {
				return createPathSelectorAdapter();
			}
			@Override
			public Adapter caseAndQualifier(AndQualifier object) {
				return createAndQualifierAdapter();
			}
			@Override
			public Adapter caseOrQualifier(OrQualifier object) {
				return createOrQualifierAdapter();
			}
			@Override
			public Adapter caseCompoundQualifier(CompoundQualifier object) {
				return createCompoundQualifierAdapter();
			}
			@Override
			public Adapter caseCompoundPathQualifier(CompoundPathQualifier object) {
				return createCompoundPathQualifierAdapter();
			}
			@Override
			public Adapter casePathAnd(PathAnd object) {
				return createPathAndAdapter();
			}
			@Override
			public Adapter casePathOr(PathOr object) {
				return createPathOrAdapter();
			}
			@Override
			public Adapter caseAttributeContainment(AttributeContainment object) {
				return createAttributeContainmentAdapter();
			}
			@Override
			public Adapter caseReferenceContainment(ReferenceContainment object) {
				return createReferenceContainmentAdapter();
			}
			@Override
			public Adapter caseContRoot(ContRoot object) {
				return createContRootAdapter();
			}
			@Override
			public Adapter caseContLeaf(ContLeaf object) {
				return createContLeafAdapter();
			}
			@Override
			public Adapter caseElementRelation(ElementRelation object) {
				return createElementRelationAdapter();
			}
			@Override
			public Adapter caseClassReaches(ClassReaches object) {
				return createClassReachesAdapter();
			}
			@Override
			public Adapter caseClassReachedBy(ClassReachedBy object) {
				return createClassReachedByAdapter();
			}
			@Override
			public Adapter caseContainmentPath(ContainmentPath object) {
				return createContainmentPathAdapter();
			}
			@Override
			public Adapter caseWithInheritancePath(WithInheritancePath object) {
				return createWithInheritancePathAdapter();
			}
			@Override
			public Adapter caseCyclicPath(CyclicPath object) {
				return createCyclicPathAdapter();
			}
			@Override
			public Adapter caseSome(Some object) {
				return createSomeAdapter();
			}
			@Override
			public Adapter caseInheritedFeature(InheritedFeature object) {
				return createInheritedFeatureAdapter();
			}
			@Override
			public Adapter caseClassCollects(ClassCollects object) {
				return createClassCollectsAdapter();
			}
			@Override
			public Adapter caseAnnotated(Annotated object) {
				return createAnnotatedAdapter();
			}
			@Override
			public Adapter caseImport(Import object) {
				return createImportAdapter();
			}
			@Override
			public Adapter caseParameterValue(ParameterValue object) {
				return createParameterValueAdapter();
			}
			@Override
			public Adapter caseSelfInheritance(SelfInheritance object) {
				return createSelfInheritanceAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseAnnotation(Annotation object) {
				return createAnnotationAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseDescribedElement(DescribedElement object) {
				return createDescribedElementAdapter();
			}
			@Override
			public Adapter caseSynonym(Synonym object) {
				return createSynonymAdapter();
			}
			@Override
			public Adapter caseVerb(Verb object) {
				return createVerbAdapter();
			}
			@Override
			public Adapter caseNoun(Noun object) {
				return createNounAdapter();
			}
			@Override
			public Adapter caseAdjective(Adjective object) {
				return createAdjectiveAdapter();
			}
			@Override
			public Adapter caseWordNature(WordNature object) {
				return createWordNatureAdapter();
			}
			@Override
			public Adapter casePhraseNamed(PhraseNamed object) {
				return createPhraseNamedAdapter();
			}
			@Override
			public Adapter caseNamePart(NamePart object) {
				return createNamePartAdapter();
			}
			@Override
			public Adapter caseLiteral(Literal object) {
				return createLiteralAdapter();
			}
			@Override
			public Adapter caseTrivialString(TrivialString object) {
				return createTrivialStringAdapter();
			}
			@Override
			public Adapter caseOppositeTo(OppositeTo object) {
				return createOppositeToAdapter();
			}
			@Override
			public Adapter caseParameterStringValue(ParameterStringValue object) {
				return createParameterStringValueAdapter();
			}
			@Override
			public Adapter caseParameterElementSetValue(ParameterElementSetValue object) {
				return createParameterElementSetValueAdapter();
			}
			@Override
			public Adapter caseNamedAfter(NamedAfter object) {
				return createNamedAfterAdapter();
			}
			@Override
			public Adapter caseOCLExpression(OCLExpression object) {
				return createOCLExpressionAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.MetaModelTest <em>Meta Model Test</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.MetaModelTest
	 * @generated
	 */
	public Adapter createMetaModelTestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Assertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Assertion
	 * @generated
	 */
	public Adapter createAssertionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Quantifier <em>Quantifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Quantifier
	 * @generated
	 */
	public Adapter createQuantifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.None <em>None</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.None
	 * @generated
	 */
	public Adapter createNoneAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Number <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Number
	 * @generated
	 */
	public Adapter createNumberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Every <em>Every</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Every
	 * @generated
	 */
	public Adapter createEveryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.LessThan <em>Less Than</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.LessThan
	 * @generated
	 */
	public Adapter createLessThanAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.MoreThan <em>More Than</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.MoreThan
	 * @generated
	 */
	public Adapter createMoreThanAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ElementAssertion <em>Element Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ElementAssertion
	 * @generated
	 */
	public Adapter createElementAssertionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Name <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Name
	 * @generated
	 */
	public Adapter createNameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Abstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Abstract
	 * @generated
	 */
	public Adapter createAbstractAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Filter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Filter
	 * @generated
	 */
	public Adapter createFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ElementFilter <em>Element Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ElementFilter
	 * @generated
	 */
	public Adapter createElementFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ClassFilter <em>Class Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ClassFilter
	 * @generated
	 */
	public Adapter createClassFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.FeatureFilter <em>Feature Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.FeatureFilter
	 * @generated
	 */
	public Adapter createFeatureFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AttributeFilter <em>Attribute Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AttributeFilter
	 * @generated
	 */
	public Adapter createAttributeFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ReferenceFilter <em>Reference Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ReferenceFilter
	 * @generated
	 */
	public Adapter createReferenceFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ElementQualifier <em>Element Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ElementQualifier
	 * @generated
	 */
	public Adapter createElementQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Qualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Qualifier
	 * @generated
	 */
	public Adapter createQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Named <em>Named</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Named
	 * @generated
	 */
	public Adapter createNamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ClassQualifier <em>Class Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ClassQualifier
	 * @generated
	 */
	public Adapter createClassQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.FeatureQualifier <em>Feature Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.FeatureQualifier
	 * @generated
	 */
	public Adapter createFeatureQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AttributeQualifier <em>Attribute Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AttributeQualifier
	 * @generated
	 */
	public Adapter createAttributeQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ReferenceQualifier <em>Reference Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ReferenceQualifier
	 * @generated
	 */
	public Adapter createReferenceQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Abstraction <em>Abstraction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Abstraction
	 * @generated
	 */
	public Adapter createAbstractionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.IsSuperTo <em>Is Super To</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.IsSuperTo
	 * @generated
	 */
	public Adapter createIsSuperToAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.IsSubTo <em>Is Sub To</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.IsSubTo
	 * @generated
	 */
	public Adapter createIsSubToAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ClassRelation <em>Class Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ClassRelation
	 * @generated
	 */
	public Adapter createClassRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.FeatureContainment <em>Feature Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.FeatureContainment
	 * @generated
	 */
	public Adapter createFeatureContainmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.InheritedFeatureContainment <em>Inherited Feature Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.InheritedFeatureContainment
	 * @generated
	 */
	public Adapter createInheritedFeatureContainmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.InhLeaf <em>Inh Leaf</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.InhLeaf
	 * @generated
	 */
	public Adapter createInhLeafAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.InhRoot <em>Inh Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.InhRoot
	 * @generated
	 */
	public Adapter createInhRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.CompositionLeaf <em>Composition Leaf</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.CompositionLeaf
	 * @generated
	 */
	public Adapter createCompositionLeafAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.MaxMultiplicity <em>Max Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.MaxMultiplicity
	 * @generated
	 */
	public Adapter createMaxMultiplicityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.FixedBound <em>Fixed Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.FixedBound
	 * @generated
	 */
	public Adapter createFixedBoundAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Infinite <em>Infinite</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Infinite
	 * @generated
	 */
	public Adapter createInfiniteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ClassContainment <em>Class Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ClassContainment
	 * @generated
	 */
	public Adapter createClassContainmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ClassContainee <em>Class Containee</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ClassContainee
	 * @generated
	 */
	public Adapter createClassContaineeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Typed <em>Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Typed
	 * @generated
	 */
	public Adapter createTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Prefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Prefix
	 * @generated
	 */
	public Adapter createPrefixAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Suffix <em>Suffix</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Suffix
	 * @generated
	 */
	public Adapter createSuffixAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Contains <em>Contains</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Contains
	 * @generated
	 */
	public Adapter createContainsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.SideClass <em>Side Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.SideClass
	 * @generated
	 */
	public Adapter createSideClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.FromTo <em>From To</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.FromTo
	 * @generated
	 */
	public Adapter createFromToAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.StrictNumber <em>Strict Number</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.StrictNumber
	 * @generated
	 */
	public Adapter createStrictNumberAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Inheritance <em>Inheritance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Inheritance
	 * @generated
	 */
	public Adapter createInheritanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.FeatureMultiplicity <em>Feature Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.FeatureMultiplicity
	 * @generated
	 */
	public Adapter createFeatureMultiplicityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.UpperCase <em>Upper Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.UpperCase
	 * @generated
	 */
	public Adapter createUpperCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.LowerCase <em>Lower Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.LowerCase
	 * @generated
	 */
	public Adapter createLowerCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Substring <em>Substring</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Substring
	 * @generated
	 */
	public Adapter createSubstringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AssertionLibrary <em>Assertion Library</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AssertionLibrary
	 * @generated
	 */
	public Adapter createAssertionLibraryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AssertionTest <em>Assertion Test</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AssertionTest
	 * @generated
	 */
	public Adapter createAssertionTestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AssertionDefinition <em>Assertion Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AssertionDefinition
	 * @generated
	 */
	public Adapter createAssertionDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.LibraryAssertionCall <em>Library Assertion Call</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.LibraryAssertionCall
	 * @generated
	 */
	public Adapter createLibraryAssertionCallAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AssertionCall <em>Assertion Call</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AssertionCall
	 * @generated
	 */
	public Adapter createAssertionCallAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Parameter
	 * @generated
	 */
	public Adapter createParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.BooleanParameter <em>Boolean Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.BooleanParameter
	 * @generated
	 */
	public Adapter createBooleanParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.IntegerParameter <em>Integer Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.IntegerParameter
	 * @generated
	 */
	public Adapter createIntegerParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.StringParameter <em>String Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.StringParameter
	 * @generated
	 */
	public Adapter createStringParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.UserDefinedParameter <em>User Defined Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.UserDefinedParameter
	 * @generated
	 */
	public Adapter createUserDefinedParameterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.PathFilter <em>Path Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.PathFilter
	 * @generated
	 */
	public Adapter createPathFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Length <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Length
	 * @generated
	 */
	public Adapter createLengthAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.SourceClass <em>Source Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.SourceClass
	 * @generated
	 */
	public Adapter createSourceClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.TargetClass <em>Target Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.TargetClass
	 * @generated
	 */
	public Adapter createTargetClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ContainmentReference <em>Containment Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ContainmentReference
	 * @generated
	 */
	public Adapter createContainmentReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.PathQualifier <em>Path Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.PathQualifier
	 * @generated
	 */
	public Adapter createPathQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.PathSide <em>Path Side</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.PathSide
	 * @generated
	 */
	public Adapter createPathSideAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.PathTarget <em>Path Target</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.PathTarget
	 * @generated
	 */
	public Adapter createPathTargetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.PathSource <em>Path Source</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.PathSource
	 * @generated
	 */
	public Adapter createPathSourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ElementFeaturement <em>Element Featurement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ElementFeaturement
	 * @generated
	 */
	public Adapter createElementFeaturementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.StepClass <em>Step Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.StepClass
	 * @generated
	 */
	public Adapter createStepClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Existance <em>Existance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Existance
	 * @generated
	 */
	public Adapter createExistanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Selector <em>Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Selector
	 * @generated
	 */
	public Adapter createSelectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ElementSelector <em>Element Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ElementSelector
	 * @generated
	 */
	public Adapter createElementSelectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ClassSelector <em>Class Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ClassSelector
	 * @generated
	 */
	public Adapter createClassSelectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.FeatureSelector <em>Feature Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.FeatureSelector
	 * @generated
	 */
	public Adapter createFeatureSelectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AttributeSelector <em>Attribute Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AttributeSelector
	 * @generated
	 */
	public Adapter createAttributeSelectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ReferenceSelector <em>Reference Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ReferenceSelector
	 * @generated
	 */
	public Adapter createReferenceSelectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.PathSelector <em>Path Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.PathSelector
	 * @generated
	 */
	public Adapter createPathSelectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AndQualifier <em>And Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AndQualifier
	 * @generated
	 */
	public Adapter createAndQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.OrQualifier <em>Or Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.OrQualifier
	 * @generated
	 */
	public Adapter createOrQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.CompoundQualifier <em>Compound Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.CompoundQualifier
	 * @generated
	 */
	public Adapter createCompoundQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.CompoundPathQualifier <em>Compound Path Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.CompoundPathQualifier
	 * @generated
	 */
	public Adapter createCompoundPathQualifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.PathAnd <em>Path And</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.PathAnd
	 * @generated
	 */
	public Adapter createPathAndAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.PathOr <em>Path Or</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.PathOr
	 * @generated
	 */
	public Adapter createPathOrAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AttributeContainment <em>Attribute Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AttributeContainment
	 * @generated
	 */
	public Adapter createAttributeContainmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ReferenceContainment <em>Reference Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ReferenceContainment
	 * @generated
	 */
	public Adapter createReferenceContainmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ContRoot <em>Cont Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ContRoot
	 * @generated
	 */
	public Adapter createContRootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ContLeaf <em>Cont Leaf</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ContLeaf
	 * @generated
	 */
	public Adapter createContLeafAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ElementRelation <em>Element Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ElementRelation
	 * @generated
	 */
	public Adapter createElementRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ClassReaches <em>Class Reaches</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ClassReaches
	 * @generated
	 */
	public Adapter createClassReachesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ClassReachedBy <em>Class Reached By</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ClassReachedBy
	 * @generated
	 */
	public Adapter createClassReachedByAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ContainmentPath <em>Containment Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ContainmentPath
	 * @generated
	 */
	public Adapter createContainmentPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.WithInheritancePath <em>With Inheritance Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.WithInheritancePath
	 * @generated
	 */
	public Adapter createWithInheritancePathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.CyclicPath <em>Cyclic Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.CyclicPath
	 * @generated
	 */
	public Adapter createCyclicPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Some <em>Some</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Some
	 * @generated
	 */
	public Adapter createSomeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.InheritedFeature <em>Inherited Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.InheritedFeature
	 * @generated
	 */
	public Adapter createInheritedFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ClassCollects <em>Class Collects</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ClassCollects
	 * @generated
	 */
	public Adapter createClassCollectsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Annotated <em>Annotated</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Annotated
	 * @generated
	 */
	public Adapter createAnnotatedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Import
	 * @generated
	 */
	public Adapter createImportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ParameterValue <em>Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ParameterValue
	 * @generated
	 */
	public Adapter createParameterValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.SelfInheritance <em>Self Inheritance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.SelfInheritance
	 * @generated
	 */
	public Adapter createSelfInheritanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Annotation
	 * @generated
	 */
	public Adapter createAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.DescribedElement <em>Described Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.DescribedElement
	 * @generated
	 */
	public Adapter createDescribedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Synonym <em>Synonym</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Synonym
	 * @generated
	 */
	public Adapter createSynonymAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Verb <em>Verb</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Verb
	 * @generated
	 */
	public Adapter createVerbAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Noun <em>Noun</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Noun
	 * @generated
	 */
	public Adapter createNounAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Adjective <em>Adjective</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Adjective
	 * @generated
	 */
	public Adapter createAdjectiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.WordNature <em>Word Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.WordNature
	 * @generated
	 */
	public Adapter createWordNatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.PhraseNamed <em>Phrase Named</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.PhraseNamed
	 * @generated
	 */
	public Adapter createPhraseNamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.NamePart <em>Name Part</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.NamePart
	 * @generated
	 */
	public Adapter createNamePartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.Literal <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.Literal
	 * @generated
	 */
	public Adapter createLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.TrivialString <em>Trivial String</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.TrivialString
	 * @generated
	 */
	public Adapter createTrivialStringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.OppositeTo <em>Opposite To</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.OppositeTo
	 * @generated
	 */
	public Adapter createOppositeToAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ParameterStringValue <em>Parameter String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ParameterStringValue
	 * @generated
	 */
	public Adapter createParameterStringValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.ParameterElementSetValue <em>Parameter Element Set Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.ParameterElementSetValue
	 * @generated
	 */
	public Adapter createParameterElementSetValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.NamedAfter <em>Named After</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.NamedAfter
	 * @generated
	 */
	public Adapter createNamedAfterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link metamodeltest.OCLExpression <em>OCL Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see metamodeltest.OCLExpression
	 * @generated
	 */
	public Adapter createOCLExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //MetamodeltestAdapterFactory
