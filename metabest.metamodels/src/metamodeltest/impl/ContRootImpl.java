/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.BooleanParameter;
import metamodeltest.ContRoot;
import metamodeltest.MetamodeltestPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cont Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.ContRootImpl#getAbsolute <em>Absolute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ContRootImpl extends ClassQualifierImpl implements ContRoot {
	/**
	 * The cached value of the '{@link #getAbsolute() <em>Absolute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbsolute()
	 * @generated
	 * @ordered
	 */
	protected BooleanParameter absolute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContRootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.CONT_ROOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameter getAbsolute() {
		return absolute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbsolute(BooleanParameter newAbsolute, NotificationChain msgs) {
		BooleanParameter oldAbsolute = absolute;
		absolute = newAbsolute;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CONT_ROOT__ABSOLUTE, oldAbsolute, newAbsolute);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbsolute(BooleanParameter newAbsolute) {
		if (newAbsolute != absolute) {
			NotificationChain msgs = null;
			if (absolute != null)
				msgs = ((InternalEObject)absolute).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CONT_ROOT__ABSOLUTE, null, msgs);
			if (newAbsolute != null)
				msgs = ((InternalEObject)newAbsolute).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CONT_ROOT__ABSOLUTE, null, msgs);
			msgs = basicSetAbsolute(newAbsolute, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CONT_ROOT__ABSOLUTE, newAbsolute, newAbsolute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.CONT_ROOT__ABSOLUTE:
				return basicSetAbsolute(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.CONT_ROOT__ABSOLUTE:
				return getAbsolute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.CONT_ROOT__ABSOLUTE:
				setAbsolute((BooleanParameter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CONT_ROOT__ABSOLUTE:
				setAbsolute((BooleanParameter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CONT_ROOT__ABSOLUTE:
				return absolute != null;
		}
		return super.eIsSet(featureID);
	}

} //ContRootImpl
