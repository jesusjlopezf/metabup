package metabup.fragments.export.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;

public class EMFHandler {
	private String id = "";
	private List<EPackage> packages;
	private Resource resource;
	
	private HashSet<EObject> allCreatedObjectsIntoResource = new HashSet<EObject>();

	public EMFHandler(List<EPackage> packages, Resource model) {		
		this.packages = packages;
		this.resource = model;
	}
	
	public boolean contains(EObject obj) {
		if ( obj.eResource() != resource) {
			// this is to allow queries on target models
			return allCreatedObjectsIntoResource.contains(obj);
		}
		return true; 
	}

	public Resource getResource() {
		return resource;
	}

	public String getId() {
		return id;
	}

	public EClass findMetaclass(String metaclass) {
		for (EPackage pkg : packages) {
			EClass result = findMetaclassInSubPackages(pkg, metaclass);
			if (result != null)
				return result;
		}
		throw new RuntimeException("Metaclass " + metaclass
				+ " not found in namespace " + id);
	}

	protected EClass findMetaclassInSubPackages(EPackage pkg, String metaclass) {
		if (pkg.getEClassifier(metaclass) != null) {
			return (EClass) pkg.getEClassifier(metaclass);
		}
		for (EPackage subPkg : pkg.getESubpackages()) {
			EClass r = findMetaclassInSubPackages(subPkg, metaclass);
			if (r != null)
				return r;
		}
		return null;
	}

	public List<EObject> allObjectsOf(EClass klass) {

		LinkedList<EObject> objects = new LinkedList<EObject>();
		Iterator<EObject> it = allCreatedObjectsIntoResource.iterator();
		while (it.hasNext()) {
			EObject obj = it.next();
			if (klass.isInstance(obj)) // .eClass() == klass )
				objects.add(obj);
		}
		return objects;
	}

	public void addToResource(EObject obj) {
		synchronized (allCreatedObjectsIntoResource) {
			allCreatedObjectsIntoResource.add(obj);
		}
		// resource.getContents().add(obj);
	}

	public void packRootElements() {
		for (EObject obj : allCreatedObjectsIntoResource) {
			if (obj.eContainer() == null) {
				resource.getContents().add(obj);
			}
		}
	}

	public List<EObject> getRootElements(){
	    	List<EObject> objects = new ArrayList<EObject>();
		for (EObject obj : allCreatedObjectsIntoResource) {
			if (obj.eContainer() == null) {
			    objects.add(obj);
			}
		}
		return objects;
	}
	public EEnumLiteral findLiteral(String enumName, String literalName) {
		return ((EEnum) packages.get(0).getEClassifier(enumName))
				.getEEnumLiteral(literalName);
	}

	public void serialize() {
		try {
			// TODO: Make "pack" an option
			packRootElements();

			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(XMLResource.OPTION_SCHEMA_LOCATION, true);
			resource.save(map);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}		
	}

	public void serialize(OutputStream output) {
		try {
			// TODO: Make "pack" an option
			packRootElements();

			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(XMLResource.OPTION_SCHEMA_LOCATION, true);
			resource.save(output, map);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}				
	}
}
