/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches.importers.yed;

import java.io.FileInputStream;

import metabup.sketches.Sketch;
import metabup.sketches.SketchElement;
import metabup.sketches.legend.Legend;
import metabup.sketches.legend.LegendElement;
import metabup.sketches.legend.LegendFactory;
import metabup.sketches.resources.LegendFolder;

public class ImportFragmentFromYedWithLegendFolderAction extends ImportFragmentFromYedAction {
	@Override
	public void initialize(){
		super.initialize();
		super.storeLegendInFolder(true);
	}
	
	@Override
	public Sketch readSketch(FileInputStream stream) {
		YedImporter importer = new YedImporter();
		Sketch sketch = importer.read(stream, "yed_sketch");
		return sketch;
	}

	@Override
	public Legend readLegend(FileInputStream stream) {
		LegendFolder folder = new LegendFolder(this.getProject());
		if(!folder.exists()) return null;
		return folder.getLegend();		
	}
}
