/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fragments.Fragment;
import fragments.FragmentType;
import fragments.FragmentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fragment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fragments.impl.FragmentImpl#getFtype <em>Ftype</em>}</li>
 *   <li>{@link fragments.impl.FragmentImpl#getObjects <em>Objects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FragmentImpl extends TypedElementImpl implements Fragment {
	/**
	 * The default value of the '{@link #getFtype() <em>Ftype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFtype()
	 * @generated
	 * @ordered
	 */
	protected static final FragmentType FTYPE_EDEFAULT = FragmentType.POSITIVE;

	/**
	 * The cached value of the '{@link #getFtype() <em>Ftype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFtype()
	 * @generated
	 * @ordered
	 */
	protected FragmentType ftype = FTYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getObjects() <em>Objects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<fragments.Object> objects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FragmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FragmentsPackage.Literals.FRAGMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FragmentType getFtype() {
		return ftype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFtype(FragmentType newFtype) {
		FragmentType oldFtype = ftype;
		ftype = newFtype == null ? FTYPE_EDEFAULT : newFtype;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FragmentsPackage.FRAGMENT__FTYPE, oldFtype, ftype));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<fragments.Object> getObjects() {
		if (objects == null) {
			objects = new EObjectContainmentEList<fragments.Object>(fragments.Object.class, this, FragmentsPackage.FRAGMENT__OBJECTS);
		}
		return objects;
	}
	
	public List<fragments.Object> getObjects(String type) {
		List<fragments.Object> objs = new ArrayList<fragments.Object>();
		for(fragments.Object o : this.getObjects()) if(o.getType().equals(type)) objs.add(o);

		return objs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FragmentsPackage.FRAGMENT__OBJECTS:
				return ((InternalEList<?>)getObjects()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FragmentsPackage.FRAGMENT__FTYPE:
				return getFtype();
			case FragmentsPackage.FRAGMENT__OBJECTS:
				return getObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FragmentsPackage.FRAGMENT__FTYPE:
				setFtype((FragmentType)newValue);
				return;
			case FragmentsPackage.FRAGMENT__OBJECTS:
				getObjects().clear();
				getObjects().addAll((Collection<? extends fragments.Object>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FragmentsPackage.FRAGMENT__FTYPE:
				setFtype(FTYPE_EDEFAULT);
				return;
			case FragmentsPackage.FRAGMENT__OBJECTS:
				getObjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FragmentsPackage.FRAGMENT__FTYPE:
				return ftype != FTYPE_EDEFAULT;
			case FragmentsPackage.FRAGMENT__OBJECTS:
				return objects != null && !objects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (ftype: ");
		result.append(ftype);
		result.append(')');
		return result.toString();
	}

	@Override
	public fragments.Object getObjectByName(String name) {
		for(fragments.Object o : this.getObjects()){
			if(o.getName().toLowerCase().equals(name.toLowerCase())) return o;
		}
		
		return null;
	}

	
} //FragmentImpl
