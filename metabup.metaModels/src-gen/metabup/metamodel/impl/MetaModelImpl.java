/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.metamodel.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import metabup.metamodel.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link metabup.metamodel.impl.MetaModelImpl#getClasses <em>Classes</em>}</li>
 *   <li>{@link metabup.metamodel.impl.MetaModelImpl#getName <em>Name</em>}</li>
 *   <li>{@link metabup.metamodel.impl.MetaModelImpl#getSrcURI <em>Src URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetaModelImpl extends MinimalEObjectImpl.Container implements MetaModel {
	/**
	 * The cached value of the '{@link #getClasses() <em>Classes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClasses()
	 * @generated
	 * @ordered
	 */
	protected EList<MetaClass> classes;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSrcURI() <em>Src URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcURI()
	 * @generated
	 * @ordered
	 */
	protected static final String SRC_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSrcURI() <em>Src URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcURI()
	 * @generated
	 * @ordered
	 */
	protected String srcURI = SRC_URI_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetaModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.META_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MetaClass> getClasses() {
		if (classes == null) {
			classes = new EObjectContainmentEList<MetaClass>(MetaClass.class, this, MetamodelPackage.META_MODEL__CLASSES);
		}
		return classes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.META_MODEL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSrcURI() {
		return srcURI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSrcURI(String newSrcURI) {
		String oldSrcURI = srcURI;
		srcURI = newSrcURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.META_MODEL__SRC_URI, oldSrcURI, srcURI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodelPackage.META_MODEL__CLASSES:
				return ((InternalEList<?>)getClasses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodelPackage.META_MODEL__CLASSES:
				return getClasses();
			case MetamodelPackage.META_MODEL__NAME:
				return getName();
			case MetamodelPackage.META_MODEL__SRC_URI:
				return getSrcURI();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodelPackage.META_MODEL__CLASSES:
				getClasses().clear();
				getClasses().addAll((Collection<? extends MetaClass>)newValue);
				return;
			case MetamodelPackage.META_MODEL__NAME:
				setName((String)newValue);
				return;
			case MetamodelPackage.META_MODEL__SRC_URI:
				setSrcURI((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodelPackage.META_MODEL__CLASSES:
				getClasses().clear();
				return;
			case MetamodelPackage.META_MODEL__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MetamodelPackage.META_MODEL__SRC_URI:
				setSrcURI(SRC_URI_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodelPackage.META_MODEL__CLASSES:
				return classes != null && !classes.isEmpty();
			case MetamodelPackage.META_MODEL__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MetamodelPackage.META_MODEL__SRC_URI:
				return SRC_URI_EDEFAULT == null ? srcURI != null : !SRC_URI_EDEFAULT.equals(srcURI);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", srcURI: ");
		result.append(srcURI);
		result.append(')');
		return result.toString();
	}
	
	
	public List<EObject> getAllElements() {
		List<EObject> elements = new ArrayList<EObject>();
		
		if(this.classes != null){
			elements.addAll(this.classes);
			for(MetaClass mc : this.classes) elements.addAll(mc.getFeatures());
		}
		
		return elements;
	}

	public List<Reference> getReferences() {
		List<Reference> references = new ArrayList<Reference>();
		
		for(MetaClass mc : this.getClasses())
			references.addAll(mc.getReferences());
		
		return references;
	}

	
	public List<Attribute> getAttributes() {
		List<Attribute> attributes = new ArrayList<Attribute>();
		
		for(MetaClass mc : this.getClasses())
			attributes.addAll(mc.getAttributes());
		
		return attributes;
	}
	
	public MetaClass getClassById(String id){
		for(MetaClass mc : this.getClasses())
			if(mc.getId().equals(id))
				return mc;
		
		return null;		
	}
	
	public AnnotatedElement getElementById(String id){
		for(MetaClass mc : this.getClasses()){
			if(mc.getId().equals(id)){
				return mc;
			}else{
				for(Feature f : mc.getFeatures()){
					if(f.getId().equals(id)) return f;
				}
			}
		}
	
		return null;		
	}
	
	public MetaClass getClassByName(String name){
		for(MetaClass mc : this.getClasses())
			if(mc.getName() != null && !mc.getName().equals("") && mc.getName().equals(name))
				return mc;
		
		return null;
		
	}
} //MetaModelImpl
