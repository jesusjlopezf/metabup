package metabest.test.fragments.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import metabest.test.fragments.services.MBFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMBFParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'metamodel'", "'{'", "'}'", "':'", "';'", "'attr'", "'='", "','", "'ref'", "'rel'", "'@'", "'('", "')'", "'->'", "'.'", "'true'", "'false'", "'fails'", "'at'", "'least'", "'because:'", "'extension'", "'rules:'", "'-'", "'=>'", "'no'", "'some'", "'not'", "'every'", "'strictly'", "'['", "'*)'", "']'", "'new'", "'and'", "'or'", "'reference'", "'via'", "'contain'", "'missing'", "'cont'", "'from'", "'to'", "'unknown'", "'abstract'", "'attribute'", "'mismatch on'", "'constraint violation'", "'on'", "'type of'", "'multiplicity of'", "'nature of'", "'positive'", "'negative'", "'fragment'", "'example'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__64=64;
    public static final int T__21=21;
    public static final int T__65=65;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalMBFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMBFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMBFParser.tokenNames; }
    public String getGrammarFileName() { return "../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g"; }



     	private MBFGrammarAccess grammarAccess;
     	
        public InternalMBFParser(TokenStream input, MBFGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "TestModel";	
       	}
       	
       	@Override
       	protected MBFGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleTestModel"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:68:1: entryRuleTestModel returns [EObject current=null] : iv_ruleTestModel= ruleTestModel EOF ;
    public final EObject entryRuleTestModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTestModel = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:69:2: (iv_ruleTestModel= ruleTestModel EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:70:2: iv_ruleTestModel= ruleTestModel EOF
            {
             newCompositeNode(grammarAccess.getTestModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleTestModel_in_entryRuleTestModel75);
            iv_ruleTestModel=ruleTestModel();

            state._fsp--;

             current =iv_ruleTestModel; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTestModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTestModel"


    // $ANTLR start "ruleTestModel"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:77:1: ruleTestModel returns [EObject current=null] : ( (otherlv_0= 'metamodel' ( (lv_importURI_1_0= RULE_STRING ) ) )? ( (lv_testableFragments_2_0= ruleTestableFragment ) ) ( (lv_testableFragments_3_0= ruleTestableFragment ) )* ) ;
    public final EObject ruleTestModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;
        EObject lv_testableFragments_2_0 = null;

        EObject lv_testableFragments_3_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:80:28: ( ( (otherlv_0= 'metamodel' ( (lv_importURI_1_0= RULE_STRING ) ) )? ( (lv_testableFragments_2_0= ruleTestableFragment ) ) ( (lv_testableFragments_3_0= ruleTestableFragment ) )* ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:81:1: ( (otherlv_0= 'metamodel' ( (lv_importURI_1_0= RULE_STRING ) ) )? ( (lv_testableFragments_2_0= ruleTestableFragment ) ) ( (lv_testableFragments_3_0= ruleTestableFragment ) )* )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:81:1: ( (otherlv_0= 'metamodel' ( (lv_importURI_1_0= RULE_STRING ) ) )? ( (lv_testableFragments_2_0= ruleTestableFragment ) ) ( (lv_testableFragments_3_0= ruleTestableFragment ) )* )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:81:2: (otherlv_0= 'metamodel' ( (lv_importURI_1_0= RULE_STRING ) ) )? ( (lv_testableFragments_2_0= ruleTestableFragment ) ) ( (lv_testableFragments_3_0= ruleTestableFragment ) )*
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:81:2: (otherlv_0= 'metamodel' ( (lv_importURI_1_0= RULE_STRING ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:81:4: otherlv_0= 'metamodel' ( (lv_importURI_1_0= RULE_STRING ) )
                    {
                    otherlv_0=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleTestModel123); 

                        	newLeafNode(otherlv_0, grammarAccess.getTestModelAccess().getMetamodelKeyword_0_0());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:85:1: ( (lv_importURI_1_0= RULE_STRING ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:86:1: (lv_importURI_1_0= RULE_STRING )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:86:1: (lv_importURI_1_0= RULE_STRING )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:87:3: lv_importURI_1_0= RULE_STRING
                    {
                    lv_importURI_1_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleTestModel140); 

                    			newLeafNode(lv_importURI_1_0, grammarAccess.getTestModelAccess().getImportURISTRINGTerminalRuleCall_0_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getTestModelRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"importURI",
                            		lv_importURI_1_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:103:4: ( (lv_testableFragments_2_0= ruleTestableFragment ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:104:1: (lv_testableFragments_2_0= ruleTestableFragment )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:104:1: (lv_testableFragments_2_0= ruleTestableFragment )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:105:3: lv_testableFragments_2_0= ruleTestableFragment
            {
             
            	        newCompositeNode(grammarAccess.getTestModelAccess().getTestableFragmentsTestableFragmentParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleTestableFragment_in_ruleTestModel168);
            lv_testableFragments_2_0=ruleTestableFragment();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTestModelRule());
            	        }
                   		add(
                   			current, 
                   			"testableFragments",
                    		lv_testableFragments_2_0, 
                    		"TestableFragment");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:121:2: ( (lv_testableFragments_3_0= ruleTestableFragment ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==21||(LA2_0>=63 && LA2_0<=66)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:122:1: (lv_testableFragments_3_0= ruleTestableFragment )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:122:1: (lv_testableFragments_3_0= ruleTestableFragment )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:123:3: lv_testableFragments_3_0= ruleTestableFragment
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTestModelAccess().getTestableFragmentsTestableFragmentParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleTestableFragment_in_ruleTestModel189);
            	    lv_testableFragments_3_0=ruleTestableFragment();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTestModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"testableFragments",
            	            		lv_testableFragments_3_0, 
            	            		"TestableFragment");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTestModel"


    // $ANTLR start "entryRuleTestableFragment"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:147:1: entryRuleTestableFragment returns [EObject current=null] : iv_ruleTestableFragment= ruleTestableFragment EOF ;
    public final EObject entryRuleTestableFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTestableFragment = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:148:2: (iv_ruleTestableFragment= ruleTestableFragment EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:149:2: iv_ruleTestableFragment= ruleTestableFragment EOF
            {
             newCompositeNode(grammarAccess.getTestableFragmentRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleTestableFragment_in_entryRuleTestableFragment226);
            iv_ruleTestableFragment=ruleTestableFragment();

            state._fsp--;

             current =iv_ruleTestableFragment; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTestableFragment236); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTestableFragment"


    // $ANTLR start "ruleTestableFragment"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:156:1: ruleTestableFragment returns [EObject current=null] : ( ( ( (lv_annotation_0_0= ruleAnnotation ) ) ( (lv_annotation_1_0= ruleAnnotation ) )* )? ( (lv_ftype_2_0= ruleFragmentType ) )? ( (lv_completion_3_0= ruleCompletionType ) ) ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' ( (lv_objects_6_0= ruleObject ) ) ( (lv_objects_7_0= ruleObject ) )* ( (lv_assertionSet_8_0= ruleComplementaryBlock ) )? otherlv_9= '}' ) ;
    public final EObject ruleTestableFragment() throws RecognitionException {
        EObject current = null;

        Token otherlv_5=null;
        Token otherlv_9=null;
        EObject lv_annotation_0_0 = null;

        EObject lv_annotation_1_0 = null;

        Enumerator lv_ftype_2_0 = null;

        Enumerator lv_completion_3_0 = null;

        AntlrDatatypeRuleToken lv_name_4_0 = null;

        EObject lv_objects_6_0 = null;

        EObject lv_objects_7_0 = null;

        EObject lv_assertionSet_8_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:159:28: ( ( ( ( (lv_annotation_0_0= ruleAnnotation ) ) ( (lv_annotation_1_0= ruleAnnotation ) )* )? ( (lv_ftype_2_0= ruleFragmentType ) )? ( (lv_completion_3_0= ruleCompletionType ) ) ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' ( (lv_objects_6_0= ruleObject ) ) ( (lv_objects_7_0= ruleObject ) )* ( (lv_assertionSet_8_0= ruleComplementaryBlock ) )? otherlv_9= '}' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:160:1: ( ( ( (lv_annotation_0_0= ruleAnnotation ) ) ( (lv_annotation_1_0= ruleAnnotation ) )* )? ( (lv_ftype_2_0= ruleFragmentType ) )? ( (lv_completion_3_0= ruleCompletionType ) ) ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' ( (lv_objects_6_0= ruleObject ) ) ( (lv_objects_7_0= ruleObject ) )* ( (lv_assertionSet_8_0= ruleComplementaryBlock ) )? otherlv_9= '}' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:160:1: ( ( ( (lv_annotation_0_0= ruleAnnotation ) ) ( (lv_annotation_1_0= ruleAnnotation ) )* )? ( (lv_ftype_2_0= ruleFragmentType ) )? ( (lv_completion_3_0= ruleCompletionType ) ) ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' ( (lv_objects_6_0= ruleObject ) ) ( (lv_objects_7_0= ruleObject ) )* ( (lv_assertionSet_8_0= ruleComplementaryBlock ) )? otherlv_9= '}' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:160:2: ( ( (lv_annotation_0_0= ruleAnnotation ) ) ( (lv_annotation_1_0= ruleAnnotation ) )* )? ( (lv_ftype_2_0= ruleFragmentType ) )? ( (lv_completion_3_0= ruleCompletionType ) ) ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' ( (lv_objects_6_0= ruleObject ) ) ( (lv_objects_7_0= ruleObject ) )* ( (lv_assertionSet_8_0= ruleComplementaryBlock ) )? otherlv_9= '}'
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:160:2: ( ( (lv_annotation_0_0= ruleAnnotation ) ) ( (lv_annotation_1_0= ruleAnnotation ) )* )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==21) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:160:3: ( (lv_annotation_0_0= ruleAnnotation ) ) ( (lv_annotation_1_0= ruleAnnotation ) )*
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:160:3: ( (lv_annotation_0_0= ruleAnnotation ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:161:1: (lv_annotation_0_0= ruleAnnotation )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:161:1: (lv_annotation_0_0= ruleAnnotation )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:162:3: lv_annotation_0_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getTestableFragmentAccess().getAnnotationAnnotationParserRuleCall_0_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleTestableFragment283);
                    lv_annotation_0_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTestableFragmentRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_0_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:178:2: ( (lv_annotation_1_0= ruleAnnotation ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==21) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:179:1: (lv_annotation_1_0= ruleAnnotation )
                    	    {
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:179:1: (lv_annotation_1_0= ruleAnnotation )
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:180:3: lv_annotation_1_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getTestableFragmentAccess().getAnnotationAnnotationParserRuleCall_0_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleTestableFragment304);
                    	    lv_annotation_1_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getTestableFragmentRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_1_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:196:5: ( (lv_ftype_2_0= ruleFragmentType ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( ((LA5_0>=63 && LA5_0<=64)) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:197:1: (lv_ftype_2_0= ruleFragmentType )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:197:1: (lv_ftype_2_0= ruleFragmentType )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:198:3: lv_ftype_2_0= ruleFragmentType
                    {
                     
                    	        newCompositeNode(grammarAccess.getTestableFragmentAccess().getFtypeFragmentTypeEnumRuleCall_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFragmentType_in_ruleTestableFragment328);
                    lv_ftype_2_0=ruleFragmentType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTestableFragmentRule());
                    	        }
                           		set(
                           			current, 
                           			"ftype",
                            		lv_ftype_2_0, 
                            		"FragmentType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:214:3: ( (lv_completion_3_0= ruleCompletionType ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:215:1: (lv_completion_3_0= ruleCompletionType )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:215:1: (lv_completion_3_0= ruleCompletionType )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:216:3: lv_completion_3_0= ruleCompletionType
            {
             
            	        newCompositeNode(grammarAccess.getTestableFragmentAccess().getCompletionCompletionTypeEnumRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleCompletionType_in_ruleTestableFragment350);
            lv_completion_3_0=ruleCompletionType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTestableFragmentRule());
            	        }
                   		set(
                   			current, 
                   			"completion",
                    		lv_completion_3_0, 
                    		"CompletionType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:232:2: ( (lv_name_4_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:233:1: (lv_name_4_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:233:1: (lv_name_4_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:234:3: lv_name_4_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getTestableFragmentAccess().getNameEStringParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleTestableFragment371);
            lv_name_4_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTestableFragmentRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_4_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_5=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleTestableFragment383); 

                	newLeafNode(otherlv_5, grammarAccess.getTestableFragmentAccess().getLeftCurlyBracketKeyword_4());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:254:1: ( (lv_objects_6_0= ruleObject ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:255:1: (lv_objects_6_0= ruleObject )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:255:1: (lv_objects_6_0= ruleObject )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:256:3: lv_objects_6_0= ruleObject
            {
             
            	        newCompositeNode(grammarAccess.getTestableFragmentAccess().getObjectsObjectParserRuleCall_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_ruleTestableFragment404);
            lv_objects_6_0=ruleObject();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTestableFragmentRule());
            	        }
                   		add(
                   			current, 
                   			"objects",
                    		lv_objects_6_0, 
                    		"Object");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:272:2: ( (lv_objects_7_0= ruleObject ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=RULE_STRING && LA6_0<=RULE_ID)||LA6_0==21) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:273:1: (lv_objects_7_0= ruleObject )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:273:1: (lv_objects_7_0= ruleObject )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:274:3: lv_objects_7_0= ruleObject
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getTestableFragmentAccess().getObjectsObjectParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleObject_in_ruleTestableFragment425);
            	    lv_objects_7_0=ruleObject();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getTestableFragmentRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"objects",
            	            		lv_objects_7_0, 
            	            		"Object");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:290:3: ( (lv_assertionSet_8_0= ruleComplementaryBlock ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==28||LA7_0==32) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:291:1: (lv_assertionSet_8_0= ruleComplementaryBlock )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:291:1: (lv_assertionSet_8_0= ruleComplementaryBlock )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:292:3: lv_assertionSet_8_0= ruleComplementaryBlock
                    {
                     
                    	        newCompositeNode(grammarAccess.getTestableFragmentAccess().getAssertionSetComplementaryBlockParserRuleCall_7_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleComplementaryBlock_in_ruleTestableFragment447);
                    lv_assertionSet_8_0=ruleComplementaryBlock();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getTestableFragmentRule());
                    	        }
                           		set(
                           			current, 
                           			"assertionSet",
                            		lv_assertionSet_8_0, 
                            		"ComplementaryBlock");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleTestableFragment460); 

                	newLeafNode(otherlv_9, grammarAccess.getTestableFragmentAccess().getRightCurlyBracketKeyword_8());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTestableFragment"


    // $ANTLR start "entryRuleComplementaryBlock"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:320:1: entryRuleComplementaryBlock returns [EObject current=null] : iv_ruleComplementaryBlock= ruleComplementaryBlock EOF ;
    public final EObject entryRuleComplementaryBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComplementaryBlock = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:321:2: (iv_ruleComplementaryBlock= ruleComplementaryBlock EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:322:2: iv_ruleComplementaryBlock= ruleComplementaryBlock EOF
            {
             newCompositeNode(grammarAccess.getComplementaryBlockRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleComplementaryBlock_in_entryRuleComplementaryBlock496);
            iv_ruleComplementaryBlock=ruleComplementaryBlock();

            state._fsp--;

             current =iv_ruleComplementaryBlock; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleComplementaryBlock506); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComplementaryBlock"


    // $ANTLR start "ruleComplementaryBlock"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:329:1: ruleComplementaryBlock returns [EObject current=null] : (this_AssertionSet_0= ruleAssertionSet | this_ExtensionAssertionSet_1= ruleExtensionAssertionSet ) ;
    public final EObject ruleComplementaryBlock() throws RecognitionException {
        EObject current = null;

        EObject this_AssertionSet_0 = null;

        EObject this_ExtensionAssertionSet_1 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:332:28: ( (this_AssertionSet_0= ruleAssertionSet | this_ExtensionAssertionSet_1= ruleExtensionAssertionSet ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:333:1: (this_AssertionSet_0= ruleAssertionSet | this_ExtensionAssertionSet_1= ruleExtensionAssertionSet )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:333:1: (this_AssertionSet_0= ruleAssertionSet | this_ExtensionAssertionSet_1= ruleExtensionAssertionSet )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==28) ) {
                alt8=1;
            }
            else if ( (LA8_0==32) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:334:5: this_AssertionSet_0= ruleAssertionSet
                    {
                     
                            newCompositeNode(grammarAccess.getComplementaryBlockAccess().getAssertionSetParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleAssertionSet_in_ruleComplementaryBlock553);
                    this_AssertionSet_0=ruleAssertionSet();

                    state._fsp--;

                     
                            current = this_AssertionSet_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:344:5: this_ExtensionAssertionSet_1= ruleExtensionAssertionSet
                    {
                     
                            newCompositeNode(grammarAccess.getComplementaryBlockAccess().getExtensionAssertionSetParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleExtensionAssertionSet_in_ruleComplementaryBlock580);
                    this_ExtensionAssertionSet_1=ruleExtensionAssertionSet();

                    state._fsp--;

                     
                            current = this_ExtensionAssertionSet_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComplementaryBlock"


    // $ANTLR start "entryRuleObject"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:360:1: entryRuleObject returns [EObject current=null] : iv_ruleObject= ruleObject EOF ;
    public final EObject entryRuleObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObject = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:361:2: (iv_ruleObject= ruleObject EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:362:2: iv_ruleObject= ruleObject EOF
            {
             newCompositeNode(grammarAccess.getObjectRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_entryRuleObject615);
            iv_ruleObject=ruleObject();

            state._fsp--;

             current =iv_ruleObject; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObject625); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObject"


    // $ANTLR start "ruleObject"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:369:1: ruleObject returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' ) ;
    public final EObject ruleObject() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        AntlrDatatypeRuleToken lv_name_3_0 = null;

        AntlrDatatypeRuleToken lv_type_5_0 = null;

        EObject lv_features_7_0 = null;

        EObject lv_features_9_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:372:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:373:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:373:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:373:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}'
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:373:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:374:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getObjectAccess().getObjectAction_0(),
                        current);
                

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:379:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==21) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:379:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:379:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:380:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:380:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:381:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleObject681);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:397:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==21) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:398:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:398:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:399:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleObject702);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:415:5: ( (lv_name_3_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:416:1: (lv_name_3_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:416:1: (lv_name_3_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:417:3: lv_name_3_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getObjectAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObject726);
            lv_name_3_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getObjectRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_3_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleObject738); 

                	newLeafNode(otherlv_4, grammarAccess.getObjectAccess().getColonKeyword_3());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:437:1: ( (lv_type_5_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:438:1: (lv_type_5_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:438:1: (lv_type_5_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:439:3: lv_type_5_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getObjectAccess().getTypeEStringParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObject759);
            lv_type_5_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getObjectRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_5_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleObject771); 

                	newLeafNode(otherlv_6, grammarAccess.getObjectAccess().getLeftCurlyBracketKeyword_5());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:459:1: ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==16||(LA14_0>=19 && LA14_0<=21)) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:459:2: ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )*
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:459:2: ( (lv_features_7_0= ruleFeature ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:460:1: (lv_features_7_0= ruleFeature )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:460:1: (lv_features_7_0= ruleFeature )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:461:3: lv_features_7_0= ruleFeature
                    {
                     
                    	        newCompositeNode(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleObject793);
                    lv_features_7_0=ruleFeature();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	        }
                           		add(
                           			current, 
                           			"features",
                            		lv_features_7_0, 
                            		"Feature");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:477:2: (otherlv_8= ';' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0==15) ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:477:4: otherlv_8= ';'
                            {
                            otherlv_8=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleObject806); 

                                	newLeafNode(otherlv_8, grammarAccess.getObjectAccess().getSemicolonKeyword_6_1());
                                

                            }
                            break;

                    }

                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:481:3: ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==16||(LA13_0>=19 && LA13_0<=21)) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:481:4: ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )?
                    	    {
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:481:4: ( (lv_features_9_0= ruleFeature ) )
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:482:1: (lv_features_9_0= ruleFeature )
                    	    {
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:482:1: (lv_features_9_0= ruleFeature )
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:483:3: lv_features_9_0= ruleFeature
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_2_0_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleObject830);
                    	    lv_features_9_0=ruleFeature();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"features",
                    	            		lv_features_9_0, 
                    	            		"Feature");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }

                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:499:2: (otherlv_10= ';' )?
                    	    int alt12=2;
                    	    int LA12_0 = input.LA(1);

                    	    if ( (LA12_0==15) ) {
                    	        alt12=1;
                    	    }
                    	    switch (alt12) {
                    	        case 1 :
                    	            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:499:4: otherlv_10= ';'
                    	            {
                    	            otherlv_10=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleObject843); 

                    	                	newLeafNode(otherlv_10, grammarAccess.getObjectAccess().getSemicolonKeyword_6_2_1());
                    	                

                    	            }
                    	            break;

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_11=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleObject861); 

                	newLeafNode(otherlv_11, grammarAccess.getObjectAccess().getRightCurlyBracketKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObject"


    // $ANTLR start "entryRuleFeature"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:515:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:516:2: (iv_ruleFeature= ruleFeature EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:517:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_entryRuleFeature897);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeature907); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:524:1: ruleFeature returns [EObject current=null] : (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        EObject this_Attribute_0 = null;

        EObject this_Reference_1 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:527:28: ( (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:528:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:528:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )
            int alt15=2;
            alt15 = dfa15.predict(input);
            switch (alt15) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:529:5: this_Attribute_0= ruleAttribute
                    {
                     
                            newCompositeNode(grammarAccess.getFeatureAccess().getAttributeParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_ruleFeature954);
                    this_Attribute_0=ruleAttribute();

                    state._fsp--;

                     
                            current = this_Attribute_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:539:5: this_Reference_1= ruleReference
                    {
                     
                            newCompositeNode(grammarAccess.getFeatureAccess().getReferenceParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleReference_in_ruleFeature981);
                    this_Reference_1=ruleReference();

                    state._fsp--;

                     
                            current = this_Reference_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleAttribute"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:555:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:556:2: (iv_ruleAttribute= ruleAttribute EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:557:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_entryRuleAttribute1016);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttribute1026); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:564:1: ruleAttribute returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        AntlrDatatypeRuleToken lv_name_4_0 = null;

        EObject lv_values_6_0 = null;

        EObject lv_values_8_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:567:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:568:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:568:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:568:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )*
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:568:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:569:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAttributeAccess().getAttributeAction_0(),
                        current);
                

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:574:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==21) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:574:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:574:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:575:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:575:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:576:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleAttribute1082);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:592:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==21) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:593:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:593:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:594:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleAttribute1103);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_3=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleAttribute1118); 

                	newLeafNode(otherlv_3, grammarAccess.getAttributeAccess().getAttrKeyword_2());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:614:1: ( (lv_name_4_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:615:1: (lv_name_4_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:615:1: (lv_name_4_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:616:3: lv_name_4_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAttribute1139);
            lv_name_4_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_4_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_5=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleAttribute1151); 

                	newLeafNode(otherlv_5, grammarAccess.getAttributeAccess().getEqualsSignKeyword_4());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:636:1: ( (lv_values_6_0= rulePrimitiveValue ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:637:1: (lv_values_6_0= rulePrimitiveValue )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:637:1: (lv_values_6_0= rulePrimitiveValue )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:638:3: lv_values_6_0= rulePrimitiveValue
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_ruleAttribute1172);
            lv_values_6_0=rulePrimitiveValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		add(
                   			current, 
                   			"values",
                    		lv_values_6_0, 
                    		"PrimitiveValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:654:2: (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==18) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:654:4: otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) )
            	    {
            	    otherlv_7=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleAttribute1185); 

            	        	newLeafNode(otherlv_7, grammarAccess.getAttributeAccess().getCommaKeyword_6_0());
            	        
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:658:1: ( (lv_values_8_0= rulePrimitiveValue ) )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:659:1: (lv_values_8_0= rulePrimitiveValue )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:659:1: (lv_values_8_0= rulePrimitiveValue )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:660:3: lv_values_8_0= rulePrimitiveValue
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_6_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_ruleAttribute1206);
            	    lv_values_8_0=rulePrimitiveValue();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"values",
            	            		lv_values_8_0, 
            	            		"PrimitiveValue");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleReference"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:684:1: entryRuleReference returns [EObject current=null] : iv_ruleReference= ruleReference EOF ;
    public final EObject entryRuleReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReference = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:685:2: (iv_ruleReference= ruleReference EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:686:2: iv_ruleReference= ruleReference EOF
            {
             newCompositeNode(grammarAccess.getReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReference_in_entryRuleReference1244);
            iv_ruleReference=ruleReference();

            state._fsp--;

             current =iv_ruleReference; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReference1254); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:693:1: ruleReference returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? (otherlv_3= 'ref' | otherlv_4= 'rel' ) ( (lv_name_5_0= ruleEString ) ) otherlv_6= '=' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* ) ;
    public final EObject ruleReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        AntlrDatatypeRuleToken lv_name_5_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:696:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? (otherlv_3= 'ref' | otherlv_4= 'rel' ) ( (lv_name_5_0= ruleEString ) ) otherlv_6= '=' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:697:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? (otherlv_3= 'ref' | otherlv_4= 'rel' ) ( (lv_name_5_0= ruleEString ) ) otherlv_6= '=' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:697:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? (otherlv_3= 'ref' | otherlv_4= 'rel' ) ( (lv_name_5_0= ruleEString ) ) otherlv_6= '=' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )* )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:697:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? (otherlv_3= 'ref' | otherlv_4= 'rel' ) ( (lv_name_5_0= ruleEString ) ) otherlv_6= '=' ( ( ruleEString ) ) (otherlv_8= ',' ( ( ruleEString ) ) )*
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:697:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:698:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getReferenceAccess().getReferenceAction_0(),
                        current);
                

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:703:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==21) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:703:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:703:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:704:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:704:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:705:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleReference1310);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReferenceRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:721:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==21) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:722:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:722:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:723:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleReference1331);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getReferenceRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop19;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:739:5: (otherlv_3= 'ref' | otherlv_4= 'rel' )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==19) ) {
                alt21=1;
            }
            else if ( (LA21_0==20) ) {
                alt21=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:739:7: otherlv_3= 'ref'
                    {
                    otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleReference1347); 

                        	newLeafNode(otherlv_3, grammarAccess.getReferenceAccess().getRefKeyword_2_0());
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:744:7: otherlv_4= 'rel'
                    {
                    otherlv_4=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleReference1365); 

                        	newLeafNode(otherlv_4, grammarAccess.getReferenceAccess().getRelKeyword_2_1());
                        

                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:748:2: ( (lv_name_5_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:749:1: (lv_name_5_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:749:1: (lv_name_5_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:750:3: lv_name_5_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference1387);
            lv_name_5_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReferenceRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_5_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleReference1399); 

                	newLeafNode(otherlv_6, grammarAccess.getReferenceAccess().getEqualsSignKeyword_4());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:770:1: ( ( ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:771:1: ( ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:771:1: ( ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:772:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getReferenceRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference1422);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:785:2: (otherlv_8= ',' ( ( ruleEString ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==18) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:785:4: otherlv_8= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_8=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleReference1435); 

            	        	newLeafNode(otherlv_8, grammarAccess.getReferenceAccess().getCommaKeyword_6_0());
            	        
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:789:1: ( ( ruleEString ) )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:790:1: ( ruleEString )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:790:1: ( ruleEString )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:791:3: ruleEString
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getReferenceRule());
            	    	        }
            	            
            	     
            	    	        newCompositeNode(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_6_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference1458);
            	    ruleEString();

            	    state._fsp--;

            	     
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleEString"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:812:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:813:2: (iv_ruleEString= ruleEString EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:814:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString1497);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString1508); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:821:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:824:28: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:825:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:825:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==RULE_STRING) ) {
                alt23=1;
            }
            else if ( (LA23_0==RULE_ID) ) {
                alt23=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:825:6: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleEString1548); 

                    		current.merge(this_STRING_0);
                        
                     
                        newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:833:10: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleEString1574); 

                    		current.merge(this_ID_1);
                        
                     
                        newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRulePrimitiveValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:848:1: entryRulePrimitiveValue returns [EObject current=null] : iv_rulePrimitiveValue= rulePrimitiveValue EOF ;
    public final EObject entryRulePrimitiveValue() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveValue = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:849:2: (iv_rulePrimitiveValue= rulePrimitiveValue EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:850:2: iv_rulePrimitiveValue= rulePrimitiveValue EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveValueRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue1619);
            iv_rulePrimitiveValue=rulePrimitiveValue();

            state._fsp--;

             current =iv_rulePrimitiveValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValue1629); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveValue"


    // $ANTLR start "rulePrimitiveValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:857:1: rulePrimitiveValue returns [EObject current=null] : (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue ) ;
    public final EObject rulePrimitiveValue() throws RecognitionException {
        EObject current = null;

        EObject this_StringValue_0 = null;

        EObject this_IntegerValue_1 = null;

        EObject this_BooleanValue_2 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:860:28: ( (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:861:1: (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:861:1: (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue )
            int alt24=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt24=1;
                }
                break;
            case RULE_INT:
                {
                alt24=2;
                }
                break;
            case 26:
            case 27:
                {
                alt24=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:862:5: this_StringValue_0= ruleStringValue
                    {
                     
                            newCompositeNode(grammarAccess.getPrimitiveValueAccess().getStringValueParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_rulePrimitiveValue1676);
                    this_StringValue_0=ruleStringValue();

                    state._fsp--;

                     
                            current = this_StringValue_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:872:5: this_IntegerValue_1= ruleIntegerValue
                    {
                     
                            newCompositeNode(grammarAccess.getPrimitiveValueAccess().getIntegerValueParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_rulePrimitiveValue1703);
                    this_IntegerValue_1=ruleIntegerValue();

                    state._fsp--;

                     
                            current = this_IntegerValue_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:882:5: this_BooleanValue_2= ruleBooleanValue
                    {
                     
                            newCompositeNode(grammarAccess.getPrimitiveValueAccess().getBooleanValueParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_rulePrimitiveValue1730);
                    this_BooleanValue_2=ruleBooleanValue();

                    state._fsp--;

                     
                            current = this_BooleanValue_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveValue"


    // $ANTLR start "entryRuleAnnotation"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:898:1: entryRuleAnnotation returns [EObject current=null] : iv_ruleAnnotation= ruleAnnotation EOF ;
    public final EObject entryRuleAnnotation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotation = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:899:2: (iv_ruleAnnotation= ruleAnnotation EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:900:2: iv_ruleAnnotation= ruleAnnotation EOF
            {
             newCompositeNode(grammarAccess.getAnnotationRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_entryRuleAnnotation1765);
            iv_ruleAnnotation=ruleAnnotation();

            state._fsp--;

             current =iv_ruleAnnotation; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotation1775); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotation"


    // $ANTLR start "ruleAnnotation"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:907:1: ruleAnnotation returns [EObject current=null] : (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? ) ;
    public final EObject ruleAnnotation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_params_3_0 = null;

        EObject lv_params_5_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:910:28: ( (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:911:1: (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:911:1: (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:911:3: otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )?
            {
            otherlv_0=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleAnnotation1812); 

                	newLeafNode(otherlv_0, grammarAccess.getAnnotationAccess().getCommercialAtKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:915:1: ( (lv_name_1_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:916:1: (lv_name_1_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:916:1: (lv_name_1_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:917:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAnnotationAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAnnotation1833);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:933:2: (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==22) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:933:4: otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')'
                    {
                    otherlv_2=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleAnnotation1846); 

                        	newLeafNode(otherlv_2, grammarAccess.getAnnotationAccess().getLeftParenthesisKeyword_2_0());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:937:1: ( (lv_params_3_0= ruleAnnotationParam ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:938:1: (lv_params_3_0= ruleAnnotationParam )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:938:1: (lv_params_3_0= ruleAnnotationParam )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:939:3: lv_params_3_0= ruleAnnotationParam
                    {
                     
                    	        newCompositeNode(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_ruleAnnotation1867);
                    lv_params_3_0=ruleAnnotationParam();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
                    	        }
                           		add(
                           			current, 
                           			"params",
                            		lv_params_3_0, 
                            		"AnnotationParam");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:955:2: (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )*
                    loop25:
                    do {
                        int alt25=2;
                        int LA25_0 = input.LA(1);

                        if ( (LA25_0==18) ) {
                            alt25=1;
                        }


                        switch (alt25) {
                    	case 1 :
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:955:4: otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) )
                    	    {
                    	    otherlv_4=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleAnnotation1880); 

                    	        	newLeafNode(otherlv_4, grammarAccess.getAnnotationAccess().getCommaKeyword_2_2_0());
                    	        
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:959:1: ( (lv_params_5_0= ruleAnnotationParam ) )
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:960:1: (lv_params_5_0= ruleAnnotationParam )
                    	    {
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:960:1: (lv_params_5_0= ruleAnnotationParam )
                    	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:961:3: lv_params_5_0= ruleAnnotationParam
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_2_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_ruleAnnotation1901);
                    	    lv_params_5_0=ruleAnnotationParam();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"params",
                    	            		lv_params_5_0, 
                    	            		"AnnotationParam");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop25;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,23,FollowSets000.FOLLOW_23_in_ruleAnnotation1915); 

                        	newLeafNode(otherlv_6, grammarAccess.getAnnotationAccess().getRightParenthesisKeyword_2_3());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotation"


    // $ANTLR start "entryRuleAnnotationParam"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:989:1: entryRuleAnnotationParam returns [EObject current=null] : iv_ruleAnnotationParam= ruleAnnotationParam EOF ;
    public final EObject entryRuleAnnotationParam() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotationParam = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:990:2: (iv_ruleAnnotationParam= ruleAnnotationParam EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:991:2: iv_ruleAnnotationParam= ruleAnnotationParam EOF
            {
             newCompositeNode(grammarAccess.getAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam1953);
            iv_ruleAnnotationParam=ruleAnnotationParam();

            state._fsp--;

             current =iv_ruleAnnotationParam; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotationParam1963); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotationParam"


    // $ANTLR start "ruleAnnotationParam"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:998:1: ruleAnnotationParam returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) ) ;
    public final EObject ruleAnnotationParam() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;

        EObject lv_paramValue_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1001:28: ( ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1002:1: ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1002:1: ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1002:2: ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1002:2: ( (lv_name_0_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1003:1: (lv_name_0_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1003:1: (lv_name_0_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1004:3: lv_name_0_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAnnotationParamAccess().getNameEStringParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAnnotationParam2009);
            lv_name_0_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAnnotationParamRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1020:2: ( (lv_paramValue_1_0= ruleParamValue ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1021:1: (lv_paramValue_1_0= ruleParamValue )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1021:1: (lv_paramValue_1_0= ruleParamValue )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1022:3: lv_paramValue_1_0= ruleParamValue
            {
             
            	        newCompositeNode(grammarAccess.getAnnotationParamAccess().getParamValueParamValueParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_ruleAnnotationParam2030);
            lv_paramValue_1_0=ruleParamValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAnnotationParamRule());
            	        }
                   		set(
                   			current, 
                   			"paramValue",
                    		lv_paramValue_1_0, 
                    		"ParamValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotationParam"


    // $ANTLR start "entryRuleParamValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1046:1: entryRuleParamValue returns [EObject current=null] : iv_ruleParamValue= ruleParamValue EOF ;
    public final EObject entryRuleParamValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParamValue = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1047:2: (iv_ruleParamValue= ruleParamValue EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1048:2: iv_ruleParamValue= ruleParamValue EOF
            {
             newCompositeNode(grammarAccess.getParamValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_entryRuleParamValue2066);
            iv_ruleParamValue=ruleParamValue();

            state._fsp--;

             current =iv_ruleParamValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleParamValue2076); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParamValue"


    // $ANTLR start "ruleParamValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1055:1: ruleParamValue returns [EObject current=null] : (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam ) ;
    public final EObject ruleParamValue() throws RecognitionException {
        EObject current = null;

        EObject this_PrimitiveValueAnnotationParam_0 = null;

        EObject this_ObjectValueAnnotationParam_1 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1058:28: ( (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1059:1: (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1059:1: (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam )
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==17) ) {
                alt27=1;
            }
            else if ( (LA27_0==24) ) {
                alt27=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }
            switch (alt27) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1060:5: this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam
                    {
                     
                            newCompositeNode(grammarAccess.getParamValueAccess().getPrimitiveValueAnnotationParamParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_ruleParamValue2123);
                    this_PrimitiveValueAnnotationParam_0=rulePrimitiveValueAnnotationParam();

                    state._fsp--;

                     
                            current = this_PrimitiveValueAnnotationParam_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1070:5: this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam
                    {
                     
                            newCompositeNode(grammarAccess.getParamValueAccess().getObjectValueAnnotationParamParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_ruleParamValue2150);
                    this_ObjectValueAnnotationParam_1=ruleObjectValueAnnotationParam();

                    state._fsp--;

                     
                            current = this_ObjectValueAnnotationParam_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParamValue"


    // $ANTLR start "entryRuleObjectValueAnnotationParam"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1086:1: entryRuleObjectValueAnnotationParam returns [EObject current=null] : iv_ruleObjectValueAnnotationParam= ruleObjectValueAnnotationParam EOF ;
    public final EObject entryRuleObjectValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectValueAnnotationParam = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1087:2: (iv_ruleObjectValueAnnotationParam= ruleObjectValueAnnotationParam EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1088:2: iv_ruleObjectValueAnnotationParam= ruleObjectValueAnnotationParam EOF
            {
             newCompositeNode(grammarAccess.getObjectValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam2185);
            iv_ruleObjectValueAnnotationParam=ruleObjectValueAnnotationParam();

            state._fsp--;

             current =iv_ruleObjectValueAnnotationParam; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam2195); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectValueAnnotationParam"


    // $ANTLR start "ruleObjectValueAnnotationParam"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1095:1: ruleObjectValueAnnotationParam returns [EObject current=null] : (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? ) ;
    public final EObject ruleObjectValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1098:28: ( (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1099:1: (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1099:1: (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1099:3: otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )?
            {
            otherlv_0=(Token)match(input,24,FollowSets000.FOLLOW_24_in_ruleObjectValueAnnotationParam2232); 

                	newLeafNode(otherlv_0, grammarAccess.getObjectValueAnnotationParamAccess().getHyphenMinusGreaterThanSignKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1103:1: ( ( ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1104:1: ( ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1104:1: ( ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1105:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getObjectValueAnnotationParamRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectCrossReference_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam2255);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1118:2: (otherlv_2= '.' ( ( ruleEString ) ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==25) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1118:4: otherlv_2= '.' ( ( ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleObjectValueAnnotationParam2268); 

                        	newLeafNode(otherlv_2, grammarAccess.getObjectValueAnnotationParamAccess().getFullStopKeyword_2_0());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1122:1: ( ( ruleEString ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1123:1: ( ruleEString )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1123:1: ( ruleEString )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1124:3: ruleEString
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectValueAnnotationParamRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureCrossReference_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam2291);
                    ruleEString();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectValueAnnotationParam"


    // $ANTLR start "entryRulePrimitiveValueAnnotationParam"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1145:1: entryRulePrimitiveValueAnnotationParam returns [EObject current=null] : iv_rulePrimitiveValueAnnotationParam= rulePrimitiveValueAnnotationParam EOF ;
    public final EObject entryRulePrimitiveValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveValueAnnotationParam = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1146:2: (iv_rulePrimitiveValueAnnotationParam= rulePrimitiveValueAnnotationParam EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1147:2: iv_rulePrimitiveValueAnnotationParam= rulePrimitiveValueAnnotationParam EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam2329);
            iv_rulePrimitiveValueAnnotationParam=rulePrimitiveValueAnnotationParam();

            state._fsp--;

             current =iv_rulePrimitiveValueAnnotationParam; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam2339); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveValueAnnotationParam"


    // $ANTLR start "rulePrimitiveValueAnnotationParam"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1154:1: rulePrimitiveValueAnnotationParam returns [EObject current=null] : (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) ) ;
    public final EObject rulePrimitiveValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1157:28: ( (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1158:1: (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1158:1: (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1158:3: otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) )
            {
            otherlv_0=(Token)match(input,17,FollowSets000.FOLLOW_17_in_rulePrimitiveValueAnnotationParam2376); 

                	newLeafNode(otherlv_0, grammarAccess.getPrimitiveValueAnnotationParamAccess().getEqualsSignKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1162:1: ( (lv_value_1_0= rulePrimitiveValue ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1163:1: (lv_value_1_0= rulePrimitiveValue )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1163:1: (lv_value_1_0= rulePrimitiveValue )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1164:3: lv_value_1_0= rulePrimitiveValue
            {
             
            	        newCompositeNode(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValuePrimitiveValueParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_rulePrimitiveValueAnnotationParam2397);
            lv_value_1_0=rulePrimitiveValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPrimitiveValueAnnotationParamRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"PrimitiveValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveValueAnnotationParam"


    // $ANTLR start "entryRuleIntegerValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1188:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1189:2: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1190:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
             newCompositeNode(grammarAccess.getIntegerValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue2433);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;

             current =iv_ruleIntegerValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntegerValue2443); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1197:1: ruleIntegerValue returns [EObject current=null] : ( (lv_value_0_0= RULE_INT ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1200:28: ( ( (lv_value_0_0= RULE_INT ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1201:1: ( (lv_value_0_0= RULE_INT ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1201:1: ( (lv_value_0_0= RULE_INT ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1202:1: (lv_value_0_0= RULE_INT )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1202:1: (lv_value_0_0= RULE_INT )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1203:3: lv_value_0_0= RULE_INT
            {
            lv_value_0_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleIntegerValue2484); 

            			newLeafNode(lv_value_0_0, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getIntegerValueRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"INT");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleStringValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1227:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1228:2: (iv_ruleStringValue= ruleStringValue EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1229:2: iv_ruleStringValue= ruleStringValue EOF
            {
             newCompositeNode(grammarAccess.getStringValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_entryRuleStringValue2524);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;

             current =iv_ruleStringValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringValue2534); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1236:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1239:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1240:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1240:1: ( (lv_value_0_0= RULE_STRING ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1241:1: (lv_value_0_0= RULE_STRING )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1241:1: (lv_value_0_0= RULE_STRING )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1242:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleStringValue2575); 

            			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getStringValueRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"STRING");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleBooleanValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1266:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1267:2: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1268:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
             newCompositeNode(grammarAccess.getBooleanValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue2615);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;

             current =iv_ruleBooleanValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValue2625); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1275:1: ruleBooleanValue returns [EObject current=null] : ( (lv_value_0_0= ruleBooleanValueTerminal ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1278:28: ( ( (lv_value_0_0= ruleBooleanValueTerminal ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1279:1: ( (lv_value_0_0= ruleBooleanValueTerminal ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1279:1: ( (lv_value_0_0= ruleBooleanValueTerminal ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1280:1: (lv_value_0_0= ruleBooleanValueTerminal )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1280:1: (lv_value_0_0= ruleBooleanValueTerminal )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1281:3: lv_value_0_0= ruleBooleanValueTerminal
            {
             
            	        newCompositeNode(grammarAccess.getBooleanValueAccess().getValueBooleanValueTerminalParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_ruleBooleanValue2670);
            lv_value_0_0=ruleBooleanValueTerminal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanValueRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"BooleanValueTerminal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleBooleanValueTerminal"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1305:1: entryRuleBooleanValueTerminal returns [String current=null] : iv_ruleBooleanValueTerminal= ruleBooleanValueTerminal EOF ;
    public final String entryRuleBooleanValueTerminal() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBooleanValueTerminal = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1306:2: (iv_ruleBooleanValueTerminal= ruleBooleanValueTerminal EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1307:2: iv_ruleBooleanValueTerminal= ruleBooleanValueTerminal EOF
            {
             newCompositeNode(grammarAccess.getBooleanValueTerminalRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal2706);
            iv_ruleBooleanValueTerminal=ruleBooleanValueTerminal();

            state._fsp--;

             current =iv_ruleBooleanValueTerminal.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValueTerminal2717); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValueTerminal"


    // $ANTLR start "ruleBooleanValueTerminal"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1314:1: ruleBooleanValueTerminal returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleBooleanValueTerminal() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1317:28: ( (kw= 'true' | kw= 'false' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1318:1: (kw= 'true' | kw= 'false' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1318:1: (kw= 'true' | kw= 'false' )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==26) ) {
                alt29=1;
            }
            else if ( (LA29_0==27) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1319:2: kw= 'true'
                    {
                    kw=(Token)match(input,26,FollowSets000.FOLLOW_26_in_ruleBooleanValueTerminal2755); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getBooleanValueTerminalAccess().getTrueKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1326:2: kw= 'false'
                    {
                    kw=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleBooleanValueTerminal2774); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getBooleanValueTerminalAccess().getFalseKeyword_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValueTerminal"


    // $ANTLR start "entryRuleAssertionSet"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1339:1: entryRuleAssertionSet returns [EObject current=null] : iv_ruleAssertionSet= ruleAssertionSet EOF ;
    public final EObject entryRuleAssertionSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssertionSet = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1340:2: (iv_ruleAssertionSet= ruleAssertionSet EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1341:2: iv_ruleAssertionSet= ruleAssertionSet EOF
            {
             newCompositeNode(grammarAccess.getAssertionSetRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAssertionSet_in_entryRuleAssertionSet2814);
            iv_ruleAssertionSet=ruleAssertionSet();

            state._fsp--;

             current =iv_ruleAssertionSet; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAssertionSet2824); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssertionSet"


    // $ANTLR start "ruleAssertionSet"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1348:1: ruleAssertionSet returns [EObject current=null] : ( () otherlv_1= 'fails' ( ( (lv_atLeast_2_0= 'at' ) ) otherlv_3= 'least' )? otherlv_4= 'because:' ( (lv_assertions_5_0= ruleAssertion ) ) (otherlv_6= '.' ( (lv_assertions_7_0= ruleAssertion ) ) )* ) ;
    public final EObject ruleAssertionSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_atLeast_2_0=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_assertions_5_0 = null;

        EObject lv_assertions_7_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1351:28: ( ( () otherlv_1= 'fails' ( ( (lv_atLeast_2_0= 'at' ) ) otherlv_3= 'least' )? otherlv_4= 'because:' ( (lv_assertions_5_0= ruleAssertion ) ) (otherlv_6= '.' ( (lv_assertions_7_0= ruleAssertion ) ) )* ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1352:1: ( () otherlv_1= 'fails' ( ( (lv_atLeast_2_0= 'at' ) ) otherlv_3= 'least' )? otherlv_4= 'because:' ( (lv_assertions_5_0= ruleAssertion ) ) (otherlv_6= '.' ( (lv_assertions_7_0= ruleAssertion ) ) )* )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1352:1: ( () otherlv_1= 'fails' ( ( (lv_atLeast_2_0= 'at' ) ) otherlv_3= 'least' )? otherlv_4= 'because:' ( (lv_assertions_5_0= ruleAssertion ) ) (otherlv_6= '.' ( (lv_assertions_7_0= ruleAssertion ) ) )* )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1352:2: () otherlv_1= 'fails' ( ( (lv_atLeast_2_0= 'at' ) ) otherlv_3= 'least' )? otherlv_4= 'because:' ( (lv_assertions_5_0= ruleAssertion ) ) (otherlv_6= '.' ( (lv_assertions_7_0= ruleAssertion ) ) )*
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1352:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1353:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAssertionSetAccess().getAssertionSetAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,28,FollowSets000.FOLLOW_28_in_ruleAssertionSet2870); 

                	newLeafNode(otherlv_1, grammarAccess.getAssertionSetAccess().getFailsKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1362:1: ( ( (lv_atLeast_2_0= 'at' ) ) otherlv_3= 'least' )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==29) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1362:2: ( (lv_atLeast_2_0= 'at' ) ) otherlv_3= 'least'
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1362:2: ( (lv_atLeast_2_0= 'at' ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1363:1: (lv_atLeast_2_0= 'at' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1363:1: (lv_atLeast_2_0= 'at' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1364:3: lv_atLeast_2_0= 'at'
                    {
                    lv_atLeast_2_0=(Token)match(input,29,FollowSets000.FOLLOW_29_in_ruleAssertionSet2889); 

                            newLeafNode(lv_atLeast_2_0, grammarAccess.getAssertionSetAccess().getAtLeastAtKeyword_2_0_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getAssertionSetRule());
                    	        }
                           		setWithLastConsumed(current, "atLeast", true, "at");
                    	    

                    }


                    }

                    otherlv_3=(Token)match(input,30,FollowSets000.FOLLOW_30_in_ruleAssertionSet2914); 

                        	newLeafNode(otherlv_3, grammarAccess.getAssertionSetAccess().getLeastKeyword_2_1());
                        

                    }
                    break;

            }

            otherlv_4=(Token)match(input,31,FollowSets000.FOLLOW_31_in_ruleAssertionSet2928); 

                	newLeafNode(otherlv_4, grammarAccess.getAssertionSetAccess().getBecauseKeyword_3());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1385:1: ( (lv_assertions_5_0= ruleAssertion ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1386:1: (lv_assertions_5_0= ruleAssertion )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1386:1: (lv_assertions_5_0= ruleAssertion )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1387:3: lv_assertions_5_0= ruleAssertion
            {
             
            	        newCompositeNode(grammarAccess.getAssertionSetAccess().getAssertionsAssertionParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleAssertion_in_ruleAssertionSet2949);
            lv_assertions_5_0=ruleAssertion();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAssertionSetRule());
            	        }
                   		add(
                   			current, 
                   			"assertions",
                    		lv_assertions_5_0, 
                    		"Assertion");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1403:2: (otherlv_6= '.' ( (lv_assertions_7_0= ruleAssertion ) ) )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==25) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1403:4: otherlv_6= '.' ( (lv_assertions_7_0= ruleAssertion ) )
            	    {
            	    otherlv_6=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleAssertionSet2962); 

            	        	newLeafNode(otherlv_6, grammarAccess.getAssertionSetAccess().getFullStopKeyword_5_0());
            	        
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1407:1: ( (lv_assertions_7_0= ruleAssertion ) )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1408:1: (lv_assertions_7_0= ruleAssertion )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1408:1: (lv_assertions_7_0= ruleAssertion )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1409:3: lv_assertions_7_0= ruleAssertion
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAssertionSetAccess().getAssertionsAssertionParserRuleCall_5_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleAssertion_in_ruleAssertionSet2983);
            	    lv_assertions_7_0=ruleAssertion();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAssertionSetRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"assertions",
            	            		lv_assertions_7_0, 
            	            		"Assertion");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssertionSet"


    // $ANTLR start "entryRuleExtensionAssertionSet"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1433:1: entryRuleExtensionAssertionSet returns [EObject current=null] : iv_ruleExtensionAssertionSet= ruleExtensionAssertionSet EOF ;
    public final EObject entryRuleExtensionAssertionSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExtensionAssertionSet = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1434:2: (iv_ruleExtensionAssertionSet= ruleExtensionAssertionSet EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1435:2: iv_ruleExtensionAssertionSet= ruleExtensionAssertionSet EOF
            {
             newCompositeNode(grammarAccess.getExtensionAssertionSetRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleExtensionAssertionSet_in_entryRuleExtensionAssertionSet3021);
            iv_ruleExtensionAssertionSet=ruleExtensionAssertionSet();

            state._fsp--;

             current =iv_ruleExtensionAssertionSet; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleExtensionAssertionSet3031); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtensionAssertionSet"


    // $ANTLR start "ruleExtensionAssertionSet"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1442:1: ruleExtensionAssertionSet returns [EObject current=null] : ( () otherlv_1= 'extension' otherlv_2= 'rules:' ( (lv_assertions_3_0= ruleExtensionAssertion ) ) ( (lv_assertions_4_0= ruleExtensionAssertion ) )* ) ;
    public final EObject ruleExtensionAssertionSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_assertions_3_0 = null;

        EObject lv_assertions_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1445:28: ( ( () otherlv_1= 'extension' otherlv_2= 'rules:' ( (lv_assertions_3_0= ruleExtensionAssertion ) ) ( (lv_assertions_4_0= ruleExtensionAssertion ) )* ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1446:1: ( () otherlv_1= 'extension' otherlv_2= 'rules:' ( (lv_assertions_3_0= ruleExtensionAssertion ) ) ( (lv_assertions_4_0= ruleExtensionAssertion ) )* )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1446:1: ( () otherlv_1= 'extension' otherlv_2= 'rules:' ( (lv_assertions_3_0= ruleExtensionAssertion ) ) ( (lv_assertions_4_0= ruleExtensionAssertion ) )* )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1446:2: () otherlv_1= 'extension' otherlv_2= 'rules:' ( (lv_assertions_3_0= ruleExtensionAssertion ) ) ( (lv_assertions_4_0= ruleExtensionAssertion ) )*
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1446:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1447:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getExtensionAssertionSetAccess().getExtensionAssertionSetAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,32,FollowSets000.FOLLOW_32_in_ruleExtensionAssertionSet3077); 

                	newLeafNode(otherlv_1, grammarAccess.getExtensionAssertionSetAccess().getExtensionKeyword_1());
                
            otherlv_2=(Token)match(input,33,FollowSets000.FOLLOW_33_in_ruleExtensionAssertionSet3089); 

                	newLeafNode(otherlv_2, grammarAccess.getExtensionAssertionSetAccess().getRulesKeyword_2());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1460:1: ( (lv_assertions_3_0= ruleExtensionAssertion ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1461:1: (lv_assertions_3_0= ruleExtensionAssertion )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1461:1: (lv_assertions_3_0= ruleExtensionAssertion )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1462:3: lv_assertions_3_0= ruleExtensionAssertion
            {
             
            	        newCompositeNode(grammarAccess.getExtensionAssertionSetAccess().getAssertionsExtensionAssertionParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleExtensionAssertion_in_ruleExtensionAssertionSet3110);
            lv_assertions_3_0=ruleExtensionAssertion();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getExtensionAssertionSetRule());
            	        }
                   		add(
                   			current, 
                   			"assertions",
                    		lv_assertions_3_0, 
                    		"ExtensionAssertion");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1478:2: ( (lv_assertions_4_0= ruleExtensionAssertion ) )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( ((LA32_0>=RULE_STRING && LA32_0<=RULE_INT)||LA32_0==22||LA32_0==34||(LA32_0>=36 && LA32_0<=41)) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1479:1: (lv_assertions_4_0= ruleExtensionAssertion )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1479:1: (lv_assertions_4_0= ruleExtensionAssertion )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1480:3: lv_assertions_4_0= ruleExtensionAssertion
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getExtensionAssertionSetAccess().getAssertionsExtensionAssertionParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleExtensionAssertion_in_ruleExtensionAssertionSet3131);
            	    lv_assertions_4_0=ruleExtensionAssertion();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getExtensionAssertionSetRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"assertions",
            	            		lv_assertions_4_0, 
            	            		"ExtensionAssertion");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtensionAssertionSet"


    // $ANTLR start "entryRuleEIntegerObject"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1504:1: entryRuleEIntegerObject returns [String current=null] : iv_ruleEIntegerObject= ruleEIntegerObject EOF ;
    public final String entryRuleEIntegerObject() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEIntegerObject = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1505:2: (iv_ruleEIntegerObject= ruleEIntegerObject EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1506:2: iv_ruleEIntegerObject= ruleEIntegerObject EOF
            {
             newCompositeNode(grammarAccess.getEIntegerObjectRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_entryRuleEIntegerObject3169);
            iv_ruleEIntegerObject=ruleEIntegerObject();

            state._fsp--;

             current =iv_ruleEIntegerObject.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEIntegerObject3180); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEIntegerObject"


    // $ANTLR start "ruleEIntegerObject"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1513:1: ruleEIntegerObject returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEIntegerObject() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1516:28: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1517:1: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1517:1: ( (kw= '-' )? this_INT_1= RULE_INT )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1517:2: (kw= '-' )? this_INT_1= RULE_INT
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1517:2: (kw= '-' )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==34) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1518:2: kw= '-'
                    {
                    kw=(Token)match(input,34,FollowSets000.FOLLOW_34_in_ruleEIntegerObject3219); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getEIntegerObjectAccess().getHyphenMinusKeyword_0()); 
                        

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleEIntegerObject3236); 

            		current.merge(this_INT_1);
                
             
                newLeafNode(this_INT_1, grammarAccess.getEIntegerObjectAccess().getINTTerminalRuleCall_1()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEIntegerObject"


    // $ANTLR start "entryRuleAssertion"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1542:1: entryRuleAssertion returns [EObject current=null] : iv_ruleAssertion= ruleAssertion EOF ;
    public final EObject entryRuleAssertion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssertion = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1543:2: (iv_ruleAssertion= ruleAssertion EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1544:2: iv_ruleAssertion= ruleAssertion EOF
            {
             newCompositeNode(grammarAccess.getAssertionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAssertion_in_entryRuleAssertion3285);
            iv_ruleAssertion=ruleAssertion();

            state._fsp--;

             current =iv_ruleAssertion; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAssertion3295); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssertion"


    // $ANTLR start "ruleAssertion"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1551:1: ruleAssertion returns [EObject current=null] : ( () ( (lv_check_1_0= ruleCheck ) ) ) ;
    public final EObject ruleAssertion() throws RecognitionException {
        EObject current = null;

        EObject lv_check_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1554:28: ( ( () ( (lv_check_1_0= ruleCheck ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1555:1: ( () ( (lv_check_1_0= ruleCheck ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1555:1: ( () ( (lv_check_1_0= ruleCheck ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1555:2: () ( (lv_check_1_0= ruleCheck ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1555:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1556:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAssertionAccess().getAssertionAction_0(),
                        current);
                

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1561:2: ( (lv_check_1_0= ruleCheck ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1562:1: (lv_check_1_0= ruleCheck )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1562:1: (lv_check_1_0= ruleCheck )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1563:3: lv_check_1_0= ruleCheck
            {
             
            	        newCompositeNode(grammarAccess.getAssertionAccess().getCheckCheckParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleCheck_in_ruleAssertion3350);
            lv_check_1_0=ruleCheck();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAssertionRule());
            	        }
                   		set(
                   			current, 
                   			"check",
                    		lv_check_1_0, 
                    		"Check");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssertion"


    // $ANTLR start "entryRuleExtensionAssertion"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1587:1: entryRuleExtensionAssertion returns [EObject current=null] : iv_ruleExtensionAssertion= ruleExtensionAssertion EOF ;
    public final EObject entryRuleExtensionAssertion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExtensionAssertion = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1588:2: (iv_ruleExtensionAssertion= ruleExtensionAssertion EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1589:2: iv_ruleExtensionAssertion= ruleExtensionAssertion EOF
            {
             newCompositeNode(grammarAccess.getExtensionAssertionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleExtensionAssertion_in_entryRuleExtensionAssertion3386);
            iv_ruleExtensionAssertion=ruleExtensionAssertion();

            state._fsp--;

             current =iv_ruleExtensionAssertion; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleExtensionAssertion3396); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtensionAssertion"


    // $ANTLR start "ruleExtensionAssertion"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1596:1: ruleExtensionAssertion returns [EObject current=null] : (this_MetaClassExtensionAssertion_0= ruleMetaClassExtensionAssertion | this_FragmentObjectExtensionAssertion_1= ruleFragmentObjectExtensionAssertion ) ;
    public final EObject ruleExtensionAssertion() throws RecognitionException {
        EObject current = null;

        EObject this_MetaClassExtensionAssertion_0 = null;

        EObject this_FragmentObjectExtensionAssertion_1 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1599:28: ( (this_MetaClassExtensionAssertion_0= ruleMetaClassExtensionAssertion | this_FragmentObjectExtensionAssertion_1= ruleFragmentObjectExtensionAssertion ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1600:1: (this_MetaClassExtensionAssertion_0= ruleMetaClassExtensionAssertion | this_FragmentObjectExtensionAssertion_1= ruleFragmentObjectExtensionAssertion )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1600:1: (this_MetaClassExtensionAssertion_0= ruleMetaClassExtensionAssertion | this_FragmentObjectExtensionAssertion_1= ruleFragmentObjectExtensionAssertion )
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==RULE_INT||LA34_0==22||LA34_0==34||(LA34_0>=36 && LA34_0<=41)) ) {
                alt34=1;
            }
            else if ( ((LA34_0>=RULE_STRING && LA34_0<=RULE_ID)) ) {
                alt34=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }
            switch (alt34) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1601:5: this_MetaClassExtensionAssertion_0= ruleMetaClassExtensionAssertion
                    {
                     
                            newCompositeNode(grammarAccess.getExtensionAssertionAccess().getMetaClassExtensionAssertionParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleMetaClassExtensionAssertion_in_ruleExtensionAssertion3443);
                    this_MetaClassExtensionAssertion_0=ruleMetaClassExtensionAssertion();

                    state._fsp--;

                     
                            current = this_MetaClassExtensionAssertion_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1611:5: this_FragmentObjectExtensionAssertion_1= ruleFragmentObjectExtensionAssertion
                    {
                     
                            newCompositeNode(grammarAccess.getExtensionAssertionAccess().getFragmentObjectExtensionAssertionParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectExtensionAssertion_in_ruleExtensionAssertion3470);
                    this_FragmentObjectExtensionAssertion_1=ruleFragmentObjectExtensionAssertion();

                    state._fsp--;

                     
                            current = this_FragmentObjectExtensionAssertion_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtensionAssertion"


    // $ANTLR start "entryRuleFragmentObjectExtensionAssertion"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1627:1: entryRuleFragmentObjectExtensionAssertion returns [EObject current=null] : iv_ruleFragmentObjectExtensionAssertion= ruleFragmentObjectExtensionAssertion EOF ;
    public final EObject entryRuleFragmentObjectExtensionAssertion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragmentObjectExtensionAssertion = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1628:2: (iv_ruleFragmentObjectExtensionAssertion= ruleFragmentObjectExtensionAssertion EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1629:2: iv_ruleFragmentObjectExtensionAssertion= ruleFragmentObjectExtensionAssertion EOF
            {
             newCompositeNode(grammarAccess.getFragmentObjectExtensionAssertionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectExtensionAssertion_in_entryRuleFragmentObjectExtensionAssertion3505);
            iv_ruleFragmentObjectExtensionAssertion=ruleFragmentObjectExtensionAssertion();

            state._fsp--;

             current =iv_ruleFragmentObjectExtensionAssertion; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentObjectExtensionAssertion3515); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragmentObjectExtensionAssertion"


    // $ANTLR start "ruleFragmentObjectExtensionAssertion"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1636:1: ruleFragmentObjectExtensionAssertion returns [EObject current=null] : ( ( (lv_selector_0_0= ruleFragmentObjectSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleFragmentObjectQualifier ) ) )? otherlv_3= '.' ) ;
    public final EObject ruleFragmentObjectExtensionAssertion() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_selector_0_0 = null;

        EObject lv_condition_2_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1639:28: ( ( ( (lv_selector_0_0= ruleFragmentObjectSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleFragmentObjectQualifier ) ) )? otherlv_3= '.' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1640:1: ( ( (lv_selector_0_0= ruleFragmentObjectSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleFragmentObjectQualifier ) ) )? otherlv_3= '.' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1640:1: ( ( (lv_selector_0_0= ruleFragmentObjectSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleFragmentObjectQualifier ) ) )? otherlv_3= '.' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1640:2: ( (lv_selector_0_0= ruleFragmentObjectSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleFragmentObjectQualifier ) ) )? otherlv_3= '.'
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1640:2: ( (lv_selector_0_0= ruleFragmentObjectSelector ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1641:1: (lv_selector_0_0= ruleFragmentObjectSelector )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1641:1: (lv_selector_0_0= ruleFragmentObjectSelector )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1642:3: lv_selector_0_0= ruleFragmentObjectSelector
            {
             
            	        newCompositeNode(grammarAccess.getFragmentObjectExtensionAssertionAccess().getSelectorFragmentObjectSelectorParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectSelector_in_ruleFragmentObjectExtensionAssertion3561);
            lv_selector_0_0=ruleFragmentObjectSelector();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentObjectExtensionAssertionRule());
            	        }
                   		set(
                   			current, 
                   			"selector",
                    		lv_selector_0_0, 
                    		"FragmentObjectSelector");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1658:2: (otherlv_1= '=>' ( (lv_condition_2_0= ruleFragmentObjectQualifier ) ) )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==35) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1658:4: otherlv_1= '=>' ( (lv_condition_2_0= ruleFragmentObjectQualifier ) )
                    {
                    otherlv_1=(Token)match(input,35,FollowSets000.FOLLOW_35_in_ruleFragmentObjectExtensionAssertion3574); 

                        	newLeafNode(otherlv_1, grammarAccess.getFragmentObjectExtensionAssertionAccess().getEqualsSignGreaterThanSignKeyword_1_0());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1662:1: ( (lv_condition_2_0= ruleFragmentObjectQualifier ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1663:1: (lv_condition_2_0= ruleFragmentObjectQualifier )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1663:1: (lv_condition_2_0= ruleFragmentObjectQualifier )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1664:3: lv_condition_2_0= ruleFragmentObjectQualifier
                    {
                     
                    	        newCompositeNode(grammarAccess.getFragmentObjectExtensionAssertionAccess().getConditionFragmentObjectQualifierParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectExtensionAssertion3595);
                    lv_condition_2_0=ruleFragmentObjectQualifier();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFragmentObjectExtensionAssertionRule());
                    	        }
                           		set(
                           			current, 
                           			"condition",
                            		lv_condition_2_0, 
                            		"FragmentObjectQualifier");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleFragmentObjectExtensionAssertion3609); 

                	newLeafNode(otherlv_3, grammarAccess.getFragmentObjectExtensionAssertionAccess().getFullStopKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentObjectExtensionAssertion"


    // $ANTLR start "entryRuleMetaClassExtensionAssertion"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1692:1: entryRuleMetaClassExtensionAssertion returns [EObject current=null] : iv_ruleMetaClassExtensionAssertion= ruleMetaClassExtensionAssertion EOF ;
    public final EObject entryRuleMetaClassExtensionAssertion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMetaClassExtensionAssertion = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1693:2: (iv_ruleMetaClassExtensionAssertion= ruleMetaClassExtensionAssertion EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1694:2: iv_ruleMetaClassExtensionAssertion= ruleMetaClassExtensionAssertion EOF
            {
             newCompositeNode(grammarAccess.getMetaClassExtensionAssertionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassExtensionAssertion_in_entryRuleMetaClassExtensionAssertion3645);
            iv_ruleMetaClassExtensionAssertion=ruleMetaClassExtensionAssertion();

            state._fsp--;

             current =iv_ruleMetaClassExtensionAssertion; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaClassExtensionAssertion3655); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMetaClassExtensionAssertion"


    // $ANTLR start "ruleMetaClassExtensionAssertion"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1701:1: ruleMetaClassExtensionAssertion returns [EObject current=null] : ( ( (lv_selector_0_0= ruleMetaClassSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleMetaClassQualifier ) ) )? otherlv_3= '.' ) ;
    public final EObject ruleMetaClassExtensionAssertion() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_selector_0_0 = null;

        EObject lv_condition_2_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1704:28: ( ( ( (lv_selector_0_0= ruleMetaClassSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleMetaClassQualifier ) ) )? otherlv_3= '.' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1705:1: ( ( (lv_selector_0_0= ruleMetaClassSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleMetaClassQualifier ) ) )? otherlv_3= '.' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1705:1: ( ( (lv_selector_0_0= ruleMetaClassSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleMetaClassQualifier ) ) )? otherlv_3= '.' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1705:2: ( (lv_selector_0_0= ruleMetaClassSelector ) ) (otherlv_1= '=>' ( (lv_condition_2_0= ruleMetaClassQualifier ) ) )? otherlv_3= '.'
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1705:2: ( (lv_selector_0_0= ruleMetaClassSelector ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1706:1: (lv_selector_0_0= ruleMetaClassSelector )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1706:1: (lv_selector_0_0= ruleMetaClassSelector )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1707:3: lv_selector_0_0= ruleMetaClassSelector
            {
             
            	        newCompositeNode(grammarAccess.getMetaClassExtensionAssertionAccess().getSelectorMetaClassSelectorParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassSelector_in_ruleMetaClassExtensionAssertion3701);
            lv_selector_0_0=ruleMetaClassSelector();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMetaClassExtensionAssertionRule());
            	        }
                   		set(
                   			current, 
                   			"selector",
                    		lv_selector_0_0, 
                    		"MetaClassSelector");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1723:2: (otherlv_1= '=>' ( (lv_condition_2_0= ruleMetaClassQualifier ) ) )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==35) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1723:4: otherlv_1= '=>' ( (lv_condition_2_0= ruleMetaClassQualifier ) )
                    {
                    otherlv_1=(Token)match(input,35,FollowSets000.FOLLOW_35_in_ruleMetaClassExtensionAssertion3714); 

                        	newLeafNode(otherlv_1, grammarAccess.getMetaClassExtensionAssertionAccess().getEqualsSignGreaterThanSignKeyword_1_0());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1727:1: ( (lv_condition_2_0= ruleMetaClassQualifier ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1728:1: (lv_condition_2_0= ruleMetaClassQualifier )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1728:1: (lv_condition_2_0= ruleMetaClassQualifier )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1729:3: lv_condition_2_0= ruleMetaClassQualifier
                    {
                     
                    	        newCompositeNode(grammarAccess.getMetaClassExtensionAssertionAccess().getConditionMetaClassQualifierParserRuleCall_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassExtensionAssertion3735);
                    lv_condition_2_0=ruleMetaClassQualifier();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMetaClassExtensionAssertionRule());
                    	        }
                           		set(
                           			current, 
                           			"condition",
                            		lv_condition_2_0, 
                            		"MetaClassQualifier");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleMetaClassExtensionAssertion3749); 

                	newLeafNode(otherlv_3, grammarAccess.getMetaClassExtensionAssertionAccess().getFullStopKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMetaClassExtensionAssertion"


    // $ANTLR start "entryRuleQuantifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1757:1: entryRuleQuantifier returns [EObject current=null] : iv_ruleQuantifier= ruleQuantifier EOF ;
    public final EObject entryRuleQuantifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuantifier = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1758:2: (iv_ruleQuantifier= ruleQuantifier EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1759:2: iv_ruleQuantifier= ruleQuantifier EOF
            {
             newCompositeNode(grammarAccess.getQuantifierRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleQuantifier_in_entryRuleQuantifier3785);
            iv_ruleQuantifier=ruleQuantifier();

            state._fsp--;

             current =iv_ruleQuantifier; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleQuantifier3795); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuantifier"


    // $ANTLR start "ruleQuantifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1766:1: ruleQuantifier returns [EObject current=null] : (this_None_0= ruleNone | this_Some_1= ruleSome | this_Every_2= ruleEvery | this_Number_3= ruleNumber ) ;
    public final EObject ruleQuantifier() throws RecognitionException {
        EObject current = null;

        EObject this_None_0 = null;

        EObject this_Some_1 = null;

        EObject this_Every_2 = null;

        EObject this_Number_3 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1769:28: ( (this_None_0= ruleNone | this_Some_1= ruleSome | this_Every_2= ruleEvery | this_Number_3= ruleNumber ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1770:1: (this_None_0= ruleNone | this_Some_1= ruleSome | this_Every_2= ruleEvery | this_Number_3= ruleNumber )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1770:1: (this_None_0= ruleNone | this_Some_1= ruleSome | this_Every_2= ruleEvery | this_Number_3= ruleNumber )
            int alt37=4;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt37=1;
                }
                break;
            case 37:
                {
                alt37=2;
                }
                break;
            case 38:
            case 39:
                {
                alt37=3;
                }
                break;
            case RULE_INT:
            case 22:
            case 34:
            case 40:
            case 41:
                {
                alt37=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }

            switch (alt37) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1771:5: this_None_0= ruleNone
                    {
                     
                            newCompositeNode(grammarAccess.getQuantifierAccess().getNoneParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleNone_in_ruleQuantifier3842);
                    this_None_0=ruleNone();

                    state._fsp--;

                     
                            current = this_None_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1781:5: this_Some_1= ruleSome
                    {
                     
                            newCompositeNode(grammarAccess.getQuantifierAccess().getSomeParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleSome_in_ruleQuantifier3869);
                    this_Some_1=ruleSome();

                    state._fsp--;

                     
                            current = this_Some_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1791:5: this_Every_2= ruleEvery
                    {
                     
                            newCompositeNode(grammarAccess.getQuantifierAccess().getEveryParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleEvery_in_ruleQuantifier3896);
                    this_Every_2=ruleEvery();

                    state._fsp--;

                     
                            current = this_Every_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1801:5: this_Number_3= ruleNumber
                    {
                     
                            newCompositeNode(grammarAccess.getQuantifierAccess().getNumberParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleNumber_in_ruleQuantifier3923);
                    this_Number_3=ruleNumber();

                    state._fsp--;

                     
                            current = this_Number_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuantifier"


    // $ANTLR start "entryRuleNone"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1817:1: entryRuleNone returns [EObject current=null] : iv_ruleNone= ruleNone EOF ;
    public final EObject entryRuleNone() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNone = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1818:2: (iv_ruleNone= ruleNone EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1819:2: iv_ruleNone= ruleNone EOF
            {
             newCompositeNode(grammarAccess.getNoneRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleNone_in_entryRuleNone3958);
            iv_ruleNone=ruleNone();

            state._fsp--;

             current =iv_ruleNone; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleNone3968); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNone"


    // $ANTLR start "ruleNone"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1826:1: ruleNone returns [EObject current=null] : ( () otherlv_1= 'no' ) ;
    public final EObject ruleNone() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1829:28: ( ( () otherlv_1= 'no' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1830:1: ( () otherlv_1= 'no' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1830:1: ( () otherlv_1= 'no' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1830:2: () otherlv_1= 'no'
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1830:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1831:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getNoneAccess().getNoneAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,36,FollowSets000.FOLLOW_36_in_ruleNone4014); 

                	newLeafNode(otherlv_1, grammarAccess.getNoneAccess().getNoKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNone"


    // $ANTLR start "entryRuleSome"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1848:1: entryRuleSome returns [EObject current=null] : iv_ruleSome= ruleSome EOF ;
    public final EObject entryRuleSome() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSome = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1849:2: (iv_ruleSome= ruleSome EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1850:2: iv_ruleSome= ruleSome EOF
            {
             newCompositeNode(grammarAccess.getSomeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleSome_in_entryRuleSome4050);
            iv_ruleSome=ruleSome();

            state._fsp--;

             current =iv_ruleSome; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleSome4060); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSome"


    // $ANTLR start "ruleSome"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1857:1: ruleSome returns [EObject current=null] : ( () otherlv_1= 'some' ) ;
    public final EObject ruleSome() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1860:28: ( ( () otherlv_1= 'some' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1861:1: ( () otherlv_1= 'some' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1861:1: ( () otherlv_1= 'some' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1861:2: () otherlv_1= 'some'
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1861:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1862:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getSomeAccess().getSomeAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_37_in_ruleSome4106); 

                	newLeafNode(otherlv_1, grammarAccess.getSomeAccess().getSomeKeyword_1());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSome"


    // $ANTLR start "entryRuleEvery"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1879:1: entryRuleEvery returns [EObject current=null] : iv_ruleEvery= ruleEvery EOF ;
    public final EObject entryRuleEvery() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvery = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1880:2: (iv_ruleEvery= ruleEvery EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1881:2: iv_ruleEvery= ruleEvery EOF
            {
             newCompositeNode(grammarAccess.getEveryRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEvery_in_entryRuleEvery4142);
            iv_ruleEvery=ruleEvery();

            state._fsp--;

             current =iv_ruleEvery; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEvery4152); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvery"


    // $ANTLR start "ruleEvery"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1888:1: ruleEvery returns [EObject current=null] : ( () ( (lv_not_1_0= 'not' ) )? otherlv_2= 'every' ) ;
    public final EObject ruleEvery() throws RecognitionException {
        EObject current = null;

        Token lv_not_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1891:28: ( ( () ( (lv_not_1_0= 'not' ) )? otherlv_2= 'every' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1892:1: ( () ( (lv_not_1_0= 'not' ) )? otherlv_2= 'every' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1892:1: ( () ( (lv_not_1_0= 'not' ) )? otherlv_2= 'every' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1892:2: () ( (lv_not_1_0= 'not' ) )? otherlv_2= 'every'
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1892:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1893:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getEveryAccess().getEveryAction_0(),
                        current);
                

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1898:2: ( (lv_not_1_0= 'not' ) )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==38) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1899:1: (lv_not_1_0= 'not' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1899:1: (lv_not_1_0= 'not' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1900:3: lv_not_1_0= 'not'
                    {
                    lv_not_1_0=(Token)match(input,38,FollowSets000.FOLLOW_38_in_ruleEvery4204); 

                            newLeafNode(lv_not_1_0, grammarAccess.getEveryAccess().getNotNotKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getEveryRule());
                    	        }
                           		setWithLastConsumed(current, "not", true, "not");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,39,FollowSets000.FOLLOW_39_in_ruleEvery4230); 

                	newLeafNode(otherlv_2, grammarAccess.getEveryAccess().getEveryKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvery"


    // $ANTLR start "entryRuleNumber"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1925:1: entryRuleNumber returns [EObject current=null] : iv_ruleNumber= ruleNumber EOF ;
    public final EObject entryRuleNumber() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumber = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1926:2: (iv_ruleNumber= ruleNumber EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1927:2: iv_ruleNumber= ruleNumber EOF
            {
             newCompositeNode(grammarAccess.getNumberRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleNumber_in_entryRuleNumber4266);
            iv_ruleNumber=ruleNumber();

            state._fsp--;

             current =iv_ruleNumber; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleNumber4276); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumber"


    // $ANTLR start "ruleNumber"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1934:1: ruleNumber returns [EObject current=null] : (this_Number_Impl_0= ruleNumber_Impl | this_StrictNumber_1= ruleStrictNumber | this_MoreThan_2= ruleMoreThan ) ;
    public final EObject ruleNumber() throws RecognitionException {
        EObject current = null;

        EObject this_Number_Impl_0 = null;

        EObject this_StrictNumber_1 = null;

        EObject this_MoreThan_2 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1937:28: ( (this_Number_Impl_0= ruleNumber_Impl | this_StrictNumber_1= ruleStrictNumber | this_MoreThan_2= ruleMoreThan ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1938:1: (this_Number_Impl_0= ruleNumber_Impl | this_StrictNumber_1= ruleStrictNumber | this_MoreThan_2= ruleMoreThan )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1938:1: (this_Number_Impl_0= ruleNumber_Impl | this_StrictNumber_1= ruleStrictNumber | this_MoreThan_2= ruleMoreThan )
            int alt39=3;
            switch ( input.LA(1) ) {
            case RULE_INT:
            case 34:
                {
                alt39=1;
                }
                break;
            case 40:
                {
                alt39=2;
                }
                break;
            case 22:
            case 41:
                {
                alt39=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;
            }

            switch (alt39) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1939:5: this_Number_Impl_0= ruleNumber_Impl
                    {
                     
                            newCompositeNode(grammarAccess.getNumberAccess().getNumber_ImplParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleNumber_Impl_in_ruleNumber4323);
                    this_Number_Impl_0=ruleNumber_Impl();

                    state._fsp--;

                     
                            current = this_Number_Impl_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1949:5: this_StrictNumber_1= ruleStrictNumber
                    {
                     
                            newCompositeNode(grammarAccess.getNumberAccess().getStrictNumberParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleStrictNumber_in_ruleNumber4350);
                    this_StrictNumber_1=ruleStrictNumber();

                    state._fsp--;

                     
                            current = this_StrictNumber_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1959:5: this_MoreThan_2= ruleMoreThan
                    {
                     
                            newCompositeNode(grammarAccess.getNumberAccess().getMoreThanParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleMoreThan_in_ruleNumber4377);
                    this_MoreThan_2=ruleMoreThan();

                    state._fsp--;

                     
                            current = this_MoreThan_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumber"


    // $ANTLR start "entryRuleNumber_Impl"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1975:1: entryRuleNumber_Impl returns [EObject current=null] : iv_ruleNumber_Impl= ruleNumber_Impl EOF ;
    public final EObject entryRuleNumber_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumber_Impl = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1976:2: (iv_ruleNumber_Impl= ruleNumber_Impl EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1977:2: iv_ruleNumber_Impl= ruleNumber_Impl EOF
            {
             newCompositeNode(grammarAccess.getNumber_ImplRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleNumber_Impl_in_entryRuleNumber_Impl4412);
            iv_ruleNumber_Impl=ruleNumber_Impl();

            state._fsp--;

             current =iv_ruleNumber_Impl; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleNumber_Impl4422); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumber_Impl"


    // $ANTLR start "ruleNumber_Impl"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1984:1: ruleNumber_Impl returns [EObject current=null] : ( (lv_n_0_0= ruleIntegerParameter ) ) ;
    public final EObject ruleNumber_Impl() throws RecognitionException {
        EObject current = null;

        EObject lv_n_0_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1987:28: ( ( (lv_n_0_0= ruleIntegerParameter ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1988:1: ( (lv_n_0_0= ruleIntegerParameter ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1988:1: ( (lv_n_0_0= ruleIntegerParameter ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1989:1: (lv_n_0_0= ruleIntegerParameter )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1989:1: (lv_n_0_0= ruleIntegerParameter )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:1990:3: lv_n_0_0= ruleIntegerParameter
            {
             
            	        newCompositeNode(grammarAccess.getNumber_ImplAccess().getNIntegerParameterParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleIntegerParameter_in_ruleNumber_Impl4467);
            lv_n_0_0=ruleIntegerParameter();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getNumber_ImplRule());
            	        }
                   		set(
                   			current, 
                   			"n",
                    		lv_n_0_0, 
                    		"IntegerParameter");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumber_Impl"


    // $ANTLR start "entryRuleStrictNumber"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2014:1: entryRuleStrictNumber returns [EObject current=null] : iv_ruleStrictNumber= ruleStrictNumber EOF ;
    public final EObject entryRuleStrictNumber() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStrictNumber = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2015:2: (iv_ruleStrictNumber= ruleStrictNumber EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2016:2: iv_ruleStrictNumber= ruleStrictNumber EOF
            {
             newCompositeNode(grammarAccess.getStrictNumberRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleStrictNumber_in_entryRuleStrictNumber4502);
            iv_ruleStrictNumber=ruleStrictNumber();

            state._fsp--;

             current =iv_ruleStrictNumber; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStrictNumber4512); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStrictNumber"


    // $ANTLR start "ruleStrictNumber"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2023:1: ruleStrictNumber returns [EObject current=null] : ( () otherlv_1= 'strictly' ( (lv_n_2_0= ruleIntegerParameter ) ) ) ;
    public final EObject ruleStrictNumber() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_n_2_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2026:28: ( ( () otherlv_1= 'strictly' ( (lv_n_2_0= ruleIntegerParameter ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2027:1: ( () otherlv_1= 'strictly' ( (lv_n_2_0= ruleIntegerParameter ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2027:1: ( () otherlv_1= 'strictly' ( (lv_n_2_0= ruleIntegerParameter ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2027:2: () otherlv_1= 'strictly' ( (lv_n_2_0= ruleIntegerParameter ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2027:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2028:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getStrictNumberAccess().getStrictNumberAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,40,FollowSets000.FOLLOW_40_in_ruleStrictNumber4558); 

                	newLeafNode(otherlv_1, grammarAccess.getStrictNumberAccess().getStrictlyKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2037:1: ( (lv_n_2_0= ruleIntegerParameter ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2038:1: (lv_n_2_0= ruleIntegerParameter )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2038:1: (lv_n_2_0= ruleIntegerParameter )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2039:3: lv_n_2_0= ruleIntegerParameter
            {
             
            	        newCompositeNode(grammarAccess.getStrictNumberAccess().getNIntegerParameterParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleIntegerParameter_in_ruleStrictNumber4579);
            lv_n_2_0=ruleIntegerParameter();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getStrictNumberRule());
            	        }
                   		set(
                   			current, 
                   			"n",
                    		lv_n_2_0, 
                    		"IntegerParameter");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStrictNumber"


    // $ANTLR start "entryRuleMoreThan"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2063:1: entryRuleMoreThan returns [EObject current=null] : iv_ruleMoreThan= ruleMoreThan EOF ;
    public final EObject entryRuleMoreThan() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMoreThan = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2064:2: (iv_ruleMoreThan= ruleMoreThan EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2065:2: iv_ruleMoreThan= ruleMoreThan EOF
            {
             newCompositeNode(grammarAccess.getMoreThanRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMoreThan_in_entryRuleMoreThan4615);
            iv_ruleMoreThan=ruleMoreThan();

            state._fsp--;

             current =iv_ruleMoreThan; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMoreThan4625); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMoreThan"


    // $ANTLR start "ruleMoreThan"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2072:1: ruleMoreThan returns [EObject current=null] : ( () ( (otherlv_1= '(' | ( (lv_orEqual_2_0= '[' ) ) ) ( (lv_n_3_0= ruleIntegerParameter ) ) otherlv_4= ',' ( ( (lv_and_5_0= ruleLessThan ) ) | otherlv_6= '*)' ) ) ) ;
    public final EObject ruleMoreThan() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_orEqual_2_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_n_3_0 = null;

        EObject lv_and_5_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2075:28: ( ( () ( (otherlv_1= '(' | ( (lv_orEqual_2_0= '[' ) ) ) ( (lv_n_3_0= ruleIntegerParameter ) ) otherlv_4= ',' ( ( (lv_and_5_0= ruleLessThan ) ) | otherlv_6= '*)' ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2076:1: ( () ( (otherlv_1= '(' | ( (lv_orEqual_2_0= '[' ) ) ) ( (lv_n_3_0= ruleIntegerParameter ) ) otherlv_4= ',' ( ( (lv_and_5_0= ruleLessThan ) ) | otherlv_6= '*)' ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2076:1: ( () ( (otherlv_1= '(' | ( (lv_orEqual_2_0= '[' ) ) ) ( (lv_n_3_0= ruleIntegerParameter ) ) otherlv_4= ',' ( ( (lv_and_5_0= ruleLessThan ) ) | otherlv_6= '*)' ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2076:2: () ( (otherlv_1= '(' | ( (lv_orEqual_2_0= '[' ) ) ) ( (lv_n_3_0= ruleIntegerParameter ) ) otherlv_4= ',' ( ( (lv_and_5_0= ruleLessThan ) ) | otherlv_6= '*)' ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2076:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2077:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getMoreThanAccess().getMoreThanAction_0(),
                        current);
                

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2082:2: ( (otherlv_1= '(' | ( (lv_orEqual_2_0= '[' ) ) ) ( (lv_n_3_0= ruleIntegerParameter ) ) otherlv_4= ',' ( ( (lv_and_5_0= ruleLessThan ) ) | otherlv_6= '*)' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2082:3: (otherlv_1= '(' | ( (lv_orEqual_2_0= '[' ) ) ) ( (lv_n_3_0= ruleIntegerParameter ) ) otherlv_4= ',' ( ( (lv_and_5_0= ruleLessThan ) ) | otherlv_6= '*)' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2082:3: (otherlv_1= '(' | ( (lv_orEqual_2_0= '[' ) ) )
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==22) ) {
                alt40=1;
            }
            else if ( (LA40_0==41) ) {
                alt40=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;
            }
            switch (alt40) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2082:5: otherlv_1= '('
                    {
                    otherlv_1=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleMoreThan4673); 

                        	newLeafNode(otherlv_1, grammarAccess.getMoreThanAccess().getLeftParenthesisKeyword_1_0_0());
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2087:6: ( (lv_orEqual_2_0= '[' ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2087:6: ( (lv_orEqual_2_0= '[' ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2088:1: (lv_orEqual_2_0= '[' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2088:1: (lv_orEqual_2_0= '[' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2089:3: lv_orEqual_2_0= '['
                    {
                    lv_orEqual_2_0=(Token)match(input,41,FollowSets000.FOLLOW_41_in_ruleMoreThan4697); 

                            newLeafNode(lv_orEqual_2_0, grammarAccess.getMoreThanAccess().getOrEqualLeftSquareBracketKeyword_1_0_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMoreThanRule());
                    	        }
                           		setWithLastConsumed(current, "orEqual", true, "[");
                    	    

                    }


                    }


                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2102:3: ( (lv_n_3_0= ruleIntegerParameter ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2103:1: (lv_n_3_0= ruleIntegerParameter )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2103:1: (lv_n_3_0= ruleIntegerParameter )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2104:3: lv_n_3_0= ruleIntegerParameter
            {
             
            	        newCompositeNode(grammarAccess.getMoreThanAccess().getNIntegerParameterParserRuleCall_1_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleIntegerParameter_in_ruleMoreThan4732);
            lv_n_3_0=ruleIntegerParameter();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMoreThanRule());
            	        }
                   		set(
                   			current, 
                   			"n",
                    		lv_n_3_0, 
                    		"IntegerParameter");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleMoreThan4744); 

                	newLeafNode(otherlv_4, grammarAccess.getMoreThanAccess().getCommaKeyword_1_2());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2124:1: ( ( (lv_and_5_0= ruleLessThan ) ) | otherlv_6= '*)' )
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==RULE_INT||LA41_0==34) ) {
                alt41=1;
            }
            else if ( (LA41_0==42) ) {
                alt41=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 41, 0, input);

                throw nvae;
            }
            switch (alt41) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2124:2: ( (lv_and_5_0= ruleLessThan ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2124:2: ( (lv_and_5_0= ruleLessThan ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2125:1: (lv_and_5_0= ruleLessThan )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2125:1: (lv_and_5_0= ruleLessThan )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2126:3: lv_and_5_0= ruleLessThan
                    {
                     
                    	        newCompositeNode(grammarAccess.getMoreThanAccess().getAndLessThanParserRuleCall_1_3_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleLessThan_in_ruleMoreThan4766);
                    lv_and_5_0=ruleLessThan();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMoreThanRule());
                    	        }
                           		set(
                           			current, 
                           			"and",
                            		lv_and_5_0, 
                            		"LessThan");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2143:7: otherlv_6= '*)'
                    {
                    otherlv_6=(Token)match(input,42,FollowSets000.FOLLOW_42_in_ruleMoreThan4784); 

                        	newLeafNode(otherlv_6, grammarAccess.getMoreThanAccess().getAsteriskRightParenthesisKeyword_1_3_1());
                        

                    }
                    break;

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMoreThan"


    // $ANTLR start "entryRuleLessThan"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2155:1: entryRuleLessThan returns [EObject current=null] : iv_ruleLessThan= ruleLessThan EOF ;
    public final EObject entryRuleLessThan() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLessThan = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2156:2: (iv_ruleLessThan= ruleLessThan EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2157:2: iv_ruleLessThan= ruleLessThan EOF
            {
             newCompositeNode(grammarAccess.getLessThanRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleLessThan_in_entryRuleLessThan4822);
            iv_ruleLessThan=ruleLessThan();

            state._fsp--;

             current =iv_ruleLessThan; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleLessThan4832); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLessThan"


    // $ANTLR start "ruleLessThan"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2164:1: ruleLessThan returns [EObject current=null] : ( () ( (lv_n_1_0= ruleIntegerParameter ) ) (otherlv_2= ')' | ( (lv_orEqual_3_0= ']' ) ) ) ) ;
    public final EObject ruleLessThan() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_orEqual_3_0=null;
        EObject lv_n_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2167:28: ( ( () ( (lv_n_1_0= ruleIntegerParameter ) ) (otherlv_2= ')' | ( (lv_orEqual_3_0= ']' ) ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2168:1: ( () ( (lv_n_1_0= ruleIntegerParameter ) ) (otherlv_2= ')' | ( (lv_orEqual_3_0= ']' ) ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2168:1: ( () ( (lv_n_1_0= ruleIntegerParameter ) ) (otherlv_2= ')' | ( (lv_orEqual_3_0= ']' ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2168:2: () ( (lv_n_1_0= ruleIntegerParameter ) ) (otherlv_2= ')' | ( (lv_orEqual_3_0= ']' ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2168:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2169:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getLessThanAccess().getLessThanAction_0(),
                        current);
                

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2174:2: ( (lv_n_1_0= ruleIntegerParameter ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2175:1: (lv_n_1_0= ruleIntegerParameter )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2175:1: (lv_n_1_0= ruleIntegerParameter )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2176:3: lv_n_1_0= ruleIntegerParameter
            {
             
            	        newCompositeNode(grammarAccess.getLessThanAccess().getNIntegerParameterParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleIntegerParameter_in_ruleLessThan4887);
            lv_n_1_0=ruleIntegerParameter();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getLessThanRule());
            	        }
                   		set(
                   			current, 
                   			"n",
                    		lv_n_1_0, 
                    		"IntegerParameter");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2192:2: (otherlv_2= ')' | ( (lv_orEqual_3_0= ']' ) ) )
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==23) ) {
                alt42=1;
            }
            else if ( (LA42_0==43) ) {
                alt42=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 42, 0, input);

                throw nvae;
            }
            switch (alt42) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2192:4: otherlv_2= ')'
                    {
                    otherlv_2=(Token)match(input,23,FollowSets000.FOLLOW_23_in_ruleLessThan4900); 

                        	newLeafNode(otherlv_2, grammarAccess.getLessThanAccess().getRightParenthesisKeyword_2_0());
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2197:6: ( (lv_orEqual_3_0= ']' ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2197:6: ( (lv_orEqual_3_0= ']' ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2198:1: (lv_orEqual_3_0= ']' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2198:1: (lv_orEqual_3_0= ']' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2199:3: lv_orEqual_3_0= ']'
                    {
                    lv_orEqual_3_0=(Token)match(input,43,FollowSets000.FOLLOW_43_in_ruleLessThan4924); 

                            newLeafNode(lv_orEqual_3_0, grammarAccess.getLessThanAccess().getOrEqualRightSquareBracketKeyword_2_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getLessThanRule());
                    	        }
                           		setWithLastConsumed(current, "orEqual", true, "]");
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLessThan"


    // $ANTLR start "entryRuleIntegerParameter"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2220:1: entryRuleIntegerParameter returns [EObject current=null] : iv_ruleIntegerParameter= ruleIntegerParameter EOF ;
    public final EObject entryRuleIntegerParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerParameter = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2221:2: (iv_ruleIntegerParameter= ruleIntegerParameter EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2222:2: iv_ruleIntegerParameter= ruleIntegerParameter EOF
            {
             newCompositeNode(grammarAccess.getIntegerParameterRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIntegerParameter_in_entryRuleIntegerParameter4974);
            iv_ruleIntegerParameter=ruleIntegerParameter();

            state._fsp--;

             current =iv_ruleIntegerParameter; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntegerParameter4984); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerParameter"


    // $ANTLR start "ruleIntegerParameter"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2229:1: ruleIntegerParameter returns [EObject current=null] : ( (lv_value_0_0= ruleEIntegerObject ) ) ;
    public final EObject ruleIntegerParameter() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2232:28: ( ( (lv_value_0_0= ruleEIntegerObject ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2233:1: ( (lv_value_0_0= ruleEIntegerObject ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2233:1: ( (lv_value_0_0= ruleEIntegerObject ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2234:1: (lv_value_0_0= ruleEIntegerObject )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2234:1: (lv_value_0_0= ruleEIntegerObject )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2235:3: lv_value_0_0= ruleEIntegerObject
            {
             
            	        newCompositeNode(grammarAccess.getIntegerParameterAccess().getValueEIntegerObjectParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleIntegerParameter5029);
            lv_value_0_0=ruleEIntegerObject();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getIntegerParameterRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"EIntegerObject");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerParameter"


    // $ANTLR start "entryRuleMetaClassSelector"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2259:1: entryRuleMetaClassSelector returns [EObject current=null] : iv_ruleMetaClassSelector= ruleMetaClassSelector EOF ;
    public final EObject entryRuleMetaClassSelector() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMetaClassSelector = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2260:2: (iv_ruleMetaClassSelector= ruleMetaClassSelector EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2261:2: iv_ruleMetaClassSelector= ruleMetaClassSelector EOF
            {
             newCompositeNode(grammarAccess.getMetaClassSelectorRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassSelector_in_entryRuleMetaClassSelector5064);
            iv_ruleMetaClassSelector=ruleMetaClassSelector();

            state._fsp--;

             current =iv_ruleMetaClassSelector; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaClassSelector5074); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMetaClassSelector"


    // $ANTLR start "ruleMetaClassSelector"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2268:1: ruleMetaClassSelector returns [EObject current=null] : ( ( (lv_quantifier_0_0= ruleQuantifier ) ) ( (lv_onlyNewElements_1_0= 'new' ) )? ( (lv_metaclassName_2_0= ruleEString ) ) (otherlv_3= '{' ( (lv_filter_4_0= ruleMetaClassQualifier ) ) otherlv_5= '}' )? ) ;
    public final EObject ruleMetaClassSelector() throws RecognitionException {
        EObject current = null;

        Token lv_onlyNewElements_1_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_quantifier_0_0 = null;

        AntlrDatatypeRuleToken lv_metaclassName_2_0 = null;

        EObject lv_filter_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2271:28: ( ( ( (lv_quantifier_0_0= ruleQuantifier ) ) ( (lv_onlyNewElements_1_0= 'new' ) )? ( (lv_metaclassName_2_0= ruleEString ) ) (otherlv_3= '{' ( (lv_filter_4_0= ruleMetaClassQualifier ) ) otherlv_5= '}' )? ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2272:1: ( ( (lv_quantifier_0_0= ruleQuantifier ) ) ( (lv_onlyNewElements_1_0= 'new' ) )? ( (lv_metaclassName_2_0= ruleEString ) ) (otherlv_3= '{' ( (lv_filter_4_0= ruleMetaClassQualifier ) ) otherlv_5= '}' )? )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2272:1: ( ( (lv_quantifier_0_0= ruleQuantifier ) ) ( (lv_onlyNewElements_1_0= 'new' ) )? ( (lv_metaclassName_2_0= ruleEString ) ) (otherlv_3= '{' ( (lv_filter_4_0= ruleMetaClassQualifier ) ) otherlv_5= '}' )? )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2272:2: ( (lv_quantifier_0_0= ruleQuantifier ) ) ( (lv_onlyNewElements_1_0= 'new' ) )? ( (lv_metaclassName_2_0= ruleEString ) ) (otherlv_3= '{' ( (lv_filter_4_0= ruleMetaClassQualifier ) ) otherlv_5= '}' )?
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2272:2: ( (lv_quantifier_0_0= ruleQuantifier ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2273:1: (lv_quantifier_0_0= ruleQuantifier )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2273:1: (lv_quantifier_0_0= ruleQuantifier )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2274:3: lv_quantifier_0_0= ruleQuantifier
            {
             
            	        newCompositeNode(grammarAccess.getMetaClassSelectorAccess().getQuantifierQuantifierParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleQuantifier_in_ruleMetaClassSelector5120);
            lv_quantifier_0_0=ruleQuantifier();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMetaClassSelectorRule());
            	        }
                   		set(
                   			current, 
                   			"quantifier",
                    		lv_quantifier_0_0, 
                    		"Quantifier");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2290:2: ( (lv_onlyNewElements_1_0= 'new' ) )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==44) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2291:1: (lv_onlyNewElements_1_0= 'new' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2291:1: (lv_onlyNewElements_1_0= 'new' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2292:3: lv_onlyNewElements_1_0= 'new'
                    {
                    lv_onlyNewElements_1_0=(Token)match(input,44,FollowSets000.FOLLOW_44_in_ruleMetaClassSelector5138); 

                            newLeafNode(lv_onlyNewElements_1_0, grammarAccess.getMetaClassSelectorAccess().getOnlyNewElementsNewKeyword_1_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getMetaClassSelectorRule());
                    	        }
                           		setWithLastConsumed(current, "onlyNewElements", true, "new");
                    	    

                    }


                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2305:3: ( (lv_metaclassName_2_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2306:1: (lv_metaclassName_2_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2306:1: (lv_metaclassName_2_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2307:3: lv_metaclassName_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getMetaClassSelectorAccess().getMetaclassNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleMetaClassSelector5173);
            lv_metaclassName_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMetaClassSelectorRule());
            	        }
                   		set(
                   			current, 
                   			"metaclassName",
                    		lv_metaclassName_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2323:2: (otherlv_3= '{' ( (lv_filter_4_0= ruleMetaClassQualifier ) ) otherlv_5= '}' )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==12) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2323:4: otherlv_3= '{' ( (lv_filter_4_0= ruleMetaClassQualifier ) ) otherlv_5= '}'
                    {
                    otherlv_3=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleMetaClassSelector5186); 

                        	newLeafNode(otherlv_3, grammarAccess.getMetaClassSelectorAccess().getLeftCurlyBracketKeyword_3_0());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2327:1: ( (lv_filter_4_0= ruleMetaClassQualifier ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2328:1: (lv_filter_4_0= ruleMetaClassQualifier )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2328:1: (lv_filter_4_0= ruleMetaClassQualifier )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2329:3: lv_filter_4_0= ruleMetaClassQualifier
                    {
                     
                    	        newCompositeNode(grammarAccess.getMetaClassSelectorAccess().getFilterMetaClassQualifierParserRuleCall_3_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassSelector5207);
                    lv_filter_4_0=ruleMetaClassQualifier();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMetaClassSelectorRule());
                    	        }
                           		set(
                           			current, 
                           			"filter",
                            		lv_filter_4_0, 
                            		"MetaClassQualifier");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleMetaClassSelector5219); 

                        	newLeafNode(otherlv_5, grammarAccess.getMetaClassSelectorAccess().getRightCurlyBracketKeyword_3_2());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMetaClassSelector"


    // $ANTLR start "entryRuleMetaClassQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2357:1: entryRuleMetaClassQualifier returns [EObject current=null] : iv_ruleMetaClassQualifier= ruleMetaClassQualifier EOF ;
    public final EObject entryRuleMetaClassQualifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMetaClassQualifier = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2358:2: (iv_ruleMetaClassQualifier= ruleMetaClassQualifier EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2359:2: iv_ruleMetaClassQualifier= ruleMetaClassQualifier EOF
            {
             newCompositeNode(grammarAccess.getMetaClassQualifierRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassQualifier_in_entryRuleMetaClassQualifier5257);
            iv_ruleMetaClassQualifier=ruleMetaClassQualifier();

            state._fsp--;

             current =iv_ruleMetaClassQualifier; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaClassQualifier5267); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMetaClassQualifier"


    // $ANTLR start "ruleMetaClassQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2366:1: ruleMetaClassQualifier returns [EObject current=null] : (this_MetaClassAndQualifier_0= ruleMetaClassAndQualifier | this_MetaClassOrQualifier_1= ruleMetaClassOrQualifier | this_Reach_2= ruleReach | this_Contains_3= ruleContains | this_AttributeValue_4= ruleAttributeValue ) ;
    public final EObject ruleMetaClassQualifier() throws RecognitionException {
        EObject current = null;

        EObject this_MetaClassAndQualifier_0 = null;

        EObject this_MetaClassOrQualifier_1 = null;

        EObject this_Reach_2 = null;

        EObject this_Contains_3 = null;

        EObject this_AttributeValue_4 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2369:28: ( (this_MetaClassAndQualifier_0= ruleMetaClassAndQualifier | this_MetaClassOrQualifier_1= ruleMetaClassOrQualifier | this_Reach_2= ruleReach | this_Contains_3= ruleContains | this_AttributeValue_4= ruleAttributeValue ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2370:1: (this_MetaClassAndQualifier_0= ruleMetaClassAndQualifier | this_MetaClassOrQualifier_1= ruleMetaClassOrQualifier | this_Reach_2= ruleReach | this_Contains_3= ruleContains | this_AttributeValue_4= ruleAttributeValue )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2370:1: (this_MetaClassAndQualifier_0= ruleMetaClassAndQualifier | this_MetaClassOrQualifier_1= ruleMetaClassOrQualifier | this_Reach_2= ruleReach | this_Contains_3= ruleContains | this_AttributeValue_4= ruleAttributeValue )
            int alt45=5;
            switch ( input.LA(1) ) {
            case 45:
                {
                alt45=1;
                }
                break;
            case 46:
                {
                alt45=2;
                }
                break;
            case 47:
                {
                alt45=3;
                }
                break;
            case 49:
                {
                alt45=4;
                }
                break;
            case RULE_STRING:
            case RULE_ID:
                {
                alt45=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;
            }

            switch (alt45) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2371:5: this_MetaClassAndQualifier_0= ruleMetaClassAndQualifier
                    {
                     
                            newCompositeNode(grammarAccess.getMetaClassQualifierAccess().getMetaClassAndQualifierParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleMetaClassAndQualifier_in_ruleMetaClassQualifier5314);
                    this_MetaClassAndQualifier_0=ruleMetaClassAndQualifier();

                    state._fsp--;

                     
                            current = this_MetaClassAndQualifier_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2381:5: this_MetaClassOrQualifier_1= ruleMetaClassOrQualifier
                    {
                     
                            newCompositeNode(grammarAccess.getMetaClassQualifierAccess().getMetaClassOrQualifierParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleMetaClassOrQualifier_in_ruleMetaClassQualifier5341);
                    this_MetaClassOrQualifier_1=ruleMetaClassOrQualifier();

                    state._fsp--;

                     
                            current = this_MetaClassOrQualifier_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2391:5: this_Reach_2= ruleReach
                    {
                     
                            newCompositeNode(grammarAccess.getMetaClassQualifierAccess().getReachParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleReach_in_ruleMetaClassQualifier5368);
                    this_Reach_2=ruleReach();

                    state._fsp--;

                     
                            current = this_Reach_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2401:5: this_Contains_3= ruleContains
                    {
                     
                            newCompositeNode(grammarAccess.getMetaClassQualifierAccess().getContainsParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleContains_in_ruleMetaClassQualifier5395);
                    this_Contains_3=ruleContains();

                    state._fsp--;

                     
                            current = this_Contains_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2411:5: this_AttributeValue_4= ruleAttributeValue
                    {
                     
                            newCompositeNode(grammarAccess.getMetaClassQualifierAccess().getAttributeValueParserRuleCall_4()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleAttributeValue_in_ruleMetaClassQualifier5422);
                    this_AttributeValue_4=ruleAttributeValue();

                    state._fsp--;

                     
                            current = this_AttributeValue_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMetaClassQualifier"


    // $ANTLR start "entryRuleFragmentObjectQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2427:1: entryRuleFragmentObjectQualifier returns [EObject current=null] : iv_ruleFragmentObjectQualifier= ruleFragmentObjectQualifier EOF ;
    public final EObject entryRuleFragmentObjectQualifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragmentObjectQualifier = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2428:2: (iv_ruleFragmentObjectQualifier= ruleFragmentObjectQualifier EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2429:2: iv_ruleFragmentObjectQualifier= ruleFragmentObjectQualifier EOF
            {
             newCompositeNode(grammarAccess.getFragmentObjectQualifierRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectQualifier_in_entryRuleFragmentObjectQualifier5457);
            iv_ruleFragmentObjectQualifier=ruleFragmentObjectQualifier();

            state._fsp--;

             current =iv_ruleFragmentObjectQualifier; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentObjectQualifier5467); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragmentObjectQualifier"


    // $ANTLR start "ruleFragmentObjectQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2436:1: ruleFragmentObjectQualifier returns [EObject current=null] : (this_FragmentObjectAndQualifier_0= ruleFragmentObjectAndQualifier | this_FragmentObjectOrQualifier_1= ruleFragmentObjectOrQualifier | this_Reach_2= ruleReach | this_Contains_3= ruleContains | this_AttributeValue_4= ruleAttributeValue ) ;
    public final EObject ruleFragmentObjectQualifier() throws RecognitionException {
        EObject current = null;

        EObject this_FragmentObjectAndQualifier_0 = null;

        EObject this_FragmentObjectOrQualifier_1 = null;

        EObject this_Reach_2 = null;

        EObject this_Contains_3 = null;

        EObject this_AttributeValue_4 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2439:28: ( (this_FragmentObjectAndQualifier_0= ruleFragmentObjectAndQualifier | this_FragmentObjectOrQualifier_1= ruleFragmentObjectOrQualifier | this_Reach_2= ruleReach | this_Contains_3= ruleContains | this_AttributeValue_4= ruleAttributeValue ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2440:1: (this_FragmentObjectAndQualifier_0= ruleFragmentObjectAndQualifier | this_FragmentObjectOrQualifier_1= ruleFragmentObjectOrQualifier | this_Reach_2= ruleReach | this_Contains_3= ruleContains | this_AttributeValue_4= ruleAttributeValue )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2440:1: (this_FragmentObjectAndQualifier_0= ruleFragmentObjectAndQualifier | this_FragmentObjectOrQualifier_1= ruleFragmentObjectOrQualifier | this_Reach_2= ruleReach | this_Contains_3= ruleContains | this_AttributeValue_4= ruleAttributeValue )
            int alt46=5;
            switch ( input.LA(1) ) {
            case 45:
                {
                alt46=1;
                }
                break;
            case 46:
                {
                alt46=2;
                }
                break;
            case 47:
                {
                alt46=3;
                }
                break;
            case 49:
                {
                alt46=4;
                }
                break;
            case RULE_STRING:
            case RULE_ID:
                {
                alt46=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 46, 0, input);

                throw nvae;
            }

            switch (alt46) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2441:5: this_FragmentObjectAndQualifier_0= ruleFragmentObjectAndQualifier
                    {
                     
                            newCompositeNode(grammarAccess.getFragmentObjectQualifierAccess().getFragmentObjectAndQualifierParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectAndQualifier_in_ruleFragmentObjectQualifier5514);
                    this_FragmentObjectAndQualifier_0=ruleFragmentObjectAndQualifier();

                    state._fsp--;

                     
                            current = this_FragmentObjectAndQualifier_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2451:5: this_FragmentObjectOrQualifier_1= ruleFragmentObjectOrQualifier
                    {
                     
                            newCompositeNode(grammarAccess.getFragmentObjectQualifierAccess().getFragmentObjectOrQualifierParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectOrQualifier_in_ruleFragmentObjectQualifier5541);
                    this_FragmentObjectOrQualifier_1=ruleFragmentObjectOrQualifier();

                    state._fsp--;

                     
                            current = this_FragmentObjectOrQualifier_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2461:5: this_Reach_2= ruleReach
                    {
                     
                            newCompositeNode(grammarAccess.getFragmentObjectQualifierAccess().getReachParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleReach_in_ruleFragmentObjectQualifier5568);
                    this_Reach_2=ruleReach();

                    state._fsp--;

                     
                            current = this_Reach_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2471:5: this_Contains_3= ruleContains
                    {
                     
                            newCompositeNode(grammarAccess.getFragmentObjectQualifierAccess().getContainsParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleContains_in_ruleFragmentObjectQualifier5595);
                    this_Contains_3=ruleContains();

                    state._fsp--;

                     
                            current = this_Contains_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2481:5: this_AttributeValue_4= ruleAttributeValue
                    {
                     
                            newCompositeNode(grammarAccess.getFragmentObjectQualifierAccess().getAttributeValueParserRuleCall_4()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleAttributeValue_in_ruleFragmentObjectQualifier5622);
                    this_AttributeValue_4=ruleAttributeValue();

                    state._fsp--;

                     
                            current = this_AttributeValue_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentObjectQualifier"


    // $ANTLR start "entryRuleMetaClassAndQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2497:1: entryRuleMetaClassAndQualifier returns [EObject current=null] : iv_ruleMetaClassAndQualifier= ruleMetaClassAndQualifier EOF ;
    public final EObject entryRuleMetaClassAndQualifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMetaClassAndQualifier = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2498:2: (iv_ruleMetaClassAndQualifier= ruleMetaClassAndQualifier EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2499:2: iv_ruleMetaClassAndQualifier= ruleMetaClassAndQualifier EOF
            {
             newCompositeNode(grammarAccess.getMetaClassAndQualifierRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassAndQualifier_in_entryRuleMetaClassAndQualifier5657);
            iv_ruleMetaClassAndQualifier=ruleMetaClassAndQualifier();

            state._fsp--;

             current =iv_ruleMetaClassAndQualifier; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaClassAndQualifier5667); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMetaClassAndQualifier"


    // $ANTLR start "ruleMetaClassAndQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2506:1: ruleMetaClassAndQualifier returns [EObject current=null] : (otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}' ) ;
    public final EObject ruleMetaClassAndQualifier() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_qualifiers_2_0 = null;

        EObject lv_qualifiers_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2509:28: ( (otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2510:1: (otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2510:1: (otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2510:3: otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,45,FollowSets000.FOLLOW_45_in_ruleMetaClassAndQualifier5704); 

                	newLeafNode(otherlv_0, grammarAccess.getMetaClassAndQualifierAccess().getAndKeyword_0());
                
            otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleMetaClassAndQualifier5716); 

                	newLeafNode(otherlv_1, grammarAccess.getMetaClassAndQualifierAccess().getLeftCurlyBracketKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2518:1: ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2519:1: (lv_qualifiers_2_0= ruleMetaClassQualifier )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2519:1: (lv_qualifiers_2_0= ruleMetaClassQualifier )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2520:3: lv_qualifiers_2_0= ruleMetaClassQualifier
            {
             
            	        newCompositeNode(grammarAccess.getMetaClassAndQualifierAccess().getQualifiersMetaClassQualifierParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassAndQualifier5737);
            lv_qualifiers_2_0=ruleMetaClassQualifier();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMetaClassAndQualifierRule());
            	        }
                   		add(
                   			current, 
                   			"qualifiers",
                    		lv_qualifiers_2_0, 
                    		"MetaClassQualifier");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleMetaClassAndQualifier5749); 

                	newLeafNode(otherlv_3, grammarAccess.getMetaClassAndQualifierAccess().getCommaKeyword_3());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2540:1: ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )*
            loop47:
            do {
                int alt47=2;
                int LA47_0 = input.LA(1);

                if ( ((LA47_0>=RULE_STRING && LA47_0<=RULE_ID)||(LA47_0>=45 && LA47_0<=47)||LA47_0==49) ) {
                    alt47=1;
                }


                switch (alt47) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2541:1: (lv_qualifiers_4_0= ruleMetaClassQualifier )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2541:1: (lv_qualifiers_4_0= ruleMetaClassQualifier )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2542:3: lv_qualifiers_4_0= ruleMetaClassQualifier
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMetaClassAndQualifierAccess().getQualifiersMetaClassQualifierParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassAndQualifier5770);
            	    lv_qualifiers_4_0=ruleMetaClassQualifier();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMetaClassAndQualifierRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"qualifiers",
            	            		lv_qualifiers_4_0, 
            	            		"MetaClassQualifier");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop47;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleMetaClassAndQualifier5783); 

                	newLeafNode(otherlv_5, grammarAccess.getMetaClassAndQualifierAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMetaClassAndQualifier"


    // $ANTLR start "entryRuleMetaClassOrQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2570:1: entryRuleMetaClassOrQualifier returns [EObject current=null] : iv_ruleMetaClassOrQualifier= ruleMetaClassOrQualifier EOF ;
    public final EObject entryRuleMetaClassOrQualifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMetaClassOrQualifier = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2571:2: (iv_ruleMetaClassOrQualifier= ruleMetaClassOrQualifier EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2572:2: iv_ruleMetaClassOrQualifier= ruleMetaClassOrQualifier EOF
            {
             newCompositeNode(grammarAccess.getMetaClassOrQualifierRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassOrQualifier_in_entryRuleMetaClassOrQualifier5819);
            iv_ruleMetaClassOrQualifier=ruleMetaClassOrQualifier();

            state._fsp--;

             current =iv_ruleMetaClassOrQualifier; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaClassOrQualifier5829); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMetaClassOrQualifier"


    // $ANTLR start "ruleMetaClassOrQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2579:1: ruleMetaClassOrQualifier returns [EObject current=null] : (otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}' ) ;
    public final EObject ruleMetaClassOrQualifier() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_qualifiers_2_0 = null;

        EObject lv_qualifiers_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2582:28: ( (otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2583:1: (otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2583:1: (otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2583:3: otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,46,FollowSets000.FOLLOW_46_in_ruleMetaClassOrQualifier5866); 

                	newLeafNode(otherlv_0, grammarAccess.getMetaClassOrQualifierAccess().getOrKeyword_0());
                
            otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleMetaClassOrQualifier5878); 

                	newLeafNode(otherlv_1, grammarAccess.getMetaClassOrQualifierAccess().getLeftCurlyBracketKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2591:1: ( (lv_qualifiers_2_0= ruleMetaClassQualifier ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2592:1: (lv_qualifiers_2_0= ruleMetaClassQualifier )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2592:1: (lv_qualifiers_2_0= ruleMetaClassQualifier )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2593:3: lv_qualifiers_2_0= ruleMetaClassQualifier
            {
             
            	        newCompositeNode(grammarAccess.getMetaClassOrQualifierAccess().getQualifiersMetaClassQualifierParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassOrQualifier5899);
            lv_qualifiers_2_0=ruleMetaClassQualifier();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMetaClassOrQualifierRule());
            	        }
                   		add(
                   			current, 
                   			"qualifiers",
                    		lv_qualifiers_2_0, 
                    		"MetaClassQualifier");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleMetaClassOrQualifier5911); 

                	newLeafNode(otherlv_3, grammarAccess.getMetaClassOrQualifierAccess().getCommaKeyword_3());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2613:1: ( (lv_qualifiers_4_0= ruleMetaClassQualifier ) )*
            loop48:
            do {
                int alt48=2;
                int LA48_0 = input.LA(1);

                if ( ((LA48_0>=RULE_STRING && LA48_0<=RULE_ID)||(LA48_0>=45 && LA48_0<=47)||LA48_0==49) ) {
                    alt48=1;
                }


                switch (alt48) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2614:1: (lv_qualifiers_4_0= ruleMetaClassQualifier )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2614:1: (lv_qualifiers_4_0= ruleMetaClassQualifier )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2615:3: lv_qualifiers_4_0= ruleMetaClassQualifier
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMetaClassOrQualifierAccess().getQualifiersMetaClassQualifierParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassOrQualifier5932);
            	    lv_qualifiers_4_0=ruleMetaClassQualifier();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMetaClassOrQualifierRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"qualifiers",
            	            		lv_qualifiers_4_0, 
            	            		"MetaClassQualifier");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop48;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleMetaClassOrQualifier5945); 

                	newLeafNode(otherlv_5, grammarAccess.getMetaClassOrQualifierAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMetaClassOrQualifier"


    // $ANTLR start "entryRuleFragmentObjectAndQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2643:1: entryRuleFragmentObjectAndQualifier returns [EObject current=null] : iv_ruleFragmentObjectAndQualifier= ruleFragmentObjectAndQualifier EOF ;
    public final EObject entryRuleFragmentObjectAndQualifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragmentObjectAndQualifier = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2644:2: (iv_ruleFragmentObjectAndQualifier= ruleFragmentObjectAndQualifier EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2645:2: iv_ruleFragmentObjectAndQualifier= ruleFragmentObjectAndQualifier EOF
            {
             newCompositeNode(grammarAccess.getFragmentObjectAndQualifierRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectAndQualifier_in_entryRuleFragmentObjectAndQualifier5981);
            iv_ruleFragmentObjectAndQualifier=ruleFragmentObjectAndQualifier();

            state._fsp--;

             current =iv_ruleFragmentObjectAndQualifier; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentObjectAndQualifier5991); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragmentObjectAndQualifier"


    // $ANTLR start "ruleFragmentObjectAndQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2652:1: ruleFragmentObjectAndQualifier returns [EObject current=null] : (otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}' ) ;
    public final EObject ruleFragmentObjectAndQualifier() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_qualifiers_2_0 = null;

        EObject lv_qualifiers_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2655:28: ( (otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2656:1: (otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2656:1: (otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2656:3: otherlv_0= 'and' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,45,FollowSets000.FOLLOW_45_in_ruleFragmentObjectAndQualifier6028); 

                	newLeafNode(otherlv_0, grammarAccess.getFragmentObjectAndQualifierAccess().getAndKeyword_0());
                
            otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleFragmentObjectAndQualifier6040); 

                	newLeafNode(otherlv_1, grammarAccess.getFragmentObjectAndQualifierAccess().getLeftCurlyBracketKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2664:1: ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2665:1: (lv_qualifiers_2_0= ruleFragmentObjectQualifier )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2665:1: (lv_qualifiers_2_0= ruleFragmentObjectQualifier )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2666:3: lv_qualifiers_2_0= ruleFragmentObjectQualifier
            {
             
            	        newCompositeNode(grammarAccess.getFragmentObjectAndQualifierAccess().getQualifiersFragmentObjectQualifierParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectAndQualifier6061);
            lv_qualifiers_2_0=ruleFragmentObjectQualifier();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentObjectAndQualifierRule());
            	        }
                   		add(
                   			current, 
                   			"qualifiers",
                    		lv_qualifiers_2_0, 
                    		"FragmentObjectQualifier");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleFragmentObjectAndQualifier6073); 

                	newLeafNode(otherlv_3, grammarAccess.getFragmentObjectAndQualifierAccess().getCommaKeyword_3());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2686:1: ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )*
            loop49:
            do {
                int alt49=2;
                int LA49_0 = input.LA(1);

                if ( ((LA49_0>=RULE_STRING && LA49_0<=RULE_ID)||(LA49_0>=45 && LA49_0<=47)||LA49_0==49) ) {
                    alt49=1;
                }


                switch (alt49) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2687:1: (lv_qualifiers_4_0= ruleFragmentObjectQualifier )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2687:1: (lv_qualifiers_4_0= ruleFragmentObjectQualifier )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2688:3: lv_qualifiers_4_0= ruleFragmentObjectQualifier
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFragmentObjectAndQualifierAccess().getQualifiersFragmentObjectQualifierParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectAndQualifier6094);
            	    lv_qualifiers_4_0=ruleFragmentObjectQualifier();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFragmentObjectAndQualifierRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"qualifiers",
            	            		lv_qualifiers_4_0, 
            	            		"FragmentObjectQualifier");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop49;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleFragmentObjectAndQualifier6107); 

                	newLeafNode(otherlv_5, grammarAccess.getFragmentObjectAndQualifierAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentObjectAndQualifier"


    // $ANTLR start "entryRuleFragmentObjectOrQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2716:1: entryRuleFragmentObjectOrQualifier returns [EObject current=null] : iv_ruleFragmentObjectOrQualifier= ruleFragmentObjectOrQualifier EOF ;
    public final EObject entryRuleFragmentObjectOrQualifier() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragmentObjectOrQualifier = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2717:2: (iv_ruleFragmentObjectOrQualifier= ruleFragmentObjectOrQualifier EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2718:2: iv_ruleFragmentObjectOrQualifier= ruleFragmentObjectOrQualifier EOF
            {
             newCompositeNode(grammarAccess.getFragmentObjectOrQualifierRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectOrQualifier_in_entryRuleFragmentObjectOrQualifier6143);
            iv_ruleFragmentObjectOrQualifier=ruleFragmentObjectOrQualifier();

            state._fsp--;

             current =iv_ruleFragmentObjectOrQualifier; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentObjectOrQualifier6153); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragmentObjectOrQualifier"


    // $ANTLR start "ruleFragmentObjectOrQualifier"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2725:1: ruleFragmentObjectOrQualifier returns [EObject current=null] : (otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}' ) ;
    public final EObject ruleFragmentObjectOrQualifier() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_qualifiers_2_0 = null;

        EObject lv_qualifiers_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2728:28: ( (otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}' ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2729:1: (otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}' )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2729:1: (otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}' )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2729:3: otherlv_0= 'or' otherlv_1= '{' ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) ) otherlv_3= ',' ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )* otherlv_5= '}'
            {
            otherlv_0=(Token)match(input,46,FollowSets000.FOLLOW_46_in_ruleFragmentObjectOrQualifier6190); 

                	newLeafNode(otherlv_0, grammarAccess.getFragmentObjectOrQualifierAccess().getOrKeyword_0());
                
            otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleFragmentObjectOrQualifier6202); 

                	newLeafNode(otherlv_1, grammarAccess.getFragmentObjectOrQualifierAccess().getLeftCurlyBracketKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2737:1: ( (lv_qualifiers_2_0= ruleFragmentObjectQualifier ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2738:1: (lv_qualifiers_2_0= ruleFragmentObjectQualifier )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2738:1: (lv_qualifiers_2_0= ruleFragmentObjectQualifier )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2739:3: lv_qualifiers_2_0= ruleFragmentObjectQualifier
            {
             
            	        newCompositeNode(grammarAccess.getFragmentObjectOrQualifierAccess().getQualifiersFragmentObjectQualifierParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectOrQualifier6223);
            lv_qualifiers_2_0=ruleFragmentObjectQualifier();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentObjectOrQualifierRule());
            	        }
                   		add(
                   			current, 
                   			"qualifiers",
                    		lv_qualifiers_2_0, 
                    		"FragmentObjectQualifier");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleFragmentObjectOrQualifier6235); 

                	newLeafNode(otherlv_3, grammarAccess.getFragmentObjectOrQualifierAccess().getCommaKeyword_3());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2759:1: ( (lv_qualifiers_4_0= ruleFragmentObjectQualifier ) )*
            loop50:
            do {
                int alt50=2;
                int LA50_0 = input.LA(1);

                if ( ((LA50_0>=RULE_STRING && LA50_0<=RULE_ID)||(LA50_0>=45 && LA50_0<=47)||LA50_0==49) ) {
                    alt50=1;
                }


                switch (alt50) {
            	case 1 :
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2760:1: (lv_qualifiers_4_0= ruleFragmentObjectQualifier )
            	    {
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2760:1: (lv_qualifiers_4_0= ruleFragmentObjectQualifier )
            	    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2761:3: lv_qualifiers_4_0= ruleFragmentObjectQualifier
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFragmentObjectOrQualifierAccess().getQualifiersFragmentObjectQualifierParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectOrQualifier6256);
            	    lv_qualifiers_4_0=ruleFragmentObjectQualifier();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFragmentObjectOrQualifierRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"qualifiers",
            	            		lv_qualifiers_4_0, 
            	            		"FragmentObjectQualifier");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop50;
                }
            } while (true);

            otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleFragmentObjectOrQualifier6269); 

                	newLeafNode(otherlv_5, grammarAccess.getFragmentObjectOrQualifierAccess().getRightCurlyBracketKeyword_5());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentObjectOrQualifier"


    // $ANTLR start "entryRuleReach"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2789:1: entryRuleReach returns [EObject current=null] : iv_ruleReach= ruleReach EOF ;
    public final EObject entryRuleReach() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReach = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2790:2: (iv_ruleReach= ruleReach EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2791:2: iv_ruleReach= ruleReach EOF
            {
             newCompositeNode(grammarAccess.getReachRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReach_in_entryRuleReach6305);
            iv_ruleReach=ruleReach();

            state._fsp--;

             current =iv_ruleReach; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReach6315); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReach"


    // $ANTLR start "ruleReach"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2798:1: ruleReach returns [EObject current=null] : (otherlv_0= 'reference' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) ) ) ;
    public final EObject ruleReach() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_refName_3_0 = null;

        EObject lv_reachSelector_5_1 = null;

        EObject lv_reachSelector_5_2 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2801:28: ( (otherlv_0= 'reference' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2802:1: (otherlv_0= 'reference' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2802:1: (otherlv_0= 'reference' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2802:3: otherlv_0= 'reference' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) )
            {
            otherlv_0=(Token)match(input,47,FollowSets000.FOLLOW_47_in_ruleReach6352); 

                	newLeafNode(otherlv_0, grammarAccess.getReachAccess().getReferenceKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2806:1: (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==12) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2806:3: otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}'
                    {
                    otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleReach6365); 

                        	newLeafNode(otherlv_1, grammarAccess.getReachAccess().getLeftCurlyBracketKeyword_1_0());
                        
                    otherlv_2=(Token)match(input,48,FollowSets000.FOLLOW_48_in_ruleReach6377); 

                        	newLeafNode(otherlv_2, grammarAccess.getReachAccess().getViaKeyword_1_1());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2814:1: ( (lv_refName_3_0= ruleEString ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2815:1: (lv_refName_3_0= ruleEString )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2815:1: (lv_refName_3_0= ruleEString )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2816:3: lv_refName_3_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getReachAccess().getRefNameEStringParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReach6398);
                    lv_refName_3_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReachRule());
                    	        }
                           		set(
                           			current, 
                           			"refName",
                            		lv_refName_3_0, 
                            		"EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_4=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleReach6410); 

                        	newLeafNode(otherlv_4, grammarAccess.getReachAccess().getRightCurlyBracketKeyword_1_3());
                        

                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2836:3: ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2837:1: ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2837:1: ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2838:1: (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2838:1: (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector )
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==RULE_INT||LA52_0==22||LA52_0==34||(LA52_0>=36 && LA52_0<=41)) ) {
                alt52=1;
            }
            else if ( ((LA52_0>=RULE_STRING && LA52_0<=RULE_ID)) ) {
                alt52=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 52, 0, input);

                throw nvae;
            }
            switch (alt52) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2839:3: lv_reachSelector_5_1= ruleMetaClassSelector
                    {
                     
                    	        newCompositeNode(grammarAccess.getReachAccess().getReachSelectorMetaClassSelectorParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleMetaClassSelector_in_ruleReach6435);
                    lv_reachSelector_5_1=ruleMetaClassSelector();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReachRule());
                    	        }
                           		set(
                           			current, 
                           			"reachSelector",
                            		lv_reachSelector_5_1, 
                            		"MetaClassSelector");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2854:8: lv_reachSelector_5_2= ruleFragmentObjectSelector
                    {
                     
                    	        newCompositeNode(grammarAccess.getReachAccess().getReachSelectorFragmentObjectSelectorParserRuleCall_2_0_1()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectSelector_in_ruleReach6454);
                    lv_reachSelector_5_2=ruleFragmentObjectSelector();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReachRule());
                    	        }
                           		set(
                           			current, 
                           			"reachSelector",
                            		lv_reachSelector_5_2, 
                            		"FragmentObjectSelector");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReach"


    // $ANTLR start "entryRuleFragmentObjectSelector"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2880:1: entryRuleFragmentObjectSelector returns [EObject current=null] : iv_ruleFragmentObjectSelector= ruleFragmentObjectSelector EOF ;
    public final EObject entryRuleFragmentObjectSelector() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragmentObjectSelector = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2881:2: (iv_ruleFragmentObjectSelector= ruleFragmentObjectSelector EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2882:2: iv_ruleFragmentObjectSelector= ruleFragmentObjectSelector EOF
            {
             newCompositeNode(grammarAccess.getFragmentObjectSelectorRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectSelector_in_entryRuleFragmentObjectSelector6493);
            iv_ruleFragmentObjectSelector=ruleFragmentObjectSelector();

            state._fsp--;

             current =iv_ruleFragmentObjectSelector; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentObjectSelector6503); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragmentObjectSelector"


    // $ANTLR start "ruleFragmentObjectSelector"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2889:1: ruleFragmentObjectSelector returns [EObject current=null] : ( ( ruleEString ) ) ;
    public final EObject ruleFragmentObjectSelector() throws RecognitionException {
        EObject current = null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2892:28: ( ( ( ruleEString ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2893:1: ( ( ruleEString ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2893:1: ( ( ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2894:1: ( ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2894:1: ( ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2895:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getFragmentObjectSelectorRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getFragmentObjectSelectorAccess().getObjectObjectCrossReference_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFragmentObjectSelector6550);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentObjectSelector"


    // $ANTLR start "entryRuleContains"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2916:1: entryRuleContains returns [EObject current=null] : iv_ruleContains= ruleContains EOF ;
    public final EObject entryRuleContains() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContains = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2917:2: (iv_ruleContains= ruleContains EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2918:2: iv_ruleContains= ruleContains EOF
            {
             newCompositeNode(grammarAccess.getContainsRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleContains_in_entryRuleContains6585);
            iv_ruleContains=ruleContains();

            state._fsp--;

             current =iv_ruleContains; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleContains6595); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContains"


    // $ANTLR start "ruleContains"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2925:1: ruleContains returns [EObject current=null] : (otherlv_0= 'contain' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) ) ) ;
    public final EObject ruleContains() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_refName_3_0 = null;

        EObject lv_reachSelector_5_1 = null;

        EObject lv_reachSelector_5_2 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2928:28: ( (otherlv_0= 'contain' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2929:1: (otherlv_0= 'contain' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2929:1: (otherlv_0= 'contain' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2929:3: otherlv_0= 'contain' (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )? ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) )
            {
            otherlv_0=(Token)match(input,49,FollowSets000.FOLLOW_49_in_ruleContains6632); 

                	newLeafNode(otherlv_0, grammarAccess.getContainsAccess().getContainKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2933:1: (otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}' )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==12) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2933:3: otherlv_1= '{' otherlv_2= 'via' ( (lv_refName_3_0= ruleEString ) ) otherlv_4= '}'
                    {
                    otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleContains6645); 

                        	newLeafNode(otherlv_1, grammarAccess.getContainsAccess().getLeftCurlyBracketKeyword_1_0());
                        
                    otherlv_2=(Token)match(input,48,FollowSets000.FOLLOW_48_in_ruleContains6657); 

                        	newLeafNode(otherlv_2, grammarAccess.getContainsAccess().getViaKeyword_1_1());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2941:1: ( (lv_refName_3_0= ruleEString ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2942:1: (lv_refName_3_0= ruleEString )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2942:1: (lv_refName_3_0= ruleEString )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2943:3: lv_refName_3_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getContainsAccess().getRefNameEStringParserRuleCall_1_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleContains6678);
                    lv_refName_3_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getContainsRule());
                    	        }
                           		set(
                           			current, 
                           			"refName",
                            		lv_refName_3_0, 
                            		"EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_4=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleContains6690); 

                        	newLeafNode(otherlv_4, grammarAccess.getContainsAccess().getRightCurlyBracketKeyword_1_3());
                        

                    }
                    break;

            }

            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2963:3: ( ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2964:1: ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2964:1: ( (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2965:1: (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2965:1: (lv_reachSelector_5_1= ruleMetaClassSelector | lv_reachSelector_5_2= ruleFragmentObjectSelector )
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==RULE_INT||LA54_0==22||LA54_0==34||(LA54_0>=36 && LA54_0<=41)) ) {
                alt54=1;
            }
            else if ( ((LA54_0>=RULE_STRING && LA54_0<=RULE_ID)) ) {
                alt54=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 54, 0, input);

                throw nvae;
            }
            switch (alt54) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2966:3: lv_reachSelector_5_1= ruleMetaClassSelector
                    {
                     
                    	        newCompositeNode(grammarAccess.getContainsAccess().getReachSelectorMetaClassSelectorParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleMetaClassSelector_in_ruleContains6715);
                    lv_reachSelector_5_1=ruleMetaClassSelector();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getContainsRule());
                    	        }
                           		set(
                           			current, 
                           			"reachSelector",
                            		lv_reachSelector_5_1, 
                            		"MetaClassSelector");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:2981:8: lv_reachSelector_5_2= ruleFragmentObjectSelector
                    {
                     
                    	        newCompositeNode(grammarAccess.getContainsAccess().getReachSelectorFragmentObjectSelectorParserRuleCall_2_0_1()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFragmentObjectSelector_in_ruleContains6734);
                    lv_reachSelector_5_2=ruleFragmentObjectSelector();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getContainsRule());
                    	        }
                           		set(
                           			current, 
                           			"reachSelector",
                            		lv_reachSelector_5_2, 
                            		"FragmentObjectSelector");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }
                    break;

            }


            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContains"


    // $ANTLR start "entryRuleAttributeValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3007:1: entryRuleAttributeValue returns [EObject current=null] : iv_ruleAttributeValue= ruleAttributeValue EOF ;
    public final EObject entryRuleAttributeValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeValue = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3008:2: (iv_ruleAttributeValue= ruleAttributeValue EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3009:2: iv_ruleAttributeValue= ruleAttributeValue EOF
            {
             newCompositeNode(grammarAccess.getAttributeValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttributeValue_in_entryRuleAttributeValue6773);
            iv_ruleAttributeValue=ruleAttributeValue();

            state._fsp--;

             current =iv_ruleAttributeValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttributeValue6783); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeValue"


    // $ANTLR start "ruleAttributeValue"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3016:1: ruleAttributeValue returns [EObject current=null] : ( ( (lv_attributeName_0_0= ruleEString ) ) otherlv_1= '=' ( ( (lv_stringValue_2_0= ruleEString ) ) | ( (lv_intValue_3_0= ruleEIntegerObject ) ) | ( (lv_booleanValue_4_0= 'true' ) ) | ( (lv_booleanValue_5_0= 'false' ) ) ) ) ;
    public final EObject ruleAttributeValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_booleanValue_4_0=null;
        Token lv_booleanValue_5_0=null;
        AntlrDatatypeRuleToken lv_attributeName_0_0 = null;

        AntlrDatatypeRuleToken lv_stringValue_2_0 = null;

        AntlrDatatypeRuleToken lv_intValue_3_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3019:28: ( ( ( (lv_attributeName_0_0= ruleEString ) ) otherlv_1= '=' ( ( (lv_stringValue_2_0= ruleEString ) ) | ( (lv_intValue_3_0= ruleEIntegerObject ) ) | ( (lv_booleanValue_4_0= 'true' ) ) | ( (lv_booleanValue_5_0= 'false' ) ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3020:1: ( ( (lv_attributeName_0_0= ruleEString ) ) otherlv_1= '=' ( ( (lv_stringValue_2_0= ruleEString ) ) | ( (lv_intValue_3_0= ruleEIntegerObject ) ) | ( (lv_booleanValue_4_0= 'true' ) ) | ( (lv_booleanValue_5_0= 'false' ) ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3020:1: ( ( (lv_attributeName_0_0= ruleEString ) ) otherlv_1= '=' ( ( (lv_stringValue_2_0= ruleEString ) ) | ( (lv_intValue_3_0= ruleEIntegerObject ) ) | ( (lv_booleanValue_4_0= 'true' ) ) | ( (lv_booleanValue_5_0= 'false' ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3020:2: ( (lv_attributeName_0_0= ruleEString ) ) otherlv_1= '=' ( ( (lv_stringValue_2_0= ruleEString ) ) | ( (lv_intValue_3_0= ruleEIntegerObject ) ) | ( (lv_booleanValue_4_0= 'true' ) ) | ( (lv_booleanValue_5_0= 'false' ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3020:2: ( (lv_attributeName_0_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3021:1: (lv_attributeName_0_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3021:1: (lv_attributeName_0_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3022:3: lv_attributeName_0_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAttributeValueAccess().getAttributeNameEStringParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAttributeValue6829);
            lv_attributeName_0_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeValueRule());
            	        }
                   		set(
                   			current, 
                   			"attributeName",
                    		lv_attributeName_0_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleAttributeValue6841); 

                	newLeafNode(otherlv_1, grammarAccess.getAttributeValueAccess().getEqualsSignKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3042:1: ( ( (lv_stringValue_2_0= ruleEString ) ) | ( (lv_intValue_3_0= ruleEIntegerObject ) ) | ( (lv_booleanValue_4_0= 'true' ) ) | ( (lv_booleanValue_5_0= 'false' ) ) )
            int alt55=4;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_ID:
                {
                alt55=1;
                }
                break;
            case RULE_INT:
            case 34:
                {
                alt55=2;
                }
                break;
            case 26:
                {
                alt55=3;
                }
                break;
            case 27:
                {
                alt55=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 55, 0, input);

                throw nvae;
            }

            switch (alt55) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3042:2: ( (lv_stringValue_2_0= ruleEString ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3042:2: ( (lv_stringValue_2_0= ruleEString ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3043:1: (lv_stringValue_2_0= ruleEString )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3043:1: (lv_stringValue_2_0= ruleEString )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3044:3: lv_stringValue_2_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getAttributeValueAccess().getStringValueEStringParserRuleCall_2_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAttributeValue6863);
                    lv_stringValue_2_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAttributeValueRule());
                    	        }
                           		set(
                           			current, 
                           			"stringValue",
                            		lv_stringValue_2_0, 
                            		"EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3061:6: ( (lv_intValue_3_0= ruleEIntegerObject ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3061:6: ( (lv_intValue_3_0= ruleEIntegerObject ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3062:1: (lv_intValue_3_0= ruleEIntegerObject )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3062:1: (lv_intValue_3_0= ruleEIntegerObject )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3063:3: lv_intValue_3_0= ruleEIntegerObject
                    {
                     
                    	        newCompositeNode(grammarAccess.getAttributeValueAccess().getIntValueEIntegerObjectParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEIntegerObject_in_ruleAttributeValue6890);
                    lv_intValue_3_0=ruleEIntegerObject();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAttributeValueRule());
                    	        }
                           		set(
                           			current, 
                           			"intValue",
                            		lv_intValue_3_0, 
                            		"EIntegerObject");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;
                case 3 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3080:6: ( (lv_booleanValue_4_0= 'true' ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3080:6: ( (lv_booleanValue_4_0= 'true' ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3081:1: (lv_booleanValue_4_0= 'true' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3081:1: (lv_booleanValue_4_0= 'true' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3082:3: lv_booleanValue_4_0= 'true'
                    {
                    lv_booleanValue_4_0=(Token)match(input,26,FollowSets000.FOLLOW_26_in_ruleAttributeValue6914); 

                            newLeafNode(lv_booleanValue_4_0, grammarAccess.getAttributeValueAccess().getBooleanValueTrueKeyword_2_2_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getAttributeValueRule());
                    	        }
                           		setWithLastConsumed(current, "booleanValue", lv_booleanValue_4_0, "true");
                    	    

                    }


                    }


                    }
                    break;
                case 4 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3096:6: ( (lv_booleanValue_5_0= 'false' ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3096:6: ( (lv_booleanValue_5_0= 'false' ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3097:1: (lv_booleanValue_5_0= 'false' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3097:1: (lv_booleanValue_5_0= 'false' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3098:3: lv_booleanValue_5_0= 'false'
                    {
                    lv_booleanValue_5_0=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleAttributeValue6951); 

                            newLeafNode(lv_booleanValue_5_0, grammarAccess.getAttributeValueAccess().getBooleanValueFalseKeyword_2_3_0());
                        

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getAttributeValueRule());
                    	        }
                           		setWithLastConsumed(current, "booleanValue", lv_booleanValue_5_0, "false");
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeValue"


    // $ANTLR start "entryRuleCheck"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3119:1: entryRuleCheck returns [EObject current=null] : iv_ruleCheck= ruleCheck EOF ;
    public final EObject entryRuleCheck() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCheck = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3120:2: (iv_ruleCheck= ruleCheck EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3121:2: iv_ruleCheck= ruleCheck EOF
            {
             newCompositeNode(grammarAccess.getCheckRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleCheck_in_entryRuleCheck7001);
            iv_ruleCheck=ruleCheck();

            state._fsp--;

             current =iv_ruleCheck; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleCheck7011); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheck"


    // $ANTLR start "ruleCheck"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3128:1: ruleCheck returns [EObject current=null] : (this_FeatureExistence_0= ruleFeatureExistence | this_TypeExistence_1= ruleTypeExistence | this_AbstractTypeInstance_2= ruleAbstractTypeInstance | this_MissingAttribute_3= ruleMissingAttribute | this_MissingReference_4= ruleMissingReference | this_MultiplicityMismatch_5= ruleMultiplicityMismatch | this_TypeMismatch_6= ruleTypeMismatch | this_FeatureNatureMismatch_7= ruleFeatureNatureMismatch | this_RestrictionViolation_8= ruleRestrictionViolation | this_Uncontainment_9= ruleUncontainment ) ;
    public final EObject ruleCheck() throws RecognitionException {
        EObject current = null;

        EObject this_FeatureExistence_0 = null;

        EObject this_TypeExistence_1 = null;

        EObject this_AbstractTypeInstance_2 = null;

        EObject this_MissingAttribute_3 = null;

        EObject this_MissingReference_4 = null;

        EObject this_MultiplicityMismatch_5 = null;

        EObject this_TypeMismatch_6 = null;

        EObject this_FeatureNatureMismatch_7 = null;

        EObject this_RestrictionViolation_8 = null;

        EObject this_Uncontainment_9 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3131:28: ( (this_FeatureExistence_0= ruleFeatureExistence | this_TypeExistence_1= ruleTypeExistence | this_AbstractTypeInstance_2= ruleAbstractTypeInstance | this_MissingAttribute_3= ruleMissingAttribute | this_MissingReference_4= ruleMissingReference | this_MultiplicityMismatch_5= ruleMultiplicityMismatch | this_TypeMismatch_6= ruleTypeMismatch | this_FeatureNatureMismatch_7= ruleFeatureNatureMismatch | this_RestrictionViolation_8= ruleRestrictionViolation | this_Uncontainment_9= ruleUncontainment ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3132:1: (this_FeatureExistence_0= ruleFeatureExistence | this_TypeExistence_1= ruleTypeExistence | this_AbstractTypeInstance_2= ruleAbstractTypeInstance | this_MissingAttribute_3= ruleMissingAttribute | this_MissingReference_4= ruleMissingReference | this_MultiplicityMismatch_5= ruleMultiplicityMismatch | this_TypeMismatch_6= ruleTypeMismatch | this_FeatureNatureMismatch_7= ruleFeatureNatureMismatch | this_RestrictionViolation_8= ruleRestrictionViolation | this_Uncontainment_9= ruleUncontainment )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3132:1: (this_FeatureExistence_0= ruleFeatureExistence | this_TypeExistence_1= ruleTypeExistence | this_AbstractTypeInstance_2= ruleAbstractTypeInstance | this_MissingAttribute_3= ruleMissingAttribute | this_MissingReference_4= ruleMissingReference | this_MultiplicityMismatch_5= ruleMultiplicityMismatch | this_TypeMismatch_6= ruleTypeMismatch | this_FeatureNatureMismatch_7= ruleFeatureNatureMismatch | this_RestrictionViolation_8= ruleRestrictionViolation | this_Uncontainment_9= ruleUncontainment )
            int alt56=10;
            alt56 = dfa56.predict(input);
            switch (alt56) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3133:5: this_FeatureExistence_0= ruleFeatureExistence
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getFeatureExistenceParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureExistence_in_ruleCheck7058);
                    this_FeatureExistence_0=ruleFeatureExistence();

                    state._fsp--;

                     
                            current = this_FeatureExistence_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3143:5: this_TypeExistence_1= ruleTypeExistence
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getTypeExistenceParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleTypeExistence_in_ruleCheck7085);
                    this_TypeExistence_1=ruleTypeExistence();

                    state._fsp--;

                     
                            current = this_TypeExistence_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3153:5: this_AbstractTypeInstance_2= ruleAbstractTypeInstance
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getAbstractTypeInstanceParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleAbstractTypeInstance_in_ruleCheck7112);
                    this_AbstractTypeInstance_2=ruleAbstractTypeInstance();

                    state._fsp--;

                     
                            current = this_AbstractTypeInstance_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3163:5: this_MissingAttribute_3= ruleMissingAttribute
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getMissingAttributeParserRuleCall_3()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleMissingAttribute_in_ruleCheck7139);
                    this_MissingAttribute_3=ruleMissingAttribute();

                    state._fsp--;

                     
                            current = this_MissingAttribute_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3173:5: this_MissingReference_4= ruleMissingReference
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getMissingReferenceParserRuleCall_4()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleMissingReference_in_ruleCheck7166);
                    this_MissingReference_4=ruleMissingReference();

                    state._fsp--;

                     
                            current = this_MissingReference_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 6 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3183:5: this_MultiplicityMismatch_5= ruleMultiplicityMismatch
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getMultiplicityMismatchParserRuleCall_5()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleMultiplicityMismatch_in_ruleCheck7193);
                    this_MultiplicityMismatch_5=ruleMultiplicityMismatch();

                    state._fsp--;

                     
                            current = this_MultiplicityMismatch_5; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 7 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3193:5: this_TypeMismatch_6= ruleTypeMismatch
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getTypeMismatchParserRuleCall_6()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleTypeMismatch_in_ruleCheck7220);
                    this_TypeMismatch_6=ruleTypeMismatch();

                    state._fsp--;

                     
                            current = this_TypeMismatch_6; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 8 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3203:5: this_FeatureNatureMismatch_7= ruleFeatureNatureMismatch
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getFeatureNatureMismatchParserRuleCall_7()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureNatureMismatch_in_ruleCheck7247);
                    this_FeatureNatureMismatch_7=ruleFeatureNatureMismatch();

                    state._fsp--;

                     
                            current = this_FeatureNatureMismatch_7; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 9 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3213:5: this_RestrictionViolation_8= ruleRestrictionViolation
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getRestrictionViolationParserRuleCall_8()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleRestrictionViolation_in_ruleCheck7274);
                    this_RestrictionViolation_8=ruleRestrictionViolation();

                    state._fsp--;

                     
                            current = this_RestrictionViolation_8; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 10 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3223:5: this_Uncontainment_9= ruleUncontainment
                    {
                     
                            newCompositeNode(grammarAccess.getCheckAccess().getUncontainmentParserRuleCall_9()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleUncontainment_in_ruleCheck7301);
                    this_Uncontainment_9=ruleUncontainment();

                    state._fsp--;

                     
                            current = this_Uncontainment_9; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheck"


    // $ANTLR start "entryRuleUncontainment"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3239:1: entryRuleUncontainment returns [EObject current=null] : iv_ruleUncontainment= ruleUncontainment EOF ;
    public final EObject entryRuleUncontainment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUncontainment = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3240:2: (iv_ruleUncontainment= ruleUncontainment EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3241:2: iv_ruleUncontainment= ruleUncontainment EOF
            {
             newCompositeNode(grammarAccess.getUncontainmentRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleUncontainment_in_entryRuleUncontainment7336);
            iv_ruleUncontainment=ruleUncontainment();

            state._fsp--;

             current =iv_ruleUncontainment; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleUncontainment7346); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUncontainment"


    // $ANTLR start "ruleUncontainment"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3248:1: ruleUncontainment returns [EObject current=null] : (otherlv_0= 'missing' otherlv_1= 'cont' (this_Uncontainment_Impl_2= ruleUncontainment_Impl | this_UncontainmentByMetaClass_3= ruleUncontainmentByMetaClass | this_UncontainmentByObject_4= ruleUncontainmentByObject ) ) ;
    public final EObject ruleUncontainment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject this_Uncontainment_Impl_2 = null;

        EObject this_UncontainmentByMetaClass_3 = null;

        EObject this_UncontainmentByObject_4 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3251:28: ( (otherlv_0= 'missing' otherlv_1= 'cont' (this_Uncontainment_Impl_2= ruleUncontainment_Impl | this_UncontainmentByMetaClass_3= ruleUncontainmentByMetaClass | this_UncontainmentByObject_4= ruleUncontainmentByObject ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3252:1: (otherlv_0= 'missing' otherlv_1= 'cont' (this_Uncontainment_Impl_2= ruleUncontainment_Impl | this_UncontainmentByMetaClass_3= ruleUncontainmentByMetaClass | this_UncontainmentByObject_4= ruleUncontainmentByObject ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3252:1: (otherlv_0= 'missing' otherlv_1= 'cont' (this_Uncontainment_Impl_2= ruleUncontainment_Impl | this_UncontainmentByMetaClass_3= ruleUncontainmentByMetaClass | this_UncontainmentByObject_4= ruleUncontainmentByObject ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3252:3: otherlv_0= 'missing' otherlv_1= 'cont' (this_Uncontainment_Impl_2= ruleUncontainment_Impl | this_UncontainmentByMetaClass_3= ruleUncontainmentByMetaClass | this_UncontainmentByObject_4= ruleUncontainmentByObject )
            {
            otherlv_0=(Token)match(input,50,FollowSets000.FOLLOW_50_in_ruleUncontainment7383); 

                	newLeafNode(otherlv_0, grammarAccess.getUncontainmentAccess().getMissingKeyword_0());
                
            otherlv_1=(Token)match(input,51,FollowSets000.FOLLOW_51_in_ruleUncontainment7395); 

                	newLeafNode(otherlv_1, grammarAccess.getUncontainmentAccess().getContKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3260:1: (this_Uncontainment_Impl_2= ruleUncontainment_Impl | this_UncontainmentByMetaClass_3= ruleUncontainmentByMetaClass | this_UncontainmentByObject_4= ruleUncontainmentByObject )
            int alt57=3;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==53) ) {
                alt57=1;
            }
            else if ( (LA57_0==52) ) {
                int LA57_2 = input.LA(2);

                if ( ((LA57_2>=RULE_STRING && LA57_2<=RULE_ID)) ) {
                    alt57=3;
                }
                else if ( (LA57_2==37) ) {
                    alt57=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 57, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 57, 0, input);

                throw nvae;
            }
            switch (alt57) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3261:5: this_Uncontainment_Impl_2= ruleUncontainment_Impl
                    {
                     
                            newCompositeNode(grammarAccess.getUncontainmentAccess().getUncontainment_ImplParserRuleCall_2_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleUncontainment_Impl_in_ruleUncontainment7418);
                    this_Uncontainment_Impl_2=ruleUncontainment_Impl();

                    state._fsp--;

                     
                            current = this_Uncontainment_Impl_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3271:5: this_UncontainmentByMetaClass_3= ruleUncontainmentByMetaClass
                    {
                     
                            newCompositeNode(grammarAccess.getUncontainmentAccess().getUncontainmentByMetaClassParserRuleCall_2_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleUncontainmentByMetaClass_in_ruleUncontainment7445);
                    this_UncontainmentByMetaClass_3=ruleUncontainmentByMetaClass();

                    state._fsp--;

                     
                            current = this_UncontainmentByMetaClass_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3281:5: this_UncontainmentByObject_4= ruleUncontainmentByObject
                    {
                     
                            newCompositeNode(grammarAccess.getUncontainmentAccess().getUncontainmentByObjectParserRuleCall_2_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleUncontainmentByObject_in_ruleUncontainment7472);
                    this_UncontainmentByObject_4=ruleUncontainmentByObject();

                    state._fsp--;

                     
                            current = this_UncontainmentByObject_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUncontainment"


    // $ANTLR start "entryRuleUncontainmentByMetaClass"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3297:1: entryRuleUncontainmentByMetaClass returns [EObject current=null] : iv_ruleUncontainmentByMetaClass= ruleUncontainmentByMetaClass EOF ;
    public final EObject entryRuleUncontainmentByMetaClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUncontainmentByMetaClass = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3298:2: (iv_ruleUncontainmentByMetaClass= ruleUncontainmentByMetaClass EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3299:2: iv_ruleUncontainmentByMetaClass= ruleUncontainmentByMetaClass EOF
            {
             newCompositeNode(grammarAccess.getUncontainmentByMetaClassRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleUncontainmentByMetaClass_in_entryRuleUncontainmentByMetaClass7508);
            iv_ruleUncontainmentByMetaClass=ruleUncontainmentByMetaClass();

            state._fsp--;

             current =iv_ruleUncontainmentByMetaClass; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleUncontainmentByMetaClass7518); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUncontainmentByMetaClass"


    // $ANTLR start "ruleUncontainmentByMetaClass"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3306:1: ruleUncontainmentByMetaClass returns [EObject current=null] : (otherlv_0= 'from' otherlv_1= 'some' ( (lv_metaclass_2_0= ruleMetaClassExpression ) ) otherlv_3= 'to' ( (lv_object_4_0= ruleObjectExpression ) ) ) ;
    public final EObject ruleUncontainmentByMetaClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_metaclass_2_0 = null;

        EObject lv_object_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3309:28: ( (otherlv_0= 'from' otherlv_1= 'some' ( (lv_metaclass_2_0= ruleMetaClassExpression ) ) otherlv_3= 'to' ( (lv_object_4_0= ruleObjectExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3310:1: (otherlv_0= 'from' otherlv_1= 'some' ( (lv_metaclass_2_0= ruleMetaClassExpression ) ) otherlv_3= 'to' ( (lv_object_4_0= ruleObjectExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3310:1: (otherlv_0= 'from' otherlv_1= 'some' ( (lv_metaclass_2_0= ruleMetaClassExpression ) ) otherlv_3= 'to' ( (lv_object_4_0= ruleObjectExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3310:3: otherlv_0= 'from' otherlv_1= 'some' ( (lv_metaclass_2_0= ruleMetaClassExpression ) ) otherlv_3= 'to' ( (lv_object_4_0= ruleObjectExpression ) )
            {
            otherlv_0=(Token)match(input,52,FollowSets000.FOLLOW_52_in_ruleUncontainmentByMetaClass7555); 

                	newLeafNode(otherlv_0, grammarAccess.getUncontainmentByMetaClassAccess().getFromKeyword_0());
                
            otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_37_in_ruleUncontainmentByMetaClass7567); 

                	newLeafNode(otherlv_1, grammarAccess.getUncontainmentByMetaClassAccess().getSomeKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3318:1: ( (lv_metaclass_2_0= ruleMetaClassExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3319:1: (lv_metaclass_2_0= ruleMetaClassExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3319:1: (lv_metaclass_2_0= ruleMetaClassExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3320:3: lv_metaclass_2_0= ruleMetaClassExpression
            {
             
            	        newCompositeNode(grammarAccess.getUncontainmentByMetaClassAccess().getMetaclassMetaClassExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassExpression_in_ruleUncontainmentByMetaClass7588);
            lv_metaclass_2_0=ruleMetaClassExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getUncontainmentByMetaClassRule());
            	        }
                   		set(
                   			current, 
                   			"metaclass",
                    		lv_metaclass_2_0, 
                    		"MetaClassExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,53,FollowSets000.FOLLOW_53_in_ruleUncontainmentByMetaClass7600); 

                	newLeafNode(otherlv_3, grammarAccess.getUncontainmentByMetaClassAccess().getToKeyword_3());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3340:1: ( (lv_object_4_0= ruleObjectExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3341:1: (lv_object_4_0= ruleObjectExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3341:1: (lv_object_4_0= ruleObjectExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3342:3: lv_object_4_0= ruleObjectExpression
            {
             
            	        newCompositeNode(grammarAccess.getUncontainmentByMetaClassAccess().getObjectObjectExpressionParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleObjectExpression_in_ruleUncontainmentByMetaClass7621);
            lv_object_4_0=ruleObjectExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getUncontainmentByMetaClassRule());
            	        }
                   		set(
                   			current, 
                   			"object",
                    		lv_object_4_0, 
                    		"ObjectExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUncontainmentByMetaClass"


    // $ANTLR start "entryRuleUncontainmentByObject"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3366:1: entryRuleUncontainmentByObject returns [EObject current=null] : iv_ruleUncontainmentByObject= ruleUncontainmentByObject EOF ;
    public final EObject entryRuleUncontainmentByObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUncontainmentByObject = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3367:2: (iv_ruleUncontainmentByObject= ruleUncontainmentByObject EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3368:2: iv_ruleUncontainmentByObject= ruleUncontainmentByObject EOF
            {
             newCompositeNode(grammarAccess.getUncontainmentByObjectRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleUncontainmentByObject_in_entryRuleUncontainmentByObject7657);
            iv_ruleUncontainmentByObject=ruleUncontainmentByObject();

            state._fsp--;

             current =iv_ruleUncontainmentByObject; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleUncontainmentByObject7667); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUncontainmentByObject"


    // $ANTLR start "ruleUncontainmentByObject"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3375:1: ruleUncontainmentByObject returns [EObject current=null] : (otherlv_0= 'from' ( (lv_container_1_0= ruleObjectExpression ) ) otherlv_2= 'to' ( (lv_object_3_0= ruleObjectExpression ) ) ) ;
    public final EObject ruleUncontainmentByObject() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_container_1_0 = null;

        EObject lv_object_3_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3378:28: ( (otherlv_0= 'from' ( (lv_container_1_0= ruleObjectExpression ) ) otherlv_2= 'to' ( (lv_object_3_0= ruleObjectExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3379:1: (otherlv_0= 'from' ( (lv_container_1_0= ruleObjectExpression ) ) otherlv_2= 'to' ( (lv_object_3_0= ruleObjectExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3379:1: (otherlv_0= 'from' ( (lv_container_1_0= ruleObjectExpression ) ) otherlv_2= 'to' ( (lv_object_3_0= ruleObjectExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3379:3: otherlv_0= 'from' ( (lv_container_1_0= ruleObjectExpression ) ) otherlv_2= 'to' ( (lv_object_3_0= ruleObjectExpression ) )
            {
            otherlv_0=(Token)match(input,52,FollowSets000.FOLLOW_52_in_ruleUncontainmentByObject7704); 

                	newLeafNode(otherlv_0, grammarAccess.getUncontainmentByObjectAccess().getFromKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3383:1: ( (lv_container_1_0= ruleObjectExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3384:1: (lv_container_1_0= ruleObjectExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3384:1: (lv_container_1_0= ruleObjectExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3385:3: lv_container_1_0= ruleObjectExpression
            {
             
            	        newCompositeNode(grammarAccess.getUncontainmentByObjectAccess().getContainerObjectExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleObjectExpression_in_ruleUncontainmentByObject7725);
            lv_container_1_0=ruleObjectExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getUncontainmentByObjectRule());
            	        }
                   		set(
                   			current, 
                   			"container",
                    		lv_container_1_0, 
                    		"ObjectExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_2=(Token)match(input,53,FollowSets000.FOLLOW_53_in_ruleUncontainmentByObject7737); 

                	newLeafNode(otherlv_2, grammarAccess.getUncontainmentByObjectAccess().getToKeyword_2());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3405:1: ( (lv_object_3_0= ruleObjectExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3406:1: (lv_object_3_0= ruleObjectExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3406:1: (lv_object_3_0= ruleObjectExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3407:3: lv_object_3_0= ruleObjectExpression
            {
             
            	        newCompositeNode(grammarAccess.getUncontainmentByObjectAccess().getObjectObjectExpressionParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleObjectExpression_in_ruleUncontainmentByObject7758);
            lv_object_3_0=ruleObjectExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getUncontainmentByObjectRule());
            	        }
                   		set(
                   			current, 
                   			"object",
                    		lv_object_3_0, 
                    		"ObjectExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUncontainmentByObject"


    // $ANTLR start "entryRuleUncontainment_Impl"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3431:1: entryRuleUncontainment_Impl returns [EObject current=null] : iv_ruleUncontainment_Impl= ruleUncontainment_Impl EOF ;
    public final EObject entryRuleUncontainment_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUncontainment_Impl = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3432:2: (iv_ruleUncontainment_Impl= ruleUncontainment_Impl EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3433:2: iv_ruleUncontainment_Impl= ruleUncontainment_Impl EOF
            {
             newCompositeNode(grammarAccess.getUncontainment_ImplRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleUncontainment_Impl_in_entryRuleUncontainment_Impl7794);
            iv_ruleUncontainment_Impl=ruleUncontainment_Impl();

            state._fsp--;

             current =iv_ruleUncontainment_Impl; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleUncontainment_Impl7804); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUncontainment_Impl"


    // $ANTLR start "ruleUncontainment_Impl"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3440:1: ruleUncontainment_Impl returns [EObject current=null] : ( () otherlv_1= 'to' ( (lv_object_2_0= ruleObjectExpression ) ) ) ;
    public final EObject ruleUncontainment_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_object_2_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3443:28: ( ( () otherlv_1= 'to' ( (lv_object_2_0= ruleObjectExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3444:1: ( () otherlv_1= 'to' ( (lv_object_2_0= ruleObjectExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3444:1: ( () otherlv_1= 'to' ( (lv_object_2_0= ruleObjectExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3444:2: () otherlv_1= 'to' ( (lv_object_2_0= ruleObjectExpression ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3444:2: ()
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3445:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getUncontainment_ImplAccess().getUncontainmentAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,53,FollowSets000.FOLLOW_53_in_ruleUncontainment_Impl7850); 

                	newLeafNode(otherlv_1, grammarAccess.getUncontainment_ImplAccess().getToKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3454:1: ( (lv_object_2_0= ruleObjectExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3455:1: (lv_object_2_0= ruleObjectExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3455:1: (lv_object_2_0= ruleObjectExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3456:3: lv_object_2_0= ruleObjectExpression
            {
             
            	        newCompositeNode(grammarAccess.getUncontainment_ImplAccess().getObjectObjectExpressionParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleObjectExpression_in_ruleUncontainment_Impl7871);
            lv_object_2_0=ruleObjectExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getUncontainment_ImplRule());
            	        }
                   		set(
                   			current, 
                   			"object",
                    		lv_object_2_0, 
                    		"ObjectExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUncontainment_Impl"


    // $ANTLR start "entryRuleFeatureExistence"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3480:1: entryRuleFeatureExistence returns [EObject current=null] : iv_ruleFeatureExistence= ruleFeatureExistence EOF ;
    public final EObject entryRuleFeatureExistence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureExistence = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3481:2: (iv_ruleFeatureExistence= ruleFeatureExistence EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3482:2: iv_ruleFeatureExistence= ruleFeatureExistence EOF
            {
             newCompositeNode(grammarAccess.getFeatureExistenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeatureExistence_in_entryRuleFeatureExistence7907);
            iv_ruleFeatureExistence=ruleFeatureExistence();

            state._fsp--;

             current =iv_ruleFeatureExistence; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureExistence7917); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureExistence"


    // $ANTLR start "ruleFeatureExistence"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3489:1: ruleFeatureExistence returns [EObject current=null] : (otherlv_0= 'unknown' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) ) ;
    public final EObject ruleFeatureExistence() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_feature_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3492:28: ( (otherlv_0= 'unknown' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3493:1: (otherlv_0= 'unknown' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3493:1: (otherlv_0= 'unknown' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3493:3: otherlv_0= 'unknown' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) )
            {
            otherlv_0=(Token)match(input,54,FollowSets000.FOLLOW_54_in_ruleFeatureExistence7954); 

                	newLeafNode(otherlv_0, grammarAccess.getFeatureExistenceAccess().getUnknownKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3497:1: ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3498:1: (lv_feature_1_0= ruleFeatureWithinObjectExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3498:1: (lv_feature_1_0= ruleFeatureWithinObjectExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3499:3: lv_feature_1_0= ruleFeatureWithinObjectExpression
            {
             
            	        newCompositeNode(grammarAccess.getFeatureExistenceAccess().getFeatureFeatureWithinObjectExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFeatureWithinObjectExpression_in_ruleFeatureExistence7975);
            lv_feature_1_0=ruleFeatureWithinObjectExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFeatureExistenceRule());
            	        }
                   		set(
                   			current, 
                   			"feature",
                    		lv_feature_1_0, 
                    		"FeatureWithinObjectExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureExistence"


    // $ANTLR start "entryRuleTypeExistence"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3523:1: entryRuleTypeExistence returns [EObject current=null] : iv_ruleTypeExistence= ruleTypeExistence EOF ;
    public final EObject entryRuleTypeExistence() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeExistence = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3524:2: (iv_ruleTypeExistence= ruleTypeExistence EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3525:2: iv_ruleTypeExistence= ruleTypeExistence EOF
            {
             newCompositeNode(grammarAccess.getTypeExistenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleTypeExistence_in_entryRuleTypeExistence8011);
            iv_ruleTypeExistence=ruleTypeExistence();

            state._fsp--;

             current =iv_ruleTypeExistence; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTypeExistence8021); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeExistence"


    // $ANTLR start "ruleTypeExistence"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3532:1: ruleTypeExistence returns [EObject current=null] : (otherlv_0= 'unknown' ( (lv_type_1_0= ruleObjectTypeExpression ) ) ) ;
    public final EObject ruleTypeExistence() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_type_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3535:28: ( (otherlv_0= 'unknown' ( (lv_type_1_0= ruleObjectTypeExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3536:1: (otherlv_0= 'unknown' ( (lv_type_1_0= ruleObjectTypeExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3536:1: (otherlv_0= 'unknown' ( (lv_type_1_0= ruleObjectTypeExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3536:3: otherlv_0= 'unknown' ( (lv_type_1_0= ruleObjectTypeExpression ) )
            {
            otherlv_0=(Token)match(input,54,FollowSets000.FOLLOW_54_in_ruleTypeExistence8058); 

                	newLeafNode(otherlv_0, grammarAccess.getTypeExistenceAccess().getUnknownKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3540:1: ( (lv_type_1_0= ruleObjectTypeExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3541:1: (lv_type_1_0= ruleObjectTypeExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3541:1: (lv_type_1_0= ruleObjectTypeExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3542:3: lv_type_1_0= ruleObjectTypeExpression
            {
             
            	        newCompositeNode(grammarAccess.getTypeExistenceAccess().getTypeObjectTypeExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleObjectTypeExpression_in_ruleTypeExistence8079);
            lv_type_1_0=ruleObjectTypeExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTypeExistenceRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_1_0, 
                    		"ObjectTypeExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeExistence"


    // $ANTLR start "entryRuleAbstractTypeInstance"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3566:1: entryRuleAbstractTypeInstance returns [EObject current=null] : iv_ruleAbstractTypeInstance= ruleAbstractTypeInstance EOF ;
    public final EObject entryRuleAbstractTypeInstance() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractTypeInstance = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3567:2: (iv_ruleAbstractTypeInstance= ruleAbstractTypeInstance EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3568:2: iv_ruleAbstractTypeInstance= ruleAbstractTypeInstance EOF
            {
             newCompositeNode(grammarAccess.getAbstractTypeInstanceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAbstractTypeInstance_in_entryRuleAbstractTypeInstance8115);
            iv_ruleAbstractTypeInstance=ruleAbstractTypeInstance();

            state._fsp--;

             current =iv_ruleAbstractTypeInstance; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAbstractTypeInstance8125); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractTypeInstance"


    // $ANTLR start "ruleAbstractTypeInstance"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3575:1: ruleAbstractTypeInstance returns [EObject current=null] : (otherlv_0= 'abstract' ( (lv_type_1_0= ruleObjectTypeExpression ) ) ) ;
    public final EObject ruleAbstractTypeInstance() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_type_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3578:28: ( (otherlv_0= 'abstract' ( (lv_type_1_0= ruleObjectTypeExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3579:1: (otherlv_0= 'abstract' ( (lv_type_1_0= ruleObjectTypeExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3579:1: (otherlv_0= 'abstract' ( (lv_type_1_0= ruleObjectTypeExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3579:3: otherlv_0= 'abstract' ( (lv_type_1_0= ruleObjectTypeExpression ) )
            {
            otherlv_0=(Token)match(input,55,FollowSets000.FOLLOW_55_in_ruleAbstractTypeInstance8162); 

                	newLeafNode(otherlv_0, grammarAccess.getAbstractTypeInstanceAccess().getAbstractKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3583:1: ( (lv_type_1_0= ruleObjectTypeExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3584:1: (lv_type_1_0= ruleObjectTypeExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3584:1: (lv_type_1_0= ruleObjectTypeExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3585:3: lv_type_1_0= ruleObjectTypeExpression
            {
             
            	        newCompositeNode(grammarAccess.getAbstractTypeInstanceAccess().getTypeObjectTypeExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleObjectTypeExpression_in_ruleAbstractTypeInstance8183);
            lv_type_1_0=ruleObjectTypeExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAbstractTypeInstanceRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_1_0, 
                    		"ObjectTypeExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractTypeInstance"


    // $ANTLR start "entryRuleMissingAttribute"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3609:1: entryRuleMissingAttribute returns [EObject current=null] : iv_ruleMissingAttribute= ruleMissingAttribute EOF ;
    public final EObject entryRuleMissingAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMissingAttribute = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3610:2: (iv_ruleMissingAttribute= ruleMissingAttribute EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3611:2: iv_ruleMissingAttribute= ruleMissingAttribute EOF
            {
             newCompositeNode(grammarAccess.getMissingAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMissingAttribute_in_entryRuleMissingAttribute8219);
            iv_ruleMissingAttribute=ruleMissingAttribute();

            state._fsp--;

             current =iv_ruleMissingAttribute; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMissingAttribute8229); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMissingAttribute"


    // $ANTLR start "ruleMissingAttribute"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3618:1: ruleMissingAttribute returns [EObject current=null] : (otherlv_0= 'missing' otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= 'from' ( ( ruleEString ) ) ) ;
    public final EObject ruleMissingAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3621:28: ( (otherlv_0= 'missing' otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= 'from' ( ( ruleEString ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3622:1: (otherlv_0= 'missing' otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= 'from' ( ( ruleEString ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3622:1: (otherlv_0= 'missing' otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= 'from' ( ( ruleEString ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3622:3: otherlv_0= 'missing' otherlv_1= 'attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= 'from' ( ( ruleEString ) )
            {
            otherlv_0=(Token)match(input,50,FollowSets000.FOLLOW_50_in_ruleMissingAttribute8266); 

                	newLeafNode(otherlv_0, grammarAccess.getMissingAttributeAccess().getMissingKeyword_0());
                
            otherlv_1=(Token)match(input,56,FollowSets000.FOLLOW_56_in_ruleMissingAttribute8278); 

                	newLeafNode(otherlv_1, grammarAccess.getMissingAttributeAccess().getAttributeKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3630:1: ( (lv_name_2_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3631:1: (lv_name_2_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3631:1: (lv_name_2_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3632:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getMissingAttributeAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleMissingAttribute8299);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMissingAttributeRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,52,FollowSets000.FOLLOW_52_in_ruleMissingAttribute8311); 

                	newLeafNode(otherlv_3, grammarAccess.getMissingAttributeAccess().getFromKeyword_3());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3652:1: ( ( ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3653:1: ( ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3653:1: ( ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3654:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getMissingAttributeRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getMissingAttributeAccess().getObjectObjectCrossReference_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleMissingAttribute8334);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMissingAttribute"


    // $ANTLR start "entryRuleMissingReference"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3675:1: entryRuleMissingReference returns [EObject current=null] : iv_ruleMissingReference= ruleMissingReference EOF ;
    public final EObject entryRuleMissingReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMissingReference = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3676:2: (iv_ruleMissingReference= ruleMissingReference EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3677:2: iv_ruleMissingReference= ruleMissingReference EOF
            {
             newCompositeNode(grammarAccess.getMissingReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMissingReference_in_entryRuleMissingReference8370);
            iv_ruleMissingReference=ruleMissingReference();

            state._fsp--;

             current =iv_ruleMissingReference; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMissingReference8380); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMissingReference"


    // $ANTLR start "ruleMissingReference"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3684:1: ruleMissingReference returns [EObject current=null] : (otherlv_0= 'missing' otherlv_1= 'reference' ( (otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) ) ) ) ) ;
    public final EObject ruleMissingReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_from_3_0 = null;

        EObject lv_to_5_0 = null;

        EObject lv_from_7_0 = null;

        EObject lv_to_9_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3687:28: ( (otherlv_0= 'missing' otherlv_1= 'reference' ( (otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) ) ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3688:1: (otherlv_0= 'missing' otherlv_1= 'reference' ( (otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) ) ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3688:1: (otherlv_0= 'missing' otherlv_1= 'reference' ( (otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3688:3: otherlv_0= 'missing' otherlv_1= 'reference' ( (otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) ) ) )
            {
            otherlv_0=(Token)match(input,50,FollowSets000.FOLLOW_50_in_ruleMissingReference8417); 

                	newLeafNode(otherlv_0, grammarAccess.getMissingReferenceAccess().getMissingKeyword_0());
                
            otherlv_1=(Token)match(input,47,FollowSets000.FOLLOW_47_in_ruleMissingReference8429); 

                	newLeafNode(otherlv_1, grammarAccess.getMissingReferenceAccess().getReferenceKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3696:1: ( (otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) ) ) )
            int alt58=3;
            alt58 = dfa58.predict(input);
            switch (alt58) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3696:2: (otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3696:2: (otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3696:4: otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) )
                    {
                    otherlv_2=(Token)match(input,52,FollowSets000.FOLLOW_52_in_ruleMissingReference8443); 

                        	newLeafNode(otherlv_2, grammarAccess.getMissingReferenceAccess().getFromKeyword_2_0_0());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3700:1: ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3701:1: (lv_from_3_0= ruleObjectOrMetaClassExpression )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3701:1: (lv_from_3_0= ruleObjectOrMetaClassExpression )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3702:3: lv_from_3_0= ruleObjectOrMetaClassExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getMissingReferenceAccess().getFromObjectOrMetaClassExpressionParserRuleCall_2_0_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleObjectOrMetaClassExpression_in_ruleMissingReference8464);
                    lv_from_3_0=ruleObjectOrMetaClassExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMissingReferenceRule());
                    	        }
                           		set(
                           			current, 
                           			"from",
                            		lv_from_3_0, 
                            		"ObjectOrMetaClassExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3719:6: (otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3719:6: (otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3719:8: otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) )
                    {
                    otherlv_4=(Token)match(input,53,FollowSets000.FOLLOW_53_in_ruleMissingReference8484); 

                        	newLeafNode(otherlv_4, grammarAccess.getMissingReferenceAccess().getToKeyword_2_1_0());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3723:1: ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3724:1: (lv_to_5_0= ruleObjectOrMetaClassExpression )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3724:1: (lv_to_5_0= ruleObjectOrMetaClassExpression )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3725:3: lv_to_5_0= ruleObjectOrMetaClassExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getMissingReferenceAccess().getToObjectOrMetaClassExpressionParserRuleCall_2_1_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleObjectOrMetaClassExpression_in_ruleMissingReference8505);
                    lv_to_5_0=ruleObjectOrMetaClassExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMissingReferenceRule());
                    	        }
                           		set(
                           			current, 
                           			"to",
                            		lv_to_5_0, 
                            		"ObjectOrMetaClassExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3742:6: (otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) ) )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3742:6: (otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3742:8: otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) )
                    {
                    otherlv_6=(Token)match(input,52,FollowSets000.FOLLOW_52_in_ruleMissingReference8525); 

                        	newLeafNode(otherlv_6, grammarAccess.getMissingReferenceAccess().getFromKeyword_2_2_0());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3746:1: ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3747:1: (lv_from_7_0= ruleObjectOrMetaClassExpression )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3747:1: (lv_from_7_0= ruleObjectOrMetaClassExpression )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3748:3: lv_from_7_0= ruleObjectOrMetaClassExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getMissingReferenceAccess().getFromObjectOrMetaClassExpressionParserRuleCall_2_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleObjectOrMetaClassExpression_in_ruleMissingReference8546);
                    lv_from_7_0=ruleObjectOrMetaClassExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMissingReferenceRule());
                    	        }
                           		set(
                           			current, 
                           			"from",
                            		lv_from_7_0, 
                            		"ObjectOrMetaClassExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_8=(Token)match(input,53,FollowSets000.FOLLOW_53_in_ruleMissingReference8558); 

                        	newLeafNode(otherlv_8, grammarAccess.getMissingReferenceAccess().getToKeyword_2_2_2());
                        
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3768:1: ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3769:1: (lv_to_9_0= ruleObjectOrMetaClassExpression )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3769:1: (lv_to_9_0= ruleObjectOrMetaClassExpression )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3770:3: lv_to_9_0= ruleObjectOrMetaClassExpression
                    {
                     
                    	        newCompositeNode(grammarAccess.getMissingReferenceAccess().getToObjectOrMetaClassExpressionParserRuleCall_2_2_3_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleObjectOrMetaClassExpression_in_ruleMissingReference8579);
                    lv_to_9_0=ruleObjectOrMetaClassExpression();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getMissingReferenceRule());
                    	        }
                           		set(
                           			current, 
                           			"to",
                            		lv_to_9_0, 
                            		"ObjectOrMetaClassExpression");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMissingReference"


    // $ANTLR start "entryRuleMultiplicityMismatch"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3794:1: entryRuleMultiplicityMismatch returns [EObject current=null] : iv_ruleMultiplicityMismatch= ruleMultiplicityMismatch EOF ;
    public final EObject entryRuleMultiplicityMismatch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicityMismatch = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3795:2: (iv_ruleMultiplicityMismatch= ruleMultiplicityMismatch EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3796:2: iv_ruleMultiplicityMismatch= ruleMultiplicityMismatch EOF
            {
             newCompositeNode(grammarAccess.getMultiplicityMismatchRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMultiplicityMismatch_in_entryRuleMultiplicityMismatch8617);
            iv_ruleMultiplicityMismatch=ruleMultiplicityMismatch();

            state._fsp--;

             current =iv_ruleMultiplicityMismatch; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMultiplicityMismatch8627); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicityMismatch"


    // $ANTLR start "ruleMultiplicityMismatch"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3803:1: ruleMultiplicityMismatch returns [EObject current=null] : (otherlv_0= 'mismatch on' ( (lv_featureMultiplicity_1_0= ruleMultiplicityExpression ) ) ) ;
    public final EObject ruleMultiplicityMismatch() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_featureMultiplicity_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3806:28: ( (otherlv_0= 'mismatch on' ( (lv_featureMultiplicity_1_0= ruleMultiplicityExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3807:1: (otherlv_0= 'mismatch on' ( (lv_featureMultiplicity_1_0= ruleMultiplicityExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3807:1: (otherlv_0= 'mismatch on' ( (lv_featureMultiplicity_1_0= ruleMultiplicityExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3807:3: otherlv_0= 'mismatch on' ( (lv_featureMultiplicity_1_0= ruleMultiplicityExpression ) )
            {
            otherlv_0=(Token)match(input,57,FollowSets000.FOLLOW_57_in_ruleMultiplicityMismatch8664); 

                	newLeafNode(otherlv_0, grammarAccess.getMultiplicityMismatchAccess().getMismatchOnKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3811:1: ( (lv_featureMultiplicity_1_0= ruleMultiplicityExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3812:1: (lv_featureMultiplicity_1_0= ruleMultiplicityExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3812:1: (lv_featureMultiplicity_1_0= ruleMultiplicityExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3813:3: lv_featureMultiplicity_1_0= ruleMultiplicityExpression
            {
             
            	        newCompositeNode(grammarAccess.getMultiplicityMismatchAccess().getFeatureMultiplicityMultiplicityExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleMultiplicityExpression_in_ruleMultiplicityMismatch8685);
            lv_featureMultiplicity_1_0=ruleMultiplicityExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMultiplicityMismatchRule());
            	        }
                   		set(
                   			current, 
                   			"featureMultiplicity",
                    		lv_featureMultiplicity_1_0, 
                    		"MultiplicityExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicityMismatch"


    // $ANTLR start "entryRuleTypeMismatch"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3837:1: entryRuleTypeMismatch returns [EObject current=null] : iv_ruleTypeMismatch= ruleTypeMismatch EOF ;
    public final EObject entryRuleTypeMismatch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTypeMismatch = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3838:2: (iv_ruleTypeMismatch= ruleTypeMismatch EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3839:2: iv_ruleTypeMismatch= ruleTypeMismatch EOF
            {
             newCompositeNode(grammarAccess.getTypeMismatchRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleTypeMismatch_in_entryRuleTypeMismatch8721);
            iv_ruleTypeMismatch=ruleTypeMismatch();

            state._fsp--;

             current =iv_ruleTypeMismatch; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleTypeMismatch8731); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTypeMismatch"


    // $ANTLR start "ruleTypeMismatch"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3846:1: ruleTypeMismatch returns [EObject current=null] : (otherlv_0= 'mismatch on' ( (lv_featureType_1_0= ruleFeatureTypeExpression ) ) ) ;
    public final EObject ruleTypeMismatch() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_featureType_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3849:28: ( (otherlv_0= 'mismatch on' ( (lv_featureType_1_0= ruleFeatureTypeExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3850:1: (otherlv_0= 'mismatch on' ( (lv_featureType_1_0= ruleFeatureTypeExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3850:1: (otherlv_0= 'mismatch on' ( (lv_featureType_1_0= ruleFeatureTypeExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3850:3: otherlv_0= 'mismatch on' ( (lv_featureType_1_0= ruleFeatureTypeExpression ) )
            {
            otherlv_0=(Token)match(input,57,FollowSets000.FOLLOW_57_in_ruleTypeMismatch8768); 

                	newLeafNode(otherlv_0, grammarAccess.getTypeMismatchAccess().getMismatchOnKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3854:1: ( (lv_featureType_1_0= ruleFeatureTypeExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3855:1: (lv_featureType_1_0= ruleFeatureTypeExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3855:1: (lv_featureType_1_0= ruleFeatureTypeExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3856:3: lv_featureType_1_0= ruleFeatureTypeExpression
            {
             
            	        newCompositeNode(grammarAccess.getTypeMismatchAccess().getFeatureTypeFeatureTypeExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFeatureTypeExpression_in_ruleTypeMismatch8789);
            lv_featureType_1_0=ruleFeatureTypeExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getTypeMismatchRule());
            	        }
                   		set(
                   			current, 
                   			"featureType",
                    		lv_featureType_1_0, 
                    		"FeatureTypeExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeMismatch"


    // $ANTLR start "entryRuleFeatureNatureMismatch"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3880:1: entryRuleFeatureNatureMismatch returns [EObject current=null] : iv_ruleFeatureNatureMismatch= ruleFeatureNatureMismatch EOF ;
    public final EObject entryRuleFeatureNatureMismatch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureNatureMismatch = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3881:2: (iv_ruleFeatureNatureMismatch= ruleFeatureNatureMismatch EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3882:2: iv_ruleFeatureNatureMismatch= ruleFeatureNatureMismatch EOF
            {
             newCompositeNode(grammarAccess.getFeatureNatureMismatchRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeatureNatureMismatch_in_entryRuleFeatureNatureMismatch8825);
            iv_ruleFeatureNatureMismatch=ruleFeatureNatureMismatch();

            state._fsp--;

             current =iv_ruleFeatureNatureMismatch; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureNatureMismatch8835); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureNatureMismatch"


    // $ANTLR start "ruleFeatureNatureMismatch"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3889:1: ruleFeatureNatureMismatch returns [EObject current=null] : (otherlv_0= 'mismatch on' ( (lv_featureNature_1_0= ruleFeatureNatureExpression ) ) ) ;
    public final EObject ruleFeatureNatureMismatch() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_featureNature_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3892:28: ( (otherlv_0= 'mismatch on' ( (lv_featureNature_1_0= ruleFeatureNatureExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3893:1: (otherlv_0= 'mismatch on' ( (lv_featureNature_1_0= ruleFeatureNatureExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3893:1: (otherlv_0= 'mismatch on' ( (lv_featureNature_1_0= ruleFeatureNatureExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3893:3: otherlv_0= 'mismatch on' ( (lv_featureNature_1_0= ruleFeatureNatureExpression ) )
            {
            otherlv_0=(Token)match(input,57,FollowSets000.FOLLOW_57_in_ruleFeatureNatureMismatch8872); 

                	newLeafNode(otherlv_0, grammarAccess.getFeatureNatureMismatchAccess().getMismatchOnKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3897:1: ( (lv_featureNature_1_0= ruleFeatureNatureExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3898:1: (lv_featureNature_1_0= ruleFeatureNatureExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3898:1: (lv_featureNature_1_0= ruleFeatureNatureExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3899:3: lv_featureNature_1_0= ruleFeatureNatureExpression
            {
             
            	        newCompositeNode(grammarAccess.getFeatureNatureMismatchAccess().getFeatureNatureFeatureNatureExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFeatureNatureExpression_in_ruleFeatureNatureMismatch8893);
            lv_featureNature_1_0=ruleFeatureNatureExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFeatureNatureMismatchRule());
            	        }
                   		set(
                   			current, 
                   			"featureNature",
                    		lv_featureNature_1_0, 
                    		"FeatureNatureExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureNatureMismatch"


    // $ANTLR start "entryRuleRestrictionViolation"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3923:1: entryRuleRestrictionViolation returns [EObject current=null] : iv_ruleRestrictionViolation= ruleRestrictionViolation EOF ;
    public final EObject entryRuleRestrictionViolation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRestrictionViolation = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3924:2: (iv_ruleRestrictionViolation= ruleRestrictionViolation EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3925:2: iv_ruleRestrictionViolation= ruleRestrictionViolation EOF
            {
             newCompositeNode(grammarAccess.getRestrictionViolationRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleRestrictionViolation_in_entryRuleRestrictionViolation8929);
            iv_ruleRestrictionViolation=ruleRestrictionViolation();

            state._fsp--;

             current =iv_ruleRestrictionViolation; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleRestrictionViolation8939); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRestrictionViolation"


    // $ANTLR start "ruleRestrictionViolation"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3932:1: ruleRestrictionViolation returns [EObject current=null] : (otherlv_0= 'constraint violation' otherlv_1= '@' ( (lv_restrictionName_2_0= ruleEString ) ) otherlv_3= 'on' ( (lv_fragmentElement_4_0= ruleFragmentElementExpression ) ) ) ;
    public final EObject ruleRestrictionViolation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_restrictionName_2_0 = null;

        EObject lv_fragmentElement_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3935:28: ( (otherlv_0= 'constraint violation' otherlv_1= '@' ( (lv_restrictionName_2_0= ruleEString ) ) otherlv_3= 'on' ( (lv_fragmentElement_4_0= ruleFragmentElementExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3936:1: (otherlv_0= 'constraint violation' otherlv_1= '@' ( (lv_restrictionName_2_0= ruleEString ) ) otherlv_3= 'on' ( (lv_fragmentElement_4_0= ruleFragmentElementExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3936:1: (otherlv_0= 'constraint violation' otherlv_1= '@' ( (lv_restrictionName_2_0= ruleEString ) ) otherlv_3= 'on' ( (lv_fragmentElement_4_0= ruleFragmentElementExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3936:3: otherlv_0= 'constraint violation' otherlv_1= '@' ( (lv_restrictionName_2_0= ruleEString ) ) otherlv_3= 'on' ( (lv_fragmentElement_4_0= ruleFragmentElementExpression ) )
            {
            otherlv_0=(Token)match(input,58,FollowSets000.FOLLOW_58_in_ruleRestrictionViolation8976); 

                	newLeafNode(otherlv_0, grammarAccess.getRestrictionViolationAccess().getConstraintViolationKeyword_0());
                
            otherlv_1=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleRestrictionViolation8988); 

                	newLeafNode(otherlv_1, grammarAccess.getRestrictionViolationAccess().getCommercialAtKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3944:1: ( (lv_restrictionName_2_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3945:1: (lv_restrictionName_2_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3945:1: (lv_restrictionName_2_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3946:3: lv_restrictionName_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getRestrictionViolationAccess().getRestrictionNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleRestrictionViolation9009);
            lv_restrictionName_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRestrictionViolationRule());
            	        }
                   		set(
                   			current, 
                   			"restrictionName",
                    		lv_restrictionName_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,59,FollowSets000.FOLLOW_59_in_ruleRestrictionViolation9021); 

                	newLeafNode(otherlv_3, grammarAccess.getRestrictionViolationAccess().getOnKeyword_3());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3966:1: ( (lv_fragmentElement_4_0= ruleFragmentElementExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3967:1: (lv_fragmentElement_4_0= ruleFragmentElementExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3967:1: (lv_fragmentElement_4_0= ruleFragmentElementExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3968:3: lv_fragmentElement_4_0= ruleFragmentElementExpression
            {
             
            	        newCompositeNode(grammarAccess.getRestrictionViolationAccess().getFragmentElementFragmentElementExpressionParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFragmentElementExpression_in_ruleRestrictionViolation9042);
            lv_fragmentElement_4_0=ruleFragmentElementExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getRestrictionViolationRule());
            	        }
                   		set(
                   			current, 
                   			"fragmentElement",
                    		lv_fragmentElement_4_0, 
                    		"FragmentElementExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRestrictionViolation"


    // $ANTLR start "entryRuleMetaClassExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3992:1: entryRuleMetaClassExpression returns [EObject current=null] : iv_ruleMetaClassExpression= ruleMetaClassExpression EOF ;
    public final EObject entryRuleMetaClassExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMetaClassExpression = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3993:2: (iv_ruleMetaClassExpression= ruleMetaClassExpression EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:3994:2: iv_ruleMetaClassExpression= ruleMetaClassExpression EOF
            {
             newCompositeNode(grammarAccess.getMetaClassExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClassExpression_in_entryRuleMetaClassExpression9078);
            iv_ruleMetaClassExpression=ruleMetaClassExpression();

            state._fsp--;

             current =iv_ruleMetaClassExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaClassExpression9088); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMetaClassExpression"


    // $ANTLR start "ruleMetaClassExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4001:1: ruleMetaClassExpression returns [EObject current=null] : ( (lv_metaclassName_0_0= ruleEString ) ) ;
    public final EObject ruleMetaClassExpression() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_metaclassName_0_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4004:28: ( ( (lv_metaclassName_0_0= ruleEString ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4005:1: ( (lv_metaclassName_0_0= ruleEString ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4005:1: ( (lv_metaclassName_0_0= ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4006:1: (lv_metaclassName_0_0= ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4006:1: (lv_metaclassName_0_0= ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4007:3: lv_metaclassName_0_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getMetaClassExpressionAccess().getMetaclassNameEStringParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleMetaClassExpression9133);
            lv_metaclassName_0_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMetaClassExpressionRule());
            	        }
                   		set(
                   			current, 
                   			"metaclassName",
                    		lv_metaclassName_0_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMetaClassExpression"


    // $ANTLR start "entryRuleFragmentElementExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4031:1: entryRuleFragmentElementExpression returns [EObject current=null] : iv_ruleFragmentElementExpression= ruleFragmentElementExpression EOF ;
    public final EObject entryRuleFragmentElementExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragmentElementExpression = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4032:2: (iv_ruleFragmentElementExpression= ruleFragmentElementExpression EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4033:2: iv_ruleFragmentElementExpression= ruleFragmentElementExpression EOF
            {
             newCompositeNode(grammarAccess.getFragmentElementExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentElementExpression_in_entryRuleFragmentElementExpression9168);
            iv_ruleFragmentElementExpression=ruleFragmentElementExpression();

            state._fsp--;

             current =iv_ruleFragmentElementExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentElementExpression9178); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragmentElementExpression"


    // $ANTLR start "ruleFragmentElementExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4040:1: ruleFragmentElementExpression returns [EObject current=null] : (this_FeatureWithinObjectExpression_0= ruleFeatureWithinObjectExpression | this_ObjectExpression_1= ruleObjectExpression ) ;
    public final EObject ruleFragmentElementExpression() throws RecognitionException {
        EObject current = null;

        EObject this_FeatureWithinObjectExpression_0 = null;

        EObject this_ObjectExpression_1 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4043:28: ( (this_FeatureWithinObjectExpression_0= ruleFeatureWithinObjectExpression | this_ObjectExpression_1= ruleObjectExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4044:1: (this_FeatureWithinObjectExpression_0= ruleFeatureWithinObjectExpression | this_ObjectExpression_1= ruleObjectExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4044:1: (this_FeatureWithinObjectExpression_0= ruleFeatureWithinObjectExpression | this_ObjectExpression_1= ruleObjectExpression )
            int alt59=2;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==RULE_STRING) ) {
                int LA59_1 = input.LA(2);

                if ( (LA59_1==25) ) {
                    int LA59_3 = input.LA(3);

                    if ( ((LA59_3>=RULE_STRING && LA59_3<=RULE_ID)) ) {
                        alt59=1;
                    }
                    else if ( (LA59_3==50||(LA59_3>=54 && LA59_3<=55)||(LA59_3>=57 && LA59_3<=58)) ) {
                        alt59=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 59, 3, input);

                        throw nvae;
                    }
                }
                else if ( (LA59_1==EOF||LA59_1==13) ) {
                    alt59=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 59, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA59_0==RULE_ID) ) {
                int LA59_2 = input.LA(2);

                if ( (LA59_2==25) ) {
                    int LA59_3 = input.LA(3);

                    if ( ((LA59_3>=RULE_STRING && LA59_3<=RULE_ID)) ) {
                        alt59=1;
                    }
                    else if ( (LA59_3==50||(LA59_3>=54 && LA59_3<=55)||(LA59_3>=57 && LA59_3<=58)) ) {
                        alt59=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 59, 3, input);

                        throw nvae;
                    }
                }
                else if ( (LA59_2==EOF||LA59_2==13) ) {
                    alt59=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 59, 2, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 59, 0, input);

                throw nvae;
            }
            switch (alt59) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4045:5: this_FeatureWithinObjectExpression_0= ruleFeatureWithinObjectExpression
                    {
                     
                            newCompositeNode(grammarAccess.getFragmentElementExpressionAccess().getFeatureWithinObjectExpressionParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleFeatureWithinObjectExpression_in_ruleFragmentElementExpression9225);
                    this_FeatureWithinObjectExpression_0=ruleFeatureWithinObjectExpression();

                    state._fsp--;

                     
                            current = this_FeatureWithinObjectExpression_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4055:5: this_ObjectExpression_1= ruleObjectExpression
                    {
                     
                            newCompositeNode(grammarAccess.getFragmentElementExpressionAccess().getObjectExpressionParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleObjectExpression_in_ruleFragmentElementExpression9252);
                    this_ObjectExpression_1=ruleObjectExpression();

                    state._fsp--;

                     
                            current = this_ObjectExpression_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentElementExpression"


    // $ANTLR start "entryRuleObjectTypeExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4071:1: entryRuleObjectTypeExpression returns [EObject current=null] : iv_ruleObjectTypeExpression= ruleObjectTypeExpression EOF ;
    public final EObject entryRuleObjectTypeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectTypeExpression = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4072:2: (iv_ruleObjectTypeExpression= ruleObjectTypeExpression EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4073:2: iv_ruleObjectTypeExpression= ruleObjectTypeExpression EOF
            {
             newCompositeNode(grammarAccess.getObjectTypeExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObjectTypeExpression_in_entryRuleObjectTypeExpression9287);
            iv_ruleObjectTypeExpression=ruleObjectTypeExpression();

            state._fsp--;

             current =iv_ruleObjectTypeExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObjectTypeExpression9297); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectTypeExpression"


    // $ANTLR start "ruleObjectTypeExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4080:1: ruleObjectTypeExpression returns [EObject current=null] : (otherlv_0= 'type of' ( ( ruleEString ) ) ) ;
    public final EObject ruleObjectTypeExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4083:28: ( (otherlv_0= 'type of' ( ( ruleEString ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4084:1: (otherlv_0= 'type of' ( ( ruleEString ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4084:1: (otherlv_0= 'type of' ( ( ruleEString ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4084:3: otherlv_0= 'type of' ( ( ruleEString ) )
            {
            otherlv_0=(Token)match(input,60,FollowSets000.FOLLOW_60_in_ruleObjectTypeExpression9334); 

                	newLeafNode(otherlv_0, grammarAccess.getObjectTypeExpressionAccess().getTypeOfKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4088:1: ( ( ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4089:1: ( ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4089:1: ( ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4090:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getObjectTypeExpressionRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getObjectTypeExpressionAccess().getObjectObjectCrossReference_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObjectTypeExpression9357);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectTypeExpression"


    // $ANTLR start "entryRuleMultiplicityExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4111:1: entryRuleMultiplicityExpression returns [EObject current=null] : iv_ruleMultiplicityExpression= ruleMultiplicityExpression EOF ;
    public final EObject entryRuleMultiplicityExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicityExpression = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4112:2: (iv_ruleMultiplicityExpression= ruleMultiplicityExpression EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4113:2: iv_ruleMultiplicityExpression= ruleMultiplicityExpression EOF
            {
             newCompositeNode(grammarAccess.getMultiplicityExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMultiplicityExpression_in_entryRuleMultiplicityExpression9393);
            iv_ruleMultiplicityExpression=ruleMultiplicityExpression();

            state._fsp--;

             current =iv_ruleMultiplicityExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMultiplicityExpression9403); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicityExpression"


    // $ANTLR start "ruleMultiplicityExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4120:1: ruleMultiplicityExpression returns [EObject current=null] : (otherlv_0= 'multiplicity of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) ) ;
    public final EObject ruleMultiplicityExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_feature_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4123:28: ( (otherlv_0= 'multiplicity of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4124:1: (otherlv_0= 'multiplicity of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4124:1: (otherlv_0= 'multiplicity of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4124:3: otherlv_0= 'multiplicity of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) )
            {
            otherlv_0=(Token)match(input,61,FollowSets000.FOLLOW_61_in_ruleMultiplicityExpression9440); 

                	newLeafNode(otherlv_0, grammarAccess.getMultiplicityExpressionAccess().getMultiplicityOfKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4128:1: ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4129:1: (lv_feature_1_0= ruleFeatureWithinObjectExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4129:1: (lv_feature_1_0= ruleFeatureWithinObjectExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4130:3: lv_feature_1_0= ruleFeatureWithinObjectExpression
            {
             
            	        newCompositeNode(grammarAccess.getMultiplicityExpressionAccess().getFeatureFeatureWithinObjectExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFeatureWithinObjectExpression_in_ruleMultiplicityExpression9461);
            lv_feature_1_0=ruleFeatureWithinObjectExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMultiplicityExpressionRule());
            	        }
                   		set(
                   			current, 
                   			"feature",
                    		lv_feature_1_0, 
                    		"FeatureWithinObjectExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicityExpression"


    // $ANTLR start "entryRuleFeatureTypeExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4154:1: entryRuleFeatureTypeExpression returns [EObject current=null] : iv_ruleFeatureTypeExpression= ruleFeatureTypeExpression EOF ;
    public final EObject entryRuleFeatureTypeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureTypeExpression = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4155:2: (iv_ruleFeatureTypeExpression= ruleFeatureTypeExpression EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4156:2: iv_ruleFeatureTypeExpression= ruleFeatureTypeExpression EOF
            {
             newCompositeNode(grammarAccess.getFeatureTypeExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeatureTypeExpression_in_entryRuleFeatureTypeExpression9497);
            iv_ruleFeatureTypeExpression=ruleFeatureTypeExpression();

            state._fsp--;

             current =iv_ruleFeatureTypeExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureTypeExpression9507); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureTypeExpression"


    // $ANTLR start "ruleFeatureTypeExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4163:1: ruleFeatureTypeExpression returns [EObject current=null] : (otherlv_0= 'type of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) ) ;
    public final EObject ruleFeatureTypeExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_feature_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4166:28: ( (otherlv_0= 'type of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4167:1: (otherlv_0= 'type of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4167:1: (otherlv_0= 'type of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4167:3: otherlv_0= 'type of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) )
            {
            otherlv_0=(Token)match(input,60,FollowSets000.FOLLOW_60_in_ruleFeatureTypeExpression9544); 

                	newLeafNode(otherlv_0, grammarAccess.getFeatureTypeExpressionAccess().getTypeOfKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4171:1: ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4172:1: (lv_feature_1_0= ruleFeatureWithinObjectExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4172:1: (lv_feature_1_0= ruleFeatureWithinObjectExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4173:3: lv_feature_1_0= ruleFeatureWithinObjectExpression
            {
             
            	        newCompositeNode(grammarAccess.getFeatureTypeExpressionAccess().getFeatureFeatureWithinObjectExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFeatureWithinObjectExpression_in_ruleFeatureTypeExpression9565);
            lv_feature_1_0=ruleFeatureWithinObjectExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFeatureTypeExpressionRule());
            	        }
                   		set(
                   			current, 
                   			"feature",
                    		lv_feature_1_0, 
                    		"FeatureWithinObjectExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureTypeExpression"


    // $ANTLR start "entryRuleFeatureNatureExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4197:1: entryRuleFeatureNatureExpression returns [EObject current=null] : iv_ruleFeatureNatureExpression= ruleFeatureNatureExpression EOF ;
    public final EObject entryRuleFeatureNatureExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureNatureExpression = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4198:2: (iv_ruleFeatureNatureExpression= ruleFeatureNatureExpression EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4199:2: iv_ruleFeatureNatureExpression= ruleFeatureNatureExpression EOF
            {
             newCompositeNode(grammarAccess.getFeatureNatureExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeatureNatureExpression_in_entryRuleFeatureNatureExpression9601);
            iv_ruleFeatureNatureExpression=ruleFeatureNatureExpression();

            state._fsp--;

             current =iv_ruleFeatureNatureExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureNatureExpression9611); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureNatureExpression"


    // $ANTLR start "ruleFeatureNatureExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4206:1: ruleFeatureNatureExpression returns [EObject current=null] : (otherlv_0= 'nature of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) ) ;
    public final EObject ruleFeatureNatureExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_feature_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4209:28: ( (otherlv_0= 'nature of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4210:1: (otherlv_0= 'nature of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4210:1: (otherlv_0= 'nature of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4210:3: otherlv_0= 'nature of' ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) )
            {
            otherlv_0=(Token)match(input,62,FollowSets000.FOLLOW_62_in_ruleFeatureNatureExpression9648); 

                	newLeafNode(otherlv_0, grammarAccess.getFeatureNatureExpressionAccess().getNatureOfKeyword_0());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4214:1: ( (lv_feature_1_0= ruleFeatureWithinObjectExpression ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4215:1: (lv_feature_1_0= ruleFeatureWithinObjectExpression )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4215:1: (lv_feature_1_0= ruleFeatureWithinObjectExpression )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4216:3: lv_feature_1_0= ruleFeatureWithinObjectExpression
            {
             
            	        newCompositeNode(grammarAccess.getFeatureNatureExpressionAccess().getFeatureFeatureWithinObjectExpressionParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFeatureWithinObjectExpression_in_ruleFeatureNatureExpression9669);
            lv_feature_1_0=ruleFeatureWithinObjectExpression();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFeatureNatureExpressionRule());
            	        }
                   		set(
                   			current, 
                   			"feature",
                    		lv_feature_1_0, 
                    		"FeatureWithinObjectExpression");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureNatureExpression"


    // $ANTLR start "entryRuleFeatureWithinObjectExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4240:1: entryRuleFeatureWithinObjectExpression returns [EObject current=null] : iv_ruleFeatureWithinObjectExpression= ruleFeatureWithinObjectExpression EOF ;
    public final EObject entryRuleFeatureWithinObjectExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureWithinObjectExpression = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4241:2: (iv_ruleFeatureWithinObjectExpression= ruleFeatureWithinObjectExpression EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4242:2: iv_ruleFeatureWithinObjectExpression= ruleFeatureWithinObjectExpression EOF
            {
             newCompositeNode(grammarAccess.getFeatureWithinObjectExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeatureWithinObjectExpression_in_entryRuleFeatureWithinObjectExpression9705);
            iv_ruleFeatureWithinObjectExpression=ruleFeatureWithinObjectExpression();

            state._fsp--;

             current =iv_ruleFeatureWithinObjectExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeatureWithinObjectExpression9715); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureWithinObjectExpression"


    // $ANTLR start "ruleFeatureWithinObjectExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4249:1: ruleFeatureWithinObjectExpression returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '.' ( ( ruleEString ) ) ) ;
    public final EObject ruleFeatureWithinObjectExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4252:28: ( ( ( ( ruleEString ) ) otherlv_1= '.' ( ( ruleEString ) ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4253:1: ( ( ( ruleEString ) ) otherlv_1= '.' ( ( ruleEString ) ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4253:1: ( ( ( ruleEString ) ) otherlv_1= '.' ( ( ruleEString ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4253:2: ( ( ruleEString ) ) otherlv_1= '.' ( ( ruleEString ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4253:2: ( ( ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4254:1: ( ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4254:1: ( ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4255:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getFeatureWithinObjectExpressionRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getFeatureWithinObjectExpressionAccess().getObjectObjectCrossReference_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFeatureWithinObjectExpression9763);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_1=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleFeatureWithinObjectExpression9775); 

                	newLeafNode(otherlv_1, grammarAccess.getFeatureWithinObjectExpressionAccess().getFullStopKeyword_1());
                
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4272:1: ( ( ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4273:1: ( ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4273:1: ( ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4274:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getFeatureWithinObjectExpressionRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getFeatureWithinObjectExpressionAccess().getFeatureFeatureCrossReference_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFeatureWithinObjectExpression9798);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureWithinObjectExpression"


    // $ANTLR start "entryRuleObjectExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4295:1: entryRuleObjectExpression returns [EObject current=null] : iv_ruleObjectExpression= ruleObjectExpression EOF ;
    public final EObject entryRuleObjectExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectExpression = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4296:2: (iv_ruleObjectExpression= ruleObjectExpression EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4297:2: iv_ruleObjectExpression= ruleObjectExpression EOF
            {
             newCompositeNode(grammarAccess.getObjectExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObjectExpression_in_entryRuleObjectExpression9834);
            iv_ruleObjectExpression=ruleObjectExpression();

            state._fsp--;

             current =iv_ruleObjectExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObjectExpression9844); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectExpression"


    // $ANTLR start "ruleObjectExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4304:1: ruleObjectExpression returns [EObject current=null] : ( ( ruleEString ) ) ;
    public final EObject ruleObjectExpression() throws RecognitionException {
        EObject current = null;

         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4307:28: ( ( ( ruleEString ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4308:1: ( ( ruleEString ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4308:1: ( ( ruleEString ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4309:1: ( ruleEString )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4309:1: ( ruleEString )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4310:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getObjectExpressionRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getObjectExpressionAccess().getObjectObjectCrossReference_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObjectExpression9891);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectExpression"


    // $ANTLR start "entryRuleObjectOrMetaClassExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4331:1: entryRuleObjectOrMetaClassExpression returns [EObject current=null] : iv_ruleObjectOrMetaClassExpression= ruleObjectOrMetaClassExpression EOF ;
    public final EObject entryRuleObjectOrMetaClassExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectOrMetaClassExpression = null;


        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4332:2: (iv_ruleObjectOrMetaClassExpression= ruleObjectOrMetaClassExpression EOF )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4333:2: iv_ruleObjectOrMetaClassExpression= ruleObjectOrMetaClassExpression EOF
            {
             newCompositeNode(grammarAccess.getObjectOrMetaClassExpressionRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObjectOrMetaClassExpression_in_entryRuleObjectOrMetaClassExpression9926);
            iv_ruleObjectOrMetaClassExpression=ruleObjectOrMetaClassExpression();

            state._fsp--;

             current =iv_ruleObjectOrMetaClassExpression; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObjectOrMetaClassExpression9936); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectOrMetaClassExpression"


    // $ANTLR start "ruleObjectOrMetaClassExpression"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4340:1: ruleObjectOrMetaClassExpression returns [EObject current=null] : (this_ObjectExpression_0= ruleObjectExpression | (otherlv_1= 'some' this_MetaClassExpression_2= ruleMetaClassExpression ) ) ;
    public final EObject ruleObjectOrMetaClassExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject this_ObjectExpression_0 = null;

        EObject this_MetaClassExpression_2 = null;


         enterRule(); 
            
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4343:28: ( (this_ObjectExpression_0= ruleObjectExpression | (otherlv_1= 'some' this_MetaClassExpression_2= ruleMetaClassExpression ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4344:1: (this_ObjectExpression_0= ruleObjectExpression | (otherlv_1= 'some' this_MetaClassExpression_2= ruleMetaClassExpression ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4344:1: (this_ObjectExpression_0= ruleObjectExpression | (otherlv_1= 'some' this_MetaClassExpression_2= ruleMetaClassExpression ) )
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( ((LA60_0>=RULE_STRING && LA60_0<=RULE_ID)) ) {
                alt60=1;
            }
            else if ( (LA60_0==37) ) {
                alt60=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 60, 0, input);

                throw nvae;
            }
            switch (alt60) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4345:5: this_ObjectExpression_0= ruleObjectExpression
                    {
                     
                            newCompositeNode(grammarAccess.getObjectOrMetaClassExpressionAccess().getObjectExpressionParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleObjectExpression_in_ruleObjectOrMetaClassExpression9983);
                    this_ObjectExpression_0=ruleObjectExpression();

                    state._fsp--;

                     
                            current = this_ObjectExpression_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4354:6: (otherlv_1= 'some' this_MetaClassExpression_2= ruleMetaClassExpression )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4354:6: (otherlv_1= 'some' this_MetaClassExpression_2= ruleMetaClassExpression )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4354:8: otherlv_1= 'some' this_MetaClassExpression_2= ruleMetaClassExpression
                    {
                    otherlv_1=(Token)match(input,37,FollowSets000.FOLLOW_37_in_ruleObjectOrMetaClassExpression10001); 

                        	newLeafNode(otherlv_1, grammarAccess.getObjectOrMetaClassExpressionAccess().getSomeKeyword_1_0());
                        
                     
                            newCompositeNode(grammarAccess.getObjectOrMetaClassExpressionAccess().getMetaClassExpressionParserRuleCall_1_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleMetaClassExpression_in_ruleObjectOrMetaClassExpression10023);
                    this_MetaClassExpression_2=ruleMetaClassExpression();

                    state._fsp--;

                     
                            current = this_MetaClassExpression_2; 
                            afterParserOrEnumRuleCall();
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectOrMetaClassExpression"


    // $ANTLR start "ruleFragmentType"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4375:1: ruleFragmentType returns [Enumerator current=null] : ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) ) ;
    public final Enumerator ruleFragmentType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4377:28: ( ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4378:1: ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4378:1: ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) )
            int alt61=2;
            int LA61_0 = input.LA(1);

            if ( (LA61_0==63) ) {
                alt61=1;
            }
            else if ( (LA61_0==64) ) {
                alt61=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }
            switch (alt61) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4378:2: (enumLiteral_0= 'positive' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4378:2: (enumLiteral_0= 'positive' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4378:4: enumLiteral_0= 'positive'
                    {
                    enumLiteral_0=(Token)match(input,63,FollowSets000.FOLLOW_63_in_ruleFragmentType10073); 

                            current = grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4384:6: (enumLiteral_1= 'negative' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4384:6: (enumLiteral_1= 'negative' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4384:8: enumLiteral_1= 'negative'
                    {
                    enumLiteral_1=(Token)match(input,64,FollowSets000.FOLLOW_64_in_ruleFragmentType10090); 

                            current = grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentType"


    // $ANTLR start "ruleCompletionType"
    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4394:1: ruleCompletionType returns [Enumerator current=null] : ( (enumLiteral_0= 'fragment' ) | (enumLiteral_1= 'example' ) ) ;
    public final Enumerator ruleCompletionType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4396:28: ( ( (enumLiteral_0= 'fragment' ) | (enumLiteral_1= 'example' ) ) )
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4397:1: ( (enumLiteral_0= 'fragment' ) | (enumLiteral_1= 'example' ) )
            {
            // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4397:1: ( (enumLiteral_0= 'fragment' ) | (enumLiteral_1= 'example' ) )
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==65) ) {
                alt62=1;
            }
            else if ( (LA62_0==66) ) {
                alt62=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 62, 0, input);

                throw nvae;
            }
            switch (alt62) {
                case 1 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4397:2: (enumLiteral_0= 'fragment' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4397:2: (enumLiteral_0= 'fragment' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4397:4: enumLiteral_0= 'fragment'
                    {
                    enumLiteral_0=(Token)match(input,65,FollowSets000.FOLLOW_65_in_ruleCompletionType10135); 

                            current = grammarAccess.getCompletionTypeAccess().getIsFragmentEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getCompletionTypeAccess().getIsFragmentEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4403:6: (enumLiteral_1= 'example' )
                    {
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4403:6: (enumLiteral_1= 'example' )
                    // ../metabest.test.fragments.editor/src-gen/metabest/test/fragments/parser/antlr/internal/InternalMBF.g:4403:8: enumLiteral_1= 'example'
                    {
                    enumLiteral_1=(Token)match(input,66,FollowSets000.FOLLOW_66_in_ruleCompletionType10152); 

                            current = grammarAccess.getCompletionTypeAccess().getIsExampleEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getCompletionTypeAccess().getIsExampleEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompletionType"

    // Delegated rules


    protected DFA15 dfa15 = new DFA15(this);
    protected DFA56 dfa56 = new DFA56(this);
    protected DFA58 dfa58 = new DFA58(this);
    static final String DFA15_eotS =
        "\103\uffff";
    static final String DFA15_eofS =
        "\103\uffff";
    static final String DFA15_minS =
        "\1\20\1\4\2\uffff\2\20\2\4\2\21\2\20\3\4\6\22\2\21\1\4\1\20\3\4"+
        "\2\21\10\22\3\4\1\20\1\4\6\22\2\21\2\22\3\4\10\22\1\4\2\22";
    static final String DFA15_maxS =
        "\1\25\1\5\2\uffff\2\26\2\5\2\30\2\26\1\33\2\5\4\27\2\31\2\30\1"+
        "\5\1\25\1\5\1\33\1\5\2\30\6\27\2\31\1\33\2\5\1\25\1\5\4\27\2\31"+
        "\2\30\2\27\1\5\1\33\1\5\6\27\2\31\1\5\2\27";
    static final String DFA15_acceptS =
        "\2\uffff\1\1\1\2\77\uffff";
    static final String DFA15_specialS =
        "\103\uffff}>";
    static final String[] DFA15_transitionS = {
            "\1\2\2\uffff\2\3\1\1",
            "\1\4\1\5",
            "",
            "",
            "\1\2\2\uffff\2\3\1\7\1\6",
            "\1\2\2\uffff\2\3\1\7\1\6",
            "\1\10\1\11",
            "\1\12\1\13",
            "\1\14\6\uffff\1\15",
            "\1\14\6\uffff\1\15",
            "\1\2\2\uffff\2\3\1\7\1\16",
            "\1\2\2\uffff\2\3\1\7\1\16",
            "\1\17\1\uffff\1\20\23\uffff\1\21\1\22",
            "\1\23\1\24",
            "\1\25\1\26",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30\1\uffff\1\31",
            "\1\27\4\uffff\1\30\1\uffff\1\31",
            "\1\32\6\uffff\1\33",
            "\1\32\6\uffff\1\33",
            "\1\34\1\35",
            "\1\2\2\uffff\2\3\1\7",
            "\1\36\1\37",
            "\1\40\1\uffff\1\41\23\uffff\1\42\1\43",
            "\1\44\1\45",
            "\1\46\6\uffff\1\47",
            "\1\46\6\uffff\1\47",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51\1\uffff\1\52",
            "\1\50\4\uffff\1\51\1\uffff\1\52",
            "\1\53\1\uffff\1\54\23\uffff\1\55\1\56",
            "\1\57\1\60",
            "\1\61\1\62",
            "\1\2\2\uffff\2\3\1\7",
            "\1\63\1\64",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30\1\uffff\1\65",
            "\1\27\4\uffff\1\30\1\uffff\1\65",
            "\1\66\6\uffff\1\67",
            "\1\66\6\uffff\1\67",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51",
            "\1\70\1\71",
            "\1\72\1\uffff\1\73\23\uffff\1\74\1\75",
            "\1\76\1\77",
            "\1\27\4\uffff\1\30",
            "\1\27\4\uffff\1\30",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51\1\uffff\1\100",
            "\1\50\4\uffff\1\51\1\uffff\1\100",
            "\1\101\1\102",
            "\1\50\4\uffff\1\51",
            "\1\50\4\uffff\1\51"
    };

    static final short[] DFA15_eot = DFA.unpackEncodedString(DFA15_eotS);
    static final short[] DFA15_eof = DFA.unpackEncodedString(DFA15_eofS);
    static final char[] DFA15_min = DFA.unpackEncodedStringToUnsignedChars(DFA15_minS);
    static final char[] DFA15_max = DFA.unpackEncodedStringToUnsignedChars(DFA15_maxS);
    static final short[] DFA15_accept = DFA.unpackEncodedString(DFA15_acceptS);
    static final short[] DFA15_special = DFA.unpackEncodedString(DFA15_specialS);
    static final short[][] DFA15_transition;

    static {
        int numStates = DFA15_transitionS.length;
        DFA15_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA15_transition[i] = DFA.unpackEncodedString(DFA15_transitionS[i]);
        }
    }

    class DFA15 extends DFA {

        public DFA15(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = DFA15_eot;
            this.eof = DFA15_eof;
            this.min = DFA15_min;
            this.max = DFA15_max;
            this.accept = DFA15_accept;
            this.special = DFA15_special;
            this.transition = DFA15_transition;
        }
        public String getDescription() {
            return "528:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )";
        }
    }
    static final String DFA56_eotS =
        "\16\uffff";
    static final String DFA56_eofS =
        "\16\uffff";
    static final String DFA56_minS =
        "\1\62\1\4\1\uffff\1\57\1\74\11\uffff";
    static final String DFA56_maxS =
        "\1\72\1\74\1\uffff\1\70\1\76\11\uffff";
    static final String DFA56_acceptS =
        "\2\uffff\1\3\2\uffff\1\11\1\2\1\1\1\5\1\12\1\4\1\10\1\7\1\6";
    static final String DFA56_specialS =
        "\16\uffff}>";
    static final String[] DFA56_transitionS = {
            "\1\3\3\uffff\1\1\1\2\1\uffff\1\4\1\5",
            "\2\7\66\uffff\1\6",
            "",
            "\1\10\3\uffff\1\11\4\uffff\1\12",
            "\1\14\1\15\1\13",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA56_eot = DFA.unpackEncodedString(DFA56_eotS);
    static final short[] DFA56_eof = DFA.unpackEncodedString(DFA56_eofS);
    static final char[] DFA56_min = DFA.unpackEncodedStringToUnsignedChars(DFA56_minS);
    static final char[] DFA56_max = DFA.unpackEncodedStringToUnsignedChars(DFA56_maxS);
    static final short[] DFA56_accept = DFA.unpackEncodedString(DFA56_acceptS);
    static final short[] DFA56_special = DFA.unpackEncodedString(DFA56_specialS);
    static final short[][] DFA56_transition;

    static {
        int numStates = DFA56_transitionS.length;
        DFA56_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA56_transition[i] = DFA.unpackEncodedString(DFA56_transitionS[i]);
        }
    }

    class DFA56 extends DFA {

        public DFA56(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 56;
            this.eot = DFA56_eot;
            this.eof = DFA56_eof;
            this.min = DFA56_min;
            this.max = DFA56_max;
            this.accept = DFA56_accept;
            this.special = DFA56_special;
            this.transition = DFA56_transition;
        }
        public String getDescription() {
            return "3132:1: (this_FeatureExistence_0= ruleFeatureExistence | this_TypeExistence_1= ruleTypeExistence | this_AbstractTypeInstance_2= ruleAbstractTypeInstance | this_MissingAttribute_3= ruleMissingAttribute | this_MissingReference_4= ruleMissingReference | this_MultiplicityMismatch_5= ruleMultiplicityMismatch | this_TypeMismatch_6= ruleTypeMismatch | this_FeatureNatureMismatch_7= ruleFeatureNatureMismatch | this_RestrictionViolation_8= ruleRestrictionViolation | this_Uncontainment_9= ruleUncontainment )";
        }
    }
    static final String DFA58_eotS =
        "\12\uffff";
    static final String DFA58_eofS =
        "\3\uffff\2\7\3\uffff\2\7";
    static final String DFA58_minS =
        "\1\64\1\4\1\uffff\2\15\1\4\2\uffff\2\15";
    static final String DFA58_maxS =
        "\1\65\1\45\1\uffff\2\65\1\5\2\uffff\2\65";
    static final String DFA58_acceptS =
        "\2\uffff\1\2\3\uffff\1\3\1\1\2\uffff";
    static final String DFA58_specialS =
        "\12\uffff}>";
    static final String[] DFA58_transitionS = {
            "\1\1\1\2",
            "\1\3\1\4\37\uffff\1\5",
            "",
            "\1\7\13\uffff\1\7\33\uffff\1\6",
            "\1\7\13\uffff\1\7\33\uffff\1\6",
            "\1\10\1\11",
            "",
            "",
            "\1\7\13\uffff\1\7\33\uffff\1\6",
            "\1\7\13\uffff\1\7\33\uffff\1\6"
    };

    static final short[] DFA58_eot = DFA.unpackEncodedString(DFA58_eotS);
    static final short[] DFA58_eof = DFA.unpackEncodedString(DFA58_eofS);
    static final char[] DFA58_min = DFA.unpackEncodedStringToUnsignedChars(DFA58_minS);
    static final char[] DFA58_max = DFA.unpackEncodedStringToUnsignedChars(DFA58_maxS);
    static final short[] DFA58_accept = DFA.unpackEncodedString(DFA58_acceptS);
    static final short[] DFA58_special = DFA.unpackEncodedString(DFA58_specialS);
    static final short[][] DFA58_transition;

    static {
        int numStates = DFA58_transitionS.length;
        DFA58_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA58_transition[i] = DFA.unpackEncodedString(DFA58_transitionS[i]);
        }
    }

    class DFA58 extends DFA {

        public DFA58(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 58;
            this.eot = DFA58_eot;
            this.eof = DFA58_eof;
            this.min = DFA58_min;
            this.max = DFA58_max;
            this.accept = DFA58_accept;
            this.special = DFA58_special;
            this.transition = DFA58_transition;
        }
        public String getDescription() {
            return "3696:1: ( (otherlv_2= 'from' ( (lv_from_3_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_4= 'to' ( (lv_to_5_0= ruleObjectOrMetaClassExpression ) ) ) | (otherlv_6= 'from' ( (lv_from_7_0= ruleObjectOrMetaClassExpression ) ) otherlv_8= 'to' ( (lv_to_9_0= ruleObjectOrMetaClassExpression ) ) ) )";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleTestModel_in_entryRuleTestModel75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTestModel85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_ruleTestModel123 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleTestModel140 = new BitSet(new long[]{0x8000000000200000L,0x0000000000000007L});
        public static final BitSet FOLLOW_ruleTestableFragment_in_ruleTestModel168 = new BitSet(new long[]{0x8000000000200002L,0x0000000000000007L});
        public static final BitSet FOLLOW_ruleTestableFragment_in_ruleTestModel189 = new BitSet(new long[]{0x8000000000200002L,0x0000000000000007L});
        public static final BitSet FOLLOW_ruleTestableFragment_in_entryRuleTestableFragment226 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTestableFragment236 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleTestableFragment283 = new BitSet(new long[]{0x8000000000200000L,0x0000000000000007L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleTestableFragment304 = new BitSet(new long[]{0x8000000000200000L,0x0000000000000007L});
        public static final BitSet FOLLOW_ruleFragmentType_in_ruleTestableFragment328 = new BitSet(new long[]{0x8000000000200000L,0x0000000000000007L});
        public static final BitSet FOLLOW_ruleCompletionType_in_ruleTestableFragment350 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleTestableFragment371 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleTestableFragment383 = new BitSet(new long[]{0x0000000000200030L});
        public static final BitSet FOLLOW_ruleObject_in_ruleTestableFragment404 = new BitSet(new long[]{0x0000000110202030L});
        public static final BitSet FOLLOW_ruleObject_in_ruleTestableFragment425 = new BitSet(new long[]{0x0000000110202030L});
        public static final BitSet FOLLOW_ruleComplementaryBlock_in_ruleTestableFragment447 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_13_in_ruleTestableFragment460 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleComplementaryBlock_in_entryRuleComplementaryBlock496 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleComplementaryBlock506 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAssertionSet_in_ruleComplementaryBlock553 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExtensionAssertionSet_in_ruleComplementaryBlock580 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObject_in_entryRuleObject615 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObject625 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleObject681 = new BitSet(new long[]{0x0000000000200030L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleObject702 = new BitSet(new long[]{0x0000000000200030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObject726 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_14_in_ruleObject738 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObject759 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleObject771 = new BitSet(new long[]{0x0000000000392000L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleObject793 = new BitSet(new long[]{0x000000000039A000L});
        public static final BitSet FOLLOW_15_in_ruleObject806 = new BitSet(new long[]{0x0000000000392000L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleObject830 = new BitSet(new long[]{0x000000000039A000L});
        public static final BitSet FOLLOW_15_in_ruleObject843 = new BitSet(new long[]{0x0000000000392000L});
        public static final BitSet FOLLOW_13_in_ruleObject861 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature897 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeature907 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_ruleFeature954 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_ruleFeature981 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_entryRuleAttribute1016 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttribute1026 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleAttribute1082 = new BitSet(new long[]{0x0000000000210000L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleAttribute1103 = new BitSet(new long[]{0x0000000000210000L});
        public static final BitSet FOLLOW_16_in_ruleAttribute1118 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAttribute1139 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleAttribute1151 = new BitSet(new long[]{0x000000000C000050L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_ruleAttribute1172 = new BitSet(new long[]{0x0000000000040002L});
        public static final BitSet FOLLOW_18_in_ruleAttribute1185 = new BitSet(new long[]{0x000000000C000050L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_ruleAttribute1206 = new BitSet(new long[]{0x0000000000040002L});
        public static final BitSet FOLLOW_ruleReference_in_entryRuleReference1244 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReference1254 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleReference1310 = new BitSet(new long[]{0x0000000000380000L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleReference1331 = new BitSet(new long[]{0x0000000000380000L});
        public static final BitSet FOLLOW_19_in_ruleReference1347 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_20_in_ruleReference1365 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference1387 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleReference1399 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference1422 = new BitSet(new long[]{0x0000000000040002L});
        public static final BitSet FOLLOW_18_in_ruleReference1435 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference1458 = new BitSet(new long[]{0x0000000000040002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString1497 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString1508 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleEString1548 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleEString1574 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue1619 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValue1629 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_rulePrimitiveValue1676 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_rulePrimitiveValue1703 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_rulePrimitiveValue1730 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_entryRuleAnnotation1765 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotation1775 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_ruleAnnotation1812 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAnnotation1833 = new BitSet(new long[]{0x0000000000400002L});
        public static final BitSet FOLLOW_22_in_ruleAnnotation1846 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_ruleAnnotation1867 = new BitSet(new long[]{0x0000000000840000L});
        public static final BitSet FOLLOW_18_in_ruleAnnotation1880 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_ruleAnnotation1901 = new BitSet(new long[]{0x0000000000840000L});
        public static final BitSet FOLLOW_23_in_ruleAnnotation1915 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam1953 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotationParam1963 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAnnotationParam2009 = new BitSet(new long[]{0x0000000001020000L});
        public static final BitSet FOLLOW_ruleParamValue_in_ruleAnnotationParam2030 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParamValue_in_entryRuleParamValue2066 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleParamValue2076 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_ruleParamValue2123 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_ruleParamValue2150 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam2185 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam2195 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_ruleObjectValueAnnotationParam2232 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam2255 = new BitSet(new long[]{0x0000000002000002L});
        public static final BitSet FOLLOW_25_in_ruleObjectValueAnnotationParam2268 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam2291 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam2329 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam2339 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rulePrimitiveValueAnnotationParam2376 = new BitSet(new long[]{0x000000000C000050L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_rulePrimitiveValueAnnotationParam2397 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue2433 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntegerValue2443 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerValue2484 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_entryRuleStringValue2524 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringValue2534 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleStringValue2575 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue2615 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValue2625 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_ruleBooleanValue2670 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal2706 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValueTerminal2717 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_ruleBooleanValueTerminal2755 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_ruleBooleanValueTerminal2774 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAssertionSet_in_entryRuleAssertionSet2814 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAssertionSet2824 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_28_in_ruleAssertionSet2870 = new BitSet(new long[]{0x00000000A0000000L});
        public static final BitSet FOLLOW_29_in_ruleAssertionSet2889 = new BitSet(new long[]{0x0000000040000000L});
        public static final BitSet FOLLOW_30_in_ruleAssertionSet2914 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_31_in_ruleAssertionSet2928 = new BitSet(new long[]{0x06C4000000000000L});
        public static final BitSet FOLLOW_ruleAssertion_in_ruleAssertionSet2949 = new BitSet(new long[]{0x0000000002000002L});
        public static final BitSet FOLLOW_25_in_ruleAssertionSet2962 = new BitSet(new long[]{0x06C4000000000000L});
        public static final BitSet FOLLOW_ruleAssertion_in_ruleAssertionSet2983 = new BitSet(new long[]{0x0000000002000002L});
        public static final BitSet FOLLOW_ruleExtensionAssertionSet_in_entryRuleExtensionAssertionSet3021 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleExtensionAssertionSet3031 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_ruleExtensionAssertionSet3077 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_33_in_ruleExtensionAssertionSet3089 = new BitSet(new long[]{0x000003F400400070L});
        public static final BitSet FOLLOW_ruleExtensionAssertion_in_ruleExtensionAssertionSet3110 = new BitSet(new long[]{0x000003F400400072L});
        public static final BitSet FOLLOW_ruleExtensionAssertion_in_ruleExtensionAssertionSet3131 = new BitSet(new long[]{0x000003F400400072L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_entryRuleEIntegerObject3169 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEIntegerObject3180 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_34_in_ruleEIntegerObject3219 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleEIntegerObject3236 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAssertion_in_entryRuleAssertion3285 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAssertion3295 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCheck_in_ruleAssertion3350 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleExtensionAssertion_in_entryRuleExtensionAssertion3386 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleExtensionAssertion3396 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassExtensionAssertion_in_ruleExtensionAssertion3443 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectExtensionAssertion_in_ruleExtensionAssertion3470 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectExtensionAssertion_in_entryRuleFragmentObjectExtensionAssertion3505 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentObjectExtensionAssertion3515 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectSelector_in_ruleFragmentObjectExtensionAssertion3561 = new BitSet(new long[]{0x0000000802000000L});
        public static final BitSet FOLLOW_35_in_ruleFragmentObjectExtensionAssertion3574 = new BitSet(new long[]{0x0002E00000000030L});
        public static final BitSet FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectExtensionAssertion3595 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleFragmentObjectExtensionAssertion3609 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassExtensionAssertion_in_entryRuleMetaClassExtensionAssertion3645 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaClassExtensionAssertion3655 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassSelector_in_ruleMetaClassExtensionAssertion3701 = new BitSet(new long[]{0x0000000802000000L});
        public static final BitSet FOLLOW_35_in_ruleMetaClassExtensionAssertion3714 = new BitSet(new long[]{0x0002E00000000030L});
        public static final BitSet FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassExtensionAssertion3735 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleMetaClassExtensionAssertion3749 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleQuantifier_in_entryRuleQuantifier3785 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleQuantifier3795 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleNone_in_ruleQuantifier3842 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleSome_in_ruleQuantifier3869 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEvery_in_ruleQuantifier3896 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleNumber_in_ruleQuantifier3923 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleNone_in_entryRuleNone3958 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleNone3968 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_ruleNone4014 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleSome_in_entryRuleSome4050 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleSome4060 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_ruleSome4106 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEvery_in_entryRuleEvery4142 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEvery4152 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_38_in_ruleEvery4204 = new BitSet(new long[]{0x0000008000000000L});
        public static final BitSet FOLLOW_39_in_ruleEvery4230 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleNumber_in_entryRuleNumber4266 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleNumber4276 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleNumber_Impl_in_ruleNumber4323 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStrictNumber_in_ruleNumber4350 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMoreThan_in_ruleNumber4377 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleNumber_Impl_in_entryRuleNumber_Impl4412 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleNumber_Impl4422 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerParameter_in_ruleNumber_Impl4467 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStrictNumber_in_entryRuleStrictNumber4502 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStrictNumber4512 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_40_in_ruleStrictNumber4558 = new BitSet(new long[]{0x0000000400000040L});
        public static final BitSet FOLLOW_ruleIntegerParameter_in_ruleStrictNumber4579 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMoreThan_in_entryRuleMoreThan4615 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMoreThan4625 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_ruleMoreThan4673 = new BitSet(new long[]{0x0000000400000040L});
        public static final BitSet FOLLOW_41_in_ruleMoreThan4697 = new BitSet(new long[]{0x0000000400000040L});
        public static final BitSet FOLLOW_ruleIntegerParameter_in_ruleMoreThan4732 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleMoreThan4744 = new BitSet(new long[]{0x0000040400000040L});
        public static final BitSet FOLLOW_ruleLessThan_in_ruleMoreThan4766 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_42_in_ruleMoreThan4784 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleLessThan_in_entryRuleLessThan4822 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleLessThan4832 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerParameter_in_ruleLessThan4887 = new BitSet(new long[]{0x0000080000800000L});
        public static final BitSet FOLLOW_23_in_ruleLessThan4900 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_43_in_ruleLessThan4924 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerParameter_in_entryRuleIntegerParameter4974 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntegerParameter4984 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleIntegerParameter5029 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassSelector_in_entryRuleMetaClassSelector5064 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaClassSelector5074 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleQuantifier_in_ruleMetaClassSelector5120 = new BitSet(new long[]{0x0000100000000030L});
        public static final BitSet FOLLOW_44_in_ruleMetaClassSelector5138 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleMetaClassSelector5173 = new BitSet(new long[]{0x0000000000001002L});
        public static final BitSet FOLLOW_12_in_ruleMetaClassSelector5186 = new BitSet(new long[]{0x0002E00000000030L});
        public static final BitSet FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassSelector5207 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_13_in_ruleMetaClassSelector5219 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassQualifier_in_entryRuleMetaClassQualifier5257 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaClassQualifier5267 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassAndQualifier_in_ruleMetaClassQualifier5314 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassOrQualifier_in_ruleMetaClassQualifier5341 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReach_in_ruleMetaClassQualifier5368 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleContains_in_ruleMetaClassQualifier5395 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttributeValue_in_ruleMetaClassQualifier5422 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectQualifier_in_entryRuleFragmentObjectQualifier5457 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentObjectQualifier5467 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectAndQualifier_in_ruleFragmentObjectQualifier5514 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectOrQualifier_in_ruleFragmentObjectQualifier5541 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReach_in_ruleFragmentObjectQualifier5568 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleContains_in_ruleFragmentObjectQualifier5595 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttributeValue_in_ruleFragmentObjectQualifier5622 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassAndQualifier_in_entryRuleMetaClassAndQualifier5657 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaClassAndQualifier5667 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_45_in_ruleMetaClassAndQualifier5704 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleMetaClassAndQualifier5716 = new BitSet(new long[]{0x0002E00000000030L});
        public static final BitSet FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassAndQualifier5737 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleMetaClassAndQualifier5749 = new BitSet(new long[]{0x0002E00000002030L});
        public static final BitSet FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassAndQualifier5770 = new BitSet(new long[]{0x0002E00000002030L});
        public static final BitSet FOLLOW_13_in_ruleMetaClassAndQualifier5783 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassOrQualifier_in_entryRuleMetaClassOrQualifier5819 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaClassOrQualifier5829 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_46_in_ruleMetaClassOrQualifier5866 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleMetaClassOrQualifier5878 = new BitSet(new long[]{0x0002E00000000030L});
        public static final BitSet FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassOrQualifier5899 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleMetaClassOrQualifier5911 = new BitSet(new long[]{0x0002E00000002030L});
        public static final BitSet FOLLOW_ruleMetaClassQualifier_in_ruleMetaClassOrQualifier5932 = new BitSet(new long[]{0x0002E00000002030L});
        public static final BitSet FOLLOW_13_in_ruleMetaClassOrQualifier5945 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectAndQualifier_in_entryRuleFragmentObjectAndQualifier5981 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentObjectAndQualifier5991 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_45_in_ruleFragmentObjectAndQualifier6028 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleFragmentObjectAndQualifier6040 = new BitSet(new long[]{0x0002E00000000030L});
        public static final BitSet FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectAndQualifier6061 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleFragmentObjectAndQualifier6073 = new BitSet(new long[]{0x0002E00000002030L});
        public static final BitSet FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectAndQualifier6094 = new BitSet(new long[]{0x0002E00000002030L});
        public static final BitSet FOLLOW_13_in_ruleFragmentObjectAndQualifier6107 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectOrQualifier_in_entryRuleFragmentObjectOrQualifier6143 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentObjectOrQualifier6153 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_46_in_ruleFragmentObjectOrQualifier6190 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleFragmentObjectOrQualifier6202 = new BitSet(new long[]{0x0002E00000000030L});
        public static final BitSet FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectOrQualifier6223 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_18_in_ruleFragmentObjectOrQualifier6235 = new BitSet(new long[]{0x0002E00000002030L});
        public static final BitSet FOLLOW_ruleFragmentObjectQualifier_in_ruleFragmentObjectOrQualifier6256 = new BitSet(new long[]{0x0002E00000002030L});
        public static final BitSet FOLLOW_13_in_ruleFragmentObjectOrQualifier6269 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReach_in_entryRuleReach6305 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReach6315 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_47_in_ruleReach6352 = new BitSet(new long[]{0x000003F400401070L});
        public static final BitSet FOLLOW_12_in_ruleReach6365 = new BitSet(new long[]{0x0001000000000000L});
        public static final BitSet FOLLOW_48_in_ruleReach6377 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReach6398 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_13_in_ruleReach6410 = new BitSet(new long[]{0x000003F400400070L});
        public static final BitSet FOLLOW_ruleMetaClassSelector_in_ruleReach6435 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectSelector_in_ruleReach6454 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectSelector_in_entryRuleFragmentObjectSelector6493 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentObjectSelector6503 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFragmentObjectSelector6550 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleContains_in_entryRuleContains6585 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleContains6595 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_49_in_ruleContains6632 = new BitSet(new long[]{0x000003F400401070L});
        public static final BitSet FOLLOW_12_in_ruleContains6645 = new BitSet(new long[]{0x0001000000000000L});
        public static final BitSet FOLLOW_48_in_ruleContains6657 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleContains6678 = new BitSet(new long[]{0x0000000000002000L});
        public static final BitSet FOLLOW_13_in_ruleContains6690 = new BitSet(new long[]{0x000003F400400070L});
        public static final BitSet FOLLOW_ruleMetaClassSelector_in_ruleContains6715 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentObjectSelector_in_ruleContains6734 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttributeValue_in_entryRuleAttributeValue6773 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttributeValue6783 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAttributeValue6829 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleAttributeValue6841 = new BitSet(new long[]{0x000000040C000070L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAttributeValue6863 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEIntegerObject_in_ruleAttributeValue6890 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_ruleAttributeValue6914 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_ruleAttributeValue6951 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleCheck_in_entryRuleCheck7001 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleCheck7011 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureExistence_in_ruleCheck7058 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTypeExistence_in_ruleCheck7085 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAbstractTypeInstance_in_ruleCheck7112 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMissingAttribute_in_ruleCheck7139 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMissingReference_in_ruleCheck7166 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMultiplicityMismatch_in_ruleCheck7193 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTypeMismatch_in_ruleCheck7220 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureNatureMismatch_in_ruleCheck7247 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleRestrictionViolation_in_ruleCheck7274 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUncontainment_in_ruleCheck7301 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUncontainment_in_entryRuleUncontainment7336 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleUncontainment7346 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_50_in_ruleUncontainment7383 = new BitSet(new long[]{0x0008000000000000L});
        public static final BitSet FOLLOW_51_in_ruleUncontainment7395 = new BitSet(new long[]{0x0030000000000000L});
        public static final BitSet FOLLOW_ruleUncontainment_Impl_in_ruleUncontainment7418 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUncontainmentByMetaClass_in_ruleUncontainment7445 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUncontainmentByObject_in_ruleUncontainment7472 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUncontainmentByMetaClass_in_entryRuleUncontainmentByMetaClass7508 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleUncontainmentByMetaClass7518 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_52_in_ruleUncontainmentByMetaClass7555 = new BitSet(new long[]{0x0000002000000000L});
        public static final BitSet FOLLOW_37_in_ruleUncontainmentByMetaClass7567 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleMetaClassExpression_in_ruleUncontainmentByMetaClass7588 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_53_in_ruleUncontainmentByMetaClass7600 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleObjectExpression_in_ruleUncontainmentByMetaClass7621 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUncontainmentByObject_in_entryRuleUncontainmentByObject7657 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleUncontainmentByObject7667 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_52_in_ruleUncontainmentByObject7704 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleObjectExpression_in_ruleUncontainmentByObject7725 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_53_in_ruleUncontainmentByObject7737 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleObjectExpression_in_ruleUncontainmentByObject7758 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleUncontainment_Impl_in_entryRuleUncontainment_Impl7794 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleUncontainment_Impl7804 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_ruleUncontainment_Impl7850 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleObjectExpression_in_ruleUncontainment_Impl7871 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureExistence_in_entryRuleFeatureExistence7907 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureExistence7917 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_54_in_ruleFeatureExistence7954 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleFeatureWithinObjectExpression_in_ruleFeatureExistence7975 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTypeExistence_in_entryRuleTypeExistence8011 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTypeExistence8021 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_54_in_ruleTypeExistence8058 = new BitSet(new long[]{0x1000000000000000L});
        public static final BitSet FOLLOW_ruleObjectTypeExpression_in_ruleTypeExistence8079 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAbstractTypeInstance_in_entryRuleAbstractTypeInstance8115 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAbstractTypeInstance8125 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_55_in_ruleAbstractTypeInstance8162 = new BitSet(new long[]{0x1000000000000000L});
        public static final BitSet FOLLOW_ruleObjectTypeExpression_in_ruleAbstractTypeInstance8183 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMissingAttribute_in_entryRuleMissingAttribute8219 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMissingAttribute8229 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_50_in_ruleMissingAttribute8266 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_56_in_ruleMissingAttribute8278 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleMissingAttribute8299 = new BitSet(new long[]{0x0010000000000000L});
        public static final BitSet FOLLOW_52_in_ruleMissingAttribute8311 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleMissingAttribute8334 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMissingReference_in_entryRuleMissingReference8370 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMissingReference8380 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_50_in_ruleMissingReference8417 = new BitSet(new long[]{0x0000800000000000L});
        public static final BitSet FOLLOW_47_in_ruleMissingReference8429 = new BitSet(new long[]{0x0030000000000000L});
        public static final BitSet FOLLOW_52_in_ruleMissingReference8443 = new BitSet(new long[]{0x0000002000000030L});
        public static final BitSet FOLLOW_ruleObjectOrMetaClassExpression_in_ruleMissingReference8464 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_ruleMissingReference8484 = new BitSet(new long[]{0x0000002000000030L});
        public static final BitSet FOLLOW_ruleObjectOrMetaClassExpression_in_ruleMissingReference8505 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_52_in_ruleMissingReference8525 = new BitSet(new long[]{0x0000002000000030L});
        public static final BitSet FOLLOW_ruleObjectOrMetaClassExpression_in_ruleMissingReference8546 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_53_in_ruleMissingReference8558 = new BitSet(new long[]{0x0000002000000030L});
        public static final BitSet FOLLOW_ruleObjectOrMetaClassExpression_in_ruleMissingReference8579 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMultiplicityMismatch_in_entryRuleMultiplicityMismatch8617 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMultiplicityMismatch8627 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_57_in_ruleMultiplicityMismatch8664 = new BitSet(new long[]{0x2000000000000000L});
        public static final BitSet FOLLOW_ruleMultiplicityExpression_in_ruleMultiplicityMismatch8685 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTypeMismatch_in_entryRuleTypeMismatch8721 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleTypeMismatch8731 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_57_in_ruleTypeMismatch8768 = new BitSet(new long[]{0x1000000000000000L});
        public static final BitSet FOLLOW_ruleFeatureTypeExpression_in_ruleTypeMismatch8789 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureNatureMismatch_in_entryRuleFeatureNatureMismatch8825 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureNatureMismatch8835 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_57_in_ruleFeatureNatureMismatch8872 = new BitSet(new long[]{0x4000000000000000L});
        public static final BitSet FOLLOW_ruleFeatureNatureExpression_in_ruleFeatureNatureMismatch8893 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleRestrictionViolation_in_entryRuleRestrictionViolation8929 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleRestrictionViolation8939 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_58_in_ruleRestrictionViolation8976 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleRestrictionViolation8988 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleRestrictionViolation9009 = new BitSet(new long[]{0x0800000000000000L});
        public static final BitSet FOLLOW_59_in_ruleRestrictionViolation9021 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleFragmentElementExpression_in_ruleRestrictionViolation9042 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClassExpression_in_entryRuleMetaClassExpression9078 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaClassExpression9088 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleMetaClassExpression9133 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentElementExpression_in_entryRuleFragmentElementExpression9168 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentElementExpression9178 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureWithinObjectExpression_in_ruleFragmentElementExpression9225 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectExpression_in_ruleFragmentElementExpression9252 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectTypeExpression_in_entryRuleObjectTypeExpression9287 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObjectTypeExpression9297 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_60_in_ruleObjectTypeExpression9334 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObjectTypeExpression9357 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMultiplicityExpression_in_entryRuleMultiplicityExpression9393 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMultiplicityExpression9403 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_61_in_ruleMultiplicityExpression9440 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleFeatureWithinObjectExpression_in_ruleMultiplicityExpression9461 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureTypeExpression_in_entryRuleFeatureTypeExpression9497 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureTypeExpression9507 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_60_in_ruleFeatureTypeExpression9544 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleFeatureWithinObjectExpression_in_ruleFeatureTypeExpression9565 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureNatureExpression_in_entryRuleFeatureNatureExpression9601 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureNatureExpression9611 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_62_in_ruleFeatureNatureExpression9648 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleFeatureWithinObjectExpression_in_ruleFeatureNatureExpression9669 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeatureWithinObjectExpression_in_entryRuleFeatureWithinObjectExpression9705 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeatureWithinObjectExpression9715 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFeatureWithinObjectExpression9763 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleFeatureWithinObjectExpression9775 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFeatureWithinObjectExpression9798 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectExpression_in_entryRuleObjectExpression9834 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObjectExpression9844 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObjectExpression9891 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectOrMetaClassExpression_in_entryRuleObjectOrMetaClassExpression9926 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObjectOrMetaClassExpression9936 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectExpression_in_ruleObjectOrMetaClassExpression9983 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_ruleObjectOrMetaClassExpression10001 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleMetaClassExpression_in_ruleObjectOrMetaClassExpression10023 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_63_in_ruleFragmentType10073 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_64_in_ruleFragmentType10090 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_65_in_ruleCompletionType10135 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_66_in_ruleCompletionType10152 = new BitSet(new long[]{0x0000000000000002L});
    }


}