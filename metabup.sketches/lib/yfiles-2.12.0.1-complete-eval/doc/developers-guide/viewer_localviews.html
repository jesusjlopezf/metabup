<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   
      <title>Local Views</title>
      <link rel="stylesheet" href="ystyle.css" type="text/css">
      <meta name="generator" content="DocBook XSL Stylesheets V1.65.1">
      <link rel="home" href="index.html" title="yFiles for Java Developer's Guide">
      <link rel="up" href="view.html" title="Chapter&nbsp;6.&nbsp;Displaying and Editing Graphs">
      <link rel="previous" href="multiviews.html" title="Multiple Views on a Common Model Graph">
      <link rel="next" href="printing.html" title="Printing a Graph's Visual Representation">
      <link rel="stylesheet" href="jsdt/toc.css" type="text/css"><script type="text/javascript" src="jsdt/jquery.min.js"></script><script type="text/javascript" src="jsdt/toc.js"></script><link type="text/css" rel="stylesheet" href="jssh/SyntaxHighlighter.css"><script type="text/javascript" src="jssh/shCore.js"></script><script type="text/javascript" src="jssh/shBrushJava.js"></script><script type="text/javascript" src="jssh/shBrushXml.js"></script><script type="text/javascript">
  function sh() {

    dp.SyntaxHighlighter.HighlightAll('programlisting', false, false, false, null, false);
  }
  </script></head>
   <body onload="sh()" bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
      <div class="navheader">
         <table width="100%" summary="Navigation header">
            <tr>
               <th colspan="3" align="center">Local Views</th>
            </tr>
            <tr>
               <td width="20%" align="left"><a accesskey="p" href="multiviews.html">Prev</a>&nbsp;
               </td>
               <th width="60%" align="center">Chapter&nbsp;6.&nbsp;Displaying and Editing Graphs</th>
               <td width="20%" align="right">&nbsp;<a accesskey="n" href="printing.html">Next</a></td>
            </tr>
         </table>
         <div class="navline"></div>
         <div style="display:none"><img src="figures/navbg.jpg" alt=""><img src="figures/navline.jpg" alt=""></div>
      </div>
      <div class="section" lang="en">
         <div class="titlepage">
            <div>
               <div>
                  <h2 class="title" style="clear: both"><a name="viewer_localviews"></a>Local Views
                  </h2>
               </div>
            </div>
            <div></div>
         </div><a class="indexterm" name="d0e30660"></a><p>
            
         </p>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h3 class="title"><a name="localviews_concept"></a>Concept
                     </h3>
                  </div>
               </div>
               <div></div>
            </div>
            <p>
               Local views provide an interactive means to concentrate on "parts of interest" in 
               a diagram. 
               The basic idea is to prepare only a small part of an entire diagram which is directly 
               related to some node or edge of particular interest.
               
            </p>
            <p>
               This feature is particularly useful when displaying large and complex graphs. It is possible
               to "focus" on a node or edge, e.g. by selection. The local view then displays the
               source and target node of an edge or the neighbors of a node, even if they are out of the
               display area of the entire graph.
               
            </p>
            <p>
               Local views can be considered as <a href="multiviews.html" title="Multiple Views on a Common Model Graph">multiple views of a common model graph</a>
               with predefined filters for the most common usecases.
               
            </p>
            <p>
               A local view can comprise the following filtered information, for example: 
               
            </p>
            <div class="itemizedlist">
               <ul type="disc">
                  <li>
                     all neighbors of a given node (set), i.e., all predecessors and/or all successors 
                     up to a specifiable level
                     
                  </li>
                  <li>the ancestor hierarchy/hierarchies of a given node (set)</li>
                  <li>all siblings of a given node set contained in group nodes</li>
                  <li>the end nodes of a given edge set</li>
                  <li>
                     edge grouping structures that a given edge is part of, consisting of all nodes and 
                     edges
                     
                  </li>
               </ul>
            </div>
            <p>
               
               
            </p>
            <div class="figure"><a name="fig_local_view_concept"></a><p class="title"><b>Figure&nbsp;6.65.&nbsp;A model graph and its local view</b></p>
               <div class="informaltable">
                  <center>
                     <table border="0">
                        <colgroup>
                           <col width="50%">
                           <col width="50%">
                        </colgroup>
                        <tbody valign="top">
                           <tr>
                              <td>
                                 <div class="mediaobject" align="center"><img src="figures/local_view_model.jpg" align="middle" alt="The entire (model) graph."></div>
                              </td>
                              <td>
                                 <div class="mediaobject" align="center"><img src="figures/local_view_view.jpg" align="middle" alt="A local view: predecessors of the node in focus."></div>
                              </td>
                           </tr>
                           <tr align="center">
                              <td align="center">The model: the entire graph.</td>
                              <td align="center">Local view: predecessors of "List".</td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
         </div>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h3 class="title"><a name="cls_LocalViewCreator"></a>Class LocalViewCreator
                     </h3>
                  </div>
               </div>
               <div></div>
            </div><a class="indexterm" name="d0e30726"></a><p>
               Class <a href="../api/y/view/LocalViewCreator.html" title="Link to API documentation" target="_top">LocalViewCreator<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> is an abstract class whose implementations
               create a local view (Graph2D) from a given model graph (Graph2D). Its central method is
               <a href="../api/y/view/LocalViewCreator.html#updateViewGraph()" title="Link to API documentation" target="_top">updateViewGraph()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> which populates the local view graph,
               applies an automatic layout and updates the
               <a href="../api/y/view/Graph2DView.html" title="Link to API documentation" target="_top">Graph2DView<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> instances which display the local view.
               
            </p>
            <p>
               The layouter which will be invoked upon <tt class="code">updateViewGraph()</tt>
               can be set using <a href="../api/y/view/LocalViewCreator.html#setLayouter()" title="Link to API documentation" target="_top">setLayouter()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>.
               By default an <a href="incremental_hierarchical_layouter.html" title="Hierarchical Layout Style">IncrementalHierarchicLayouter</a> is used.
               
            </p>
            <div class="section" lang="en">
               <div class="titlepage">
                  <div>
                     <div>
                        <h4 class="title"><a name="local_views_creators"></a>Creator Implementations
                        </h4>
                     </div>
                  </div>
                  <div></div>
               </div>
               <p>
                  The yFiles API offers a number of predefined implementations for the most common use cases.
                  
               </p>
               <p>
                  Node based view creator implementations:
                  
               </p>
               <div class="techn_templ">
                  <center>
                     <table width="100%" border="1">
                        <colgroup>
                           <col width="16%" align="left">
                           <col width="84%" align="left">
                        </colgroup>
                        <tbody valign="top">
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.Neighborhood.html" title="Link to API documentation" target="_top">LocalViewCreator.Neighborhood<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">
                                 Creates a local view including all nodes which can be reached within a given distance
                                 from the node in focus.
                                 Optionally, real end nodes of inter-edges can be shown.
                                 
                              </td>
                           </tr>
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.CommonParentGroup.html" title="Link to API documentation" target="_top">LocalViewCreator.CommonParentGroup<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">
                                 Creates a local view including all nodes within the same group.
                                 The container can be included in the view.
                                 
                              </td>
                           </tr>
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.AncestorGroups.html" title="Link to API documentation" target="_top">LocalViewCreator.AncestorGroups<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">
                                 Shows the ancestors of the focused node in the hierarchy, i.e. its containing group
                                 node and the group node's ancestors.
                                 
                              </td>
                           </tr>
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.FolderContents.html" title="Link to API documentation" target="_top">LocalViewCreator.FolderContents<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">Shows the contents of the focused folder node.</td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
               <p>
                  Edge based view creator implementations:
                  
               </p>
               <div class="techn_templ">
                  <center>
                     <table width="100%" border="1">
                        <colgroup>
                           <col width="16%" align="left">
                           <col width="84%" align="left">
                        </colgroup>
                        <tbody valign="top">
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.SourceAndTarget.html" title="Link to API documentation" target="_top">LocalViewCreator.SourceAndTarget<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">
                                 Shows the source and target nodes of the focused edge.
                                 Optionally, real end nodes of an inter-edge can be shown.
                                 
                              </td>
                           </tr>
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.EdgeGroup.html" title="Link to API documentation" target="_top">LocalViewCreator.EdgeGroup<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">
                                 Shows the source and target nodes of all edges of a given group.
                                 An edge group can be defined as all edges with a common source and/or target or
                                 by edge groups as defined in <a href="layout_advanced_features.html#adv_edge_groups" title="Edge/Port Grouping (Bus-Style Edge Routing)">the section called &#8220;Edge/Port Grouping (Bus-Style Edge Routing)&#8221;</a>.
                                 
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
               <p>
                  Abstract implementation:
                  
               </p>
               <div class="techn_templ">
                  <center>
                     <table width="100%" border="1">
                        <colgroup>
                           <col width="16%" align="left">
                           <col width="84%" align="left">
                        </colgroup>
                        <tbody valign="top">
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html" title="Link to API documentation" target="_top">LocalViewCreator.AbstractLocalViewCreator<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">
                                 Abstract view creator which offers convenience methods to create local views based
                                 on focused graph elements.
                                 All other predefined implementations inherit from this class.
                                 
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
            <div class="section" lang="en">
               <div class="titlepage">
                  <div>
                     <div>
                        <h4 class="title"><a name="local_views_manage"></a>Managing Local Views
                        </h4>
                     </div>
                  </div>
                  <div></div>
               </div>
               <p>
                  Class <a href="../api/y/view/LocalViewCreator.html" title="Link to API documentation" target="_top">LocalViewCreator<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> offers getters for the model graph as well as for the local view graph.
                  It is up to the implementation to set the graph instances. The above listed predefined implementations
                  set the model graph in the constructor and create the local view themselves. The local view graph
                  then has to be set to the <a href="../api/y/view/Graph2DView.html" title="Link to API documentation" target="_top">Graph2DView<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> instance which should
                  display the local view as shown in <a href="viewer_localviews.html#ex_LocalViewCreator_setup" title="Example&nbsp;6.48.&nbsp;Setting up a local view">Example&nbsp;6.48, &#8220;Setting up a local view&#8221;</a>.
                  
               </p>
               <div class="example"><a name="ex_LocalViewCreator_setup"></a><p class="title"><b>Example&nbsp;6.48.&nbsp;Setting up a local view</b></p><pre class="programlisting java">
// 'localView' is of type y.view.Graph2DView.
// 'modelGraph' is of type y.view.Graph2D.

AbstractLocalViewCreator creator = new LocalViewCreator.Neighborhood(modelGraph);
localView.setGraph2D(creator.getViewGraph());
</pre></div>
               <p>
                  <a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html" title="Link to API documentation" target="_top">AbstractLocalViewCreator<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> and the predefined implementations offer convenience methods to
                  create local views based on one or more graph elements which are focused. After setting the focused
                  element the new view is created by invocation of the method
                  <a href="../api/y/view/LocalViewCreator.html#updateViewGraph()" title="Link to API documentation" target="_top">updateViewGraph()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
                  as shown in <a href="viewer_localviews.html#ex_LocalViewCreator_create" title="Example&nbsp;6.49.&nbsp;Creating a local view">Example&nbsp;6.49, &#8220;Creating a local view&#8221;</a>.
                  
               </p>
               <div class="techn_templ">
                  <center>
                     <table width="100%" border="1">
                        <colgroup>
                           <col width="16%" align="left">
                           <col width="84%" align="left">
                        </colgroup>
                        <tbody valign="top">
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#addFocusEdge(y.base.Edge)" title="Link to API documentation" target="_top">void addFocusEdge(Edge edge)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#removeFocusEdge(y.base.Edge)" title="Link to API documentation" target="_top">void removeFocusEdge(Edge edge)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#clearFocusEdges()" title="Link to API documentation" target="_top">void clearFocusEdges()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">Handling focused edges.</td>
                           </tr>
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#addFocusNode(y.base.Node)" title="Link to API documentation" target="_top">void addFocusNode(Node node )<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#removeFocusNode(y.base.Node)" title="Link to API documentation" target="_top">void removeFocusNode(Node node)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#clearFocusNodes()" title="Link to API documentation" target="_top">void clearFocusNodes()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">Handling focused nodes.</td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
               <div class="example"><a name="ex_LocalViewCreator_create"></a><p class="title"><b>Example&nbsp;6.49.&nbsp;Creating a local view</b></p><pre class="programlisting java">
// 'creator' is of type y.view.LocalViewCreator.AbstractLocalViewCreator.
// 'focusNode' is of type y.base.Node.

creator.clearFocusNodes();
creator.addFocusNode(focusNode);
creator.updateViewGraph();
</pre></div>
               <p>
                  In addition to creating a new local view programmatically, <a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html" title="Link to API documentation" target="_top">AbstractLocalViewCreator<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
                  offers the possibility to let the creation be triggered by hovering over a graph item or by selecting
                  a graph item. A selection trigger can be (de)activated automatically by using the add or remove methods.
                  A hover trigger has to be created by the method
                  <a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#createHoverTrigger()" title="Link to API documentation" target="_top">createHoverTrigger()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
                  and added as view mode to the <a href="../api/y/view/Graph2DView.html" title="Link to API documentation" target="_top">Graph2DView<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> instance which display the local view.
                  
               </p>
               <div class="techn_templ">
                  <center>
                     <table width="100%" border="1">
                        <colgroup>
                           <col width="16%" align="left">
                           <col width="84%" align="left">
                        </colgroup>
                        <tbody valign="top">
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#addSelectionTrigger()" title="Link to API documentation" target="_top">void addSelectionTrigger()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#removeSelectionTrigger()" title="Link to API documentation" target="_top">void removeSelectionTrigger()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/LocalViewCreator.AbstractLocalViewCreator.html#createHoverTrigger()" title="Link to API documentation" target="_top">ViewMode createHoverTrigger()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">Managing automatic triggers for hovering/selection.</td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
            <div class="section" lang="en">
               <div class="titlepage">
                  <div>
                     <div>
                        <h4 class="title"><a name="local_view_mappings"></a>Mapping Graph Elements between Local View and Model
                        </h4>
                     </div>
                  </div>
                  <div></div>
               </div>
               <p>
                  Technically, the local view graph is not a subset but a copy of the model graph. Thus, the node or edge
                  instance of the local view is different from the instance of the corresponding model.
                  <a href="../api/y/view/LocalViewCreator.html" title="Link to API documentation" target="_top">LocalViewCreator<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> therefore offers methods to get the model item for a given view item and
                  vice versa.
                  
               </p>
               <div class="techn_templ">
                  <center>
                     <table width="100%" border="1">
                        <colgroup>
                           <col width="16%" align="left">
                           <col width="84%" align="left">
                        </colgroup>
                        <tbody valign="top">
                           <tr class="techn_api">
                              <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/LocalViewCreator.html#getModelNode(y.base.Node)" title="Link to API documentation" target="_top">Node getModelNode(Node view)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/LocalViewCreator.html#getModelEdge(y.base.Edge)" title="Link to API documentation" target="_top">Edge getModelEdge(Edge view)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/LocalViewCreator.html#getViewNode(y.base.Node)" title="Link to API documentation" target="_top">Node getViewNode(Node view)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/LocalViewCreator.html#getViewEdge(y.base.Edge)" title="Link to API documentation" target="_top">Edge getViewEdge(Edge view)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                           </tr>
                           <tr class="spacing">
                              <td align="left">Description</td>
                              <td align="left">Mappings between model and view items.</td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
         </div>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h3 class="title"><a name="tutorial_local_views"></a>Tutorial Demo Code
                     </h3>
                  </div>
               </div>
               <div></div>
            </div>
            <p>
               The tutorial demo application
               <a href="../../src/demo/view/application/LocalViewDemo.java" title="Link to demo code" target="_top">LocalViewDemo.java</a> gives a
               detailed demonstration on how to use and set up the different predefined
               LocalViewCreator implementations. It also shows how to create a custom
               <tt class="code">LocalViewCreator</tt> implementation based on the class
               <tt class="code">AbstractLocalViewCreator</tt>. 
               
            </p>
         </div>
      </div>
      <table class="copyright" border="0" cellpadding="0" cellspacing="0" width="100%">
         <tbody>
            <tr>
               <td align="right">
                  <p class="copyright">Copyright &copy;2004-2015, yWorks GmbH. All rights reserved.</p>
               </td>
            </tr>
         </tbody>
      </table>
      <div class="navfooter">
         <div class="navline2"></div>
         <div style="display:none"><img src="figures/navline2.jpg" alt=""><img src="figures/navbg2.jpg" alt=""></div>
         <table width="100%" summary="Navigation footer">
            <tr>
               <td width="40%" align="left"><a accesskey="p" href="multiviews.html">Prev</a>&nbsp;
               </td>
               <td width="20%" align="center"><a accesskey="u" href="view.html">Up</a></td>
               <td width="40%" align="right">&nbsp;<a accesskey="n" href="printing.html">Next</a></td>
            </tr>
            <tr>
               <td width="40%" align="left" valign="top">Multiple Views on a Common Model Graph&nbsp;</td>
               <td width="20%" align="center"><a accesskey="h" href="index.html" target="_top">Home</a></td>
               <td width="40%" align="right" valign="top">&nbsp;Printing a Graph's Visual Representation</td>
            </tr>
         </table>
      </div>
   </body>
</html>