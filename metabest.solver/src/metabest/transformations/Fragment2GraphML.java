/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.transformations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.ErrorDialog;
 
import org.tzi.use.kodkod.plugin.KodkodValidateCmd;
import org.tzi.use.main.Session;
import org.tzi.use.main.shell.Shell;
import org.tzi.use.main.shell.runtime.IPluginShellCmd;
import org.tzi.use.runtime.impl.PluginRuntime;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.sys.MObject;
import org.tzi.use.uml.sys.MSystem;
import org.tzi.use.uml.sys.MSystemState;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.jesusjlopezf.utils.primitive.NumericUtils;
import com.jesusjlopezf.utils.resources.FileUtils;

import fragments.Fragment;
import fragments.Object;
import fragments.Reference;

import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import metabup.annotations.catalog.design.Composition;
import metabup.annotations.catalog.geometry.Adjacency;
import metabup.annotations.catalog.geometry.Overlapping;
import metabup.sketches.Sketch;
import metabup.sketches.Sketch2Legend;
import metabup.sketches.XMLBasedImporter;
import metabup.sketches.importers.yed.YedImporter;
import metabup.sketches.legend.Legend;
import metabup.sketches.legend.LegendElement;
import metabup.transformations.Activator;
import test.TestModel;

public class Fragment2GraphML {
	Fragment fragment;
	File legendFile;
	HashMap<fragments.Object, String> objectIndex = null;
	boolean addGeometry;
	IProject project;
	
	static private ByteArrayOutputStream errorTrace = new ByteArrayOutputStream();
	
	public Fragment2GraphML(File fragmentFile, File legendFile, IProject project, boolean addGeometry){
		if(!FileUtils.checkExtension(fragmentFile, "mbf")) return;
		this.legendFile = legendFile;
		this.addGeometry = addGeometry;		
		this.fragment = file2Fragment(fragmentFile, addGeometry);
		this.project = project;
	}
	
	public Fragment2GraphML(Fragment fragment, File legendFile, IProject project, boolean addGeometry){
		this.fragment = fragment;
		this.legendFile = legendFile;
		this.addGeometry = addGeometry;
		this.project = project;
	}
	 
	private Fragment file2Fragment(File file, boolean addGeometry) {
		if(file.exists()) System.out.println("El archivo de fragmento existe");
		
		try {											
			if( file != null && file.exists()){
				
				ResourceSet rs = new ResourceSetImpl();
				TestModel tm = null;
				org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(file.toURI().toString());
				Resource r = rs.getResource(uri, true);
				org.eclipse.emf.common.util.TreeIterator<EObject> it = r.getAllContents();
				
				while(it.hasNext()){
					EObject eo = it.next();
					
					if(eo instanceof TestModel){
						tm = (TestModel) eo;
						break;
					}
				}
				
				if(tm == null) return null;
				return tm.getTestableFragments().get(0);
			}
			
		} catch (Exception e) {
			System.out.println("exception");
			System.out.println(e.getMessage() + e.getCause() + e.getLocalizedMessage());
			Status s = new Status(Status.ERROR, metabup.transformations.Activator.PLUGIN_ID, e.getMessage(), e);
			ErrorDialog.openError(null, "Error importing from yED", e.getMessage(), s);
			return null;
		}
		
		return null;
	}

	public File execute(File graphFile) throws IOException, TransformerException, ParserConfigurationException, SAXException{
		objectIndex = new HashMap<fragments.Object, String>();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setValidating(true);
	    factory.setIgnoringElementContentWhitespace(true);
	    
	    Document doc = null;
	    
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    doc = builder.newDocument();	        	        	        
	    
	    XMLBasedImporter legendImporter = new YedImporter();
		Sketch legendSketch = ((YedImporter)legendImporter).read(new FileInputStream(legendFile), "metabup.sketches.legend");
		Legend legend = EcoreUtil.copy(new Sketch2Legend(legendSketch).execute());				
	    
	    // build the graph
	    Element rootNode = addGraphMLNode(doc);
	    addKeys(doc, rootNode);
	    addGraph(doc, rootNode, fragment.getObjects(), legend);
	    
	    TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult(graphFile);
        transformer.transform(domSource, streamResult);

		return null;
	}

	private void addGraph(Document doc, Element rootNode, EList<Object> objects, Legend legend) throws IOException {
		HashMap<String, String> atts = new HashMap<String, String>();
		
		atts.put("edgedefault", "directed");
		atts.put("id", "G");		
		Element graph = addElement(doc, rootNode, "graph", null, atts);
		atts.clear();								

		HashMap<fragments.Object, Integer[]> coordinates = solveGeometry(objects, legend);

		for(fragments.Object o : objects){
			System.out.println(o.getName() + ": {" + coordinates.get(o)[0] + ", " + coordinates.get(o)[1]+ "}");
			String id = "n" + objects.indexOf(o);			
			if(addNode(doc, graph, o, legend, id, coordinates) != null) objectIndex.put(o, id);
		}
		
		int cont = 0;
		
		for(fragments.Object o : objects){
			for(fragments.Feature f : o.getFeatures()){
				if(f instanceof fragments.Reference){
					Reference r = (Reference)f;
					
					if(!r.isAnnotated(Adjacency.NAME) && !r.isAnnotated(Overlapping.NAME) && !r.isAnnotated(Composition.NAME)){
						int increment = addEdge(doc, graph, o, r.getReference(), cont);
						cont = increment;
					}
				}
			}
		}	
	}

	private HashMap<Object, Integer[]> solveGeometry(EList<Object> objects, Legend legend) throws IOException {
		
		// calculate scale for easing the solver's job
		List<Integer> sizeList = new ArrayList<Integer>();
				
		for(fragments.Object o : objects){
			for(LegendElement element : legend.getElements()){
				if(element.getNewSymbolName().toLowerCase().equals(o.getType().toLowerCase())){
					sizeList.add(Math.round(element.getDefaultWidth()));
					sizeList.add(Math.round(element.getDefaultHeight()));
				}
			}
		}
						
		int scale = NumericUtils.getGCD(sizeList);
		
		MSystemState useResult = generateUseInstance(objects, legend, scale);
		HashMap<Object, Integer[]> results = new HashMap<Object, Integer[]>();
		
		//System.out.println(useResult.allObjects().size());
		
		for(fragments.Object o : objects) results.put(o, new Integer[]{0,0});
		
		if(useResult.allObjects() != null && !useResult.allObjects().isEmpty()){
			System.out.println("solution found");
			for (MObject useObject : useResult.allObjects()) {
				for (MAttribute attribute : useObject.state(useResult).attributeValueMap().keySet()) {
					String value = trim( useObject.state(useResult).attributeValueMap().get(attribute).toString() );
					System.out.println(value);
					for(fragments.Object o : objects){
						if((o.getName() + "x").equals(attribute.name())){
							//System.out.println(useObject.state(useResult).attributeValueMap().get(attribute).toString());
							results.get(o)[0] = Integer.parseInt(trim( useObject.state(useResult).attributeValueMap().get(attribute).toString()))*scale;
						}else if ((o.getName() + "y").equals(attribute.name())){
							//System.out.println(useObject.state(useResult).attributeValueMap().get(attribute).toString());
							results.get(o)[1] = Integer.parseInt(trim( useObject.state(useResult).attributeValueMap().get(attribute).toString()))*scale;
						}
					}
				}
			}
		}
		
		return results;
	}
	
	private MSystemState generateUseInstance(List<fragments.Object> objects, Legend legend, int scale) throws IOException {
		final File useFile = new File(project.getFile("geometry_solver.use").getLocation().toString());
		final File propertiesFile = new File(useFile.getAbsolutePath().substring(0, useFile.getAbsolutePath().length()-4)+".properties");
		
		if(useFile != null && useFile.exists()) useFile.delete();
		if(propertiesFile != null && propertiesFile.exists()) propertiesFile.delete();
		
		useFile.createNewFile();
		propertiesFile.createNewFile();
		
		if(!useFile.exists() || !propertiesFile.exists()) return null;
		
		// calculate max canvas width and height
		int maxWidth=0, maxHeight=0;
		
		for(fragments.Object o : objects){
			for(LegendElement element : legend.getElements()){
				if(element.getNewSymbolName().toLowerCase().equals(o.getType().toLowerCase())){
					maxWidth += Math.round(element.getDefaultWidth())/scale;
					maxHeight += Math.round(element.getDefaultHeight())/scale;
				}
			}
		}				
		
		maxWidth += (objects.size()*1);
		maxHeight += (objects.size()*1);				
		
		writeUseFile(useFile, objects, legend, scale);
		
		final Session fSession = new Session();
		fSession.reset();
		Shell.createInstance(fSession, PluginRuntime.getInstance());		
		WrapperErrorConsole.start();  // wrapper for standard error console
		Shell.getInstance().processLineSafely("open " + useFile.getAbsolutePath());
		String error = WrapperErrorConsole.read();
		WrapperErrorConsole.finish(); // restore standard error console
		
		Status status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Invalid USE file", null);
		if (!fSession.hasSystem()) ErrorDialog.openError(null, "Invalid USE file", "USE file contains errors: " + error, status);
		
		String root = null;
		try {
			FileReader fr = new FileReader (new File(useFile.getAbsolutePath()));
			BufferedReader br = new BufferedReader(fr);
			root = br.readLine().split(" ")[1];
			br.close();
		} catch (FileNotFoundException e1) {
			status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "File not found", null);
			ErrorDialog.openError(null, "File not found", "USE file not found", status);
		} catch (IOException e) {}
		
		writePropertyFile(propertiesFile, objects, Integer.max(maxWidth, maxHeight));
		
		errorTrace.reset();
		
		KodkodValidateCmd c = new KodkodValidateCmd() {
			@Override
			public void performCommand(IPluginShellCmd pluginCommand) {
				mSystem = fSession.system();
				mModel = mSystem.model();
				String arguments = " "+ useFile.getAbsolutePath().substring(0, useFile.getAbsolutePath().length()-4)+".properties";;
				handleArguments(arguments);
				System.out.println("current operation: " + mSystem.getCurrentOperation());
			}			
		};
		
		c.performCommand(null);
		
		if (errorTrace.size()>0) {
			status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Model generaton returned errors", null);
			ErrorDialog.openError(null, "USE model generation error", errorTrace.toString(), status);
		}
				
		fSession.system().registerPPCHandlerOverride(Shell.getInstance());
		MSystem system = fSession.system();
		System.out.println(system.state().allObjects());
		return system.state();
	}

	private void writeUseFile(File useFile, List<Object> objects, Legend legend, int scale) throws IOException {
		FileWriter fw = new FileWriter(useFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write("model ConcreteSyntax");
		
		bw.write("\n\nclass Layout");
		bw.write("\n\tattributes");
		
		for(fragments.Object o : objects){
			bw.write("\n\t\t" + o.getName() + "x : Integer");
			bw.write("\n\t\t" + o.getName() + "y : Integer");
		}
		
		bw.write("\n\noperations");
		bw.write("\n\tadjacent (x1:Integer, y1:Integer, w1:Integer, h1:Integer, x2:Integer, y2:Integer, w2:Integer, h2:Integer) : Boolean = (x1+w1 = x2 and ( (y2 < y1 and y1 < y2+h2) or (y2 < y1+h1 and y1+h1 < y2+h2) )) or (x2+w2 = x1 and ( (y1 < y2 and y2 < y1+h1) or (y1 < y2+h2 and y2+h2 < y1+h1) )) or (y1+h1 = y2 and ( (x2 < x1 and x1 < x2+w2) or (x2 < x1+w1 and x1+w1 < x2+w2) )) or (y2+h2 = y1 and ( (x1 < x2 and x2 < x1+w1) or (x1 < x2+w2 and x2+w2 < x1+w1) ))");
		bw.write("\n\toverlaps (x1:Integer, y1:Integer, w1:Integer, h1:Integer, x2:Integer, y2:Integer, w2:Integer, h2:Integer) : Boolean = (x1 = x2 and y1 = y2) or ( ((x1 <= x2 and x2 < x1+w1) or (x1 < x2+w2 and x2+w2 <= x1+w1)) and ( (y1 <= y2 and y2 < y1+h1) or (y1 < y2+h2 and y2+h2 <= y1+h1) )) or ( ((x2 <= x1 and x1 < x2+w2) or (x2 < x1+w1 and x1+w1 <= x2+w2)) and ( (y2 <= y1 and y1 < y2+h2) or (y2 < y1+h1 and y1+h1 <= y2+h2) )) or ( ((y1 <= y2 and y2 < y1+h1) or (y1 < y2+h2 and y2+h2 <= y1+h1)) and ( (x1 <= x2 and x2 < x1+w1) or (x1 < x2+w2 and x2+w2 <= x1+w1) )) or ( ((y2 <= y1 and y1 < y2+h2) or (y2 < y1+h1 and y1+h1 <= y2+h2)) and ( (x2 <= x1 and x1 < x2+w2) or (x2 < x1+w1 and x1+w1 <= x2+w2) )) ");
		
		bw.write("\n\nconstraints");
				
		for(fragments.Object srcObj : objects){
			for(fragments.Feature f : srcObj.getFeatures()){
				if(f instanceof fragments.Reference){					
					Reference r = (Reference)f;			
					
					if(r.isAnnotated("adjacency")){
						for(fragments.Object tarObj : r.getReference())
							bw.write(this.getAdjacencyConstraint(srcObj, tarObj, legend, scale));
					}
					else if(r.isAnnotated("overlapping")){
						for(fragments.Object tarObj : r.getReference())
							bw.write(this.getOverlappingConstraint(srcObj, tarObj, legend, scale, true));
						
						
						for(fragments.Object tarObj : objects){
							if(tarObj != srcObj && !r.getReference().contains(tarObj))
								bw.write(this.getOverlappingConstraint(srcObj, tarObj, legend, scale, false));
						}
					}
					else if(r.isAnnotated(Composition.NAME)){					
						// nothing so far
					}
				}
			}
		}
		
		bw.write("\n\nend");
		bw.close();
		fw.close();
	}
	
	private String getAdjacencyConstraint(fragments.Object o1, fragments.Object o2, Legend legend, int scale){
		//inv: adjacent(room1x, room1y, 10, 10, room3x, room3y, 10, 10)
		int o1w = this.getObjectWidth(o1, legend)/scale;
		int o2w = this.getObjectWidth(o2, legend)/scale;
		int o1h = this.getObjectHeight(o1, legend)/scale;
		int o2h = this.getObjectHeight(o2, legend)/scale;
		
		return "\n\tinv: adjacent(" + o1.getName() + "x, " + o1.getName() +"y, " + o1w + ", " + o1h + ", " + o2.getName()+ "x, " + o2.getName() + "y, " + o2w + ", " + o2h + ")";
	}
	
	private String getOverlappingConstraint(fragments.Object o1, fragments.Object o2, Legend legend, int scale, boolean positive){
		//inv: adjacent(room1x, room1y, 10, 10, room3x, room3y, 10, 10)
		int o1w = this.getObjectWidth(o1, legend)/scale;
		int o2w = this.getObjectWidth(o2, legend)/scale;
		int o1h = this.getObjectHeight(o1, legend)/scale;
		int o2h = this.getObjectHeight(o2, legend)/scale;
		
		String constraint = "\n\tinv :";
		if(!positive) constraint += " not ";
		constraint += "overlaps(" + o1.getName() + "x, " + o1.getName() +"y, " + o1w + ", " + o1h + ", " + o2.getName()+ "x, " + o2.getName() + "y, " + o2w + ", " + o2h + ")";
		
		return constraint;
	}
	
	private void writePropertyFile(File propertiesFile, List<fragments.Object> objects, int max) throws IOException {
		FileWriter fw = new FileWriter(propertiesFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write("Layout_min = 1");
		bw.write("\nLayout_max = 1");
		bw.write("\nInteger_min = 0");
		bw.write("\nInteger_max = " + max);
		
		for(fragments.Object o : objects){
			bw.write("\nLayout_" + o.getName() +"x_min = 1");
			bw.write("\nLayout_" + o.getName() +"x_max = 1");
			bw.write("\nLayout_" + o.getName() +"y_min = 1");
			bw.write("\nLayout_" + o.getName() +"y_max = 1");
		}
					
		bw.close();
		fw.close();
		
	}

	private int getObjectWidth(fragments.Object o, Legend legend){
		for(LegendElement element : legend.getElements()){
			if(element.getNewSymbolName().toLowerCase().equals(o.getType().toLowerCase())){
				return Math.round(element.getDefaultWidth());
			}
		}
		
		return 0;
	}
	
	private int getObjectHeight(fragments.Object o, Legend legend){
		for(LegendElement element : legend.getElements()){
			if(element.getNewSymbolName().toLowerCase().equals(o.getType().toLowerCase())){
				return Math.round(element.getDefaultHeight());
			}
		}
		
		return 0;
	}
	
	private int addEdge(Document doc, Element graph, Object source, EList<Object> targets, int cont) {
		HashMap<String, String> atts = new HashMap<String, String>();
		
		for(fragments.Object o : targets){
			atts.put("id", "e"+cont);
			atts.put("source", objectIndex.get(source));
			atts.put("target", objectIndex.get(o));
			Element edge = addElement(doc, graph, "edge", null, atts);
			atts.clear();
			
			// the reference name might be added
			
			cont++;
		}
						
		return cont;
	}

	private Element addNode(Document doc, Element graph, Object o, Legend legend, String id, HashMap<Object, Integer[]> coordinates) {				
		//System.out.println("serializing " + o.getName());
		HashMap<String, String> atts = new HashMap<String, String>();	
		
		atts.put("id", id);
		Element node = addElement(doc, graph, "node", null, atts);
		atts.clear();
		
		atts.put("key", "d5");
		addElement(doc, node, "data", null, atts);
		atts.clear();
		
		atts.put("key", "d6");
		Element d6 = addElement(doc, node, "data", null, atts);
		atts.clear();
		
		Element shapeNode = addElement(doc, d6, "y:ShapeNode", null, null);
		
		// set the object type and graphic settings according to the metabup.sketches.legend
		LegendElement legendElement = null;
		
		//System.out.println(metabup.sketches.legend.toString());
		
		for(LegendElement element : legend.getElements()){
			if(element.getNewSymbolName().toLowerCase().equals(o.getType().toLowerCase())){
				legendElement = element;
				break;
			}
		}
				
		if(legendElement == null){
			System.out.println("The fragment features an object type which can't be found in the metabup.sketches.legend");
			return null;
		}
		
		//  outta be calculated
		int height = Math.round(legendElement.getDefaultHeight());
		int width = Math.round(legendElement.getDefaultWidth());		
		int x = 0, y = 0;				
		
		if(coordinates.get(o) != null){
			x = coordinates.get(o)[0];
			y = coordinates.get(o)[1];
		}
		
		/*
		 * PARA LEERLOS DEL FRAGMENTO
		 * if(o.getAttributeByName("x") != null) x = ((IntegerValue)o.getAttributeByName("x").getValues().get(0)).getValue();
		if(o.getAttributeByName("y") != null) y = ((IntegerValue)o.getAttributeByName("y").getValues().get(0)).getValue();
		if(o.getAttributeByName("width") != null) width = ((IntegerValue)o.getAttributeByName("width").getValues().get(0)).getValue();
		if(o.getAttributeByName("height") != null) height = ((IntegerValue)o.getAttributeByName("height").getValues().get(0)).getValue();*/
		
		atts.put("height", Integer.toString(height));
		atts.put("width", Integer.toString(width));
		atts.put("x", Integer.toString(x));
		atts.put("y", Integer.toString(y));
		addElement(doc, shapeNode, "y:Geometry", null, atts);
		atts.clear();
		
		//System.out.println("setting color = " + legendElement.getColor() + " at the transformer");
		
		atts.put("color", legendElement.getColor());
		atts.put("transparent", String.valueOf(legendElement.isTransparent()));
		addElement(doc, shapeNode, "y:Fill", null, atts);
		atts.clear();
		
		addElement(doc, shapeNode, "y:NodeLabel", o.getName(), null);				
		
		atts.put("type", legendElement.getOriginalSymbolName());
		addElement(doc, shapeNode, "y:Shape", null, atts);
		atts.clear();
		
		return node;
	}

	private void addKeys(Document doc, Element root) {
		HashMap<String, String> atts = new HashMap<String, String>();				
		
		atts.put("for", "graphml");
		atts.put("id", "d0");
		atts.put("yfiles.type", "resources");				
		addElement(doc, root, "key", null, atts);		
		atts.clear();
		
		atts.put("for", "port");
		atts.put("id", "d1");
		atts.put("yfiles.type", "portgraphics");
		addElement(doc, root, "key", null, atts);
		atts.clear();
		
		atts.put("for", "port");
		atts.put("id", "d2");
		atts.put("yfiles.type", "portgeometry");
		addElement(doc, root, "key", null, atts);
		atts.clear();
		
		atts.put("for", "port");
		atts.put("id", "d3");
		atts.put("yfiles.type", "portuserdata");
		addElement(doc, root, "key", null, atts);
		atts.clear();
		
		atts.put("attr.name", "url");
		atts.put("attr.type", "string");
		atts.put("for", "node");
		atts.put("id", "d4");
		addElement(doc, root, "key", null, atts);
		atts.clear();
		
		atts.put("attr.name", "description");
		atts.put("attr.type", "string");
		atts.put("for", "node");
		atts.put("id", "d5");
		addElement(doc, root, "key", null, atts);
		atts.clear();
		
		atts.put("for", "node");
		atts.put("id", "d6");
		atts.put("yfiles.type", "nodegraphics");
		addElement(doc, root, "key", null, atts);
		atts.clear();
		
		atts.put("attr.name", "url");
		atts.put("attr.type", "string");
		atts.put("for", "edge");
		atts.put("id", "d7");
		addElement(doc, root, "key", null, atts);
		atts.clear();
		
		atts.put("attr.name", "description");
		atts.put("attr.type", "string");
		atts.put("for", "edge");
		atts.put("id", "d8");
		addElement(doc, root, "key", null, atts);
		atts.clear();
		
		atts.put("for", "edge");
		atts.put("id", "d9");
		atts.put("yfiles.type", "edgegraphics");
		addElement(doc, root, "key", null, atts);
		atts.clear();
	}

	private Element addGraphMLNode(Document doc) {
		HashMap<String, String> atts = new HashMap<String, String>();
		
		atts.put("xmlns", "http://graphml.graphdrawing.org/xmlns");
		atts.put("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		atts.put("xmlns:y", "http://www.yworks.com/xml/graphml");
		atts.put("xmlns:yed", "http://www.yworks.com/xml/yed/3");
		atts.put("xsi:schemaLocation", "http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd");
		
		return addElement(doc, null, "graphml", null, atts);
	}
	
	private Element addElement(Document doc, Element parent, String name, String textNode, HashMap<String, String> attributes){
		Element elem = doc.createElement(name);			
		
		if(attributes != null){
			Iterator it = attributes.entrySet().iterator();
			    
			while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        
		        Attr attr = doc.createAttribute((String)pair.getKey());
		        attr.setValue((String)pair.getValue());
		        elem.setAttributeNode(attr);	        
		    }
		}
		
		if(textNode != null) elem.appendChild(doc.createTextNode(textNode));
		
        if(parent == null) doc.appendChild(elem);
        else parent.appendChild(elem);
	
        return elem;
	}
	
	static class WrapperErrorConsole extends PrintStream {

	    private static StringBuilder       buffer;
	    private static PrintStream         standardConsole;
	    private static WrapperErrorConsole wrapperConsole = null;
	    
	    private WrapperErrorConsole(StringBuilder sb, OutputStream os, PrintStream ul) {
	        super(os);
	        buffer          = sb;
	        standardConsole = ul;
	    }
	    
	    // redirect system.err to buffer
	    public static WrapperErrorConsole start() {
	        try {
	            final StringBuilder sb = new StringBuilder();
	            Field f = FilterOutputStream.class.getDeclaredField("out");
	            f.setAccessible(true);
	            OutputStream psout = (OutputStream) f.get(System.err);
	            wrapperConsole = new WrapperErrorConsole(
	            		    sb, 
	            			new FilterOutputStream(psout) {
	            				public void write(int b) throws IOException {
	            					super.write(b);
	            					sb.append((char) b);
	            				}
	            			}, 
	            			System.err);
	            System.setErr(wrapperConsole);
	        } 
	        catch (NoSuchFieldException shouldNotHappen)     {} 
	        catch (IllegalArgumentException shouldNotHappen) {} 
	        catch (IllegalAccessException shouldNotHappen)   {}
	        return null;
	    }
	    
	    // get content of buffer
	    public static String read() {
	    	if (wrapperConsole!=null) {
	    		System.err.flush();
	    		return buffer.toString();
	    	}
	    	else return "";
	    }
	    
	    // restore system.err
	    public static void finish() {
	    	if (standardConsole!=null) {
	    		System.setErr(standardConsole);
	    		standardConsole = null;
	    		wrapperConsole  = null;
	    	}
	    }
	}
	
	private String trim (String phrase) {
		while (phrase.startsWith("'")) phrase = phrase.substring(1);
		while (phrase.endsWith("'"))   phrase = phrase.substring(0, phrase.length()-1);
		return phrase;
	}
}
