/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.impl;

import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import fragments.FragmentsPackage;
import test.Assertion;
import test.AssertionSet;
import test.ComplementaryBlock;
import test.CompletionType;
import test.ExtensionAssertion;
import test.ExtensionAssertionSet;
import test.FragmentObjectExtensionAssertion;
import test.MetaClassExtensionAssertion;
import test.TestFactory;
import test.TestModel;
import test.TestPackage;
import test.TestableFragment;
import test.checks.ChecksPackage;
import test.checks.expressions.ExpressionsPackage;
import test.checks.expressions.impl.ExpressionsPackageImpl;
import test.checks.impl.ChecksPackageImpl;
import test.extensions.ExtensionsPackage;
import test.extensions.impl.ExtensionsPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TestPackageImpl extends EPackageImpl implements TestPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass testableFragmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertionSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assertionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extensionAssertionSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metaClassExtensionAssertionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extensionAssertionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fragmentObjectExtensionAssertionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complementaryBlockEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum completionTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see test.TestPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TestPackageImpl() {
		super(eNS_URI, TestFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TestPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TestPackage init() {
		if (isInited) return (TestPackage)EPackage.Registry.INSTANCE.getEPackage(TestPackage.eNS_URI);

		// Obtain or create and register package
		TestPackageImpl theTestPackage = (TestPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TestPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TestPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		FragmentsPackage.eINSTANCE.eClass();
		MetamodeltestPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ChecksPackageImpl theChecksPackage = (ChecksPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ChecksPackage.eNS_URI) instanceof ChecksPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ChecksPackage.eNS_URI) : ChecksPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExpressionsPackage.eNS_URI) : ExpressionsPackage.eINSTANCE);
		ExtensionsPackageImpl theExtensionsPackage = (ExtensionsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExtensionsPackage.eNS_URI) instanceof ExtensionsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExtensionsPackage.eNS_URI) : ExtensionsPackage.eINSTANCE);

		// Create package meta-data objects
		theTestPackage.createPackageContents();
		theChecksPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theExtensionsPackage.createPackageContents();

		// Initialize created meta-data
		theTestPackage.initializePackageContents();
		theChecksPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theExtensionsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTestPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TestPackage.eNS_URI, theTestPackage);
		return theTestPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestModel() {
		return testModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestModel_ImportURI() {
		return (EAttribute)testModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestModel_TestableFragments() {
		return (EReference)testModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTestableFragment() {
		return testableFragmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTestableFragment_AssertionSet() {
		return (EReference)testableFragmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTestableFragment_Completion() {
		return (EAttribute)testableFragmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertionSet() {
		return assertionSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertionSet_Assertions() {
		return (EReference)assertionSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssertion() {
		return assertionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAssertion_Check() {
		return (EReference)assertionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtensionAssertionSet() {
		return extensionAssertionSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExtensionAssertionSet_Assertions() {
		return (EReference)extensionAssertionSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetaClassExtensionAssertion() {
		return metaClassExtensionAssertionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaClassExtensionAssertion_Selector() {
		return (EReference)metaClassExtensionAssertionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaClassExtensionAssertion_Condition() {
		return (EReference)metaClassExtensionAssertionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExtensionAssertion() {
		return extensionAssertionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFragmentObjectExtensionAssertion() {
		return fragmentObjectExtensionAssertionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFragmentObjectExtensionAssertion_Selector() {
		return (EReference)fragmentObjectExtensionAssertionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFragmentObjectExtensionAssertion_Condition() {
		return (EReference)fragmentObjectExtensionAssertionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplementaryBlock() {
		return complementaryBlockEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplementaryBlock_AtLeast() {
		return (EAttribute)complementaryBlockEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComplementaryBlock_Description() {
		return (EAttribute)complementaryBlockEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCompletionType() {
		return completionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TestFactory getTestFactory() {
		return (TestFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		testModelEClass = createEClass(TEST_MODEL);
		createEAttribute(testModelEClass, TEST_MODEL__IMPORT_URI);
		createEReference(testModelEClass, TEST_MODEL__TESTABLE_FRAGMENTS);

		testableFragmentEClass = createEClass(TESTABLE_FRAGMENT);
		createEReference(testableFragmentEClass, TESTABLE_FRAGMENT__ASSERTION_SET);
		createEAttribute(testableFragmentEClass, TESTABLE_FRAGMENT__COMPLETION);

		assertionSetEClass = createEClass(ASSERTION_SET);
		createEReference(assertionSetEClass, ASSERTION_SET__ASSERTIONS);

		assertionEClass = createEClass(ASSERTION);
		createEReference(assertionEClass, ASSERTION__CHECK);

		extensionAssertionSetEClass = createEClass(EXTENSION_ASSERTION_SET);
		createEReference(extensionAssertionSetEClass, EXTENSION_ASSERTION_SET__ASSERTIONS);

		metaClassExtensionAssertionEClass = createEClass(META_CLASS_EXTENSION_ASSERTION);
		createEReference(metaClassExtensionAssertionEClass, META_CLASS_EXTENSION_ASSERTION__SELECTOR);
		createEReference(metaClassExtensionAssertionEClass, META_CLASS_EXTENSION_ASSERTION__CONDITION);

		complementaryBlockEClass = createEClass(COMPLEMENTARY_BLOCK);
		createEAttribute(complementaryBlockEClass, COMPLEMENTARY_BLOCK__AT_LEAST);
		createEAttribute(complementaryBlockEClass, COMPLEMENTARY_BLOCK__DESCRIPTION);

		extensionAssertionEClass = createEClass(EXTENSION_ASSERTION);

		fragmentObjectExtensionAssertionEClass = createEClass(FRAGMENT_OBJECT_EXTENSION_ASSERTION);
		createEReference(fragmentObjectExtensionAssertionEClass, FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR);
		createEReference(fragmentObjectExtensionAssertionEClass, FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION);

		// Create enums
		completionTypeEEnum = createEEnum(COMPLETION_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ChecksPackage theChecksPackage = (ChecksPackage)EPackage.Registry.INSTANCE.getEPackage(ChecksPackage.eNS_URI);
		ExtensionsPackage theExtensionsPackage = (ExtensionsPackage)EPackage.Registry.INSTANCE.getEPackage(ExtensionsPackage.eNS_URI);
		FragmentsPackage theFragmentsPackage = (FragmentsPackage)EPackage.Registry.INSTANCE.getEPackage(FragmentsPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theChecksPackage);
		getESubpackages().add(theExtensionsPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		testableFragmentEClass.getESuperTypes().add(theFragmentsPackage.getFragment());
		assertionSetEClass.getESuperTypes().add(this.getComplementaryBlock());
		extensionAssertionSetEClass.getESuperTypes().add(this.getComplementaryBlock());
		metaClassExtensionAssertionEClass.getESuperTypes().add(this.getExtensionAssertion());
		fragmentObjectExtensionAssertionEClass.getESuperTypes().add(this.getExtensionAssertion());

		// Initialize classes, features, and operations; add parameters
		initEClass(testModelEClass, TestModel.class, "TestModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTestModel_ImportURI(), ecorePackage.getEString(), "importURI", null, 1, 1, TestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTestModel_TestableFragments(), this.getTestableFragment(), null, "testableFragments", null, 1, -1, TestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(testableFragmentEClass, TestableFragment.class, "TestableFragment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTestableFragment_AssertionSet(), this.getComplementaryBlock(), null, "assertionSet", null, 0, 1, TestableFragment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTestableFragment_Completion(), this.getCompletionType(), "completion", null, 1, 1, TestableFragment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertionSetEClass, AssertionSet.class, "AssertionSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssertionSet_Assertions(), this.getAssertion(), null, "assertions", null, 0, -1, AssertionSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assertionEClass, Assertion.class, "Assertion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAssertion_Check(), theChecksPackage.getCheck(), null, "check", null, 1, 1, Assertion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(extensionAssertionSetEClass, ExtensionAssertionSet.class, "ExtensionAssertionSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExtensionAssertionSet_Assertions(), this.getExtensionAssertion(), null, "assertions", null, 1, -1, ExtensionAssertionSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(metaClassExtensionAssertionEClass, MetaClassExtensionAssertion.class, "MetaClassExtensionAssertion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetaClassExtensionAssertion_Selector(), theExtensionsPackage.getMetaClassSelector(), null, "selector", null, 1, 1, MetaClassExtensionAssertion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetaClassExtensionAssertion_Condition(), theExtensionsPackage.getMetaClassQualifier(), null, "condition", null, 0, 1, MetaClassExtensionAssertion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complementaryBlockEClass, ComplementaryBlock.class, "ComplementaryBlock", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComplementaryBlock_AtLeast(), ecorePackage.getEBoolean(), "atLeast", null, 0, 1, ComplementaryBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComplementaryBlock_Description(), ecorePackage.getEString(), "description", null, 0, 1, ComplementaryBlock.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(extensionAssertionEClass, ExtensionAssertion.class, "ExtensionAssertion", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fragmentObjectExtensionAssertionEClass, FragmentObjectExtensionAssertion.class, "FragmentObjectExtensionAssertion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFragmentObjectExtensionAssertion_Selector(), theExtensionsPackage.getFragmentObjectSelector(), null, "selector", null, 1, 1, FragmentObjectExtensionAssertion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFragmentObjectExtensionAssertion_Condition(), theExtensionsPackage.getFragmentObjectQualifier(), null, "condition", null, 0, 1, FragmentObjectExtensionAssertion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(completionTypeEEnum, CompletionType.class, "CompletionType");
		addEEnumLiteral(completionTypeEEnum, CompletionType.IS_FRAGMENT);
		addEEnumLiteral(completionTypeEEnum, CompletionType.IS_EXAMPLE);

		// Create resource
		createResource(eNS_URI);
	}

} //TestPackageImpl
