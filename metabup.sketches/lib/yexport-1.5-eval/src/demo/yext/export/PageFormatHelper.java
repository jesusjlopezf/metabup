/****************************************************************************
 * This demo file is part of the yFiles extension package yExport-1.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * yExport demo files exhibit yExport functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a yExport version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own yExport powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the yExport
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.export;


import javax.print.attribute.standard.MediaSize;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.Insets;


/**
 * Utility class that provides convenience methods to set the size and margins
 * for <code>PageFormat</code> instances.
 *
 */
public class PageFormatHelper {
  /**
   * Sets the paper size of the given <code>PageFormat</code> instance to
   * match the specified <code>MediaSize</code>.
   */
  public static void assignMediaSize(PageFormat pageFormat, MediaSize mediaSize) {
    float[] size = mediaSize.getSize(MediaSize.INCH);
    int dpi = 72; //Toolkit.getDefaultToolkit().getScreenResolution();
    size[0] *= dpi;
    size[1] *= dpi;
    Paper paper = new Paper();
    paper.setSize(size[0], size[1]);
    paper.setImageableArea(0, 0, size[0], size[1]);
    pageFormat.setPaper(paper);
  }

  /**
   * Returns the <em>margins</em> for the given <code>PageFormat</code>
   * instance.
   */
  public static Insets getMargin(PageFormat pageFormat) {
    final int top  = (int)Math.ceil(pageFormat.getImageableY());
    final int left = (int)Math.ceil(pageFormat.getImageableX());
    return new Insets(top, left,
                      (int)Math.ceil(pageFormat.getHeight()
                                     - pageFormat.getImageableHeight()) - top,
                      (int)Math.ceil(pageFormat.getWidth()
                                     - pageFormat.getImageableWidth())  - left);
  }

  /**
   * Sets the specified <code>Insets</code> as <em>margins</em> for the given
   * <code>PageFormat</code> instance.
   */
  public static void setMargin(PageFormat pageFormat, Insets insets) {
    Paper paper = pageFormat.getPaper();
    paper.setImageableArea(
            insets.left,
            insets.top,
            paper.getWidth() - insets.left - insets.right,
            paper.getHeight() - insets.top - insets.bottom
    );
    pageFormat.setPaper(paper);
  }

  private PageFormatHelper() {
  }
}