package metabup.sketches.preferences;

public class SketchPreferenceConstants {
	public static final String AssociationNamingCriteriaPreference = "associationNamingCriteriaPreference";
		// possible values
		public static final String OBJECT  = "object";
		public static final String STYLE  = "style";
}
