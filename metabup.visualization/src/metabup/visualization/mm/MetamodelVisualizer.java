/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.visualization.mm;

import java.util.List;
import java.util.Map;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaModel;
import metabup.visualization.Activator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.gef4.common.inject.AdaptableScopes;
import org.eclipse.gef4.layout.algorithms.SugiyamaLayoutAlgorithm;
import org.eclipse.gef4.mvc.behaviors.IBehavior;
import org.eclipse.gef4.mvc.fx.viewer.FXViewer;
import org.eclipse.gef4.mvc.parts.IContentPart;
import org.eclipse.gef4.mvc.parts.IContentPartFactory;
import org.eclipse.gef4.zest.fx.ZestFxModule;
import org.eclipse.gef4.zest.fx.parts.ContentPartFactory;
import org.eclipse.gef4.zest.fx.ui.jface.ZestContentViewer;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.TypeLiteral;

import javafx.scene.Node;


public class MetamodelVisualizer {
	private AnnotatedElement selection = null;
	
	public static ZestContentViewer createViewer(Composite parent, MetaModel mm) {
		final ZestContentViewer viewer = new ZestContentViewer(new CustomModule(mm));
		
		viewer.createControl(parent, SWT.NONE);
		viewer.setLabelProvider(new MetamodelLabelProvider(mm, null, null, null));
		viewer.setContentProvider(new MetamodelContentProvider(mm, viewer.getContentNodeMap()));
		viewer.setLayoutAlgorithm(new SugiyamaLayoutAlgorithm());
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				System.out.println("Selection changed: "
						+ (event.getSelection()));
			}
		});
		
		try{
			viewer.setInput(mm);
		}catch (Exception e){
			Activator.log(IStatus.ERROR, e.getMessage());
			MessageDialog.openError(null, "error", e.getMessage());
			return viewer;
		}
		
		//viewer.refresh();

		return viewer;
	}
	
	public static ZestContentViewer createViewer(Composite parent, MetaModel mm, List<AnnotatedElement> right, List<AnnotatedElement> warnings, List<AnnotatedElement> errors) {
		final ZestContentViewer viewer = new ZestContentViewer(new CustomModule(mm));

		viewer.createControl(parent, SWT.NONE);
		viewer.setLabelProvider(new MetamodelLabelProvider(mm, right, warnings, errors));
		viewer.setContentProvider(new MetamodelContentProvider(mm, viewer.getContentNodeMap()));		
		viewer.setLayoutAlgorithm(new SugiyamaLayoutAlgorithm());
		
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				System.out.println("Selection changed: "
						+ (event.getSelection()));
			}
		});
		
		try{
			viewer.setInput(mm);
		} catch (Exception e){
			Activator.log(IStatus.ERROR, e.getMessage());
			//MessageDialog.openError(null, "error", e.getMessage());
			return viewer;
		}
		
		//viewer.refresh();
		
		return viewer;
	}
	
	public static class CustomModule extends ZestFxModule {
		MetaModel mm;
		
		CustomModule(MetaModel mm){
			this.mm = mm;
		}
		
		@Override
		protected void bindIContentPartFactory() {
			binder().bind(new TypeLiteral<IContentPartFactory<Node>>() {
			}).to(CustomContentPartFactory.class)
					.in(AdaptableScopes.typed(FXViewer.class));
		}
	}
	
	public static class CustomContentPartFactory extends ContentPartFactory {
		@Inject
		private Injector injector;

		@Override
		public IContentPart<Node, ? extends Node> createContentPart(
				Object content, IBehavior<Node> contextBehavior,
				Map<Object, Object> contextMap) {
			if (content instanceof org.eclipse.gef4.graph.Node) {
				// create custom node if we find the custom attribute
				org.eclipse.gef4.graph.Node n = (org.eclipse.gef4.graph.Node) content;
				if (n.getAttrs().containsKey(MetaClassContentPart.ATTR_CUSTOM)) {
					MetaClassContentPart part = new MetaClassContentPart();
					if (part != null) {
						injector.injectMembers(part);
					}
					return part;
				}
			}
			return super.createContentPart(content, contextBehavior,
					contextMap);
		}
	}
}