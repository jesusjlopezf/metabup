package com.jesusjlopezf.utils.eclipse.resources;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

public class IFolderUtils {
	/**
	 * Folder deletion via IFolder instance.
	 *
	 * @param folder the folder
	 * @return boolean	True-if the folder was successfully removed; False either.
	 */
	public static boolean deleteIFolder(IFolder folder){
		while(!folder.isAccessible()){};
		while(folder.exists()){
			try {					
				folder.delete(true, null);
				return true;
			} catch (CoreException e) {				
			}
		}
		
		return false;
	}
	
	public static boolean createIFolder(IFolder parent, String folderName){
		IFolder folder = parent.getFolder(folderName);
		if(folder.exists()) return false;
		
		try {
			folder.create(IResource.NONE, true, null);
		} catch (CoreException e) {
			return false;
		}
		
		return true;
	}		
	
	public static List<IFile> getFilesFromFolder(IContainer container, String format, boolean recursive) throws CoreException{				
		IResource folderMembers[] = container.members();
		List<IFile> files = new ArrayList<IFile>();						
		
		for(int i=0; i<folderMembers.length; i++){
			if((folderMembers[i].getFileExtension() != null) && (folderMembers[i].getFileExtension().equals("mbup"))){
				files.add((IFile)folderMembers[i]);				
			}else{
				if(recursive && folderMembers[i].getFileExtension() == null){
					List<IFile> moreQueries = getFilesFromFolder((IContainer)folderMembers[i], format, recursive);					
					if(!moreQueries.isEmpty()) files.addAll(moreQueries);
				}
			}
		}
		
		return files;
	}
}
