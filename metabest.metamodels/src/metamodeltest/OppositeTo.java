/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Opposite To</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.OppositeTo#getRefSel <em>Ref Sel</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getOppositeTo()
 * @model
 * @generated
 */
public interface OppositeTo extends ReferenceQualifier {
	/**
	 * Returns the value of the '<em><b>Ref Sel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Sel</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Sel</em>' containment reference.
	 * @see #setRefSel(ReferenceSelector)
	 * @see metamodeltest.MetamodeltestPackage#getOppositeTo_RefSel()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ReferenceSelector getRefSel();

	/**
	 * Sets the value of the '{@link metamodeltest.OppositeTo#getRefSel <em>Ref Sel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref Sel</em>' containment reference.
	 * @see #getRefSel()
	 * @generated
	 */
	void setRefSel(ReferenceSelector value);

} // OppositeTo
