/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.MetamodeltestPackage;
import metamodeltest.PathFilter;
import metamodeltest.PathSelector;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Path Selector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.PathSelectorImpl#getPathFilter <em>Path Filter</em>}</li>
 *   <li>{@link metamodeltest.impl.PathSelectorImpl#getPathConditioner <em>Path Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PathSelectorImpl extends SelectorImpl implements PathSelector {
	/**
	 * The cached value of the '{@link #getPathFilter() <em>Path Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPathFilter()
	 * @generated
	 * @ordered
	 */
	protected PathFilter pathFilter;

	/**
	 * The cached value of the '{@link #getPathConditioner() <em>Path Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPathConditioner()
	 * @generated
	 * @ordered
	 */
	protected PathFilter pathConditioner;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PathSelectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.PATH_SELECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathFilter getPathFilter() {
		return pathFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPathFilter(PathFilter newPathFilter, NotificationChain msgs) {
		PathFilter oldPathFilter = pathFilter;
		pathFilter = newPathFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PATH_SELECTOR__PATH_FILTER, oldPathFilter, newPathFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPathFilter(PathFilter newPathFilter) {
		if (newPathFilter != pathFilter) {
			NotificationChain msgs = null;
			if (pathFilter != null)
				msgs = ((InternalEObject)pathFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PATH_SELECTOR__PATH_FILTER, null, msgs);
			if (newPathFilter != null)
				msgs = ((InternalEObject)newPathFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PATH_SELECTOR__PATH_FILTER, null, msgs);
			msgs = basicSetPathFilter(newPathFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PATH_SELECTOR__PATH_FILTER, newPathFilter, newPathFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathFilter getPathConditioner() {
		return pathConditioner;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPathConditioner(PathFilter newPathConditioner, NotificationChain msgs) {
		PathFilter oldPathConditioner = pathConditioner;
		pathConditioner = newPathConditioner;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PATH_SELECTOR__PATH_CONDITIONER, oldPathConditioner, newPathConditioner);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPathConditioner(PathFilter newPathConditioner) {
		if (newPathConditioner != pathConditioner) {
			NotificationChain msgs = null;
			if (pathConditioner != null)
				msgs = ((InternalEObject)pathConditioner).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PATH_SELECTOR__PATH_CONDITIONER, null, msgs);
			if (newPathConditioner != null)
				msgs = ((InternalEObject)newPathConditioner).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PATH_SELECTOR__PATH_CONDITIONER, null, msgs);
			msgs = basicSetPathConditioner(newPathConditioner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PATH_SELECTOR__PATH_CONDITIONER, newPathConditioner, newPathConditioner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SELECTOR__PATH_FILTER:
				return basicSetPathFilter(null, msgs);
			case MetamodeltestPackage.PATH_SELECTOR__PATH_CONDITIONER:
				return basicSetPathConditioner(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SELECTOR__PATH_FILTER:
				return getPathFilter();
			case MetamodeltestPackage.PATH_SELECTOR__PATH_CONDITIONER:
				return getPathConditioner();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SELECTOR__PATH_FILTER:
				setPathFilter((PathFilter)newValue);
				return;
			case MetamodeltestPackage.PATH_SELECTOR__PATH_CONDITIONER:
				setPathConditioner((PathFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SELECTOR__PATH_FILTER:
				setPathFilter((PathFilter)null);
				return;
			case MetamodeltestPackage.PATH_SELECTOR__PATH_CONDITIONER:
				setPathConditioner((PathFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SELECTOR__PATH_FILTER:
				return pathFilter != null;
			case MetamodeltestPackage.PATH_SELECTOR__PATH_CONDITIONER:
				return pathConditioner != null;
		}
		return super.eIsSet(featureID);
	}

} //PathSelectorImpl
