/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.ClassCollects;
import metamodeltest.MetamodeltestPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Collects</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.ClassCollectsImpl#getCollectedMult <em>Collected Mult</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClassCollectsImpl extends ClassReachesImpl implements ClassCollects {
	/**
	 * The cached value of the '{@link #getCollectedMult() <em>Collected Mult</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollectedMult()
	 * @generated
	 * @ordered
	 */
	protected metamodeltest.Number collectedMult;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassCollectsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.CLASS_COLLECTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public metamodeltest.Number getCollectedMult() {
		return collectedMult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCollectedMult(metamodeltest.Number newCollectedMult, NotificationChain msgs) {
		metamodeltest.Number oldCollectedMult = collectedMult;
		collectedMult = newCollectedMult;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_COLLECTS__COLLECTED_MULT, oldCollectedMult, newCollectedMult);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollectedMult(metamodeltest.Number newCollectedMult) {
		if (newCollectedMult != collectedMult) {
			NotificationChain msgs = null;
			if (collectedMult != null)
				msgs = ((InternalEObject)collectedMult).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_COLLECTS__COLLECTED_MULT, null, msgs);
			if (newCollectedMult != null)
				msgs = ((InternalEObject)newCollectedMult).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_COLLECTS__COLLECTED_MULT, null, msgs);
			msgs = basicSetCollectedMult(newCollectedMult, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_COLLECTS__COLLECTED_MULT, newCollectedMult, newCollectedMult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_COLLECTS__COLLECTED_MULT:
				return basicSetCollectedMult(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_COLLECTS__COLLECTED_MULT:
				return getCollectedMult();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_COLLECTS__COLLECTED_MULT:
				setCollectedMult((metamodeltest.Number)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_COLLECTS__COLLECTED_MULT:
				setCollectedMult((metamodeltest.Number)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_COLLECTS__COLLECTED_MULT:
				return collectedMult != null;
		}
		return super.eIsSet(featureID);
	}

} //ClassCollectsImpl
