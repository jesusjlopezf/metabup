package metabup.ui.editor.history.rename;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import metabup.metamodel.Reference;

public class RenameHistory {
	private List<RenamedReference> rrefs = new ArrayList<RenamedReference>();
	
	private class RenamedReference{
		Deque<String> nameStack = new ArrayDeque<String>();
		private String refId = null;
		
		public RenamedReference(Reference ref) {
			this.refId = ref.getId();
			nameStack.push(ref.getName());
		}

		public String getReferenceId(){
			return this.refId;
		}
		
		public void saveOldName(String oldName){
			if(this.nameStack.isEmpty() || !this.getUpdatedName().equals(oldName) )this.nameStack.push(oldName);			
		}
		
		public String forgetLastName(){
			return this.nameStack.pop();
		}
		
		public String getUpdatedName(){
			return this.nameStack.getFirst();
		}
	}
	
	public String getUpdatedReferenceName(Reference ref){
		for(RenamedReference rr : rrefs)
			if(rr.getReferenceId().equals(ref.getId()))
				return rr.getUpdatedName();
		
		return null;
	}
	
	public void addRenamedReference(Reference ref, String oldName){
		for(RenamedReference rr : rrefs){
			if(rr.getReferenceId().equals(ref.getId())){
				rr.saveOldName(oldName);
				return;
			}
		}
		
		RenamedReference rr = new RenamedReference(ref);
		rr.saveOldName(oldName);
		rrefs.add(rr);
	}
	
	public void undoLastRename(Reference ref){
		for(RenamedReference rr : rrefs){
			if(rr.getReferenceId().equals(ref.getId())){
				rr.forgetLastName();
				return;
			}
		}
	}
}
