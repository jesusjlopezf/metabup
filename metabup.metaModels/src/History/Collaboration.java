/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package History;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Collaboration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link History.Collaboration#getRationale <em>Rationale</em>}</li>
 *   <li>{@link History.Collaboration#getProposedBy <em>Proposed By</em>}</li>
 *   <li>{@link History.Collaboration#getComments <em>Comments</em>}</li>
 *   <li>{@link History.Collaboration#getVotes <em>Votes</em>}</li>
 * </ul>
 * </p>
 *
 * @see History.HistoryPackage#getCollaboration()
 * @model
 * @generated
 */
public interface Collaboration extends IdElement {
	/**
	 * Returns the value of the '<em><b>Rationale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rationale</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rationale</em>' attribute.
	 * @see #setRationale(String)
	 * @see History.HistoryPackage#getCollaboration_Rationale()
	 * @model
	 * @generated
	 */
	String getRationale();

	/**
	 * Sets the value of the '{@link History.Collaboration#getRationale <em>Rationale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rationale</em>' attribute.
	 * @see #getRationale()
	 * @generated
	 */
	void setRationale(String value);

	/**
	 * Returns the value of the '<em><b>Proposed By</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link History.User#getCollaborations <em>Collaborations</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Proposed By</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Proposed By</em>' reference.
	 * @see #setProposedBy(User)
	 * @see History.HistoryPackage#getCollaboration_ProposedBy()
	 * @see History.User#getCollaborations
	 * @model opposite="collaborations"
	 * @generated
	 */
	User getProposedBy();

	/**
	 * Sets the value of the '{@link History.Collaboration#getProposedBy <em>Proposed By</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Proposed By</em>' reference.
	 * @see #getProposedBy()
	 * @generated
	 */
	void setProposedBy(User value);

	/**
	 * Returns the value of the '<em><b>Comments</b></em>' containment reference list.
	 * The list contents are of type {@link History.Comment}.
	 * It is bidirectional and its opposite is '{@link History.Comment#getCommentedElement <em>Commented Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comments</em>' containment reference list.
	 * @see History.HistoryPackage#getCollaboration_Comments()
	 * @see History.Comment#getCommentedElement
	 * @model opposite="commentedElement" containment="true"
	 * @generated
	 */
	EList<Comment> getComments();

	/**
	 * Returns the value of the '<em><b>Votes</b></em>' containment reference list.
	 * The list contents are of type {@link History.Vote}.
	 * It is bidirectional and its opposite is '{@link History.Vote#getCollaboration <em>Collaboration</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Votes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Votes</em>' containment reference list.
	 * @see History.HistoryPackage#getCollaboration_Votes()
	 * @see History.Vote#getCollaboration
	 * @model opposite="collaboration" containment="true"
	 * @generated
	 */
	EList<Vote> getVotes();

} // Collaboration
