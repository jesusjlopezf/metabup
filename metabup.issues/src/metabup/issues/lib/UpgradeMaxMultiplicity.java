/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.issues.lib;

import metabup.issues.general.categories.Automatic;
import metabup.metamodel.Feature;

public class UpgradeMaxMultiplicity extends Automatic {
	public UpgradeMaxMultiplicity(Feature f){
		if(f != null){
			super.elements.add(f);
			super.setDescription("Fixing feature " + f.getName() + " max multiplicity suggested");
		}	
	}
	
	@Override
	public void run(){
		Feature f = (Feature)elements.get(0);
		f.setMax(-1);
	}
}
