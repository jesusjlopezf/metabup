/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.lib;

import java.util.ArrayList;
import java.util.List;

import metabup.annotations.catalog.refactoring.InLine;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.general.AssistanceTip;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;

public class ReplaceClassByIntAssistance extends AssistanceTip implements IAssistanceTip {
	public ReplaceClassByIntAssistance(){
		super();
		super.setName("Turn reference into IntType attribute");
		this.setAnnotation(new InLine());
	}
	
	public boolean calculateSolutions() {		
		////System.out.println("hey there");
		for(MetaClass mc : this.getTargetMetaModel().getClasses()){
			////System.out.println("I'm checking class " + mc.getName());
			for(Feature f : mc.getFeatures()){				
				if(f instanceof Reference){
					////System.out.println("I'm checking reference " + f.getName());
					Reference r = (Reference)f;
					
					if(r.getReference() != null){						
						if((r.getReference().getFeatures() != null && r.getReference().getFeatures().isEmpty()) 
								&& r.getReference().getSubclasses().isEmpty()
								&& r.getReference().getSupers().isEmpty()
								&& !super.isTargetforAnyReference(r.getReference(), mc)){
									List<AnnotatedElement> set = new ArrayList<AnnotatedElement>();
									set.add(r);
									super.putSolution("The reference \'" + r.getName() + "\' might be in-lined as an IntType attribute within \'" + mc.getName() + "\'" , set, new InLine());
						}
					}				
				}
			}
		}
		
		if(super.solutions.isEmpty()) return false;
		else return true;
	}


	
}
