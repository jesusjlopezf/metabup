/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.lexicon.de.colibri.lib;

/**
 * Thrown to indicate that the set can not be modified.
 * @author Daniel N. Goetzmann
 * @version 1.0
 */
public class UnmodifiableSetException extends UnsupportedOperationException {
	
	private static final long serialVersionUID = -7977653209139916994L;
	
	
	/**
	 * Constructs an <code>UnmodifiableSetException</code> without detail message.
	 *
	 */
	public UnmodifiableSetException() {
		super();
	}
	
	
	/**
	 * Constructs an <code>UnmodifiableSetException</code> with the specified
	 * detail message.
	 * @param message the detail message.
	 */
	public UnmodifiableSetException(String message) {
		super(message);
	}
}
