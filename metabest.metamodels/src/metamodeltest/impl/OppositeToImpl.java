/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.MetamodeltestPackage;
import metamodeltest.OppositeTo;
import metamodeltest.ReferenceSelector;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Opposite To</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.OppositeToImpl#getRefSel <em>Ref Sel</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OppositeToImpl extends ReferenceQualifierImpl implements OppositeTo {
	/**
	 * The cached value of the '{@link #getRefSel() <em>Ref Sel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefSel()
	 * @generated
	 * @ordered
	 */
	protected ReferenceSelector refSel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OppositeToImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.OPPOSITE_TO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceSelector getRefSel() {
		return refSel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRefSel(ReferenceSelector newRefSel, NotificationChain msgs) {
		ReferenceSelector oldRefSel = refSel;
		refSel = newRefSel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.OPPOSITE_TO__REF_SEL, oldRefSel, newRefSel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefSel(ReferenceSelector newRefSel) {
		if (newRefSel != refSel) {
			NotificationChain msgs = null;
			if (refSel != null)
				msgs = ((InternalEObject)refSel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.OPPOSITE_TO__REF_SEL, null, msgs);
			if (newRefSel != null)
				msgs = ((InternalEObject)newRefSel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.OPPOSITE_TO__REF_SEL, null, msgs);
			msgs = basicSetRefSel(newRefSel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.OPPOSITE_TO__REF_SEL, newRefSel, newRefSel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.OPPOSITE_TO__REF_SEL:
				return basicSetRefSel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.OPPOSITE_TO__REF_SEL:
				return getRefSel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.OPPOSITE_TO__REF_SEL:
				setRefSel((ReferenceSelector)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.OPPOSITE_TO__REF_SEL:
				setRefSel((ReferenceSelector)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.OPPOSITE_TO__REF_SEL:
				return refSel != null;
		}
		return super.eIsSet(featureID);
	}

} //OppositeToImpl
