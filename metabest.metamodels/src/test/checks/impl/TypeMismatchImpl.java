/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import test.checks.ChecksPackage;
import test.checks.TypeMismatch;

import test.checks.expressions.FeatureTypeExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Mismatch</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link test.checks.impl.TypeMismatchImpl#getFeatureType <em>Feature Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TypeMismatchImpl extends MismatchImpl implements TypeMismatch {
	/**
	 * The cached value of the '{@link #getFeatureType() <em>Feature Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureType()
	 * @generated
	 * @ordered
	 */
	protected FeatureTypeExpression featureType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeMismatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChecksPackage.Literals.TYPE_MISMATCH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureTypeExpression getFeatureType() {
		return featureType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureType(FeatureTypeExpression newFeatureType, NotificationChain msgs) {
		FeatureTypeExpression oldFeatureType = featureType;
		featureType = newFeatureType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ChecksPackage.TYPE_MISMATCH__FEATURE_TYPE, oldFeatureType, newFeatureType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureType(FeatureTypeExpression newFeatureType) {
		if (newFeatureType != featureType) {
			NotificationChain msgs = null;
			if (featureType != null)
				msgs = ((InternalEObject)featureType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.TYPE_MISMATCH__FEATURE_TYPE, null, msgs);
			if (newFeatureType != null)
				msgs = ((InternalEObject)newFeatureType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.TYPE_MISMATCH__FEATURE_TYPE, null, msgs);
			msgs = basicSetFeatureType(newFeatureType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChecksPackage.TYPE_MISMATCH__FEATURE_TYPE, newFeatureType, newFeatureType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ChecksPackage.TYPE_MISMATCH__FEATURE_TYPE:
				return basicSetFeatureType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChecksPackage.TYPE_MISMATCH__FEATURE_TYPE:
				return getFeatureType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChecksPackage.TYPE_MISMATCH__FEATURE_TYPE:
				setFeatureType((FeatureTypeExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChecksPackage.TYPE_MISMATCH__FEATURE_TYPE:
				setFeatureType((FeatureTypeExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChecksPackage.TYPE_MISMATCH__FEATURE_TYPE:
				return featureType != null;
		}
		return super.eIsSet(featureID);
	}

} //TypeMismatchImpl
