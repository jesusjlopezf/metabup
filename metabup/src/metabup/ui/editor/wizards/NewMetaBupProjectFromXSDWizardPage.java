/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.wizards;

import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one (mbup).
 */

public class NewMetaBupProjectFromXSDWizardPage extends WizardPage {
	private ISelection selection;
	private Text xsdFileText;

	
	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param pageName
	 */
	public NewMetaBupProjectFromXSDWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("Select an XSD file");
		setDescription("Please select an XSD file from which the new metaBup meta-model should be created.");
		this.selection = selection;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		final Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		
		/*Label label = new Label(container, SWT.NULL);
		label.setText("&Fragment file:");

		fileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		fileText.setLayoutData(gd);
		fileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		
		Button button = new Button(container, SWT.PUSH);
		button.setText("Browse...");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				try {
					handleBrowse();
				} catch (CoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		Group g = new Group(container, 0);
	    //g.setSize(400, 100);
	    g.setText("Generate:");
	    
	    b1 = new Button(g, SWT.CHECK);
	    b2 = new Button(g, SWT.CHECK);
	    
	    b1.setBounds(10, 20, 300, 25);	    
	    b1.setText("MBF fragment");
	    b1.setSelection(generateFragment); // default selection

	    b2.setBounds(10, 45, 300, 25);
	    b2.setText("GraphML fragment representation");	 
	    b2.setSelection(generateGraphML); // default selection
		
	    label = new Label(container, SWT.NULL);
		label.setText("");
		label = new Label(container, SWT.NULL);
		label.setText("");
		
		b1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){				
				generateFragment = getB1Selection(); 
			}
		});
		
		b2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){				
				generateGraphML = getB2Selection(); 
			}
		});*/
		
		Label label = new Label(container, SWT.NULL);
		label.setText("XSD file:");
	    
	    xsdFileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		xsdFileText.setLayoutData(gd);		
		xsdFileText.setEnabled(true);
				
		final Button button4 = new Button(container, SWT.PUSH);
		button4.setText("Browse...");
		button4.setEnabled(true);
		button4.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				handleFragmentLegendBrowse(container);
			}
		});
		
		//initialize();
		dialogChanged();
		setControl(container);
	}
	
/*	private boolean getB1Selection(){
		return b1.getSelection();
	}
	
	private boolean getB2Selection(){
		return b2.getSelection();
	}*/
	
	protected void handleFragmentLegendBrowse(Composite container) {
		FileDialog dialog = new FileDialog(container.getShell());
		dialog.setText("Select a metabup.sketches.legend file...");
		dialog.setFilterExtensions(new String[] { "*.xsd" } );
		String fileName = dialog.open();
		
		if(fileName != null){
			Path path = new Path(fileName);
			xsdFileText.setText(path.toOSString());
		}			
	}
	
	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */

	/*private void initialize() {
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			
			if (obj instanceof IFile)
				if(((IFile)obj).getFullPath().toOSString().endsWith(".mbf"))
					fileText.setText(((IFile)obj).getFullPath().toOSString());
		}
		fileText.setText("");
	}*/

	/**
	 * Uses the standard container selection dialog to choose the new value for
	 * the container field.
	 * @throws CoreException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */

	/*private void handleBrowse() throws CoreException, FileNotFoundException, IOException {
		
		FileDialog dialog = new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		fileText.setText(dialog.open());
		
		File file = new File(fileText.getText());
		IWorkspace workspace= ResourcesPlugin.getWorkspace();    
		IPath location= Path.fromOSString(file.getAbsolutePath()); 
		IFile ifile= workspace.getRoot().getFileForLocation(location);
		
		final Injector injector = FragmentsActivator.getInstance().getInjector(FragmentsActivator.METABUP_FRAGMENTS);
		
		XtextResourceSet resourceSet = (XtextResourceSet) injector.getInstance(XtextResourceSetProvider.class).get(ifile.getProject());
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		Resource resource = resourceSet.getResource(URI.createURI(ifile.getLocationURI().toString()),true);
	    	    
		TestModel tm = (TestModel)resource.getContents().get(0);
		baseFragment = tm.getTestableFragments().get(0); 
	}*/

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {			
		String fileName = getXSDFile();
		
		if (fileName.length() == 0) {
			//updateStatus("File name must be specified");
			return;
		}
		
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}
	
	public String getXSDFile(){
		return xsdFileText.getText();
	}
}