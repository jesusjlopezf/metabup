/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks;

import test.checks.expressions.FragmentElementExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Restriction Violation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.RestrictionViolation#getFragmentElement <em>Fragment Element</em>}</li>
 *   <li>{@link test.checks.RestrictionViolation#getRestrictionName <em>Restriction Name</em>}</li>
 * </ul>
 *
 * @see test.checks.ChecksPackage#getRestrictionViolation()
 * @model
 * @generated
 */
public interface RestrictionViolation extends Check {
	/**
	 * Returns the value of the '<em><b>Fragment Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fragment Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fragment Element</em>' containment reference.
	 * @see #setFragmentElement(FragmentElementExpression)
	 * @see test.checks.ChecksPackage#getRestrictionViolation_FragmentElement()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FragmentElementExpression getFragmentElement();

	/**
	 * Sets the value of the '{@link test.checks.RestrictionViolation#getFragmentElement <em>Fragment Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fragment Element</em>' containment reference.
	 * @see #getFragmentElement()
	 * @generated
	 */
	void setFragmentElement(FragmentElementExpression value);

	/**
	 * Returns the value of the '<em><b>Restriction Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Restriction Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Restriction Name</em>' attribute.
	 * @see #setRestrictionName(String)
	 * @see test.checks.ChecksPackage#getRestrictionViolation_RestrictionName()
	 * @model required="true"
	 * @generated
	 */
	String getRestrictionName();

	/**
	 * Sets the value of the '{@link test.checks.RestrictionViolation#getRestrictionName <em>Restriction Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Restriction Name</em>' attribute.
	 * @see #getRestrictionName()
	 * @generated
	 */
	void setRestrictionName(String value);

} // RestrictionViolation
