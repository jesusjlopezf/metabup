/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;



/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Is Super To</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.IsSuperTo#getClassSels <em>Class Sels</em>}</li>
 *   <li>{@link metamodeltest.IsSuperTo#getOrEqual <em>Or Equal</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getIsSuperTo()
 * @model
 * @generated
 */
public interface IsSuperTo extends Inheritance {

	/**
	 * Returns the value of the '<em><b>Class Sels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Sels</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Sels</em>' containment reference.
	 * @see #setClassSels(ClassSelector)
	 * @see metamodeltest.MetamodeltestPackage#getIsSuperTo_ClassSels()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ClassSelector getClassSels();

	/**
	 * Sets the value of the '{@link metamodeltest.IsSuperTo#getClassSels <em>Class Sels</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Sels</em>' containment reference.
	 * @see #getClassSels()
	 * @generated
	 */
	void setClassSels(ClassSelector value);

	/**
	 * Returns the value of the '<em><b>Or Equal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Equal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Equal</em>' containment reference.
	 * @see #setOrEqual(BooleanParameter)
	 * @see metamodeltest.MetamodeltestPackage#getIsSuperTo_OrEqual()
	 * @model containment="true"
	 * @generated
	 */
	BooleanParameter getOrEqual();

	/**
	 * Sets the value of the '{@link metamodeltest.IsSuperTo#getOrEqual <em>Or Equal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Equal</em>' containment reference.
	 * @see #getOrEqual()
	 * @generated
	 */
	void setOrEqual(BooleanParameter value);
} // IsSuperTo
