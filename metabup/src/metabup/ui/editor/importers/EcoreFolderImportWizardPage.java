/*******************************************************************************
 * Copyright (c) 2006, 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.importers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;


public class EcoreFolderImportWizardPage extends WizardPage {
	private Text sourceFolderText;
	private Text targetProjectText;

	private ISelection selection;

	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param pageName
	 */
	public EcoreFolderImportWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("Multi-page Editor File");
		setDescription("This wizard creates a new file with *.mbest extension that can be opened by a multi-page editor.");
		this.selection = selection;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;
		Label label = new Label(container, SWT.NULL);
		label.setText("&Source folder:");

		sourceFolderText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		sourceFolderText.setLayoutData(gd);
		sourceFolderText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		Button button = new Button(container, SWT.PUSH);
		button.setText("Browse...");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowse();
			}
		});
		
		Label label2 = new Label(container, SWT.NULL);
		label2.setText("&Target project:");

		targetProjectText = new Text(container, SWT.BORDER | SWT.SINGLE);
		targetProjectText.setLayoutData(gd);
		targetProjectText.setEnabled(false);
		
		initialize();
		dialogChanged();
		setControl(container);
	}

	public List<IFile> getEcoreFilesFromFolder() throws CoreException{
		IResource folderResource = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(getContainerName()));
		
		if(folderResource.getType() == IResource.FOLDER){
			IContainer container = (IContainer)folderResource;
			IResource folderMembers[] = container.members();
			List<IFile> queries = new ArrayList<IFile>();						
			
			for(int i=0; i<folderMembers.length; i++){
				if((folderMembers[i].getFileExtension() != null) && (folderMembers[i].getFileExtension().equals("ecore"))){
					queries.add((IFile)folderMembers[i]);				
				}/*else{
					if(folderMembers[i].getFileExtension() == null){
						List<IFile> moreQueries = getMetamodelsFromFolder((IContainer)folderMembers[i]);
						
						if(!moreQueries.isEmpty()){
							queries.addAll(moreQueries);
						}
					}
				}*/
			}
			
			return queries;
		}
		
		return null;
	}		

	private void initialize() {
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			if (obj instanceof IResource) {
				IContainer container;
				if (obj instanceof IContainer)
					container = (IContainer) obj;
				else
					container = ((IResource) obj).getParent();
				sourceFolderText.setText("");
				targetProjectText.setText(container.getFullPath().toString());
			}
		}
	}

	/**
	 * Uses the standard container selection dialog to choose the new value for
	 * the container field.
	 */

	private void handleBrowse() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				getShell(), ResourcesPlugin.getWorkspace().getRoot(), false,
				"Select new file container");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				sourceFolderText.setText(((Path) result[0]).toString());
			}
		}
	}

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {
		IResource container = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(getContainerName()));

		if (getContainerName().length() == 0) {
			updateStatus("File container must be specified");
			return;
		}
		if (container == null
				|| (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
			updateStatus("File container must exist");
			return;
		}
		if (!container.isAccessible()) {
			updateStatus("Project must be writable");
			return;
		}
		
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	public String getContainerName() {
		return sourceFolderText.getText();
	}
	
	public IProject getTargetProject(){
		IResource projectResource = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(targetProjectText.getText()));
		
		if(projectResource.getType() == IResource.PROJECT){
			// COMPROBAR QUE LA NATURALEZA DEL PROYECTO ES LA ADECUADA
			return (IProject)projectResource;
		}
		
		return null;
	}
}