/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Selector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.AttributeSelector#getAttributeFilter <em>Attribute Filter</em>}</li>
 *   <li>{@link metamodeltest.AttributeSelector#getAttributeConditioner <em>Attribute Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getAttributeSelector()
 * @model
 * @generated
 */
public interface AttributeSelector extends Selector {
	/**
	 * Returns the value of the '<em><b>Attribute Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Filter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Filter</em>' containment reference.
	 * @see #setAttributeFilter(AttributeFilter)
	 * @see metamodeltest.MetamodeltestPackage#getAttributeSelector_AttributeFilter()
	 * @model containment="true"
	 * @generated
	 */
	AttributeFilter getAttributeFilter();

	/**
	 * Sets the value of the '{@link metamodeltest.AttributeSelector#getAttributeFilter <em>Attribute Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Filter</em>' containment reference.
	 * @see #getAttributeFilter()
	 * @generated
	 */
	void setAttributeFilter(AttributeFilter value);

	/**
	 * Returns the value of the '<em><b>Attribute Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Conditioner</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Conditioner</em>' containment reference.
	 * @see #setAttributeConditioner(AttributeFilter)
	 * @see metamodeltest.MetamodeltestPackage#getAttributeSelector_AttributeConditioner()
	 * @model containment="true"
	 * @generated
	 */
	AttributeFilter getAttributeConditioner();

	/**
	 * Sets the value of the '{@link metamodeltest.AttributeSelector#getAttributeConditioner <em>Attribute Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Conditioner</em>' containment reference.
	 * @see #getAttributeConditioner()
	 * @generated
	 */
	void setAttributeConditioner(AttributeFilter value);

} // AttributeSelector
