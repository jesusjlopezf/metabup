/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.jesusjlopezf.utils.resources;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtils {
	public static boolean checkExtension(File file, String ext){
		String path = file.getAbsolutePath();
		String extension = path.substring(path.lastIndexOf('.')+1, path.length());			
		//System.out.println("comparing: " + extension + " with: " + ext);
		return extension.equals(ext);
	}

	public static File copyFile(File source, File dest) throws IOException {
	    InputStream input = null;
	    OutputStream output = null;
	
	    try {
	        input = new FileInputStream(source);
	        output = new FileOutputStream(dest);
	
	        byte[] buf = new byte[1024];
	        int bytesRead;
	        while ((bytesRead = input.read(buf)) > 0) output.write(buf, 0, bytesRead);
	        
	    } finally {
	        input.close();
	        output.close();
	    }
	    
	    if(dest.exists()) return dest;
	    else return null;
	}
	
	public static String readFile(String fileName) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}
	
	public static void writeFile(File file, String content){
		try {
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(content);
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}		
}
