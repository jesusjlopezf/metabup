/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.actions;

import java.io.IOException;

import metabup.SessionState;
import metabup.extensionpoints.MetaBupMetaModelExportActionContribution;
import metabup.ui.editor.popup.actions.MetaBupEditorAction;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;

import metabest.solver.ui.wizards.GenerateValidExampleWizard;
import metabest.solver.ui.wizards.GenerateExampleSetWizard;

public class GenerateExampleSetAction extends MetaBupMetaModelExportActionContribution {
	public void run() {
		SessionState session = editor.getSession();		
		if(session.getMetamodelRoot() == null) return;
		
		if(session.getMmFile() == null) return;
		IProject project = session.getMmFile().getProject();				 
		
		WizardDialog dialog = new WizardDialog(null, new GenerateExampleSetWizard(project, session.getMetamodelRoot()));
		dialog.open(); 		
	}

}
