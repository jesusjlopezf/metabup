/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.preferences;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.preference.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.IWorkbench;

import metabup.annotations.Activator;
import metabup.annotations.IAnnotation;
import metabup.annotations.general.AnnotationFactory;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class AnnotationsPreferencePage
	extends FieldEditorPreferencePage
	implements IWorkbenchPreferencePage {

	public AnnotationsPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Annotation catalog preferences");		
	}
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {		
		AnnotationFactory af = new AnnotationFactory();
		af.initializeFactory();
		createSpace();
		
		for(String ann : af.listAllAnnotationsNames()){
			Class<? extends IAnnotation> annotationClass = af.getAnnotation(ann);
			try {
				IAnnotation annotation = annotationClass.newInstance();
				HashMap<String, Object> preferences = annotation.getPreferences();
				
				if(preferences != null && !preferences.isEmpty()){
					Label label= new Label(getFieldEditorParent(), SWT.BOLD);
					GridData gd= new GridData(GridData.HORIZONTAL_ALIGN_FILL);
					gd.horizontalSpan= 2;
					label.setLayoutData(gd);
					label.setText("@" + ann);
					Iterator it = preferences.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry e = (Map.Entry)it.next();
						if(e.getValue() instanceof String ) 
							addField(new StringFieldEditor((String) e.getKey(), (String) e.getKey(), getFieldEditorParent()));
						else if(e.getValue() instanceof Boolean )
							addField(new BooleanFieldEditor((String) e.getKey(), (String) e.getKey(), getFieldEditorParent()));
						/* radio group (multi-option) preferences should be furtherly approached */
					}
												
					createSpace();
				}											
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}				
		}
		
		/*addField(new DirectoryFieldEditor(PreferenceConstants.P_PATH, 
				"&Directory preference:", getFieldEditorParent()));*/
		/*addField(
			new BooleanFieldEditor(
				PreferenceConstants.P_BOOLEAN,
				"&An example of a boolean preference",
				getFieldEditorParent()));*/

		/*addField(new RadioGroupFieldEditor(
				PreferenceConstants.P_CHOICE,
			"An example of a multiple-choice preference",
			1,
			new String[][] { { "&Choice 1", "choice1" }, {
				"C&hoice 2", "choice2" }
		}, getFieldEditorParent()));*/
		/*addField(
			new StringFieldEditor(PreferenceConstants.P_STRING, "A &text preference:", getFieldEditorParent()));*/
	}
	
	private void createSpace() {
		Label label= new Label(getFieldEditorParent(), SWT.NONE);
		GridData gd= new GridData(GridData.HORIZONTAL_ALIGN_FILL);
		gd.horizontalSpan= 3;
		label.setLayoutData(gd);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}