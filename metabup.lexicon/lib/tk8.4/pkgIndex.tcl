#*******************************************************************************
# Copyright (c) 2015 IBM Corporation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
if {[package vcompare [package provide Tcl] 8.4] != 0} { return }
package ifneeded Tk 8.4 [list load [file join $dir .. .. bin tk84.dll] Tk]
