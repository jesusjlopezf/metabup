/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.dnd;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;

import metabup.metamodel.MetaModel;

public class DragMetaModelElementViewer extends TreeViewer{
	MetaModel metamodel = null;
	IStructuredSelection selection = null;
	
	public void refresh(MetaModel metamodel) {
		if(metamodel!=null){
			this.metamodel = metamodel;
			setInput(metamodel);
			refresh();
			expandAll();
		}
	}
	
	public DragMetaModelElementViewer(Composite parent, MetaModel metamodel) {
		super(parent);
		this.metamodel = metamodel;
		int operations = DND.DROP_COPY| DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[]{TextTransfer.getInstance()};
		addDragSupport(operations, transferTypes , new MetaModelElementDragListener(this));
		setContentProvider(new TreeContentProvider());
		setLabelProvider(new TreeLabelProvider());
		setInput(metamodel);
		refresh(metamodel);
		expandAll();
	}
}
