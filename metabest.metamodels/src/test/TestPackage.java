/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import fragments.FragmentsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see test.TestFactory
 * @model kind="package"
 * @generated
 */
public interface TestPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "test";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/metabest.metamodels/model/test.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "metabest.metamodels";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestPackage eINSTANCE = test.impl.TestPackageImpl.init();

	/**
	 * The meta object id for the '{@link test.impl.TestModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.impl.TestModelImpl
	 * @see test.impl.TestPackageImpl#getTestModel()
	 * @generated
	 */
	int TEST_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_MODEL__IMPORT_URI = 0;

	/**
	 * The feature id for the '<em><b>Testable Fragments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_MODEL__TESTABLE_FRAGMENTS = 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.impl.TestableFragmentImpl <em>Testable Fragment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.impl.TestableFragmentImpl
	 * @see test.impl.TestPackageImpl#getTestableFragment()
	 * @generated
	 */
	int TESTABLE_FRAGMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TESTABLE_FRAGMENT__NAME = FragmentsPackage.FRAGMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TESTABLE_FRAGMENT__TYPE = FragmentsPackage.FRAGMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TESTABLE_FRAGMENT__ANNOTATION = FragmentsPackage.FRAGMENT__ANNOTATION;

	/**
	 * The feature id for the '<em><b>Ftype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TESTABLE_FRAGMENT__FTYPE = FragmentsPackage.FRAGMENT__FTYPE;

	/**
	 * The feature id for the '<em><b>Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TESTABLE_FRAGMENT__OBJECTS = FragmentsPackage.FRAGMENT__OBJECTS;

	/**
	 * The feature id for the '<em><b>Assertion Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TESTABLE_FRAGMENT__ASSERTION_SET = FragmentsPackage.FRAGMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Completion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TESTABLE_FRAGMENT__COMPLETION = FragmentsPackage.FRAGMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Testable Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TESTABLE_FRAGMENT_FEATURE_COUNT = FragmentsPackage.FRAGMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Testable Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TESTABLE_FRAGMENT_OPERATION_COUNT = FragmentsPackage.FRAGMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.impl.ComplementaryBlockImpl <em>Complementary Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.impl.ComplementaryBlockImpl
	 * @see test.impl.TestPackageImpl#getComplementaryBlock()
	 * @generated
	 */
	int COMPLEMENTARY_BLOCK = 6;

	/**
	 * The feature id for the '<em><b>At Least</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEMENTARY_BLOCK__AT_LEAST = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEMENTARY_BLOCK__DESCRIPTION = 1;

	/**
	 * The number of structural features of the '<em>Complementary Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEMENTARY_BLOCK_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Complementary Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEMENTARY_BLOCK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.impl.AssertionSetImpl <em>Assertion Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.impl.AssertionSetImpl
	 * @see test.impl.TestPackageImpl#getAssertionSet()
	 * @generated
	 */
	int ASSERTION_SET = 2;

	/**
	 * The feature id for the '<em><b>At Least</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_SET__AT_LEAST = COMPLEMENTARY_BLOCK__AT_LEAST;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_SET__DESCRIPTION = COMPLEMENTARY_BLOCK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Assertions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_SET__ASSERTIONS = COMPLEMENTARY_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Assertion Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_SET_FEATURE_COUNT = COMPLEMENTARY_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Assertion Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_SET_OPERATION_COUNT = COMPLEMENTARY_BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.impl.AssertionImpl <em>Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.impl.AssertionImpl
	 * @see test.impl.TestPackageImpl#getAssertion()
	 * @generated
	 */
	int ASSERTION = 3;

	/**
	 * The feature id for the '<em><b>Check</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__CHECK = 0;

	/**
	 * The number of structural features of the '<em>Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.impl.ExtensionAssertionSetImpl <em>Extension Assertion Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.impl.ExtensionAssertionSetImpl
	 * @see test.impl.TestPackageImpl#getExtensionAssertionSet()
	 * @generated
	 */
	int EXTENSION_ASSERTION_SET = 4;

	/**
	 * The feature id for the '<em><b>At Least</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_ASSERTION_SET__AT_LEAST = COMPLEMENTARY_BLOCK__AT_LEAST;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_ASSERTION_SET__DESCRIPTION = COMPLEMENTARY_BLOCK__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Assertions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_ASSERTION_SET__ASSERTIONS = COMPLEMENTARY_BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Extension Assertion Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_ASSERTION_SET_FEATURE_COUNT = COMPLEMENTARY_BLOCK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Extension Assertion Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_ASSERTION_SET_OPERATION_COUNT = COMPLEMENTARY_BLOCK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.impl.ExtensionAssertionImpl <em>Extension Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.impl.ExtensionAssertionImpl
	 * @see test.impl.TestPackageImpl#getExtensionAssertion()
	 * @generated
	 */
	int EXTENSION_ASSERTION = 7;

	/**
	 * The number of structural features of the '<em>Extension Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_ASSERTION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Extension Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_ASSERTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.impl.MetaClassExtensionAssertionImpl <em>Meta Class Extension Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.impl.MetaClassExtensionAssertionImpl
	 * @see test.impl.TestPackageImpl#getMetaClassExtensionAssertion()
	 * @generated
	 */
	int META_CLASS_EXTENSION_ASSERTION = 5;

	/**
	 * The feature id for the '<em><b>Selector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_EXTENSION_ASSERTION__SELECTOR = EXTENSION_ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_EXTENSION_ASSERTION__CONDITION = EXTENSION_ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Meta Class Extension Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_EXTENSION_ASSERTION_FEATURE_COUNT = EXTENSION_ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Meta Class Extension Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_EXTENSION_ASSERTION_OPERATION_COUNT = EXTENSION_ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.impl.FragmentObjectExtensionAssertionImpl <em>Fragment Object Extension Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.impl.FragmentObjectExtensionAssertionImpl
	 * @see test.impl.TestPackageImpl#getFragmentObjectExtensionAssertion()
	 * @generated
	 */
	int FRAGMENT_OBJECT_EXTENSION_ASSERTION = 8;

	/**
	 * The feature id for the '<em><b>Selector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR = EXTENSION_ASSERTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION = EXTENSION_ASSERTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Fragment Object Extension Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_OBJECT_EXTENSION_ASSERTION_FEATURE_COUNT = EXTENSION_ASSERTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Fragment Object Extension Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_OBJECT_EXTENSION_ASSERTION_OPERATION_COUNT = EXTENSION_ASSERTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.CompletionType <em>Completion Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.CompletionType
	 * @see test.impl.TestPackageImpl#getCompletionType()
	 * @generated
	 */
	int COMPLETION_TYPE = 9;


	/**
	 * Returns the meta object for class '{@link test.TestModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see test.TestModel
	 * @generated
	 */
	EClass getTestModel();

	/**
	 * Returns the meta object for the attribute '{@link test.TestModel#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see test.TestModel#getImportURI()
	 * @see #getTestModel()
	 * @generated
	 */
	EAttribute getTestModel_ImportURI();

	/**
	 * Returns the meta object for the containment reference list '{@link test.TestModel#getTestableFragments <em>Testable Fragments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Testable Fragments</em>'.
	 * @see test.TestModel#getTestableFragments()
	 * @see #getTestModel()
	 * @generated
	 */
	EReference getTestModel_TestableFragments();

	/**
	 * Returns the meta object for class '{@link test.TestableFragment <em>Testable Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Testable Fragment</em>'.
	 * @see test.TestableFragment
	 * @generated
	 */
	EClass getTestableFragment();

	/**
	 * Returns the meta object for the containment reference '{@link test.TestableFragment#getAssertionSet <em>Assertion Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assertion Set</em>'.
	 * @see test.TestableFragment#getAssertionSet()
	 * @see #getTestableFragment()
	 * @generated
	 */
	EReference getTestableFragment_AssertionSet();

	/**
	 * Returns the meta object for the attribute '{@link test.TestableFragment#getCompletion <em>Completion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Completion</em>'.
	 * @see test.TestableFragment#getCompletion()
	 * @see #getTestableFragment()
	 * @generated
	 */
	EAttribute getTestableFragment_Completion();

	/**
	 * Returns the meta object for class '{@link test.AssertionSet <em>Assertion Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion Set</em>'.
	 * @see test.AssertionSet
	 * @generated
	 */
	EClass getAssertionSet();

	/**
	 * Returns the meta object for the containment reference list '{@link test.AssertionSet#getAssertions <em>Assertions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Assertions</em>'.
	 * @see test.AssertionSet#getAssertions()
	 * @see #getAssertionSet()
	 * @generated
	 */
	EReference getAssertionSet_Assertions();

	/**
	 * Returns the meta object for class '{@link test.Assertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion</em>'.
	 * @see test.Assertion
	 * @generated
	 */
	EClass getAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link test.Assertion#getCheck <em>Check</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Check</em>'.
	 * @see test.Assertion#getCheck()
	 * @see #getAssertion()
	 * @generated
	 */
	EReference getAssertion_Check();

	/**
	 * Returns the meta object for class '{@link test.ExtensionAssertionSet <em>Extension Assertion Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extension Assertion Set</em>'.
	 * @see test.ExtensionAssertionSet
	 * @generated
	 */
	EClass getExtensionAssertionSet();

	/**
	 * Returns the meta object for the containment reference list '{@link test.ExtensionAssertionSet#getAssertions <em>Assertions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Assertions</em>'.
	 * @see test.ExtensionAssertionSet#getAssertions()
	 * @see #getExtensionAssertionSet()
	 * @generated
	 */
	EReference getExtensionAssertionSet_Assertions();

	/**
	 * Returns the meta object for class '{@link test.MetaClassExtensionAssertion <em>Meta Class Extension Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Class Extension Assertion</em>'.
	 * @see test.MetaClassExtensionAssertion
	 * @generated
	 */
	EClass getMetaClassExtensionAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link test.MetaClassExtensionAssertion#getSelector <em>Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Selector</em>'.
	 * @see test.MetaClassExtensionAssertion#getSelector()
	 * @see #getMetaClassExtensionAssertion()
	 * @generated
	 */
	EReference getMetaClassExtensionAssertion_Selector();

	/**
	 * Returns the meta object for the containment reference '{@link test.MetaClassExtensionAssertion#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see test.MetaClassExtensionAssertion#getCondition()
	 * @see #getMetaClassExtensionAssertion()
	 * @generated
	 */
	EReference getMetaClassExtensionAssertion_Condition();

	/**
	 * Returns the meta object for class '{@link test.ExtensionAssertion <em>Extension Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extension Assertion</em>'.
	 * @see test.ExtensionAssertion
	 * @generated
	 */
	EClass getExtensionAssertion();

	/**
	 * Returns the meta object for class '{@link test.FragmentObjectExtensionAssertion <em>Fragment Object Extension Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fragment Object Extension Assertion</em>'.
	 * @see test.FragmentObjectExtensionAssertion
	 * @generated
	 */
	EClass getFragmentObjectExtensionAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link test.FragmentObjectExtensionAssertion#getSelector <em>Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Selector</em>'.
	 * @see test.FragmentObjectExtensionAssertion#getSelector()
	 * @see #getFragmentObjectExtensionAssertion()
	 * @generated
	 */
	EReference getFragmentObjectExtensionAssertion_Selector();

	/**
	 * Returns the meta object for the containment reference '{@link test.FragmentObjectExtensionAssertion#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see test.FragmentObjectExtensionAssertion#getCondition()
	 * @see #getFragmentObjectExtensionAssertion()
	 * @generated
	 */
	EReference getFragmentObjectExtensionAssertion_Condition();

	/**
	 * Returns the meta object for class '{@link test.ComplementaryBlock <em>Complementary Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complementary Block</em>'.
	 * @see test.ComplementaryBlock
	 * @generated
	 */
	EClass getComplementaryBlock();

	/**
	 * Returns the meta object for the attribute '{@link test.ComplementaryBlock#isAtLeast <em>At Least</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>At Least</em>'.
	 * @see test.ComplementaryBlock#isAtLeast()
	 * @see #getComplementaryBlock()
	 * @generated
	 */
	EAttribute getComplementaryBlock_AtLeast();

	/**
	 * Returns the meta object for the attribute '{@link test.ComplementaryBlock#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see test.ComplementaryBlock#getDescription()
	 * @see #getComplementaryBlock()
	 * @generated
	 */
	EAttribute getComplementaryBlock_Description();

	/**
	 * Returns the meta object for enum '{@link test.CompletionType <em>Completion Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Completion Type</em>'.
	 * @see test.CompletionType
	 * @generated
	 */
	EEnum getCompletionType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TestFactory getTestFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link test.impl.TestModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.impl.TestModelImpl
		 * @see test.impl.TestPackageImpl#getTestModel()
		 * @generated
		 */
		EClass TEST_MODEL = eINSTANCE.getTestModel();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_MODEL__IMPORT_URI = eINSTANCE.getTestModel_ImportURI();

		/**
		 * The meta object literal for the '<em><b>Testable Fragments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_MODEL__TESTABLE_FRAGMENTS = eINSTANCE.getTestModel_TestableFragments();

		/**
		 * The meta object literal for the '{@link test.impl.TestableFragmentImpl <em>Testable Fragment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.impl.TestableFragmentImpl
		 * @see test.impl.TestPackageImpl#getTestableFragment()
		 * @generated
		 */
		EClass TESTABLE_FRAGMENT = eINSTANCE.getTestableFragment();

		/**
		 * The meta object literal for the '<em><b>Assertion Set</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TESTABLE_FRAGMENT__ASSERTION_SET = eINSTANCE.getTestableFragment_AssertionSet();

		/**
		 * The meta object literal for the '<em><b>Completion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TESTABLE_FRAGMENT__COMPLETION = eINSTANCE.getTestableFragment_Completion();

		/**
		 * The meta object literal for the '{@link test.impl.AssertionSetImpl <em>Assertion Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.impl.AssertionSetImpl
		 * @see test.impl.TestPackageImpl#getAssertionSet()
		 * @generated
		 */
		EClass ASSERTION_SET = eINSTANCE.getAssertionSet();

		/**
		 * The meta object literal for the '<em><b>Assertions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTION_SET__ASSERTIONS = eINSTANCE.getAssertionSet_Assertions();

		/**
		 * The meta object literal for the '{@link test.impl.AssertionImpl <em>Assertion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.impl.AssertionImpl
		 * @see test.impl.TestPackageImpl#getAssertion()
		 * @generated
		 */
		EClass ASSERTION = eINSTANCE.getAssertion();

		/**
		 * The meta object literal for the '<em><b>Check</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTION__CHECK = eINSTANCE.getAssertion_Check();

		/**
		 * The meta object literal for the '{@link test.impl.ExtensionAssertionSetImpl <em>Extension Assertion Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.impl.ExtensionAssertionSetImpl
		 * @see test.impl.TestPackageImpl#getExtensionAssertionSet()
		 * @generated
		 */
		EClass EXTENSION_ASSERTION_SET = eINSTANCE.getExtensionAssertionSet();

		/**
		 * The meta object literal for the '<em><b>Assertions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTENSION_ASSERTION_SET__ASSERTIONS = eINSTANCE.getExtensionAssertionSet_Assertions();

		/**
		 * The meta object literal for the '{@link test.impl.MetaClassExtensionAssertionImpl <em>Meta Class Extension Assertion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.impl.MetaClassExtensionAssertionImpl
		 * @see test.impl.TestPackageImpl#getMetaClassExtensionAssertion()
		 * @generated
		 */
		EClass META_CLASS_EXTENSION_ASSERTION = eINSTANCE.getMetaClassExtensionAssertion();

		/**
		 * The meta object literal for the '<em><b>Selector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_CLASS_EXTENSION_ASSERTION__SELECTOR = eINSTANCE.getMetaClassExtensionAssertion_Selector();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_CLASS_EXTENSION_ASSERTION__CONDITION = eINSTANCE.getMetaClassExtensionAssertion_Condition();

		/**
		 * The meta object literal for the '{@link test.impl.ExtensionAssertionImpl <em>Extension Assertion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.impl.ExtensionAssertionImpl
		 * @see test.impl.TestPackageImpl#getExtensionAssertion()
		 * @generated
		 */
		EClass EXTENSION_ASSERTION = eINSTANCE.getExtensionAssertion();

		/**
		 * The meta object literal for the '{@link test.impl.FragmentObjectExtensionAssertionImpl <em>Fragment Object Extension Assertion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.impl.FragmentObjectExtensionAssertionImpl
		 * @see test.impl.TestPackageImpl#getFragmentObjectExtensionAssertion()
		 * @generated
		 */
		EClass FRAGMENT_OBJECT_EXTENSION_ASSERTION = eINSTANCE.getFragmentObjectExtensionAssertion();

		/**
		 * The meta object literal for the '<em><b>Selector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR = eINSTANCE.getFragmentObjectExtensionAssertion_Selector();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION = eINSTANCE.getFragmentObjectExtensionAssertion_Condition();

		/**
		 * The meta object literal for the '{@link test.impl.ComplementaryBlockImpl <em>Complementary Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.impl.ComplementaryBlockImpl
		 * @see test.impl.TestPackageImpl#getComplementaryBlock()
		 * @generated
		 */
		EClass COMPLEMENTARY_BLOCK = eINSTANCE.getComplementaryBlock();

		/**
		 * The meta object literal for the '<em><b>At Least</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEMENTARY_BLOCK__AT_LEAST = eINSTANCE.getComplementaryBlock_AtLeast();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPLEMENTARY_BLOCK__DESCRIPTION = eINSTANCE.getComplementaryBlock_Description();

		/**
		 * The meta object literal for the '{@link test.CompletionType <em>Completion Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.CompletionType
		 * @see test.impl.TestPackageImpl#getCompletionType()
		 * @generated
		 */
		EEnum COMPLETION_TYPE = eINSTANCE.getCompletionType();

	}

} //TestPackage
