/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.editors;

import metabup.Activator;
import metabup.perspective.views.actions.UpdateMetamodelByAssistanceTip;
import metabup.ui.editor.popup.actions.CompileToEcoreAction;
import metabup.ui.editor.popup.actions.GenerateCollaboroHistory;
import metabup.ui.editor.popup.actions.MetaBupEditorAction;
import metabup.ui.editor.popup.actions.UpdateMetamodel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.action.*;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.ide.IDEActionFactory;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;
import org.eclipse.ui.part.MultiPageEditorSite;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;

import com.jesusjlopezf.utils.eclipse.PluginUtils;
import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;


/**
 * Manages the installation/deinstallation of global actions for multi-page editors.
 * Responsible for the redirection of global actions to the active editor.
 * Multi-page contributor replaces the contributors for the individual editors in the multi-page editor.
 */
public class MetamodelInteractiveEditorContributor extends MultiPageEditorActionBarContributor {
	private IEditorPart activeEditorPart;
	private UpdateMetamodel         updateMetamodelAction;
	private UpdateMetamodelByAssistanceTip         updateMetamodelByAssistanceTipAction;
	private GenerateCollaboroHistory generateCollaboroHistory;
	private CompileToEcoreAction           generateEcore;
	
	private List<MetaBupEditorAction> compilersActions = new ArrayList<MetaBupEditorAction>();
	private List<MetaBupEditorAction> importersActions = new ArrayList<MetaBupEditorAction>();
	private List<MetaBupEditorAction> metamodelEditActions = new ArrayList<MetaBupEditorAction>();
	private List<MetaBupEditorAction> toolbarActions = new ArrayList<MetaBupEditorAction>();
	
	public MetamodelInteractiveEditorContributor() {

			createContributionActions();
		
		
		createActions();
	}
	
	/**
	 * Returns the action registed with the given text editor.
	 * @return IAction or null if editor is null.
	 */
	protected IAction getAction(ITextEditor editor, String actionID) {
		return (editor == null ? null : editor.getAction(actionID));
	}
	
	/* (non-JavaDoc)
	 * Method declared in AbstractMultiPageEditorActionBarContributor.
	 */

	MetamodelInteractiveEditor pageEditor;
	public void setActivePage(IEditorPart part) {
		if (activeEditorPart == part)
			return;

		activeEditorPart = part;

		if ( pageEditor == null ) {
			MultiPageEditorSite s = ((MultiPageEditorSite) part.getSite());
			pageEditor = (MetamodelInteractiveEditor) s.getMultiPageEditor();


			updateMetamodelAction.setEditor(pageEditor);
			generateCollaboroHistory.setEditor(pageEditor);
			generateEcore.setEditor(pageEditor);
			
			for(MetaBupEditorAction action : this.metamodelEditActions) action.setEditor(pageEditor);
			for(MetaBupEditorAction action : this.importersActions) action.setEditor(pageEditor);
			for(MetaBupEditorAction action : this.toolbarActions) action.setEditor(pageEditor);
			for(MetaBupEditorAction action : this.compilersActions) action.setEditor(pageEditor);
		}
		
		IActionBars actionBars = getActionBars();
		if (actionBars != null) {

			ITextEditor editor = (part instanceof ITextEditor) ? (ITextEditor) part : null;

			actionBars.setGlobalActionHandler(
				ActionFactory.DELETE.getId(),
				getAction(editor, ITextEditorActionConstants.DELETE));
			actionBars.setGlobalActionHandler(
				ActionFactory.UNDO.getId(),
				getAction(editor, ITextEditorActionConstants.UNDO));
			actionBars.setGlobalActionHandler(
				ActionFactory.REDO.getId(),
				getAction(editor, ITextEditorActionConstants.REDO));
			actionBars.setGlobalActionHandler(
				ActionFactory.CUT.getId(),
				getAction(editor, ITextEditorActionConstants.CUT));
			actionBars.setGlobalActionHandler(
				ActionFactory.COPY.getId(),
				getAction(editor, ITextEditorActionConstants.COPY));
			actionBars.setGlobalActionHandler(
				ActionFactory.PASTE.getId(),
				getAction(editor, ITextEditorActionConstants.PASTE));
			actionBars.setGlobalActionHandler(
				ActionFactory.SELECT_ALL.getId(),
				getAction(editor, ITextEditorActionConstants.SELECT_ALL));
			actionBars.setGlobalActionHandler(
				ActionFactory.FIND.getId(),
				getAction(editor, ITextEditorActionConstants.FIND));
			actionBars.setGlobalActionHandler(
				IDEActionFactory.BOOKMARK.getId(),
				getAction(editor, IDEActionFactory.BOOKMARK.getId()));
		
			// Added for xText
			// TODO: Do this right!
			actionBars.setGlobalActionHandler("metabup.fragments.updateMetamodel", 
				getAction(editor, "metabup.fragments.updateMetamodel"));						
			
			actionBars.updateActionBars();
		}
		
	}
	
	private void createContributionActions(){
		// add editor contributions
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IConfigurationElement[] extensions = reg.getConfigurationElementsFor(Activator.ACTIONS_EXTENSION_ID);
						
		for(int i = 0; i<extensions.length; i++){
			MetaBupEditorAction action;
			try {
				action = (MetaBupEditorAction) extensions[i].createExecutableExtension("class");
				action.setText(extensions[i].getAttribute("text"));
				action.setToolTipText(extensions[i].getAttribute("tooltip"));
				
				// if specified, assign an icon
				String nsId = extensions[i].getDeclaringExtension().getNamespaceIdentifier();	
				//System.out.println("detected extension: " + nsId);
				ImageDescriptor desc = null;
				
				while(desc == null && !nsId.isEmpty()){
					desc = PluginUtils.getImageDescriptor(nsId,  extensions[i].getAttribute("icon"));
					
					if(desc == null){
						if(nsId.contains(".")){
							nsId = nsId.substring(0, nsId.lastIndexOf("."));
							//System.out.println("nsId = " + nsId);
						}else nsId = "";
					}
				}
				
				if(desc != null) action.setImageDescriptor(desc);
				
				if(action != null && extensions[i].getAttribute("group").equals("edit"))
					this.metamodelEditActions.add(action);
				if(action != null && extensions[i].getAttribute("group").equals("importers"))
					this.importersActions.add(action);
				else if(action != null && extensions[i].getAttribute("group").equals("toolbar"))
					this.toolbarActions.add(action);
				else if(action != null && extensions[i].getAttribute("group").equals("compilers"))
					this.compilersActions.add(action);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				System.out.println("problem found when loading extension: " + e.getMessage());
			}									
		}
	}
	
	private void createActions() {
		
		// sampleAction = (Action) ((MetamodelInteractiveEditor) activeEditorPart).xtextEditor.getAction("metabup.ui.editor.popup.actions.UpdateMetamodel");
		updateMetamodelAction = new UpdateMetamodel();
		updateMetamodelAction.setText("Update");
		updateMetamodelAction.setToolTipText("Update metamodel");
		
		ImageDescriptor desc = PluginUtils.getImageDescriptor(Activator.PLUGIN_ID, "icons/tree.png");
		if(desc != null) updateMetamodelAction.setImageDescriptor(desc);
		
		updateMetamodelByAssistanceTipAction = new UpdateMetamodelByAssistanceTip();
		updateMetamodelByAssistanceTipAction.setText("Update metamodel by assistance tip");
		updateMetamodelByAssistanceTipAction.setToolTipText("Update metamodel by assistance tip");
		
		generateCollaboroHistory = new GenerateCollaboroHistory();
		generateCollaboroHistory.setText("Collaboro History...");
		generateCollaboroHistory.setToolTipText("Generate Collaboro History");
		
	
		generateEcore = new CompileToEcoreAction();
		generateEcore.setText("Ecore...");
		generateEcore.setToolTipText("Compile metamodel to Ecore");

	}
	
	public void contributeToMenu(IMenuManager manager) {
		IMenuManager menu = new MenuManager("&MetaBup");
		manager.prependToGroup(IWorkbenchActionConstants.MB_ADDITIONS, menu);
		menu.add(updateMetamodelAction);
		
		IMenuManager fedit = new MenuManager("Edit Metamodel");
		for(MetaBupEditorAction action : this.metamodelEditActions) fedit.add(action);
		menu.add(fedit);	
		
		IMenuManager fimport = new MenuManager("Import Fragment");
		for(MetaBupEditorAction action : this.importersActions) fimport.add(action);
		menu.add(fimport);		
		
		IMenuManager compilers = new MenuManager("Generate");
		compilers.add(generateEcore);
		compilers.add(generateCollaboroHistory);
		for(MetaBupEditorAction action : this.compilersActions) compilers.add(action);
		menu.add(compilers);
	}
	
	public void contributeToToolBar(IToolBarManager manager) {
		manager.add(new Separator());
		manager.add(updateMetamodelAction);
		
		for(MetaBupEditorAction action : this.toolbarActions) manager.add(action);
	}
}
