/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.ClassFilter;
import metamodeltest.ClassSelector;
import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Selector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.ClassSelectorImpl#getClassConditioner <em>Class Conditioner</em>}</li>
 *   <li>{@link metamodeltest.impl.ClassSelectorImpl#getClassFilter <em>Class Filter</em>}</li>
 * </ul>
 * </p>
 *
 * @protected
 */
public class ClassSelectorImpl extends SelectorImpl implements ClassSelector {
	/**
	 * The cached value of the '{@link #getClassConditioner() <em>Class Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassConditioner()
	 * @generated
	 * @ordered
	 */
	protected ClassFilter classConditioner;

	/**
	 * The cached value of the '{@link #getClassFilter() <em>Class Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassFilter()
	 * @generated
	 * @ordered
	 */
	protected ClassFilter classFilter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassSelectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.CLASS_SELECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassFilter getClassConditioner() {
		return classConditioner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassConditioner(ClassFilter newClassConditioner, NotificationChain msgs) {
		ClassFilter oldClassConditioner = classConditioner;
		classConditioner = newClassConditioner;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_SELECTOR__CLASS_CONDITIONER, oldClassConditioner, newClassConditioner);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassConditioner(ClassFilter newClassConditioner) {
		if (newClassConditioner != classConditioner) {
			NotificationChain msgs = null;
			if (classConditioner != null)
				msgs = ((InternalEObject)classConditioner).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_SELECTOR__CLASS_CONDITIONER, null, msgs);
			if (newClassConditioner != null)
				msgs = ((InternalEObject)newClassConditioner).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_SELECTOR__CLASS_CONDITIONER, null, msgs);
			msgs = basicSetClassConditioner(newClassConditioner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_SELECTOR__CLASS_CONDITIONER, newClassConditioner, newClassConditioner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassFilter getClassFilter() {
		return classFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassFilter(ClassFilter newClassFilter, NotificationChain msgs) {
		ClassFilter oldClassFilter = classFilter;
		classFilter = newClassFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_SELECTOR__CLASS_FILTER, oldClassFilter, newClassFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassFilter(ClassFilter newClassFilter) {
		if (newClassFilter != classFilter) {
			NotificationChain msgs = null;
			if (classFilter != null)
				msgs = ((InternalEObject)classFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_SELECTOR__CLASS_FILTER, null, msgs);
			if (newClassFilter != null)
				msgs = ((InternalEObject)newClassFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_SELECTOR__CLASS_FILTER, null, msgs);
			msgs = basicSetClassFilter(newClassFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_SELECTOR__CLASS_FILTER, newClassFilter, newClassFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_CONDITIONER:
				return basicSetClassConditioner(null, msgs);
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_FILTER:
				return basicSetClassFilter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_CONDITIONER:
				return getClassConditioner();
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_FILTER:
				return getClassFilter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_CONDITIONER:
				setClassConditioner((ClassFilter)newValue);
				return;
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_FILTER:
				setClassFilter((ClassFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_CONDITIONER:
				setClassConditioner((ClassFilter)null);
				return;
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_FILTER:
				setClassFilter((ClassFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_CONDITIONER:
				return classConditioner != null;
			case MetamodeltestPackage.CLASS_SELECTOR__CLASS_FILTER:
				return classFilter != null;
		}
		return super.eIsSet(featureID);
	}

} //ClassSelectorImpl
