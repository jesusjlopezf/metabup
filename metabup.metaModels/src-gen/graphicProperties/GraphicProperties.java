/**
 */
package graphicProperties;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Graphic Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see graphicProperties.GraphicPropertiesPackage#getGraphicProperties()
 * @model abstract="true"
 * @generated
 */
public interface GraphicProperties extends EObject {
} // GraphicProperties
