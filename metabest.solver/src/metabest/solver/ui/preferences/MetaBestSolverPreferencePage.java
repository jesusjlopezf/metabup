/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.preferences;

import org.eclipse.jface.preference.*;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.IWorkbench;
import metabest.solver.Activator;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class MetaBestSolverPreferencePage
	extends FieldEditorPreferencePage
	implements IWorkbenchPreferencePage {

	public MetaBestSolverPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Select your preferences for example generation.");
	}
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {
		IntegerFieldEditor minObjectNumber = new IntegerFieldEditor(
				PreferenceConstants.P_MIN_OBJECT_NUMBER,
				"Minimum number of objects of each type to generate:",
		 		getFieldEditorParent());
		minObjectNumber.setValidRange(0, Integer.MAX_VALUE);
		addField(minObjectNumber);
		
		IntegerFieldEditor maxObjectNumber = new IntegerFieldEditor(
				PreferenceConstants.P_MAX_OBJECT_NUMBER,
				"Maximum number of objects of each type to generate:",
		 		getFieldEditorParent());
		maxObjectNumber.setValidRange(1, Integer.MAX_VALUE);
		addField(maxObjectNumber);
		
		IntegerFieldEditor minDefaultReferenceCardinality = new IntegerFieldEditor(
				PreferenceConstants.P_MIN_DEFAULT_REFERENCE_CARDINALITY,
				"Minimum default reference cardinality:",
		 		getFieldEditorParent());
		minDefaultReferenceCardinality.setValidRange(0, Integer.MAX_VALUE);
		addField(minDefaultReferenceCardinality);
		
		IntegerFieldEditor maxDefaultReferenceCardinality = new IntegerFieldEditor(
				PreferenceConstants.P_MAX_DEFAULT_REFERENCE_CARDINALITY,
				"Maximum default reference cardinality:",
		 		getFieldEditorParent());
		maxDefaultReferenceCardinality.setValidRange(1, Integer.MAX_VALUE);
		addField(maxDefaultReferenceCardinality);
		
		BooleanFieldEditor addAttributes = new BooleanFieldEditor(
				PreferenceConstants.P_ADD_ATTRIBUTES,
				"Add new attributes to base fragment objects",
				getFieldEditorParent());
		addField(addAttributes);				
		
		BooleanFieldEditor addAssociations = new BooleanFieldEditor(
				PreferenceConstants.P_ADD_ASSOCIATIONS,
				"Add new associations to base fragment objects",
				getFieldEditorParent());
		addField(addAssociations);
		
		BooleanFieldEditor addContainees = new BooleanFieldEditor(
				PreferenceConstants.P_ADD_CONTAINEES,
				"Add new objects to base fragment containers",
				getFieldEditorParent());
		addField(addContainees);
		
		BooleanFieldEditor makeUnreachable = new BooleanFieldEditor(
				PreferenceConstants.P_MAKE_UNREACHABLE,
				"Make base objects unreachable from new ones",
				getFieldEditorParent());
		addField(makeUnreachable);
		
		/*addField(new DirectoryFieldEditor(PreferenceConstants.P_PATH, 
				"&Directory preference:", getFieldEditorParent()));*/
		
		/*addField(
			new BooleanFieldEditor(
				PreferenceConstants.P_FORZE_ZERO_TO_ONE,
				"&Force reference generation for min 0 cardinality",
				getFieldEditorParent()));*/

		/*addField(new RadioGroupFieldEditor(
				PreferenceConstants.P_CHOICE,
			"An example of a multiple-choice preference",
			1,
			new String[][] { { "&Choice 1", "choice1" }, {
				"C&hoice 2", "choice2" }
		}, getFieldEditorParent()));
		addField(
			new StringFieldEditor(PreferenceConstants.P_STRING, "A &text preference:", getFieldEditorParent()));*/
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}