/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.BooleanParameter;
import metamodeltest.ClassContainee;
import metamodeltest.ClassSelector;
import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Containee</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.ClassContaineeImpl#getClassSel <em>Class Sel</em>}</li>
 *   <li>{@link metamodeltest.impl.ClassContaineeImpl#getByInheritance <em>By Inheritance</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ClassContaineeImpl extends FeatureQualifierImpl implements ClassContainee {
	/**
	 * The cached value of the '{@link #getClassSel() <em>Class Sel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassSel()
	 * @generated
	 * @ordered
	 */
	protected ClassSelector classSel;
	/**
	 * The cached value of the '{@link #getByInheritance() <em>By Inheritance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getByInheritance()
	 * @generated
	 * @ordered
	 */
	protected BooleanParameter byInheritance;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassContaineeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.CLASS_CONTAINEE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSelector getClassSel() {
		return classSel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassSel(ClassSelector newClassSel, NotificationChain msgs) {
		ClassSelector oldClassSel = classSel;
		classSel = newClassSel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_CONTAINEE__CLASS_SEL, oldClassSel, newClassSel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassSel(ClassSelector newClassSel) {
		if (newClassSel != classSel) {
			NotificationChain msgs = null;
			if (classSel != null)
				msgs = ((InternalEObject)classSel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_CONTAINEE__CLASS_SEL, null, msgs);
			if (newClassSel != null)
				msgs = ((InternalEObject)newClassSel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_CONTAINEE__CLASS_SEL, null, msgs);
			msgs = basicSetClassSel(newClassSel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_CONTAINEE__CLASS_SEL, newClassSel, newClassSel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameter getByInheritance() {
		return byInheritance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetByInheritance(BooleanParameter newByInheritance, NotificationChain msgs) {
		BooleanParameter oldByInheritance = byInheritance;
		byInheritance = newByInheritance;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_CONTAINEE__BY_INHERITANCE, oldByInheritance, newByInheritance);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setByInheritance(BooleanParameter newByInheritance) {
		if (newByInheritance != byInheritance) {
			NotificationChain msgs = null;
			if (byInheritance != null)
				msgs = ((InternalEObject)byInheritance).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_CONTAINEE__BY_INHERITANCE, null, msgs);
			if (newByInheritance != null)
				msgs = ((InternalEObject)newByInheritance).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_CONTAINEE__BY_INHERITANCE, null, msgs);
			msgs = basicSetByInheritance(newByInheritance, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_CONTAINEE__BY_INHERITANCE, newByInheritance, newByInheritance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_CONTAINEE__CLASS_SEL:
				return basicSetClassSel(null, msgs);
			case MetamodeltestPackage.CLASS_CONTAINEE__BY_INHERITANCE:
				return basicSetByInheritance(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_CONTAINEE__CLASS_SEL:
				return getClassSel();
			case MetamodeltestPackage.CLASS_CONTAINEE__BY_INHERITANCE:
				return getByInheritance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_CONTAINEE__CLASS_SEL:
				setClassSel((ClassSelector)newValue);
				return;
			case MetamodeltestPackage.CLASS_CONTAINEE__BY_INHERITANCE:
				setByInheritance((BooleanParameter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_CONTAINEE__CLASS_SEL:
				setClassSel((ClassSelector)null);
				return;
			case MetamodeltestPackage.CLASS_CONTAINEE__BY_INHERITANCE:
				setByInheritance((BooleanParameter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_CONTAINEE__CLASS_SEL:
				return classSel != null;
			case MetamodeltestPackage.CLASS_CONTAINEE__BY_INHERITANCE:
				return byInheritance != null;
		}
		return super.eIsSet(featureID);
	}

} //ClassContaineeImpl
