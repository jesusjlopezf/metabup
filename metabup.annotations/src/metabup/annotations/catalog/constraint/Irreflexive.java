/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.constraint;

import java.util.ArrayList;
import java.util.List;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.Object;
import metabup.annotations.extensionpoint.ConstraintAnnotation;
import metabup.annotations.extensionpoint.DesignAnnotation;
import metabup.issues.general.IOpenIssue;
import metabup.issues.lib.AcyclicReferenceCycle;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;

public class Irreflexive extends ConstraintAnnotation {
	public Irreflexive (){
		this.setName("irreflexive");
		this.setDescription("It forbids self-loops through a reference");
	}
	
	@Override
	public List<IOpenIssue> detectFragmentIssues(FragmentModel fm, AnnotatedElement ae){
		if(fm == null || ae == null || fm.getFragments() == null || fm.getFragments().isEmpty()) return null;
		List<IOpenIssue> issues = new ArrayList<IOpenIssue>();
		
		if(ae instanceof Reference){			
			Reference mmr = (Reference) ae;
			String name = mmr.getName();			
			
			List<fragments.Reference> references = new ArrayList<fragments.Reference>();
			
			for(Fragment f : fm.getFragments()){
				for(Object o : f.getObjects()){
					for(fragments.Reference fr : getReferences(o)){
						if(name.equals(fr.getName())){
							fragments.Object parent = (fragments.Object)fr.eContainer();
							
							for(fragments.Object tar : fr.getReference())
								if(tar.equals(parent) && !references.contains(fr)) 
									references.add(fr);
						}
					}
				}
			}
			
			if(!references.isEmpty()){ 
				for(fragments.Reference r : references){
					AcyclicReferenceCycle arc = new AcyclicReferenceCycle(mmr, r, AcyclicReferenceCycle.DEPTH_ONE);
					issues.add(arc);
					return issues;
				}				
			}
		}
		
		return null;
	}	

	private List<fragments.Reference> getReferences(Object o){
		List<fragments.Reference> references = new ArrayList<fragments.Reference>();
		
		for(fragments.Feature f : o.getFeatures())
			if(f instanceof fragments.Reference)
				references.add((fragments.Reference)f);
		
		return references;
	}

	@Override
	public void updateConstraints() {
		if(element instanceof Reference){
			Reference reference = (Reference)element;
			
			String REFSRC = ((MetaClass)reference.eContainer()).getName();
			String ELEMENT = reference.getName();
			
			super.addConstraint("irreflexive", "context "+ REFSRC + " inv " + REFSRC + "_" + ELEMENT + "_irreflexive: self." + ELEMENT + " -> excludes(self)");		
		}
		
	}
}
