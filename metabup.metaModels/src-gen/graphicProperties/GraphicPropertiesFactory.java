/**
 */
package graphicProperties;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see graphicProperties.GraphicPropertiesPackage
 * @generated
 */
public interface GraphicPropertiesFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GraphicPropertiesFactory eINSTANCE = graphicProperties.impl.GraphicPropertiesFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Node Properties</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Node Properties</em>'.
	 * @generated
	 */
	NodeProperties createNodeProperties();

	/**
	 * Returns a new object of class '<em>Edge Properties</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Edge Properties</em>'.
	 * @generated
	 */
	EdgeProperties createEdgeProperties();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GraphicPropertiesPackage getGraphicPropertiesPackage();

} //GraphicPropertiesFactory
