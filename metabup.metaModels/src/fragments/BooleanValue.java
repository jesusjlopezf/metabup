/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fragments.BooleanValue#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fragments.FragmentsPackage#getBooleanValue()
 * @model
 * @generated
 */
public interface BooleanValue extends PrimitiveValue {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Boolean)
	 * @see fragments.FragmentsPackage#getBooleanValue_Value()
	 * @model required="true"
	 * @generated
	 */
	Boolean getValue();

	/**
	 * Sets the value of the '{@link fragments.BooleanValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Boolean value);

} // BooleanValue
