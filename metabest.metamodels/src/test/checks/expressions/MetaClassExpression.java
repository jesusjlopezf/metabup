/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.expressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Class Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.expressions.MetaClassExpression#getMetaclassName <em>Metaclass Name</em>}</li>
 * </ul>
 *
 * @see test.checks.expressions.ExpressionsPackage#getMetaClassExpression()
 * @model
 * @generated
 */
public interface MetaClassExpression extends ObjectOrMetaClassExpression {
	/**
	 * Returns the value of the '<em><b>Metaclass Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metaclass Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metaclass Name</em>' attribute.
	 * @see #setMetaclassName(String)
	 * @see test.checks.expressions.ExpressionsPackage#getMetaClassExpression_MetaclassName()
	 * @model required="true"
	 * @generated
	 */
	String getMetaclassName();

	/**
	 * Sets the value of the '{@link test.checks.expressions.MetaClassExpression#getMetaclassName <em>Metaclass Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metaclass Name</em>' attribute.
	 * @see #getMetaclassName()
	 * @generated
	 */
	void setMetaclassName(String value);

} // MetaClassExpression
