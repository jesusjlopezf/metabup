/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.ParameterValue#getParamDefinition <em>Param Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getParameterValue()
 * @model abstract="true"
 * @generated
 */
public interface ParameterValue extends EObject {

	/**
	 * Returns the value of the '<em><b>Param Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Param Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Param Definition</em>' reference.
	 * @see #setParamDefinition(UserDefinedParameter)
	 * @see metamodeltest.MetamodeltestPackage#getParameterValue_ParamDefinition()
	 * @model required="true"
	 * @generated
	 */
	UserDefinedParameter getParamDefinition();

	/**
	 * Sets the value of the '{@link metamodeltest.ParameterValue#getParamDefinition <em>Param Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Param Definition</em>' reference.
	 * @see #getParamDefinition()
	 * @generated
	 */
	void setParamDefinition(UserDefinedParameter value);
} // ParameterValue
