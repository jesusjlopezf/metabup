/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import java.util.Collection;
import metamodeltest.BooleanParameter;
import metamodeltest.ClassRelation;
import metamodeltest.ClassSelector;
import metamodeltest.Length;
import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.ClassRelationImpl#getClassSels <em>Class Sels</em>}</li>
 *   <li>{@link metamodeltest.impl.ClassRelationImpl#getJumps <em>Jumps</em>}</li>
 *   <li>{@link metamodeltest.impl.ClassRelationImpl#getByInheritance <em>By Inheritance</em>}</li>
 *   <li>{@link metamodeltest.impl.ClassRelationImpl#getContainment <em>Containment</em>}</li>
 *   <li>{@link metamodeltest.impl.ClassRelationImpl#getStrict <em>Strict</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class ClassRelationImpl extends ClassQualifierImpl implements ClassRelation {
	/**
	 * The cached value of the '{@link #getClassSels() <em>Class Sels</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassSels()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassSelector> classSels;

	/**
	 * The cached value of the '{@link #getJumps() <em>Jumps</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJumps()
	 * @generated
	 * @ordered
	 */
	protected Length jumps;

	/**
	 * The cached value of the '{@link #getByInheritance() <em>By Inheritance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getByInheritance()
	 * @generated
	 * @ordered
	 */
	protected BooleanParameter byInheritance;

	/**
	 * The cached value of the '{@link #getContainment() <em>Containment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainment()
	 * @generated
	 * @ordered
	 */
	protected BooleanParameter containment;

	/**
	 * The cached value of the '{@link #getStrict() <em>Strict</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrict()
	 * @generated
	 * @ordered
	 */
	protected BooleanParameter strict;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.CLASS_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassSelector> getClassSels() {
		if (classSels == null) {
			classSels = new EObjectContainmentEList<ClassSelector>(ClassSelector.class, this, MetamodeltestPackage.CLASS_RELATION__CLASS_SELS);
		}
		return classSels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Length getJumps() {
		return jumps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetJumps(Length newJumps, NotificationChain msgs) {
		Length oldJumps = jumps;
		jumps = newJumps;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_RELATION__JUMPS, oldJumps, newJumps);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJumps(Length newJumps) {
		if (newJumps != jumps) {
			NotificationChain msgs = null;
			if (jumps != null)
				msgs = ((InternalEObject)jumps).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_RELATION__JUMPS, null, msgs);
			if (newJumps != null)
				msgs = ((InternalEObject)newJumps).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_RELATION__JUMPS, null, msgs);
			msgs = basicSetJumps(newJumps, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_RELATION__JUMPS, newJumps, newJumps));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameter getByInheritance() {
		return byInheritance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetByInheritance(BooleanParameter newByInheritance, NotificationChain msgs) {
		BooleanParameter oldByInheritance = byInheritance;
		byInheritance = newByInheritance;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_RELATION__BY_INHERITANCE, oldByInheritance, newByInheritance);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setByInheritance(BooleanParameter newByInheritance) {
		if (newByInheritance != byInheritance) {
			NotificationChain msgs = null;
			if (byInheritance != null)
				msgs = ((InternalEObject)byInheritance).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_RELATION__BY_INHERITANCE, null, msgs);
			if (newByInheritance != null)
				msgs = ((InternalEObject)newByInheritance).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_RELATION__BY_INHERITANCE, null, msgs);
			msgs = basicSetByInheritance(newByInheritance, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_RELATION__BY_INHERITANCE, newByInheritance, newByInheritance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameter getContainment() {
		return containment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainment(BooleanParameter newContainment, NotificationChain msgs) {
		BooleanParameter oldContainment = containment;
		containment = newContainment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_RELATION__CONTAINMENT, oldContainment, newContainment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainment(BooleanParameter newContainment) {
		if (newContainment != containment) {
			NotificationChain msgs = null;
			if (containment != null)
				msgs = ((InternalEObject)containment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_RELATION__CONTAINMENT, null, msgs);
			if (newContainment != null)
				msgs = ((InternalEObject)newContainment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_RELATION__CONTAINMENT, null, msgs);
			msgs = basicSetContainment(newContainment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_RELATION__CONTAINMENT, newContainment, newContainment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameter getStrict() {
		return strict;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStrict(BooleanParameter newStrict, NotificationChain msgs) {
		BooleanParameter oldStrict = strict;
		strict = newStrict;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_RELATION__STRICT, oldStrict, newStrict);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrict(BooleanParameter newStrict) {
		if (newStrict != strict) {
			NotificationChain msgs = null;
			if (strict != null)
				msgs = ((InternalEObject)strict).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_RELATION__STRICT, null, msgs);
			if (newStrict != null)
				msgs = ((InternalEObject)newStrict).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.CLASS_RELATION__STRICT, null, msgs);
			msgs = basicSetStrict(newStrict, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.CLASS_RELATION__STRICT, newStrict, newStrict));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_RELATION__CLASS_SELS:
				return ((InternalEList<?>)getClassSels()).basicRemove(otherEnd, msgs);
			case MetamodeltestPackage.CLASS_RELATION__JUMPS:
				return basicSetJumps(null, msgs);
			case MetamodeltestPackage.CLASS_RELATION__BY_INHERITANCE:
				return basicSetByInheritance(null, msgs);
			case MetamodeltestPackage.CLASS_RELATION__CONTAINMENT:
				return basicSetContainment(null, msgs);
			case MetamodeltestPackage.CLASS_RELATION__STRICT:
				return basicSetStrict(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_RELATION__CLASS_SELS:
				return getClassSels();
			case MetamodeltestPackage.CLASS_RELATION__JUMPS:
				return getJumps();
			case MetamodeltestPackage.CLASS_RELATION__BY_INHERITANCE:
				return getByInheritance();
			case MetamodeltestPackage.CLASS_RELATION__CONTAINMENT:
				return getContainment();
			case MetamodeltestPackage.CLASS_RELATION__STRICT:
				return getStrict();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_RELATION__CLASS_SELS:
				getClassSels().clear();
				getClassSels().addAll((Collection<? extends ClassSelector>)newValue);
				return;
			case MetamodeltestPackage.CLASS_RELATION__JUMPS:
				setJumps((Length)newValue);
				return;
			case MetamodeltestPackage.CLASS_RELATION__BY_INHERITANCE:
				setByInheritance((BooleanParameter)newValue);
				return;
			case MetamodeltestPackage.CLASS_RELATION__CONTAINMENT:
				setContainment((BooleanParameter)newValue);
				return;
			case MetamodeltestPackage.CLASS_RELATION__STRICT:
				setStrict((BooleanParameter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_RELATION__CLASS_SELS:
				getClassSels().clear();
				return;
			case MetamodeltestPackage.CLASS_RELATION__JUMPS:
				setJumps((Length)null);
				return;
			case MetamodeltestPackage.CLASS_RELATION__BY_INHERITANCE:
				setByInheritance((BooleanParameter)null);
				return;
			case MetamodeltestPackage.CLASS_RELATION__CONTAINMENT:
				setContainment((BooleanParameter)null);
				return;
			case MetamodeltestPackage.CLASS_RELATION__STRICT:
				setStrict((BooleanParameter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.CLASS_RELATION__CLASS_SELS:
				return classSels != null && !classSels.isEmpty();
			case MetamodeltestPackage.CLASS_RELATION__JUMPS:
				return jumps != null;
			case MetamodeltestPackage.CLASS_RELATION__BY_INHERITANCE:
				return byInheritance != null;
			case MetamodeltestPackage.CLASS_RELATION__CONTAINMENT:
				return containment != null;
			case MetamodeltestPackage.CLASS_RELATION__STRICT:
				return strict != null;
		}
		return super.eIsSet(featureID);
	}

} //ClassRelationImpl
