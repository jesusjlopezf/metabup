/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.extensions;

import metamodeltest.Quantifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Class Selector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.extensions.MetaClassSelector#getFilter <em>Filter</em>}</li>
 *   <li>{@link test.extensions.MetaClassSelector#isOnlyNewElements <em>Only New Elements</em>}</li>
 *   <li>{@link test.extensions.MetaClassSelector#getQuantifier <em>Quantifier</em>}</li>
 *   <li>{@link test.extensions.MetaClassSelector#getMetaclassName <em>Metaclass Name</em>}</li>
 * </ul>
 *
 * @see test.extensions.ExtensionsPackage#getMetaClassSelector()
 * @model
 * @generated
 */
public interface MetaClassSelector extends Selector {
	/**
	 * Returns the value of the '<em><b>Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter</em>' containment reference.
	 * @see #setFilter(MetaClassQualifier)
	 * @see test.extensions.ExtensionsPackage#getMetaClassSelector_Filter()
	 * @model containment="true"
	 * @generated
	 */
	MetaClassQualifier getFilter();

	/**
	 * Sets the value of the '{@link test.extensions.MetaClassSelector#getFilter <em>Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter</em>' containment reference.
	 * @see #getFilter()
	 * @generated
	 */
	void setFilter(MetaClassQualifier value);

	/**
	 * Returns the value of the '<em><b>Only New Elements</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Only New Elements</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Only New Elements</em>' attribute.
	 * @see #setOnlyNewElements(boolean)
	 * @see test.extensions.ExtensionsPackage#getMetaClassSelector_OnlyNewElements()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isOnlyNewElements();

	/**
	 * Sets the value of the '{@link test.extensions.MetaClassSelector#isOnlyNewElements <em>Only New Elements</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Only New Elements</em>' attribute.
	 * @see #isOnlyNewElements()
	 * @generated
	 */
	void setOnlyNewElements(boolean value);

	/**
	 * Returns the value of the '<em><b>Quantifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantifier</em>' containment reference.
	 * @see #setQuantifier(Quantifier)
	 * @see test.extensions.ExtensionsPackage#getMetaClassSelector_Quantifier()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Quantifier getQuantifier();

	/**
	 * Sets the value of the '{@link test.extensions.MetaClassSelector#getQuantifier <em>Quantifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantifier</em>' containment reference.
	 * @see #getQuantifier()
	 * @generated
	 */
	void setQuantifier(Quantifier value);

	/**
	 * Returns the value of the '<em><b>Metaclass Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metaclass Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metaclass Name</em>' attribute.
	 * @see #setMetaclassName(String)
	 * @see test.extensions.ExtensionsPackage#getMetaClassSelector_MetaclassName()
	 * @model required="true"
	 * @generated
	 */
	String getMetaclassName();

	/**
	 * Sets the value of the '{@link test.extensions.MetaClassSelector#getMetaclassName <em>Metaclass Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metaclass Name</em>' attribute.
	 * @see #getMetaclassName()
	 * @generated
	 */
	void setMetaclassName(String value);

} // MetaClassSelector
