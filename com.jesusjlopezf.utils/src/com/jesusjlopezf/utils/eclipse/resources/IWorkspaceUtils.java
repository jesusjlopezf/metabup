package com.jesusjlopezf.utils.eclipse.resources;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;

public class IWorkspaceUtils {
	public static IWorkspaceRoot getCurrentWorkspace(){
		return ResourcesPlugin.getWorkspace().getRoot();
	}
}
