/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Existance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see metamodeltest.MetamodeltestPackage#getExistance()
 * @model
 * @generated
 */
public interface Existance extends ElementQualifier, PathQualifier {
} // Existance
