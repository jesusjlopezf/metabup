/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Multiplicity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.FeatureMultiplicity#getMin <em>Min</em>}</li>
 *   <li>{@link metamodeltest.FeatureMultiplicity#getMax <em>Max</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getFeatureMultiplicity()
 * @model
 * @generated
 */
public interface FeatureMultiplicity extends FeatureQualifier {
	/**
	 * Returns the value of the '<em><b>Min</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min</em>' containment reference.
	 * @see #setMin(metamodeltest.Number)
	 * @see metamodeltest.MetamodeltestPackage#getFeatureMultiplicity_Min()
	 * @model containment="true"
	 * @generated
	 */
	metamodeltest.Number getMin();

	/**
	 * Sets the value of the '{@link metamodeltest.FeatureMultiplicity#getMin <em>Min</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min</em>' containment reference.
	 * @see #getMin()
	 * @generated
	 */
	void setMin(metamodeltest.Number value);

	/**
	 * Returns the value of the '<em><b>Max</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max</em>' containment reference.
	 * @see #setMax(metamodeltest.Number)
	 * @see metamodeltest.MetamodeltestPackage#getFeatureMultiplicity_Max()
	 * @model containment="true"
	 * @generated
	 */
	metamodeltest.Number getMax();

	/**
	 * Sets the value of the '{@link metamodeltest.FeatureMultiplicity#getMax <em>Max</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max</em>' containment reference.
	 * @see #getMax()
	 * @generated
	 */
	void setMax(metamodeltest.Number value);

} // FeatureMultiplicity
