/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.refactoring;

import metabup.annotations.extensionpoint.RefactoringAnnotation;
import metabup.annotations.general.Annotation;
import metabup.metamodel.MetaModel;
import metabup.metamodel.ParamValue;
import metabup.metamodel.Reference;
import metabup.metamodel.StringValue;

public class Multiplicity extends RefactoringAnnotation {

	public Multiplicity(){
		setName("multiplicity");
		setDescription("Set min and max multiplicity of the annotated reference");
	}		
	
	@Override
	public MetaModel perform() {
		if(element instanceof Reference){
			ParamValue minPV = super.getAnnotationParamValueByParamName("min");
			ParamValue maxPV = super.getAnnotationParamValueByParamName("max");			
			String min, max;
			
			if(minPV instanceof StringValue){
				min = ((StringValue) minPV).getValue();
			}else{
				this.putError("An integer value parameter was expected for \"min\"");
				return null;
			}
			
			if(maxPV instanceof StringValue){
				max = ((StringValue) maxPV).getValue();
			}else{
				this.putError("An integer value parameter was expected for \"name\"");
				return null;
			}
			
			int minvalue, maxvalue;
			
			if(isNumeric(min)) minvalue = Integer.parseInt(min);							
			else{
				super.putError("Min attribute was parsed a non-integer value");
				return null;
			}
			
			if(isNumeric(max)) maxvalue = Integer.parseInt(max);			
			else{
				super.putError("Max attribute was parsed a non-integer value");
				return null;
			}
			
			if((maxvalue != -1) && (maxvalue < minvalue)){
				super.putError("Parsed min attribute is greater than max");
				return null;
			}
			
			((Reference) element).setMin(minvalue);
			((Reference) element).setMax(maxvalue);
			
			return targetMetaModel;
		}
		return null;
	}
	
	private boolean isNumeric(String str){
		try{  
			double d = Double.parseDouble(str);  
		}catch(NumberFormatException nfe){  
			return false;  
		}  
		
		return true;  
	}
}
