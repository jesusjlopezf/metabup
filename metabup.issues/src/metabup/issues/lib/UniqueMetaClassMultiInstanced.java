/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.issues.lib;

import java.util.ArrayList;
import java.util.List;

import fragments.Object;
import metabup.issues.general.categories.Conflict;
import metabup.metamodel.Annotation;
import metabup.metamodel.MetaClass;

public class UniqueMetaClassMultiInstanced extends Conflict {
	public UniqueMetaClassMultiInstanced(MetaClass mc, List<Object> objects){
		List<String> oNames = new ArrayList<String>();
		for(Object o : objects) oNames.add(o.getName());
		
		if(mc != null && objects != null && !objects.isEmpty()){
			super.elements.add(mc);
			super.setDescription("The objects " + oNames + " make instance of a @unique annotated MetaClass " + mc.getName() + ". Delete annotation");
		}		
	}
	
	@Override
	public void run() {
		MetaClass mc = (MetaClass)elements.get(0);
		Annotation removeA = null;
		
		for(Annotation a : mc.getAnnotations()) if(a.getName().equals("unique")) removeA = a;		
		if(removeA != null) mc.getAnnotations().remove(removeA);
	}

}
