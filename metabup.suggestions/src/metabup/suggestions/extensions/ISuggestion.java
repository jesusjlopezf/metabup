package metabup.suggestions.extensions;

import java.util.List;

import metabup.metamodel.AnnotatedElement;

public interface ISuggestion {
	void execute(List<AnnotatedElement> annotatedElements);
	public void setDescription(String description);
	public String getDescription();
}
