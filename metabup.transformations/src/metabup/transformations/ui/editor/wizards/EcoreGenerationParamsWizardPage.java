/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations.ui.editor.wizards;

import java.awt.TrayIcon.MessageType;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class EcoreGenerationParamsWizardPage extends WizardPage {	
	private Text name;
	private String suggName;
	private Text nsPrefix;
	private String suggPrefix;
	private Text nsExtension;
	private String suggExtension;
	private Text nsURI;
	private String suggURI;
	private Composite container;
	
	public EcoreGenerationParamsWizardPage(String sName, String sPrefix, String sURI, String sExtension) {
		super("Ecore compilation");
		setTitle("Ecore metamodel compilation");
	    setDescription("Configure basic info for \'.ecore\' model generation");
	    setControl(name);
	    
	    this.suggName = sName;
	    this.suggPrefix = sPrefix;
	    this.suggURI = sURI;
	    this.suggExtension = sExtension;
	}	


	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
	    container.setLayout(layout);
	    layout.numColumns = 2;
	    Label label1 = new Label(container, SWT.NONE);
	    label1.setText("Name");

	    name = new Text(container, SWT.BORDER | SWT.SINGLE);
	    name.setText(suggName);
	    name.addKeyListener(new KeyListener() {
	    	@Override
	      	public void keyPressed(KeyEvent e) {
	    		// TODO Auto-generated method stub
	      	}

	      	@Override
	      	public void keyReleased(KeyEvent e) {
	      		if (name.getText().isEmpty() || nsPrefix.getText().isEmpty() || nsURI.getText().isEmpty() || nsExtension.getText().isEmpty()){
	      			setPageComplete(false);
	      			setErrorMessage("All fields must be completed.");
	      		}
	    	  	else if(nsExtension.getText().toLowerCase().equals(nsPrefix.getText().toLowerCase())){
	    	  		setPageComplete(false);
	      			setErrorMessage("Extension and prefix fields must have a different value.");
	    	  	}
	    	  	else setPageComplete(true);
	      	}
	    });
	    
	    GridData gd = new GridData(GridData.FILL_HORIZONTAL);
	    name.setLayoutData(gd);
	    
	    Label label2 = new Label(container, SWT.NONE);
	    label2.setText("Namespace prefix");
	    
	    nsPrefix = new Text(container, SWT.BORDER | SWT.SINGLE);
	    nsPrefix.setText(suggPrefix);
	    nsPrefix.addKeyListener(new KeyListener() {
	    	@Override
	      	public void keyPressed(KeyEvent e) {
	    		// TODO Auto-generated method stub
	      	}

	      	@Override
	      	public void keyReleased(KeyEvent e) {
	      		if (name.getText().isEmpty() || nsPrefix.getText().isEmpty() || nsURI.getText().isEmpty() || nsExtension.getText().isEmpty()){
	      			setPageComplete(false);
	      			setErrorMessage("All fields must be completed.");
	      		}
	    	  	else if(nsExtension.getText().toLowerCase().equals(nsPrefix.getText().toLowerCase())){
	    	  		setPageComplete(false);
	      			setErrorMessage("Extension and prefix fields must have a different value.");
	    	  	}
	    	  	else setPageComplete(true);
	      	}
	    });
	    
	    nsPrefix.setLayoutData(gd);
	    
	    Label label3 = new Label(container, SWT.NONE);
	    label3.setText("Namespace URI");
	    
	    nsURI = new Text(container, SWT.BORDER | SWT.SINGLE);
	    nsURI.setText(suggURI);
	    nsURI.addKeyListener(new KeyListener() {
	    	@Override
	      	public void keyPressed(KeyEvent e) {
	    		// TODO Auto-generated method stub
	      	}

	      	@Override
	      	public void keyReleased(KeyEvent e) {
	      		if (name.getText().isEmpty() || nsPrefix.getText().isEmpty() || nsURI.getText().isEmpty() || nsExtension.getText().isEmpty()){
	      			setPageComplete(false);
	      			setErrorMessage("All fields must be completed.");
	      		}
	    	  	else if(nsExtension.getText().toLowerCase().equals(nsPrefix.getText().toLowerCase())){
	    	  		setPageComplete(false);
	      			setErrorMessage("Extension and prefix fields must have a different value.");
	    	  	}
	    	  	else setPageComplete(true);
	      	}
	    });
	    
	    nsURI.setLayoutData(gd);	    	    	    
	    
	    Label label4 = new Label(container, SWT.NONE);
	    label4.setText("Extension");
	    
	    nsExtension = new Text(container, SWT.BORDER | SWT.SINGLE);
	    nsExtension.setText(suggExtension);
	    nsExtension.addKeyListener(new KeyListener() {
	    	@Override
	      	public void keyPressed(KeyEvent e) {
	    		// TODO Auto-generated method stub
	      	}

	      	@Override
	      	public void keyReleased(KeyEvent e) {
	      		if (name.getText().isEmpty() || nsPrefix.getText().isEmpty() || nsURI.getText().isEmpty() || nsExtension.getText().isEmpty()){
	      			setPageComplete(false);
	      			setErrorMessage("All fields must be completed.");
	      		}
	    	  	else if(nsExtension.getText().toLowerCase().equals(nsPrefix.getText().toLowerCase())){
	    	  		setPageComplete(false);
	      			setErrorMessage("Extension and prefix fields must have a different value.");
	    	  	}
	    	  	else setPageComplete(true);
	      	}
	    });
	    
	    nsExtension.setLayoutData(gd);
	    
	    // Required to avoid an error in the system
	    setControl(container);
	    setPageComplete(true);
	  }

	  public String getEcoreName() {
	    return name.getText();
	  }
	  
	  public String getEcorePrefix() {
		  return nsPrefix.getText();
	  }
	  
	  public String getEcoreURI() {
		  return nsURI.getText();
	  }
	  
	  public String getExtension(){
		  return nsExtension.getText();
	  }
}
