package metabup.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import metabup.services.FragmentsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFragmentsParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'model'", "'fragment'", "'['", "']'", "'{'", "'}'", "':'", "';'", "'@'", "'('", "','", "')'", "'->'", "'.'", "'='", "'attr'", "'ref'", "'rel'", "'style'", "'color'", "'width'", "'line'", "'source'", "'target'", "'true'", "'false'", "'positive'", "'negative'", "'dashed'", "'dashed-dotted'", "'dotted'", "'circle'", "'concave'", "'convex'", "'crows-foot-many'", "'crows-foot-many-mandatory'", "'crows-foot-many-optional'", "'crows-foot-one'", "'crows-foot-one-mandatory'", "'crows-foot-one-optional'", "'dash'", "'delta'", "'diamond'", "'none'", "'plain'", "'short'", "'skewed-dash'", "'standard'", "'transparent-circle'", "'t-shape'", "'white-delta'", "'white-diamond'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__62=62;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalFragmentsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFragmentsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFragmentsParser.tokenNames; }
    public String getGrammarFileName() { return "../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g"; }



     	private FragmentsGrammarAccess grammarAccess;
     	
        public InternalFragmentsParser(TokenStream input, FragmentsGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "FragmentModel";	
       	}
       	
       	@Override
       	protected FragmentsGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleFragmentModel"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:68:1: entryRuleFragmentModel returns [EObject current=null] : iv_ruleFragmentModel= ruleFragmentModel EOF ;
    public final EObject entryRuleFragmentModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragmentModel = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:69:2: (iv_ruleFragmentModel= ruleFragmentModel EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:70:2: iv_ruleFragmentModel= ruleFragmentModel EOF
            {
             newCompositeNode(grammarAccess.getFragmentModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentModel_in_entryRuleFragmentModel75);
            iv_ruleFragmentModel=ruleFragmentModel();

            state._fsp--;

             current =iv_ruleFragmentModel; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragmentModel"


    // $ANTLR start "ruleFragmentModel"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:77:1: ruleFragmentModel returns [EObject current=null] : ( () otherlv_1= 'model' ( (lv_name_2_0= ruleEString ) ) ( (lv_fragments_3_0= ruleFragment ) ) ( (lv_fragments_4_0= ruleFragment ) )* ) ;
    public final EObject ruleFragmentModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_fragments_3_0 = null;

        EObject lv_fragments_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:80:28: ( ( () otherlv_1= 'model' ( (lv_name_2_0= ruleEString ) ) ( (lv_fragments_3_0= ruleFragment ) ) ( (lv_fragments_4_0= ruleFragment ) )* ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:81:1: ( () otherlv_1= 'model' ( (lv_name_2_0= ruleEString ) ) ( (lv_fragments_3_0= ruleFragment ) ) ( (lv_fragments_4_0= ruleFragment ) )* )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:81:1: ( () otherlv_1= 'model' ( (lv_name_2_0= ruleEString ) ) ( (lv_fragments_3_0= ruleFragment ) ) ( (lv_fragments_4_0= ruleFragment ) )* )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:81:2: () otherlv_1= 'model' ( (lv_name_2_0= ruleEString ) ) ( (lv_fragments_3_0= ruleFragment ) ) ( (lv_fragments_4_0= ruleFragment ) )*
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:81:2: ()
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:82:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getFragmentModelAccess().getFragmentModelAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleFragmentModel131); 

                	newLeafNode(otherlv_1, grammarAccess.getFragmentModelAccess().getModelKeyword_1());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:91:1: ( (lv_name_2_0= ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:92:1: (lv_name_2_0= ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:92:1: (lv_name_2_0= ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:93:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getFragmentModelAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFragmentModel152);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentModelRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:109:2: ( (lv_fragments_3_0= ruleFragment ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:110:1: (lv_fragments_3_0= ruleFragment )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:110:1: (lv_fragments_3_0= ruleFragment )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:111:3: lv_fragments_3_0= ruleFragment
            {
             
            	        newCompositeNode(grammarAccess.getFragmentModelAccess().getFragmentsFragmentParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleFragment_in_ruleFragmentModel173);
            lv_fragments_3_0=ruleFragment();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentModelRule());
            	        }
                   		add(
                   			current, 
                   			"fragments",
                    		lv_fragments_3_0, 
                    		"Fragment");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:127:2: ( (lv_fragments_4_0= ruleFragment ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12||LA1_0==19||(LA1_0>=37 && LA1_0<=38)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:128:1: (lv_fragments_4_0= ruleFragment )
            	    {
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:128:1: (lv_fragments_4_0= ruleFragment )
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:129:3: lv_fragments_4_0= ruleFragment
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFragmentModelAccess().getFragmentsFragmentParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleFragment_in_ruleFragmentModel194);
            	    lv_fragments_4_0=ruleFragment();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFragmentModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"fragments",
            	            		lv_fragments_4_0, 
            	            		"Fragment");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentModel"


    // $ANTLR start "entryRuleFragment"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:153:1: entryRuleFragment returns [EObject current=null] : iv_ruleFragment= ruleFragment EOF ;
    public final EObject entryRuleFragment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFragment = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:154:2: (iv_ruleFragment= ruleFragment EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:155:2: iv_ruleFragment= ruleFragment EOF
            {
             newCompositeNode(grammarAccess.getFragmentRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragment_in_entryRuleFragment231);
            iv_ruleFragment=ruleFragment();

            state._fsp--;

             current =iv_ruleFragment; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragment241); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFragment"


    // $ANTLR start "ruleFragment"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:162:1: ruleFragment returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}' ) ;
    public final EObject ruleFragment() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_12=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        Enumerator lv_ftype_3_0 = null;

        AntlrDatatypeRuleToken lv_type_6_0 = null;

        AntlrDatatypeRuleToken lv_name_8_0 = null;

        EObject lv_objects_10_0 = null;

        EObject lv_objects_11_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:165:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}' ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:166:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}' )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:166:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}' )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:166:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_ftype_3_0= ruleFragmentType ) )? otherlv_4= 'fragment' (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )? ( (lv_name_8_0= ruleEString ) ) otherlv_9= '{' ( (lv_objects_10_0= ruleObject ) ) ( (lv_objects_11_0= ruleObject ) )* otherlv_12= '}'
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:166:2: ()
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:167:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getFragmentAccess().getFragmentAction_0(),
                        current);
                

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:172:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==19) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:172:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:172:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:173:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:173:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:174:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleFragment297);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:190:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==19) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:191:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:191:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:192:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleFragment318);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:208:5: ( (lv_ftype_3_0= ruleFragmentType ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>=37 && LA4_0<=38)) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:209:1: (lv_ftype_3_0= ruleFragmentType )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:209:1: (lv_ftype_3_0= ruleFragmentType )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:210:3: lv_ftype_3_0= ruleFragmentType
                    {
                     
                    	        newCompositeNode(grammarAccess.getFragmentAccess().getFtypeFragmentTypeEnumRuleCall_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFragmentType_in_ruleFragment342);
                    lv_ftype_3_0=ruleFragmentType();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
                    	        }
                           		set(
                           			current, 
                           			"ftype",
                            		lv_ftype_3_0, 
                            		"FragmentType");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleFragment355); 

                	newLeafNode(otherlv_4, grammarAccess.getFragmentAccess().getFragmentKeyword_3());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:230:1: (otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==13) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:230:3: otherlv_5= '[' ( (lv_type_6_0= ruleEString ) ) otherlv_7= ']'
                    {
                    otherlv_5=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleFragment368); 

                        	newLeafNode(otherlv_5, grammarAccess.getFragmentAccess().getLeftSquareBracketKeyword_4_0());
                        
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:234:1: ( (lv_type_6_0= ruleEString ) )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:235:1: (lv_type_6_0= ruleEString )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:235:1: (lv_type_6_0= ruleEString )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:236:3: lv_type_6_0= ruleEString
                    {
                     
                    	        newCompositeNode(grammarAccess.getFragmentAccess().getTypeEStringParserRuleCall_4_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFragment389);
                    lv_type_6_0=ruleEString();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
                    	        }
                           		set(
                           			current, 
                           			"type",
                            		lv_type_6_0, 
                            		"EString");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    otherlv_7=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleFragment401); 

                        	newLeafNode(otherlv_7, grammarAccess.getFragmentAccess().getRightSquareBracketKeyword_4_2());
                        

                    }
                    break;

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:256:3: ( (lv_name_8_0= ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:257:1: (lv_name_8_0= ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:257:1: (lv_name_8_0= ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:258:3: lv_name_8_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getFragmentAccess().getNameEStringParserRuleCall_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleFragment424);
            lv_name_8_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_8_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_9=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleFragment436); 

                	newLeafNode(otherlv_9, grammarAccess.getFragmentAccess().getLeftCurlyBracketKeyword_6());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:278:1: ( (lv_objects_10_0= ruleObject ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:279:1: (lv_objects_10_0= ruleObject )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:279:1: (lv_objects_10_0= ruleObject )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:280:3: lv_objects_10_0= ruleObject
            {
             
            	        newCompositeNode(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_7_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_ruleFragment457);
            lv_objects_10_0=ruleObject();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getFragmentRule());
            	        }
                   		add(
                   			current, 
                   			"objects",
                    		lv_objects_10_0, 
                    		"Object");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:296:2: ( (lv_objects_11_0= ruleObject ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=RULE_STRING && LA6_0<=RULE_ID)||LA6_0==19) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:297:1: (lv_objects_11_0= ruleObject )
            	    {
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:297:1: (lv_objects_11_0= ruleObject )
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:298:3: lv_objects_11_0= ruleObject
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_8_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleObject_in_ruleFragment478);
            	    lv_objects_11_0=ruleObject();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFragmentRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"objects",
            	            		lv_objects_11_0, 
            	            		"Object");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            otherlv_12=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleFragment491); 

                	newLeafNode(otherlv_12, grammarAccess.getFragmentAccess().getRightCurlyBracketKeyword_9());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragment"


    // $ANTLR start "entryRuleFeature"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:326:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:327:2: (iv_ruleFeature= ruleFeature EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:328:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_entryRuleFeature527);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeature537); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:335:1: ruleFeature returns [EObject current=null] : this_AttrOrReference_0= ruleAttrOrReference ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        EObject this_AttrOrReference_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:338:28: (this_AttrOrReference_0= ruleAttrOrReference )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:340:5: this_AttrOrReference_0= ruleAttrOrReference
            {
             
                    newCompositeNode(grammarAccess.getFeatureAccess().getAttrOrReferenceParserRuleCall()); 
                
            pushFollow(FollowSets000.FOLLOW_ruleAttrOrReference_in_ruleFeature583);
            this_AttrOrReference_0=ruleAttrOrReference();

            state._fsp--;

             
                    current = this_AttrOrReference_0; 
                    afterParserOrEnumRuleCall();
                

            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleAttrOrReference"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:356:1: entryRuleAttrOrReference returns [EObject current=null] : iv_ruleAttrOrReference= ruleAttrOrReference EOF ;
    public final EObject entryRuleAttrOrReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttrOrReference = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:357:2: (iv_ruleAttrOrReference= ruleAttrOrReference EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:358:2: iv_ruleAttrOrReference= ruleAttrOrReference EOF
            {
             newCompositeNode(grammarAccess.getAttrOrReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttrOrReference_in_entryRuleAttrOrReference617);
            iv_ruleAttrOrReference=ruleAttrOrReference();

            state._fsp--;

             current =iv_ruleAttrOrReference; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttrOrReference627); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttrOrReference"


    // $ANTLR start "ruleAttrOrReference"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:365:1: ruleAttrOrReference returns [EObject current=null] : (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference ) ;
    public final EObject ruleAttrOrReference() throws RecognitionException {
        EObject current = null;

        EObject this_Attribute_0 = null;

        EObject this_Reference_1 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:368:28: ( (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:369:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:369:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )
            int alt7=2;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:370:5: this_Attribute_0= ruleAttribute
                    {
                     
                            newCompositeNode(grammarAccess.getAttrOrReferenceAccess().getAttributeParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_ruleAttrOrReference674);
                    this_Attribute_0=ruleAttribute();

                    state._fsp--;

                     
                            current = this_Attribute_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:380:5: this_Reference_1= ruleReference
                    {
                     
                            newCompositeNode(grammarAccess.getAttrOrReferenceAccess().getReferenceParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleReference_in_ruleAttrOrReference701);
                    this_Reference_1=ruleReference();

                    state._fsp--;

                     
                            current = this_Reference_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttrOrReference"


    // $ANTLR start "entryRuleEString"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:396:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:397:2: (iv_ruleEString= ruleEString EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:398:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString737);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString748); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:405:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:408:28: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:409:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:409:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_STRING) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_ID) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:409:6: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleEString788); 

                    		current.merge(this_STRING_0);
                        
                     
                        newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:417:10: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleEString814); 

                    		current.merge(this_ID_1);
                        
                     
                        newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleObject"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:432:1: entryRuleObject returns [EObject current=null] : iv_ruleObject= ruleObject EOF ;
    public final EObject entryRuleObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObject = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:433:2: (iv_ruleObject= ruleObject EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:434:2: iv_ruleObject= ruleObject EOF
            {
             newCompositeNode(grammarAccess.getObjectRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_entryRuleObject859);
            iv_ruleObject=ruleObject();

            state._fsp--;

             current =iv_ruleObject; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObject869); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObject"


    // $ANTLR start "ruleObject"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:441:1: ruleObject returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' ) ;
    public final EObject ruleObject() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        AntlrDatatypeRuleToken lv_name_3_0 = null;

        AntlrDatatypeRuleToken lv_type_5_0 = null;

        EObject lv_features_7_0 = null;

        EObject lv_features_9_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:444:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:445:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:445:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}' )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:445:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_name_3_0= ruleEString ) ) otherlv_4= ':' ( (lv_type_5_0= ruleEString ) ) otherlv_6= '{' ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )? otherlv_11= '}'
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:445:2: ()
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:446:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getObjectAccess().getObjectAction_0(),
                        current);
                

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:451:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==19) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:451:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:451:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:452:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:452:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:453:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleObject925);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:469:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==19) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:470:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:470:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:471:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleObject946);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:487:5: ( (lv_name_3_0= ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:488:1: (lv_name_3_0= ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:488:1: (lv_name_3_0= ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:489:3: lv_name_3_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getObjectAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObject970);
            lv_name_3_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getObjectRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_3_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleObject982); 

                	newLeafNode(otherlv_4, grammarAccess.getObjectAccess().getColonKeyword_3());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:509:1: ( (lv_type_5_0= ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:510:1: (lv_type_5_0= ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:510:1: (lv_type_5_0= ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:511:3: lv_type_5_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getObjectAccess().getTypeEStringParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObject1003);
            lv_type_5_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getObjectRule());
            	        }
                   		set(
                   			current, 
                   			"type",
                    		lv_type_5_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_6=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleObject1015); 

                	newLeafNode(otherlv_6, grammarAccess.getObjectAccess().getLeftCurlyBracketKeyword_5());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:531:1: ( ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )* )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==19||(LA14_0>=26 && LA14_0<=28)) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:531:2: ( (lv_features_7_0= ruleFeature ) ) (otherlv_8= ';' )? ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )*
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:531:2: ( (lv_features_7_0= ruleFeature ) )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:532:1: (lv_features_7_0= ruleFeature )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:532:1: (lv_features_7_0= ruleFeature )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:533:3: lv_features_7_0= ruleFeature
                    {
                     
                    	        newCompositeNode(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleObject1037);
                    lv_features_7_0=ruleFeature();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	        }
                           		add(
                           			current, 
                           			"features",
                            		lv_features_7_0, 
                            		"Feature");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:549:2: (otherlv_8= ';' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0==18) ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:549:4: otherlv_8= ';'
                            {
                            otherlv_8=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleObject1050); 

                                	newLeafNode(otherlv_8, grammarAccess.getObjectAccess().getSemicolonKeyword_6_1());
                                

                            }
                            break;

                    }

                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:553:3: ( ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )? )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==19||(LA13_0>=26 && LA13_0<=28)) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:553:4: ( (lv_features_9_0= ruleFeature ) ) (otherlv_10= ';' )?
                    	    {
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:553:4: ( (lv_features_9_0= ruleFeature ) )
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:554:1: (lv_features_9_0= ruleFeature )
                    	    {
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:554:1: (lv_features_9_0= ruleFeature )
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:555:3: lv_features_9_0= ruleFeature
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_2_0_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleObject1074);
                    	    lv_features_9_0=ruleFeature();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getObjectRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"features",
                    	            		lv_features_9_0, 
                    	            		"Feature");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }

                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:571:2: (otherlv_10= ';' )?
                    	    int alt12=2;
                    	    int LA12_0 = input.LA(1);

                    	    if ( (LA12_0==18) ) {
                    	        alt12=1;
                    	    }
                    	    switch (alt12) {
                    	        case 1 :
                    	            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:571:4: otherlv_10= ';'
                    	            {
                    	            otherlv_10=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleObject1087); 

                    	                	newLeafNode(otherlv_10, grammarAccess.getObjectAccess().getSemicolonKeyword_6_2_1());
                    	                

                    	            }
                    	            break;

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_11=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleObject1105); 

                	newLeafNode(otherlv_11, grammarAccess.getObjectAccess().getRightCurlyBracketKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObject"


    // $ANTLR start "entryRuleAnnotation"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:587:1: entryRuleAnnotation returns [EObject current=null] : iv_ruleAnnotation= ruleAnnotation EOF ;
    public final EObject entryRuleAnnotation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotation = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:588:2: (iv_ruleAnnotation= ruleAnnotation EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:589:2: iv_ruleAnnotation= ruleAnnotation EOF
            {
             newCompositeNode(grammarAccess.getAnnotationRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_entryRuleAnnotation1141);
            iv_ruleAnnotation=ruleAnnotation();

            state._fsp--;

             current =iv_ruleAnnotation; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotation1151); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotation"


    // $ANTLR start "ruleAnnotation"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:596:1: ruleAnnotation returns [EObject current=null] : (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? ) ;
    public final EObject ruleAnnotation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_params_3_0 = null;

        EObject lv_params_5_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:599:28: ( (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:600:1: (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:600:1: (otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )? )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:600:3: otherlv_0= '@' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )?
            {
            otherlv_0=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleAnnotation1188); 

                	newLeafNode(otherlv_0, grammarAccess.getAnnotationAccess().getCommercialAtKeyword_0());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:604:1: ( (lv_name_1_0= ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:605:1: (lv_name_1_0= ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:605:1: (lv_name_1_0= ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:606:3: lv_name_1_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAnnotationAccess().getNameEStringParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAnnotation1209);
            lv_name_1_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:622:2: (otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')' )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==20) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:622:4: otherlv_2= '(' ( (lv_params_3_0= ruleAnnotationParam ) ) (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )* otherlv_6= ')'
                    {
                    otherlv_2=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleAnnotation1222); 

                        	newLeafNode(otherlv_2, grammarAccess.getAnnotationAccess().getLeftParenthesisKeyword_2_0());
                        
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:626:1: ( (lv_params_3_0= ruleAnnotationParam ) )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:627:1: (lv_params_3_0= ruleAnnotationParam )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:627:1: (lv_params_3_0= ruleAnnotationParam )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:628:3: lv_params_3_0= ruleAnnotationParam
                    {
                     
                    	        newCompositeNode(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_ruleAnnotation1243);
                    lv_params_3_0=ruleAnnotationParam();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
                    	        }
                           		add(
                           			current, 
                           			"params",
                            		lv_params_3_0, 
                            		"AnnotationParam");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:644:2: (otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==21) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:644:4: otherlv_4= ',' ( (lv_params_5_0= ruleAnnotationParam ) )
                    	    {
                    	    otherlv_4=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleAnnotation1256); 

                    	        	newLeafNode(otherlv_4, grammarAccess.getAnnotationAccess().getCommaKeyword_2_2_0());
                    	        
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:648:1: ( (lv_params_5_0= ruleAnnotationParam ) )
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:649:1: (lv_params_5_0= ruleAnnotationParam )
                    	    {
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:649:1: (lv_params_5_0= ruleAnnotationParam )
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:650:3: lv_params_5_0= ruleAnnotationParam
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_2_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_ruleAnnotation1277);
                    	    lv_params_5_0=ruleAnnotationParam();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAnnotationRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"params",
                    	            		lv_params_5_0, 
                    	            		"AnnotationParam");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);

                    otherlv_6=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleAnnotation1291); 

                        	newLeafNode(otherlv_6, grammarAccess.getAnnotationAccess().getRightParenthesisKeyword_2_3());
                        

                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotation"


    // $ANTLR start "entryRuleAnnotationParam"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:678:1: entryRuleAnnotationParam returns [EObject current=null] : iv_ruleAnnotationParam= ruleAnnotationParam EOF ;
    public final EObject entryRuleAnnotationParam() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnnotationParam = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:679:2: (iv_ruleAnnotationParam= ruleAnnotationParam EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:680:2: iv_ruleAnnotationParam= ruleAnnotationParam EOF
            {
             newCompositeNode(grammarAccess.getAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam1329);
            iv_ruleAnnotationParam=ruleAnnotationParam();

            state._fsp--;

             current =iv_ruleAnnotationParam; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotationParam1339); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnnotationParam"


    // $ANTLR start "ruleAnnotationParam"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:687:1: ruleAnnotationParam returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) ) ;
    public final EObject ruleAnnotationParam() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;

        EObject lv_paramValue_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:690:28: ( ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:691:1: ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:691:1: ( ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:691:2: ( (lv_name_0_0= ruleEString ) ) ( (lv_paramValue_1_0= ruleParamValue ) )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:691:2: ( (lv_name_0_0= ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:692:1: (lv_name_0_0= ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:692:1: (lv_name_0_0= ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:693:3: lv_name_0_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAnnotationParamAccess().getNameEStringParserRuleCall_0_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAnnotationParam1385);
            lv_name_0_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAnnotationParamRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_0_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:709:2: ( (lv_paramValue_1_0= ruleParamValue ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:710:1: (lv_paramValue_1_0= ruleParamValue )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:710:1: (lv_paramValue_1_0= ruleParamValue )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:711:3: lv_paramValue_1_0= ruleParamValue
            {
             
            	        newCompositeNode(grammarAccess.getAnnotationParamAccess().getParamValueParamValueParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_ruleAnnotationParam1406);
            lv_paramValue_1_0=ruleParamValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAnnotationParamRule());
            	        }
                   		set(
                   			current, 
                   			"paramValue",
                    		lv_paramValue_1_0, 
                    		"ParamValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnnotationParam"


    // $ANTLR start "entryRuleParamValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:735:1: entryRuleParamValue returns [EObject current=null] : iv_ruleParamValue= ruleParamValue EOF ;
    public final EObject entryRuleParamValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParamValue = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:736:2: (iv_ruleParamValue= ruleParamValue EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:737:2: iv_ruleParamValue= ruleParamValue EOF
            {
             newCompositeNode(grammarAccess.getParamValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_entryRuleParamValue1442);
            iv_ruleParamValue=ruleParamValue();

            state._fsp--;

             current =iv_ruleParamValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleParamValue1452); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParamValue"


    // $ANTLR start "ruleParamValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:744:1: ruleParamValue returns [EObject current=null] : (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam ) ;
    public final EObject ruleParamValue() throws RecognitionException {
        EObject current = null;

        EObject this_PrimitiveValueAnnotationParam_0 = null;

        EObject this_ObjectValueAnnotationParam_1 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:747:28: ( (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:748:1: (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:748:1: (this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam | this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==25) ) {
                alt17=1;
            }
            else if ( (LA17_0==23) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:749:5: this_PrimitiveValueAnnotationParam_0= rulePrimitiveValueAnnotationParam
                    {
                     
                            newCompositeNode(grammarAccess.getParamValueAccess().getPrimitiveValueAnnotationParamParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_ruleParamValue1499);
                    this_PrimitiveValueAnnotationParam_0=rulePrimitiveValueAnnotationParam();

                    state._fsp--;

                     
                            current = this_PrimitiveValueAnnotationParam_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:759:5: this_ObjectValueAnnotationParam_1= ruleObjectValueAnnotationParam
                    {
                     
                            newCompositeNode(grammarAccess.getParamValueAccess().getObjectValueAnnotationParamParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_ruleParamValue1526);
                    this_ObjectValueAnnotationParam_1=ruleObjectValueAnnotationParam();

                    state._fsp--;

                     
                            current = this_ObjectValueAnnotationParam_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParamValue"


    // $ANTLR start "entryRuleObjectValueAnnotationParam"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:775:1: entryRuleObjectValueAnnotationParam returns [EObject current=null] : iv_ruleObjectValueAnnotationParam= ruleObjectValueAnnotationParam EOF ;
    public final EObject entryRuleObjectValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObjectValueAnnotationParam = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:776:2: (iv_ruleObjectValueAnnotationParam= ruleObjectValueAnnotationParam EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:777:2: iv_ruleObjectValueAnnotationParam= ruleObjectValueAnnotationParam EOF
            {
             newCompositeNode(grammarAccess.getObjectValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam1561);
            iv_ruleObjectValueAnnotationParam=ruleObjectValueAnnotationParam();

            state._fsp--;

             current =iv_ruleObjectValueAnnotationParam; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam1571); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObjectValueAnnotationParam"


    // $ANTLR start "ruleObjectValueAnnotationParam"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:784:1: ruleObjectValueAnnotationParam returns [EObject current=null] : (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? ) ;
    public final EObject ruleObjectValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:787:28: ( (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:788:1: (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:788:1: (otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )? )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:788:3: otherlv_0= '->' ( ( ruleEString ) ) (otherlv_2= '.' ( ( ruleEString ) ) )?
            {
            otherlv_0=(Token)match(input,23,FollowSets000.FOLLOW_23_in_ruleObjectValueAnnotationParam1608); 

                	newLeafNode(otherlv_0, grammarAccess.getObjectValueAnnotationParamAccess().getHyphenMinusGreaterThanSignKeyword_0());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:792:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:793:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:793:1: ( ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:794:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getObjectValueAnnotationParamRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectCrossReference_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam1631);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:807:2: (otherlv_2= '.' ( ( ruleEString ) ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==24) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:807:4: otherlv_2= '.' ( ( ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,24,FollowSets000.FOLLOW_24_in_ruleObjectValueAnnotationParam1644); 

                        	newLeafNode(otherlv_2, grammarAccess.getObjectValueAnnotationParamAccess().getFullStopKeyword_2_0());
                        
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:811:1: ( ( ruleEString ) )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:812:1: ( ruleEString )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:812:1: ( ruleEString )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:813:3: ruleEString
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getObjectValueAnnotationParamRule());
                    	        }
                            
                     
                    	        newCompositeNode(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureCrossReference_2_1_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam1667);
                    ruleEString();

                    state._fsp--;

                     
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectValueAnnotationParam"


    // $ANTLR start "entryRulePrimitiveValueAnnotationParam"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:834:1: entryRulePrimitiveValueAnnotationParam returns [EObject current=null] : iv_rulePrimitiveValueAnnotationParam= rulePrimitiveValueAnnotationParam EOF ;
    public final EObject entryRulePrimitiveValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveValueAnnotationParam = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:835:2: (iv_rulePrimitiveValueAnnotationParam= rulePrimitiveValueAnnotationParam EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:836:2: iv_rulePrimitiveValueAnnotationParam= rulePrimitiveValueAnnotationParam EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam1705);
            iv_rulePrimitiveValueAnnotationParam=rulePrimitiveValueAnnotationParam();

            state._fsp--;

             current =iv_rulePrimitiveValueAnnotationParam; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam1715); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveValueAnnotationParam"


    // $ANTLR start "rulePrimitiveValueAnnotationParam"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:843:1: rulePrimitiveValueAnnotationParam returns [EObject current=null] : (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) ) ;
    public final EObject rulePrimitiveValueAnnotationParam() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:846:28: ( (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:847:1: (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:847:1: (otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:847:3: otherlv_0= '=' ( (lv_value_1_0= rulePrimitiveValue ) )
            {
            otherlv_0=(Token)match(input,25,FollowSets000.FOLLOW_25_in_rulePrimitiveValueAnnotationParam1752); 

                	newLeafNode(otherlv_0, grammarAccess.getPrimitiveValueAnnotationParamAccess().getEqualsSignKeyword_0());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:851:1: ( (lv_value_1_0= rulePrimitiveValue ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:852:1: (lv_value_1_0= rulePrimitiveValue )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:852:1: (lv_value_1_0= rulePrimitiveValue )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:853:3: lv_value_1_0= rulePrimitiveValue
            {
             
            	        newCompositeNode(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValuePrimitiveValueParserRuleCall_1_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_rulePrimitiveValueAnnotationParam1773);
            lv_value_1_0=rulePrimitiveValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPrimitiveValueAnnotationParamRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_1_0, 
                    		"PrimitiveValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveValueAnnotationParam"


    // $ANTLR start "entryRuleAttribute"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:877:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:878:2: (iv_ruleAttribute= ruleAttribute EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:879:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_entryRuleAttribute1809);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttribute1819); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:886:1: ruleAttribute returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        AntlrDatatypeRuleToken lv_name_4_0 = null;

        EObject lv_values_6_0 = null;

        EObject lv_values_8_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:889:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:890:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:890:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )* )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:890:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? otherlv_3= 'attr' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '=' ( (lv_values_6_0= rulePrimitiveValue ) ) (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )*
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:890:2: ()
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:891:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAttributeAccess().getAttributeAction_0(),
                        current);
                

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:896:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==19) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:896:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:896:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:897:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:897:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:898:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleAttribute1875);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:914:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==19) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:915:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:915:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:916:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleAttribute1896);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop19;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_3=(Token)match(input,26,FollowSets000.FOLLOW_26_in_ruleAttribute1911); 

                	newLeafNode(otherlv_3, grammarAccess.getAttributeAccess().getAttrKeyword_2());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:936:1: ( (lv_name_4_0= ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:937:1: (lv_name_4_0= ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:937:1: (lv_name_4_0= ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:938:3: lv_name_4_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAttribute1932);
            lv_name_4_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_4_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_5=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleAttribute1944); 

                	newLeafNode(otherlv_5, grammarAccess.getAttributeAccess().getEqualsSignKeyword_4());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:958:1: ( (lv_values_6_0= rulePrimitiveValue ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:959:1: (lv_values_6_0= rulePrimitiveValue )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:959:1: (lv_values_6_0= rulePrimitiveValue )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:960:3: lv_values_6_0= rulePrimitiveValue
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_5_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_ruleAttribute1965);
            lv_values_6_0=rulePrimitiveValue();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		add(
                   			current, 
                   			"values",
                    		lv_values_6_0, 
                    		"PrimitiveValue");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:976:2: (otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==21) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:976:4: otherlv_7= ',' ( (lv_values_8_0= rulePrimitiveValue ) )
            	    {
            	    otherlv_7=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleAttribute1978); 

            	        	newLeafNode(otherlv_7, grammarAccess.getAttributeAccess().getCommaKeyword_6_0());
            	        
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:980:1: ( (lv_values_8_0= rulePrimitiveValue ) )
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:981:1: (lv_values_8_0= rulePrimitiveValue )
            	    {
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:981:1: (lv_values_8_0= rulePrimitiveValue )
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:982:3: lv_values_8_0= rulePrimitiveValue
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_6_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_ruleAttribute1999);
            	    lv_values_8_0=rulePrimitiveValue();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"values",
            	            		lv_values_8_0, 
            	            		"PrimitiveValue");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleReference"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1006:1: entryRuleReference returns [EObject current=null] : iv_ruleReference= ruleReference EOF ;
    public final EObject entryRuleReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReference = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1007:2: (iv_ruleReference= ruleReference EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1008:2: iv_ruleReference= ruleReference EOF
            {
             newCompositeNode(grammarAccess.getReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReference_in_entryRuleReference2037);
            iv_ruleReference=ruleReference();

            state._fsp--;

             current =iv_ruleReference; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReference2047); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1015:1: ruleReference returns [EObject current=null] : ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* ) ;
    public final EObject ruleReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_annotation_1_0 = null;

        EObject lv_annotation_2_0 = null;

        EObject lv_graphicProperties_3_0 = null;

        AntlrDatatypeRuleToken lv_name_6_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1018:28: ( ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1019:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1019:1: ( () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )* )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1019:2: () ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )? ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )? (otherlv_4= 'ref' | otherlv_5= 'rel' ) ( (lv_name_6_0= ruleEString ) ) otherlv_7= '=' ( ( ruleEString ) ) (otherlv_9= ',' ( ( ruleEString ) ) )*
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1019:2: ()
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1020:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getReferenceAccess().getReferenceAction_0(),
                        current);
                

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1025:2: ( ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )* )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==19) ) {
                int LA23_1 = input.LA(2);

                if ( ((LA23_1>=RULE_STRING && LA23_1<=RULE_ID)) ) {
                    alt23=1;
                }
            }
            switch (alt23) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1025:3: ( (lv_annotation_1_0= ruleAnnotation ) ) ( (lv_annotation_2_0= ruleAnnotation ) )*
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1025:3: ( (lv_annotation_1_0= ruleAnnotation ) )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1026:1: (lv_annotation_1_0= ruleAnnotation )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1026:1: (lv_annotation_1_0= ruleAnnotation )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1027:3: lv_annotation_1_0= ruleAnnotation
                    {
                     
                    	        newCompositeNode(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleReference2103);
                    lv_annotation_1_0=ruleAnnotation();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReferenceRule());
                    	        }
                           		add(
                           			current, 
                           			"annotation",
                            		lv_annotation_1_0, 
                            		"Annotation");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }

                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1043:2: ( (lv_annotation_2_0= ruleAnnotation ) )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==19) ) {
                            int LA22_1 = input.LA(2);

                            if ( ((LA22_1>=RULE_STRING && LA22_1<=RULE_ID)) ) {
                                alt22=1;
                            }


                        }


                        switch (alt22) {
                    	case 1 :
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1044:1: (lv_annotation_2_0= ruleAnnotation )
                    	    {
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1044:1: (lv_annotation_2_0= ruleAnnotation )
                    	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1045:3: lv_annotation_2_0= ruleAnnotation
                    	    {
                    	     
                    	    	        newCompositeNode(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
                    	    	    
                    	    pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_ruleReference2124);
                    	    lv_annotation_2_0=ruleAnnotation();

                    	    state._fsp--;


                    	    	        if (current==null) {
                    	    	            current = createModelElementForParent(grammarAccess.getReferenceRule());
                    	    	        }
                    	           		add(
                    	           			current, 
                    	           			"annotation",
                    	            		lv_annotation_2_0, 
                    	            		"Annotation");
                    	    	        afterParserOrEnumRuleCall();
                    	    	    

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);


                    }
                    break;

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1061:5: ( (lv_graphicProperties_3_0= ruleEdgeProperties ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==19) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1062:1: (lv_graphicProperties_3_0= ruleEdgeProperties )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1062:1: (lv_graphicProperties_3_0= ruleEdgeProperties )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1063:3: lv_graphicProperties_3_0= ruleEdgeProperties
                    {
                     
                    	        newCompositeNode(grammarAccess.getReferenceAccess().getGraphicPropertiesEdgePropertiesParserRuleCall_2_0()); 
                    	    
                    pushFollow(FollowSets000.FOLLOW_ruleEdgeProperties_in_ruleReference2148);
                    lv_graphicProperties_3_0=ruleEdgeProperties();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getReferenceRule());
                    	        }
                           		set(
                           			current, 
                           			"graphicProperties",
                            		lv_graphicProperties_3_0, 
                            		"EdgeProperties");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1079:3: (otherlv_4= 'ref' | otherlv_5= 'rel' )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==27) ) {
                alt25=1;
            }
            else if ( (LA25_0==28) ) {
                alt25=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }
            switch (alt25) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1079:5: otherlv_4= 'ref'
                    {
                    otherlv_4=(Token)match(input,27,FollowSets000.FOLLOW_27_in_ruleReference2162); 

                        	newLeafNode(otherlv_4, grammarAccess.getReferenceAccess().getRefKeyword_3_0());
                        

                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1084:7: otherlv_5= 'rel'
                    {
                    otherlv_5=(Token)match(input,28,FollowSets000.FOLLOW_28_in_ruleReference2180); 

                        	newLeafNode(otherlv_5, grammarAccess.getReferenceAccess().getRelKeyword_3_1());
                        

                    }
                    break;

            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1088:2: ( (lv_name_6_0= ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1089:1: (lv_name_6_0= ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1089:1: (lv_name_6_0= ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1090:3: lv_name_6_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference2202);
            lv_name_6_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReferenceRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_6_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_7=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleReference2214); 

                	newLeafNode(otherlv_7, grammarAccess.getReferenceAccess().getEqualsSignKeyword_5());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1110:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1111:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1111:1: ( ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1112:3: ruleEString
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getReferenceRule());
            	        }
                    
             
            	        newCompositeNode(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_6_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference2237);
            ruleEString();

            state._fsp--;

             
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1125:2: (otherlv_9= ',' ( ( ruleEString ) ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==21) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1125:4: otherlv_9= ',' ( ( ruleEString ) )
            	    {
            	    otherlv_9=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleReference2250); 

            	        	newLeafNode(otherlv_9, grammarAccess.getReferenceAccess().getCommaKeyword_7_0());
            	        
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1129:1: ( ( ruleEString ) )
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1130:1: ( ruleEString )
            	    {
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1130:1: ( ruleEString )
            	    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1131:3: ruleEString
            	    {

            	    			if (current==null) {
            	    	            current = createModelElement(grammarAccess.getReferenceRule());
            	    	        }
            	            
            	     
            	    	        newCompositeNode(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_7_1_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference2273);
            	    ruleEString();

            	    state._fsp--;

            	     
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleEdgeProperties"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1152:1: entryRuleEdgeProperties returns [EObject current=null] : iv_ruleEdgeProperties= ruleEdgeProperties EOF ;
    public final EObject entryRuleEdgeProperties() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEdgeProperties = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1153:2: (iv_ruleEdgeProperties= ruleEdgeProperties EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1154:2: iv_ruleEdgeProperties= ruleEdgeProperties EOF
            {
             newCompositeNode(grammarAccess.getEdgePropertiesRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEdgeProperties_in_entryRuleEdgeProperties2311);
            iv_ruleEdgeProperties=ruleEdgeProperties();

            state._fsp--;

             current =iv_ruleEdgeProperties; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEdgeProperties2321); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEdgeProperties"


    // $ANTLR start "ruleEdgeProperties"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1161:1: ruleEdgeProperties returns [EObject current=null] : ( () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')' ) ;
    public final EObject ruleEdgeProperties() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token lv_width_10_0=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        AntlrDatatypeRuleToken lv_color_6_0 = null;

        Enumerator lv_lineType_14_0 = null;

        Enumerator lv_srcArrow_18_0 = null;

        Enumerator lv_tgtArrow_22_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1164:28: ( ( () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')' ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1165:1: ( () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')' )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1165:1: ( () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')' )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1165:2: () otherlv_1= '@' otherlv_2= 'style' otherlv_3= '(' otherlv_4= 'color' otherlv_5= '=' ( (lv_color_6_0= ruleEString ) ) otherlv_7= ',' otherlv_8= 'width' otherlv_9= '=' ( (lv_width_10_0= RULE_INT ) ) otherlv_11= ',' otherlv_12= 'line' otherlv_13= '=' ( (lv_lineType_14_0= ruleLineType ) ) otherlv_15= ',' otherlv_16= 'source' otherlv_17= '=' ( (lv_srcArrow_18_0= ruleArrowType ) ) otherlv_19= ',' otherlv_20= 'target' otherlv_21= '=' ( (lv_tgtArrow_22_0= ruleArrowType ) ) otherlv_23= ')'
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1165:2: ()
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1166:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getEdgePropertiesAccess().getEdgePropertiesAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleEdgeProperties2367); 

                	newLeafNode(otherlv_1, grammarAccess.getEdgePropertiesAccess().getCommercialAtKeyword_1());
                
            otherlv_2=(Token)match(input,29,FollowSets000.FOLLOW_29_in_ruleEdgeProperties2379); 

                	newLeafNode(otherlv_2, grammarAccess.getEdgePropertiesAccess().getStyleKeyword_2());
                
            otherlv_3=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleEdgeProperties2391); 

                	newLeafNode(otherlv_3, grammarAccess.getEdgePropertiesAccess().getLeftParenthesisKeyword_3());
                
            otherlv_4=(Token)match(input,30,FollowSets000.FOLLOW_30_in_ruleEdgeProperties2403); 

                	newLeafNode(otherlv_4, grammarAccess.getEdgePropertiesAccess().getColorKeyword_4());
                
            otherlv_5=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties2415); 

                	newLeafNode(otherlv_5, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_5());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1191:1: ( (lv_color_6_0= ruleEString ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1192:1: (lv_color_6_0= ruleEString )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1192:1: (lv_color_6_0= ruleEString )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1193:3: lv_color_6_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getEdgePropertiesAccess().getColorEStringParserRuleCall_6_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleEdgeProperties2436);
            lv_color_6_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEdgePropertiesRule());
            	        }
                   		set(
                   			current, 
                   			"color",
                    		lv_color_6_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_7=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleEdgeProperties2448); 

                	newLeafNode(otherlv_7, grammarAccess.getEdgePropertiesAccess().getCommaKeyword_7());
                
            otherlv_8=(Token)match(input,31,FollowSets000.FOLLOW_31_in_ruleEdgeProperties2460); 

                	newLeafNode(otherlv_8, grammarAccess.getEdgePropertiesAccess().getWidthKeyword_8());
                
            otherlv_9=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties2472); 

                	newLeafNode(otherlv_9, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_9());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1221:1: ( (lv_width_10_0= RULE_INT ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1222:1: (lv_width_10_0= RULE_INT )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1222:1: (lv_width_10_0= RULE_INT )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1223:3: lv_width_10_0= RULE_INT
            {
            lv_width_10_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleEdgeProperties2489); 

            			newLeafNode(lv_width_10_0, grammarAccess.getEdgePropertiesAccess().getWidthINTTerminalRuleCall_10_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getEdgePropertiesRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"width",
                    		lv_width_10_0, 
                    		"INT");
            	    

            }


            }

            otherlv_11=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleEdgeProperties2506); 

                	newLeafNode(otherlv_11, grammarAccess.getEdgePropertiesAccess().getCommaKeyword_11());
                
            otherlv_12=(Token)match(input,32,FollowSets000.FOLLOW_32_in_ruleEdgeProperties2518); 

                	newLeafNode(otherlv_12, grammarAccess.getEdgePropertiesAccess().getLineKeyword_12());
                
            otherlv_13=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties2530); 

                	newLeafNode(otherlv_13, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_13());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1251:1: ( (lv_lineType_14_0= ruleLineType ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1252:1: (lv_lineType_14_0= ruleLineType )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1252:1: (lv_lineType_14_0= ruleLineType )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1253:3: lv_lineType_14_0= ruleLineType
            {
             
            	        newCompositeNode(grammarAccess.getEdgePropertiesAccess().getLineTypeLineTypeEnumRuleCall_14_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleLineType_in_ruleEdgeProperties2551);
            lv_lineType_14_0=ruleLineType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEdgePropertiesRule());
            	        }
                   		set(
                   			current, 
                   			"lineType",
                    		lv_lineType_14_0, 
                    		"LineType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_15=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleEdgeProperties2563); 

                	newLeafNode(otherlv_15, grammarAccess.getEdgePropertiesAccess().getCommaKeyword_15());
                
            otherlv_16=(Token)match(input,33,FollowSets000.FOLLOW_33_in_ruleEdgeProperties2575); 

                	newLeafNode(otherlv_16, grammarAccess.getEdgePropertiesAccess().getSourceKeyword_16());
                
            otherlv_17=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties2587); 

                	newLeafNode(otherlv_17, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_17());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1281:1: ( (lv_srcArrow_18_0= ruleArrowType ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1282:1: (lv_srcArrow_18_0= ruleArrowType )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1282:1: (lv_srcArrow_18_0= ruleArrowType )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1283:3: lv_srcArrow_18_0= ruleArrowType
            {
             
            	        newCompositeNode(grammarAccess.getEdgePropertiesAccess().getSrcArrowArrowTypeEnumRuleCall_18_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleArrowType_in_ruleEdgeProperties2608);
            lv_srcArrow_18_0=ruleArrowType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEdgePropertiesRule());
            	        }
                   		set(
                   			current, 
                   			"srcArrow",
                    		lv_srcArrow_18_0, 
                    		"ArrowType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_19=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleEdgeProperties2620); 

                	newLeafNode(otherlv_19, grammarAccess.getEdgePropertiesAccess().getCommaKeyword_19());
                
            otherlv_20=(Token)match(input,34,FollowSets000.FOLLOW_34_in_ruleEdgeProperties2632); 

                	newLeafNode(otherlv_20, grammarAccess.getEdgePropertiesAccess().getTargetKeyword_20());
                
            otherlv_21=(Token)match(input,25,FollowSets000.FOLLOW_25_in_ruleEdgeProperties2644); 

                	newLeafNode(otherlv_21, grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_21());
                
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1311:1: ( (lv_tgtArrow_22_0= ruleArrowType ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1312:1: (lv_tgtArrow_22_0= ruleArrowType )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1312:1: (lv_tgtArrow_22_0= ruleArrowType )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1313:3: lv_tgtArrow_22_0= ruleArrowType
            {
             
            	        newCompositeNode(grammarAccess.getEdgePropertiesAccess().getTgtArrowArrowTypeEnumRuleCall_22_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleArrowType_in_ruleEdgeProperties2665);
            lv_tgtArrow_22_0=ruleArrowType();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getEdgePropertiesRule());
            	        }
                   		set(
                   			current, 
                   			"tgtArrow",
                    		lv_tgtArrow_22_0, 
                    		"ArrowType");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_23=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleEdgeProperties2677); 

                	newLeafNode(otherlv_23, grammarAccess.getEdgePropertiesAccess().getRightParenthesisKeyword_23());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEdgeProperties"


    // $ANTLR start "entryRulePrimitiveValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1341:1: entryRulePrimitiveValue returns [EObject current=null] : iv_rulePrimitiveValue= rulePrimitiveValue EOF ;
    public final EObject entryRulePrimitiveValue() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePrimitiveValue = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1342:2: (iv_rulePrimitiveValue= rulePrimitiveValue EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1343:2: iv_rulePrimitiveValue= rulePrimitiveValue EOF
            {
             newCompositeNode(grammarAccess.getPrimitiveValueRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue2713);
            iv_rulePrimitiveValue=rulePrimitiveValue();

            state._fsp--;

             current =iv_rulePrimitiveValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValue2723); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePrimitiveValue"


    // $ANTLR start "rulePrimitiveValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1350:1: rulePrimitiveValue returns [EObject current=null] : (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue ) ;
    public final EObject rulePrimitiveValue() throws RecognitionException {
        EObject current = null;

        EObject this_StringValue_0 = null;

        EObject this_IntegerValue_1 = null;

        EObject this_BooleanValue_2 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1353:28: ( (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1354:1: (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1354:1: (this_StringValue_0= ruleStringValue | this_IntegerValue_1= ruleIntegerValue | this_BooleanValue_2= ruleBooleanValue )
            int alt27=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt27=1;
                }
                break;
            case RULE_INT:
                {
                alt27=2;
                }
                break;
            case 35:
            case 36:
                {
                alt27=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1355:5: this_StringValue_0= ruleStringValue
                    {
                     
                            newCompositeNode(grammarAccess.getPrimitiveValueAccess().getStringValueParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_rulePrimitiveValue2770);
                    this_StringValue_0=ruleStringValue();

                    state._fsp--;

                     
                            current = this_StringValue_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1365:5: this_IntegerValue_1= ruleIntegerValue
                    {
                     
                            newCompositeNode(grammarAccess.getPrimitiveValueAccess().getIntegerValueParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_rulePrimitiveValue2797);
                    this_IntegerValue_1=ruleIntegerValue();

                    state._fsp--;

                     
                            current = this_IntegerValue_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1375:5: this_BooleanValue_2= ruleBooleanValue
                    {
                     
                            newCompositeNode(grammarAccess.getPrimitiveValueAccess().getBooleanValueParserRuleCall_2()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_rulePrimitiveValue2824);
                    this_BooleanValue_2=ruleBooleanValue();

                    state._fsp--;

                     
                            current = this_BooleanValue_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePrimitiveValue"


    // $ANTLR start "entryRuleIntegerValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1391:1: entryRuleIntegerValue returns [EObject current=null] : iv_ruleIntegerValue= ruleIntegerValue EOF ;
    public final EObject entryRuleIntegerValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntegerValue = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1392:2: (iv_ruleIntegerValue= ruleIntegerValue EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1393:2: iv_ruleIntegerValue= ruleIntegerValue EOF
            {
             newCompositeNode(grammarAccess.getIntegerValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue2859);
            iv_ruleIntegerValue=ruleIntegerValue();

            state._fsp--;

             current =iv_ruleIntegerValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntegerValue2869); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1400:1: ruleIntegerValue returns [EObject current=null] : ( (lv_value_0_0= RULE_INT ) ) ;
    public final EObject ruleIntegerValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1403:28: ( ( (lv_value_0_0= RULE_INT ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1404:1: ( (lv_value_0_0= RULE_INT ) )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1404:1: ( (lv_value_0_0= RULE_INT ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1405:1: (lv_value_0_0= RULE_INT )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1405:1: (lv_value_0_0= RULE_INT )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1406:3: lv_value_0_0= RULE_INT
            {
            lv_value_0_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleIntegerValue2910); 

            			newLeafNode(lv_value_0_0, grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getIntegerValueRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"INT");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleStringValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1430:1: entryRuleStringValue returns [EObject current=null] : iv_ruleStringValue= ruleStringValue EOF ;
    public final EObject entryRuleStringValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringValue = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1431:2: (iv_ruleStringValue= ruleStringValue EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1432:2: iv_ruleStringValue= ruleStringValue EOF
            {
             newCompositeNode(grammarAccess.getStringValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_entryRuleStringValue2950);
            iv_ruleStringValue=ruleStringValue();

            state._fsp--;

             current =iv_ruleStringValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringValue2960); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1439:1: ruleStringValue returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1442:28: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1443:1: ( (lv_value_0_0= RULE_STRING ) )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1443:1: ( (lv_value_0_0= RULE_STRING ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1444:1: (lv_value_0_0= RULE_STRING )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1444:1: (lv_value_0_0= RULE_STRING )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1445:3: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleStringValue3001); 

            			newLeafNode(lv_value_0_0, grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getStringValueRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"STRING");
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleBooleanValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1469:1: entryRuleBooleanValue returns [EObject current=null] : iv_ruleBooleanValue= ruleBooleanValue EOF ;
    public final EObject entryRuleBooleanValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanValue = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1470:2: (iv_ruleBooleanValue= ruleBooleanValue EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1471:2: iv_ruleBooleanValue= ruleBooleanValue EOF
            {
             newCompositeNode(grammarAccess.getBooleanValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue3041);
            iv_ruleBooleanValue=ruleBooleanValue();

            state._fsp--;

             current =iv_ruleBooleanValue; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValue3051); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1478:1: ruleBooleanValue returns [EObject current=null] : ( (lv_value_0_0= ruleBooleanValueTerminal ) ) ;
    public final EObject ruleBooleanValue() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_value_0_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1481:28: ( ( (lv_value_0_0= ruleBooleanValueTerminal ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1482:1: ( (lv_value_0_0= ruleBooleanValueTerminal ) )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1482:1: ( (lv_value_0_0= ruleBooleanValueTerminal ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1483:1: (lv_value_0_0= ruleBooleanValueTerminal )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1483:1: (lv_value_0_0= ruleBooleanValueTerminal )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1484:3: lv_value_0_0= ruleBooleanValueTerminal
            {
             
            	        newCompositeNode(grammarAccess.getBooleanValueAccess().getValueBooleanValueTerminalParserRuleCall_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_ruleBooleanValue3096);
            lv_value_0_0=ruleBooleanValueTerminal();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getBooleanValueRule());
            	        }
                   		set(
                   			current, 
                   			"value",
                    		lv_value_0_0, 
                    		"BooleanValueTerminal");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleBooleanValueTerminal"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1508:1: entryRuleBooleanValueTerminal returns [String current=null] : iv_ruleBooleanValueTerminal= ruleBooleanValueTerminal EOF ;
    public final String entryRuleBooleanValueTerminal() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBooleanValueTerminal = null;


        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1509:2: (iv_ruleBooleanValueTerminal= ruleBooleanValueTerminal EOF )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1510:2: iv_ruleBooleanValueTerminal= ruleBooleanValueTerminal EOF
            {
             newCompositeNode(grammarAccess.getBooleanValueTerminalRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal3132);
            iv_ruleBooleanValueTerminal=ruleBooleanValueTerminal();

            state._fsp--;

             current =iv_ruleBooleanValueTerminal.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValueTerminal3143); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanValueTerminal"


    // $ANTLR start "ruleBooleanValueTerminal"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1517:1: ruleBooleanValueTerminal returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleBooleanValueTerminal() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;

         enterRule(); 
            
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1520:28: ( (kw= 'true' | kw= 'false' ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1521:1: (kw= 'true' | kw= 'false' )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1521:1: (kw= 'true' | kw= 'false' )
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==35) ) {
                alt28=1;
            }
            else if ( (LA28_0==36) ) {
                alt28=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 28, 0, input);

                throw nvae;
            }
            switch (alt28) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1522:2: kw= 'true'
                    {
                    kw=(Token)match(input,35,FollowSets000.FOLLOW_35_in_ruleBooleanValueTerminal3181); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getBooleanValueTerminalAccess().getTrueKeyword_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1529:2: kw= 'false'
                    {
                    kw=(Token)match(input,36,FollowSets000.FOLLOW_36_in_ruleBooleanValueTerminal3200); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getBooleanValueTerminalAccess().getFalseKeyword_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanValueTerminal"


    // $ANTLR start "ruleFragmentType"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1542:1: ruleFragmentType returns [Enumerator current=null] : ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) ) ;
    public final Enumerator ruleFragmentType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;

         enterRule(); 
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1544:28: ( ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1545:1: ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1545:1: ( (enumLiteral_0= 'positive' ) | (enumLiteral_1= 'negative' ) )
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==37) ) {
                alt29=1;
            }
            else if ( (LA29_0==38) ) {
                alt29=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 29, 0, input);

                throw nvae;
            }
            switch (alt29) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1545:2: (enumLiteral_0= 'positive' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1545:2: (enumLiteral_0= 'positive' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1545:4: enumLiteral_0= 'positive'
                    {
                    enumLiteral_0=(Token)match(input,37,FollowSets000.FOLLOW_37_in_ruleFragmentType3254); 

                            current = grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1551:6: (enumLiteral_1= 'negative' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1551:6: (enumLiteral_1= 'negative' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1551:8: enumLiteral_1= 'negative'
                    {
                    enumLiteral_1=(Token)match(input,38,FollowSets000.FOLLOW_38_in_ruleFragmentType3271); 

                            current = grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFragmentType"


    // $ANTLR start "ruleLineType"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1561:1: ruleLineType returns [Enumerator current=null] : ( (enumLiteral_0= 'line' ) | (enumLiteral_1= 'dashed' ) | (enumLiteral_2= 'dashed-dotted' ) | (enumLiteral_3= 'dotted' ) ) ;
    public final Enumerator ruleLineType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;

         enterRule(); 
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1563:28: ( ( (enumLiteral_0= 'line' ) | (enumLiteral_1= 'dashed' ) | (enumLiteral_2= 'dashed-dotted' ) | (enumLiteral_3= 'dotted' ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1564:1: ( (enumLiteral_0= 'line' ) | (enumLiteral_1= 'dashed' ) | (enumLiteral_2= 'dashed-dotted' ) | (enumLiteral_3= 'dotted' ) )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1564:1: ( (enumLiteral_0= 'line' ) | (enumLiteral_1= 'dashed' ) | (enumLiteral_2= 'dashed-dotted' ) | (enumLiteral_3= 'dotted' ) )
            int alt30=4;
            switch ( input.LA(1) ) {
            case 32:
                {
                alt30=1;
                }
                break;
            case 39:
                {
                alt30=2;
                }
                break;
            case 40:
                {
                alt30=3;
                }
                break;
            case 41:
                {
                alt30=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }

            switch (alt30) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1564:2: (enumLiteral_0= 'line' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1564:2: (enumLiteral_0= 'line' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1564:4: enumLiteral_0= 'line'
                    {
                    enumLiteral_0=(Token)match(input,32,FollowSets000.FOLLOW_32_in_ruleLineType3316); 

                            current = grammarAccess.getLineTypeAccess().getLineEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getLineTypeAccess().getLineEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1570:6: (enumLiteral_1= 'dashed' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1570:6: (enumLiteral_1= 'dashed' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1570:8: enumLiteral_1= 'dashed'
                    {
                    enumLiteral_1=(Token)match(input,39,FollowSets000.FOLLOW_39_in_ruleLineType3333); 

                            current = grammarAccess.getLineTypeAccess().getDashedEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getLineTypeAccess().getDashedEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1576:6: (enumLiteral_2= 'dashed-dotted' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1576:6: (enumLiteral_2= 'dashed-dotted' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1576:8: enumLiteral_2= 'dashed-dotted'
                    {
                    enumLiteral_2=(Token)match(input,40,FollowSets000.FOLLOW_40_in_ruleLineType3350); 

                            current = grammarAccess.getLineTypeAccess().getDashedDottedEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getLineTypeAccess().getDashedDottedEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1582:6: (enumLiteral_3= 'dotted' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1582:6: (enumLiteral_3= 'dotted' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1582:8: enumLiteral_3= 'dotted'
                    {
                    enumLiteral_3=(Token)match(input,41,FollowSets000.FOLLOW_41_in_ruleLineType3367); 

                            current = grammarAccess.getLineTypeAccess().getDottedEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getLineTypeAccess().getDottedEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLineType"


    // $ANTLR start "ruleArrowType"
    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1592:1: ruleArrowType returns [Enumerator current=null] : ( (enumLiteral_0= 'circle' ) | (enumLiteral_1= 'concave' ) | (enumLiteral_2= 'convex' ) | (enumLiteral_3= 'crows-foot-many' ) | (enumLiteral_4= 'crows-foot-many-mandatory' ) | (enumLiteral_5= 'crows-foot-many-optional' ) | (enumLiteral_6= 'crows-foot-one' ) | (enumLiteral_7= 'crows-foot-one-mandatory' ) | (enumLiteral_8= 'crows-foot-one-optional' ) | (enumLiteral_9= 'dash' ) | (enumLiteral_10= 'delta' ) | (enumLiteral_11= 'diamond' ) | (enumLiteral_12= 'none' ) | (enumLiteral_13= 'plain' ) | (enumLiteral_14= 'short' ) | (enumLiteral_15= 'skewed-dash' ) | (enumLiteral_16= 'standard' ) | (enumLiteral_17= 'transparent-circle' ) | (enumLiteral_18= 't-shape' ) | (enumLiteral_19= 'white-delta' ) | (enumLiteral_20= 'white-diamond' ) ) ;
    public final Enumerator ruleArrowType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;
        Token enumLiteral_10=null;
        Token enumLiteral_11=null;
        Token enumLiteral_12=null;
        Token enumLiteral_13=null;
        Token enumLiteral_14=null;
        Token enumLiteral_15=null;
        Token enumLiteral_16=null;
        Token enumLiteral_17=null;
        Token enumLiteral_18=null;
        Token enumLiteral_19=null;
        Token enumLiteral_20=null;

         enterRule(); 
        try {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1594:28: ( ( (enumLiteral_0= 'circle' ) | (enumLiteral_1= 'concave' ) | (enumLiteral_2= 'convex' ) | (enumLiteral_3= 'crows-foot-many' ) | (enumLiteral_4= 'crows-foot-many-mandatory' ) | (enumLiteral_5= 'crows-foot-many-optional' ) | (enumLiteral_6= 'crows-foot-one' ) | (enumLiteral_7= 'crows-foot-one-mandatory' ) | (enumLiteral_8= 'crows-foot-one-optional' ) | (enumLiteral_9= 'dash' ) | (enumLiteral_10= 'delta' ) | (enumLiteral_11= 'diamond' ) | (enumLiteral_12= 'none' ) | (enumLiteral_13= 'plain' ) | (enumLiteral_14= 'short' ) | (enumLiteral_15= 'skewed-dash' ) | (enumLiteral_16= 'standard' ) | (enumLiteral_17= 'transparent-circle' ) | (enumLiteral_18= 't-shape' ) | (enumLiteral_19= 'white-delta' ) | (enumLiteral_20= 'white-diamond' ) ) )
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1595:1: ( (enumLiteral_0= 'circle' ) | (enumLiteral_1= 'concave' ) | (enumLiteral_2= 'convex' ) | (enumLiteral_3= 'crows-foot-many' ) | (enumLiteral_4= 'crows-foot-many-mandatory' ) | (enumLiteral_5= 'crows-foot-many-optional' ) | (enumLiteral_6= 'crows-foot-one' ) | (enumLiteral_7= 'crows-foot-one-mandatory' ) | (enumLiteral_8= 'crows-foot-one-optional' ) | (enumLiteral_9= 'dash' ) | (enumLiteral_10= 'delta' ) | (enumLiteral_11= 'diamond' ) | (enumLiteral_12= 'none' ) | (enumLiteral_13= 'plain' ) | (enumLiteral_14= 'short' ) | (enumLiteral_15= 'skewed-dash' ) | (enumLiteral_16= 'standard' ) | (enumLiteral_17= 'transparent-circle' ) | (enumLiteral_18= 't-shape' ) | (enumLiteral_19= 'white-delta' ) | (enumLiteral_20= 'white-diamond' ) )
            {
            // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1595:1: ( (enumLiteral_0= 'circle' ) | (enumLiteral_1= 'concave' ) | (enumLiteral_2= 'convex' ) | (enumLiteral_3= 'crows-foot-many' ) | (enumLiteral_4= 'crows-foot-many-mandatory' ) | (enumLiteral_5= 'crows-foot-many-optional' ) | (enumLiteral_6= 'crows-foot-one' ) | (enumLiteral_7= 'crows-foot-one-mandatory' ) | (enumLiteral_8= 'crows-foot-one-optional' ) | (enumLiteral_9= 'dash' ) | (enumLiteral_10= 'delta' ) | (enumLiteral_11= 'diamond' ) | (enumLiteral_12= 'none' ) | (enumLiteral_13= 'plain' ) | (enumLiteral_14= 'short' ) | (enumLiteral_15= 'skewed-dash' ) | (enumLiteral_16= 'standard' ) | (enumLiteral_17= 'transparent-circle' ) | (enumLiteral_18= 't-shape' ) | (enumLiteral_19= 'white-delta' ) | (enumLiteral_20= 'white-diamond' ) )
            int alt31=21;
            switch ( input.LA(1) ) {
            case 42:
                {
                alt31=1;
                }
                break;
            case 43:
                {
                alt31=2;
                }
                break;
            case 44:
                {
                alt31=3;
                }
                break;
            case 45:
                {
                alt31=4;
                }
                break;
            case 46:
                {
                alt31=5;
                }
                break;
            case 47:
                {
                alt31=6;
                }
                break;
            case 48:
                {
                alt31=7;
                }
                break;
            case 49:
                {
                alt31=8;
                }
                break;
            case 50:
                {
                alt31=9;
                }
                break;
            case 51:
                {
                alt31=10;
                }
                break;
            case 52:
                {
                alt31=11;
                }
                break;
            case 53:
                {
                alt31=12;
                }
                break;
            case 54:
                {
                alt31=13;
                }
                break;
            case 55:
                {
                alt31=14;
                }
                break;
            case 56:
                {
                alt31=15;
                }
                break;
            case 57:
                {
                alt31=16;
                }
                break;
            case 58:
                {
                alt31=17;
                }
                break;
            case 59:
                {
                alt31=18;
                }
                break;
            case 60:
                {
                alt31=19;
                }
                break;
            case 61:
                {
                alt31=20;
                }
                break;
            case 62:
                {
                alt31=21;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;
            }

            switch (alt31) {
                case 1 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1595:2: (enumLiteral_0= 'circle' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1595:2: (enumLiteral_0= 'circle' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1595:4: enumLiteral_0= 'circle'
                    {
                    enumLiteral_0=(Token)match(input,42,FollowSets000.FOLLOW_42_in_ruleArrowType3412); 

                            current = grammarAccess.getArrowTypeAccess().getCircleEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_0, grammarAccess.getArrowTypeAccess().getCircleEnumLiteralDeclaration_0()); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1601:6: (enumLiteral_1= 'concave' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1601:6: (enumLiteral_1= 'concave' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1601:8: enumLiteral_1= 'concave'
                    {
                    enumLiteral_1=(Token)match(input,43,FollowSets000.FOLLOW_43_in_ruleArrowType3429); 

                            current = grammarAccess.getArrowTypeAccess().getConcaveEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_1, grammarAccess.getArrowTypeAccess().getConcaveEnumLiteralDeclaration_1()); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1607:6: (enumLiteral_2= 'convex' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1607:6: (enumLiteral_2= 'convex' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1607:8: enumLiteral_2= 'convex'
                    {
                    enumLiteral_2=(Token)match(input,44,FollowSets000.FOLLOW_44_in_ruleArrowType3446); 

                            current = grammarAccess.getArrowTypeAccess().getConvexEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_2, grammarAccess.getArrowTypeAccess().getConvexEnumLiteralDeclaration_2()); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1613:6: (enumLiteral_3= 'crows-foot-many' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1613:6: (enumLiteral_3= 'crows-foot-many' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1613:8: enumLiteral_3= 'crows-foot-many'
                    {
                    enumLiteral_3=(Token)match(input,45,FollowSets000.FOLLOW_45_in_ruleArrowType3463); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootManyEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_3, grammarAccess.getArrowTypeAccess().getCrowsFootManyEnumLiteralDeclaration_3()); 
                        

                    }


                    }
                    break;
                case 5 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1619:6: (enumLiteral_4= 'crows-foot-many-mandatory' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1619:6: (enumLiteral_4= 'crows-foot-many-mandatory' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1619:8: enumLiteral_4= 'crows-foot-many-mandatory'
                    {
                    enumLiteral_4=(Token)match(input,46,FollowSets000.FOLLOW_46_in_ruleArrowType3480); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootManyMandatoryEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_4, grammarAccess.getArrowTypeAccess().getCrowsFootManyMandatoryEnumLiteralDeclaration_4()); 
                        

                    }


                    }
                    break;
                case 6 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1625:6: (enumLiteral_5= 'crows-foot-many-optional' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1625:6: (enumLiteral_5= 'crows-foot-many-optional' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1625:8: enumLiteral_5= 'crows-foot-many-optional'
                    {
                    enumLiteral_5=(Token)match(input,47,FollowSets000.FOLLOW_47_in_ruleArrowType3497); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootManyOptionalEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_5, grammarAccess.getArrowTypeAccess().getCrowsFootManyOptionalEnumLiteralDeclaration_5()); 
                        

                    }


                    }
                    break;
                case 7 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1631:6: (enumLiteral_6= 'crows-foot-one' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1631:6: (enumLiteral_6= 'crows-foot-one' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1631:8: enumLiteral_6= 'crows-foot-one'
                    {
                    enumLiteral_6=(Token)match(input,48,FollowSets000.FOLLOW_48_in_ruleArrowType3514); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootOneEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_6, grammarAccess.getArrowTypeAccess().getCrowsFootOneEnumLiteralDeclaration_6()); 
                        

                    }


                    }
                    break;
                case 8 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1637:6: (enumLiteral_7= 'crows-foot-one-mandatory' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1637:6: (enumLiteral_7= 'crows-foot-one-mandatory' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1637:8: enumLiteral_7= 'crows-foot-one-mandatory'
                    {
                    enumLiteral_7=(Token)match(input,49,FollowSets000.FOLLOW_49_in_ruleArrowType3531); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootOneMandatoryEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_7, grammarAccess.getArrowTypeAccess().getCrowsFootOneMandatoryEnumLiteralDeclaration_7()); 
                        

                    }


                    }
                    break;
                case 9 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1643:6: (enumLiteral_8= 'crows-foot-one-optional' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1643:6: (enumLiteral_8= 'crows-foot-one-optional' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1643:8: enumLiteral_8= 'crows-foot-one-optional'
                    {
                    enumLiteral_8=(Token)match(input,50,FollowSets000.FOLLOW_50_in_ruleArrowType3548); 

                            current = grammarAccess.getArrowTypeAccess().getCrowsFootOneOptionalEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_8, grammarAccess.getArrowTypeAccess().getCrowsFootOneOptionalEnumLiteralDeclaration_8()); 
                        

                    }


                    }
                    break;
                case 10 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1649:6: (enumLiteral_9= 'dash' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1649:6: (enumLiteral_9= 'dash' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1649:8: enumLiteral_9= 'dash'
                    {
                    enumLiteral_9=(Token)match(input,51,FollowSets000.FOLLOW_51_in_ruleArrowType3565); 

                            current = grammarAccess.getArrowTypeAccess().getDashEnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_9, grammarAccess.getArrowTypeAccess().getDashEnumLiteralDeclaration_9()); 
                        

                    }


                    }
                    break;
                case 11 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1655:6: (enumLiteral_10= 'delta' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1655:6: (enumLiteral_10= 'delta' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1655:8: enumLiteral_10= 'delta'
                    {
                    enumLiteral_10=(Token)match(input,52,FollowSets000.FOLLOW_52_in_ruleArrowType3582); 

                            current = grammarAccess.getArrowTypeAccess().getDeltaEnumLiteralDeclaration_10().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_10, grammarAccess.getArrowTypeAccess().getDeltaEnumLiteralDeclaration_10()); 
                        

                    }


                    }
                    break;
                case 12 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1661:6: (enumLiteral_11= 'diamond' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1661:6: (enumLiteral_11= 'diamond' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1661:8: enumLiteral_11= 'diamond'
                    {
                    enumLiteral_11=(Token)match(input,53,FollowSets000.FOLLOW_53_in_ruleArrowType3599); 

                            current = grammarAccess.getArrowTypeAccess().getDiamondEnumLiteralDeclaration_11().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_11, grammarAccess.getArrowTypeAccess().getDiamondEnumLiteralDeclaration_11()); 
                        

                    }


                    }
                    break;
                case 13 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1667:6: (enumLiteral_12= 'none' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1667:6: (enumLiteral_12= 'none' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1667:8: enumLiteral_12= 'none'
                    {
                    enumLiteral_12=(Token)match(input,54,FollowSets000.FOLLOW_54_in_ruleArrowType3616); 

                            current = grammarAccess.getArrowTypeAccess().getNoneEnumLiteralDeclaration_12().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_12, grammarAccess.getArrowTypeAccess().getNoneEnumLiteralDeclaration_12()); 
                        

                    }


                    }
                    break;
                case 14 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1673:6: (enumLiteral_13= 'plain' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1673:6: (enumLiteral_13= 'plain' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1673:8: enumLiteral_13= 'plain'
                    {
                    enumLiteral_13=(Token)match(input,55,FollowSets000.FOLLOW_55_in_ruleArrowType3633); 

                            current = grammarAccess.getArrowTypeAccess().getPlainEnumLiteralDeclaration_13().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_13, grammarAccess.getArrowTypeAccess().getPlainEnumLiteralDeclaration_13()); 
                        

                    }


                    }
                    break;
                case 15 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1679:6: (enumLiteral_14= 'short' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1679:6: (enumLiteral_14= 'short' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1679:8: enumLiteral_14= 'short'
                    {
                    enumLiteral_14=(Token)match(input,56,FollowSets000.FOLLOW_56_in_ruleArrowType3650); 

                            current = grammarAccess.getArrowTypeAccess().getShortEnumLiteralDeclaration_14().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_14, grammarAccess.getArrowTypeAccess().getShortEnumLiteralDeclaration_14()); 
                        

                    }


                    }
                    break;
                case 16 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1685:6: (enumLiteral_15= 'skewed-dash' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1685:6: (enumLiteral_15= 'skewed-dash' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1685:8: enumLiteral_15= 'skewed-dash'
                    {
                    enumLiteral_15=(Token)match(input,57,FollowSets000.FOLLOW_57_in_ruleArrowType3667); 

                            current = grammarAccess.getArrowTypeAccess().getSkewedDashEnumLiteralDeclaration_15().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_15, grammarAccess.getArrowTypeAccess().getSkewedDashEnumLiteralDeclaration_15()); 
                        

                    }


                    }
                    break;
                case 17 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1691:6: (enumLiteral_16= 'standard' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1691:6: (enumLiteral_16= 'standard' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1691:8: enumLiteral_16= 'standard'
                    {
                    enumLiteral_16=(Token)match(input,58,FollowSets000.FOLLOW_58_in_ruleArrowType3684); 

                            current = grammarAccess.getArrowTypeAccess().getStandardEnumLiteralDeclaration_16().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_16, grammarAccess.getArrowTypeAccess().getStandardEnumLiteralDeclaration_16()); 
                        

                    }


                    }
                    break;
                case 18 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1697:6: (enumLiteral_17= 'transparent-circle' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1697:6: (enumLiteral_17= 'transparent-circle' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1697:8: enumLiteral_17= 'transparent-circle'
                    {
                    enumLiteral_17=(Token)match(input,59,FollowSets000.FOLLOW_59_in_ruleArrowType3701); 

                            current = grammarAccess.getArrowTypeAccess().getTransparentCircleEnumLiteralDeclaration_17().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_17, grammarAccess.getArrowTypeAccess().getTransparentCircleEnumLiteralDeclaration_17()); 
                        

                    }


                    }
                    break;
                case 19 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1703:6: (enumLiteral_18= 't-shape' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1703:6: (enumLiteral_18= 't-shape' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1703:8: enumLiteral_18= 't-shape'
                    {
                    enumLiteral_18=(Token)match(input,60,FollowSets000.FOLLOW_60_in_ruleArrowType3718); 

                            current = grammarAccess.getArrowTypeAccess().getTShapeEnumLiteralDeclaration_18().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_18, grammarAccess.getArrowTypeAccess().getTShapeEnumLiteralDeclaration_18()); 
                        

                    }


                    }
                    break;
                case 20 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1709:6: (enumLiteral_19= 'white-delta' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1709:6: (enumLiteral_19= 'white-delta' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1709:8: enumLiteral_19= 'white-delta'
                    {
                    enumLiteral_19=(Token)match(input,61,FollowSets000.FOLLOW_61_in_ruleArrowType3735); 

                            current = grammarAccess.getArrowTypeAccess().getWhiteDeltaEnumLiteralDeclaration_19().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_19, grammarAccess.getArrowTypeAccess().getWhiteDeltaEnumLiteralDeclaration_19()); 
                        

                    }


                    }
                    break;
                case 21 :
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1715:6: (enumLiteral_20= 'white-diamond' )
                    {
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1715:6: (enumLiteral_20= 'white-diamond' )
                    // ../metabup.fragments.texteditor/src-gen/metabup/parser/antlr/internal/InternalFragments.g:1715:8: enumLiteral_20= 'white-diamond'
                    {
                    enumLiteral_20=(Token)match(input,62,FollowSets000.FOLLOW_62_in_ruleArrowType3752); 

                            current = grammarAccess.getArrowTypeAccess().getWhiteDiamondEnumLiteralDeclaration_20().getEnumLiteral().getInstance();
                            newLeafNode(enumLiteral_20, grammarAccess.getArrowTypeAccess().getWhiteDiamondEnumLiteralDeclaration_20()); 
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrowType"

    // Delegated rules


    protected DFA7 dfa7 = new DFA7(this);
    static final String DFA7_eotS =
        "\103\uffff";
    static final String DFA7_eofS =
        "\103\uffff";
    static final String DFA7_minS =
        "\1\23\1\4\2\uffff\2\23\2\4\2\27\2\23\3\4\6\25\2\27\1\4\1\23\3\4"+
        "\2\27\10\25\3\4\1\23\1\4\6\25\2\27\2\25\3\4\10\25\1\4\2\25";
    static final String DFA7_maxS =
        "\1\34\1\35\2\uffff\2\34\1\5\1\35\2\31\2\34\1\44\2\5\4\26\2\30\2"+
        "\31\1\5\1\34\1\5\1\44\1\5\2\31\6\26\2\30\1\44\2\5\1\34\1\5\4\26"+
        "\2\30\2\31\2\26\1\5\1\44\1\5\6\26\2\30\1\5\2\26";
    static final String DFA7_acceptS =
        "\2\uffff\1\1\1\2\77\uffff";
    static final String DFA7_specialS =
        "\103\uffff}>";
    static final String[] DFA7_transitionS = {
            "\1\1\6\uffff\1\2\2\3",
            "\1\4\1\5\27\uffff\1\3",
            "",
            "",
            "\1\7\1\6\5\uffff\1\2\2\3",
            "\1\7\1\6\5\uffff\1\2\2\3",
            "\1\10\1\11",
            "\1\12\1\13\27\uffff\1\3",
            "\1\15\1\uffff\1\14",
            "\1\15\1\uffff\1\14",
            "\1\7\1\16\5\uffff\1\2\2\3",
            "\1\7\1\16\5\uffff\1\2\2\3",
            "\1\17\1\uffff\1\20\34\uffff\1\21\1\22",
            "\1\23\1\24",
            "\1\25\1\26",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30\1\uffff\1\31",
            "\1\27\1\30\1\uffff\1\31",
            "\1\33\1\uffff\1\32",
            "\1\33\1\uffff\1\32",
            "\1\34\1\35",
            "\1\7\6\uffff\1\2\2\3",
            "\1\36\1\37",
            "\1\40\1\uffff\1\41\34\uffff\1\42\1\43",
            "\1\44\1\45",
            "\1\47\1\uffff\1\46",
            "\1\47\1\uffff\1\46",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51\1\uffff\1\52",
            "\1\50\1\51\1\uffff\1\52",
            "\1\53\1\uffff\1\54\34\uffff\1\55\1\56",
            "\1\57\1\60",
            "\1\61\1\62",
            "\1\7\6\uffff\1\2\2\3",
            "\1\63\1\64",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\27\1\30\1\uffff\1\65",
            "\1\27\1\30\1\uffff\1\65",
            "\1\67\1\uffff\1\66",
            "\1\67\1\uffff\1\66",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\70\1\71",
            "\1\72\1\uffff\1\73\34\uffff\1\74\1\75",
            "\1\76\1\77",
            "\1\27\1\30",
            "\1\27\1\30",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51",
            "\1\50\1\51\1\uffff\1\100",
            "\1\50\1\51\1\uffff\1\100",
            "\1\101\1\102",
            "\1\50\1\51",
            "\1\50\1\51"
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }
        public String getDescription() {
            return "369:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleFragmentModel_in_entryRuleFragmentModel75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentModel85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_ruleFragmentModel131 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFragmentModel152 = new BitSet(new long[]{0x0000006000081000L});
        public static final BitSet FOLLOW_ruleFragment_in_ruleFragmentModel173 = new BitSet(new long[]{0x0000006000081002L});
        public static final BitSet FOLLOW_ruleFragment_in_ruleFragmentModel194 = new BitSet(new long[]{0x0000006000081002L});
        public static final BitSet FOLLOW_ruleFragment_in_entryRuleFragment231 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragment241 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleFragment297 = new BitSet(new long[]{0x0000006000081000L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleFragment318 = new BitSet(new long[]{0x0000006000081000L});
        public static final BitSet FOLLOW_ruleFragmentType_in_ruleFragment342 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_12_in_ruleFragment355 = new BitSet(new long[]{0x0000000000002030L});
        public static final BitSet FOLLOW_13_in_ruleFragment368 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFragment389 = new BitSet(new long[]{0x0000000000004000L});
        public static final BitSet FOLLOW_14_in_ruleFragment401 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleFragment424 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_ruleFragment436 = new BitSet(new long[]{0x0000000000080030L});
        public static final BitSet FOLLOW_ruleObject_in_ruleFragment457 = new BitSet(new long[]{0x0000000000090030L});
        public static final BitSet FOLLOW_ruleObject_in_ruleFragment478 = new BitSet(new long[]{0x0000000000090030L});
        public static final BitSet FOLLOW_16_in_ruleFragment491 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature527 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeature537 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttrOrReference_in_ruleFeature583 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttrOrReference_in_entryRuleAttrOrReference617 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttrOrReference627 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_ruleAttrOrReference674 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_ruleAttrOrReference701 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString737 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString748 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleEString788 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleEString814 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObject_in_entryRuleObject859 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObject869 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleObject925 = new BitSet(new long[]{0x0000000000080030L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleObject946 = new BitSet(new long[]{0x0000000000080030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObject970 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_17_in_ruleObject982 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObject1003 = new BitSet(new long[]{0x0000000000008000L});
        public static final BitSet FOLLOW_15_in_ruleObject1015 = new BitSet(new long[]{0x000000001C090000L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleObject1037 = new BitSet(new long[]{0x000000001C0D0000L});
        public static final BitSet FOLLOW_18_in_ruleObject1050 = new BitSet(new long[]{0x000000001C090000L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleObject1074 = new BitSet(new long[]{0x000000001C0D0000L});
        public static final BitSet FOLLOW_18_in_ruleObject1087 = new BitSet(new long[]{0x000000001C090000L});
        public static final BitSet FOLLOW_16_in_ruleObject1105 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_entryRuleAnnotation1141 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotation1151 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_ruleAnnotation1188 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAnnotation1209 = new BitSet(new long[]{0x0000000000100002L});
        public static final BitSet FOLLOW_20_in_ruleAnnotation1222 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_ruleAnnotation1243 = new BitSet(new long[]{0x0000000000600000L});
        public static final BitSet FOLLOW_21_in_ruleAnnotation1256 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_ruleAnnotation1277 = new BitSet(new long[]{0x0000000000600000L});
        public static final BitSet FOLLOW_22_in_ruleAnnotation1291 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam1329 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotationParam1339 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAnnotationParam1385 = new BitSet(new long[]{0x0000000002800000L});
        public static final BitSet FOLLOW_ruleParamValue_in_ruleAnnotationParam1406 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParamValue_in_entryRuleParamValue1442 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleParamValue1452 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_ruleParamValue1499 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_ruleParamValue1526 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam1561 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam1571 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_ruleObjectValueAnnotationParam1608 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam1631 = new BitSet(new long[]{0x0000000001000002L});
        public static final BitSet FOLLOW_24_in_ruleObjectValueAnnotationParam1644 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleObjectValueAnnotationParam1667 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam1705 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam1715 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_rulePrimitiveValueAnnotationParam1752 = new BitSet(new long[]{0x0000001800000050L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_rulePrimitiveValueAnnotationParam1773 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_entryRuleAttribute1809 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttribute1819 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleAttribute1875 = new BitSet(new long[]{0x0000000004080000L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleAttribute1896 = new BitSet(new long[]{0x0000000004080000L});
        public static final BitSet FOLLOW_26_in_ruleAttribute1911 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAttribute1932 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleAttribute1944 = new BitSet(new long[]{0x0000001800000050L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_ruleAttribute1965 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_21_in_ruleAttribute1978 = new BitSet(new long[]{0x0000001800000050L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_ruleAttribute1999 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_ruleReference_in_entryRuleReference2037 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReference2047 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleReference2103 = new BitSet(new long[]{0x0000000018080000L});
        public static final BitSet FOLLOW_ruleAnnotation_in_ruleReference2124 = new BitSet(new long[]{0x0000000018080000L});
        public static final BitSet FOLLOW_ruleEdgeProperties_in_ruleReference2148 = new BitSet(new long[]{0x0000000018000000L});
        public static final BitSet FOLLOW_27_in_ruleReference2162 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_28_in_ruleReference2180 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference2202 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleReference2214 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference2237 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_21_in_ruleReference2250 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference2273 = new BitSet(new long[]{0x0000000000200002L});
        public static final BitSet FOLLOW_ruleEdgeProperties_in_entryRuleEdgeProperties2311 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEdgeProperties2321 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_ruleEdgeProperties2367 = new BitSet(new long[]{0x0000000020000000L});
        public static final BitSet FOLLOW_29_in_ruleEdgeProperties2379 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_20_in_ruleEdgeProperties2391 = new BitSet(new long[]{0x0000000040000000L});
        public static final BitSet FOLLOW_30_in_ruleEdgeProperties2403 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties2415 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_ruleEString_in_ruleEdgeProperties2436 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleEdgeProperties2448 = new BitSet(new long[]{0x0000000080000000L});
        public static final BitSet FOLLOW_31_in_ruleEdgeProperties2460 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties2472 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleEdgeProperties2489 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleEdgeProperties2506 = new BitSet(new long[]{0x0000000100000000L});
        public static final BitSet FOLLOW_32_in_ruleEdgeProperties2518 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties2530 = new BitSet(new long[]{0x0000038100000000L});
        public static final BitSet FOLLOW_ruleLineType_in_ruleEdgeProperties2551 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleEdgeProperties2563 = new BitSet(new long[]{0x0000000200000000L});
        public static final BitSet FOLLOW_33_in_ruleEdgeProperties2575 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties2587 = new BitSet(new long[]{0x7FFFFC0000000000L});
        public static final BitSet FOLLOW_ruleArrowType_in_ruleEdgeProperties2608 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleEdgeProperties2620 = new BitSet(new long[]{0x0000000400000000L});
        public static final BitSet FOLLOW_34_in_ruleEdgeProperties2632 = new BitSet(new long[]{0x0000000002000000L});
        public static final BitSet FOLLOW_25_in_ruleEdgeProperties2644 = new BitSet(new long[]{0x7FFFFC0000000000L});
        public static final BitSet FOLLOW_ruleArrowType_in_ruleEdgeProperties2665 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_22_in_ruleEdgeProperties2677 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue2713 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValue2723 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_rulePrimitiveValue2770 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_rulePrimitiveValue2797 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_rulePrimitiveValue2824 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue2859 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntegerValue2869 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleIntegerValue2910 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_entryRuleStringValue2950 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringValue2960 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleStringValue3001 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue3041 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValue3051 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_ruleBooleanValue3096 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal3132 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValueTerminal3143 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_35_in_ruleBooleanValueTerminal3181 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_ruleBooleanValueTerminal3200 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_ruleFragmentType3254 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_38_in_ruleFragmentType3271 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_ruleLineType3316 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_39_in_ruleLineType3333 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_40_in_ruleLineType3350 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_41_in_ruleLineType3367 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_42_in_ruleArrowType3412 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_43_in_ruleArrowType3429 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_44_in_ruleArrowType3446 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_45_in_ruleArrowType3463 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_46_in_ruleArrowType3480 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_47_in_ruleArrowType3497 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_48_in_ruleArrowType3514 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_49_in_ruleArrowType3531 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_50_in_ruleArrowType3548 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_51_in_ruleArrowType3565 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_52_in_ruleArrowType3582 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_ruleArrowType3599 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_54_in_ruleArrowType3616 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_55_in_ruleArrowType3633 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_ruleArrowType3650 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_57_in_ruleArrowType3667 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_58_in_ruleArrowType3684 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_59_in_ruleArrowType3701 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_60_in_ruleArrowType3718 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_61_in_ruleArrowType3735 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_62_in_ruleArrowType3752 = new BitSet(new long[]{0x0000000000000002L});
    }


}