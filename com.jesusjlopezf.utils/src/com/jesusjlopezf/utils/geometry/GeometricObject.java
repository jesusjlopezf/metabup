/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.jesusjlopezf.utils.geometry;


public class GeometricObject {
	private int x, y, width, height;
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getX() {
		return x;
	}

	public GeometricObject(int x, int y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public boolean contains(GeometricObject element){		
		if(element == null || element.getWidth() <= 0 || element.getHeight() <= 0) return false;
		
		int b1x = element.getX();
		int b1y = element.getY();
		
		int b2x = element.getX() + element.getWidth();
		int b2y = element.getY();
		
		int b3x = element.getX();
		int b3y = element.getY() + element.getHeight();
		
		int b4x = element.getX() + element.getWidth();
		int b4y = element.getY() + element.getHeight();
		
		if(this.containsPoint(b1x, b1y))
			if(this.containsPoint(b2x, b2y))
				if(this.containsPoint(b3x, b3y))
					if(this.containsPoint(b4x, b4y)) return true;		
		
		return false;
	}
	
	public boolean isContainedIn(GeometricObject element){		
		if(element == null || element.getWidth() <= 0 || element.getHeight() <= 0) return false;		
		return element.contains(this);		
	}
	
	public boolean isOverlapping(GeometricObject element, boolean extCall){		
		if(element == null || element.getWidth() <= 0 || element.getHeight() <= 0) return false;
		
		int b1x = element.getX();
		int b1y = element.getY();
		
		int b2x = element.getX() + element.getWidth();
		int b2y = element.getY();
		
		int b3x = element.getX();
		int b3y = element.getY() + element.getHeight();
		
		int b4x = element.getX() + element.getWidth();
		int b4y = element.getY() + element.getHeight();
		
		if(this.containsPoint(b1x, b1y)){
			return true;
		}
		if(this.containsPoint(b2x, b2y)){
			return true;
		}
		if(this.containsPoint(b3x, b3y)){
			return true;
		}
		if(this.containsPoint(b4x, b4y)){
			return true;		
		}
		if(extCall) return element.isOverlapping(this, false);
		
		return false;
	}
	
	private boolean containsPoint(int x, int y){
		if(x > this.getX() && x < this.getX()+this.getWidth())
			if(y > this.getY() && y < this.getY()+this.getHeight())
				return true;
		
		return false;
	}
	
	public boolean isGreaterThan(GeometricObject go){
		int thisArea = this.getWidth() * this.getHeight();
		int goArea = go.getWidth() * go.getHeight();
		return (thisArea > goArea);
	}
	
	public boolean isAdjacent(GeometricObject element, int maxDistance, boolean extCall){
		// this algorithm assumes the usage of right-angled objects
		if(this.isOverlapping(element, true)) return false; 
		
		int a1x = this.getX();
		int a1y = this.getY();		
				
		int a2x = this.getX() + this.getWidth();
		int a2y = a1y;
				
		int a3x = a1x;
		int a3y = this.getY() + this.getHeight();
				
		int a4x = a2x;
		int a4y = a3y; 
				
		int b1x = element.getX();
		int b1y = element.getY();
				
		int b2x = element.getX() + element.getWidth();
		int b2y = b1y;
		
		int b3x = b1x;
		int b3y = element.getY() + element.getHeight();
		
		int b4x = b2x;
		int b4y = b3y;
		
		if(a1x == b4x && a1y == b4y) return false;
		if(a2x == b3x && a2y == b3y) return false;
		if(a4x == b1x && a4y == b1y) return false;
		if(a3x == b2x && a3y == b2y) return false;
		
		// top adjacency
		if(b3y >= (a1y - maxDistance) && b3y <= a1y){
			if(b3x >= a1x && b3x <= a2x) return true;
			if(b4x >= a1x && b4x <= a2x) return true;
		}
		
		// right adjacency
		if(b1x <= (a2x + maxDistance) && b1x >= a2x){
			if(b1y >= a2y && b1y <= a4y) return true;
			if(b3y >= a2y && b3y <= a4y) return true;
		}
		
		// bottom adjacency
		if(b2y <= (a3y + maxDistance) && b2y >= a3y){
			if(b1x >= a3x && b1x <= a4x) return true;
			if(b2x >= a3x && b2x <= a4x) return true;
		}
		
		// left adjacency
		if(b2x >= (a1x - maxDistance) && b2x <= a1x){
			if(b2y >= a1y && b2y <= a3y) return true;
			if(b4y >= a1y && b4y <= a3y) return true;
		}				
		
		if(extCall) return element.isAdjacent(this, maxDistance, false);
		
		return false;
	}
	
	public int getAdjacencySide(GeometricObject element, int maxDistance, boolean extCall){
		if(!isAdjacent(element, maxDistance, extCall)) return ObjectRelativePosition.NONE;
		
		int a1x = this.getX();
		int a1y = this.getY();		
				
		int a2x = this.getX() + this.getWidth();
		int a2y = a1y;
				
		int a3x = a1x;
		int a3y = this.getY() + this.getHeight();
				
		int a4x = a2x;
		int a4y = a3y; 
				
		int b1x = element.getX();
		int b1y = element.getY();
				
		int b2x = element.getX() + element.getWidth();
		int b2y = b1y;
		
		int b3x = b1x;
		int b3y = element.getY() + element.getHeight();
		
		int b4x = b2x;
		int b4y = b3y;
		
		// top adjacency
		if(b3y >= (a1y - maxDistance) && b3y <= a1y){
			if(b3x >= a1x && b3x <= a2x) return ObjectRelativePosition.TOP;
			if(b4x >= a1x && b4x <= a2x) return ObjectRelativePosition.TOP;
		}
				
		// right adjacency
		if(b1x <= (a2x + maxDistance) && b1x >= a2x){
			if(b1y >= a2y && b1y <= a4y) return ObjectRelativePosition.RIGHT;
			if(b3y >= a2y && b3y <= a4y) return ObjectRelativePosition.RIGHT;
		}
				
		// bottom adjacency
		if(b2y <= (a3y + maxDistance) && b2y >= a3y){
			if(b1x >= a3x && b1x <= a4x) return ObjectRelativePosition.BOTTOM;
			if(b2x >= a3x && b2x <= a4x) return ObjectRelativePosition.BOTTOM;
		}
				
		// left adjacency
		if(b2x >= (a1x - maxDistance) && b2x <= a1x){
			if(b2y >= a1y && b2y <= a3y) return ObjectRelativePosition.LEFT;
			if(b4y >= a1y && b4y <= a3y) return ObjectRelativePosition.LEFT;
		}
		
		return ObjectRelativePosition.NONE;
	}
	
	public int getAlignmentSide(GeometricObject element, int maxDistance, boolean extCall){
		if(!isAdjacent(element, maxDistance, extCall)) return ObjectRelativePosition.NONE;
		
		int a1x = this.getX();
		int a1y = this.getY();		
				
		int a2x = this.getX() + this.getWidth();
		int a2y = a1y;
				
		int a3x = a1x;
		int a3y = this.getY() + this.getHeight();
				
		int a4x = a2x;
		int a4y = a3y; 
				
		int b1x = element.getX();
		int b1y = element.getY();
				
		int b2x = element.getX() + element.getWidth();
		int b2y = b1y;
		
		int b3x = b1x;
		int b3y = element.getY() + element.getHeight();
		
		int b4x = b2x;
		int b4y = b3y;
		
		// top adjacency
		if(b3y >= (a1y - maxDistance) && b3y <= a1y){
			if((b3x >= a1x && b3x <= a2x) || (b4x >= a1x && b4x <= a2x)){
				if(b1x == a1x && b3x == a3x) return ObjectRelativePosition.LEFT;
				if(b2x == a2x && b4x == a4x) return ObjectRelativePosition.RIGHT;
			}
		}
				
		// right adjacency
		if(b1x <= (a2x + maxDistance) && b1x >= a2x){
			if((b1y >= a2y && b1y <= a4y) || (b3y >= a2y && b3y <= a4y)){
				if(b1y == a1y && b2y == a2y) return ObjectRelativePosition.TOP;
				if(b3y == a3y && b4y == a4y) return ObjectRelativePosition.BOTTOM;
			}
		}
				
		// bottom adjacency
		if(b2y <= (a3y + maxDistance) && b2y >= a3y){
			if((b1x >= a3x && b1x <= a4x) || (b2x >= a3x && b2x <= a4x)){
				if(b1x == a1x && b3x == a3x) return ObjectRelativePosition.LEFT;
				if(b2x == a2x && b4x == a4x) return ObjectRelativePosition.RIGHT;
			}
		}
				
		// left adjacency
		if(b2x >= (a1x - maxDistance) && b2x <= a1x){
			if((b2y >= a1y && b2y <= a3y) || (b4y >= a1y && b4y <= a3y)){
				if(b1y == a1y && b2y == a2y) return ObjectRelativePosition.TOP;
				if(b3y == a3y && b4y == a4y) return ObjectRelativePosition.BOTTOM;
			}
		}
		
		return ObjectRelativePosition.UNIDENTIFIED;
	}
	
	
}
