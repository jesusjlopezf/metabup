/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path Selector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.PathSelector#getPathFilter <em>Path Filter</em>}</li>
 *   <li>{@link metamodeltest.PathSelector#getPathConditioner <em>Path Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getPathSelector()
 * @model
 * @generated
 */
public interface PathSelector extends Selector {
	/**
	 * Returns the value of the '<em><b>Path Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Path Filter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Path Filter</em>' containment reference.
	 * @see #setPathFilter(PathFilter)
	 * @see metamodeltest.MetamodeltestPackage#getPathSelector_PathFilter()
	 * @model containment="true"
	 * @generated
	 */
	PathFilter getPathFilter();

	/**
	 * Sets the value of the '{@link metamodeltest.PathSelector#getPathFilter <em>Path Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Path Filter</em>' containment reference.
	 * @see #getPathFilter()
	 * @generated
	 */
	void setPathFilter(PathFilter value);

	/**
	 * Returns the value of the '<em><b>Path Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Path Conditioner</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Path Conditioner</em>' containment reference.
	 * @see #setPathConditioner(PathFilter)
	 * @see metamodeltest.MetamodeltestPackage#getPathSelector_PathConditioner()
	 * @model containment="true"
	 * @generated
	 */
	PathFilter getPathConditioner();

	/**
	 * Sets the value of the '{@link metamodeltest.PathSelector#getPathConditioner <em>Path Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Path Conditioner</em>' containment reference.
	 * @see #getPathConditioner()
	 * @generated
	 */
	void setPathConditioner(PathFilter value);

} // PathSelector
