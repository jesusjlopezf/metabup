<?xml version="1.0" encoding="ASCII"?>
<frg:FragmentModel xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:frg="http//metabup/fragment" xmi:id="_0ABYIWRqEeG0-JHlx38SSQ" name="Factory">
  <fragments xmi:id="_0qc1MGRqEeG0-JHlx38SSQ" name="generators">
    <objects xmi:id="_1WAToGRqEeG0-JHlx38SSQ" name="g" type="Generator">
      <features xsi:type="frg:Attribute" xmi:id="_-zs9IGRqEeG0-JHlx38SSQ" name="name" type="String">
        <values xsi:type="frg:StringValue" xmi:id="_GyJaYGRrEeG0-JHlx38SSQ" value="x"/>
      </features>
      <features xsi:type="frg:Attribute" xmi:id="_PTfT4GRsEeG0-JHlx38SSQ" name="iAT" type="int">
        <values xsi:type="frg:IntegerValue" xmi:id="_Qs1FYGRsEeG0-JHlx38SSQ" value="5"/>
      </features>
      <features xsi:type="frg:Reference" xmi:id="_oSrVUGRsEeG0-JHlx38SSQ" name="out" type="out" reference="_kql0UGRsEeG0-JHlx38SSQ"/>
    </objects>
    <objects xmi:id="_kql0UGRsEeG0-JHlx38SSQ" name="c" type="Conveyor"/>
  </fragments>
  <fragments xmi:id="_8jpUIGRsEeG0-JHlx38SSQ" name="assemblers">
    <objects xmi:id="_AgSI0GRtEeG0-JHlx38SSQ" name="a" type="Assembler">
      <features xsi:type="frg:Reference" xmi:id="_GBnDYGRtEeG0-JHlx38SSQ" name="in" type="in" reference="_CdL6YGRtEeG0-JHlx38SSQ"/>
      <features xsi:type="frg:Reference" xmi:id="_JKHFYGRtEeG0-JHlx38SSQ" name="out" type="out" reference="_EUXecGRtEeG0-JHlx38SSQ">
        <annotation xmi:id="_oBN8cGRtEeG0-JHlx38SSQ" name="generalize"/>
      </features>
    </objects>
    <objects xmi:id="_CdL6YGRtEeG0-JHlx38SSQ" name="cin" type="Conveyor"/>
    <objects xmi:id="_EUXecGRtEeG0-JHlx38SSQ" name="cout" type="Conveyor">
      <features xsi:type="frg:Attribute" xmi:id="_SzLb8GRuEeG0-JHlx38SSQ" name="velocity" type="int">
        <values xsi:type="frg:IntegerValue" xmi:id="_U4mQUGRuEeG0-JHlx38SSQ" value="5"/>
      </features>
    </objects>
  </fragments>
</frg:FragmentModel>
