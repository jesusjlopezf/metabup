/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches.importers.yed;

import java.io.FileInputStream;

import org.eclipse.core.runtime.Path;

import metabup.extensionpoints.MetaBupSketchImportActionContribution;
import metabup.sketches.Sketch;
import metabup.sketches.Sketch2Legend;
import metabup.sketches.importers.dia.DiaImporter;
import metabup.sketches.legend.Legend;

/**
 * Opens an file dialog to select .dia diagram, then it applies the importer
 * and generates a fragment model, which is added to current shell.
 *  
 * @author jesus
 *
 */
public class ImportFragmentFromYedAction extends MetaBupSketchImportActionContribution {
	
	
	@Override
	public Sketch readSketch(FileInputStream stream) {
		YedImporter importer = new YedImporter();
		Sketch sketch = importer.read(stream, "yed_sketch");
		return sketch;
	}

	@Override
	public Legend readLegend(FileInputStream stream) {
		YedImporter importer = new YedImporter();
		Sketch legendSketch = importer.read(stream, "legend");
		Legend legend = new Sketch2Legend(legendSketch).execute();
		return legend;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		super.addExtension("graphml");
	}

	
	/*private void updateSVGFolder(IProject project, File legendFile){
		// get the SVG folder (it NEEDS a persistent property)
		LegendFolder sketchFolder = new LegendFolder(project);
		sketchFolder.update(legendFile);
	}*/
}
