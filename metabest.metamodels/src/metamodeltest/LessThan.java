/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Less Than</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.LessThan#getAnd <em>And</em>}</li>
 *   <li>{@link metamodeltest.LessThan#isOrEqual <em>Or Equal</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getLessThan()
 * @model
 * @generated
 */
public interface LessThan extends metamodeltest.Number {
	/**
	 * Returns the value of the '<em><b>And</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And</em>' containment reference.
	 * @see #setAnd(MoreThan)
	 * @see metamodeltest.MetamodeltestPackage#getLessThan_And()
	 * @model containment="true"
	 * @generated
	 */
	MoreThan getAnd();

	/**
	 * Sets the value of the '{@link metamodeltest.LessThan#getAnd <em>And</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>And</em>' containment reference.
	 * @see #getAnd()
	 * @generated
	 */
	void setAnd(MoreThan value);

	/**
	 * Returns the value of the '<em><b>Or Equal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Equal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Equal</em>' attribute.
	 * @see #setOrEqual(boolean)
	 * @see metamodeltest.MetamodeltestPackage#getLessThan_OrEqual()
	 * @model required="true"
	 * @generated
	 */
	boolean isOrEqual();

	/**
	 * Sets the value of the '{@link metamodeltest.LessThan#isOrEqual <em>Or Equal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Equal</em>' attribute.
	 * @see #isOrEqual()
	 * @generated
	 */
	void setOrEqual(boolean value);

} // LessThan
