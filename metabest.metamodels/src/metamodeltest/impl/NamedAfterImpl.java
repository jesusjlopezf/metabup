/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.MetamodeltestPackage;
import metamodeltest.NamedAfter;
import metamodeltest.Selector;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Named After</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.NamedAfterImpl#getElementSels <em>Element Sels</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NamedAfterImpl extends ElementQualifierImpl implements NamedAfter {
	/**
	 * The cached value of the '{@link #getElementSels() <em>Element Sels</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElementSels()
	 * @generated
	 * @ordered
	 */
	protected Selector elementSels;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NamedAfterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.NAMED_AFTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Selector getElementSels() {
		return elementSels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElementSels(Selector newElementSels, NotificationChain msgs) {
		Selector oldElementSels = elementSels;
		elementSels = newElementSels;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.NAMED_AFTER__ELEMENT_SELS, oldElementSels, newElementSels);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElementSels(Selector newElementSels) {
		if (newElementSels != elementSels) {
			NotificationChain msgs = null;
			if (elementSels != null)
				msgs = ((InternalEObject)elementSels).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.NAMED_AFTER__ELEMENT_SELS, null, msgs);
			if (newElementSels != null)
				msgs = ((InternalEObject)newElementSels).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.NAMED_AFTER__ELEMENT_SELS, null, msgs);
			msgs = basicSetElementSels(newElementSels, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.NAMED_AFTER__ELEMENT_SELS, newElementSels, newElementSels));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED_AFTER__ELEMENT_SELS:
				return basicSetElementSels(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED_AFTER__ELEMENT_SELS:
				return getElementSels();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED_AFTER__ELEMENT_SELS:
				setElementSels((Selector)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED_AFTER__ELEMENT_SELS:
				setElementSels((Selector)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED_AFTER__ELEMENT_SELS:
				return elementSels != null;
		}
		return super.eIsSet(featureID);
	}

} //NamedAfterImpl
