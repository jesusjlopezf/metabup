/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks;

import test.checks.expressions.FeatureNatureExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Nature Mismatch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.FeatureNatureMismatch#getFeatureNature <em>Feature Nature</em>}</li>
 * </ul>
 *
 * @see test.checks.ChecksPackage#getFeatureNatureMismatch()
 * @model
 * @generated
 */
public interface FeatureNatureMismatch extends Mismatch {
	/**
	 * Returns the value of the '<em><b>Feature Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Nature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Nature</em>' containment reference.
	 * @see #setFeatureNature(FeatureNatureExpression)
	 * @see test.checks.ChecksPackage#getFeatureNatureMismatch_FeatureNature()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FeatureNatureExpression getFeatureNature();

	/**
	 * Sets the value of the '{@link test.checks.FeatureNatureMismatch#getFeatureNature <em>Feature Nature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Nature</em>' containment reference.
	 * @see #getFeatureNature()
	 * @generated
	 */
	void setFeatureNature(FeatureNatureExpression value);

} // FeatureNatureMismatch
