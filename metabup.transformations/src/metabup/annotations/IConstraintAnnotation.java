package metabup.annotations;

import java.util.HashMap;

public interface IConstraintAnnotation extends IAnnotation {
	public HashMap<String, String> getConstraints();
	
	abstract void updateConstraints();
}
