package com.jesusjlopezf.utils.reflective;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;


public class ReflectiveUtils {
	public static Set<Class> getClassesFromLib(String jar, String packageName) throws ClassNotFoundException, FileNotFoundException, IOException {
		   Set<Class> classes = new HashSet<Class>();
		   JarInputStream jarFile = new JarInputStream(new FileInputStream(jar));
		   JarEntry jarEntry;
		   packageName = packageName.replace('.', '/');		   
		   do {
		      jarEntry = jarFile.getNextJarEntry();		      
		      if (jarEntry != null) {
		         String className = jarEntry.getName();		         
		         if (className.endsWith(".class")) {
		            className = className.substring(0, className.indexOf(".class"));
		            String onlyClassName = className.substring(packageName.length()+1);
		            if(!onlyClassName.contains("/")) {
		            	className = className.replace('/', '.');
		            	Class<?> clas = Class.forName(className);
		            	classes.add((Class)clas);		            	
		            }
		         }
		      }
		   } while (jarEntry != null);
		   return classes;
		}
}
