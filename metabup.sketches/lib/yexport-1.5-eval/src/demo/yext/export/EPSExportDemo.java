/****************************************************************************
 * This demo file is part of the yFiles extension package yExport-1.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * yExport demo files exhibit yExport functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a yExport version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own yExport powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the yExport
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.export;


import y.option.ConstraintManager;
import y.option.OptionGroup;
import y.option.OptionHandler;
import y.view.Graph2D;
import y.view.Graph2DView;
import y.view.YRenderingHints;
import yext.export.io.EPSOutputHandler;

import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JToolBar;


/**
 * Demonstrates how to export a graph to EPS format.
 *
 */
public class EPSExportDemo extends AbstractExportDemo
{
  private static final String RENDER_TEXT_AS_SHAPES = "Render Text as Shapes";
  private static final String EMBED_FONTS = "Embed Fonts";


  /**
   * Creates a toolbar for this demo.
   */
  protected JToolBar createToolBar() {
    JToolBar jtb = super.createToolBar();
    jtb.addSeparator();
    EPSOutputHandler outputHandler = new EPSOutputHandler();
    jtb.add(new JButton(new EPSExportAction("EPS Export", outputHandler)));
    return jtb;
  }

  /**
   * Action that exports the given graph to EPS.
   */
  class EPSExportAction extends ExportAction
  {
    EPSExportAction( String name, EPSOutputHandler outputHandler ) {
      super(name, outputHandler);

      final String[] sizes = {
              "Use Original Size", USE_CUSTOM_WIDTH, USE_CUSTOM_HEIGHT
      };
      final String[] clipRegions = {
              GRAPH, VIEW
      };

      // set up an OptionHandler to allow for user customizations
      options = new OptionHandler(name);
      final OptionGroup group = new OptionGroup();
      group.setAttribute(OptionGroup.ATTRIBUTE_TITLE, name + " Options");
      group.addItem(options.addEnum(SIZE, sizes, 0));
      group.addItem(options.addInt(CUSTOM_WIDTH,  500));
      group.addItem(options.addInt(CUSTOM_HEIGHT, 500));
      group.addItem(options.addInt(EMPTY_BORDER_WIDTH, 10));
      group.addItem(options.addEnum(CLIP_REGION, clipRegions, 0));
      group.addItem(options.addBool(SELECTION_PAINTING, false));
      group.addItem(options.addBool(RENDER_TEXT_AS_SHAPES, false));
      group.addItem(options.addBool(EMBED_FONTS, false));

      final ConstraintManager cm = new ConstraintManager(options);
      cm.setEnabledOnValueEquals(SIZE, USE_CUSTOM_WIDTH, CUSTOM_WIDTH);
      cm.setEnabledOnValueEquals(SIZE, USE_CUSTOM_HEIGHT, CUSTOM_HEIGHT);
      cm.setEnabledOnValueEquals(CLIP_REGION, GRAPH, EMPTY_BORDER_WIDTH);
      cm.setEnabledOnValueEquals(RENDER_TEXT_AS_SHAPES, Boolean.FALSE, EMBED_FONTS);
    }

    public void actionPerformed( final ActionEvent e ) {
      // show our OptionHandler to allow the user to set up the export view
      // according to his or her wishes
      if (!options.showEditor(view.getFrame())) {
        return;
      }

      if (options.getBool(SELECTION_PAINTING)) {
        outputHandler.removeRenderingHint(YRenderingHints.KEY_SELECTION_PAINTING);
      } else {
        outputHandler.addRenderingHint(
                YRenderingHints.KEY_SELECTION_PAINTING,
                YRenderingHints.VALUE_SELECTION_PAINTING_OFF);
      }

      final byte policy;
      if (options.getBool(RENDER_TEXT_AS_SHAPES)) {
        policy = EPSOutputHandler.VECTORIZED_TEXT;
      } else if (options.getBool(EMBED_FONTS)) {
        policy = EPSOutputHandler.PLAIN_TEXT_WITH_EMBEDDED_FONTS;
      } else {
        policy = EPSOutputHandler.PLAIN_TEXT;
      }
      ((EPSOutputHandler)outputHandler).setTextRenderingPolicy(policy);

      Graph2D graph = view.getGraph2D();
      Graph2DView viewPort = outputHandler.createDefaultGraph2DView(graph);

      // configure the export view according to the options specified by the
      // user
      configureViewPort(viewPort);
      graph.setCurrentView(viewPort);

      // ask for the destination file and finally write the graph to the chosen
      // destination
      export(outputHandler);

      graph.removeView(viewPort);
    }
  }


  /**
   * Launches this demo.
   */
  public static void main(String[] args) {
    initLnF();
    (new EPSExportDemo()).start();
  }
}