/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package metabest.ui.wizards;

import metabest.preferences.MetaBestPreferenceConstants;
import metabest.ui.wizards.pages.NewMetaBestTestFragmentPage;
import metabup.sketches.Sketch;
import metabup.sketches.Sketch2Fragment;
import metabup.sketches.Sketch2Legend;
import metabup.sketches.importers.dia.DiaImporter;
import metabup.sketches.importers.yed.YedImporter;
import metabup.sketches.resources.LegendFolder;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.operation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.CoreException;

import java.io.*;

import org.eclipse.ui.*;
import org.eclipse.ui.ide.IDE;

import fragments.Attribute;
import fragments.BooleanValue;
import fragments.Fragment;
import fragments.IntegerValue;
import fragments.PrimitiveValue;
import fragments.Reference;
import fragments.StringValue;

/**
 * This is a sample new wizard. Its role is to create a new file 
 * resource in the provided container. If the container resource
 * (a folder or a project) is selected in the workspace 
 * when the wizard is opened, it will accept it as the target
 * container. The wizard creates one file with the extension
 * "mbest". If a sample multi-page editor (also available
 * as a template) is registered for the same extension, it will
 * be able to open it.
 */

public class NewMetaBestTestFragment extends Wizard implements INewWizard {
	private NewMetaBestTestFragmentPage page;
	private ISelection selection;

	/**
	 * Constructor for NewMetaBestTestcase.
	 */
	public NewMetaBestTestFragment() {
		super();
		setNeedsProgressMonitor(true);
	}
	
	/**
	 * Adding the page to the wizard.
	 */

	public void addPages() {
		page = new NewMetaBestTestFragmentPage(selection);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in
	 * the wizard. We will create an operation and run it
	 * using wizard as execution context.
	 */
	public boolean performFinish() {
		final String containerName = page.getContainerName();
		final String fileName = page.getFileName();
		final String metamodel = page.getTargetMetamodelName();
		final String fragmentFile = page.getFragmentFile();
		final String legendFile = page.getLegendFile();
		final String fragmentType = page.getFragmentType();
		final IProject project = page.getProject();
		
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(containerName, fileName, metamodel, monitor, fragmentType, fragmentFile, legendFile, project);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		
		
		try {
			this.getContainer().run(true, false, op);
		} catch (InvocationTargetException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return true;
	}
	
	/**
	 * The worker method. It will find the container, create the
	 * file if missing or just replace its contents, and open
	 * the editor on the newly created file.
	 */

	private void doFinish(
		String containerName,
		String fileName, 
		final String metamodel,
		final IProgressMonitor monitor, 
		final String fragmentType, 
		final String fragmentFile, 
		final String legendFile,
		final IProject project)
		throws CoreException {
		// create a sample file
		monitor.beginTask("Creating " + fileName, 2);
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = root.findMember(new Path(containerName));
		if (!resource.exists() || !(resource instanceof IContainer)) throwCoreException("Container \"" + containerName + "\" does not exist.");
		IContainer container = (IContainer) resource;
		final IFile file = container.getFile(new Path(fileName));
		
		new Thread(new Runnable() {
		  public void run() {
		        Display.getDefault().syncExec(new Runnable() {

					@Override
					public void run() {
						InputStream stream = openContentStream(metamodel, fragmentType, fragmentFile, legendFile, project);						
							try {
								if (file.exists()) {
									file.setContents(stream, true, true, monitor);
								} else {
									file.create(stream, true, monitor);
								}
								stream.close();
							} catch (CoreException | IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}						
					}
		        	
		        });
		     }
		}).start();
		
		do{}while(!file.exists());
		
		monitor.worked(1);
		monitor.setTaskName("Opening file for editing...");
		getShell().getDisplay().syncExec(new Runnable() {
			public void run() {
				IWorkbenchPage page =
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					IDE.openEditor(page, file, true);
				} catch (PartInitException e) {
				}
			}
		});
		
		monitor.worked(1);
	}
	
	/**
	 * We will initialize file contents with a sample text.
	 */

	private InputStream openContentStream(String metamodel, String fragmentType, String fragmentFile, String legendFile, IProject project) {
		String contents;
		if(metamodel != null) contents = "metamodel \"" + metamodel + "\"";
		else contents = "";
		try{
			if(fragmentType != null){				
				Path path   = new Path(fragmentFile);
				String name = path.lastSegment().substring(0, path.lastSegment().length() - (path.getFileExtension().length() + 1));
				Object importer = null;				
				if(fragmentType.equals("dia")) importer = new DiaImporter();
				else importer = new YedImporter();
				
				metabup.sketches.legend.Legend legend = null;
				
				if ( legendFile != null ) {
					Object legendImporter = null;				
					if(fragmentType.equals("dia")) legendImporter = new DiaImporter();
					else legendImporter = new YedImporter();
					
					if(fragmentType.equals("dia")){
						Sketch legendSketch = ((DiaImporter)legendImporter).read(new FileInputStream(legendFile), "metabup.sketches.legend");
						legend = new Sketch2Legend(legendSketch).execute();
					}else{
						LegendFolder folder = new LegendFolder(project);
						if(!folder.exists()) return null;
						legend = folder.getLegend();	
					}
				}
				
				Sketch sketch;
				
				if(fragmentType.equals("dia")) sketch = ((DiaImporter)importer).read(new FileInputStream(fragmentFile), name);
				else sketch = ((YedImporter)importer).read(new FileInputStream(fragmentFile), name);
				
				Fragment fragment = new Sketch2Fragment(sketch, legend, null).execute();
				
				// serialize it
				//fragment.setType("example"); <- look for the literal to turn it into example
				/*Injector injector = Guice.createInjector(new MBFRuntimeModule());
				Serializer serializer = injector.getInstance(Serializer.class);
				contents += serializer.serialize(fModel);*/
				
				contents += "\n\nexample " + fragment.getName() + "{";
				
				List<String> missingRefAssertions = new ArrayList<String>();
				
				for(fragments.Object o : fragment.getObjects()){
					if(!o.isAnnotated(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL))){
						if(o.getAnnotation() != null && !o.getAnnotation().isEmpty()){							
							for(fragments.Annotation a : o.getAnnotation())
								if(!a.getName().equals(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL))
									&& !a.getName().equals(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.UNKNOWN_ASSERTION_LITERAL)))
										contents += "\n\t@" + a.getName();
						}
						
						contents += "\n\t" + o.getName() + " : " + o.getType() + "{";
						
						for(fragments.Feature f : o.getFeatures()){
							boolean missing = f.isAnnotated(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL));														
														
							if(f instanceof fragments.Attribute && !missing){
								if(f.getAnnotation() != null && !f.getAnnotation().isEmpty()){		
									for(fragments.Annotation a : f.getAnnotation())
										if(!a.getName().equals(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL))										   && !a.getName().equals(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.UNKNOWN_ASSERTION_LITERAL)))
											if(!missing) contents += "\n\t@" + a.getName();
								}
								
								contents += "\n\t\t";								

								Attribute a = (Attribute)f;
								contents += "attr " + a.getName() + " = ";
								
								for(PrimitiveValue pv : a.getValues()){
									if(pv instanceof StringValue){
										contents += "\""+ ((StringValue)pv).getValue() +"\", ";
									}
									if(pv instanceof IntegerValue){
										contents += ((IntegerValue)pv).getValue() + ", ";
									}
									if(pv instanceof BooleanValue){
										contents += "\""+ ((BooleanValue)pv).getValue() +"\", ";
									}
								}
								
								contents = contents.substring(0, contents.length()-2);
							}
							
							if(f instanceof fragments.Reference){								
								fragments.Reference r = (fragments.Reference)f;
								
								if(missing){
									if(r.howManyAnnotations(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL)) < r.getReference().size()){
										contents += "\n\t\t";								
										contents += "ref " + r.getName() + " = ";								
										int refNum = r.getReference().size() - r.howManyAnnotations(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL));
										for(int i = 0 ; i < refNum; i++) contents += r.getReference().get(i).getName() + ", ";										
										contents = contents.substring(0, contents.length()-2);
										int maxRef = r.getReference().size();
										
										for(int i = refNum; i < maxRef; i++){
											// save trace of removed element for assertions
											missingRefAssertions.add("missing reference from " + o.getName() + " to " + r.getReference().get(r.getReference().size()-1).getName() + ",");
											r.getReference().remove(r.getReference().size()-1);
										}
									}else{
										for(int i = 0; i < r.getReference().size(); i++)
											missingRefAssertions.add("missing reference from " + o.getName() + " to " + r.getReference().get(r.getReference().size()-1).getName() + ",");
										
										o.getFeatures().remove(f);
									}
								}else{
									if(f.getAnnotation() != null && !f.getAnnotation().isEmpty()){		
										for(fragments.Annotation a : f.getAnnotation())
											if(!a.getName().equals(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL))										   && !a.getName().equals(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.UNKNOWN_ASSERTION_LITERAL)))
												if(!missing) contents += "\n\t@" + a.getName();
									}
									
									contents += "\n\t\t";
									contents += "ref " + r.getName() + " = ";
									for(int i = 0 ; i < r.getReference().size(); i++) contents += r.getReference().get(i).getName() + ", ";										
									contents = contents.substring(0, contents.length()-2);									
								}
							}
						}
							
						contents += "\n\t}";
					}					
				}
				
				contents += generateAssertionsByAnnotations(fragment, missingRefAssertions);								
				contents += "\n}";
			}
		} catch (Exception e) {		
			Status s = new Status(Status.ERROR, metabest.Activator.PLUGIN_ID, e.getMessage(), e);		
			//ErrorDialog.openError(null, "Error importing fragment", "Unexpected error when importing the fragment file", s);
			System.out.println(s.getMessage());
			//ErrorDialog.openError(null, "Error importing from editor", e.getMessage(), s);
		}
		
		return new ByteArrayInputStream(contents.getBytes());
	}
	
	private String generateAssertionsByAnnotations(Fragment fragment, List<String> missRefAss){
		String assertions = "\n\n\tfails because:";
		List<fragments.Feature> removeF = new ArrayList<fragments.Feature>();
		List<fragments.Object> removeO = new ArrayList<fragments.Object>();
		
		for(fragments.Object o : fragment.getObjects()){
			if(o.isAnnotated(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.UNKNOWN_ASSERTION_LITERAL))){
				assertions += "\n\t\tunknown type of " + o.getName() + ",";
				o.removeAnnotation(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.UNKNOWN_ASSERTION_LITERAL));
			}
			
			if(o.isAnnotated(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL))){				
				for(fragments.Feature f : o.getFeatures()){
					if(f instanceof fragments.Reference){
						Reference r = (Reference)f;
						for(fragments.Object o2 : r.getReference()) assertions += "\n\t\tmissing composition from some " + o.getType() + " to " + o2.getName()+",";
					}
				}
				
				removeO.add(o);
			}
			
			for(fragments.Feature f : o.getFeatures()){
				if(f.isAnnotated(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.UNKNOWN_ASSERTION_LITERAL))){
					assertions += "\n\t\tunknown " + o.getName() + "." + f.getName() + ",";
					f.removeAnnotation(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.UNKNOWN_ASSERTION_LITERAL));
				}
				
				if(f.isAnnotated(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL))){					
					if(f instanceof Attribute){
						assertions += "\n\t\tmissing attribute " + f.getName() + "from " + o.getName()+ ",";
						removeF.add(f);
					}
					/*else if(f instanceof Reference){
						assertions += "reference from " + o.getName() + " to " + ((Reference)f).getReference().get(0).getName() + ",";
						f.removeAnnotation(MetaBestPreferenceConstants.getPreferenceStringValue(MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL));
						
						if(((Reference)f).getReference().size() == 1) removeF.add(f);
						else ((Reference)f).getReference().remove(0);
					}*/
				}
			}						
			
			o.getFeatures().removeAll(removeF);
		}
		
		fragment.getObjects().removeAll(removeO);
		
		for(String a : missRefAss) assertions += "\n\t\t" + a;
		
		if(assertions.endsWith(",")) assertions = assertions.substring(0, assertions.length()-1);
		
		assertions += "\n\t\t//type your fail assertions here";
		
		return assertions;
	}
	
	private void throwCoreException(String message) throws CoreException {
		IStatus status =
			new Status(IStatus.ERROR, "metabest", IStatus.OK, message, null);
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if
	 * we can initialize from it.
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}