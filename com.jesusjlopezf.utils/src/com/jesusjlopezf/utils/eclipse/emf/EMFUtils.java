/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.jesusjlopezf.utils.eclipse.emf;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public abstract class EMFUtils {
	public static List<EReference> getContainingReferences(EClass eclass){
		List<EReference> references = new ArrayList<EReference>();
		
		if(eclass.eContainer() instanceof EPackage){
			EPackage pkg = (EPackage) eclass.eContainer();
			for(EClassifier ecr : pkg.getEClassifiers()){
				if(ecr instanceof EClass){
					for(EReference r : ((EClass) ecr).getEReferences()){
						if(r.isContainment()){
							if(r.getEReferenceType().equals(eclass)){
								if(!references.contains(r)) references.add(r);
								System.out.println("adding...");
							}
							else{
								for(EClass sup : eclass.getEAllSuperTypes()){
									if(r.getEReferenceType().equals(sup)) if(!references.contains(r)) references.add(r);
								}
							}
						}
					}
				}
			}
			
			if(references.size() > 0) return references;
		}
		
		return null;
	}
	
	public static List<EClass> getEClassSubs(EClass eclass, boolean onlyConcrete){
		List<EClass> classes = new ArrayList<EClass>();
		
		if(eclass.eContainer() instanceof EPackage){
			EPackage pkg = (EPackage) eclass.eContainer();
			for(EClassifier ecr : pkg.getEClassifiers()){				
				if(ecr instanceof EClass){
					if(onlyConcrete && ((EClass)ecr).isAbstract()) continue;
					if(((EClass) ecr).getEAllSuperTypes().contains(eclass) && !classes.contains((EClass)ecr)){
						classes.add((EClass)ecr);
					}
				}
			}
			
			if(classes.size() > 0) return classes;
		}
		
		return null;
	}
	
	
	public static EObject createNamedEObject(EClass eclass, String name){
		if(eclass.isAbstract()) return null;
		EObject eo = EcoreUtil.create(eclass);	
		if(eo == null) return null;
		
		EStructuralFeature nameAtt = EMFUtils.getEStructuralFeatureByName(eclass, "name");				
		if(nameAtt != null)	eo.eSet(nameAtt, name);
		
		return eo;
	}
	
	public static EStructuralFeature getEStructuralFeatureByName(EClass eclass, String name){
		List<EStructuralFeature> atts = eclass.getEAllStructuralFeatures();
		if(atts == null || atts.isEmpty()) return null;
		for(EStructuralFeature ea : atts) if(ea.getName().equals(name)) return ea;
		return null;
	}	
	
	public static void registerXMIExtension(String ext){
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
	    Map<String, Object> m = reg.getExtensionToFactoryMap();
	    m.put(ext, new XMIResourceFactoryImpl());
	}
	
	
	public static TreeIterator<EObject> getEcoreContents(File ecore){
		if(ecore == null || !ecore.exists()) return null;
		ResourceSet resourceSet = new ResourceSetImpl(); 
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
		//resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		URI fileURI = URI.createFileURI(ecore.getAbsolutePath());
		Resource resource = resourceSet.getResource(fileURI, true);
		//resourceSet.getAllContents().next();
		return resource.getAllContents();
	}
	
	public static void serialize2File(EObject eobject, String extension, File destFile) throws IOException{
		if(extension == null || extension.equals("")) extension = "xmi";
		else{
			 Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
			 Map<String, Object> m = reg.getExtensionToFactoryMap();
			 if(m.get(extension) == null) m.put(extension, new XMIResourceFactoryImpl());
		}

		ResourceSet resSet = new ResourceSetImpl();
		String path = destFile.getAbsolutePath();
		URI uri = URI.createFileURI(path);
		Resource resource = resSet.createResource(uri);
		resource.getContents().add(eobject);		
		resource.save(Collections.EMPTY_MAP);
	}
	
	public static File getEObjectContainingFile(EObject eo){
		File file = new File (eo.eResource().getURI().toFileString());
		if(file.exists()) return file;
		return null;
	}
	
	public static EObject loadXMIFile(File file){
		ResourceSet resSet = new ResourceSetImpl();
	    Resource resource = resSet.getResource(URI.createFileURI(file.getAbsolutePath()), true);
	    EObject modelRoot = (EObject) resource.getContents().get(0);
	    return modelRoot;
	}
}
