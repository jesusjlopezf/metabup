/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.constraint;

import metabup.annotations.extensionpoint.ConstraintAnnotation;

public class Root extends ConstraintAnnotation {
	public static String NAME = "root";
	
	public Root (){
		this.setName(NAME);
		this.setDescription("Root class");
	}

	@Override
	public void updateConstraints() {
		// TODO Auto-generated method stub
		
	}
	
	
}
