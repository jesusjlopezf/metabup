/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.popup.actions;

import metabup.Activator;
import metabup.perspective.views.OpenIssuesView;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;

public class UpdateMetamodel extends MetaBupEditorAction {


	public void run() {
		/*
		XtextEditor x = editor.getXtextEditor();
		TextSelection txt = (TextSelection) x.getSelectionProvider().getSelection();
		String fragmentName = txt.getText();
		*/				
		
		IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		
		for (int i = 0; i < viewReferences.length; i++) {
			if (OpenIssuesView.ID.equals(viewReferences[i].getId())){				
				OpenIssuesView oiv = (OpenIssuesView) viewReferences[i].getView(false);
				
				////System.out.println("conflicts: " + oiv.getConflictCount());
				
				if(oiv != null && oiv.getConflictCount() > 1){
					String message = oiv.getConflictCount() + " open conflicts exist from previous iteration. Solving is required for asserting new fragments";
					Status status = new Status(IStatus.ERROR, Activator.PLUGIN_ID,  message, null);
					ErrorDialog.openError(null, "Open conflict(s)", message, status);
					return;
				}
			}
		}		
		
		if ( ! editor.getSession().updateMetamodel(null) ) {
			MessageDialog.openError(null, "Invalid fragment", "No fragments to evaluate.");
			return;			
		}
		editor.removeAssertionViewer();
		editor.refreshViewer();		
	}

}
