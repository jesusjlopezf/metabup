/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.visualization.mm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import metabup.annotations.catalog.design.Composition;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Annotation;
import metabup.metamodel.AnnotationParam;
import metabup.metamodel.Attribute;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;

import org.eclipse.emf.common.util.EList;
import org.eclipse.gef4.fx.nodes.FXConnection;
import org.eclipse.gef4.fx.nodes.IFXDecoration;
import org.eclipse.gef4.layout.algorithms.SugiyamaLayoutAlgorithm;
import org.eclipse.gef4.zest.fx.ZestProperties;
import org.eclipse.gef4.zest.fx.ui.jface.IEdgeDecorationProvider;
import org.eclipse.gef4.zest.fx.ui.jface.IGraphNodeLabelProvider;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.IFontProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;


public class MetamodelLabelProvider extends LabelProvider implements IGraphNodeLabelProvider, IEdgeDecorationProvider, IColorProvider, IFontProvider {

	private MetaModel metamodel;
	private List<AnnotatedElement> right = null;
	private List<AnnotatedElement> warnings = null;
	private List<AnnotatedElement> errors = null;
	private HashMap<String, ReferenceIndexItem> refIndex = new HashMap<String, ReferenceIndexItem>();
	
	private class ReferenceIndexItem{
		private MetaClass src, tgt;
		private List<String> refLabel = new ArrayList<String>();
		private List<String> pushLabel = new ArrayList<String>();
		
		public ReferenceIndexItem(MetaClass src, MetaClass tgt){
			this.src = src;
			this.tgt = tgt;
		}
		
		public ReferenceIndexItem(MetaClass src, MetaClass tgt, String label){
			this.src = src;
			this.tgt = tgt;
			this.push(label);
		}
		
		public void push(String label){
			if(!refLabel.contains(label)){
				refLabel.add(label);
				pushLabel.add(label);
			}			
		}
		
		public String pop(){
			if(!refLabel.isEmpty()){
				//System.out.println("label provider stack says: " + refLabel);
				return refLabel.remove(refLabel.size()-1);
			}
			
			return null;
		}
		
		public void reset(){
			if(refLabel.isEmpty())
				for(String label : pushLabel)
					refLabel.add(label);
		}
		
		public boolean isEmpty(){
			return refLabel.isEmpty();
		}
	}
	
	public MetamodelLabelProvider(MetaModel mm, List<AnnotatedElement> right, List<AnnotatedElement> warnings, List<AnnotatedElement> errors) {
		this.metamodel = mm;
		this.right = right;
		this.warnings = warnings;
		this.errors = errors;
		
		for(MetaClass mc : metamodel.getClasses()){			
			for(MetaClass smc : mc.getSupers()){
				String id = mc.getId() + "_" + smc.getId();
				if(refIndex.get(id) == null) refIndex.put(id, new ReferenceIndexItem(mc, smc, ""));
				else refIndex.get(id).push("");
			}
			
			for(Reference ref : mc.getReferences()){
				String cont = "";
				//if(ref.isAnnotated(Composition.NAME)) cont = "<>";
				for(Annotation a : ref.getAnnotations()){
					if(a.getName().toLowerCase().equals(Composition.NAME)) cont = "<>";
				}
				MetaClass tgt = ref.getReference();
				String id = mc.getId() + "_" + tgt.getId();
				if(refIndex.get(id) == null) refIndex.put(id, new ReferenceIndexItem(mc, tgt, cont + " " + ref.getName() + " (" + ref.getMin() + ", " + ref.getMax() + ")"));
				else refIndex.get(id).push(cont + " "+ref.getName() + " (" + ref.getMin() + ", " + ref.getMax() + ")");
				//System.out.println("Just inserted " + ref.getName());
			}
		}
	}
	
	@Override
	public Map<String, Object> getEdgeAttributes(Object sourceNode, Object targetNode) {			
		Map<String, Object> atts = new HashMap<String, Object>();
		MetaClass src = (MetaClass)sourceNode;
		MetaClass tgt = (MetaClass)targetNode;
		
		String id = src.getId() + "_" + tgt.getId();
				
		if(refIndex.get(id) != null){
			String label = refIndex.get(id).pop();
			
			if(label != null){
				//System.out.println("label provider says: " + label);
				atts.put("label", label);
				if(label.equals("")) atts.put("style", "dotted");
			}
			
			if(refIndex.get(id).isEmpty()) refIndex.get(id).reset();
		}
		
		
		
		return atts;
	}

	@Override
	public Map<String, Object> getNodeAttributes(Object node) {
		/* mirar ZestProperties.NODE_FISHEYE para cuando sea evaluación:
		http://git.eclipse.org/c/gef/org.eclipse.gef4.git/tree/org.eclipse.gef4.zest.examples/src/org/eclipse/gef4/zest/examples/FisheyeExample.java
		*/
		Map<String, Object> atts = new HashMap<String, Object>();
		
		if(node instanceof MetaClass){
			atts.put("label", ((MetaClass) node).getName() );
			atts.put("tooltip", ((MetaClass) node).toString() + "\n" + ((MetaClass)node).getAnnotations());
			//atts.put(CustomNodeExample.ATTR_CUSTOM, true);
		}
		
		atts.put(MetaClassContentPart.ATTR_CUSTOM, "true");
		atts.put("object", node);
		return atts;
	}

	@Override
	public Map<String, Object> getRootGraphAttributes() {
		Map<String, Object> atts = new HashMap<String, Object>();
		atts.put("type", "directed");
		atts.put(ZestProperties.GRAPH_LAYOUT, new SugiyamaLayoutAlgorithm());
		return atts;
	}
	
	@Override
	public IFXDecoration getSourceDecoration(Object sourceNode, Object targetNode){
		FXConnection connection; 
		return null;
	}
	
	@Override
	public IFXDecoration getTargetDecoration(Object sourceNode, Object targetNode){
		return null;
	}

	public Color getForeground(Object element) {
		return Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
	}

	@Override
	public Color getBackground(Object element) {
		Color color = Display.getCurrent().getSystemColor( SWT.COLOR_GRAY);
		// in case of validation
		if(element instanceof AnnotatedElement){
			if(right != null && right.contains(element)) color = Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GREEN);
			else if(warnings != null && warnings.contains(element)) color = Display.getCurrent().getSystemColor(SWT.COLOR_DARK_YELLOW);
			else if(errors != null && errors.contains(element)) color = Display.getCurrent().getSystemColor(SWT.COLOR_DARK_RED);
		}
		
		return color;
	}

	@Override
	public Font getFont(Object element) {
		String name = "Arial";
		int size = 12;
		int style = SWT.NONE;
		
		if(element instanceof MetaClass){
			size = 15;
			style = SWT.BOLD;
			
			if(((MetaClass) element).getIsAbstract()) style = SWT.ITALIC;
		}
		
		return new Font( null, new FontData(name, size, style));
	}
	
/*
	
	@Override
	public String getText(Object element) {
		// This is for the EntityGraphProvider
		String annsStr = "";
		if(element instanceof AnnotatedElement){
			List<Annotation> anns = ((AnnotatedElement) element).getAnnotations();			
			if(!anns.isEmpty()){
				annsStr = "\n{";

				for(int i=0; i<anns.size(); i++){
					if(i==0) annsStr += "@" + anns.get(i).getName();
					else if(i>0 && !anns.get(i).getName().equals(anns.get(i-1).getName())) annsStr += "@" + anns.get(i).getName() ;
					
					if(anns.get(i).getParams() != null && !anns.get(i).getParams().isEmpty()){
						for(AnnotationParam ap : anns.get(i).getParams()){
							annsStr += ap.getValue().toString() + " ";
						}
					}
					
					annsStr += ", ";
				}
 
				annsStr = annsStr.substring(0, annsStr.length()-2) + "}";
			}		
		}
		
		if ( element instanceof EntityConnectionData ) return null;
		if ( element instanceof SuperClassReference )  return null;
		if ( element instanceof Reference ) {
			String min = Integer.toString(((Reference)element).getMin());
			String max = Integer.toString(((Reference)element).getMax());
			if(min.equals("-1")) min = "*";
			if(max.equals("-1")) max = "*";
			String cardinality = "(" + min + ", " + max + ")";			
			//System.out.println("element: " + ((Reference) element).getName() + cardinality + annsStr);
			return ((Reference) element).getName() + cardinality + annsStr;
		}
		return ((MetaClass) element).getName() + annsStr;
	}
	
	@Override
	public IFigure getFigure(Object element) {
		List<AnnotatedElement> rightAtts = new ArrayList<AnnotatedElement>();
		List<AnnotatedElement> warningAtts = new ArrayList<AnnotatedElement>();
		List<AnnotatedElement> errorAtts = new ArrayList<AnnotatedElement>();
		
		for(Attribute a : ((MetaClass)element).getAttributes()){
			if(isRight(a)) rightAtts.add(a);
			else if(isWarning(a)) warningAtts.add(a);
			else if(isError(a)) errorAtts.add(a);
		}
		
		if(this.right == null && this.errors == null && this.warnings == null) return createClassFigure((MetaClass) element, regularClassColor, rightAtts, warningAtts, errorAtts);
		if(this.isError((MetaClass)element)) return createClassFigure((MetaClass) element, errorClassColor, rightAtts, warningAtts, errorAtts);
		else if(this.isWarning((MetaClass)element)) return createClassFigure((MetaClass) element, warningClassColor, rightAtts, warningAtts, errorAtts);
		else if(this.isRight((MetaClass)element)) return createClassFigure((MetaClass) element, rightClassColor, rightAtts, warningAtts, errorAtts);
		else return createClassFigure((MetaClass) element, regularClassColor, rightAtts, warningAtts, errorAtts);
	}
	
	private static Color regularClassColor = new Color(null, 255, 255, 206);
	private static Color errorClassColor = new Color(null, 236, 62, 62);
	private static Color rightClassColor = new Color(null, 0, 166, 62);
	private static Color warningClassColor = new Color(null, 238, 194, 53);
	
	private static Color regularReferenceColor = new Color(null, 0, 0, 0);
	private static Color errorReferenceColor = new Color(null, 236, 62, 62);
	private static Color rightReferenceColor = new Color(null, 0, 166, 62);
	private static Color warningReferenceColor = new Color(null, 238, 194, 53);
	
	private static Color regularAttributeColor = new Color(null, 0, 0, 0);
	private static Color errorAttributeColor = new Color(null, 124, 0, 0);
	private static Color rightAttributeColor = new Color(null, 0, 77, 28);
	private static Color warningAttributeColor = new Color(null, 131, 100, 0);
		
	private static Font classFont = new Font(null, "Arial", 12, SWT.BOLD);
	private static Font abstractClassFont = new Font(null, "Arial", 12, SWT.ITALIC);
	private static Image classImage = new Image(Display.getDefault(),
			UMLClassFigure.class.getResourceAsStream("class_obj.gif"));
	private static Image privateField = new Image(Display.getDefault(),
			UMLClassFigure.class
					.getResourceAsStream("field_private_obj.gif"));
	private static Image publicField = new Image(Display.getDefault(),
			UMLClassFigure.class.getResourceAsStream("methpub_obj.gif"));

	
	public static IFigure createClassFigure(MetaClass m, Color color, List<AnnotatedElement> rightAtts, List<AnnotatedElement> warningAtts, List<AnnotatedElement> errorAtts) {
		Label classLabel2 = new Label(m.getName(), classImage);
		
		if(m.getIsAbstract()) classLabel2.setFont(abstractClassFont);
		else classLabel2.setFont(classFont);
		
		UMLClassFigure classFigure = new UMLClassFigure(classLabel2);
		classFigure.setBackgroundColor(color);
		EList<Feature> features = m.getFeatures();
		for (Feature feature : features) {
			if ( feature instanceof Attribute ) {
				Attribute att = (Attribute) feature;
				Label attLabel = new Label(att.getName() + ": " + att.getPrimitiveType(), publicField);
				
				if(rightAtts.contains(att)) attLabel.setForegroundColor(rightAttributeColor);
				else if(warningAtts.contains(att)) attLabel.setForegroundColor(warningAttributeColor);
				else if(errorAtts.contains(att)) attLabel.setForegroundColor(errorAttributeColor);
				
				classFigure.getAttributesCompartment().add(attLabel);
			}
		}
		
		// Label method3 = new Label("getColumnID(): int", publicField);
		// Label method4 = new Label("getItems(): List", publicField);
		// classFigure.getMethodsCompartment().add(method3);
		// classFigure.getMethodsCompartment().add(method4);
		
		classFigure.setSize(-1, -1);

		return classFigure;
	}
	
	
	private boolean isRight(AnnotatedElement m){
		if(this.right == null) return false;
		for(AnnotatedElement ae : this.right)
			if(ae.getId().equals(m.getId())) return true;
		
		return false;
	}
	
	private boolean isWarning(AnnotatedElement m){
		if(this.warnings == null) return false;
		for(AnnotatedElement ae : this.warnings)
			if(ae.getId().equals(m.getId())) return true;
		
		return false;
	}
	
	private boolean isError(AnnotatedElement m){
		if(this.errors == null) return false;
		for(AnnotatedElement ae : this.errors)
			if(ae.getId().equals(m.getId())) return true;
		
		return false;
	}

	//
	// Styling methods
	//
	
	@Override
	public int getConnectionStyle(Object rel) {
		if ( rel instanceof SuperClassReference ) 
			return ZestStyles.CONNECTIONS_DIRECTED | ZestStyles.CONNECTIONS_DASH;
		return ZestStyles.CONNECTIONS_DIRECTED;
	}


	@Override
	public Color getColor(Object rel) {
		if(rel instanceof Reference){
			if(isRight((AnnotatedElement)rel)) return this.rightReferenceColor;
			else if(isWarning((AnnotatedElement)rel)) return this.warningReferenceColor;
			else if(isError((AnnotatedElement)rel)) return this.errorReferenceColor;
		}
		
		return null;
	}


	@Override
	public Color getHighlightColor(Object rel) {
		Color c = new Color(null, 45, 75, 88);
		return c;
	}


	@Override
	public int getLineWidth(Object rel) {
		return 2;
	}


	@Override
	public IFigure getTooltip(Object rel) {
		return null;
	}


	public ConnectionRouter getRouter(Object rel) {
		return null;
	}*/



}
