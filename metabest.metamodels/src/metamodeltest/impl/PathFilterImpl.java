/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.MetamodeltestPackage;
import metamodeltest.PathFilter;
import metamodeltest.PathQualifier;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Path Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.PathFilterImpl#getPathQualifier <em>Path Qualifier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PathFilterImpl extends FilterImpl implements PathFilter {
	/**
	 * The cached value of the '{@link #getPathQualifier() <em>Path Qualifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPathQualifier()
	 * @generated
	 * @ordered
	 */
	protected PathQualifier pathQualifier;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PathFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.PATH_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PathQualifier getPathQualifier() {
		return pathQualifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPathQualifier(PathQualifier newPathQualifier, NotificationChain msgs) {
		PathQualifier oldPathQualifier = pathQualifier;
		pathQualifier = newPathQualifier;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PATH_FILTER__PATH_QUALIFIER, oldPathQualifier, newPathQualifier);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPathQualifier(PathQualifier newPathQualifier) {
		if (newPathQualifier != pathQualifier) {
			NotificationChain msgs = null;
			if (pathQualifier != null)
				msgs = ((InternalEObject)pathQualifier).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PATH_FILTER__PATH_QUALIFIER, null, msgs);
			if (newPathQualifier != null)
				msgs = ((InternalEObject)newPathQualifier).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PATH_FILTER__PATH_QUALIFIER, null, msgs);
			msgs = basicSetPathQualifier(newPathQualifier, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PATH_FILTER__PATH_QUALIFIER, newPathQualifier, newPathQualifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_FILTER__PATH_QUALIFIER:
				return basicSetPathQualifier(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_FILTER__PATH_QUALIFIER:
				return getPathQualifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_FILTER__PATH_QUALIFIER:
				setPathQualifier((PathQualifier)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_FILTER__PATH_QUALIFIER:
				setPathQualifier((PathQualifier)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_FILTER__PATH_QUALIFIER:
				return pathQualifier != null;
		}
		return super.eIsSet(featureID);
	}

} //PathFilterImpl
