package metabup.fragments.export.util;

import java.util.Collection;
import java.util.Iterator;

public class IdentityConverter implements ICollectionConverter {

	@Override
	public Object convertList(Object list) {
		return list;
	}

	@Override
	public boolean isList(Object list) {
		return list instanceof Collection;
	}

	@Override
	public Iterator<Object> toIterator(Object list) {
	    	Collection<Object> collection = (Collection<Object>) list;
	    	return collection.iterator();
	}

}
