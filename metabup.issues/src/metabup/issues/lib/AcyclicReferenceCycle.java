/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.issues.lib;

import metabup.issues.general.categories.Conflict;
import metabup.metamodel.Annotation;

public class AcyclicReferenceCycle extends Conflict {
	public static int DEPTH_ONE = 1;
	public static int DEPTH_INFINITE = -1;
	
	private int depth = DEPTH_ONE;
	
	public AcyclicReferenceCycle(metabup.metamodel.Reference mmr, fragments.Reference fr, int depth){
		if(mmr != null && fr != null && (depth == DEPTH_ONE || depth == DEPTH_INFINITE)){
			this.depth = depth; 
			super.elements.add(mmr);
			fragments.Object object = (fragments.Object)fr.eContainer();
			
			if(depth == DEPTH_ONE) super.setDescription("The fragment reference " + fr.getName() + " from object " + object.getName() + " has a self-loop when it is marked as @irreflexive. Delete annotation");
			else if(depth == DEPTH_INFINITE) super.setDescription("The fragment reference " + fr.getName() + " from object " + object.getName() + " has a cycle when it is marked as @acyclic. Delete annotation");
		}
	}
	
	@Override
	public void run() {
		metabup.metamodel.Reference r = (metabup.metamodel.Reference)elements.get(0);
		Annotation removeA = null;
		
		String annotation = null;
		if(depth == DEPTH_ONE) annotation = "irreflexive";
		else if(depth == DEPTH_INFINITE) annotation = "acyclic"; 
		
		for(Annotation a : r.getAnnotations()) if(a.getName().equals(annotation)) removeA = a;		
		if(removeA != null) r.getAnnotations().remove(removeA);
	}
}
