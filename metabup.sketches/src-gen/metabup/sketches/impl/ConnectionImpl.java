/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.sketches.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import graphicProperties.EdgeProperties;
import metabup.sketches.Connection;
import metabup.sketches.SketchElement;
import metabup.sketches.sketchesPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link metabup.sketches.impl.ConnectionImpl#getSrc <em>Src</em>}</li>
 *   <li>{@link metabup.sketches.impl.ConnectionImpl#getTgt <em>Tgt</em>}</li>
 *   <li>{@link metabup.sketches.impl.ConnectionImpl#getGraphicProperties <em>Graphic Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConnectionImpl extends SketchElementImpl implements Connection {
	/**
	 * The cached value of the '{@link #getSrc() <em>Src</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrc()
	 * @generated
	 * @ordered
	 */
	protected SketchElement src;

	/**
	 * The cached value of the '{@link #getTgt() <em>Tgt</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTgt()
	 * @generated
	 * @ordered
	 */
	protected SketchElement tgt;

	/**
	 * The cached value of the '{@link #getGraphicProperties() <em>Graphic Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphicProperties()
	 * @generated
	 * @ordered
	 */
	protected EdgeProperties graphicProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return sketchesPackage.Literals.CONNECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SketchElement getSrc() {
		if (src != null && src.eIsProxy()) {
			InternalEObject oldSrc = (InternalEObject)src;
			src = (SketchElement)eResolveProxy(oldSrc);
			if (src != oldSrc) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, sketchesPackage.CONNECTION__SRC, oldSrc, src));
			}
		}
		return src;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SketchElement basicGetSrc() {
		return src;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSrc(SketchElement newSrc, NotificationChain msgs) {
		SketchElement oldSrc = src;
		src = newSrc;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, sketchesPackage.CONNECTION__SRC, oldSrc, newSrc);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSrc(SketchElement newSrc) {
		if (newSrc != src) {
			NotificationChain msgs = null;
			if (src != null)
				msgs = ((InternalEObject)src).eInverseRemove(this, sketchesPackage.SKETCH_ELEMENT__SRC_OF, SketchElement.class, msgs);
			if (newSrc != null)
				msgs = ((InternalEObject)newSrc).eInverseAdd(this, sketchesPackage.SKETCH_ELEMENT__SRC_OF, SketchElement.class, msgs);
			msgs = basicSetSrc(newSrc, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.CONNECTION__SRC, newSrc, newSrc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SketchElement getTgt() {
		if (tgt != null && tgt.eIsProxy()) {
			InternalEObject oldTgt = (InternalEObject)tgt;
			tgt = (SketchElement)eResolveProxy(oldTgt);
			if (tgt != oldTgt) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, sketchesPackage.CONNECTION__TGT, oldTgt, tgt));
			}
		}
		return tgt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SketchElement basicGetTgt() {
		return tgt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTgt(SketchElement newTgt, NotificationChain msgs) {
		SketchElement oldTgt = tgt;
		tgt = newTgt;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, sketchesPackage.CONNECTION__TGT, oldTgt, newTgt);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTgt(SketchElement newTgt) {
		if (newTgt != tgt) {
			NotificationChain msgs = null;
			if (tgt != null)
				msgs = ((InternalEObject)tgt).eInverseRemove(this, sketchesPackage.SKETCH_ELEMENT__TGT_OF, SketchElement.class, msgs);
			if (newTgt != null)
				msgs = ((InternalEObject)newTgt).eInverseAdd(this, sketchesPackage.SKETCH_ELEMENT__TGT_OF, SketchElement.class, msgs);
			msgs = basicSetTgt(newTgt, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.CONNECTION__TGT, newTgt, newTgt));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EdgeProperties getGraphicProperties() {
		return graphicProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraphicProperties(EdgeProperties newGraphicProperties, NotificationChain msgs) {
		EdgeProperties oldGraphicProperties = graphicProperties;
		graphicProperties = newGraphicProperties;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, sketchesPackage.CONNECTION__GRAPHIC_PROPERTIES, oldGraphicProperties, newGraphicProperties);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphicProperties(EdgeProperties newGraphicProperties) {
		if (newGraphicProperties != graphicProperties) {
			NotificationChain msgs = null;
			if (graphicProperties != null)
				msgs = ((InternalEObject)graphicProperties).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - sketchesPackage.CONNECTION__GRAPHIC_PROPERTIES, null, msgs);
			if (newGraphicProperties != null)
				msgs = ((InternalEObject)newGraphicProperties).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - sketchesPackage.CONNECTION__GRAPHIC_PROPERTIES, null, msgs);
			msgs = basicSetGraphicProperties(newGraphicProperties, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, sketchesPackage.CONNECTION__GRAPHIC_PROPERTIES, newGraphicProperties, newGraphicProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case sketchesPackage.CONNECTION__SRC:
				if (src != null)
					msgs = ((InternalEObject)src).eInverseRemove(this, sketchesPackage.SKETCH_ELEMENT__SRC_OF, SketchElement.class, msgs);
				return basicSetSrc((SketchElement)otherEnd, msgs);
			case sketchesPackage.CONNECTION__TGT:
				if (tgt != null)
					msgs = ((InternalEObject)tgt).eInverseRemove(this, sketchesPackage.SKETCH_ELEMENT__TGT_OF, SketchElement.class, msgs);
				return basicSetTgt((SketchElement)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case sketchesPackage.CONNECTION__SRC:
				return basicSetSrc(null, msgs);
			case sketchesPackage.CONNECTION__TGT:
				return basicSetTgt(null, msgs);
			case sketchesPackage.CONNECTION__GRAPHIC_PROPERTIES:
				return basicSetGraphicProperties(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case sketchesPackage.CONNECTION__SRC:
				if (resolve) return getSrc();
				return basicGetSrc();
			case sketchesPackage.CONNECTION__TGT:
				if (resolve) return getTgt();
				return basicGetTgt();
			case sketchesPackage.CONNECTION__GRAPHIC_PROPERTIES:
				return getGraphicProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case sketchesPackage.CONNECTION__SRC:
				setSrc((SketchElement)newValue);
				return;
			case sketchesPackage.CONNECTION__TGT:
				setTgt((SketchElement)newValue);
				return;
			case sketchesPackage.CONNECTION__GRAPHIC_PROPERTIES:
				setGraphicProperties((EdgeProperties)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case sketchesPackage.CONNECTION__SRC:
				setSrc((SketchElement)null);
				return;
			case sketchesPackage.CONNECTION__TGT:
				setTgt((SketchElement)null);
				return;
			case sketchesPackage.CONNECTION__GRAPHIC_PROPERTIES:
				setGraphicProperties((EdgeProperties)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case sketchesPackage.CONNECTION__SRC:
				return src != null;
			case sketchesPackage.CONNECTION__TGT:
				return tgt != null;
			case sketchesPackage.CONNECTION__GRAPHIC_PROPERTIES:
				return graphicProperties != null;
		}
		return super.eIsSet(featureID);
	}

} //ConnectionImpl
