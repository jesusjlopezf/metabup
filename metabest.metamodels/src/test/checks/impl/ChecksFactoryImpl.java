/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import test.checks.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ChecksFactoryImpl extends EFactoryImpl implements ChecksFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ChecksFactory init() {
		try {
			ChecksFactory theChecksFactory = (ChecksFactory)EPackage.Registry.INSTANCE.getEFactory(ChecksPackage.eNS_URI);
			if (theChecksFactory != null) {
				return theChecksFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ChecksFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChecksFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ChecksPackage.FEATURE_EXISTENCE: return createFeatureExistence();
			case ChecksPackage.TYPE_EXISTENCE: return createTypeExistence();
			case ChecksPackage.ABSTRACT_TYPE_INSTANCE: return createAbstractTypeInstance();
			case ChecksPackage.MISSING_ATTRIBUTE: return createMissingAttribute();
			case ChecksPackage.MULTIPLICITY_MISMATCH: return createMultiplicityMismatch();
			case ChecksPackage.TYPE_MISMATCH: return createTypeMismatch();
			case ChecksPackage.FEATURE_NATURE_MISMATCH: return createFeatureNatureMismatch();
			case ChecksPackage.RESTRICTION_VIOLATION: return createRestrictionViolation();
			case ChecksPackage.UNCONTAINMENT: return createUncontainment();
			case ChecksPackage.UNCONTAINMENT_BY_OBJECT: return createUncontainmentByObject();
			case ChecksPackage.UNCONTAINMENT_BY_META_CLASS: return createUncontainmentByMetaClass();
			case ChecksPackage.MISSING_REFERENCE: return createMissingReference();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureExistence createFeatureExistence() {
		FeatureExistenceImpl featureExistence = new FeatureExistenceImpl();
		return featureExistence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeExistence createTypeExistence() {
		TypeExistenceImpl typeExistence = new TypeExistenceImpl();
		return typeExistence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractTypeInstance createAbstractTypeInstance() {
		AbstractTypeInstanceImpl abstractTypeInstance = new AbstractTypeInstanceImpl();
		return abstractTypeInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MissingAttribute createMissingAttribute() {
		MissingAttributeImpl missingAttribute = new MissingAttributeImpl();
		return missingAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityMismatch createMultiplicityMismatch() {
		MultiplicityMismatchImpl multiplicityMismatch = new MultiplicityMismatchImpl();
		return multiplicityMismatch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeMismatch createTypeMismatch() {
		TypeMismatchImpl typeMismatch = new TypeMismatchImpl();
		return typeMismatch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureNatureMismatch createFeatureNatureMismatch() {
		FeatureNatureMismatchImpl featureNatureMismatch = new FeatureNatureMismatchImpl();
		return featureNatureMismatch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RestrictionViolation createRestrictionViolation() {
		RestrictionViolationImpl restrictionViolation = new RestrictionViolationImpl();
		return restrictionViolation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Uncontainment createUncontainment() {
		UncontainmentImpl uncontainment = new UncontainmentImpl();
		return uncontainment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncontainmentByObject createUncontainmentByObject() {
		UncontainmentByObjectImpl uncontainmentByObject = new UncontainmentByObjectImpl();
		return uncontainmentByObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UncontainmentByMetaClass createUncontainmentByMetaClass() {
		UncontainmentByMetaClassImpl uncontainmentByMetaClass = new UncontainmentByMetaClassImpl();
		return uncontainmentByMetaClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MissingReference createMissingReference() {
		MissingReferenceImpl missingReference = new MissingReferenceImpl();
		return missingReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChecksPackage getChecksPackage() {
		return (ChecksPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ChecksPackage getPackage() {
		return ChecksPackage.eINSTANCE;
	}

} //ChecksFactoryImpl
