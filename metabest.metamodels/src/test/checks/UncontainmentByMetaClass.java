/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks;

import test.checks.expressions.MetaClassExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uncontainment By Meta Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.UncontainmentByMetaClass#getMetaclass <em>Metaclass</em>}</li>
 * </ul>
 *
 * @see test.checks.ChecksPackage#getUncontainmentByMetaClass()
 * @model
 * @generated
 */
public interface UncontainmentByMetaClass extends Uncontainment {
	/**
	 * Returns the value of the '<em><b>Metaclass</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metaclass</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metaclass</em>' containment reference.
	 * @see #setMetaclass(MetaClassExpression)
	 * @see test.checks.ChecksPackage#getUncontainmentByMetaClass_Metaclass()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MetaClassExpression getMetaclass();

	/**
	 * Sets the value of the '{@link test.checks.UncontainmentByMetaClass#getMetaclass <em>Metaclass</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metaclass</em>' containment reference.
	 * @see #getMetaclass()
	 * @generated
	 */
	void setMetaclass(MetaClassExpression value);

} // UncontainmentByMetaClass
