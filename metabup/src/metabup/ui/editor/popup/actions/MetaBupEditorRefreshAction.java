package metabup.ui.editor.popup.actions;

import metabup.extensionpoints.MetaBupMetaModelExportActionContribution;

public class MetaBupEditorRefreshAction extends MetaBupMetaModelExportActionContribution {

	public MetaBupEditorRefreshAction() {
		// TODO Auto-generated constructor stub		
	}
	
	
	public void run() {
		super.editor.doSave(null);
		super.editor.refreshViewer();
	}
}
