/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.sketches.legend;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see metabup.sketches.legend.LegendFactory
 * @model kind="package"
 * @generated
 */
public interface LegendPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "metabup.sketches.legend";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/metabup.sketches/model/metabup.sketches.legend.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "metabup.sketches";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LegendPackage eINSTANCE = metabup.sketches.legend.impl.LegendPackageImpl.init();

	/**
	 * The meta object id for the '{@link metabup.sketches.legend.impl.LegendImpl <em>Legend</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.legend.impl.LegendImpl
	 * @see metabup.sketches.legend.impl.LegendPackageImpl#getLegend()
	 * @generated
	 */
	int LEGEND = 0;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND__ELEMENTS = 0;

	/**
	 * The number of structural features of the '<em>Legend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Legend</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.legend.impl.LegendElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.legend.impl.LegendElementImpl
	 * @see metabup.sketches.legend.impl.LegendPackageImpl#getLegendElement()
	 * @generated
	 */
	int LEGEND_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Original Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_ELEMENT__ORIGINAL_SYMBOL_NAME = 0;

	/**
	 * The feature id for the '<em><b>New Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_ELEMENT__NEW_SYMBOL_NAME = 1;

	/**
	 * The feature id for the '<em><b>Default Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_ELEMENT__DEFAULT_WIDTH = 2;

	/**
	 * The feature id for the '<em><b>Default Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_ELEMENT__DEFAULT_HEIGHT = 3;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_ELEMENT__COLOR = 4;

	/**
	 * The feature id for the '<em><b>Transparent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_ELEMENT__TRANSPARENT = 5;

	/**
	 * The feature id for the '<em><b>File</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_ELEMENT__FILE = 6;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_ELEMENT_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEGEND_ELEMENT_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link metabup.sketches.legend.Legend <em>Legend</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Legend</em>'.
	 * @see metabup.sketches.legend.Legend
	 * @generated
	 */
	EClass getLegend();

	/**
	 * Returns the meta object for the containment reference list '{@link metabup.sketches.legend.Legend#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see metabup.sketches.legend.Legend#getElements()
	 * @see #getLegend()
	 * @generated
	 */
	EReference getLegend_Elements();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.legend.LegendElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see metabup.sketches.legend.LegendElement
	 * @generated
	 */
	EClass getLegendElement();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.legend.LegendElement#getOriginalSymbolName <em>Original Symbol Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Original Symbol Name</em>'.
	 * @see metabup.sketches.legend.LegendElement#getOriginalSymbolName()
	 * @see #getLegendElement()
	 * @generated
	 */
	EAttribute getLegendElement_OriginalSymbolName();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.legend.LegendElement#getNewSymbolName <em>New Symbol Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>New Symbol Name</em>'.
	 * @see metabup.sketches.legend.LegendElement#getNewSymbolName()
	 * @see #getLegendElement()
	 * @generated
	 */
	EAttribute getLegendElement_NewSymbolName();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.legend.LegendElement#getDefaultWidth <em>Default Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Width</em>'.
	 * @see metabup.sketches.legend.LegendElement#getDefaultWidth()
	 * @see #getLegendElement()
	 * @generated
	 */
	EAttribute getLegendElement_DefaultWidth();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.legend.LegendElement#getDefaultHeight <em>Default Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Height</em>'.
	 * @see metabup.sketches.legend.LegendElement#getDefaultHeight()
	 * @see #getLegendElement()
	 * @generated
	 */
	EAttribute getLegendElement_DefaultHeight();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.legend.LegendElement#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see metabup.sketches.legend.LegendElement#getColor()
	 * @see #getLegendElement()
	 * @generated
	 */
	EAttribute getLegendElement_Color();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.legend.LegendElement#isTransparent <em>Transparent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transparent</em>'.
	 * @see metabup.sketches.legend.LegendElement#isTransparent()
	 * @see #getLegendElement()
	 * @generated
	 */
	EAttribute getLegendElement_Transparent();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.legend.LegendElement#getFile <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>File</em>'.
	 * @see metabup.sketches.legend.LegendElement#getFile()
	 * @see #getLegendElement()
	 * @generated
	 */
	EAttribute getLegendElement_File();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LegendFactory getLegendFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link metabup.sketches.legend.impl.LegendImpl <em>Legend</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.legend.impl.LegendImpl
		 * @see metabup.sketches.legend.impl.LegendPackageImpl#getLegend()
		 * @generated
		 */
		EClass LEGEND = eINSTANCE.getLegend();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEGEND__ELEMENTS = eINSTANCE.getLegend_Elements();

		/**
		 * The meta object literal for the '{@link metabup.sketches.legend.impl.LegendElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.legend.impl.LegendElementImpl
		 * @see metabup.sketches.legend.impl.LegendPackageImpl#getLegendElement()
		 * @generated
		 */
		EClass LEGEND_ELEMENT = eINSTANCE.getLegendElement();

		/**
		 * The meta object literal for the '<em><b>Original Symbol Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEGEND_ELEMENT__ORIGINAL_SYMBOL_NAME = eINSTANCE.getLegendElement_OriginalSymbolName();

		/**
		 * The meta object literal for the '<em><b>New Symbol Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEGEND_ELEMENT__NEW_SYMBOL_NAME = eINSTANCE.getLegendElement_NewSymbolName();

		/**
		 * The meta object literal for the '<em><b>Default Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEGEND_ELEMENT__DEFAULT_WIDTH = eINSTANCE.getLegendElement_DefaultWidth();

		/**
		 * The meta object literal for the '<em><b>Default Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEGEND_ELEMENT__DEFAULT_HEIGHT = eINSTANCE.getLegendElement_DefaultHeight();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEGEND_ELEMENT__COLOR = eINSTANCE.getLegendElement_Color();

		/**
		 * The meta object literal for the '<em><b>Transparent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEGEND_ELEMENT__TRANSPARENT = eINSTANCE.getLegendElement_Transparent();

		/**
		 * The meta object literal for the '<em><b>File</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LEGEND_ELEMENT__FILE = eINSTANCE.getLegendElement_File();

	}

} //LegendPackage
