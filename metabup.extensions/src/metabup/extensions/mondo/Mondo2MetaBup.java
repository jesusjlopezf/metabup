/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.extensions.mondo;

import java.io.File;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.mondo.editor.extensionpoints.IProcessMetaModelImplementation;
import org.mondo.editor.extensionpoints.ValidationInfo;

import metabup.ui.editor.wizards.NewMetaBupProjectFromEcoreWizard;

public class Mondo2MetaBup implements IProcessMetaModelImplementation {

	@Override
	public boolean execute(EPackage ePack) {
		Resource eResource = ePack.eResource();
        URI uri = eResource.getURI();

		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace()
				.getRoot();
		/** create an new IPath from the URI (decode blanks etc.) */
		IPath path = new Path(uri.toPlatformString(true));
		/** finally resolve the file with the workspace */
		IFile file = myWorkspaceRoot.getFile(path);
		//System.out.println("extension: " + file.getFileExtension());
		if (file != null && file.getLocation() != null) {

			/** get java.io.File object */
			File f = file.getLocation().toFile();
			Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
			WizardDialog dialog = new WizardDialog(shell, new NewMetaBupProjectFromEcoreWizard(f));
			dialog.open(); 
			return true;
		}
			
		return false;
			
	}

	@Override
	public ValidationInfo validate(EPackage ePack) {
		return null;
	}

}
