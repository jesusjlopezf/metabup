/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.ui.views;

import java.util.ArrayList;
import java.util.List;

import metabest.test.metamodel.MBMRuntimeModule;
import metabest.Activator;
import metabest.validation.MetamodelAssertionEvaluationOutput;
import metabest.validation.MetamodelPermutedAssertionEvaluationOutput;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Attribute;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;
import metabup.ui.editor.editors.MetamodelInteractiveEditor;
import metamodeltest.Annotation;
import metamodeltest.Assertion;
import metamodeltest.AssertionCall;
import metamodeltest.AssertionTest;
import metamodeltest.LibraryAssertionCall;
import metamodeltest.MetaModelTest;
import metamodeltest.ParameterElementSetValue;
import metamodeltest.impl.AssertionImpl;
import metabest.ui.views.MetamodelAssertionEvaluationViewTreeParent;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.xtext.serializer.impl.Serializer;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class MetamodelAssertionEvaluationView extends ViewPart {
	public static final String ID = "metabest.ui.views.MetamodelAssertionEvaluationView";
	private TreeViewer viewer;

	private Action outlineMatchesAction;
	
	private Resource metamodelTestResource;
	
	private MetamodelAssertionEvaluationViewTreeParent invisibleRoot;
	
	class TestTreeParent extends MetamodelAssertionEvaluationViewTreeParent {
		boolean valid = true;
		int validChildren = 0;
		double percentage = 0;
		boolean withWarnings = false;
		
		public TestTreeParent(Object obj) {
			super(obj);
		}
		
		public void addChild(MetamodelAssertionEvaluationViewTreeParent child){
			if(child.getObject() instanceof MetamodelAssertionEvaluationOutput){
				super.addChild(child);
				
				if(((MetamodelAssertionEvaluationOutput)child.getObject()).isTrue()) validChildren++;
				else if(((MetamodelAssertionEvaluationOutput)child.getObject()).isWarning()) withWarnings = true;
				else valid = false;
				
				percentage = round(new Double(((float)validChildren/super.getChildren().length)*100));
				super.name = object.toString() + " (" + percentage + "%) ";
				if(withWarnings) super.name += " [with warnings]";
			}
		}
		
		private double round(double num){
			return Math.rint(num*100)/100;
		}
		
		public boolean isValid(){
			return valid;
		}
		
		public boolean withWarnings(){
			return withWarnings;
		}
	}
	
	class ViewContentProvider implements IStructuredContentProvider, ITreeContentProvider{
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			if (parent.equals(getViewSite())) {
				//if (invisibleRoot!=null) refresh(metamodelTestResource);
				return getChildren(invisibleRoot);
			}
			return getChildren(parent);
		}
		
		public Object getParent(Object child) {
			if (child instanceof MetamodelAssertionEvaluationViewTreeObject) {
				return ((MetamodelAssertionEvaluationViewTreeObject)child).getParent();
			}
			return null;
		}
		public Object [] getChildren(Object parent) {
			if (parent instanceof MetamodelAssertionEvaluationViewTreeParent) {
				return ((MetamodelAssertionEvaluationViewTreeParent)parent).getChildren();
			}
			return new Object[0];
		}
		public boolean hasChildren(Object parent) {
			if (parent instanceof MetamodelAssertionEvaluationViewTreeParent)
				return ((MetamodelAssertionEvaluationViewTreeParent)parent).hasChildren();
			return false;
		}				
	}
	
	class ViewLabelProvider extends LabelProvider {
		public String getText(Object obj) {
			return obj.toString();
		}
		
		public Image getImage(Object obj) {
			ImageDescriptor desc = null;
			
			if(((MetamodelAssertionEvaluationViewTreeObject)obj).getObject() instanceof MetamodelAssertionEvaluationOutput){
				MetamodelAssertionEvaluationOutput maeo = (MetamodelAssertionEvaluationOutput)((MetamodelAssertionEvaluationViewTreeObject)obj).getObject();
				
				if(!maeo.isTrue()){
					if(maeo.isWarning()) desc = Activator.getImageDescriptor("icons/signed_warning.gif");
					else desc = Activator.getImageDescriptor("icons/signed_no.gif");				
					if ( desc != null ) return desc.createImage();
				}
				else{
					desc = Activator.getImageDescriptor("icons/signed_yes.gif");				
					if ( desc != null ) return desc.createImage();
				}
			}else{
				if(((MetamodelAssertionEvaluationViewTreeObject) obj).getObject() instanceof AssertionTest){
					MetamodelAssertionEvaluationViewTreeParent tf = ((MetamodelAssertionEvaluationViewTreeParent)obj);
					
					boolean valid = true;
					
					for(int i = 0; i<tf.getChildren().length && valid; i++){
						MetamodelAssertionEvaluationViewTreeParent to = tf.getChildren()[i];
						
						if(to.getObject() instanceof MetamodelAssertionEvaluationOutput){
							MetamodelAssertionEvaluationOutput maeo = (MetamodelAssertionEvaluationOutput)to.getObject();
							if(!maeo.isTrue() && !maeo.isWarning()) valid = false;
						}
					}
					
					if(valid) desc = Activator.getImageDescriptor("icons/testok.png");
					else desc = Activator.getImageDescriptor("icons/testfail.png");								
					if ( desc != null ) return desc.createImage();
				}else{
					if(((MetamodelAssertionEvaluationViewTreeObject)obj).getObject() instanceof MetaClass){
						desc = Activator.getImageDescriptor("icons/class.gif");				
						if ( desc != null ) return desc.createImage();
					}else{
						if(((MetamodelAssertionEvaluationViewTreeObject)obj).getObject() instanceof Attribute){
							desc = Activator.getImageDescriptor("icons/attribute.gif");				
							if ( desc != null ) return desc.createImage();
						}else{
							if(((MetamodelAssertionEvaluationViewTreeObject)obj).getObject() instanceof Reference){
								desc = Activator.getImageDescriptor("icons/reference.gif");				
								if ( desc != null ) return desc.createImage();
							}else{		
								if(obj instanceof MetamodelAssertionEvaluationViewTreeParent){
									MetamodelAssertionEvaluationViewTreeParent tp = (MetamodelAssertionEvaluationViewTreeParent)obj;
									
									if(tp.getChildren() != null && tp.getChildren().length > 0 && tp.getChildren()[0] instanceof TestTreeParent){
										boolean valid = true;
										boolean withWarnings = false;
										
										for(int i = 0 ; i < tp.getChildren().length && valid; i++){
											TestTreeParent ttp = (TestTreeParent) tp.getChildren()[i];
											if(!ttp.isValid()) valid = false;
										}
										
										if(valid) desc = Activator.getImageDescriptor("icons/tsuiteok.png");
										else desc = Activator.getImageDescriptor("icons/tsuitefail.png");										
										if ( desc != null ) return desc.createImage();
									}
								}									
							}
						}						
					}
				}
			}
			return null;
		}
	}
	
	class NameSorter extends ViewerSorter { }	

	public MetamodelAssertionEvaluationView() {
		// TODO Auto-generated constructor stub
	}
	
	public void refresh(Resource resource, IProject project){
		this.metamodelTestResource = resource;
		if(metamodelTestResource == null) return;
		refreshJob.schedule();				
		return;
	}
	
	private void setViewerSteady() {
	    Display.getDefault().asyncExec(new Runnable() {
	      public void run() {	
	    	//viewer.refresh();
	  		viewer.expandToLevel(2);
	  		return;
	      }
	    });
	}
	
	Job refreshJob = new Job("Performing tests"){
		@Override 
	    protected IStatus run(IProgressMonitor monitor) { 
			
	        List<MetamodelAssertionEvaluationViewTreeParent> tests = new ArrayList<MetamodelAssertionEvaluationViewTreeParent>();
			
			if(metamodelTestResource.getAllContents().next() instanceof MetaModelTest){
				MetaModelTest mmt = (MetaModelTest) metamodelTestResource.getContents().get(0);
				if(mmt.getTests() == null || mmt.getTests().isEmpty()) return Status.CANCEL_STATUS;
				
				int totalTests = 0;				
				for(AssertionTest at : mmt.getTests()) totalTests++;
				
		        monitor.beginTask("Performing tests ...", totalTests);
				
				for(AssertionTest at : mmt.getTests()){
			       // monitor.subTask("Testing " + at.getDescription());
					TestTreeParent testNode =  new TestTreeParent(at);	
					
					for(AssertionCall ac : at.getAssertions()){
						// spread test annotations to test assertions
						ac.getAnnotations().addAll(EcoreUtil.copyAll(at.getAnnotations()));
						
						Assertion a = null;
						String name = null;
						List<ParameterElementSetValue> permuteParams = new ArrayList<ParameterElementSetValue>();
						
						if(ac instanceof LibraryAssertionCall){
							LibraryAssertionCall lac = (LibraryAssertionCall) ac;
							permuteParams = lac.getPermutedParameters();
							
							a = EcoreUtil.copy((AssertionImpl)(lac.getReferencedAssertion().getAssertion()));
							a.getAnnotations().addAll(EcoreUtil.copyAll(lac.getAnnotations()));
							if(lac.getDescription() != null) a.setDescription(lac.getDescription());
							List<String> output = null;
							
							if(lac.getParameters() != null && !lac.getParameters().isEmpty()){
								output = a.putValuesOnParameters(lac.getParameters());
								
								if(!output.isEmpty()){
									 final String PID = Activator.PLUGIN_ID;
									 MultiStatus info = new MultiStatus(PID, 1, ((LibraryAssertionCall) a).getReferencedAssertion().getName() + " call returned errors", null);
									 for(String msg : output) info.add(new Status(IStatus.ERROR, PID, 1, msg, null));
									 ErrorDialog.openError(null, "Error", null, info);
								}									
							}
							
							if(a.getDescription() != null) name = a.getDescription();
							else{
								Injector injector = Guice.createInjector(new MBMRuntimeModule());
								Serializer serializer = injector.getInstance(Serializer.class);
								name = serializer.serialize(a);
									
								if(name.startsWith("@")){
									name = name.substring(name.lastIndexOf("@"), name.length());
									String substring = name.substring(name.lastIndexOf("@"), name.indexOf(" "));
									name = name.substring(substring.length()+1);
								}
							}
						}else if(ac instanceof Assertion){
							a = (Assertion)ac;
							
							if(a.getDescription() != null) name = a.getDescription();
							else{
								Injector injector = Guice.createInjector(new MBMRuntimeModule());
								Serializer serializer = injector.getInstance(Serializer.class);
								name = serializer.serialize(a);
								
								if(name.startsWith("@")){
									name = name.substring(name.lastIndexOf("@"), name.length());
									String substring = name.substring(name.lastIndexOf("@"), name.indexOf(" "));
									name = name.substring(substring.length()+1);
								}
							}
						}
						
						boolean warning = false;
						for(Annotation ann : a.getAnnotations()) if(ann.getName().equals("warning")) warning = true;
						
						String metamodelUri = at.getMetamodelURI();
						if(metamodelUri == null) return Status.CANCEL_STATUS;
					    IWorkspace workspace = ResourcesPlugin.getWorkspace();
					    IPath p = new Path(metamodelUri.replace(".mbup", ".mm.xmi"));
					    IFile mmFile = workspace.getRoot().getFile(p);
					    if(!mmFile.exists()) return Status.CANCEL_STATUS;
					    		
					    ResourceSet rs = new ResourceSetImpl();
					    org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(mmFile.getLocationURI().toString());
						Resource r = rs.getResource(uri, true);
						org.eclipse.emf.common.util.TreeIterator<EObject> it = r.getAllContents();
						
						MetaModel mm = null;
						
						while(it.hasNext() && mm == null){
							EObject eo = it.next();
							if(eo instanceof MetaModel){
								mm = (MetaModel)eo;
							}
						}
						
						if(mm != null){
							if(permuteParams.isEmpty() || permuteParams == null){
								MetamodelAssertionEvaluationOutput ev = new MetamodelAssertionEvaluationOutput(a, name, warning);
								ev.evaluate(mm);
								MetamodelAssertionEvaluationViewTreeParent testAssertion = new MetamodelAssertionEvaluationViewTreeParent(ev);
								
								// this level will display the elements to outline
								if(ev.isTrue() && ev.getMatches() != null && !ev.getMatches().isEmpty()){							
									for(AnnotatedElement ae : ev.getMatches()){
										MetamodelAssertionEvaluationViewTreeParent to = null;
										if(ae instanceof MetaClass) to = new MetamodelAssertionEvaluationViewTreeParent((MetaClass)ae);
										else if(ae instanceof Reference) to = new MetamodelAssertionEvaluationViewTreeParent((Reference)ae);
										else if(ae instanceof Attribute) to = new MetamodelAssertionEvaluationViewTreeParent((Attribute)ae);
										if(to != null) testAssertion.addChild(to);
									}
								}
								
								if(!ev.isTrue() && ev.getErrors() != null && !ev.getErrors().isEmpty()){							
									for(AnnotatedElement ae : ev.getErrors()){
										MetamodelAssertionEvaluationViewTreeParent to = null;
										if(ae instanceof MetaClass) to = new MetamodelAssertionEvaluationViewTreeParent((MetaClass)ae);
										else if(ae instanceof Reference) to = new MetamodelAssertionEvaluationViewTreeParent((Reference)ae);
										else if(ae instanceof Attribute) to = new MetamodelAssertionEvaluationViewTreeParent((Attribute)ae);
										if(to != null) testAssertion.addChild(to);
									}
								}
									
								if(testAssertion !=null) testNode.addChild(testAssertion);
							}else{
								//permuted evaluation
								MetamodelPermutedAssertionEvaluationOutput pev = new MetamodelPermutedAssertionEvaluationOutput(a, name, warning);
								pev.evaluate(mm, permuteParams);
								MetamodelAssertionEvaluationViewTreeParent testAssertion = new MetamodelAssertionEvaluationViewTreeParent(pev);
								
								// this level will display the elements to outline
								if(pev.isTrue() && pev.getPermutationMatchTree() != null){
									MetamodelAssertionEvaluationViewTreeParent et = (MetamodelAssertionEvaluationViewTreeParent)pev.getPermutationMatchTree().getRoot().getViewTree();									
									
									for(MetamodelAssertionEvaluationViewTreeParent tp : et.getChildren())
										if(tp.hasChildren())
											testAssertion.addChild(tp);
								}
								
								/*if(!pev.isTrue() && pev.getErrors() != null && !pev.getErrors().isEmpty()){							
									for(AnnotatedElement ae : pev.getErrors()){
										MetamodelAssertionEvaluationViewTreeObject to = null;
										if(ae instanceof MetaClass) to = new MetamodelAssertionEvaluationViewTreeObject((MetaClass)ae);
										else if(ae instanceof Reference) to = new MetamodelAssertionEvaluationViewTreeObject((Reference)ae);
										else if(ae instanceof Attribute) to = new MetamodelAssertionEvaluationViewTreeObject((Attribute)ae);
										if(to != null) testAssertion.addChild(to);
									}
								}*/
									
								if(testAssertion !=null) testNode.addChild(testAssertion);
							}
						}else showMessage("MetaModel not found");
					}
					
					tests.add(testNode);	
					monitor.worked(1);
				}
				
				String testFileName = metamodelTestResource.getURI().toFileString();
				if(testFileName.lastIndexOf("/") != -1) testFileName = testFileName.substring(testFileName.lastIndexOf("/")+1);
				else testFileName = testFileName.substring(testFileName.lastIndexOf("\\")+1);
				MetamodelAssertionEvaluationViewTreeParent root = new MetamodelAssertionEvaluationViewTreeParent(testFileName);
				for(MetamodelAssertionEvaluationViewTreeParent t : tests)root.addChild(t);
				
				invisibleRoot = new MetamodelAssertionEvaluationViewTreeParent("");
				invisibleRoot.addChild(root);

			}
			
	        monitor.done();
	        setViewerSteady();	        
	        return Status.OK_STATUS; 
	    } 
		
	};
	
	private boolean processPermutedEvaluation(MetamodelAssertionEvaluationOutput ev, MetaModel mm){
		
		return false;
	}
	
	private void outlineOutput(){
		  ISelection selection = viewer.getSelection();
		  Object obj = ((IStructuredSelection)selection).getFirstElement();
		  
		  MetamodelAssertionEvaluationViewTreeParent to = (MetamodelAssertionEvaluationViewTreeParent)obj;
		  System.out.println("to = " + to.getName());
		  while(!(to.getObject() instanceof MetamodelAssertionEvaluationOutput) && to != null) to = to.getParent();		  
		  if(to == null) return;
		  
		  System.out.println("to = " + to.getName());
		  
		  MetamodelAssertionEvaluationOutput maeo = (MetamodelAssertionEvaluationOutput)to.getObject();
		  
		  System.out.println("maeo = " + maeo.getRationale());
		  
		  MetamodelAssertionEvaluationViewTreeParent tp = (MetamodelAssertionEvaluationViewTreeParent) to.getParent();	
		  
		  System.out.println("tp = " + tp.getName());
		  
		  AssertionTest at = (AssertionTest)tp.getObject();
		  
		  IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		  IResource resource = root.findMember(new Path(at.getMetamodelURI()));
			
		  IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		  
		  try {
			  IDE.openEditor(page, (IFile)resource, true);
			  
			  if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor() instanceof MetamodelInteractiveEditor){
				  MetamodelInteractiveEditor editor = (MetamodelInteractiveEditor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();					
				  IWorkspace workspace = ResourcesPlugin.getWorkspace();
				  IPath p = new Path(at.getMetamodelURI().replace(".mbup", ".mm.xmi"));
				  IFile mmFile = workspace.getRoot().getFile(p);
				  if(!mmFile.exists()) return;
				    		
				  ResourceSet rs = new ResourceSetImpl();
				  org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(mmFile.getLocationURI().toString());
				  Resource r = rs.getResource(uri, true);
				  MetaModel mm = (MetaModel)r.getContents().get(0);
				  if(maeo.isWarning()) editor.addAssertionViewer(mm, maeo.getMatches(), maeo.getErrors(), null);
				  else if(maeo.isTrue()) editor.addAssertionViewer(mm, maeo.getMatches(), null, null);
				  else if(!maeo.isTrue()) editor.addAssertionViewer(mm, null, null, maeo.getErrors());
			  }

		  } catch (PartInitException e) {
			  return;
		  }
	}
	
	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {					
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		
		outlineMatchesAction = new Action(){
			public void run(){
				outlineOutput();
			}
		};
			
		outlineMatchesAction.setText("Outline matches");
		outlineMatchesAction.setToolTipText("Outline matches");
		outlineMatchesAction.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
				getImageDescriptor(IDE.SharedImages.IMG_OPEN_MARKER));
		
		IActionBars actionBars = getViewSite().getActionBars();
		IMenuManager dropDownMenu = actionBars.getMenuManager();
		IToolBarManager toolBar = actionBars.getToolBarManager();
		dropDownMenu.add(outlineMatchesAction);
		toolBar.add(outlineMatchesAction);
		
		// Creating the Screen
		Section section = toolkit.createSection(parent, Section.DESCRIPTION | Section.TITLE_BAR);
		section.setText("MetaModel validation set"); //$NON-NLS-1$
		//section.setDescription("MetaModel validation output");
		
		// Composite for storing the data
		Composite client = toolkit.createComposite(section, SWT.WRAP);
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.marginWidth = 2;
		layout.marginHeight = 2;
		client.setLayout(layout);		  		    						
		
		Tree t = toolkit.createTree(client, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);    						
		    
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 50;
		gd.widthHint = 50;
		t.setLayoutData(gd);		    		    		    		  		    		    		    
		toolkit.paintBordersFor(client);
		    
		section.setClient(client);
		viewer = new TreeViewer(t);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		//viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());			

		// Create the help context id for the viewer's control
		PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(), "metabup.perspective.viewer");
		//makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
			 
		viewer.setInput(getViewSite());
	}
	
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				MetamodelAssertionEvaluationView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(outlineMatchesAction);
		//manager.add(new Separator());
		//manager.add(action2);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(outlineMatchesAction);
		//manager.add(action2);
		// Other plug-ins can contribute there actions here
		//manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(outlineMatchesAction);
		//manager.add(action2);
	}
	
	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				outlineMatchesAction.run();
			}
		});
	}
		
	private void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"Validation View",
			message);
	}
		
	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
		
}