/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import metabup.services.MetamodelDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMetamodelDSLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'metamodel'", "'class'", "'{'", "'}'", "'<'", "','", "';'", "'attr'", "':'", "'['", "'..'", "']'", "'ref'", "'-'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMetamodelDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMetamodelDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMetamodelDSLParser.tokenNames; }
    public String getGrammarFileName() { return "../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g"; }


     
     	private MetamodelDSLGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(MetamodelDSLGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleMetaModel"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:60:1: entryRuleMetaModel : ruleMetaModel EOF ;
    public final void entryRuleMetaModel() throws RecognitionException {
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:61:1: ( ruleMetaModel EOF )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:62:1: ruleMetaModel EOF
            {
             before(grammarAccess.getMetaModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaModel_in_entryRuleMetaModel61);
            ruleMetaModel();

            state._fsp--;

             after(grammarAccess.getMetaModelRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMetaModel"


    // $ANTLR start "ruleMetaModel"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:69:1: ruleMetaModel : ( ( rule__MetaModel__Group__0 ) ) ;
    public final void ruleMetaModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:73:2: ( ( ( rule__MetaModel__Group__0 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:74:1: ( ( rule__MetaModel__Group__0 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:74:1: ( ( rule__MetaModel__Group__0 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:75:1: ( rule__MetaModel__Group__0 )
            {
             before(grammarAccess.getMetaModelAccess().getGroup()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:76:1: ( rule__MetaModel__Group__0 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:76:2: rule__MetaModel__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__0_in_ruleMetaModel94);
            rule__MetaModel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMetaModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMetaModel"


    // $ANTLR start "entryRuleFeature"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:88:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:89:1: ( ruleFeature EOF )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:90:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_entryRuleFeature121);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeature128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:97:1: ruleFeature : ( ( rule__Feature__Alternatives ) ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:101:2: ( ( ( rule__Feature__Alternatives ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:102:1: ( ( rule__Feature__Alternatives ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:102:1: ( ( rule__Feature__Alternatives ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:103:1: ( rule__Feature__Alternatives )
            {
             before(grammarAccess.getFeatureAccess().getAlternatives()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:104:1: ( rule__Feature__Alternatives )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:104:2: rule__Feature__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__Feature__Alternatives_in_ruleFeature154);
            rule__Feature__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleMetaClass"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:116:1: entryRuleMetaClass : ruleMetaClass EOF ;
    public final void entryRuleMetaClass() throws RecognitionException {
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:117:1: ( ruleMetaClass EOF )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:118:1: ruleMetaClass EOF
            {
             before(grammarAccess.getMetaClassRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClass_in_entryRuleMetaClass181);
            ruleMetaClass();

            state._fsp--;

             after(grammarAccess.getMetaClassRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaClass188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMetaClass"


    // $ANTLR start "ruleMetaClass"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:125:1: ruleMetaClass : ( ( rule__MetaClass__Group__0 ) ) ;
    public final void ruleMetaClass() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:129:2: ( ( ( rule__MetaClass__Group__0 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:130:1: ( ( rule__MetaClass__Group__0 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:130:1: ( ( rule__MetaClass__Group__0 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:131:1: ( rule__MetaClass__Group__0 )
            {
             before(grammarAccess.getMetaClassAccess().getGroup()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:132:1: ( rule__MetaClass__Group__0 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:132:2: rule__MetaClass__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__0_in_ruleMetaClass214);
            rule__MetaClass__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMetaClassAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMetaClass"


    // $ANTLR start "entryRuleAttribute"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:144:1: entryRuleAttribute : ruleAttribute EOF ;
    public final void entryRuleAttribute() throws RecognitionException {
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:145:1: ( ruleAttribute EOF )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:146:1: ruleAttribute EOF
            {
             before(grammarAccess.getAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_entryRuleAttribute241);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getAttributeRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttribute248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:153:1: ruleAttribute : ( ( rule__Attribute__Group__0 ) ) ;
    public final void ruleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:157:2: ( ( ( rule__Attribute__Group__0 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:158:1: ( ( rule__Attribute__Group__0 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:158:1: ( ( rule__Attribute__Group__0 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:159:1: ( rule__Attribute__Group__0 )
            {
             before(grammarAccess.getAttributeAccess().getGroup()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:160:1: ( rule__Attribute__Group__0 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:160:2: rule__Attribute__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__0_in_ruleAttribute274);
            rule__Attribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleReference"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:172:1: entryRuleReference : ruleReference EOF ;
    public final void entryRuleReference() throws RecognitionException {
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:173:1: ( ruleReference EOF )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:174:1: ruleReference EOF
            {
             before(grammarAccess.getReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReference_in_entryRuleReference301);
            ruleReference();

            state._fsp--;

             after(grammarAccess.getReferenceRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReference308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:181:1: ruleReference : ( ( rule__Reference__Group__0 ) ) ;
    public final void ruleReference() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:185:2: ( ( ( rule__Reference__Group__0 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:186:1: ( ( rule__Reference__Group__0 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:186:1: ( ( rule__Reference__Group__0 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:187:1: ( rule__Reference__Group__0 )
            {
             before(grammarAccess.getReferenceAccess().getGroup()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:188:1: ( rule__Reference__Group__0 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:188:2: rule__Reference__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__0_in_ruleReference334);
            rule__Reference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleEString"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:200:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:201:1: ( ruleEString EOF )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:202:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString361);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:209:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:213:2: ( ( ( rule__EString__Alternatives ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:214:1: ( ( rule__EString__Alternatives ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:214:1: ( ( rule__EString__Alternatives ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:215:1: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:216:1: ( rule__EString__Alternatives )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:216:2: rule__EString__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__EString__Alternatives_in_ruleEString394);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEInt"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:228:1: entryRuleEInt : ruleEInt EOF ;
    public final void entryRuleEInt() throws RecognitionException {
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:229:1: ( ruleEInt EOF )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:230:1: ruleEInt EOF
            {
             before(grammarAccess.getEIntRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_entryRuleEInt421);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getEIntRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEInt428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:237:1: ruleEInt : ( ( rule__EInt__Group__0 ) ) ;
    public final void ruleEInt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:241:2: ( ( ( rule__EInt__Group__0 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:242:1: ( ( rule__EInt__Group__0 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:242:1: ( ( rule__EInt__Group__0 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:243:1: ( rule__EInt__Group__0 )
            {
             before(grammarAccess.getEIntAccess().getGroup()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:244:1: ( rule__EInt__Group__0 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:244:2: rule__EInt__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__EInt__Group__0_in_ruleEInt454);
            rule__EInt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEIntAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "rule__Feature__Alternatives"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:256:1: rule__Feature__Alternatives : ( ( ruleAttribute ) | ( ruleReference ) );
    public final void rule__Feature__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:260:1: ( ( ruleAttribute ) | ( ruleReference ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==18) ) {
                alt1=1;
            }
            else if ( (LA1_0==23) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:261:1: ( ruleAttribute )
                    {
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:261:1: ( ruleAttribute )
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:262:1: ruleAttribute
                    {
                     before(grammarAccess.getFeatureAccess().getAttributeParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_rule__Feature__Alternatives490);
                    ruleAttribute();

                    state._fsp--;

                     after(grammarAccess.getFeatureAccess().getAttributeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:267:6: ( ruleReference )
                    {
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:267:6: ( ruleReference )
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:268:1: ruleReference
                    {
                     before(grammarAccess.getFeatureAccess().getReferenceParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleReference_in_rule__Feature__Alternatives507);
                    ruleReference();

                    state._fsp--;

                     after(grammarAccess.getFeatureAccess().getReferenceParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:278:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:282:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:283:1: ( RULE_STRING )
                    {
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:283:1: ( RULE_STRING )
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:284:1: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__EString__Alternatives539); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:289:6: ( RULE_ID )
                    {
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:289:6: ( RULE_ID )
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:290:1: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__EString__Alternatives556); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__MetaModel__Group__0"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:302:1: rule__MetaModel__Group__0 : rule__MetaModel__Group__0__Impl rule__MetaModel__Group__1 ;
    public final void rule__MetaModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:306:1: ( rule__MetaModel__Group__0__Impl rule__MetaModel__Group__1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:307:2: rule__MetaModel__Group__0__Impl rule__MetaModel__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__0__Impl_in_rule__MetaModel__Group__0586);
            rule__MetaModel__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__1_in_rule__MetaModel__Group__0589);
            rule__MetaModel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__0"


    // $ANTLR start "rule__MetaModel__Group__0__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:314:1: rule__MetaModel__Group__0__Impl : ( () ) ;
    public final void rule__MetaModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:318:1: ( ( () ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:319:1: ( () )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:319:1: ( () )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:320:1: ()
            {
             before(grammarAccess.getMetaModelAccess().getMetaModelAction_0()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:321:1: ()
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:323:1: 
            {
            }

             after(grammarAccess.getMetaModelAccess().getMetaModelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__0__Impl"


    // $ANTLR start "rule__MetaModel__Group__1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:333:1: rule__MetaModel__Group__1 : rule__MetaModel__Group__1__Impl rule__MetaModel__Group__2 ;
    public final void rule__MetaModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:337:1: ( rule__MetaModel__Group__1__Impl rule__MetaModel__Group__2 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:338:2: rule__MetaModel__Group__1__Impl rule__MetaModel__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__1__Impl_in_rule__MetaModel__Group__1647);
            rule__MetaModel__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__2_in_rule__MetaModel__Group__1650);
            rule__MetaModel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__1"


    // $ANTLR start "rule__MetaModel__Group__1__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:345:1: rule__MetaModel__Group__1__Impl : ( 'metamodel' ) ;
    public final void rule__MetaModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:349:1: ( ( 'metamodel' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:350:1: ( 'metamodel' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:350:1: ( 'metamodel' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:351:1: 'metamodel'
            {
             before(grammarAccess.getMetaModelAccess().getMetamodelKeyword_1()); 
            match(input,11,FollowSets000.FOLLOW_11_in_rule__MetaModel__Group__1__Impl678); 
             after(grammarAccess.getMetaModelAccess().getMetamodelKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__1__Impl"


    // $ANTLR start "rule__MetaModel__Group__2"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:364:1: rule__MetaModel__Group__2 : rule__MetaModel__Group__2__Impl rule__MetaModel__Group__3 ;
    public final void rule__MetaModel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:368:1: ( rule__MetaModel__Group__2__Impl rule__MetaModel__Group__3 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:369:2: rule__MetaModel__Group__2__Impl rule__MetaModel__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__2__Impl_in_rule__MetaModel__Group__2709);
            rule__MetaModel__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__3_in_rule__MetaModel__Group__2712);
            rule__MetaModel__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__2"


    // $ANTLR start "rule__MetaModel__Group__2__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:376:1: rule__MetaModel__Group__2__Impl : ( ( rule__MetaModel__NameAssignment_2 ) ) ;
    public final void rule__MetaModel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:380:1: ( ( ( rule__MetaModel__NameAssignment_2 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:381:1: ( ( rule__MetaModel__NameAssignment_2 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:381:1: ( ( rule__MetaModel__NameAssignment_2 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:382:1: ( rule__MetaModel__NameAssignment_2 )
            {
             before(grammarAccess.getMetaModelAccess().getNameAssignment_2()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:383:1: ( rule__MetaModel__NameAssignment_2 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:383:2: rule__MetaModel__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__NameAssignment_2_in_rule__MetaModel__Group__2__Impl739);
            rule__MetaModel__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getMetaModelAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__2__Impl"


    // $ANTLR start "rule__MetaModel__Group__3"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:393:1: rule__MetaModel__Group__3 : rule__MetaModel__Group__3__Impl rule__MetaModel__Group__4 ;
    public final void rule__MetaModel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:397:1: ( rule__MetaModel__Group__3__Impl rule__MetaModel__Group__4 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:398:2: rule__MetaModel__Group__3__Impl rule__MetaModel__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__3__Impl_in_rule__MetaModel__Group__3769);
            rule__MetaModel__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__4_in_rule__MetaModel__Group__3772);
            rule__MetaModel__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__3"


    // $ANTLR start "rule__MetaModel__Group__3__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:405:1: rule__MetaModel__Group__3__Impl : ( ( rule__MetaModel__ClassesAssignment_3 ) ) ;
    public final void rule__MetaModel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:409:1: ( ( ( rule__MetaModel__ClassesAssignment_3 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:410:1: ( ( rule__MetaModel__ClassesAssignment_3 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:410:1: ( ( rule__MetaModel__ClassesAssignment_3 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:411:1: ( rule__MetaModel__ClassesAssignment_3 )
            {
             before(grammarAccess.getMetaModelAccess().getClassesAssignment_3()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:412:1: ( rule__MetaModel__ClassesAssignment_3 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:412:2: rule__MetaModel__ClassesAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__ClassesAssignment_3_in_rule__MetaModel__Group__3__Impl799);
            rule__MetaModel__ClassesAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getMetaModelAccess().getClassesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__3__Impl"


    // $ANTLR start "rule__MetaModel__Group__4"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:422:1: rule__MetaModel__Group__4 : rule__MetaModel__Group__4__Impl ;
    public final void rule__MetaModel__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:426:1: ( rule__MetaModel__Group__4__Impl )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:427:2: rule__MetaModel__Group__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaModel__Group__4__Impl_in_rule__MetaModel__Group__4829);
            rule__MetaModel__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__4"


    // $ANTLR start "rule__MetaModel__Group__4__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:433:1: rule__MetaModel__Group__4__Impl : ( ( rule__MetaModel__ClassesAssignment_4 )* ) ;
    public final void rule__MetaModel__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:437:1: ( ( ( rule__MetaModel__ClassesAssignment_4 )* ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:438:1: ( ( rule__MetaModel__ClassesAssignment_4 )* )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:438:1: ( ( rule__MetaModel__ClassesAssignment_4 )* )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:439:1: ( rule__MetaModel__ClassesAssignment_4 )*
            {
             before(grammarAccess.getMetaModelAccess().getClassesAssignment_4()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:440:1: ( rule__MetaModel__ClassesAssignment_4 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==12) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:440:2: rule__MetaModel__ClassesAssignment_4
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__MetaModel__ClassesAssignment_4_in_rule__MetaModel__Group__4__Impl856);
            	    rule__MetaModel__ClassesAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getMetaModelAccess().getClassesAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__Group__4__Impl"


    // $ANTLR start "rule__MetaClass__Group__0"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:460:1: rule__MetaClass__Group__0 : rule__MetaClass__Group__0__Impl rule__MetaClass__Group__1 ;
    public final void rule__MetaClass__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:464:1: ( rule__MetaClass__Group__0__Impl rule__MetaClass__Group__1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:465:2: rule__MetaClass__Group__0__Impl rule__MetaClass__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__0__Impl_in_rule__MetaClass__Group__0897);
            rule__MetaClass__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__1_in_rule__MetaClass__Group__0900);
            rule__MetaClass__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__0"


    // $ANTLR start "rule__MetaClass__Group__0__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:472:1: rule__MetaClass__Group__0__Impl : ( () ) ;
    public final void rule__MetaClass__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:476:1: ( ( () ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:477:1: ( () )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:477:1: ( () )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:478:1: ()
            {
             before(grammarAccess.getMetaClassAccess().getMetaClassAction_0()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:479:1: ()
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:481:1: 
            {
            }

             after(grammarAccess.getMetaClassAccess().getMetaClassAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__0__Impl"


    // $ANTLR start "rule__MetaClass__Group__1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:491:1: rule__MetaClass__Group__1 : rule__MetaClass__Group__1__Impl rule__MetaClass__Group__2 ;
    public final void rule__MetaClass__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:495:1: ( rule__MetaClass__Group__1__Impl rule__MetaClass__Group__2 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:496:2: rule__MetaClass__Group__1__Impl rule__MetaClass__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__1__Impl_in_rule__MetaClass__Group__1958);
            rule__MetaClass__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__2_in_rule__MetaClass__Group__1961);
            rule__MetaClass__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__1"


    // $ANTLR start "rule__MetaClass__Group__1__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:503:1: rule__MetaClass__Group__1__Impl : ( 'class' ) ;
    public final void rule__MetaClass__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:507:1: ( ( 'class' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:508:1: ( 'class' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:508:1: ( 'class' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:509:1: 'class'
            {
             before(grammarAccess.getMetaClassAccess().getClassKeyword_1()); 
            match(input,12,FollowSets000.FOLLOW_12_in_rule__MetaClass__Group__1__Impl989); 
             after(grammarAccess.getMetaClassAccess().getClassKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__1__Impl"


    // $ANTLR start "rule__MetaClass__Group__2"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:522:1: rule__MetaClass__Group__2 : rule__MetaClass__Group__2__Impl rule__MetaClass__Group__3 ;
    public final void rule__MetaClass__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:526:1: ( rule__MetaClass__Group__2__Impl rule__MetaClass__Group__3 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:527:2: rule__MetaClass__Group__2__Impl rule__MetaClass__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__2__Impl_in_rule__MetaClass__Group__21020);
            rule__MetaClass__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__3_in_rule__MetaClass__Group__21023);
            rule__MetaClass__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__2"


    // $ANTLR start "rule__MetaClass__Group__2__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:534:1: rule__MetaClass__Group__2__Impl : ( ( rule__MetaClass__NameAssignment_2 ) ) ;
    public final void rule__MetaClass__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:538:1: ( ( ( rule__MetaClass__NameAssignment_2 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:539:1: ( ( rule__MetaClass__NameAssignment_2 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:539:1: ( ( rule__MetaClass__NameAssignment_2 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:540:1: ( rule__MetaClass__NameAssignment_2 )
            {
             before(grammarAccess.getMetaClassAccess().getNameAssignment_2()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:541:1: ( rule__MetaClass__NameAssignment_2 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:541:2: rule__MetaClass__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__NameAssignment_2_in_rule__MetaClass__Group__2__Impl1050);
            rule__MetaClass__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getMetaClassAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__2__Impl"


    // $ANTLR start "rule__MetaClass__Group__3"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:551:1: rule__MetaClass__Group__3 : rule__MetaClass__Group__3__Impl rule__MetaClass__Group__4 ;
    public final void rule__MetaClass__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:555:1: ( rule__MetaClass__Group__3__Impl rule__MetaClass__Group__4 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:556:2: rule__MetaClass__Group__3__Impl rule__MetaClass__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__3__Impl_in_rule__MetaClass__Group__31080);
            rule__MetaClass__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__4_in_rule__MetaClass__Group__31083);
            rule__MetaClass__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__3"


    // $ANTLR start "rule__MetaClass__Group__3__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:563:1: rule__MetaClass__Group__3__Impl : ( ( rule__MetaClass__Group_3__0 )? ) ;
    public final void rule__MetaClass__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:567:1: ( ( ( rule__MetaClass__Group_3__0 )? ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:568:1: ( ( rule__MetaClass__Group_3__0 )? )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:568:1: ( ( rule__MetaClass__Group_3__0 )? )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:569:1: ( rule__MetaClass__Group_3__0 )?
            {
             before(grammarAccess.getMetaClassAccess().getGroup_3()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:570:1: ( rule__MetaClass__Group_3__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:570:2: rule__MetaClass__Group_3__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3__0_in_rule__MetaClass__Group__3__Impl1110);
                    rule__MetaClass__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getMetaClassAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__3__Impl"


    // $ANTLR start "rule__MetaClass__Group__4"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:580:1: rule__MetaClass__Group__4 : rule__MetaClass__Group__4__Impl rule__MetaClass__Group__5 ;
    public final void rule__MetaClass__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:584:1: ( rule__MetaClass__Group__4__Impl rule__MetaClass__Group__5 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:585:2: rule__MetaClass__Group__4__Impl rule__MetaClass__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__4__Impl_in_rule__MetaClass__Group__41141);
            rule__MetaClass__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__5_in_rule__MetaClass__Group__41144);
            rule__MetaClass__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__4"


    // $ANTLR start "rule__MetaClass__Group__4__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:592:1: rule__MetaClass__Group__4__Impl : ( '{' ) ;
    public final void rule__MetaClass__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:596:1: ( ( '{' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:597:1: ( '{' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:597:1: ( '{' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:598:1: '{'
            {
             before(grammarAccess.getMetaClassAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,13,FollowSets000.FOLLOW_13_in_rule__MetaClass__Group__4__Impl1172); 
             after(grammarAccess.getMetaClassAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__4__Impl"


    // $ANTLR start "rule__MetaClass__Group__5"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:611:1: rule__MetaClass__Group__5 : rule__MetaClass__Group__5__Impl rule__MetaClass__Group__6 ;
    public final void rule__MetaClass__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:615:1: ( rule__MetaClass__Group__5__Impl rule__MetaClass__Group__6 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:616:2: rule__MetaClass__Group__5__Impl rule__MetaClass__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__5__Impl_in_rule__MetaClass__Group__51203);
            rule__MetaClass__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__6_in_rule__MetaClass__Group__51206);
            rule__MetaClass__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__5"


    // $ANTLR start "rule__MetaClass__Group__5__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:623:1: rule__MetaClass__Group__5__Impl : ( ( rule__MetaClass__Group_5__0 )* ) ;
    public final void rule__MetaClass__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:627:1: ( ( ( rule__MetaClass__Group_5__0 )* ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:628:1: ( ( rule__MetaClass__Group_5__0 )* )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:628:1: ( ( rule__MetaClass__Group_5__0 )* )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:629:1: ( rule__MetaClass__Group_5__0 )*
            {
             before(grammarAccess.getMetaClassAccess().getGroup_5()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:630:1: ( rule__MetaClass__Group_5__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==18||LA5_0==23) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:630:2: rule__MetaClass__Group_5__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_5__0_in_rule__MetaClass__Group__5__Impl1233);
            	    rule__MetaClass__Group_5__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getMetaClassAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__5__Impl"


    // $ANTLR start "rule__MetaClass__Group__6"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:640:1: rule__MetaClass__Group__6 : rule__MetaClass__Group__6__Impl ;
    public final void rule__MetaClass__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:644:1: ( rule__MetaClass__Group__6__Impl )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:645:2: rule__MetaClass__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group__6__Impl_in_rule__MetaClass__Group__61264);
            rule__MetaClass__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__6"


    // $ANTLR start "rule__MetaClass__Group__6__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:651:1: rule__MetaClass__Group__6__Impl : ( '}' ) ;
    public final void rule__MetaClass__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:655:1: ( ( '}' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:656:1: ( '}' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:656:1: ( '}' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:657:1: '}'
            {
             before(grammarAccess.getMetaClassAccess().getRightCurlyBracketKeyword_6()); 
            match(input,14,FollowSets000.FOLLOW_14_in_rule__MetaClass__Group__6__Impl1292); 
             after(grammarAccess.getMetaClassAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group__6__Impl"


    // $ANTLR start "rule__MetaClass__Group_3__0"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:684:1: rule__MetaClass__Group_3__0 : rule__MetaClass__Group_3__0__Impl rule__MetaClass__Group_3__1 ;
    public final void rule__MetaClass__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:688:1: ( rule__MetaClass__Group_3__0__Impl rule__MetaClass__Group_3__1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:689:2: rule__MetaClass__Group_3__0__Impl rule__MetaClass__Group_3__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3__0__Impl_in_rule__MetaClass__Group_3__01337);
            rule__MetaClass__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3__1_in_rule__MetaClass__Group_3__01340);
            rule__MetaClass__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3__0"


    // $ANTLR start "rule__MetaClass__Group_3__0__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:696:1: rule__MetaClass__Group_3__0__Impl : ( '<' ) ;
    public final void rule__MetaClass__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:700:1: ( ( '<' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:701:1: ( '<' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:701:1: ( '<' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:702:1: '<'
            {
             before(grammarAccess.getMetaClassAccess().getLessThanSignKeyword_3_0()); 
            match(input,15,FollowSets000.FOLLOW_15_in_rule__MetaClass__Group_3__0__Impl1368); 
             after(grammarAccess.getMetaClassAccess().getLessThanSignKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3__0__Impl"


    // $ANTLR start "rule__MetaClass__Group_3__1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:715:1: rule__MetaClass__Group_3__1 : rule__MetaClass__Group_3__1__Impl rule__MetaClass__Group_3__2 ;
    public final void rule__MetaClass__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:719:1: ( rule__MetaClass__Group_3__1__Impl rule__MetaClass__Group_3__2 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:720:2: rule__MetaClass__Group_3__1__Impl rule__MetaClass__Group_3__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3__1__Impl_in_rule__MetaClass__Group_3__11399);
            rule__MetaClass__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3__2_in_rule__MetaClass__Group_3__11402);
            rule__MetaClass__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3__1"


    // $ANTLR start "rule__MetaClass__Group_3__1__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:727:1: rule__MetaClass__Group_3__1__Impl : ( ( rule__MetaClass__SupersAssignment_3_1 ) ) ;
    public final void rule__MetaClass__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:731:1: ( ( ( rule__MetaClass__SupersAssignment_3_1 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:732:1: ( ( rule__MetaClass__SupersAssignment_3_1 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:732:1: ( ( rule__MetaClass__SupersAssignment_3_1 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:733:1: ( rule__MetaClass__SupersAssignment_3_1 )
            {
             before(grammarAccess.getMetaClassAccess().getSupersAssignment_3_1()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:734:1: ( rule__MetaClass__SupersAssignment_3_1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:734:2: rule__MetaClass__SupersAssignment_3_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__SupersAssignment_3_1_in_rule__MetaClass__Group_3__1__Impl1429);
            rule__MetaClass__SupersAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getMetaClassAccess().getSupersAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3__1__Impl"


    // $ANTLR start "rule__MetaClass__Group_3__2"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:744:1: rule__MetaClass__Group_3__2 : rule__MetaClass__Group_3__2__Impl ;
    public final void rule__MetaClass__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:748:1: ( rule__MetaClass__Group_3__2__Impl )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:749:2: rule__MetaClass__Group_3__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3__2__Impl_in_rule__MetaClass__Group_3__21459);
            rule__MetaClass__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3__2"


    // $ANTLR start "rule__MetaClass__Group_3__2__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:755:1: rule__MetaClass__Group_3__2__Impl : ( ( rule__MetaClass__Group_3_2__0 )* ) ;
    public final void rule__MetaClass__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:759:1: ( ( ( rule__MetaClass__Group_3_2__0 )* ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:760:1: ( ( rule__MetaClass__Group_3_2__0 )* )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:760:1: ( ( rule__MetaClass__Group_3_2__0 )* )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:761:1: ( rule__MetaClass__Group_3_2__0 )*
            {
             before(grammarAccess.getMetaClassAccess().getGroup_3_2()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:762:1: ( rule__MetaClass__Group_3_2__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==16) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:762:2: rule__MetaClass__Group_3_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3_2__0_in_rule__MetaClass__Group_3__2__Impl1486);
            	    rule__MetaClass__Group_3_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getMetaClassAccess().getGroup_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3__2__Impl"


    // $ANTLR start "rule__MetaClass__Group_3_2__0"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:778:1: rule__MetaClass__Group_3_2__0 : rule__MetaClass__Group_3_2__0__Impl rule__MetaClass__Group_3_2__1 ;
    public final void rule__MetaClass__Group_3_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:782:1: ( rule__MetaClass__Group_3_2__0__Impl rule__MetaClass__Group_3_2__1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:783:2: rule__MetaClass__Group_3_2__0__Impl rule__MetaClass__Group_3_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3_2__0__Impl_in_rule__MetaClass__Group_3_2__01523);
            rule__MetaClass__Group_3_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3_2__1_in_rule__MetaClass__Group_3_2__01526);
            rule__MetaClass__Group_3_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3_2__0"


    // $ANTLR start "rule__MetaClass__Group_3_2__0__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:790:1: rule__MetaClass__Group_3_2__0__Impl : ( ',' ) ;
    public final void rule__MetaClass__Group_3_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:794:1: ( ( ',' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:795:1: ( ',' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:795:1: ( ',' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:796:1: ','
            {
             before(grammarAccess.getMetaClassAccess().getCommaKeyword_3_2_0()); 
            match(input,16,FollowSets000.FOLLOW_16_in_rule__MetaClass__Group_3_2__0__Impl1554); 
             after(grammarAccess.getMetaClassAccess().getCommaKeyword_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3_2__0__Impl"


    // $ANTLR start "rule__MetaClass__Group_3_2__1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:809:1: rule__MetaClass__Group_3_2__1 : rule__MetaClass__Group_3_2__1__Impl ;
    public final void rule__MetaClass__Group_3_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:813:1: ( rule__MetaClass__Group_3_2__1__Impl )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:814:2: rule__MetaClass__Group_3_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_3_2__1__Impl_in_rule__MetaClass__Group_3_2__11585);
            rule__MetaClass__Group_3_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3_2__1"


    // $ANTLR start "rule__MetaClass__Group_3_2__1__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:820:1: rule__MetaClass__Group_3_2__1__Impl : ( ( rule__MetaClass__SupersAssignment_3_2_1 ) ) ;
    public final void rule__MetaClass__Group_3_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:824:1: ( ( ( rule__MetaClass__SupersAssignment_3_2_1 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:825:1: ( ( rule__MetaClass__SupersAssignment_3_2_1 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:825:1: ( ( rule__MetaClass__SupersAssignment_3_2_1 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:826:1: ( rule__MetaClass__SupersAssignment_3_2_1 )
            {
             before(grammarAccess.getMetaClassAccess().getSupersAssignment_3_2_1()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:827:1: ( rule__MetaClass__SupersAssignment_3_2_1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:827:2: rule__MetaClass__SupersAssignment_3_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__SupersAssignment_3_2_1_in_rule__MetaClass__Group_3_2__1__Impl1612);
            rule__MetaClass__SupersAssignment_3_2_1();

            state._fsp--;


            }

             after(grammarAccess.getMetaClassAccess().getSupersAssignment_3_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_3_2__1__Impl"


    // $ANTLR start "rule__MetaClass__Group_5__0"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:841:1: rule__MetaClass__Group_5__0 : rule__MetaClass__Group_5__0__Impl rule__MetaClass__Group_5__1 ;
    public final void rule__MetaClass__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:845:1: ( rule__MetaClass__Group_5__0__Impl rule__MetaClass__Group_5__1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:846:2: rule__MetaClass__Group_5__0__Impl rule__MetaClass__Group_5__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_5__0__Impl_in_rule__MetaClass__Group_5__01646);
            rule__MetaClass__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_5__1_in_rule__MetaClass__Group_5__01649);
            rule__MetaClass__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_5__0"


    // $ANTLR start "rule__MetaClass__Group_5__0__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:853:1: rule__MetaClass__Group_5__0__Impl : ( ( rule__MetaClass__FeaturesAssignment_5_0 ) ) ;
    public final void rule__MetaClass__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:857:1: ( ( ( rule__MetaClass__FeaturesAssignment_5_0 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:858:1: ( ( rule__MetaClass__FeaturesAssignment_5_0 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:858:1: ( ( rule__MetaClass__FeaturesAssignment_5_0 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:859:1: ( rule__MetaClass__FeaturesAssignment_5_0 )
            {
             before(grammarAccess.getMetaClassAccess().getFeaturesAssignment_5_0()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:860:1: ( rule__MetaClass__FeaturesAssignment_5_0 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:860:2: rule__MetaClass__FeaturesAssignment_5_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__FeaturesAssignment_5_0_in_rule__MetaClass__Group_5__0__Impl1676);
            rule__MetaClass__FeaturesAssignment_5_0();

            state._fsp--;


            }

             after(grammarAccess.getMetaClassAccess().getFeaturesAssignment_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_5__0__Impl"


    // $ANTLR start "rule__MetaClass__Group_5__1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:870:1: rule__MetaClass__Group_5__1 : rule__MetaClass__Group_5__1__Impl ;
    public final void rule__MetaClass__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:874:1: ( rule__MetaClass__Group_5__1__Impl )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:875:2: rule__MetaClass__Group_5__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__MetaClass__Group_5__1__Impl_in_rule__MetaClass__Group_5__11706);
            rule__MetaClass__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_5__1"


    // $ANTLR start "rule__MetaClass__Group_5__1__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:881:1: rule__MetaClass__Group_5__1__Impl : ( ( ';' )? ) ;
    public final void rule__MetaClass__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:885:1: ( ( ( ';' )? ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:886:1: ( ( ';' )? )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:886:1: ( ( ';' )? )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:887:1: ( ';' )?
            {
             before(grammarAccess.getMetaClassAccess().getSemicolonKeyword_5_1()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:888:1: ( ';' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:889:2: ';'
                    {
                    match(input,17,FollowSets000.FOLLOW_17_in_rule__MetaClass__Group_5__1__Impl1735); 

                    }
                    break;

            }

             after(grammarAccess.getMetaClassAccess().getSemicolonKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__Group_5__1__Impl"


    // $ANTLR start "rule__Attribute__Group__0"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:904:1: rule__Attribute__Group__0 : rule__Attribute__Group__0__Impl rule__Attribute__Group__1 ;
    public final void rule__Attribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:908:1: ( rule__Attribute__Group__0__Impl rule__Attribute__Group__1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:909:2: rule__Attribute__Group__0__Impl rule__Attribute__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__0__Impl_in_rule__Attribute__Group__01772);
            rule__Attribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__1_in_rule__Attribute__Group__01775);
            rule__Attribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0"


    // $ANTLR start "rule__Attribute__Group__0__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:916:1: rule__Attribute__Group__0__Impl : ( () ) ;
    public final void rule__Attribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:920:1: ( ( () ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:921:1: ( () )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:921:1: ( () )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:922:1: ()
            {
             before(grammarAccess.getAttributeAccess().getAttributeAction_0()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:923:1: ()
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:925:1: 
            {
            }

             after(grammarAccess.getAttributeAccess().getAttributeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0__Impl"


    // $ANTLR start "rule__Attribute__Group__1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:935:1: rule__Attribute__Group__1 : rule__Attribute__Group__1__Impl rule__Attribute__Group__2 ;
    public final void rule__Attribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:939:1: ( rule__Attribute__Group__1__Impl rule__Attribute__Group__2 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:940:2: rule__Attribute__Group__1__Impl rule__Attribute__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__1__Impl_in_rule__Attribute__Group__11833);
            rule__Attribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__2_in_rule__Attribute__Group__11836);
            rule__Attribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1"


    // $ANTLR start "rule__Attribute__Group__1__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:947:1: rule__Attribute__Group__1__Impl : ( 'attr' ) ;
    public final void rule__Attribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:951:1: ( ( 'attr' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:952:1: ( 'attr' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:952:1: ( 'attr' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:953:1: 'attr'
            {
             before(grammarAccess.getAttributeAccess().getAttrKeyword_1()); 
            match(input,18,FollowSets000.FOLLOW_18_in_rule__Attribute__Group__1__Impl1864); 
             after(grammarAccess.getAttributeAccess().getAttrKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__2"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:966:1: rule__Attribute__Group__2 : rule__Attribute__Group__2__Impl rule__Attribute__Group__3 ;
    public final void rule__Attribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:970:1: ( rule__Attribute__Group__2__Impl rule__Attribute__Group__3 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:971:2: rule__Attribute__Group__2__Impl rule__Attribute__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__2__Impl_in_rule__Attribute__Group__21895);
            rule__Attribute__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__3_in_rule__Attribute__Group__21898);
            rule__Attribute__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2"


    // $ANTLR start "rule__Attribute__Group__2__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:978:1: rule__Attribute__Group__2__Impl : ( ( rule__Attribute__NameAssignment_2 ) ) ;
    public final void rule__Attribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:982:1: ( ( ( rule__Attribute__NameAssignment_2 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:983:1: ( ( rule__Attribute__NameAssignment_2 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:983:1: ( ( rule__Attribute__NameAssignment_2 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:984:1: ( rule__Attribute__NameAssignment_2 )
            {
             before(grammarAccess.getAttributeAccess().getNameAssignment_2()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:985:1: ( rule__Attribute__NameAssignment_2 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:985:2: rule__Attribute__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__NameAssignment_2_in_rule__Attribute__Group__2__Impl1925);
            rule__Attribute__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2__Impl"


    // $ANTLR start "rule__Attribute__Group__3"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:995:1: rule__Attribute__Group__3 : rule__Attribute__Group__3__Impl rule__Attribute__Group__4 ;
    public final void rule__Attribute__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:999:1: ( rule__Attribute__Group__3__Impl rule__Attribute__Group__4 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1000:2: rule__Attribute__Group__3__Impl rule__Attribute__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__3__Impl_in_rule__Attribute__Group__31955);
            rule__Attribute__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__4_in_rule__Attribute__Group__31958);
            rule__Attribute__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3"


    // $ANTLR start "rule__Attribute__Group__3__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1007:1: rule__Attribute__Group__3__Impl : ( ':' ) ;
    public final void rule__Attribute__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1011:1: ( ( ':' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1012:1: ( ':' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1012:1: ( ':' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1013:1: ':'
            {
             before(grammarAccess.getAttributeAccess().getColonKeyword_3()); 
            match(input,19,FollowSets000.FOLLOW_19_in_rule__Attribute__Group__3__Impl1986); 
             after(grammarAccess.getAttributeAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3__Impl"


    // $ANTLR start "rule__Attribute__Group__4"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1026:1: rule__Attribute__Group__4 : rule__Attribute__Group__4__Impl rule__Attribute__Group__5 ;
    public final void rule__Attribute__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1030:1: ( rule__Attribute__Group__4__Impl rule__Attribute__Group__5 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1031:2: rule__Attribute__Group__4__Impl rule__Attribute__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__4__Impl_in_rule__Attribute__Group__42017);
            rule__Attribute__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__5_in_rule__Attribute__Group__42020);
            rule__Attribute__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__4"


    // $ANTLR start "rule__Attribute__Group__4__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1038:1: rule__Attribute__Group__4__Impl : ( ( rule__Attribute__PrimitiveTypeAssignment_4 ) ) ;
    public final void rule__Attribute__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1042:1: ( ( ( rule__Attribute__PrimitiveTypeAssignment_4 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1043:1: ( ( rule__Attribute__PrimitiveTypeAssignment_4 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1043:1: ( ( rule__Attribute__PrimitiveTypeAssignment_4 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1044:1: ( rule__Attribute__PrimitiveTypeAssignment_4 )
            {
             before(grammarAccess.getAttributeAccess().getPrimitiveTypeAssignment_4()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1045:1: ( rule__Attribute__PrimitiveTypeAssignment_4 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1045:2: rule__Attribute__PrimitiveTypeAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__PrimitiveTypeAssignment_4_in_rule__Attribute__Group__4__Impl2047);
            rule__Attribute__PrimitiveTypeAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getPrimitiveTypeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__4__Impl"


    // $ANTLR start "rule__Attribute__Group__5"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1055:1: rule__Attribute__Group__5 : rule__Attribute__Group__5__Impl rule__Attribute__Group__6 ;
    public final void rule__Attribute__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1059:1: ( rule__Attribute__Group__5__Impl rule__Attribute__Group__6 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1060:2: rule__Attribute__Group__5__Impl rule__Attribute__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__5__Impl_in_rule__Attribute__Group__52077);
            rule__Attribute__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__6_in_rule__Attribute__Group__52080);
            rule__Attribute__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__5"


    // $ANTLR start "rule__Attribute__Group__5__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1067:1: rule__Attribute__Group__5__Impl : ( '[' ) ;
    public final void rule__Attribute__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1071:1: ( ( '[' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1072:1: ( '[' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1072:1: ( '[' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1073:1: '['
            {
             before(grammarAccess.getAttributeAccess().getLeftSquareBracketKeyword_5()); 
            match(input,20,FollowSets000.FOLLOW_20_in_rule__Attribute__Group__5__Impl2108); 
             after(grammarAccess.getAttributeAccess().getLeftSquareBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__5__Impl"


    // $ANTLR start "rule__Attribute__Group__6"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1086:1: rule__Attribute__Group__6 : rule__Attribute__Group__6__Impl rule__Attribute__Group__7 ;
    public final void rule__Attribute__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1090:1: ( rule__Attribute__Group__6__Impl rule__Attribute__Group__7 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1091:2: rule__Attribute__Group__6__Impl rule__Attribute__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__6__Impl_in_rule__Attribute__Group__62139);
            rule__Attribute__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__7_in_rule__Attribute__Group__62142);
            rule__Attribute__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__6"


    // $ANTLR start "rule__Attribute__Group__6__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1098:1: rule__Attribute__Group__6__Impl : ( ( rule__Attribute__MinAssignment_6 ) ) ;
    public final void rule__Attribute__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1102:1: ( ( ( rule__Attribute__MinAssignment_6 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1103:1: ( ( rule__Attribute__MinAssignment_6 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1103:1: ( ( rule__Attribute__MinAssignment_6 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1104:1: ( rule__Attribute__MinAssignment_6 )
            {
             before(grammarAccess.getAttributeAccess().getMinAssignment_6()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1105:1: ( rule__Attribute__MinAssignment_6 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1105:2: rule__Attribute__MinAssignment_6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__MinAssignment_6_in_rule__Attribute__Group__6__Impl2169);
            rule__Attribute__MinAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getMinAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__6__Impl"


    // $ANTLR start "rule__Attribute__Group__7"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1115:1: rule__Attribute__Group__7 : rule__Attribute__Group__7__Impl rule__Attribute__Group__8 ;
    public final void rule__Attribute__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1119:1: ( rule__Attribute__Group__7__Impl rule__Attribute__Group__8 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1120:2: rule__Attribute__Group__7__Impl rule__Attribute__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__7__Impl_in_rule__Attribute__Group__72199);
            rule__Attribute__Group__7__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__8_in_rule__Attribute__Group__72202);
            rule__Attribute__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__7"


    // $ANTLR start "rule__Attribute__Group__7__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1127:1: rule__Attribute__Group__7__Impl : ( '..' ) ;
    public final void rule__Attribute__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1131:1: ( ( '..' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1132:1: ( '..' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1132:1: ( '..' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1133:1: '..'
            {
             before(grammarAccess.getAttributeAccess().getFullStopFullStopKeyword_7()); 
            match(input,21,FollowSets000.FOLLOW_21_in_rule__Attribute__Group__7__Impl2230); 
             after(grammarAccess.getAttributeAccess().getFullStopFullStopKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__7__Impl"


    // $ANTLR start "rule__Attribute__Group__8"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1146:1: rule__Attribute__Group__8 : rule__Attribute__Group__8__Impl rule__Attribute__Group__9 ;
    public final void rule__Attribute__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1150:1: ( rule__Attribute__Group__8__Impl rule__Attribute__Group__9 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1151:2: rule__Attribute__Group__8__Impl rule__Attribute__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__8__Impl_in_rule__Attribute__Group__82261);
            rule__Attribute__Group__8__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__9_in_rule__Attribute__Group__82264);
            rule__Attribute__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__8"


    // $ANTLR start "rule__Attribute__Group__8__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1158:1: rule__Attribute__Group__8__Impl : ( ( rule__Attribute__MaxAssignment_8 ) ) ;
    public final void rule__Attribute__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1162:1: ( ( ( rule__Attribute__MaxAssignment_8 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1163:1: ( ( rule__Attribute__MaxAssignment_8 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1163:1: ( ( rule__Attribute__MaxAssignment_8 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1164:1: ( rule__Attribute__MaxAssignment_8 )
            {
             before(grammarAccess.getAttributeAccess().getMaxAssignment_8()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1165:1: ( rule__Attribute__MaxAssignment_8 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1165:2: rule__Attribute__MaxAssignment_8
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__MaxAssignment_8_in_rule__Attribute__Group__8__Impl2291);
            rule__Attribute__MaxAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getMaxAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__8__Impl"


    // $ANTLR start "rule__Attribute__Group__9"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1175:1: rule__Attribute__Group__9 : rule__Attribute__Group__9__Impl ;
    public final void rule__Attribute__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1179:1: ( rule__Attribute__Group__9__Impl )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1180:2: rule__Attribute__Group__9__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__9__Impl_in_rule__Attribute__Group__92321);
            rule__Attribute__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__9"


    // $ANTLR start "rule__Attribute__Group__9__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1186:1: rule__Attribute__Group__9__Impl : ( ']' ) ;
    public final void rule__Attribute__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1190:1: ( ( ']' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1191:1: ( ']' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1191:1: ( ']' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1192:1: ']'
            {
             before(grammarAccess.getAttributeAccess().getRightSquareBracketKeyword_9()); 
            match(input,22,FollowSets000.FOLLOW_22_in_rule__Attribute__Group__9__Impl2349); 
             after(grammarAccess.getAttributeAccess().getRightSquareBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__9__Impl"


    // $ANTLR start "rule__Reference__Group__0"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1225:1: rule__Reference__Group__0 : rule__Reference__Group__0__Impl rule__Reference__Group__1 ;
    public final void rule__Reference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1229:1: ( rule__Reference__Group__0__Impl rule__Reference__Group__1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1230:2: rule__Reference__Group__0__Impl rule__Reference__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__0__Impl_in_rule__Reference__Group__02400);
            rule__Reference__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__1_in_rule__Reference__Group__02403);
            rule__Reference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__0"


    // $ANTLR start "rule__Reference__Group__0__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1237:1: rule__Reference__Group__0__Impl : ( () ) ;
    public final void rule__Reference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1241:1: ( ( () ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1242:1: ( () )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1242:1: ( () )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1243:1: ()
            {
             before(grammarAccess.getReferenceAccess().getReferenceAction_0()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1244:1: ()
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1246:1: 
            {
            }

             after(grammarAccess.getReferenceAccess().getReferenceAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__0__Impl"


    // $ANTLR start "rule__Reference__Group__1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1256:1: rule__Reference__Group__1 : rule__Reference__Group__1__Impl rule__Reference__Group__2 ;
    public final void rule__Reference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1260:1: ( rule__Reference__Group__1__Impl rule__Reference__Group__2 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1261:2: rule__Reference__Group__1__Impl rule__Reference__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__1__Impl_in_rule__Reference__Group__12461);
            rule__Reference__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__2_in_rule__Reference__Group__12464);
            rule__Reference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__1"


    // $ANTLR start "rule__Reference__Group__1__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1268:1: rule__Reference__Group__1__Impl : ( 'ref' ) ;
    public final void rule__Reference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1272:1: ( ( 'ref' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1273:1: ( 'ref' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1273:1: ( 'ref' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1274:1: 'ref'
            {
             before(grammarAccess.getReferenceAccess().getRefKeyword_1()); 
            match(input,23,FollowSets000.FOLLOW_23_in_rule__Reference__Group__1__Impl2492); 
             after(grammarAccess.getReferenceAccess().getRefKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__1__Impl"


    // $ANTLR start "rule__Reference__Group__2"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1287:1: rule__Reference__Group__2 : rule__Reference__Group__2__Impl rule__Reference__Group__3 ;
    public final void rule__Reference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1291:1: ( rule__Reference__Group__2__Impl rule__Reference__Group__3 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1292:2: rule__Reference__Group__2__Impl rule__Reference__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__2__Impl_in_rule__Reference__Group__22523);
            rule__Reference__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__3_in_rule__Reference__Group__22526);
            rule__Reference__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__2"


    // $ANTLR start "rule__Reference__Group__2__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1299:1: rule__Reference__Group__2__Impl : ( ( rule__Reference__NameAssignment_2 ) ) ;
    public final void rule__Reference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1303:1: ( ( ( rule__Reference__NameAssignment_2 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1304:1: ( ( rule__Reference__NameAssignment_2 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1304:1: ( ( rule__Reference__NameAssignment_2 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1305:1: ( rule__Reference__NameAssignment_2 )
            {
             before(grammarAccess.getReferenceAccess().getNameAssignment_2()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1306:1: ( rule__Reference__NameAssignment_2 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1306:2: rule__Reference__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__NameAssignment_2_in_rule__Reference__Group__2__Impl2553);
            rule__Reference__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__2__Impl"


    // $ANTLR start "rule__Reference__Group__3"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1316:1: rule__Reference__Group__3 : rule__Reference__Group__3__Impl rule__Reference__Group__4 ;
    public final void rule__Reference__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1320:1: ( rule__Reference__Group__3__Impl rule__Reference__Group__4 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1321:2: rule__Reference__Group__3__Impl rule__Reference__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__3__Impl_in_rule__Reference__Group__32583);
            rule__Reference__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__4_in_rule__Reference__Group__32586);
            rule__Reference__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__3"


    // $ANTLR start "rule__Reference__Group__3__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1328:1: rule__Reference__Group__3__Impl : ( ':' ) ;
    public final void rule__Reference__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1332:1: ( ( ':' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1333:1: ( ':' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1333:1: ( ':' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1334:1: ':'
            {
             before(grammarAccess.getReferenceAccess().getColonKeyword_3()); 
            match(input,19,FollowSets000.FOLLOW_19_in_rule__Reference__Group__3__Impl2614); 
             after(grammarAccess.getReferenceAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__3__Impl"


    // $ANTLR start "rule__Reference__Group__4"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1347:1: rule__Reference__Group__4 : rule__Reference__Group__4__Impl rule__Reference__Group__5 ;
    public final void rule__Reference__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1351:1: ( rule__Reference__Group__4__Impl rule__Reference__Group__5 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1352:2: rule__Reference__Group__4__Impl rule__Reference__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__4__Impl_in_rule__Reference__Group__42645);
            rule__Reference__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__5_in_rule__Reference__Group__42648);
            rule__Reference__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__4"


    // $ANTLR start "rule__Reference__Group__4__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1359:1: rule__Reference__Group__4__Impl : ( ( rule__Reference__ReferenceAssignment_4 ) ) ;
    public final void rule__Reference__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1363:1: ( ( ( rule__Reference__ReferenceAssignment_4 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1364:1: ( ( rule__Reference__ReferenceAssignment_4 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1364:1: ( ( rule__Reference__ReferenceAssignment_4 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1365:1: ( rule__Reference__ReferenceAssignment_4 )
            {
             before(grammarAccess.getReferenceAccess().getReferenceAssignment_4()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1366:1: ( rule__Reference__ReferenceAssignment_4 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1366:2: rule__Reference__ReferenceAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__ReferenceAssignment_4_in_rule__Reference__Group__4__Impl2675);
            rule__Reference__ReferenceAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getReferenceAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__4__Impl"


    // $ANTLR start "rule__Reference__Group__5"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1376:1: rule__Reference__Group__5 : rule__Reference__Group__5__Impl rule__Reference__Group__6 ;
    public final void rule__Reference__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1380:1: ( rule__Reference__Group__5__Impl rule__Reference__Group__6 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1381:2: rule__Reference__Group__5__Impl rule__Reference__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__5__Impl_in_rule__Reference__Group__52705);
            rule__Reference__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__6_in_rule__Reference__Group__52708);
            rule__Reference__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__5"


    // $ANTLR start "rule__Reference__Group__5__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1388:1: rule__Reference__Group__5__Impl : ( '[' ) ;
    public final void rule__Reference__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1392:1: ( ( '[' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1393:1: ( '[' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1393:1: ( '[' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1394:1: '['
            {
             before(grammarAccess.getReferenceAccess().getLeftSquareBracketKeyword_5()); 
            match(input,20,FollowSets000.FOLLOW_20_in_rule__Reference__Group__5__Impl2736); 
             after(grammarAccess.getReferenceAccess().getLeftSquareBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__5__Impl"


    // $ANTLR start "rule__Reference__Group__6"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1407:1: rule__Reference__Group__6 : rule__Reference__Group__6__Impl rule__Reference__Group__7 ;
    public final void rule__Reference__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1411:1: ( rule__Reference__Group__6__Impl rule__Reference__Group__7 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1412:2: rule__Reference__Group__6__Impl rule__Reference__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__6__Impl_in_rule__Reference__Group__62767);
            rule__Reference__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__7_in_rule__Reference__Group__62770);
            rule__Reference__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__6"


    // $ANTLR start "rule__Reference__Group__6__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1419:1: rule__Reference__Group__6__Impl : ( ( rule__Reference__MinAssignment_6 ) ) ;
    public final void rule__Reference__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1423:1: ( ( ( rule__Reference__MinAssignment_6 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1424:1: ( ( rule__Reference__MinAssignment_6 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1424:1: ( ( rule__Reference__MinAssignment_6 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1425:1: ( rule__Reference__MinAssignment_6 )
            {
             before(grammarAccess.getReferenceAccess().getMinAssignment_6()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1426:1: ( rule__Reference__MinAssignment_6 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1426:2: rule__Reference__MinAssignment_6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__MinAssignment_6_in_rule__Reference__Group__6__Impl2797);
            rule__Reference__MinAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getMinAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__6__Impl"


    // $ANTLR start "rule__Reference__Group__7"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1436:1: rule__Reference__Group__7 : rule__Reference__Group__7__Impl rule__Reference__Group__8 ;
    public final void rule__Reference__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1440:1: ( rule__Reference__Group__7__Impl rule__Reference__Group__8 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1441:2: rule__Reference__Group__7__Impl rule__Reference__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__7__Impl_in_rule__Reference__Group__72827);
            rule__Reference__Group__7__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__8_in_rule__Reference__Group__72830);
            rule__Reference__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__7"


    // $ANTLR start "rule__Reference__Group__7__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1448:1: rule__Reference__Group__7__Impl : ( '..' ) ;
    public final void rule__Reference__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1452:1: ( ( '..' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1453:1: ( '..' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1453:1: ( '..' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1454:1: '..'
            {
             before(grammarAccess.getReferenceAccess().getFullStopFullStopKeyword_7()); 
            match(input,21,FollowSets000.FOLLOW_21_in_rule__Reference__Group__7__Impl2858); 
             after(grammarAccess.getReferenceAccess().getFullStopFullStopKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__7__Impl"


    // $ANTLR start "rule__Reference__Group__8"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1467:1: rule__Reference__Group__8 : rule__Reference__Group__8__Impl rule__Reference__Group__9 ;
    public final void rule__Reference__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1471:1: ( rule__Reference__Group__8__Impl rule__Reference__Group__9 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1472:2: rule__Reference__Group__8__Impl rule__Reference__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__8__Impl_in_rule__Reference__Group__82889);
            rule__Reference__Group__8__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__9_in_rule__Reference__Group__82892);
            rule__Reference__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__8"


    // $ANTLR start "rule__Reference__Group__8__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1479:1: rule__Reference__Group__8__Impl : ( ( rule__Reference__MaxAssignment_8 ) ) ;
    public final void rule__Reference__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1483:1: ( ( ( rule__Reference__MaxAssignment_8 ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1484:1: ( ( rule__Reference__MaxAssignment_8 ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1484:1: ( ( rule__Reference__MaxAssignment_8 ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1485:1: ( rule__Reference__MaxAssignment_8 )
            {
             before(grammarAccess.getReferenceAccess().getMaxAssignment_8()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1486:1: ( rule__Reference__MaxAssignment_8 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1486:2: rule__Reference__MaxAssignment_8
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__MaxAssignment_8_in_rule__Reference__Group__8__Impl2919);
            rule__Reference__MaxAssignment_8();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getMaxAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__8__Impl"


    // $ANTLR start "rule__Reference__Group__9"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1496:1: rule__Reference__Group__9 : rule__Reference__Group__9__Impl ;
    public final void rule__Reference__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1500:1: ( rule__Reference__Group__9__Impl )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1501:2: rule__Reference__Group__9__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__9__Impl_in_rule__Reference__Group__92949);
            rule__Reference__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__9"


    // $ANTLR start "rule__Reference__Group__9__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1507:1: rule__Reference__Group__9__Impl : ( ']' ) ;
    public final void rule__Reference__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1511:1: ( ( ']' ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1512:1: ( ']' )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1512:1: ( ']' )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1513:1: ']'
            {
             before(grammarAccess.getReferenceAccess().getRightSquareBracketKeyword_9()); 
            match(input,22,FollowSets000.FOLLOW_22_in_rule__Reference__Group__9__Impl2977); 
             after(grammarAccess.getReferenceAccess().getRightSquareBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__9__Impl"


    // $ANTLR start "rule__EInt__Group__0"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1546:1: rule__EInt__Group__0 : rule__EInt__Group__0__Impl rule__EInt__Group__1 ;
    public final void rule__EInt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1550:1: ( rule__EInt__Group__0__Impl rule__EInt__Group__1 )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1551:2: rule__EInt__Group__0__Impl rule__EInt__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__EInt__Group__0__Impl_in_rule__EInt__Group__03028);
            rule__EInt__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EInt__Group__1_in_rule__EInt__Group__03031);
            rule__EInt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0"


    // $ANTLR start "rule__EInt__Group__0__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1558:1: rule__EInt__Group__0__Impl : ( ( '-' )? ) ;
    public final void rule__EInt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1562:1: ( ( ( '-' )? ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1563:1: ( ( '-' )? )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1563:1: ( ( '-' )? )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1564:1: ( '-' )?
            {
             before(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1565:1: ( '-' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==24) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1566:2: '-'
                    {
                    match(input,24,FollowSets000.FOLLOW_24_in_rule__EInt__Group__0__Impl3060); 

                    }
                    break;

            }

             after(grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__0__Impl"


    // $ANTLR start "rule__EInt__Group__1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1577:1: rule__EInt__Group__1 : rule__EInt__Group__1__Impl ;
    public final void rule__EInt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1581:1: ( rule__EInt__Group__1__Impl )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1582:2: rule__EInt__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__EInt__Group__1__Impl_in_rule__EInt__Group__13093);
            rule__EInt__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1"


    // $ANTLR start "rule__EInt__Group__1__Impl"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1588:1: rule__EInt__Group__1__Impl : ( RULE_INT ) ;
    public final void rule__EInt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1592:1: ( ( RULE_INT ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1593:1: ( RULE_INT )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1593:1: ( RULE_INT )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1594:1: RULE_INT
            {
             before(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__EInt__Group__1__Impl3120); 
             after(grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EInt__Group__1__Impl"


    // $ANTLR start "rule__MetaModel__NameAssignment_2"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1610:1: rule__MetaModel__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__MetaModel__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1614:1: ( ( ruleEString ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1615:1: ( ruleEString )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1615:1: ( ruleEString )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1616:1: ruleEString
            {
             before(grammarAccess.getMetaModelAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__MetaModel__NameAssignment_23158);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getMetaModelAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__NameAssignment_2"


    // $ANTLR start "rule__MetaModel__ClassesAssignment_3"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1625:1: rule__MetaModel__ClassesAssignment_3 : ( ruleMetaClass ) ;
    public final void rule__MetaModel__ClassesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1629:1: ( ( ruleMetaClass ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1630:1: ( ruleMetaClass )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1630:1: ( ruleMetaClass )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1631:1: ruleMetaClass
            {
             before(grammarAccess.getMetaModelAccess().getClassesMetaClassParserRuleCall_3_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClass_in_rule__MetaModel__ClassesAssignment_33189);
            ruleMetaClass();

            state._fsp--;

             after(grammarAccess.getMetaModelAccess().getClassesMetaClassParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__ClassesAssignment_3"


    // $ANTLR start "rule__MetaModel__ClassesAssignment_4"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1640:1: rule__MetaModel__ClassesAssignment_4 : ( ruleMetaClass ) ;
    public final void rule__MetaModel__ClassesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1644:1: ( ( ruleMetaClass ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1645:1: ( ruleMetaClass )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1645:1: ( ruleMetaClass )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1646:1: ruleMetaClass
            {
             before(grammarAccess.getMetaModelAccess().getClassesMetaClassParserRuleCall_4_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClass_in_rule__MetaModel__ClassesAssignment_43220);
            ruleMetaClass();

            state._fsp--;

             after(grammarAccess.getMetaModelAccess().getClassesMetaClassParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaModel__ClassesAssignment_4"


    // $ANTLR start "rule__MetaClass__NameAssignment_2"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1655:1: rule__MetaClass__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__MetaClass__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1659:1: ( ( ruleEString ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1660:1: ( ruleEString )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1660:1: ( ruleEString )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1661:1: ruleEString
            {
             before(grammarAccess.getMetaClassAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__MetaClass__NameAssignment_23251);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getMetaClassAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__NameAssignment_2"


    // $ANTLR start "rule__MetaClass__SupersAssignment_3_1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1670:1: rule__MetaClass__SupersAssignment_3_1 : ( ( RULE_ID ) ) ;
    public final void rule__MetaClass__SupersAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1674:1: ( ( ( RULE_ID ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1675:1: ( ( RULE_ID ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1675:1: ( ( RULE_ID ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1676:1: ( RULE_ID )
            {
             before(grammarAccess.getMetaClassAccess().getSupersMetaClassCrossReference_3_1_0()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1677:1: ( RULE_ID )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1678:1: RULE_ID
            {
             before(grammarAccess.getMetaClassAccess().getSupersMetaClassIDTerminalRuleCall_3_1_0_1()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__MetaClass__SupersAssignment_3_13286); 
             after(grammarAccess.getMetaClassAccess().getSupersMetaClassIDTerminalRuleCall_3_1_0_1()); 

            }

             after(grammarAccess.getMetaClassAccess().getSupersMetaClassCrossReference_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__SupersAssignment_3_1"


    // $ANTLR start "rule__MetaClass__SupersAssignment_3_2_1"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1689:1: rule__MetaClass__SupersAssignment_3_2_1 : ( ( RULE_ID ) ) ;
    public final void rule__MetaClass__SupersAssignment_3_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1693:1: ( ( ( RULE_ID ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1694:1: ( ( RULE_ID ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1694:1: ( ( RULE_ID ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1695:1: ( RULE_ID )
            {
             before(grammarAccess.getMetaClassAccess().getSupersMetaClassCrossReference_3_2_1_0()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1696:1: ( RULE_ID )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1697:1: RULE_ID
            {
             before(grammarAccess.getMetaClassAccess().getSupersMetaClassIDTerminalRuleCall_3_2_1_0_1()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__MetaClass__SupersAssignment_3_2_13325); 
             after(grammarAccess.getMetaClassAccess().getSupersMetaClassIDTerminalRuleCall_3_2_1_0_1()); 

            }

             after(grammarAccess.getMetaClassAccess().getSupersMetaClassCrossReference_3_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__SupersAssignment_3_2_1"


    // $ANTLR start "rule__MetaClass__FeaturesAssignment_5_0"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1708:1: rule__MetaClass__FeaturesAssignment_5_0 : ( ruleFeature ) ;
    public final void rule__MetaClass__FeaturesAssignment_5_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1712:1: ( ( ruleFeature ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1713:1: ( ruleFeature )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1713:1: ( ruleFeature )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1714:1: ruleFeature
            {
             before(grammarAccess.getMetaClassAccess().getFeaturesFeatureParserRuleCall_5_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_rule__MetaClass__FeaturesAssignment_5_03360);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getMetaClassAccess().getFeaturesFeatureParserRuleCall_5_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MetaClass__FeaturesAssignment_5_0"


    // $ANTLR start "rule__Attribute__NameAssignment_2"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1723:1: rule__Attribute__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Attribute__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1727:1: ( ( ruleEString ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1728:1: ( ruleEString )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1728:1: ( ruleEString )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1729:1: ruleEString
            {
             before(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Attribute__NameAssignment_23391);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__NameAssignment_2"


    // $ANTLR start "rule__Attribute__PrimitiveTypeAssignment_4"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1738:1: rule__Attribute__PrimitiveTypeAssignment_4 : ( ruleEString ) ;
    public final void rule__Attribute__PrimitiveTypeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1742:1: ( ( ruleEString ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1743:1: ( ruleEString )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1743:1: ( ruleEString )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1744:1: ruleEString
            {
             before(grammarAccess.getAttributeAccess().getPrimitiveTypeEStringParserRuleCall_4_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Attribute__PrimitiveTypeAssignment_43422);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getPrimitiveTypeEStringParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__PrimitiveTypeAssignment_4"


    // $ANTLR start "rule__Attribute__MinAssignment_6"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1753:1: rule__Attribute__MinAssignment_6 : ( RULE_INT ) ;
    public final void rule__Attribute__MinAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1757:1: ( ( RULE_INT ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1758:1: ( RULE_INT )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1758:1: ( RULE_INT )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1759:1: RULE_INT
            {
             before(grammarAccess.getAttributeAccess().getMinINTTerminalRuleCall_6_0()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__Attribute__MinAssignment_63453); 
             after(grammarAccess.getAttributeAccess().getMinINTTerminalRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__MinAssignment_6"


    // $ANTLR start "rule__Attribute__MaxAssignment_8"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1768:1: rule__Attribute__MaxAssignment_8 : ( ruleEInt ) ;
    public final void rule__Attribute__MaxAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1772:1: ( ( ruleEInt ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1773:1: ( ruleEInt )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1773:1: ( ruleEInt )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1774:1: ruleEInt
            {
             before(grammarAccess.getAttributeAccess().getMaxEIntParserRuleCall_8_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_rule__Attribute__MaxAssignment_83484);
            ruleEInt();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getMaxEIntParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__MaxAssignment_8"


    // $ANTLR start "rule__Reference__NameAssignment_2"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1783:1: rule__Reference__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Reference__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1787:1: ( ( ruleEString ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1788:1: ( ruleEString )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1788:1: ( ruleEString )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1789:1: ruleEString
            {
             before(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Reference__NameAssignment_23515);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__NameAssignment_2"


    // $ANTLR start "rule__Reference__ReferenceAssignment_4"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1798:1: rule__Reference__ReferenceAssignment_4 : ( ( RULE_ID ) ) ;
    public final void rule__Reference__ReferenceAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1802:1: ( ( ( RULE_ID ) ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1803:1: ( ( RULE_ID ) )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1803:1: ( ( RULE_ID ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1804:1: ( RULE_ID )
            {
             before(grammarAccess.getReferenceAccess().getReferenceMetaClassCrossReference_4_0()); 
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1805:1: ( RULE_ID )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1806:1: RULE_ID
            {
             before(grammarAccess.getReferenceAccess().getReferenceMetaClassIDTerminalRuleCall_4_0_1()); 
            match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__Reference__ReferenceAssignment_43550); 
             after(grammarAccess.getReferenceAccess().getReferenceMetaClassIDTerminalRuleCall_4_0_1()); 

            }

             after(grammarAccess.getReferenceAccess().getReferenceMetaClassCrossReference_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__ReferenceAssignment_4"


    // $ANTLR start "rule__Reference__MinAssignment_6"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1817:1: rule__Reference__MinAssignment_6 : ( RULE_INT ) ;
    public final void rule__Reference__MinAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1821:1: ( ( RULE_INT ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1822:1: ( RULE_INT )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1822:1: ( RULE_INT )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1823:1: RULE_INT
            {
             before(grammarAccess.getReferenceAccess().getMinINTTerminalRuleCall_6_0()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__Reference__MinAssignment_63585); 
             after(grammarAccess.getReferenceAccess().getMinINTTerminalRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__MinAssignment_6"


    // $ANTLR start "rule__Reference__MaxAssignment_8"
    // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1832:1: rule__Reference__MaxAssignment_8 : ( RULE_INT ) ;
    public final void rule__Reference__MaxAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1836:1: ( ( RULE_INT ) )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1837:1: ( RULE_INT )
            {
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1837:1: ( RULE_INT )
            // ../metabup.mm.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalMetamodelDSL.g:1838:1: RULE_INT
            {
             before(grammarAccess.getReferenceAccess().getMaxINTTerminalRuleCall_8_0()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__Reference__MaxAssignment_83616); 
             after(grammarAccess.getReferenceAccess().getMaxINTTerminalRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__MaxAssignment_8"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleMetaModel_in_entryRuleMetaModel61 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaModel68 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__0_in_ruleMetaModel94 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature121 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeature128 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Feature__Alternatives_in_ruleFeature154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClass_in_entryRuleMetaClass181 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaClass188 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__0_in_ruleMetaClass214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_entryRuleAttribute241 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttribute248 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__0_in_ruleAttribute274 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_entryRuleReference301 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReference308 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__0_in_ruleReference334 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString361 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString368 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EString__Alternatives_in_ruleEString394 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_entryRuleEInt421 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEInt428 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EInt__Group__0_in_ruleEInt454 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_rule__Feature__Alternatives490 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_rule__Feature__Alternatives507 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__EString__Alternatives539 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__EString__Alternatives556 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__0__Impl_in_rule__MetaModel__Group__0586 = new BitSet(new long[]{0x0000000000000800L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__1_in_rule__MetaModel__Group__0589 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__1__Impl_in_rule__MetaModel__Group__1647 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__2_in_rule__MetaModel__Group__1650 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_rule__MetaModel__Group__1__Impl678 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__2__Impl_in_rule__MetaModel__Group__2709 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__3_in_rule__MetaModel__Group__2712 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaModel__NameAssignment_2_in_rule__MetaModel__Group__2__Impl739 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__3__Impl_in_rule__MetaModel__Group__3769 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__4_in_rule__MetaModel__Group__3772 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaModel__ClassesAssignment_3_in_rule__MetaModel__Group__3__Impl799 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaModel__Group__4__Impl_in_rule__MetaModel__Group__4829 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaModel__ClassesAssignment_4_in_rule__MetaModel__Group__4__Impl856 = new BitSet(new long[]{0x0000000000001002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__0__Impl_in_rule__MetaClass__Group__0897 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__1_in_rule__MetaClass__Group__0900 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__1__Impl_in_rule__MetaClass__Group__1958 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__2_in_rule__MetaClass__Group__1961 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__MetaClass__Group__1__Impl989 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__2__Impl_in_rule__MetaClass__Group__21020 = new BitSet(new long[]{0x000000000000A000L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__3_in_rule__MetaClass__Group__21023 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__NameAssignment_2_in_rule__MetaClass__Group__2__Impl1050 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__3__Impl_in_rule__MetaClass__Group__31080 = new BitSet(new long[]{0x000000000000A000L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__4_in_rule__MetaClass__Group__31083 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3__0_in_rule__MetaClass__Group__3__Impl1110 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__4__Impl_in_rule__MetaClass__Group__41141 = new BitSet(new long[]{0x0000000000844000L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__5_in_rule__MetaClass__Group__41144 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__MetaClass__Group__4__Impl1172 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__5__Impl_in_rule__MetaClass__Group__51203 = new BitSet(new long[]{0x0000000000844000L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__6_in_rule__MetaClass__Group__51206 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_5__0_in_rule__MetaClass__Group__5__Impl1233 = new BitSet(new long[]{0x0000000000840002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group__6__Impl_in_rule__MetaClass__Group__61264 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__MetaClass__Group__6__Impl1292 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3__0__Impl_in_rule__MetaClass__Group_3__01337 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3__1_in_rule__MetaClass__Group_3__01340 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__MetaClass__Group_3__0__Impl1368 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3__1__Impl_in_rule__MetaClass__Group_3__11399 = new BitSet(new long[]{0x0000000000010000L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3__2_in_rule__MetaClass__Group_3__11402 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__SupersAssignment_3_1_in_rule__MetaClass__Group_3__1__Impl1429 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3__2__Impl_in_rule__MetaClass__Group_3__21459 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3_2__0_in_rule__MetaClass__Group_3__2__Impl1486 = new BitSet(new long[]{0x0000000000010002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3_2__0__Impl_in_rule__MetaClass__Group_3_2__01523 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3_2__1_in_rule__MetaClass__Group_3_2__01526 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_rule__MetaClass__Group_3_2__0__Impl1554 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_3_2__1__Impl_in_rule__MetaClass__Group_3_2__11585 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__SupersAssignment_3_2_1_in_rule__MetaClass__Group_3_2__1__Impl1612 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_5__0__Impl_in_rule__MetaClass__Group_5__01646 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_5__1_in_rule__MetaClass__Group_5__01649 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__FeaturesAssignment_5_0_in_rule__MetaClass__Group_5__0__Impl1676 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__MetaClass__Group_5__1__Impl_in_rule__MetaClass__Group_5__11706 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__MetaClass__Group_5__1__Impl1735 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__0__Impl_in_rule__Attribute__Group__01772 = new BitSet(new long[]{0x0000000000040000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__1_in_rule__Attribute__Group__01775 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__1__Impl_in_rule__Attribute__Group__11833 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Attribute__Group__2_in_rule__Attribute__Group__11836 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_rule__Attribute__Group__1__Impl1864 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__2__Impl_in_rule__Attribute__Group__21895 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__3_in_rule__Attribute__Group__21898 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__NameAssignment_2_in_rule__Attribute__Group__2__Impl1925 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__3__Impl_in_rule__Attribute__Group__31955 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Attribute__Group__4_in_rule__Attribute__Group__31958 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__Attribute__Group__3__Impl1986 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__4__Impl_in_rule__Attribute__Group__42017 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__5_in_rule__Attribute__Group__42020 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__PrimitiveTypeAssignment_4_in_rule__Attribute__Group__4__Impl2047 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__5__Impl_in_rule__Attribute__Group__52077 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__Attribute__Group__6_in_rule__Attribute__Group__52080 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__Attribute__Group__5__Impl2108 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__6__Impl_in_rule__Attribute__Group__62139 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__7_in_rule__Attribute__Group__62142 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__MinAssignment_6_in_rule__Attribute__Group__6__Impl2169 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__7__Impl_in_rule__Attribute__Group__72199 = new BitSet(new long[]{0x0000000001000040L});
        public static final BitSet FOLLOW_rule__Attribute__Group__8_in_rule__Attribute__Group__72202 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__Attribute__Group__7__Impl2230 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__8__Impl_in_rule__Attribute__Group__82261 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__9_in_rule__Attribute__Group__82264 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__MaxAssignment_8_in_rule__Attribute__Group__8__Impl2291 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__9__Impl_in_rule__Attribute__Group__92321 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__Attribute__Group__9__Impl2349 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__0__Impl_in_rule__Reference__Group__02400 = new BitSet(new long[]{0x0000000000840000L});
        public static final BitSet FOLLOW_rule__Reference__Group__1_in_rule__Reference__Group__02403 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__1__Impl_in_rule__Reference__Group__12461 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Reference__Group__2_in_rule__Reference__Group__12464 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_rule__Reference__Group__1__Impl2492 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__2__Impl_in_rule__Reference__Group__22523 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_rule__Reference__Group__3_in_rule__Reference__Group__22526 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__NameAssignment_2_in_rule__Reference__Group__2__Impl2553 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__3__Impl_in_rule__Reference__Group__32583 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__Reference__Group__4_in_rule__Reference__Group__32586 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__Reference__Group__3__Impl2614 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__4__Impl_in_rule__Reference__Group__42645 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_rule__Reference__Group__5_in_rule__Reference__Group__42648 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__ReferenceAssignment_4_in_rule__Reference__Group__4__Impl2675 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__5__Impl_in_rule__Reference__Group__52705 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__Reference__Group__6_in_rule__Reference__Group__52708 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__Reference__Group__5__Impl2736 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__6__Impl_in_rule__Reference__Group__62767 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_rule__Reference__Group__7_in_rule__Reference__Group__62770 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__MinAssignment_6_in_rule__Reference__Group__6__Impl2797 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__7__Impl_in_rule__Reference__Group__72827 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__Reference__Group__8_in_rule__Reference__Group__72830 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__Reference__Group__7__Impl2858 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__8__Impl_in_rule__Reference__Group__82889 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_rule__Reference__Group__9_in_rule__Reference__Group__82892 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__MaxAssignment_8_in_rule__Reference__Group__8__Impl2919 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__9__Impl_in_rule__Reference__Group__92949 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__Reference__Group__9__Impl2977 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EInt__Group__0__Impl_in_rule__EInt__Group__03028 = new BitSet(new long[]{0x0000000001000040L});
        public static final BitSet FOLLOW_rule__EInt__Group__1_in_rule__EInt__Group__03031 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_rule__EInt__Group__0__Impl3060 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EInt__Group__1__Impl_in_rule__EInt__Group__13093 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__EInt__Group__1__Impl3120 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__MetaModel__NameAssignment_23158 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClass_in_rule__MetaModel__ClassesAssignment_33189 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClass_in_rule__MetaModel__ClassesAssignment_43220 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__MetaClass__NameAssignment_23251 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__MetaClass__SupersAssignment_3_13286 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__MetaClass__SupersAssignment_3_2_13325 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_rule__MetaClass__FeaturesAssignment_5_03360 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Attribute__NameAssignment_23391 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Attribute__PrimitiveTypeAssignment_43422 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__Attribute__MinAssignment_63453 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_rule__Attribute__MaxAssignment_83484 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Reference__NameAssignment_23515 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__Reference__ReferenceAssignment_43550 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__Reference__MinAssignment_63585 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__Reference__MaxAssignment_83616 = new BitSet(new long[]{0x0000000000000002L});
    }


}