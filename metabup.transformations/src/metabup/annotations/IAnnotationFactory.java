/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations;

public interface IAnnotationFactory {

	public Class<? extends IAnnotation> getAnnotation(String name);
	public boolean existsAnnotation(String name);
	public Class<? extends IConstraintAnnotation> getConstraintAnnotation(String name);
}
