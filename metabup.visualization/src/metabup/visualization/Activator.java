/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.visualization;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "metabup.visualization"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	
	/**
	 * Log.
	 *
	 * @param status the status
	 */
	static public void log(IStatus status) {
		getDefault().getLog().log(status);
	}
	
	/**
	 * Log.
	 *
	 * @param severity the severity
	 * @param msg the msg
	 */
	static public void log(int severity, String msg) {
		getDefault().getLog().log(makeStatus(severity, msg));
	}
	
	/**
	 * Log.
	 *
	 * @param severity the severity
	 * @param msg the msg
	 * @param exc the exc
	 */
	static public void log(int severity, String msg, Throwable exc) {		
		getDefault().getLog().log(makeStatus(severity, msg, exc));
	}		
	
    /**
     * Make status.
     *
     * @param severity the severity
     * @param msg the msg
     * @param exc the exc
     * @return the i status
     */
    public static IStatus makeStatus(int severity, String msg, Throwable exc) {
        if (exc != null) exc.printStackTrace();
        return new Status(severity,PLUGIN_ID,IStatus.OK,msg,exc);
    }
        
    /**
     * Make status.
     *
     * @param severity the severity
     * @param msg the msg
     * @return the i status
     */
    public static IStatus makeStatus(int severity, String msg) {
        return makeStatus(severity,msg,null);
    }
}
