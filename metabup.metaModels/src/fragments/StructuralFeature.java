/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structural Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link fragments.StructuralFeature#getValues <em>Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see fragments.FragmentsPackage#getStructuralFeature()
 * @model
 * @generated
 */
public interface StructuralFeature extends Feature {
	/**
	 * Returns the value of the '<em><b>Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' attribute list.
	 * @see fragments.FragmentsPackage#getStructuralFeature_Values()
	 * @model
	 * @generated
	 */
	EList<String> getValues();

} // StructuralFeature
