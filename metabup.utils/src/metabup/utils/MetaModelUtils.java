package metabup.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.WordUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.InputDialog;

import metabup.metamodel.AnnotationParam;
import metabup.metamodel.Attribute;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.ParamValue;
import metabup.metamodel.Reference;


public class MetaModelUtils {
	
	/* 
	 * METACLASSES 
	 * 
	 * */
	
	//source
	public static List<MetaClass> allMetaClasses(MetaModel mm){
		ArrayList<MetaClass> all = new ArrayList<MetaClass>();
		
		for ( MetaClass c : mm.getClasses()) {
			all.add(c);
		}
		
		return all;
	}		
	
	//source
	public static List<MetaClass> allMetaClassesWithFeatureName(MetaModel mm, String fName) {
		ArrayList<MetaClass> all = new ArrayList<MetaClass>();
		
		for ( MetaClass c : mm.getClasses()) {
			for (metabup.metamodel.Feature ff : c.getFeatures()) {
				if (ff.getName().equals(fName)) {		// TODO: Check also the type...
					all.add(c);
					break;
				}
			}
		}
		
		return all;
	}
	
	//source
	public static List<MetaClass> allMetaClassesWithAnnotation(MetaModel mm, metabup.metamodel.Annotation a) {
		ArrayList<MetaClass> allMC = new ArrayList<MetaClass>();
		EList<AnnotationParam> annParams = a.getParams();
		
		for ( MetaClass mc : mm.getClasses()) {			
			for (metabup.metamodel.Annotation ann : mc.getAnnotations()) {
				if (ann.getName().equals(a.getName())) {
					boolean ok = true;
					
					if(!a.getParams().isEmpty()){
						for(metabup.metamodel.AnnotationParam annParam : ann.getParams()){
							if(ok){
								for(AnnotationParam originalAnnParam : annParams){
									if(ok){
										if(!originalAnnParam.getName().equals(annParam.getName()) ||
										   !originalAnnParam.getValue().equals(annParam.getValue())){
											ok = false;
										}
									}
								}							
							}											
						}
						
						if(ok) allMC.add(mc);
					}else allMC.add(mc);
				}
			}
		}
		
		return allMC;
	}		
	
	// source
	public static boolean existsMetaClassByName(MetaModel mm, String name){
		ArrayList<MetaClass> all = new ArrayList<MetaClass>();
		
		for ( MetaClass mc : mm.getClasses()) {
			if(mc.getName().equals(name)) return true;
		}
		
		return false;
	}
	
	// source
	public static MetaClass getMetaClassByName(MetaModel mm, String name){
		ArrayList<MetaClass> all = new ArrayList<MetaClass>();
		
		for ( MetaClass mc : mm.getClasses()) {
			if(mc.getName().equals(name)) return mc;
		}
		
		return null;
	}
	
	// tar
	public static boolean removeMetaClass(MetaModel mm, MetaClass mc){							
		for(MetaClass othermc : mm.getClasses()){			
			if(othermc.getSupers().contains(mc)){
				othermc.getSupers().addAll(mc.getSupers());				
				othermc.getSupers().remove(mc);
			}
			if(othermc.getSubclasses().contains(mc)){
				othermc.getSubclasses().addAll(mc.getSubclasses());
				othermc.getSubclasses().remove(mc);
			}
			
			for(Feature f : othermc.getFeatures()){
				if(f instanceof Reference){
					Reference r = (Reference)f;
					if(r.getReference().equals(mc)) othermc.getFeatures().remove(r);
				}
			}
			
			for(Reference r : othermc.getIncomingRefs()){
				// REMOVE INCOMING REFS!!
			}
		}
		
		return mm.getClasses().remove(mc);		
	}
	
	public static List<MetaClass> getCommonSupers(List<MetaClass> mcs){
		if(mcs == null || mcs.isEmpty()) return null;		
		List<MetaClass> supers = new ArrayList<MetaClass>();
		supers.addAll(mcs.get(0).getSupers());
		
		for(MetaClass mc : mcs)
			for(MetaClass superMc : mc.getSupers())
				if(!supers.contains(superMc)) 
					supers.remove(superMc);
		
		if(supers.isEmpty()) return null;
		else return supers;
	}
	
	public static List<Feature> getCommonFeatures(List<MetaClass> mcs){
		if(mcs == null || mcs.size() == 0) return null;
		if(mcs.size() == 1) return null;
		
		List<Feature> commonff = mcs.get(0).getFeatures();
		List<Feature> noncommonff = new ArrayList<Feature>();
		
		for(MetaClass mc : mcs){
			for(Feature f1 : commonff){
				boolean found = false;
				
				for(int i=0; !found && i<mc.getFeatures().size(); i++){
					Feature f2 = mc.getFeatures().get(i);
					if(areEqual(f1, f2, false)) found = true;					
				}
				
				if(!found && !noncommonff.contains(f1)){
					noncommonff.add(f1);
				}
			}									
		}			
		
		/* generate copies of the common features */
		
		List<Feature> retcommonff = new ArrayList<Feature>();
		
		for(Feature f : commonff){
			if(!noncommonff.contains(f)){
				if( f instanceof Reference){
					Reference ref = (Reference) EcoreUtil.copy(f);
					ref.setId(EcoreUtil.generateUUID());
					retcommonff.add(ref);
				}else{
					if( f instanceof Attribute ){
						Attribute att = (Attribute) EcoreUtil.copy(f);
						att.setId(EcoreUtil.generateUUID());
						retcommonff.add(att);
					}
				}
			}
		}
		
		return retcommonff;
	}
	
	/*
	 * FEATURES
	 * 
	 */
	public static boolean areEqual(Feature f1, Feature f2, boolean withCardinality){		
		if(f1 instanceof Attribute && f2 instanceof Reference) return false;
		if(f1 instanceof Reference && f2 instanceof Attribute) return false;
		if(!f1.getName().equals(f2.getName())) return false;
		
		if(withCardinality){
			if(f1.getMin() != f2.getMin()) return false;
			if(f1.getMax() != f2.getMax()) return false;
		}
		
		if(f1 instanceof Attribute && f2 instanceof Attribute)
			if(!((Attribute) f1).getPrimitiveType().equals(((Attribute) f2).getPrimitiveType())) return false;
		if(f1 instanceof Reference && f2 instanceof Reference)
			if(!((Reference) f1).getReference().getId().equals(((Reference) f2).getReference().getId())) return false;
		return true;
	}
	
	public static boolean existsFeatureInMetaClass(Feature f, MetaClass mc){
		List<Feature> mcff = mc.getFeatures();
		if(mcff.contains(f)) return true;
		else return false;
	}
	
	public static List<metabup.metamodel.Annotation> removeFeatureWithName(MetaClass cont, String name) {
		for (metabup.metamodel.Feature ff : cont.getFeatures()) {
			if (ff.getName().equals(name)) {
				cont.getFeatures().remove(ff);
				return ff.getAnnotations();				
			}
		}
		return new ArrayList<metabup.metamodel.Annotation>();		
	}
	
	public static void removeAnnotationWithName(MetaClass cont, String name) {
		for (metabup.metamodel.Annotation a : cont.getAnnotations()) {
			if (a.getName().equals(name)) {
				cont.getAnnotations().remove(a);				
			}
		}		
	}
	
	public static void removeSelfReferences(MetaClass mc){
		List<Reference> refs = new ArrayList<Reference>();
		
		for(Feature f : mc.getFeatures()){
			if(f instanceof Reference){
				Reference r = (Reference) f;				
				if(mc.getId().equals(r.getReference().getId())) refs.add(r);
			}
		}
				
		mc.getFeatures().removeAll(refs);		
	}
	
	public static void removeReplicatedFeatures(MetaClass mc){
		List<Feature> ff = mc.getFeatures();
		List<String> ffnames = new ArrayList<String>();
		List<Feature> removeff = new ArrayList<Feature>();
		for(Feature f:ff) ffnames.add(f.getName());
		
		for(Feature f : ff){
			int occ = Collections.frequency(ffnames, f.getName());
			if(occ > 1){
				String name = f.getName();				
				int i=1;
				
				for(Feature f2: ff){
					if(f2.getName().equals(name) && i<occ){
						removeff.add(f2);
						i++;
					}
				}
			}
		}
		
		if(!removeff.isEmpty()){
			mc.getFeatures().removeAll(removeff);
		}
	}
	
	public static boolean isCompatibleType(metabup.metamodel.Feature f, metabup.metamodel.Feature ff) {
		if (f instanceof metabup.metamodel.Reference && ff instanceof metabup.metamodel.Reference) {
			if (((metabup.metamodel.Reference)f).getReference()!=null)	// Puede ser null si no se ha resuelto el target... no s� si habr�a que postponer los refactorings al final...
				return ((metabup.metamodel.Reference)f).getReference().equals(((metabup.metamodel.Reference)ff).getReference());
			return false;
		}
		if (f instanceof metabup.metamodel.Attribute && ff instanceof metabup.metamodel.Attribute) {
			return ((metabup.metamodel.Attribute)f).getPrimitiveType().equals(((metabup.metamodel.Attribute)ff).getPrimitiveType());
		}
		return false;
	}
	
	/*
	 * REFERENCES
	 * 
	 */
	
	public static List<Reference> allReferencesFromMetaClassWithAnnotation(metabup.metamodel.Annotation a, metabup.metamodel.MetaClass mc) {		
		EList<AnnotationParam> annParams = a.getParams();
		ArrayList<Reference> refs = new ArrayList<Reference>(); 
		
		for (Feature f : mc.getFeatures()) {
			if(f instanceof metabup.metamodel.Reference){				
				for (metabup.metamodel.Annotation ann : f.getAnnotations()) {
					if (ann.getName().equals(a.getName())) {
						boolean ok = true;
						
						if(!a.getParams().isEmpty()){
							for(metabup.metamodel.AnnotationParam annParam : ann.getParams()){
								if(ok){
									for(AnnotationParam originalAnnParam : annParams){
										if(ok){
											if(!originalAnnParam.getName().equals(annParam.getName()) ||
											   !originalAnnParam.getValue().equals(annParam.getValue())){
												ok = false;
											}
										}
									}								
								}											
							}
							
							if(ok) refs.add((Reference)f);
						}else refs.add((Reference)f);
					}
				}
			}
		}
		
		return refs;
	}
	
	public static List<Reference> allReferencesFromMetaClassWithName(String name, metabup.metamodel.MetaClass mc) {				
		ArrayList<Reference> refs = new ArrayList<Reference>(); 
		
		for (Feature f : mc.getFeatures()) {
			if(f instanceof metabup.metamodel.Reference){
				if(f.getName().equals(name)) refs.add((Reference)f);
			}
		}
		
		return refs;
	}
	
	/*
	 * ANNOTATIONS
	 * 
	 */
	
	public static ParamValue getAnnotationParamValueByParamName(List<AnnotationParam> annotationParams, String name){
		for(AnnotationParam ap : annotationParams){
			if(ap.getName().equals(name)) return ap.getValue();
		}
		
		return null;
	}
	
	public static String promptClassName(List<MetaClass> mcs, String suggested){				
		List<String> names = new ArrayList<String>();
		for(MetaClass mc : mcs) names.add(mc.getName());
		if(names.isEmpty()) return suggested;		
		InputDialog id = new InputDialog(null, "Generalization to MetaClass", names + " are to be generalized to the MetaClass. Type another class name, either new or existing, if you like:", suggested, null);
		if(id.open() == InputDialog.OK) return id.getValue();
		else return suggested;
	}
	
	/*
	 * String and naming conventions
	 */
	
	public static String induceCommonName(List<MetaClass> mcs){
		List<MetaClass> supers = getCommonSupers(mcs);
		
		String name = null;
		
		if(supers == null || (supers.get(0).getSubclasses().size() != mcs.size())){
			name = calculateCommonName(mcs);
			if(name.equals("") || name.length() < 3) name = calculateConcatName(mcs);
		}else name = supers.get(0).getName();
		
		return name;
	}
	
	public static String calculateCommonName(List<MetaClass> mcs){
		if(mcs == null || mcs.isEmpty() || mcs.size() < 0) return "";
		if(mcs.size() == 1) return WordUtils.capitalize(mcs.get(0).getName());		
		ArrayList<String> allNames = new ArrayList<String>();
		for(MetaClass mc : mcs) allNames.add(mc.getName());
		String name = WordUtils.capitalize(longestSubstring(allNames.get(0), allNames, 1));		
		for(MetaClass mc : mcs) if(mc.getName().equals(name)) name = "General" + name;		
		return name;
	}
	
	public static String calculateConcatName(List<MetaClass> mcs){
		if(mcs.isEmpty() || mcs.size() < 0) return "";
		String name = "General";
		for(MetaClass mc : mcs) name+=mc.getName();
		return name;
	}
	
	public static String longestSubstring(String pivot, List<String> remain, int index) {
		StringBuilder sb = new StringBuilder();
		String str1 = pivot;
		String str2 = remain.get(index);
		
		if (str1 == null || str1.isEmpty() || str2 == null || str2.isEmpty())
		  return "";

		// ignore case
		str1 = str1.toLowerCase();
		str2 = str2.toLowerCase();

		// java initializes them already with 0
		int[][] num = new int[str1.length()][str2.length()];
		int maxlen = 0;
		int lastSubsBegin = 0;

		for (int i = 0; i < str1.length(); i++) {
			for (int j = 0; j < str2.length(); j++) {
				if (str1.charAt(i) == str2.charAt(j)) {
					if ((i == 0) || (j == 0)) num[i][j] = 1;
					else num[i][j] = 1 + num[i - 1][j - 1];

					if (num[i][j] > maxlen) {
						maxlen = num[i][j];
						// generate substring from str1 => i
						int thisSubsBegin = i - num[i][j] + 1;
						if (lastSubsBegin == thisSubsBegin) {
							//if the current LCS is the same as the last time this block ran
							sb.append(str1.charAt(i));
						} else {
							//this block resets the string builder if a different LCS is found
							lastSubsBegin = thisSubsBegin;
							sb = new StringBuilder();
							sb.append(str1.substring(lastSubsBegin, i + 1));
						}
					}
				}
			}
		}
				
		if(sb.equals("")) return "";
		index++;
		if(index<remain.size()) return longestSubstring(sb.toString(), remain, index);
		else return sb.toString();
	}
}
