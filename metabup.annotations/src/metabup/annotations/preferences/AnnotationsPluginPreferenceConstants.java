/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class AnnotationsPluginPreferenceConstants {

	public static final String P_ANNOTATIONS_PACKAGE = "annotations";
	public static final String P_ANNOTATIONS_LIB_FILES = "data/metabup.annotations.lib.jar";	
	public static final String P_ASSISTANCE_TIPS_LIB_FILES = "data/metabup.annotations.assistance.lib.jar";
	public static final String P_LIB_NAMES = "metabup.annotations.lib.*";	
}
