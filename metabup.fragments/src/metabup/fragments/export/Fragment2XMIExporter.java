package metabup.fragments.export;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import com.jesusjlopezf.utils.eclipse.emf.EMFUtils;
import fragments.BooleanValue;
import fragments.Fragment;
import fragments.IntegerValue;
import fragments.StringValue;
import metabup.annotations.catalog.constraint.Root;
import metabup.fragments.export.util.BasicEMFModel;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class Fragment2XMIExporter extends AbstractHandler {
	private File ecore;
	private File outputFile;
	private Fragment fragment;
	
	public void setFragment(Fragment fragment) {
		this.fragment = fragment;
	}

	public void setOutputXMI(File file) {
		this.outputFile = file;
	}

	public void setEcore(File ecore) {
		this.ecore = ecore;
	}

	/**
	 * The constructor.
	 */
	public Fragment2XMIExporter(File ecore, File output, Fragment fragment) {
		this.ecore = ecore;
		this.outputFile = output;
		this.fragment = fragment;
	}

	/**
	 * the command has been executed, so extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		EList<EObject> objects;
		EList<EClass> classes = new BasicEList<EClass>();
		HashMap<String, EStructuralFeature> classFeatures = new HashMap<String, EStructuralFeature>();
		classes = this.getAllClasses(ecore);			
		
		for(EClass ec : classes){	
			//System.out.println(ec.getName() + ": " + ec);
			for(EStructuralFeature esf : ec.getEAllStructuralFeatures()){
				
				for(EClass ec2 : classes){
					if(ec2.getName().equals(esf.getEType().getName())){
						esf.setEType(ec2);
					}
				}
				
				classFeatures.put(ec.getName()+"."+esf.getName(), esf);
				//System.out.println(ec.getName()+"."+esf.getName()+ ": " +esf.getEType());
			}
		}
		
		// first create all objects
		objects = this.createEObjects(fragment, classes);						
		
		// then assign features (w containment)
		for(fragments.Object o : fragment.getObjects()){
			EObject eobject = this.getEObjectsByFeatureValue(objects, "name", o.getName()).get(0);					
			if(eobject == null) continue;
									
			for(fragments.Feature f : o.getFeatures()){						
				if(f instanceof fragments.Reference){
					EStructuralFeature esf = classFeatures.get(o.getType()+"."+f.getName());
					if(esf == null) continue;

					EReference eref = (EReference)esf;					
					if(!eref.isContainment()) continue;
					
					if (esf.getUpperBound() == 1){
						EObject result = this.getEObjectsByFeatureValue(objects, "name", ((fragments.Reference) f).getReference().get(0).getName()).get(0);						
						eobject.eSet(esf, result);
						System.out.println("setting " + o.getName() + "." + f.getName() + " = " + result);
					}else{
						EList<EObject> results = new BasicEList<EObject>(); 
									
						for(fragments.Object tarO : ((fragments.Reference) f).getReference())
							results.add(this.getEObjectsByFeatureValue(objects, "name", tarO.getName()).get(0));
						
						eobject.eSet(esf, results);
						System.out.println("setting " + o.getName() + "." + f.getName() + " = " + results.get(0));
					}
				}
			}
		}
		
		// then assign features (w/o containment)
		for(fragments.Object o : fragment.getObjects()){
			EObject eobject = this.getEObjectsByFeatureValue(objects, "name", o.getName()).get(0);					
			if(eobject == null) continue;
									
			for(fragments.Feature f : o.getFeatures()){
				if(f.getName().equals("name")) continue;
				EStructuralFeature esf = classFeatures.get(o.getType()+"."+f.getName());
				if(esf == null) continue;
				
				if(f instanceof fragments.Attribute){
					if (esf.getUpperBound() == 1){
						if(((fragments.Attribute) f).getValues().get(0) instanceof StringValue)						
							eobject.eSet(esf, ((StringValue)((fragments.Attribute) f).getValues().get(0)).getValue());
						else if(((fragments.Attribute) f).getValues().get(0) instanceof IntegerValue)
							eobject.eSet(esf, ((IntegerValue)((fragments.Attribute) f).getValues().get(0)).getValue());
						else if(((fragments.Attribute) f).getValues().get(0) instanceof BooleanValue)
							eobject.eSet(esf, ((BooleanValue)((fragments.Attribute) f).getValues().get(0)).getValue());
					}
					else{
						// TODO
					}
				}else if(f instanceof fragments.Reference){
					EReference eref = (EReference)esf;
					if(eref.isContainment()) continue;				
					if (esf.getUpperBound() == 1){
						EObject result = this.getEObjectsByFeatureValue(objects, "name", ((fragments.Reference) f).getReference().get(0).getName()).get(0);						
						eobject.eSet(esf, result);
						System.out.println("setting " + o.getName() + "." + f.getName() + " = " + result);
					}else{
						EList<EObject> results = new BasicEList<EObject>(); 
									
						for(fragments.Object tarO : ((fragments.Reference) f).getReference())
							results.add(this.getEObjectsByFeatureValue(objects, "name", tarO.getName()).get(0));
									
						eobject.eSet(esf, results);
						System.out.println("setting " + o.getName() + "." + f.getName() + " = " + results.get(0));
					}
				}
			}
		}		

		
		// check the root object existence. create one if absent		
		EObject rootObject = getRootEObject(classes, objects);
		if(rootObject == null) return "Cannot create an instance of a meta-model with no root class. Add one before export.";
		
		// now serialize this
		String extension = outputFile.getAbsolutePath().substring(outputFile.getAbsolutePath().lastIndexOf(".")+1);
		
		try {
			EMFUtils.serialize2File(rootObject, extension, outputFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	    return null;
	}
	
	private EList<EObject> createEObjects(Fragment fragment, List<EClass> classes) {
		EList<EObject> objects = new BasicEList<EObject>();
		List<String> types = new ArrayList<String>();	
		
		for(fragments.Object o : fragment.getObjects()){
			if(types.contains(o.getType())) continue;
					
			EClass eclass = null;
							
			for(EClass ec : classes){
				if(ec.getName().equals(o.getType())){
					eclass = ec;
					break;
				}
			}
					
			if(eclass == null) continue;
					
			EObject eo = EMFUtils.createNamedEObject(eclass, o.getName());
			if(!objects.contains(eo)) objects.add(eo);
					
			for(fragments.Object sameTypeObject : fragment.getObjects()){
				if(sameTypeObject.getType().equals(o.getType()) && sameTypeObject != o){
					EObject sto = EMFUtils.createNamedEObject(eclass, sameTypeObject.getName());
					if(!objects.contains(sto))objects.add(sto);
				}
			}
					
			types.add(o.getType());										
		}
				
		return objects;
	}

	private EObject getRootEObject(EList<EClass> classes, EList<EObject> objects){
		EClass rootClass = null;
		
		for(EClass ec: classes){
			if(ec.getEAnnotation(Root.NAME) != null){
				rootClass = ec;
				break;
			}
		}
				
		if(rootClass == null){
			System.out.println("Cannot create an instance of a meta-model with no root class. Add one before export.");
			return null;
		}

		List<EObject> eRootObjects = this.getEObjectsByType(objects, rootClass.getName(), false);
		EObject rootObject = null;
				
		EList<EObject> contained = new BasicEList<EObject>();
				
		//in case there is at least a root object
		if(!eRootObjects.isEmpty()) rootObject = eRootObjects.get(0);
		else{
			// in case not (we need to add one)
			rootObject = EcoreUtil.create(rootClass);
			List<EReference> conts = rootClass.getEAllContainments();
			List<EObject> uncontained = (List<EObject>) EcoreUtil.copyAll(objects);
			
			// get the contained classes
			List<EClass> containedClasses = new ArrayList<EClass>();
			
			for(EClass ec : classes){
				for(EReference eref : ec.getEAllContainments()){
					containedClasses.add((EClass)eref.getEType());
				}
			}
			
			// discard the ones that are contained already
			for(EObject eo : objects){
				if(containedClasses.contains(eo.eClass())){
					uncontained.remove(eo);
					contained.add(eo);
				}
			}
					
			for(EReference c : conts){
				String tarType = c.getEType().getName();

				List<EObject> tarObjects = this.getEObjectsByType(uncontained, tarType, true);
						
						if(tarObjects != null && !tarObjects.isEmpty()){
							rootObject.eSet(c, tarObjects);
							uncontained.removeAll(tarObjects);
							contained.addAll(tarObjects);
						}		
					}
				}
		
		return rootObject;
	}
	
	private List<EObject> getEObjectsByType(List<EObject> objects, String name, boolean takeSubs) {
		List<EObject> retObjects = new ArrayList<EObject>();
		
		for(EObject eo : objects){
			if(eo.eClass().getName().equals(name)) retObjects.add(eo);
			else if(takeSubs){
				for(EClass sup : eo.eClass().getEAllSuperTypes()){
					if(name.equals(sup.getName())) retObjects.add(eo);
				}
			}
		}
		
		return retObjects;
	}

	private List<EObject> getEObjectsByFeatureValue(EList<EObject> objects, String featureName, Object featureValue) {
		List<EObject> retObjects = new ArrayList<EObject>();
		
		for(EObject eo : objects){
			EStructuralFeature feat = EMFUtils.getEStructuralFeatureByName(eo.eClass(), featureName);
			if(eo.eGet(feat).equals(featureValue)) retObjects.add(eo);
		}
		
		return retObjects;
	}

	private void addRootObject(File ecoreFile, BasicEMFModel modelo, List<EObject> objects) {
		EList<EClass> classes = new BasicEList<EClass>();		
		/* segunda lectura del Ecore para obtener la clase Root (antes de esto, la excepci�n ya ha saltado) */
		classes = this.getAllClasses(ecoreFile);
		
		EClass root = null;
		
		for(EClass ec: classes){
			if(ec.getEAnnotation(Root.NAME) != null){
				root = ec;
				break;
			}
		}
		
		if(root == null){
			System.out.println("Cannot create an instance of a meta-model with no root class. Add one before export.");
			//return "Cannot create an instance of a meta-model with no root class. Add one before export.";
		}

		List<EObject> eRootObjects = modelo.allObjectsOf(root.getName());
		EObject rootObject = null;
		
		EList<EObject> contained = new BasicEList<EObject>();
		
		//in case there is at least a root object
		if(!eRootObjects.isEmpty()) rootObject = eRootObjects.get(0);
		else{
			// in case not (we need to add one)
			rootObject = modelo.createObject(root.getName());
			List<EReference> conts = root.getEAllContainments();
			Collection<EObject> uncontained = objects; 
			
			// discard the ones that are contained already
			for(EObject eo : objects){
				if(eo.eContainer() != null){
					uncontained.remove(eo);
					contained.add(eo);
				}
			}
					
			for(EReference c : conts){
				String tarType = c.getEType().getName();

				List<EObject> tarObjects = modelo.allObjectsOf(tarType);
				
				if(tarObjects != null && !tarObjects.isEmpty()){
					rootObject.eSet(c, tarObjects);
					uncontained.removeAll(tarObjects);
					contained.addAll(tarObjects);
				}		
			}
		}
	}

	private EObject getObject(fragments.Object fObj, BasicEMFModel modelo){
		List<EObject> eos = modelo.allObjectsOf(fObj.getType());
		for(EObject eo : eos) if(fObj.getName().equals(modelo.getFeature(eo, "name"))) return eo;
		return null;
	}
	
	private EList<EClass> getAllClasses(File ecore){		
		TreeIterator<EObject> ecoreAll = EMFUtils.getEcoreContents(ecore);
		EList<EClass> classes = new BasicEList<EClass>();
		
		while(ecoreAll.hasNext()){
			EObject obj = ecoreAll.next();
			
			if(obj instanceof EClass ){
				classes.add((EClass)obj);
			}		
		}
		
		return classes;
	}
}
