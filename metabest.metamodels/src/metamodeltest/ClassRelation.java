/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.ClassRelation#getClassSels <em>Class Sels</em>}</li>
 *   <li>{@link metamodeltest.ClassRelation#getJumps <em>Jumps</em>}</li>
 *   <li>{@link metamodeltest.ClassRelation#getByInheritance <em>By Inheritance</em>}</li>
 *   <li>{@link metamodeltest.ClassRelation#getContainment <em>Containment</em>}</li>
 *   <li>{@link metamodeltest.ClassRelation#getStrict <em>Strict</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getClassRelation()
 * @model abstract="true"
 * @generated
 */
public interface ClassRelation extends ClassQualifier {
	/**
	 * Returns the value of the '<em><b>Class Sels</b></em>' containment reference list.
	 * The list contents are of type {@link metamodeltest.ClassSelector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Sels</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Sels</em>' containment reference list.
	 * @see metamodeltest.MetamodeltestPackage#getClassRelation_ClassSels()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ClassSelector> getClassSels();

	/**
	 * Returns the value of the '<em><b>Jumps</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Jumps</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jumps</em>' containment reference.
	 * @see #setJumps(Length)
	 * @see metamodeltest.MetamodeltestPackage#getClassRelation_Jumps()
	 * @model containment="true"
	 * @generated
	 */
	Length getJumps();

	/**
	 * Sets the value of the '{@link metamodeltest.ClassRelation#getJumps <em>Jumps</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jumps</em>' containment reference.
	 * @see #getJumps()
	 * @generated
	 */
	void setJumps(Length value);

	/**
	 * Returns the value of the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>By Inheritance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>By Inheritance</em>' containment reference.
	 * @see #setByInheritance(BooleanParameter)
	 * @see metamodeltest.MetamodeltestPackage#getClassRelation_ByInheritance()
	 * @model containment="true"
	 * @generated
	 */
	BooleanParameter getByInheritance();

	/**
	 * Sets the value of the '{@link metamodeltest.ClassRelation#getByInheritance <em>By Inheritance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>By Inheritance</em>' containment reference.
	 * @see #getByInheritance()
	 * @generated
	 */
	void setByInheritance(BooleanParameter value);

	/**
	 * Returns the value of the '<em><b>Containment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containment</em>' containment reference.
	 * @see #setContainment(BooleanParameter)
	 * @see metamodeltest.MetamodeltestPackage#getClassRelation_Containment()
	 * @model containment="true"
	 * @generated
	 */
	BooleanParameter getContainment();

	/**
	 * Sets the value of the '{@link metamodeltest.ClassRelation#getContainment <em>Containment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containment</em>' containment reference.
	 * @see #getContainment()
	 * @generated
	 */
	void setContainment(BooleanParameter value);

	/**
	 * Returns the value of the '<em><b>Strict</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strict</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strict</em>' containment reference.
	 * @see #setStrict(BooleanParameter)
	 * @see metamodeltest.MetamodeltestPackage#getClassRelation_Strict()
	 * @model containment="true"
	 * @generated
	 */
	BooleanParameter getStrict();

	/**
	 * Sets the value of the '{@link metamodeltest.ClassRelation#getStrict <em>Strict</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strict</em>' containment reference.
	 * @see #getStrict()
	 * @generated
	 */
	void setStrict(BooleanParameter value);

} // ClassRelation
