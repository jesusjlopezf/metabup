/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.ElementRelation#getByInheritance <em>By Inheritance</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getElementRelation()
 * @model
 * @generated
 */
public interface ElementRelation extends ClassQualifier {
	/**
	 * Returns the value of the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>By Inheritance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>By Inheritance</em>' containment reference.
	 * @see #setByInheritance(BooleanParameter)
	 * @see metamodeltest.MetamodeltestPackage#getElementRelation_ByInheritance()
	 * @model containment="true"
	 * @generated
	 */
	BooleanParameter getByInheritance();

	/**
	 * Sets the value of the '{@link metamodeltest.ElementRelation#getByInheritance <em>By Inheritance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>By Inheritance</em>' containment reference.
	 * @see #getByInheritance()
	 * @generated
	 */
	void setByInheritance(BooleanParameter value);

} // ElementRelation
