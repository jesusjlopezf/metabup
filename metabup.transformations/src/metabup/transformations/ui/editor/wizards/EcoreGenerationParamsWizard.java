/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations.ui.editor.wizards;

import org.eclipse.jface.wizard.Wizard;

public class EcoreGenerationParamsWizard extends Wizard {
	
	private EcoreGenerationParamsWizardPage one = null;
	private String modelName;
	
	/* values to be captured */
	private String name;
	private String prefix;
	private String uri;
	private String extension;
	
	public EcoreGenerationParamsWizard(String modelName) {
		this.modelName = modelName;		
	}
	
	@Override
	public void addPages() {		
		String pName;
		if(modelName.contains(".")) pName = modelName.substring(0, modelName.indexOf("."));
		else pName = modelName;
		one = new EcoreGenerationParamsWizardPage(pName, modelName.substring(0, 3), "platform:/resource/" + pName + "/" + pName + ".mm.ecore", "ext");
		addPage(one);
	}	
	
	@Override
	public boolean performFinish() {
		this.name = one.getEcoreName();
		this.prefix = one.getEcorePrefix();
		this.uri = one.getEcoreURI();
		this.extension = one.getExtension();
		return true;
	}

	public String getName() {
		return name;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getUri() {
		return uri;
	}
	
	public String getExtension(){
		return extension;
	}

}
