/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.visualization.mm;

import org.eclipse.gef4.zest.fx.parts.NodeContentPart;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import metabup.metamodel.Attribute;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;

public class MetaClassContentPart extends NodeContentPart {
	private VBox vbox;
	public static final String ATTR_CUSTOM = "MetaClass";
	
	@Override
	protected void createNodeVisual(Group group, Rectangle rect,
			ImageView iconImageView, Text labelText,
			StackPane nestedContentStackPane) {
		
		BorderStroke stroke = new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, null);
		
		VBox attbox = new VBox();
		attbox.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		if (super.getContent().getAttrs().get("object") != null){
			MetaClass mc = (MetaClass)super.getContent().getAttrs().get("object");
			
			for(Attribute att : mc.getAttributes()){
				Label attLabel = new Label(att.getName() + " : " + att.getPrimitiveType() + " ("+ att.getMin() + ", " + att.getMax() + ")");
				
				attbox.getChildren().add(attLabel);
				attLabel.setFocusTraversable(true);
				attLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
					
		            @Override
		            public void handle(MouseEvent event) {
		                // TODO Auto-generated method stub
		            	System.out.println("Attribute selected");
		            }
		        });				
			}
			
			for(Reference r : mc.getReferences()){
				if(mc == r.getReference()){
					Label attLabel = new Label(r.getName() + " : " + mc.getName() + " (" + r.getMin() + ", " + r.getMax() + ")");
					
					attbox.getChildren().add(attLabel);
					attLabel.setFocusTraversable(true);
					attLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
						
			            @Override
			            public void handle(MouseEvent event) {
			                // TODO Auto-generated method stub
			            	System.out.println("Self-reference selected");
			            }
			        });
				}
			}
		}
		
		VBox classNameBox = new VBox();  
		classNameBox.setBorder(new Border(stroke));
		classNameBox.getChildren().add(labelText);
		
		vbox = new VBox();
		vbox.getChildren().addAll(classNameBox, attbox, iconImageView,
				nestedContentStackPane);
		vbox.setBorder(new Border(stroke));
		vbox.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
		group.getChildren().add(vbox);
		
		labelText.setFill(Color.BLACK);
	}
}


