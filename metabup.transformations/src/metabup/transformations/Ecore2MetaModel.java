/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import metabup.change.trace.ChangeTrace;
import metabup.metamodel.*;
import metabup.metamodel.impl.MetamodelFactoryImpl;
import metabup.metamodel.impl.ReferenceImpl;
import metabup.transformations.models.WMetamodelModel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

public class Ecore2MetaModel extends BaseTransformation {
	private File ecore = null;
	private EObject ecoreObject = null;
	private WMetamodelModel outModel;	
	private MetaModel targetMetaModel = null;
	
	private ChangeTrace changeTrace = null;
	
	private static String OCLUri = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";
	
	public ChangeTrace getChangeTrace() {
		return changeTrace;
	}

	public Ecore2MetaModel(File ecore, WMetamodelModel outModel) {
		this.ecore = ecore;
		this.outModel = outModel;		
	}
	
	public Ecore2MetaModel(EObject eObject, WMetamodelModel outModel) {
		//System.out.println("yo");
		this.ecoreObject = eObject;
		this.outModel = outModel;
	}	

	public void execute() {	
		//if(!ecore.exists() && ecoreObject == null) return;
						
		this.targetMetaModel = outModel.rootMetamodel();
		if ( targetMetaModel == null ) {
			this.targetMetaModel = outModel.createMetaModel();
			this.targetMetaModel.setName( "java.mbupf" );
		}
		
		Resource resource = null;
		TreeIterator<EObject> ecoreAll;
		
		if(ecore != null && ecore.exists()){
			ResourceSet resourceSet = new ResourceSetImpl(); 
			resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl()); 			//EcorePackage ecorePackage = EcorePackage.eINSTANCE;		
			resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("diagram", new EcoreResourceFactoryImpl()); 			//EcorePackage ecorePackage = EcorePackage.eINSTANCE;
			URI fileURI = URI.createFileURI(ecore.getAbsolutePath());
			resource = resourceSet.getResource(fileURI, true);
			resourceSet.getAllContents().next();
			//get the EPackage . this outta be provisional. EPackage changes should be approached just like the rest
			ecoreAll = resource.getAllContents();
		}else if(ecoreObject != null){
			ecoreAll = ecoreObject.eAllContents();
			changeTrace = new ChangeTrace((EPackage)ecoreObject);
			//System.out.println("I'm here and I got: " + ecoreAll.toString());
		}else return;
		
		
		boolean out = false;
		while(!out && ecoreAll.hasNext()){
			EObject obj = ecoreAll.next();
			
			if(obj instanceof EPackage){
				changeTrace = new ChangeTrace((EPackage) obj);
				////System.out.println("EPACKAGE FOUND: " + ((EPackage) obj).getName());
				out = true;
			}
		}
		
		/*transformation performance*/
		MetaModel meta = this.targetMetaModel;	
		if(ecore != null) meta.setSrcURI(ecore.getAbsolutePath());
		//System.out.println("ECORE PATH: " + meta.getSrcURI());
		
		if(ecore != null) ecoreAll = resource.getAllContents();		
		else ecoreAll = ecoreObject.eAllContents();
		
		List<String> constraintNames = new ArrayList<String>();
		
		while(ecoreAll.hasNext()){
			EObject obj = ecoreAll.next();
			
			if(obj instanceof EClass){
				EClass eclass = (EClass)obj;				
				if(!this.existsMetaClassByName(((EClass) obj).getName()))meta.getClasses().addAll(transformEClass(eclass));	
				
				for(EAnnotation ea : eclass.getEAnnotations()){
					if(ea.getSource().equals(OCLUri)){															 
							Annotation ann = MetamodelFactory.eINSTANCE.createAnnotation();
							ann.setName("constraint");
							
							for(Entry<String, String> entry : ea.getDetails().entrySet()){
								if(!constraintNames.contains(entry.getKey())){ // duplicated name constraints can't exist
									AnnotationParam param = MetamodelFactory.eINSTANCE.createAnnotationParam();
									param.setName(entry.getKey());
									constraintNames.add(entry.getKey());
									StringValue value = MetamodelFactory.eINSTANCE.createStringValue();
									value.setValue(entry.getValue());
									param.setValue(value);
									ann.getParams().add(param);
									
									MetaClass mc = meta.getClassByName(eclass.getName());
									mc.getAnnotations().add(ann);
								}
							}
					}										
				}
			}										
		}
		
		/* opposite references calculation */
		if(ecore != null) ecoreAll = resource.getAllContents();		
		else ecoreAll = ecoreObject.eAllContents();
		
		while(ecoreAll.hasNext()){
			EObject obj = ecoreAll.next();
			
			if(obj instanceof EReference && ((EReference) obj).getEOpposite() != null){
				MetaClass mc = meta.getClassByName(((EClass)obj.eContainer()).getName());
				
				for(Reference r : mc.getReferences(false, ReferenceImpl.IRRELEVANT)){
					if(r.getName().equals(((EReference)obj).getName())){																		
						TreeIterator<EObject> ecoreAll2 = resource.getAllContents();
						
						while(ecoreAll2.hasNext()){
							EObject tobj = ecoreAll2.next();
							
							if(tobj instanceof EReference){
								EReference teref = (EReference)tobj;
								
								if(teref.equals(((EReference) obj).getEOpposite())){
									////System.out.println("I'M GETTING IN");
									for(Reference tr : r.getReference().getReferences(false, ReferenceImpl.IRRELEVANT)){										
										if(tr.getName().equals(teref.getName())){
											Annotation ann = outModel.createAnnotation();
											ann.setName("opposite");
											AnnotationParam ap = outModel.createAnnotationParam();
											ap.setName("opp");
											ElementValue ev = MetamodelFactoryImpl.eINSTANCE.createElementValue();
											ev.setValue(tr);
											ap.setValue(ev);
											ann.getParams().add(ap);
											r.getAnnotations().add(ann);
										}
									}
									
									ecoreAll2.prune();
								}
							}														
						}																								
					}
				}
			}
		}
		
		List<MetaClass> removeMC = new ArrayList<MetaClass>();
		
		for(MetaClass mc : meta.getClasses()){
			if(mc.getName() == null || mc.getName().equals(""))
				if(mc.getAttributes() == null || mc.getAttributes().isEmpty())
					if(mc.getReferences() == null || mc.getReferences().isEmpty())
						if(mc.getSupers() == null || mc.getSupers().isEmpty()) removeMC.add(mc);
		}
		
		meta.getClasses().removeAll(removeMC);
	}
	
	private List<MetaClass> transformEClass(EClass eclass){
		List<MetaClass> mcs = new ArrayList<MetaClass>();
		
		/*if(eclass.getName() == null || eclass.getName().equals(""))
			if(eclass.getEAttributes() == null || eclass.getEAttributes().isEmpty())
				if(eclass.getEReferences() == null || eclass.getEReferences().isEmpty())
					if(eclass.getESuperTypes() == null || eclass.getESuperTypes().isEmpty()) return mcs;*/
		
		MetaClass mc = outModel.createMetaclass();
		mc.setName(eclass.getName());				
		mc.setIsAbstract(eclass.isAbstract());		
		if(EcoreUtil.getID(eclass) != null) mc.setId(EcoreUtil.getID(eclass));
		
		/* attribute transformation */
		EList<EAttribute> eatts = eclass.getEAttributes();		
		for(EAttribute eatt : eatts) mc.getFeatures().add(transformEAttribute(eatt));											
		
		/* reference transformation */
		EList<EReference> erefs = eclass.getEReferences();
		
		for(EReference eref : erefs){			
			if(eref.getEReferenceType() != null){
				EClass target;
				
				target = eref.getEReferenceType();
				
				if(target.getName() != null){
					if(target.getName().equals(mc.getName())){										
						mc.getFeatures().add(transformEReference(eref, mc));
					}else{	
						if(!this.existsMetaClassByName(target.getName())) mcs.addAll(transformEClass(target));															
						MetaClass targetMC = getMetaClassByName(target.getName());						
						if(targetMC != null) mc.getFeatures().add(transformEReference(eref, targetMC));					
					}
				}
				
				//System.out.println("La referencia " + eref.getName() + " de la clase " + eref.getEContainingClass().getName() + " del paquete " + eref.getEContainingClass().getEPackage().getName());
				//System.out.println(" apunta a un tipo " + target.getName());
				
			}else{
				EClassifier target = eref.getEType();			
				
				if(target != null && target.getName() != null){
					mc.getFeatures().add(transformEAttribute(target, eref.getLowerBound(), eref.getUpperBound()));
				}else{
					super.addError("Couldn't find the target reference of " + eref.getEContainingClass().getName() + "." + eref.getName());
				}
			}												
		}
		
		/* supers transformation */
		EList<EClass> esupers = eclass.getESuperTypes();
		 
		for(EClass esuper : esupers){			
			if(!this.existsMetaClassByName(esuper.getName())) mcs.addAll(transformEClass(esuper));			
			MetaClass superMC = getMetaClassByName(esuper.getName());
			
			if(superMC != null){
				mc.getSupers().add(superMC);
				superMC.getSubclasses().add(mc);				
			}
		}
		
		mcs.add(mc);
		changeTrace.load(mc, eclass);
		changeTrace.refreshIndex();
		return mcs;
	}
	
	private Attribute transformEAttribute(EAttribute eatt){		
		Attribute att = outModel.createAttribute();
		att.setName(eatt.getName());
		
		String etypename;
		String typename;
		
		if(eatt.getEAttributeType() != null){
			etypename = eatt.getEAttributeType().getName();
						
			if(etypename == null || etypename.equals("")) typename = "";		
			else if(etypename.equals("EString") || etypename.toLowerCase().contains("string")) typename = "StringType";
			else if(etypename.equals("EInt") || etypename.contains("Int")) typename = "IntType";
			else if(etypename.equals("EDouble") || etypename.toLowerCase().contains("float") || etypename.toLowerCase().contains("double") || etypename.toLowerCase().contains("real") || etypename.toLowerCase().contains("decimal")) typename = "DoubleType";
			else if(etypename.equals("EBoolean") || etypename.toLowerCase().contains("bool")) typename = "BooleanType";
			else typename = etypename;
			
			if(typename.toLowerCase().endsWith("kind")) typename.replace("kind", "type");
			else if(typename.toLowerCase().endsWith("sort")) typename.replace("sort", "type");
			
			if(!typename.endsWith("Type") && !typename.endsWith("type") && !typename.equals("")) typename += "Type";
			
			if(eatt.getEAttributeType() instanceof EEnum){
				EEnum ee = (EEnum) eatt.getEAttributeType();			
				//typename += " {";			
				//for(EEnumLiteral el : ee.getELiterals()) typename += el.getName() + ", ";			
				//typename = typename.substring(0, typename.length()-2);			
				//typename += "}";
				
				typename += "{ }";
			}
		}else typename = "";				
		
		att.setMin(eatt.getLowerBound());
		att.setMax(eatt.getUpperBound());
		
		att.setPrimitiveType(typename);
		if(EcoreUtil.getID(eatt) != null) att.setId(EcoreUtil.getID(eatt));
		changeTrace.load(att, eatt);
		return att;
	}
	
	private Attribute transformEAttribute(EClassifier eatt, int min, int max){		
		Attribute att = outModel.createAttribute();
		att.setName(eatt.getName());

		String etypename = eatt.getInstanceTypeName();
		String typename;
		
		if(etypename == null || etypename.equals("")) typename = "";		
		else if(etypename.equals("EString") || etypename.toLowerCase().contains("string")) typename = "StringType";
		else if(etypename.equals("EInt") || etypename.contains("Int")) typename = "IntType";
		else if(etypename.equals("EDouble") || etypename.toLowerCase().contains("float") || etypename.toLowerCase().contains("double") || etypename.toLowerCase().contains("real") || etypename.toLowerCase().contains("decimal")) typename = "DoubleType";
		else if(etypename.equals("EBoolean") || etypename.toLowerCase().contains("bool")) typename = "BooleanType";
		else typename = etypename;
		
		if(typename.toLowerCase().endsWith("kind")) typename.replace("kind", "type");
		else if(typename.toLowerCase().endsWith("sort")) typename.replace("sort", "type");
		
		if(!typename.endsWith("Type") && !typename.endsWith("type") && !typename.equals("")) typename += "Type";
		
		/*if(eatt.getEAttributeType() instanceof EEnum){
			EEnum ee = (EEnum) eatt.getEAttributeType();			
			//typename += " {";			
			//for(EEnumLiteral el : ee.getELiterals()) typename += el.getName() + ", ";			
			//typename = typename.substring(0, typename.length()-2);			
			//typename += "}";
			
			typename += "{ }";
		}*/
		
		att.setMin(min);
		att.setMax(max);
		
		att.setPrimitiveType(typename);
		if(EcoreUtil.getID(eatt) != null) att.setId(EcoreUtil.getID(eatt));
		changeTrace.load(att, eatt);
		return att;
	}
	
	private Reference transformEReference(EReference eref, MetaClass target){
		Reference ref = outModel.createReference();		
		ref.setName(eref.getName());
		ref.setMin(eref.getLowerBound());
		ref.setMax(eref.getUpperBound());
		ref.setReference(target);
		
		if(eref.isContainment()){
			Annotation ann = outModel.createAnnotation();
			ann.setName("composition");
			ref.getAnnotations().add(ann);
		}
		
		if(EcoreUtil.getID(eref) != null) ref.setId(EcoreUtil.getID(eref));
		changeTrace.load(ref, eref);
		return ref;
	}	
	
	private boolean existsMetaClassByName(String name){		
		for ( MetaClass mc : outModel.allMetaClasses()) {
			if(mc.getName() != null && mc.getName().equals(name)) return true;
		}
		
		return false;
	}
	
	private MetaClass getMetaClassByName(String name){		
		for ( MetaClass mc : outModel.allMetaClasses()) {
			if(mc.getName() != null && !mc.getName().equals("") && mc.getName().equals(name)) return mc;
		}
		
		return null;
	}
}
