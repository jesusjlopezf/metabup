/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.lib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import metabup.annotations.catalog.refactoring.Multiplicity;
import metabup.annotations.catalog.refactoring.Singularize;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.general.AssistanceTip;
import metabup.assistance.general.Solution;
import metabup.lexicon.utils.LexicalInflector;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;

public class SingularReferenceNameAssistance extends AssistanceTip implements IAssistanceTip {
	private final String P_ACTION = "Suggestion in case of reference 'plural name / singular multiplcity' conflict";
		private final String P_ACTION_NAME = "Pluralize name";
		private final String P_ACTION_MULTIPLICITY = "Upgrade multiplicity";
		//private final String P_ACTION_ASK = "Ask me first";
		
	public SingularReferenceNameAssistance(){
		super();
		super.setName("Detect plural-named references with singular multiplicity");
						
		super.addPreference(P_ACTION, Arrays.asList(P_ACTION_NAME, P_ACTION_MULTIPLICITY/*, P_ACTION_ASK*/));
		super.initializePreferences();
	}
	
	public boolean calculateSolutions() {		
		////System.out.println("hey there");
		for(MetaClass mc : this.getTargetMetaModel().getClasses()){
			////System.out.println("I'm checking class " + mc.getName());
			for(Feature f : mc.getFeatures()){				
				if(f instanceof Reference){
					////System.out.println("I'm checking reference " + f.getName());
					Reference r = (Reference)f;
					
					if(LexicalInflector.isPlural(r.getName()) && r.getMax() == 1){												
						List<AnnotatedElement> set = new ArrayList<AnnotatedElement>();
						set.add(r);
						
						if(super.getPreferenceValue(P_ACTION).equals(P_ACTION_NAME))						
							super.putSolution("The reference \'" + r.getName() + "\' has a plural name and singular multiplicity. Rename it \'" + LexicalInflector.singularize(r.getName()) + "\'", set, new Singularize());
						else if(super.getPreferenceValue(P_ACTION).equals(P_ACTION_MULTIPLICITY)){
							Solution sol = super.putSolution("The reference \'" + r.getName() + "\' has a plural name and singular multiplicity. Upgrade it to \'*\'", set, new Multiplicity());
							sol.addParam("min", Integer.toString(r.getMin()));
							sol.addParam("max", Integer.toString(-1));
						}
					}
				}
			}
		}
		
		if(super.solutions.isEmpty()) return false;
		else return true;
	}


	
}
