/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.dnd;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaModel;

public class DropMetaModelElementTableTreeViewer extends TreeViewer{
	List<AnnotatedElement> elements = new ArrayList<AnnotatedElement>();
	IStructuredSelection selection = null;
	MetaModel metamodel = null;
	
	/*public void refresh(MetaModel) {
		if(descriptions!=null){
			this.descriptions = descriptions;
			setInput(descriptions);
			refresh();
			expandAll();
		}
	}*/
	
	public void add(Object element){
		System.out.println("i'm here");
		if(element instanceof AnnotatedElement){
			System.out.println(" and yo no im're");
			elements.add((AnnotatedElement)element);
			setInput(elements);
		}
		
		refresh();
		expandAll();
	}
	
	public DropMetaModelElementTableTreeViewer(Composite parent, MetaModel metamodel) {	
		super(parent);
		int operations = DND.DROP_COPY | DND.DROP_MOVE;
		Transfer[] transferTypes = new Transfer[]{TextTransfer.getInstance()};
	    addDropSupport(operations, transferTypes, new MetaModelElementDropListener(this, metamodel));
	    setContentProvider(new TableTreeContentProvider());
		setLabelProvider(new TableTreeLabelProvider());
		defineColumns(getTree());
	    setInput(elements);
		expandAll();
	}

	private void defineColumns(Tree tree) {
		TreeColumn id = new TreeColumn(tree, SWT.LEFT);
	    id.setAlignment(SWT.LEFT);
	    id.setText("Element");
	    id.setWidth(200);
	      
	    TreeColumn data = new TreeColumn(tree, SWT.LEFT);
	    data.setAlignment(SWT.LEFT);
	    data.setText("Type");
	    data.setWidth(200);
	      
	    TreeColumn type = new TreeColumn(tree, SWT.LEFT);
	    type.setAlignment(SWT.LEFT);
	    type.setText("Min");
	    type.setWidth(200);
	    
	    TreeColumn fragment = new TreeColumn(tree, SWT.LEFT);
	    fragment.setAlignment(SWT.LEFT);
	    fragment.setText("Max");
	    fragment.setWidth(200);
	    
	    tree.setLinesVisible(true);
	    tree.setHeaderVisible(true);
	}
	
	public List<AnnotatedElement> getSelectedElements(){
		if(elements.isEmpty()) return null;
		return this.elements;
	}
}
