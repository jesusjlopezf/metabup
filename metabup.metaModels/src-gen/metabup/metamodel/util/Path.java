/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.metamodel.util;

import java.util.ArrayList;
import java.util.List;

import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;
import metabup.metamodel.impl.ReferenceImpl;

public class Path {
	private MetaClass nodes[] = null;
	private int refSort = ReferenceImpl.IRRELEVANT;
	private boolean byInheritance;
	
	public Path(MetaClass nodes[], int refSort, boolean inh){
		this.nodes = nodes;
		this.refSort = refSort;
		this.byInheritance = inh;
	}
	
	public MetaClass getStartingPoint(){
		return nodes[0];
	}
	
	public MetaClass getEndingPoint(){
		return nodes[nodes.length-1];
	}
	
	public List<MetaClass> getStepPoints(){
		List<MetaClass> steps = new ArrayList<MetaClass>();
		
		for(MetaClass mc : nodes)
			steps.add(mc);
		
		return steps;
	}
	
	public boolean isCyclic(){		
		if(this.getStartingPoint().equals(this.getEndingPoint())) return true;
		return false;
	}
	
	public boolean isContainment(){
		if(nodes != null && nodes.length > 1){
			for(int i = 0; i<nodes.length; i++)
				if(i<nodes.length-1)
					if(!nodes[i].references(nodes[i+1], 1, 1, 0, ReferenceImpl.CONTAINMENT, true, false, null))
						return false;
			
			return true;
		}
		
		return false;
	}
	
	public boolean anyInheritedReference(){
		if(nodes != null && nodes.length > 1){
			for(int i = 0; i<nodes.length; i++)
				if(i<nodes.length-1)
					if(!nodes[i].straightReference(nodes[i+1], ReferenceImpl.IRRELEVANT, false, false))
						return true;
			
			return false;
		}
		
		return false;
	}
	
	public int[] getCollectedMultiplicity(){
		int minmax[] = {1 , 1};
						
		for(int i=0; i<nodes.length-1; i++){
			int thisminmax[] = {0, 0};
			for(Reference r : nodes[i].getReferences(this.byInheritance, this.refSort)){
				if(r.getReference().getId().equals(nodes[i+1].getId())){
					if(thisminmax[0] < r.getMin()) thisminmax[0] = r.getMin();
					if(thisminmax[1] < r.getMax() && thisminmax[1] != -1) thisminmax[1] = r.getMax();
					else if(r.getMax() == -1) thisminmax[1] = -1; 
				}
			}						
			
			minmax[0] = minmax[0] * thisminmax[0];
			if(thisminmax[1] == -1) minmax[1] = -1;
			else minmax[1] = minmax[1] * thisminmax[1];
		}
		
		return minmax;		
	}
	
	public String toString(){	
		String string = "";
		
		for(MetaClass n : nodes) string += n.getName() + " - ";
		return string;
	}
}
