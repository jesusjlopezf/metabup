/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.preferences;

import metabest.preferences.Activator;

import org.eclipse.jface.preference.IPreferenceStore;

/**
 * Constant definitions for plug-in preferences
 */
public class MetaBestPreferenceConstants {

	public static final String ANTIPATTERN_LIB_FILES[] = {"data/antipattern/design.mbm",
		"data/antipattern/quality.mbm",
		"data/antipattern/style.mbm"};
	
	public static final String FAST_PERMUTATION_CHECK = "fast_permutation_check";
	
	public static final String MISSING_ASSERTION_LITERAL = "missing";
	
	public static final String UNKNOWN_ASSERTION_LITERAL = "unknown";
	
	public static String getPreferenceStringValue(String preferenceName){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(preferenceName);
	}
	
	public static boolean getPreferenceBooleanValue(String preferenceName){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getBoolean(preferenceName);
	}
	
}
