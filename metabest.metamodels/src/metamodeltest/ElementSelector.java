/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Selector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.ElementSelector#getElementFilter <em>Element Filter</em>}</li>
 *   <li>{@link metamodeltest.ElementSelector#getElementConditioner <em>Element Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getElementSelector()
 * @model
 * @generated
 */
public interface ElementSelector extends Selector {
	/**
	 * Returns the value of the '<em><b>Element Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Filter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Filter</em>' containment reference.
	 * @see #setElementFilter(ElementFilter)
	 * @see metamodeltest.MetamodeltestPackage#getElementSelector_ElementFilter()
	 * @model containment="true"
	 * @generated
	 */
	ElementFilter getElementFilter();

	/**
	 * Sets the value of the '{@link metamodeltest.ElementSelector#getElementFilter <em>Element Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Filter</em>' containment reference.
	 * @see #getElementFilter()
	 * @generated
	 */
	void setElementFilter(ElementFilter value);

	/**
	 * Returns the value of the '<em><b>Element Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Conditioner</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Conditioner</em>' containment reference.
	 * @see #setElementConditioner(ElementFilter)
	 * @see metamodeltest.MetamodeltestPackage#getElementSelector_ElementConditioner()
	 * @model containment="true"
	 * @generated
	 */
	ElementFilter getElementConditioner();

	/**
	 * Sets the value of the '{@link metamodeltest.ElementSelector#getElementConditioner <em>Element Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Conditioner</em>' containment reference.
	 * @see #getElementConditioner()
	 * @generated
	 */
	void setElementConditioner(ElementFilter value);

} // ElementSelector
