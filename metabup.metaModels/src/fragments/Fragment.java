/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments;

import java.util.List;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fragments.Fragment#getFtype <em>Ftype</em>}</li>
 *   <li>{@link fragments.Fragment#getObjects <em>Objects</em>}</li>
 * </ul>
 *
 * @see fragments.FragmentsPackage#getFragment()
 * @model
 * @generated
 */
public interface Fragment extends TypedElement {
	/**
	 * Returns the value of the '<em><b>Ftype</b></em>' attribute.
	 * The literals are from the enumeration {@link fragments.FragmentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ftype</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ftype</em>' attribute.
	 * @see fragments.FragmentType
	 * @see #setFtype(FragmentType)
	 * @see fragments.FragmentsPackage#getFragment_Ftype()
	 * @model
	 * @generated
	 */
	FragmentType getFtype();

	/**
	 * Sets the value of the '{@link fragments.Fragment#getFtype <em>Ftype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ftype</em>' attribute.
	 * @see fragments.FragmentType
	 * @see #getFtype()
	 * @generated
	 */
	void setFtype(FragmentType value);

	/**
	 * Returns the value of the '<em><b>Objects</b></em>' containment reference list.
	 * The list contents are of type {@link fragments.Object}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Objects</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Objects</em>' containment reference list.
	 * @see fragments.FragmentsPackage#getFragment_Objects()
	 * @model containment="true"
	 * @generated
	 */
	EList<fragments.Object> getObjects();
	List<fragments.Object> getObjects(String type);
	fragments.Object getObjectByName(String name);
	public String toString();

} // Fragment
