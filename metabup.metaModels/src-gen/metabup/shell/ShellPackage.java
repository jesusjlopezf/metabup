/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package metabup.shell;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see metabup.shell.ShellFactory
 * @model kind="package"
 * @generated
 */
public interface ShellPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "shell";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://metabup/shell";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "shell";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ShellPackage eINSTANCE = metabup.shell.impl.ShellPackageImpl.init();

	/**
	 * The meta object id for the '{@link metabup.shell.impl.ShellModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.shell.impl.ShellModelImpl
	 * @see metabup.shell.impl.ShellPackageImpl#getShellModel()
	 * @generated
	 */
	int SHELL_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL_MODEL__NAME = 0;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL_MODEL__COMMANDS = 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL_MODEL_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link metabup.shell.impl.CommandImpl <em>Command</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.shell.impl.CommandImpl
	 * @see metabup.shell.impl.ShellPackageImpl#getCommand()
	 * @generated
	 */
	int COMMAND = 1;

	/**
	 * The number of structural features of the '<em>Command</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link metabup.shell.impl.FragmentCommandImpl <em>Fragment Command</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.shell.impl.FragmentCommandImpl
	 * @see metabup.shell.impl.ShellPackageImpl#getFragmentCommand()
	 * @generated
	 */
	int FRAGMENT_COMMAND = 2;

	/**
	 * The feature id for the '<em><b>Fragment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_COMMAND__FRAGMENT = COMMAND_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Fragment Command</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_COMMAND_FEATURE_COUNT = COMMAND_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link metabup.shell.ShellModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see metabup.shell.ShellModel
	 * @generated
	 */
	EClass getShellModel();

	/**
	 * Returns the meta object for the attribute '{@link metabup.shell.ShellModel#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metabup.shell.ShellModel#getName()
	 * @see #getShellModel()
	 * @generated
	 */
	EAttribute getShellModel_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link metabup.shell.ShellModel#getCommands <em>Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Commands</em>'.
	 * @see metabup.shell.ShellModel#getCommands()
	 * @see #getShellModel()
	 * @generated
	 */
	EReference getShellModel_Commands();

	/**
	 * Returns the meta object for class '{@link metabup.shell.Command <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command</em>'.
	 * @see metabup.shell.Command
	 * @generated
	 */
	EClass getCommand();

	/**
	 * Returns the meta object for class '{@link metabup.shell.FragmentCommand <em>Fragment Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fragment Command</em>'.
	 * @see metabup.shell.FragmentCommand
	 * @generated
	 */
	EClass getFragmentCommand();

	/**
	 * Returns the meta object for the containment reference '{@link metabup.shell.FragmentCommand#getFragment <em>Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fragment</em>'.
	 * @see metabup.shell.FragmentCommand#getFragment()
	 * @see #getFragmentCommand()
	 * @generated
	 */
	EReference getFragmentCommand_Fragment();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ShellFactory getShellFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link metabup.shell.impl.ShellModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.shell.impl.ShellModelImpl
		 * @see metabup.shell.impl.ShellPackageImpl#getShellModel()
		 * @generated
		 */
		EClass SHELL_MODEL = eINSTANCE.getShellModel();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHELL_MODEL__NAME = eINSTANCE.getShellModel_Name();

		/**
		 * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHELL_MODEL__COMMANDS = eINSTANCE.getShellModel_Commands();

		/**
		 * The meta object literal for the '{@link metabup.shell.impl.CommandImpl <em>Command</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.shell.impl.CommandImpl
		 * @see metabup.shell.impl.ShellPackageImpl#getCommand()
		 * @generated
		 */
		EClass COMMAND = eINSTANCE.getCommand();

		/**
		 * The meta object literal for the '{@link metabup.shell.impl.FragmentCommandImpl <em>Fragment Command</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.shell.impl.FragmentCommandImpl
		 * @see metabup.shell.impl.ShellPackageImpl#getFragmentCommand()
		 * @generated
		 */
		EClass FRAGMENT_COMMAND = eINSTANCE.getFragmentCommand();

		/**
		 * The meta object literal for the '<em><b>Fragment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRAGMENT_COMMAND__FRAGMENT = eINSTANCE.getFragmentCommand_Fragment();

	}

} //ShellPackage
