package metabup.ui.editor.popup.actions;

import java.io.IOException;
import java.util.Collections;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.codegen.ecore.generator.Generator;
import org.eclipse.emf.codegen.ecore.genmodel.GenJDKLevel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModelFactory;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.wizard.WizardDialog;

import com.jesusjlopezf.utils.eclipse.emf.EMFUtils;
import com.jesusjlopezf.utils.eclipse.resources.IProjectUtils;

import metabup.SessionState;
import metabup.annotations.general.AnnotationFactory;
import metabup.extensionpoints.MetaBupMetaModelExportActionContribution;
import metabup.metamodel.MetaModel;
import metabup.transformations.MetaModel2Ecore;
import metabup.transformations.models.WEcoreModel;
import metabup.transformations.ui.editor.wizards.EcoreGenerationParamsWizard;

public class GenerateEMFArtifactsAction extends MetaBupMetaModelExportActionContribution {
	boolean updateEcore = true;
	private WEcoreModel ecore;
	private String name, prefix, uri, extension;
	
	public String getName() {
		return name;
	}

	public String getPrefix() {
		return prefix;
	}
	
	public String getExtension() {
		return extension;
	}

	public String getUri() {
		return uri;
	}

	public WEcoreModel getEcore() {
		return ecore;
	}

	public void run() {
		if(editor != null) super.editor.doSave(null);
		//super.editor.refreshViewer();
		
		SessionState session = editor.getSession();		
		if(session.getMetamodelRoot() == null) return;
		
		
		// CREATE ECORE
		MetaModel mm = session.getMetamodelRoot();
		ecore = session.createEcoreMetamodel();
		AnnotationFactory af = new AnnotationFactory();
		af.initializeFactory();
		
		EcoreGenerationParamsWizard wizard = new EcoreGenerationParamsWizard(mm.getName());
		WizardDialog dialog = new WizardDialog(null, wizard);
		
		if(dialog.open() != WizardDialog.OK) return;
		
		this.prefix = wizard.getPrefix().toLowerCase();
		this.name = wizard.getName();
		this.uri = wizard.getUri();
		this.extension = wizard.getExtension().toLowerCase();
		
		MetaModel2Ecore transformer = new MetaModel2Ecore(mm, af);
		ecore = transformer.execute(ecore, name, prefix, uri);
		
		try {
			ecore.serialize();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return;
		}
		
		try {
			IProjectUtils.createJavaProject(session.getProject().getName());
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}				
		
		// CREATE GENMODEL
		GenModel genModel = createGenModel(session, wizard.getPrefix(), (EPackage)ecore.getResource().getContents().get(0),
				session.getMmFile().getFullPath().removeFileExtension().addFileExtension("ecore").toString(),
				session.getMmFile().getFullPath().removeFileExtension().addFileExtension("genmodel").toString());
				
		createModelCode(genModel, session.getProject());
		
		// register extension
		EMFUtils.registerXMIExtension(this.extension.toLowerCase());
	}		
	
	private GenModel createGenModel(SessionState session, String prefix, final EPackage rootPackage, final String ecoreLocation,
	        final String genModelLocation) {

	        GenModel genModel = GenModelFactory.eINSTANCE.createGenModel();
	        genModel.setComplianceLevel(GenJDKLevel.JDK70_LITERAL);
	        genModel.setModelDirectory(session.getProject().getLocation().toString());
	        genModel.getForeignModel().add(new Path(ecoreLocation).lastSegment());
	        genModel.setModelName(session.getMetamodelRoot().getName());
	        //genModel.setRootExtendsInterface(Constants.GEN_MODEL_EXTENDS_INTERFACE.getValue());
	        genModel.initialize(Collections.singleton(rootPackage));

	        GenPackage genPackage = (GenPackage)genModel.getGenPackages().get(0);
	        genPackage.setPrefix(prefix);

	        try {
	            URI genModelURI = URI.createFileURI(genModelLocation);
	            final XMIResourceImpl genModelResource = new XMIResourceImpl(genModelURI);
	            /*genModelResource.getDefaultSaveOptions().put(XMLResource.OPTION_ENCODING,
	                Constants.GEN_MODEL_XML_ENCODING.getValue());*/
	            genModelResource.getContents().add(genModel);
	            genModelResource.save(Collections.EMPTY_MAP);
	        } catch (IOException e) {
	            String msg = null;
	            /*final String genModelLocationCleaned = LogUtil.stripForgeChars(genModelLocation);
	            if (e instanceof FileNotFoundException) {
	                msg = "Unable to open output file " + genModelLocationCleaned;
	            } else {
	                msg = "Unexpected IO Exception writing " + genModelLocationCleaned;
	            }
	            LOGGER.error(msg, e);*/
	            throw new RuntimeException(msg, e);
	        }
	        
	        return genModel;
	    }
	
	private void createModelCode(GenModel genModel, IProject project){
		genModel.reconcile();
		genModel.setCanGenerate(true);
		genModel.setValidateModel(true);
		genModel.setModelDirectory(project.getFullPath().toString()+"/src");		
		Generator generator = new Generator();
		generator.setInput(genModel);
		generator.generate(genModel, GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE, JavaCore.NATURE_ID, new BasicMonitor.Printing(System.out));
	}
}
