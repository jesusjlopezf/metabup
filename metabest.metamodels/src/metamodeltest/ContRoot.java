/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cont Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.ContRoot#getAbsolute <em>Absolute</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getContRoot()
 * @model
 * @generated
 */
public interface ContRoot extends ClassQualifier {

	/**
	 * Returns the value of the '<em><b>Absolute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Absolute</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Absolute</em>' containment reference.
	 * @see #setAbsolute(BooleanParameter)
	 * @see metamodeltest.MetamodeltestPackage#getContRoot_Absolute()
	 * @model containment="true"
	 * @generated
	 */
	BooleanParameter getAbsolute();

	/**
	 * Sets the value of the '{@link metamodeltest.ContRoot#getAbsolute <em>Absolute</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Absolute</em>' containment reference.
	 * @see #getAbsolute()
	 * @generated
	 */
	void setAbsolute(BooleanParameter value);
} // ContRoot
