/**
 */
package graphicProperties;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see graphicProperties.GraphicPropertiesFactory
 * @model kind="package"
 * @generated
 */
public interface GraphicPropertiesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "graphicProperties";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/metabup.metaModels/model/graphicProperties.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "gp";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GraphicPropertiesPackage eINSTANCE = graphicProperties.impl.GraphicPropertiesPackageImpl.init();

	/**
	 * The meta object id for the '{@link graphicProperties.impl.GraphicPropertiesImpl <em>Graphic Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see graphicProperties.impl.GraphicPropertiesImpl
	 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getGraphicProperties()
	 * @generated
	 */
	int GRAPHIC_PROPERTIES = 0;

	/**
	 * The number of structural features of the '<em>Graphic Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHIC_PROPERTIES_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Graphic Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRAPHIC_PROPERTIES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link graphicProperties.impl.NodePropertiesImpl <em>Node Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see graphicProperties.impl.NodePropertiesImpl
	 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getNodeProperties()
	 * @generated
	 */
	int NODE_PROPERTIES = 1;

	/**
	 * The number of structural features of the '<em>Node Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_PROPERTIES_FEATURE_COUNT = GRAPHIC_PROPERTIES_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Node Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_PROPERTIES_OPERATION_COUNT = GRAPHIC_PROPERTIES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link graphicProperties.impl.EdgePropertiesImpl <em>Edge Properties</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see graphicProperties.impl.EdgePropertiesImpl
	 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getEdgeProperties()
	 * @generated
	 */
	int EDGE_PROPERTIES = 2;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_PROPERTIES__COLOR = GRAPHIC_PROPERTIES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_PROPERTIES__WIDTH = GRAPHIC_PROPERTIES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Line Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_PROPERTIES__LINE_TYPE = GRAPHIC_PROPERTIES_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Src Arrow</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_PROPERTIES__SRC_ARROW = GRAPHIC_PROPERTIES_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Tgt Arrow</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_PROPERTIES__TGT_ARROW = GRAPHIC_PROPERTIES_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Edge Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_PROPERTIES_FEATURE_COUNT = GRAPHIC_PROPERTIES_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Edge Properties</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDGE_PROPERTIES_OPERATION_COUNT = GRAPHIC_PROPERTIES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link graphicProperties.LineType <em>Line Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see graphicProperties.LineType
	 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getLineType()
	 * @generated
	 */
	int LINE_TYPE = 3;

	/**
	 * The meta object id for the '{@link graphicProperties.ArrowType <em>Arrow Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see graphicProperties.ArrowType
	 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getArrowType()
	 * @generated
	 */
	int ARROW_TYPE = 4;


	/**
	 * Returns the meta object for class '{@link graphicProperties.GraphicProperties <em>Graphic Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Graphic Properties</em>'.
	 * @see graphicProperties.GraphicProperties
	 * @generated
	 */
	EClass getGraphicProperties();

	/**
	 * Returns the meta object for class '{@link graphicProperties.NodeProperties <em>Node Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Properties</em>'.
	 * @see graphicProperties.NodeProperties
	 * @generated
	 */
	EClass getNodeProperties();

	/**
	 * Returns the meta object for class '{@link graphicProperties.EdgeProperties <em>Edge Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edge Properties</em>'.
	 * @see graphicProperties.EdgeProperties
	 * @generated
	 */
	EClass getEdgeProperties();

	/**
	 * Returns the meta object for the attribute '{@link graphicProperties.EdgeProperties#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see graphicProperties.EdgeProperties#getColor()
	 * @see #getEdgeProperties()
	 * @generated
	 */
	EAttribute getEdgeProperties_Color();

	/**
	 * Returns the meta object for the attribute '{@link graphicProperties.EdgeProperties#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see graphicProperties.EdgeProperties#getWidth()
	 * @see #getEdgeProperties()
	 * @generated
	 */
	EAttribute getEdgeProperties_Width();

	/**
	 * Returns the meta object for the attribute '{@link graphicProperties.EdgeProperties#getLineType <em>Line Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Line Type</em>'.
	 * @see graphicProperties.EdgeProperties#getLineType()
	 * @see #getEdgeProperties()
	 * @generated
	 */
	EAttribute getEdgeProperties_LineType();

	/**
	 * Returns the meta object for the attribute '{@link graphicProperties.EdgeProperties#getSrcArrow <em>Src Arrow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Src Arrow</em>'.
	 * @see graphicProperties.EdgeProperties#getSrcArrow()
	 * @see #getEdgeProperties()
	 * @generated
	 */
	EAttribute getEdgeProperties_SrcArrow();

	/**
	 * Returns the meta object for the attribute '{@link graphicProperties.EdgeProperties#getTgtArrow <em>Tgt Arrow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tgt Arrow</em>'.
	 * @see graphicProperties.EdgeProperties#getTgtArrow()
	 * @see #getEdgeProperties()
	 * @generated
	 */
	EAttribute getEdgeProperties_TgtArrow();

	/**
	 * Returns the meta object for enum '{@link graphicProperties.LineType <em>Line Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Line Type</em>'.
	 * @see graphicProperties.LineType
	 * @generated
	 */
	EEnum getLineType();

	/**
	 * Returns the meta object for enum '{@link graphicProperties.ArrowType <em>Arrow Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Arrow Type</em>'.
	 * @see graphicProperties.ArrowType
	 * @generated
	 */
	EEnum getArrowType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GraphicPropertiesFactory getGraphicPropertiesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link graphicProperties.impl.GraphicPropertiesImpl <em>Graphic Properties</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see graphicProperties.impl.GraphicPropertiesImpl
		 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getGraphicProperties()
		 * @generated
		 */
		EClass GRAPHIC_PROPERTIES = eINSTANCE.getGraphicProperties();

		/**
		 * The meta object literal for the '{@link graphicProperties.impl.NodePropertiesImpl <em>Node Properties</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see graphicProperties.impl.NodePropertiesImpl
		 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getNodeProperties()
		 * @generated
		 */
		EClass NODE_PROPERTIES = eINSTANCE.getNodeProperties();

		/**
		 * The meta object literal for the '{@link graphicProperties.impl.EdgePropertiesImpl <em>Edge Properties</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see graphicProperties.impl.EdgePropertiesImpl
		 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getEdgeProperties()
		 * @generated
		 */
		EClass EDGE_PROPERTIES = eINSTANCE.getEdgeProperties();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE_PROPERTIES__COLOR = eINSTANCE.getEdgeProperties_Color();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE_PROPERTIES__WIDTH = eINSTANCE.getEdgeProperties_Width();

		/**
		 * The meta object literal for the '<em><b>Line Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE_PROPERTIES__LINE_TYPE = eINSTANCE.getEdgeProperties_LineType();

		/**
		 * The meta object literal for the '<em><b>Src Arrow</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE_PROPERTIES__SRC_ARROW = eINSTANCE.getEdgeProperties_SrcArrow();

		/**
		 * The meta object literal for the '<em><b>Tgt Arrow</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDGE_PROPERTIES__TGT_ARROW = eINSTANCE.getEdgeProperties_TgtArrow();

		/**
		 * The meta object literal for the '{@link graphicProperties.LineType <em>Line Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see graphicProperties.LineType
		 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getLineType()
		 * @generated
		 */
		EEnum LINE_TYPE = eINSTANCE.getLineType();

		/**
		 * The meta object literal for the '{@link graphicProperties.ArrowType <em>Arrow Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see graphicProperties.ArrowType
		 * @see graphicProperties.impl.GraphicPropertiesPackageImpl#getArrowType()
		 * @generated
		 */
		EEnum ARROW_TYPE = eINSTANCE.getArrowType();

	}

} //GraphicPropertiesPackage
