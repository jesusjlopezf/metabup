/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.issues.lib;

import java.util.ArrayList;
import java.util.List;

import metabup.issues.general.categories.Conflict;
import metabup.metamodel.Annotation;

public class ViolatedTreeStructure extends Conflict {
	public ViolatedTreeStructure(metabup.metamodel.Reference mmr, List<fragments.Reference> trouble){
		if(mmr != null && trouble != null && !trouble.isEmpty()){			
			super.elements.add(mmr);
			
			List<String> names = new ArrayList<String>();
			
			for(fragments.Reference r : trouble)
				names.add(((fragments.Object)r.eContainer()).getName() + ":" + r.getName());
						
			super.setDescription("Tree structure violated in reference(s) " + names + ". Delete annotation");
		}
	}
	
	@Override
	public void run() {
		metabup.metamodel.Reference r = (metabup.metamodel.Reference)elements.get(0);
		Annotation removeA = null;
		
		for(Annotation a : r.getAnnotations()) if(a.getName().equals("tree")) removeA = a;		
		if(removeA != null) r.getAnnotations().remove(removeA);
	}
}
