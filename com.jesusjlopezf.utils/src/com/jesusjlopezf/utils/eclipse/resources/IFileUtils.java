/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.jesusjlopezf.utils.eclipse.resources;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.osgi.framework.Bundle;

// TODO: Auto-generated Javadoc
/**
 * The Class EclipseIResources.
 */
public abstract class IFileUtils {
	
	/**
	 * Static method for turning an unformatted object into an IFile instance.
	 *
	 * @param object the object
	 * @return IFile	IFile instance / NULL if it's not possible.
	 */
	public static IFile convertObjectToIFile(Object object) {

		if (object instanceof IFile) {
			return (IFile) object;
		}

		if (object instanceof IAdaptable) {
			IAdaptable adaptable = (IAdaptable) object;
			IFile file = (IFile) adaptable.getAdapter(IFile.class);

			if (file != null) {
				return file;
			}
		}

		return null;
	}
	
	
	
	/**
	 * Static method for turning a file's IFile representation into File.
	 *
	 * @param ifile the ifile
	 * @return IFile	File instance.
	 */
	public static File IFile2File(IFile ifile){
		File file = new File(ifile.getLocation().toString());
		return file;
	}
	
	/**
	 * Static method for getting the list of projects with some given nature assigned
	 *
	 * @param IWorkspace	ws	the workspace
	 * @param String	nature	the project nature
	 * @return IProject[]	projects	The list of projects with 'nature' assigned
	 */
	public static IProject[] getProjectsByNature(IWorkspace ws, String nature){	
		IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
		ArrayList<IProject> retProj = new ArrayList<IProject>();
		int cont = 0;
		
		if(projects == null) return null;
		else{
			for(int i=0; (projects != null) && (i < projects.length); i++){
				try {
					if(projects[i].getNature(nature) != null){
						retProj.add(projects[i]);
						cont++;
					}
				} catch (CoreException e) {
					//TransformationTools.log(IStatus.ERROR, "Unexpected error");			
				}
			}
			
			projects = new IProject[cont];
			
			for(int i=0; i<cont; i++) projects[i] = retProj.get(i);
			
			return projects; 
		}		
	}		
}
