/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compound Qualifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.CompoundQualifier#getChildren <em>Children</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getCompoundQualifier()
 * @model abstract="true"
 * @generated
 */
public interface CompoundQualifier extends Qualifier {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link metamodeltest.Qualifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see metamodeltest.MetamodeltestPackage#getCompoundQualifier_Children()
	 * @model containment="true" lower="2"
	 * @generated
	 */
	EList<Qualifier> getChildren();

} // CompoundQualifier
