/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches.importers.dia;

import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import metabup.extensionpoints.MetaBupSketchImportActionContribution;
import metabup.sketches.Sketch;
import metabup.sketches.Sketch2Legend;
import metabup.sketches.importers.yed.YedImporter;
import metabup.sketches.legend.Legend;
import metabup.ui.editor.popup.actions.MetaBupEditorAction;

/**
 * Opens an file dialog to select .dia diagram, then it applies the importer
 * and generates a fragment model, which is added to current shell.
 *  
 * @author jesus
 *
 */
public class ImportFragmentFromDiaAction extends MetaBupSketchImportActionContribution {
	
	@Override
	public Sketch readSketch(FileInputStream stream) {
		DiaImporter importer = new DiaImporter();
		Sketch sketch = null;
		
		try {
			sketch = importer.read(stream, "dia_sketch");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		return sketch;
	}

	@Override
	public Legend readLegend(FileInputStream stream) {
		DiaImporter importer = new DiaImporter();
		Sketch legendSketch;
		
		try {
			legendSketch = importer.read(stream, "legend");
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		Legend legend = new Sketch2Legend(legendSketch).execute();
		return legend;
	}
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		super.addExtension("dia");
	}
	
	/*OLD RUNNER (just in case)
	 * 
	 * public void run() {
		Shell shell = editor.getEditorSite().getShell();
		FileDialog dialog = new FileDialog(shell);
		dialog.setText("Select a dia file...");
		dialog.setFilterExtensions(new String[] { "*.dia" } );
		String fileName = dialog.open();
		if ( fileName != null ) {
			Path path   = new Path(fileName);
			String name = path.lastSegment().substring(0, path.lastSegment().length() - (path.getFileExtension().length() + 1));

			DiaImporter importer = new DiaImporter();
			Legend metabup.sketches.legend = null;
			
			try {	
				FileDialog legendDialog = new FileDialog(shell);
				legendDialog.setText("Select a metabup.sketches.legend file... (optional)");
				legendDialog.setFilterExtensions(new String[] { "*.dia" } );
				String dialogFile = legendDialog.open();
				if ( dialogFile != null ) {
					DiaImporter legendImporter = new DiaImporter();
					Sketch legendSketch = legendImporter.read(new FileInputStream(dialogFile), "metabup.sketches.legend");
					metabup.sketches.legend = new Sketch2Legend(legendSketch).execute();
				}
			
				Sketch sketch     = importer.read(new FileInputStream(fileName), name);
				Fragment fragment = new Sketch2Fragment(sketch, metabup.sketches.legend).execute();
				getSession().updateShell(Arrays.asList(new Fragment[] { fragment }));
			} catch (Exception e) {
				Status s = new Status(Status.ERROR, metabup.Activator.PLUGIN_ID, e.getMessage(), e);
				ErrorDialog.openError(shell, "Error importing from editor", e.getMessage(), s);
			}
		}
		
	}*/



}
