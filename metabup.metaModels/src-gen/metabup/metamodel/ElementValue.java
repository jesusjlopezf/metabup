/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.metamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metabup.metamodel.ElementValue#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see metabup.metamodel.MetamodelPackage#getElementValue()
 * @model
 * @generated
 */
public interface ElementValue extends ParamValue {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference.
	 * @see #setValue(AnnotatedElement)
	 * @see metabup.metamodel.MetamodelPackage#getElementValue_Value()
	 * @model required="true"
	 * @generated
	 */
	AnnotatedElement getValue();

	/**
	 * Sets the value of the '{@link metabup.metamodel.ElementValue#getValue <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(AnnotatedElement value);

} // ElementValue
