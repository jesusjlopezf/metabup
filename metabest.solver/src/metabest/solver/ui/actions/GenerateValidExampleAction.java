/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.actions;

import metabup.SessionState;
import metabup.extensionpoints.MetaBupMetaModelExportActionContribution;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.wizard.WizardDialog;
import metabest.solver.ui.wizards.GenerateValidExampleWizard;

public class GenerateValidExampleAction extends MetaBupMetaModelExportActionContribution {
	public void run() {
		SessionState session = editor.getSession();		
		if(session.getMetamodelRoot() == null) return;
		
		if(session.getMmFile() == null) return;
		IProject project = session.getMmFile().getProject();				 
		
		WizardDialog dialog = new WizardDialog(null, new GenerateValidExampleWizard(project, session.getMetamodelRoot()));
		dialog.open(); 		
	}

}
