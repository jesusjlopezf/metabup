/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.dnd;



import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import metabest.solver.ui.preferences.PreferenceConstants;
import metabup.annotations.catalog.design.Composition;
import metabup.metamodel.Attribute;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;



public class TableTreeLabelProvider implements ITableLabelProvider {

	@Override
	public void addListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if(element instanceof MetaClass){
			switch (columnIndex){
			 	case 0: return ((MetaClass) element).getName();
		        case 1: return "metaclass";
		        case 2: return Integer.toString(PreferenceConstants.getPreferenceIntegerValue(PreferenceConstants.P_MIN_OBJECT_NUMBER));
		        case 3: return Integer.toString(PreferenceConstants.getPreferenceIntegerValue(PreferenceConstants.P_MAX_OBJECT_NUMBER));
			}
		}
		
		int min = 0, max = -1;
		String mcName = "";
		
		if(element instanceof Feature){
			Feature feature = (Feature) element;
			
			int prefMin = PreferenceConstants.getPreferenceIntegerValue(PreferenceConstants.P_MIN_DEFAULT_REFERENCE_CARDINALITY);
			int prefMax = PreferenceConstants.getPreferenceIntegerValue(PreferenceConstants.P_MAX_DEFAULT_REFERENCE_CARDINALITY);			
			
			min = Integer.max(((Feature) element).getMin(), prefMin);
			
			
			if(((Feature) element).getMax() < 0) max = prefMax;
			else if(prefMax < 0) max= ((Feature) element).getMax();
			else max = Integer.min(((Feature) element).getMax(), prefMax);
			
			MetaClass mc = (MetaClass)((Feature) element).eContainer();
			mcName = mc.getName();
		}
		
		if(element instanceof Attribute){
			switch (columnIndex){
				case 0: return mcName + "." + ((Attribute) element).getName();
				case 1: return "attribute";
				case 2: return Integer.toString(min);
		        case 3: return Integer.toString(max);
			}
		}
		
		if(element instanceof Reference){
			switch (columnIndex){
				case 0: return mcName + "." + ((Reference) element).getName();
				case 1:
					if(((Reference) element).isAnnotated(Composition.NAME)) return "containment";
					else return "association";
				case 2: return Integer.toString(min);
		        case 3: return Integer.toString(max);
			}
		}
		
		return null;
	}
	/*@Override
	public String getText(Object element) {
		String s = "";
		if(element instanceof NamedElement) s = ((NamedElement) element).getName();
		if(element instanceof DataSource) s = s + " [" + ((DataSource) element).getLink() + "]";
		if(element instanceof Node) s = s + " [" + ((Node) element).getRepresentedAs() + "]";
		return s;
	}*/
} 
