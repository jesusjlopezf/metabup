/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.design;

import metabup.annotations.extensionpoint.DesignAnnotation;
import metabup.annotations.general.Annotation;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;

public class Abstract extends DesignAnnotation {
	
	public static final String NAME = "abstract";

	public Abstract(){
		super.setName(NAME);
		super.setDescription("It sets abstract the annotated MetaClass");
	}
	
	@Override
	public MetaModel perform() {
		if(element instanceof MetaClass){			
			((MetaClass) element).setIsAbstract(true);					
			return targetMetaModel;									
		}else return null;		
	}
}
