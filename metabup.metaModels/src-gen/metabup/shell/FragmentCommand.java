/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package metabup.shell;

import org.eclipse.emf.ecore.EObject;

import fragments.Fragment;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fragment Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metabup.shell.FragmentCommand#getFragment <em>Fragment</em>}</li>
 * </ul>
 *
 * @see metabup.shell.ShellPackage#getFragmentCommand()
 * @model
 * @generated
 */
public interface FragmentCommand extends Command {
	/**
	 * Returns the value of the '<em><b>Fragment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fragment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fragment</em>' containment reference.
	 * @see #setFragment(Fragment)
	 * @see metabup.shell.ShellPackage#getFragmentCommand_Fragment()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Fragment getFragment();

	/**
	 * Sets the value of the '{@link metabup.shell.FragmentCommand#getFragment <em>Fragment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fragment</em>' containment reference.
	 * @see #getFragment()
	 * @generated
	 */
	void setFragment(Fragment value);

} // FragmentCommand
