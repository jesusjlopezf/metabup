/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.extensionpoints;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import fragments.Fragment;
import metabup.sketches.Sketch;
import metabup.sketches.Sketch2Fragment;
import metabup.sketches.legend.Legend;
import metabup.sketches.resources.LegendFolder;
import metabup.ui.editor.popup.actions.MetaBupEditorAction;

public abstract class MetaBupSketchImportActionContribution extends MetaBupEditorAction implements IMetaBupSketchImportActionContribution {
	private List<String> extensions = new ArrayList<String>();		
	protected Legend legend = null;	
	protected Sketch sketch = null;
	boolean legendByFolder = false;
	private IProject project = null;
	
	@Override
	public final void run() {		
		initialize();
		this.project = editor.getSession().getProject();	
		Shell shell = editor.getEditorSite().getShell();
		FileDialog dialog = new FileDialog(shell);
		dialog.setText("Select a sketch file...");		
		if(!extensions.isEmpty()) dialog.setFilterExtensions(extensions.toArray(new String[extensions.size()]));
		String fileName = dialog.open();
		
		if(!legendByFolder){
			FileDialog legendDialog = new FileDialog(shell);
			legendDialog.setText("Select a legend file... (optional)");
			legendDialog.setFilterExtensions(extensions.toArray(new String[extensions.size()]));
			String legendFile;
			legendFile = legendDialog.open();			
			
			if ( legendFile != null )
				try {
					legend = readLegend(new FileInputStream(legendFile));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}else{
			legend = importLegendElements();
		}
		
		if ( fileName != null ) {
			Path path   = new Path(fileName);
			String name = path.lastSegment().substring(0, path.lastSegment().length() - (path.getFileExtension().length() + 1));
			
			try {
				sketch = readSketch(new FileInputStream(fileName));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
				
			Fragment fragment = new Sketch2Fragment(sketch, legend, super.getSession().getMetamodelRoot()).execute();
			getSession().updateShell(Arrays.asList(new Fragment[] { fragment }));
		}
		
	}
	
	private Legend importLegendElements() {
		LegendFolder folder = new LegendFolder(this.project);
		if(!folder.exists()) folder.create(null);		
		return folder.getLegend();
	}

	protected void addExtension(String ext){
		if(!extensions.contains(ext.toLowerCase())) extensions.add("*." + ext.toLowerCase());
	}
	
	public Legend getLegend() {
		return legend;
	}

	protected void setLegend(Legend legend) {
		this.legend = legend;
	}

	public Sketch getSketch() {
		return sketch;
	}

	protected void setSketch(Sketch sketch) {
		this.sketch = sketch;
	}
	
	protected void storeLegendInFolder(boolean folder){
		this.legendByFolder = folder;
	}
	
	protected IProject getProject(){
		return this.project;
	}
}
