/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.refactoring;

import metabup.annotations.extensionpoint.RefactoringAnnotation;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;

public class Remove extends RefactoringAnnotation {
	
	public Remove(){
		super.setName("remove");
		super.setDescription("It removes the annotated MetaClass");
	}
	
	@Override
	public MetaModel perform() {
		if(element instanceof MetaClass){			
			if(!super.removeMetaClass((MetaClass)element)){
				super.putError("The annotated MetaClass couldn't be removed");
				return null;
			}
			
			//for(MetaClass mc : targetMetaModel.getClasses()) //System.out.println("\nCLASS: " + mc.getName());
			
			return targetMetaModel;									
		}else return null;		
	}

}
