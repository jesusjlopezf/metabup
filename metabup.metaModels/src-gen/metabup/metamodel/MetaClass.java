/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.metamodel;

import java.util.List;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metabup.metamodel.MetaClass#getName <em>Name</em>}</li>
 *   <li>{@link metabup.metamodel.MetaClass#getIsAbstract <em>Is Abstract</em>}</li>
 *   <li>{@link metabup.metamodel.MetaClass#getFeatures <em>Features</em>}</li>
 *   <li>{@link metabup.metamodel.MetaClass#getSupers <em>Supers</em>}</li>
 *   <li>{@link metabup.metamodel.MetaClass#getSubclasses <em>Subclasses</em>}</li>
 *   <li>{@link metabup.metamodel.MetaClass#getIncomingRefs <em>Incoming Refs</em>}</li>
 * </ul>
 *
 * @see metabup.metamodel.MetamodelPackage#getMetaClass()
 * @model
 * @generated
 */
public interface MetaClass extends AnnotatedElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see metabup.metamodel.MetamodelPackage#getMetaClass_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link metabup.metamodel.MetaClass#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Abstract</em>' attribute.
	 * @see #setIsAbstract(Boolean)
	 * @see metabup.metamodel.MetamodelPackage#getMetaClass_IsAbstract()
	 * @model
	 * @generated
	 */
	Boolean getIsAbstract();

	/**
	 * Sets the value of the '{@link metabup.metamodel.MetaClass#getIsAbstract <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Abstract</em>' attribute.
	 * @see #getIsAbstract()
	 * @generated
	 */
	void setIsAbstract(Boolean value);

	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link metabup.metamodel.Feature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see metabup.metamodel.MetamodelPackage#getMetaClass_Features()
	 * @model containment="true"
	 * @generated
	 */
	EList<Feature> getFeatures();
	List<Feature> getFeatures(boolean inheritance);
	Feature getFeatureByName(String name, boolean takeInh);

	/**
	 * Returns the value of the '<em><b>Supers</b></em>' reference list.
	 * The list contents are of type {@link metabup.metamodel.MetaClass}.
	 * It is bidirectional and its opposite is '{@link metabup.metamodel.MetaClass#getSubclasses <em>Subclasses</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Supers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Supers</em>' reference list.
	 * @see metabup.metamodel.MetamodelPackage#getMetaClass_Supers()
	 * @see metabup.metamodel.MetaClass#getSubclasses
	 * @model opposite="subclasses"
	 * @generated
	 */
	EList<MetaClass> getSupers();
	List<MetaClass> getSupers(int minHeight, int maxHeight);

	/**
	 * Returns the value of the '<em><b>Subclasses</b></em>' reference list.
	 * The list contents are of type {@link metabup.metamodel.MetaClass}.
	 * It is bidirectional and its opposite is '{@link metabup.metamodel.MetaClass#getSupers <em>Supers</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subclasses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subclasses</em>' reference list.
	 * @see metabup.metamodel.MetamodelPackage#getMetaClass_Subclasses()
	 * @see metabup.metamodel.MetaClass#getSupers
	 * @model opposite="supers"
	 * @generated
	 */
	EList<MetaClass> getSubclasses();
	
	/**
	 * Returns the value of the '<em><b>Incoming Refs</b></em>' reference list.
	 * The list contents are of type {@link metabup.metamodel.Reference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incoming Refs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incoming Refs</em>' reference list.
	 * @see metabup.metamodel.MetamodelPackage#getMetaClass_IncomingRefs()
	 * @model
	 * @generated
	 */
	EList<Reference> getIncomingRefs();

	public List<Reference> getReferences();
	public List<Reference> getReferences(boolean inheritance, int refSort);	
	public Reference getReferenceByName(String name, boolean inheritance);
	public List<Attribute> getAttributes();
	public List<Attribute> getAttributes(boolean inheritance);
	public Attribute getAttributeByName(String name, boolean inheritance);
	public boolean isTop();
	public boolean isSuperTo(MetaClass supermc);
	public boolean isSuperTo(MetaClass supermc, int minHeight, int maxHeight);
	public boolean isSubTo(MetaClass supermc);
	public boolean isSubTo(MetaClass supermc, int minHeight, int maxHeight);
	public boolean references(MetaClass targetmc, int min, int max, int seed, int refSort, boolean byInheritance, boolean subs, List<MetaClass> visited);
	public boolean straightReference(MetaClass targetmc, int refSort, boolean byInheritance, boolean subs);
	public List<Reference> getIncomingRefs(boolean byInheritance, boolean subs, int refSort);
	public List<MetaClass> getAllSupers();
	public List<MetaClass> getAllSubs();
	public int getInheritancePathsTo(MetaClass targetmc, int minHeight, int maxHeight);
	public boolean isReferencedFrom(MetaClass sourcemc, int min, int max, int i, int refSort, boolean byInheritance, boolean subs, List<MetaClass> visited);
	public String toString();
} // MetaClass