/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
#include <windows.h>
#include <stdio.h>

static Id = "$Id: winmain.c,v 1.2 2005/02/01 17:46:22 wn Rel $";

extern int main(int, char**, char**);

int WINAPI
WinMain(HINSTANCE current, HINSTANCE prev, LPSTR cmdline, int showcmd)
{
	int argc = 0;
	char *argv[2];

	argv[argc++] = "wnb";
    return main(argc, argv, NULL);
}
