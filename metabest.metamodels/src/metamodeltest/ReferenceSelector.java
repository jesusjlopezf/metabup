/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Selector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.ReferenceSelector#getReferenceFilter <em>Reference Filter</em>}</li>
 *   <li>{@link metamodeltest.ReferenceSelector#getReferenceConditioner <em>Reference Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getReferenceSelector()
 * @model
 * @generated
 */
public interface ReferenceSelector extends Selector {
	/**
	 * Returns the value of the '<em><b>Reference Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Filter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Filter</em>' containment reference.
	 * @see #setReferenceFilter(ReferenceFilter)
	 * @see metamodeltest.MetamodeltestPackage#getReferenceSelector_ReferenceFilter()
	 * @model containment="true"
	 * @generated
	 */
	ReferenceFilter getReferenceFilter();

	/**
	 * Sets the value of the '{@link metamodeltest.ReferenceSelector#getReferenceFilter <em>Reference Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Filter</em>' containment reference.
	 * @see #getReferenceFilter()
	 * @generated
	 */
	void setReferenceFilter(ReferenceFilter value);

	/**
	 * Returns the value of the '<em><b>Reference Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference Conditioner</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Conditioner</em>' containment reference.
	 * @see #setReferenceConditioner(ReferenceFilter)
	 * @see metamodeltest.MetamodeltestPackage#getReferenceSelector_ReferenceConditioner()
	 * @model containment="true"
	 * @generated
	 */
	ReferenceFilter getReferenceConditioner();

	/**
	 * Sets the value of the '{@link metamodeltest.ReferenceSelector#getReferenceConditioner <em>Reference Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Conditioner</em>' containment reference.
	 * @see #getReferenceConditioner()
	 * @generated
	 */
	void setReferenceConditioner(ReferenceFilter value);

} // ReferenceSelector
