/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.geometry;

import metabup.annotations.catalog.design.Composition;
import metabup.annotations.extensionpoint.DesignAnnotation;
import metabup.metamodel.Annotation;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.Reference;

public class Overlapping extends DesignAnnotation {
	public static final String NAME = "overlapping";
	
	public Overlapping (){
		this.setName("overlapping");
		this.setDescription("It indicates that the annotated reference expresses physical superposition to the pointing objects.");
	}
	
	@Override
	public MetaModel perform() {	
		if(!element.isAnnotated(Composition.NAME)){
			Annotation ann = MetamodelFactory.eINSTANCE.createAnnotation();	
			ann.setName(Composition.NAME);
			element.getAnnotations().add(ann);			
		}
		
		return targetMetaModel;
	}
}
