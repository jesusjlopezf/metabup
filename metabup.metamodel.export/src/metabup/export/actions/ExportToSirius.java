/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.export.actions;


import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.graphictoviewpoint.files.GraphicToViewPoint;
import org.eclipse.sirius.business.api.modelingproject.ModelingProject;
import org.eclipse.sirius.ui.tools.api.project.ModelingProjectManager;
import org.eclipse.sirius.ui.tools.api.project.ViewpointSpecificationProject;
import org.eclipse.ui.PlatformUI;

import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;
import com.jesusjlopezf.utils.eclipse.resources.IProjectUtils;

import graphicProperties.GraphicProperties;
import graphic_representation.GraphicRepresentation;
import metabup.SessionState;
import metabup.export.transformations.EPackage2GraphicRepresentation;
import metabup.fragments.resources.FragmentFolder;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;
import metabup.metamodel.edit.actions.AddNameAttributeToAllClasses;
import metabup.metamodel.edit.actions.AddRootClassAction;
import metabup.sketches.resources.LegendFolder;
import metabup.ui.editor.popup.actions.GenerateEMFArtifactsAction;
import metabup.ui.editor.popup.actions.MetaBupEditorRefreshAction;

public class ExportToSirius extends metabup.extensionpoints.MetaBupMetaModelExportActionContribution {
	
	private static String prefix_name = ".odesign";
	private static final String NL = System.getProperty("line.separator");
	public static final String aird = "aird";
	private MetaModel mm;
	
	public void run() {
		SessionState session = editor.getSession();		
		if(session.getMetamodelRoot() == null) return;
		
		mm = session.getMetamodelRoot();
		
		// check whether or not the meta-model has a root class; if not, then add one
		AddRootClassAction action = new AddRootClassAction();	
		action.setEditor(editor);
		if(action.checkConditions(mm)) mm = action.editMetaModel(mm);
		
		// add the attribute 'name' to all classes if they don't have it
		AddNameAttributeToAllClasses nameAction = new AddNameAttributeToAllClasses();
		nameAction.setEditor(editor);
		if(nameAction.checkConditions(mm)) mm = nameAction.editMetaModel(mm);
		
		// refresh 
		MetaBupEditorRefreshAction refreshAction = new MetaBupEditorRefreshAction();
		refreshAction.setEditor(editor);
		if(action.checkConditions(mm)) mm = action.editMetaModel(mm);
		
		GenerateEMFArtifactsAction emfAction = new GenerateEMFArtifactsAction();
		emfAction.setEditor(editor);
		emfAction.run();				
			
		LegendFolder legendFolder = new LegendFolder(session.getProject());
		
		// we provide the index necessary for the transformation to be able to produce graphic properties in elements
		HashMap<String, GraphicProperties> gpIndex = new HashMap<String, GraphicProperties>();
		
		for(MetaClass mc : mm.getClasses()) 
			for(Reference r : mc.getReferences())
				gpIndex.put(mc.getName() + "." + r.getName(), r.getGraphicProperties());
		
		EPackage2GraphicRepresentation transformer2 = new EPackage2GraphicRepresentation((EPackage)emfAction.getEcore().getResource().getContents().get(0), emfAction.getExtension(), legendFolder, gpIndex);
		GraphicRepresentation graphicRepresentation = transformer2.execute();
					
		this.createSiriusProject(session.getMetamodelRoot().getName().replace(".mbupf", "") + ".editor", graphicRepresentation, new FragmentFolder(session.getProject()), new File(session.getProject().getLocation().toOSString()+"/"+session.getProject().getName()+".mm.ecore"), emfAction.getExtension());		
	}
			
	private void createSiriusProject(String projectName, GraphicRepresentation graphicRepresentation, FragmentFolder fragmentFolder, File ecore, String extension) {
		// TODO Auto-generated method stub
		//Create ViewPoint Specification Project				
		if(graphicRepresentation == null) return;
		IProject current_pro = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);

		if(current_pro.exists()) return;
		
		ProgressMonitorDialog monitorDialog = new ProgressMonitorDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		IPath projectLocationPath = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		String modelInitialObjectName = ViewpointSpecificationProject.INITIAL_OBJECT_NAME;
		
		try {
			//Create ViewPoint Specification Project
			current_pro = ViewpointSpecificationProject.
					createNewViewpointSpecificationProject(PlatformUI.getWorkbench(), projectName,
							projectLocationPath , "WT.odesign",
								 modelInitialObjectName, "UTF-8", monitorDialog);
			//END				
		
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		try {
			IFile file = current_pro.getFile("temp.xmi");
			file.create(null, true, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ResourceSet resSet = new ResourceSetImpl();		
		String fileRelativeLocation = "/" + current_pro.getName() + "/temp.xmi";
		Resource resource = resSet.createResource(URI.createPlatformResourceURI(fileRelativeLocation, true));
		resource.getContents().add(graphicRepresentation);
		
		try { resource.save(Collections.EMPTY_MAP); } 
		catch (IOException e) { e.printStackTrace(); }
			
		GraphicToViewPoint runner;
		
		try {
			runner = new GraphicToViewPoint();						
			IFile ifile = current_pro.getFile("temp.xmi");
			runner.loadModels("file:/" + ifile.getLocation().toOSString());			
			runner.doGraphicToViewPoint(new NullProgressMonitor());
			File odFile = new File(current_pro.getFolder("description").getFile("WT1.odesign").getLocation().toOSString());
			runner.saveModels("file:/" + odFile.getAbsolutePath());			
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("IOexception");
		} catch (ATLCoreException e) {
			// TODO Auto-generated catch block
			System.out.println("ATLCoreException");
		} 
		
		IFile ifile = current_pro.getFile("temp.xmi");
		IFolder folder = current_pro.getFolder("description");
		IFile wtFile = folder.getFile("WT.odesign");
		
		try {
			//ifile.delete(true, null);
			wtFile.delete(true, null);
			folder.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			System.out.println("CoreException");
		}
		
		// add modeling nature to project
		try {
			IProjectUtils.addNatureToProject(current_pro, ModelingProject.NATURE_ID);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			ModelingProjectManager.INSTANCE.convertToModelingProject(current_pro, null);
			current_pro.refreshLocal(IResource.DEPTH_INFINITE, null);
			ModelingProjectManager.INSTANCE.createLocalRepresentationsFile(current_pro, null);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
		}		
		
		// try to create the corresponding fragments from original project, in Sirius project, stored in xmi
		
		IFolder modelFolder = current_pro.getFolder("models");
		try {
			modelFolder.create(true, true, null);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				
		if(fragmentFolder != null && fragmentFolder.exists()){
			try {
				fragmentFolder.exportFragmentFolder(modelFolder, ecore, mm, extension);
				modelFolder.refreshLocal(IResource.DEPTH_INFINITE, null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	
	/*public void GraphicRepresentation2Sirius(URI anfileURI) {
		// TODO Auto-generated method stub
		//Create *.aird if it doesn't exist
		IProgressMonitor monitor = new NullProgressMonitor();
		URI sessionResourceURI = URI.createURI(anfileURI.toString().substring(0,
										anfileURI.toString().length()-anfileURI.fileExtension().length()).concat(aird),true);
		
		//Create Session Operation
		SessionCreationOperation oper = new DefaultLocalSessionCreationOperation(sessionResourceURI, monitor);
		try {
			//Create Session
			oper.execute();
			Session createdSession = oper.getCreatedSession();

			//Add Default Representation
				//Adding the resource also to Sirius session
				AddSemanticResourceCommand addCommandToSession = new AddSemanticResourceCommand(createdSession,anfileURI,monitor);
				createdSession.getTransactionalEditingDomain().getCommandStack().execute(addCommandToSession);
				createdSession.save(monitor); 	
				//END
			//Add View
				TransactionalEditingDomain domain = createdSession.getTransactionalEditingDomain();
				final Set<Viewpoint> newSelectedViewpoints = ViewpointSelection.getViewpoints(anfileURI.fileExtension());
				Set<Viewpoint> viewpoints = new HashSet<Viewpoint>();
				final ViewpointSelection.Callback callback = new ViewpointSelectionCallbackWithConfimation();
				
				String name = null;					
				for(Viewpoint p : newSelectedViewpoints){
					viewpoints.add(SiriusResourceHelper.getCorrespondingViewpoint(createdSession, p));
					name = p.getName();
				}
				@SuppressWarnings("restriction")
				Command command = new ChangeViewpointSelectionCommand(createdSession, 
						callback, viewpoints , new HashSet<Viewpoint>(), true,
						monitor);
				domain.getCommandStack().execute(command);
				createdSession.save(monitor); 
				//END		
				
			//Create Representation
				EObject rootObject = createdSession.getSemanticResources().iterator().next().getContents().get(0);
				Collection<RepresentationDescription> descriptions = DialectManager.INSTANCE.getAvailableRepresentationDescriptions(createdSession.getSelectedViewpoints(false),  rootObject );
				if(descriptions.isEmpty())
					throw new Exception("Could not found representation description for object: " + rootObject);
					
				RepresentationDescription description = descriptions.iterator().next();
				
				//DialectManager viewpointDialectManager = DialectManager.INSTANCE;
				Command createViewCommand = new CreateRepresentationCommand(createdSession,
						  description, rootObject, "Default "+ name + " Diagram", monitor);
				
				createdSession.getTransactionalEditingDomain().getCommandStack().execute(createViewCommand);

				SessionManager.INSTANCE.notifyRepresentationCreated(createdSession);
			
			//Save session and Refresh workspace		
			createdSession.save(monitor);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			

		System.out.println("Create *.aird and Representation Command");
	}*/
}
