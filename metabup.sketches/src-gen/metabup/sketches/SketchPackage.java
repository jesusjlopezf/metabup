/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.sketches;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see metabup.sketches.SketchFactory
 * @model kind="package"
 * @generated
 */
public interface SketchPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "sketch";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://metabup/sketch_concept";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sketch_concept";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SketchPackage eINSTANCE = metabup.sketches.impl.SketchPackageImpl.init();

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.SketchElementsContainerImpl <em>Elements Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.SketchElementsContainerImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getSketchElementsContainer()
	 * @generated
	 */
	int SKETCH_ELEMENTS_CONTAINER = 0;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENTS_CONTAINER__ELEMENTS = 0;

	/**
	 * The number of structural features of the '<em>Elements Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENTS_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Elements Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENTS_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.SketchImpl <em>Sketch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.SketchImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getSketch()
	 * @generated
	 */
	int SKETCH = 1;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH__ELEMENTS = SKETCH_ELEMENTS_CONTAINER__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH__NAME = SKETCH_ELEMENTS_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sketch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_FEATURE_COUNT = SKETCH_ELEMENTS_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Sketch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_OPERATION_COUNT = SKETCH_ELEMENTS_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.SketchElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.SketchElementImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getSketchElement()
	 * @generated
	 */
	int SKETCH_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Debug Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__DEBUG_NAME = 0;

	/**
	 * The feature id for the '<em><b>Tool Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__TOOL_SYMBOL_NAME = 1;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__TEXT = 2;

	/**
	 * The feature id for the '<em><b>Skech Type Info</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__SKECH_TYPE_INFO = 3;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__X = 4;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__Y = 5;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__WIDTH = 6;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__HEIGHT = 7;

	/**
	 * The feature id for the '<em><b>Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__CONTAINER = 8;

	/**
	 * The feature id for the '<em><b>Implicitly Contained Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS = 9;

	/**
	 * The feature id for the '<em><b>Src Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__SRC_OF = 10;

	/**
	 * The feature id for the '<em><b>Tgt Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__TGT_OF = 11;

	/**
	 * The feature id for the '<em><b>Labels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__LABELS = 12;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__COLOR = 13;

	/**
	 * The feature id for the '<em><b>Transparent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT__TRANSPARENT = 14;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT_FEATURE_COUNT = 15;

	/**
	 * The number of operations of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SKETCH_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.LabelImpl <em>Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.LabelImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getLabel()
	 * @generated
	 */
	int LABEL = 3;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__ANNOTATIONS = 0;

	/**
	 * The number of structural features of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.PlainLabelImpl <em>Plain Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.PlainLabelImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getPlainLabel()
	 * @generated
	 */
	int PLAIN_LABEL = 4;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAIN_LABEL__ANNOTATIONS = LABEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAIN_LABEL__VALUE = LABEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Plain Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAIN_LABEL_FEATURE_COUNT = LABEL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Plain Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAIN_LABEL_OPERATION_COUNT = LABEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.KeyValueLabelImpl <em>Key Value Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.KeyValueLabelImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getKeyValueLabel()
	 * @generated
	 */
	int KEY_VALUE_LABEL = 5;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_VALUE_LABEL__ANNOTATIONS = LABEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_VALUE_LABEL__KEY = LABEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_VALUE_LABEL__VALUE = LABEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Key Value Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_VALUE_LABEL_FEATURE_COUNT = LABEL_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Key Value Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_VALUE_LABEL_OPERATION_COUNT = LABEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.ContainerImpl <em>Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.ContainerImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getContainer()
	 * @generated
	 */
	int CONTAINER = 6;

	/**
	 * The feature id for the '<em><b>Debug Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__DEBUG_NAME = SKETCH_ELEMENT__DEBUG_NAME;

	/**
	 * The feature id for the '<em><b>Tool Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__TOOL_SYMBOL_NAME = SKETCH_ELEMENT__TOOL_SYMBOL_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__TEXT = SKETCH_ELEMENT__TEXT;

	/**
	 * The feature id for the '<em><b>Skech Type Info</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__SKECH_TYPE_INFO = SKETCH_ELEMENT__SKECH_TYPE_INFO;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__X = SKETCH_ELEMENT__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__Y = SKETCH_ELEMENT__Y;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__WIDTH = SKETCH_ELEMENT__WIDTH;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__HEIGHT = SKETCH_ELEMENT__HEIGHT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__CONTAINER = SKETCH_ELEMENT__CONTAINER;

	/**
	 * The feature id for the '<em><b>Implicitly Contained Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__IMPLICITLY_CONTAINED_ELEMENTS = SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Src Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__SRC_OF = SKETCH_ELEMENT__SRC_OF;

	/**
	 * The feature id for the '<em><b>Tgt Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__TGT_OF = SKETCH_ELEMENT__TGT_OF;

	/**
	 * The feature id for the '<em><b>Labels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__LABELS = SKETCH_ELEMENT__LABELS;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__COLOR = SKETCH_ELEMENT__COLOR;

	/**
	 * The feature id for the '<em><b>Transparent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__TRANSPARENT = SKETCH_ELEMENT__TRANSPARENT;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__ELEMENTS = SKETCH_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_FEATURE_COUNT = SKETCH_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_OPERATION_COUNT = SKETCH_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.SingleElementImpl <em>Single Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.SingleElementImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getSingleElement()
	 * @generated
	 */
	int SINGLE_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Debug Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__DEBUG_NAME = SKETCH_ELEMENT__DEBUG_NAME;

	/**
	 * The feature id for the '<em><b>Tool Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__TOOL_SYMBOL_NAME = SKETCH_ELEMENT__TOOL_SYMBOL_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__TEXT = SKETCH_ELEMENT__TEXT;

	/**
	 * The feature id for the '<em><b>Skech Type Info</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__SKECH_TYPE_INFO = SKETCH_ELEMENT__SKECH_TYPE_INFO;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__X = SKETCH_ELEMENT__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__Y = SKETCH_ELEMENT__Y;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__WIDTH = SKETCH_ELEMENT__WIDTH;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__HEIGHT = SKETCH_ELEMENT__HEIGHT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__CONTAINER = SKETCH_ELEMENT__CONTAINER;

	/**
	 * The feature id for the '<em><b>Implicitly Contained Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS = SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Src Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__SRC_OF = SKETCH_ELEMENT__SRC_OF;

	/**
	 * The feature id for the '<em><b>Tgt Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__TGT_OF = SKETCH_ELEMENT__TGT_OF;

	/**
	 * The feature id for the '<em><b>Labels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__LABELS = SKETCH_ELEMENT__LABELS;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__COLOR = SKETCH_ELEMENT__COLOR;

	/**
	 * The feature id for the '<em><b>Transparent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT__TRANSPARENT = SKETCH_ELEMENT__TRANSPARENT;

	/**
	 * The number of structural features of the '<em>Single Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT_FEATURE_COUNT = SKETCH_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Single Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_ELEMENT_OPERATION_COUNT = SKETCH_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.ConnectionImpl <em>Connection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.ConnectionImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getConnection()
	 * @generated
	 */
	int CONNECTION = 8;

	/**
	 * The feature id for the '<em><b>Debug Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__DEBUG_NAME = SKETCH_ELEMENT__DEBUG_NAME;

	/**
	 * The feature id for the '<em><b>Tool Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__TOOL_SYMBOL_NAME = SKETCH_ELEMENT__TOOL_SYMBOL_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__TEXT = SKETCH_ELEMENT__TEXT;

	/**
	 * The feature id for the '<em><b>Skech Type Info</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__SKECH_TYPE_INFO = SKETCH_ELEMENT__SKECH_TYPE_INFO;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__X = SKETCH_ELEMENT__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__Y = SKETCH_ELEMENT__Y;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__WIDTH = SKETCH_ELEMENT__WIDTH;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__HEIGHT = SKETCH_ELEMENT__HEIGHT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__CONTAINER = SKETCH_ELEMENT__CONTAINER;

	/**
	 * The feature id for the '<em><b>Implicitly Contained Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__IMPLICITLY_CONTAINED_ELEMENTS = SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Src Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__SRC_OF = SKETCH_ELEMENT__SRC_OF;

	/**
	 * The feature id for the '<em><b>Tgt Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__TGT_OF = SKETCH_ELEMENT__TGT_OF;

	/**
	 * The feature id for the '<em><b>Labels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__LABELS = SKETCH_ELEMENT__LABELS;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__COLOR = SKETCH_ELEMENT__COLOR;

	/**
	 * The feature id for the '<em><b>Transparent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__TRANSPARENT = SKETCH_ELEMENT__TRANSPARENT;

	/**
	 * The feature id for the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__SRC = SKETCH_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tgt</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION__TGT = SKETCH_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_FEATURE_COUNT = SKETCH_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Connection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTION_OPERATION_COUNT = SKETCH_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.LineImpl <em>Line</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.LineImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getLine()
	 * @generated
	 */
	int LINE = 9;

	/**
	 * The feature id for the '<em><b>Debug Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__DEBUG_NAME = CONNECTION__DEBUG_NAME;

	/**
	 * The feature id for the '<em><b>Tool Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__TOOL_SYMBOL_NAME = CONNECTION__TOOL_SYMBOL_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__TEXT = CONNECTION__TEXT;

	/**
	 * The feature id for the '<em><b>Skech Type Info</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__SKECH_TYPE_INFO = CONNECTION__SKECH_TYPE_INFO;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__X = CONNECTION__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__Y = CONNECTION__Y;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__WIDTH = CONNECTION__WIDTH;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__HEIGHT = CONNECTION__HEIGHT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__CONTAINER = CONNECTION__CONTAINER;

	/**
	 * The feature id for the '<em><b>Implicitly Contained Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__IMPLICITLY_CONTAINED_ELEMENTS = CONNECTION__IMPLICITLY_CONTAINED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Src Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__SRC_OF = CONNECTION__SRC_OF;

	/**
	 * The feature id for the '<em><b>Tgt Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__TGT_OF = CONNECTION__TGT_OF;

	/**
	 * The feature id for the '<em><b>Labels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__LABELS = CONNECTION__LABELS;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__COLOR = CONNECTION__COLOR;

	/**
	 * The feature id for the '<em><b>Transparent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__TRANSPARENT = CONNECTION__TRANSPARENT;

	/**
	 * The feature id for the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__SRC = CONNECTION__SRC;

	/**
	 * The feature id for the '<em><b>Tgt</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE__TGT = CONNECTION__TGT;

	/**
	 * The number of structural features of the '<em>Line</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE_FEATURE_COUNT = CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Line</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINE_OPERATION_COUNT = CONNECTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metabup.sketches.impl.ArrowImpl <em>Arrow</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metabup.sketches.impl.ArrowImpl
	 * @see metabup.sketches.impl.SketchPackageImpl#getArrow()
	 * @generated
	 */
	int ARROW = 10;

	/**
	 * The feature id for the '<em><b>Debug Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__DEBUG_NAME = CONNECTION__DEBUG_NAME;

	/**
	 * The feature id for the '<em><b>Tool Symbol Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__TOOL_SYMBOL_NAME = CONNECTION__TOOL_SYMBOL_NAME;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__TEXT = CONNECTION__TEXT;

	/**
	 * The feature id for the '<em><b>Skech Type Info</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__SKECH_TYPE_INFO = CONNECTION__SKECH_TYPE_INFO;

	/**
	 * The feature id for the '<em><b>X</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__X = CONNECTION__X;

	/**
	 * The feature id for the '<em><b>Y</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__Y = CONNECTION__Y;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__WIDTH = CONNECTION__WIDTH;

	/**
	 * The feature id for the '<em><b>Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__HEIGHT = CONNECTION__HEIGHT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__CONTAINER = CONNECTION__CONTAINER;

	/**
	 * The feature id for the '<em><b>Implicitly Contained Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__IMPLICITLY_CONTAINED_ELEMENTS = CONNECTION__IMPLICITLY_CONTAINED_ELEMENTS;

	/**
	 * The feature id for the '<em><b>Src Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__SRC_OF = CONNECTION__SRC_OF;

	/**
	 * The feature id for the '<em><b>Tgt Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__TGT_OF = CONNECTION__TGT_OF;

	/**
	 * The feature id for the '<em><b>Labels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__LABELS = CONNECTION__LABELS;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__COLOR = CONNECTION__COLOR;

	/**
	 * The feature id for the '<em><b>Transparent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__TRANSPARENT = CONNECTION__TRANSPARENT;

	/**
	 * The feature id for the '<em><b>Src</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__SRC = CONNECTION__SRC;

	/**
	 * The feature id for the '<em><b>Tgt</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW__TGT = CONNECTION__TGT;

	/**
	 * The number of structural features of the '<em>Arrow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_FEATURE_COUNT = CONNECTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Arrow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARROW_OPERATION_COUNT = CONNECTION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link metabup.sketches.SketchElementsContainer <em>Elements Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Elements Container</em>'.
	 * @see metabup.sketches.SketchElementsContainer
	 * @generated
	 */
	EClass getSketchElementsContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link metabup.sketches.SketchElementsContainer#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see metabup.sketches.SketchElementsContainer#getElements()
	 * @see #getSketchElementsContainer()
	 * @generated
	 */
	EReference getSketchElementsContainer_Elements();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.Sketch <em>Sketch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sketch</em>'.
	 * @see metabup.sketches.Sketch
	 * @generated
	 */
	EClass getSketch();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.Sketch#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metabup.sketches.Sketch#getName()
	 * @see #getSketch()
	 * @generated
	 */
	EAttribute getSketch_Name();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.SketchElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see metabup.sketches.SketchElement
	 * @generated
	 */
	EClass getSketchElement();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#getDebugName <em>Debug Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Debug Name</em>'.
	 * @see metabup.sketches.SketchElement#getDebugName()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_DebugName();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#getToolSymbolName <em>Tool Symbol Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tool Symbol Name</em>'.
	 * @see metabup.sketches.SketchElement#getToolSymbolName()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_ToolSymbolName();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see metabup.sketches.SketchElement#getText()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_Text();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#getSkechTypeInfo <em>Skech Type Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Skech Type Info</em>'.
	 * @see metabup.sketches.SketchElement#getSkechTypeInfo()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_SkechTypeInfo();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X</em>'.
	 * @see metabup.sketches.SketchElement#getX()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_X();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Y</em>'.
	 * @see metabup.sketches.SketchElement#getY()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_Y();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see metabup.sketches.SketchElement#getWidth()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_Width();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#getHeight <em>Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Height</em>'.
	 * @see metabup.sketches.SketchElement#getHeight()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_Height();

	/**
	 * Returns the meta object for the reference '{@link metabup.sketches.SketchElement#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Container</em>'.
	 * @see metabup.sketches.SketchElement#getContainer()
	 * @see #getSketchElement()
	 * @generated
	 */
	EReference getSketchElement_Container();

	/**
	 * Returns the meta object for the reference list '{@link metabup.sketches.SketchElement#getImplicitlyContainedElements <em>Implicitly Contained Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Implicitly Contained Elements</em>'.
	 * @see metabup.sketches.SketchElement#getImplicitlyContainedElements()
	 * @see #getSketchElement()
	 * @generated
	 */
	EReference getSketchElement_ImplicitlyContainedElements();

	/**
	 * Returns the meta object for the reference list '{@link metabup.sketches.SketchElement#getSrcOf <em>Src Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Src Of</em>'.
	 * @see metabup.sketches.SketchElement#getSrcOf()
	 * @see #getSketchElement()
	 * @generated
	 */
	EReference getSketchElement_SrcOf();

	/**
	 * Returns the meta object for the reference list '{@link metabup.sketches.SketchElement#getTgtOf <em>Tgt Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Tgt Of</em>'.
	 * @see metabup.sketches.SketchElement#getTgtOf()
	 * @see #getSketchElement()
	 * @generated
	 */
	EReference getSketchElement_TgtOf();

	/**
	 * Returns the meta object for the containment reference list '{@link metabup.sketches.SketchElement#getLabels <em>Labels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Labels</em>'.
	 * @see metabup.sketches.SketchElement#getLabels()
	 * @see #getSketchElement()
	 * @generated
	 */
	EReference getSketchElement_Labels();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see metabup.sketches.SketchElement#getColor()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_Color();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.SketchElement#isTransparent <em>Transparent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transparent</em>'.
	 * @see metabup.sketches.SketchElement#isTransparent()
	 * @see #getSketchElement()
	 * @generated
	 */
	EAttribute getSketchElement_Transparent();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.Label <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Label</em>'.
	 * @see metabup.sketches.Label
	 * @generated
	 */
	EClass getLabel();

	/**
	 * Returns the meta object for the attribute list '{@link metabup.sketches.Label#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Annotations</em>'.
	 * @see metabup.sketches.Label#getAnnotations()
	 * @see #getLabel()
	 * @generated
	 */
	EAttribute getLabel_Annotations();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.PlainLabel <em>Plain Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plain Label</em>'.
	 * @see metabup.sketches.PlainLabel
	 * @generated
	 */
	EClass getPlainLabel();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.PlainLabel#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see metabup.sketches.PlainLabel#getValue()
	 * @see #getPlainLabel()
	 * @generated
	 */
	EAttribute getPlainLabel_Value();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.KeyValueLabel <em>Key Value Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Key Value Label</em>'.
	 * @see metabup.sketches.KeyValueLabel
	 * @generated
	 */
	EClass getKeyValueLabel();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.KeyValueLabel#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see metabup.sketches.KeyValueLabel#getKey()
	 * @see #getKeyValueLabel()
	 * @generated
	 */
	EAttribute getKeyValueLabel_Key();

	/**
	 * Returns the meta object for the attribute '{@link metabup.sketches.KeyValueLabel#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see metabup.sketches.KeyValueLabel#getValue()
	 * @see #getKeyValueLabel()
	 * @generated
	 */
	EAttribute getKeyValueLabel_Value();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.Container <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container</em>'.
	 * @see metabup.sketches.Container
	 * @generated
	 */
	EClass getContainer();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.SingleElement <em>Single Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Element</em>'.
	 * @see metabup.sketches.SingleElement
	 * @generated
	 */
	EClass getSingleElement();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.Connection <em>Connection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connection</em>'.
	 * @see metabup.sketches.Connection
	 * @generated
	 */
	EClass getConnection();

	/**
	 * Returns the meta object for the reference '{@link metabup.sketches.Connection#getSrc <em>Src</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Src</em>'.
	 * @see metabup.sketches.Connection#getSrc()
	 * @see #getConnection()
	 * @generated
	 */
	EReference getConnection_Src();

	/**
	 * Returns the meta object for the reference '{@link metabup.sketches.Connection#getTgt <em>Tgt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tgt</em>'.
	 * @see metabup.sketches.Connection#getTgt()
	 * @see #getConnection()
	 * @generated
	 */
	EReference getConnection_Tgt();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.Line <em>Line</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Line</em>'.
	 * @see metabup.sketches.Line
	 * @generated
	 */
	EClass getLine();

	/**
	 * Returns the meta object for class '{@link metabup.sketches.Arrow <em>Arrow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Arrow</em>'.
	 * @see metabup.sketches.Arrow
	 * @generated
	 */
	EClass getArrow();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SketchFactory getSketchFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.SketchElementsContainerImpl <em>Elements Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.SketchElementsContainerImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getSketchElementsContainer()
		 * @generated
		 */
		EClass SKETCH_ELEMENTS_CONTAINER = eINSTANCE.getSketchElementsContainer();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKETCH_ELEMENTS_CONTAINER__ELEMENTS = eINSTANCE.getSketchElementsContainer_Elements();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.SketchImpl <em>Sketch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.SketchImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getSketch()
		 * @generated
		 */
		EClass SKETCH = eINSTANCE.getSketch();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH__NAME = eINSTANCE.getSketch_Name();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.SketchElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.SketchElementImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getSketchElement()
		 * @generated
		 */
		EClass SKETCH_ELEMENT = eINSTANCE.getSketchElement();

		/**
		 * The meta object literal for the '<em><b>Debug Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__DEBUG_NAME = eINSTANCE.getSketchElement_DebugName();

		/**
		 * The meta object literal for the '<em><b>Tool Symbol Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__TOOL_SYMBOL_NAME = eINSTANCE.getSketchElement_ToolSymbolName();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__TEXT = eINSTANCE.getSketchElement_Text();

		/**
		 * The meta object literal for the '<em><b>Skech Type Info</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__SKECH_TYPE_INFO = eINSTANCE.getSketchElement_SkechTypeInfo();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__X = eINSTANCE.getSketchElement_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__Y = eINSTANCE.getSketchElement_Y();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__WIDTH = eINSTANCE.getSketchElement_Width();

		/**
		 * The meta object literal for the '<em><b>Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__HEIGHT = eINSTANCE.getSketchElement_Height();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKETCH_ELEMENT__CONTAINER = eINSTANCE.getSketchElement_Container();

		/**
		 * The meta object literal for the '<em><b>Implicitly Contained Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKETCH_ELEMENT__IMPLICITLY_CONTAINED_ELEMENTS = eINSTANCE.getSketchElement_ImplicitlyContainedElements();

		/**
		 * The meta object literal for the '<em><b>Src Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKETCH_ELEMENT__SRC_OF = eINSTANCE.getSketchElement_SrcOf();

		/**
		 * The meta object literal for the '<em><b>Tgt Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKETCH_ELEMENT__TGT_OF = eINSTANCE.getSketchElement_TgtOf();

		/**
		 * The meta object literal for the '<em><b>Labels</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SKETCH_ELEMENT__LABELS = eINSTANCE.getSketchElement_Labels();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__COLOR = eINSTANCE.getSketchElement_Color();

		/**
		 * The meta object literal for the '<em><b>Transparent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SKETCH_ELEMENT__TRANSPARENT = eINSTANCE.getSketchElement_Transparent();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.LabelImpl <em>Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.LabelImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getLabel()
		 * @generated
		 */
		EClass LABEL = eINSTANCE.getLabel();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LABEL__ANNOTATIONS = eINSTANCE.getLabel_Annotations();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.PlainLabelImpl <em>Plain Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.PlainLabelImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getPlainLabel()
		 * @generated
		 */
		EClass PLAIN_LABEL = eINSTANCE.getPlainLabel();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAIN_LABEL__VALUE = eINSTANCE.getPlainLabel_Value();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.KeyValueLabelImpl <em>Key Value Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.KeyValueLabelImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getKeyValueLabel()
		 * @generated
		 */
		EClass KEY_VALUE_LABEL = eINSTANCE.getKeyValueLabel();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_VALUE_LABEL__KEY = eINSTANCE.getKeyValueLabel_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_VALUE_LABEL__VALUE = eINSTANCE.getKeyValueLabel_Value();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.ContainerImpl <em>Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.ContainerImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getContainer()
		 * @generated
		 */
		EClass CONTAINER = eINSTANCE.getContainer();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.SingleElementImpl <em>Single Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.SingleElementImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getSingleElement()
		 * @generated
		 */
		EClass SINGLE_ELEMENT = eINSTANCE.getSingleElement();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.ConnectionImpl <em>Connection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.ConnectionImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getConnection()
		 * @generated
		 */
		EClass CONNECTION = eINSTANCE.getConnection();

		/**
		 * The meta object literal for the '<em><b>Src</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION__SRC = eINSTANCE.getConnection_Src();

		/**
		 * The meta object literal for the '<em><b>Tgt</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTION__TGT = eINSTANCE.getConnection_Tgt();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.LineImpl <em>Line</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.LineImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getLine()
		 * @generated
		 */
		EClass LINE = eINSTANCE.getLine();

		/**
		 * The meta object literal for the '{@link metabup.sketches.impl.ArrowImpl <em>Arrow</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metabup.sketches.impl.ArrowImpl
		 * @see metabup.sketches.impl.SketchPackageImpl#getArrow()
		 * @generated
		 */
		EClass ARROW = eINSTANCE.getArrow();

	}

} //SketchPackage
