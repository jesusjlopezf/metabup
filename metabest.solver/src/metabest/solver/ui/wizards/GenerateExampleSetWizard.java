/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

import com.jesusjlopezf.utils.eclipse.xtext.XtextUtils;

import metabest.solver.ConstraintSolver;
import metabest.solver.CoverageCriteriaConstraintGenerator;
import metabest.solver.ui.wizards.pages.MetaModelElementSelectionWizardPage;
import metabest.solver.ui.wizards.pages.TestcasePackageSelectionPage;
import metabest.test.fragments.MBFRuntimeModule;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import test.ExtensionAssertion;
import test.ExtensionAssertionSet;
import test.TestFactory;
import test.TestableFragment;


public class GenerateExampleSetWizard extends Wizard implements IImportWizard {
	MetaModelElementSelectionWizardPage elementSelectionPage;
	TestcasePackageSelectionPage containerSelectionPage;
	
	List<AnnotatedElement> elements = new ArrayList<AnnotatedElement>();
	String container = null;
	
	MetaModel metamodel = null;

	public MetaModel getDatamodel() {
		return metamodel;
	}

	public void setDatamodel(MetaModel metamodel) {
		this.metamodel = metamodel;
	}

	public GenerateExampleSetWizard(IProject project, MetaModel metamodel) {
		super();
		this.metamodel = metamodel;

		needsPreviousAndNextButtons();
		this.init(null, null);
	}

	public boolean performFinish() {
		container = containerSelectionPage.getContainerName();
		elements = elementSelectionPage.getSelectedElements();
		if(container == null || elements == null || elements.isEmpty()) return false;
		
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IResource resource = root.findMember(new Path(container));
		final IContainer container;
		if(resource instanceof IContainer) container = (IContainer)resource;
		else return false;
			
		CoverageCriteriaConstraintGenerator generator = new CoverageCriteriaConstraintGenerator(metamodel, elements, CoverageCriteriaConstraintGenerator.SIMPLE_COVERAGE_CRITERIA);
		final List<ExtensionAssertionSet> list = generator.getAssertionSetList();
		
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doFinish(monitor, list, container);									
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		
		return true;
	}
	
	public void doFinish(IProgressMonitor monitor, List<ExtensionAssertionSet> list, IContainer container){
		monitor.beginTask("Generating models...", list.size());
		
		for(ExtensionAssertionSet set : list){
			TestableFragment fragment = TestFactory.eINSTANCE.createTestableFragment();
			fragment.setAssertionSet(set);
			
			try {
				String name = null;
				for(ExtensionAssertion a : set.getAssertions()) name += XtextUtils.serialize(new MBFRuntimeModule(), a);
				name = name.replaceAll("[^A-Za-z0-9 ]", "");
				name = name.replaceAll("null", "");
				name = name.replaceAll(" ", "");
				ConstraintSolver solver = new ConstraintSolver(container.getProject(), (IFolder)container, metamodel, fragment);
				solver.setTimeOut(20);
				solver.solve(true, name, false);
				monitor.worked(1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("Automatic test model generation"); //NON-NLS-1
		setNeedsProgressMonitor(true);
		elementSelectionPage = new MetaModelElementSelectionWizardPage("Select the metamodel elements to test", selection, metamodel);
		containerSelectionPage = new TestcasePackageSelectionPage(selection);
	}
	
    public void addPages() {
        super.addPages(); 
        addPage(elementSelectionPage);
        addPage(containerSelectionPage);
    }
   
    
	public MetaModelElementSelectionWizardPage getPage2datatags() {
		return elementSelectionPage;
	}

	public void setPage2datatags(
			MetaModelElementSelectionWizardPage page2datatags) {
		this.elementSelectionPage = page2datatags;
	}
}
