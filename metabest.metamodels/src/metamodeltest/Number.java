/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.Number#getN <em>N</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getNumber()
 * @model
 * @generated
 */
public interface Number extends Quantifier {
	/**
	 * Returns the value of the '<em><b>N</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>N</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>N</em>' containment reference.
	 * @see #setN(IntegerParameter)
	 * @see metamodeltest.MetamodeltestPackage#getNumber_N()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IntegerParameter getN();

	/**
	 * Sets the value of the '{@link metamodeltest.Number#getN <em>N</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>N</em>' containment reference.
	 * @see #getN()
	 * @generated
	 */
	void setN(IntegerParameter value);

} // Number
