/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.issues.lib;

import org.eclipse.jface.dialogs.InputDialog;

import metabup.issues.general.categories.Automatic;
import metabup.metamodel.Reference;

public class TightenMinMultiplicity extends Automatic {
	public TightenMinMultiplicity(Reference r){
		if(r != null){
			super.elements.add(r);
			super.setDescription("Tightening reference " + r.getName() + " min multiplicity suggested");
		}	
	}
	
	@Override
	public void run() {
		Reference r = (Reference)elements.get(0);
		r.setMax(1);
	}

}
