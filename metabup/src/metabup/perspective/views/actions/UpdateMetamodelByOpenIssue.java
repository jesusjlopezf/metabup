/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.perspective.views.actions;

import metabup.ui.editor.popup.actions.MetaBupEditorAction;

import org.eclipse.jface.dialogs.MessageDialog;

public class UpdateMetamodelByOpenIssue extends MetaBupEditorAction {

	public void run(String openIssueDescription) {
		/*
		XtextEditor x = editor.getXtextEditor();
		TextSelection txt = (TextSelection) x.getSelectionProvider().getSelection();
		String fragmentName = txt.getText();
		*/
		
		if ( ! editor.getSession().updateMetamodelByOpenIssue(openIssueDescription) ) {
			MessageDialog.openError(null, "Error", "when performing the issue operation");
			editor.refreshViewer();
			// MessageDialog.openInformation(null, "Editor", "Sample Action Executed");			
		} else {
			editor.refreshViewer();
		}
		
	}

}