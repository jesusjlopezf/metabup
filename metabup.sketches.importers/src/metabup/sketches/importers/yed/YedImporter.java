/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches.importers.yed;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import metabup.sketches.Connection;
import metabup.sketches.Container;
import metabup.sketches.InvalidFormat;
import metabup.sketches.Label;
import metabup.sketches.SingleElement;
import metabup.sketches.Sketch;
import metabup.sketches.SketchElement;
import metabup.sketches.SketchFactory;
import metabup.sketches.XMLBasedImporter;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import graphicProperties.ArrowType;
import graphicProperties.EdgeProperties;
import graphicProperties.GraphicProperties;
import graphicProperties.GraphicPropertiesFactory;
import graphicProperties.LineType;

public class YedImporter extends XMLBasedImporter implements ErrorHandler {

	protected HashMap<String, String> resourcesHashes = new HashMap<String, String>();
	DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder;
    Document doc = null;
	
	public Sketch read(InputStream stream, String sketchName) {		                        
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			docBuilder.setErrorHandler(this);
	        doc = docBuilder.parse (stream);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
        

        Sketch sketch = SketchFactory.eINSTANCE.createSketch();
        sketch.setName(sketchName);

        // Read the embedded SVGs
        NodeList resourceData = doc.getElementsByTagName("y:Resource");
        
        for(int i = 0; i < resourceData.getLength(); i++) {
        	readResourceData(resourceData.item(i));
        }        
        
        // Read the graph
        Node graphNode = filterChildren1(doc.getFirstChild(), "graph");
        sketch = readGraphContents(graphNode, sketch, null);
        
        setConnections(sketch);
         
        return sketch;
	}

	private String readResourceData(Node node) {
		String id = node.getAttributes().getNamedItem("id").getNodeValue();
		String svg = node.getTextContent();
		this.resourcesHashes.put(id, createDigest(svg));
		return svg;
	}
	
	private String createDigest(String text) {
		try {
			byte[] result = MessageDigest.getInstance("MD5").digest(text.getBytes());
			return new BigInteger(result).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return Integer.toHexString(text.hashCode());
		}
	}

	private void setConnections(Sketch sketch) {
		Collection<ObjectKind> values = id2element.values();
		for (ObjectKind wrapper: values) {
			wrapper.setConnections();
		}
	}
	
	private Sketch readGraphContents(Node item, Sketch sketch, SketchElement parent) {
        for(int i = 0; i < item.getChildNodes().getLength(); i++) {
        	Node object = item.getChildNodes().item(i);
    		String text = null;
        	String toolSymbolName = null;
        	String sourceCode = null;
    		List<Node> labels = null;
    		boolean isContainer = false; 
    		float x=0, y=0, w=0, h=0;
    		String color = "#FFFFFF";
    		boolean transparent = true;
    		
    		EdgeProperties edgeProperties = null;
    		
        	if ( object.getNodeName().equals("node") || object.getNodeName().equals("edge") ) {
        		String type = null;
        		if ( object.getNodeName().equals("node") ) {
	        		isContainer = object.getAttributes().getNamedItem("yfiles.foldertype") != null;
        			
        			Node dataNode = filterChildrenWithAttribute1(object, "data", "key", "d6");
	        		if ( dataNode == null ) throw new InvalidFormat("Expected <data key='d6'>");
	        		
	        		if ( ! isContainer ) {
		        		Node typeNode = filterChildren1Opt(dataNode, "y:GenericNode");
		        		if ( typeNode != null ) {
		        			type = typeNode.getAttributes().getNamedItem("configuration").getNodeValue();
		        			toolSymbolName = type;
		        			x = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("x").getNodeValue());
		        			//System.out.println("x = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("x").getNodeValue());
		        			y = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("y").getNodeValue());
		        			//System.out.println("y = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("y").getNodeValue());
		        			w = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("width").getNodeValue());
		        			//System.out.println("w = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("width").getNodeValue());
		        			h = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("height").getNodeValue());
		        			//System.out.println("h = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("height").getNodeValue());
		        			color = filterChildren1(typeNode, "y:Fill").getAttributes().getNamedItem("color").getNodeValue();
		        			//System.out.println("color = " + color);
		        			transparent = Boolean.valueOf(filterChildren1(typeNode, "y:Fill").getAttributes().getNamedItem("transparent").getNodeValue());
		        			//System.out.println("transparent = " + transparent);
		        		} else if ( (typeNode = filterChildren1Opt(dataNode, "y:ShapeNode")) != null ) {
		        			type = filterChildren1(typeNode, "y:Shape").getAttributes().getNamedItem("type").getNodeValue();
		        			toolSymbolName = type;
		        			x = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("x").getNodeValue());
		        			//System.out.println("x = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("x").getNodeValue());
		        			y = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("y").getNodeValue());
		        			//System.out.println("y = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("y").getNodeValue());
		        			w = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("width").getNodeValue());
		        			//System.out.println("w = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("width").getNodeValue());
		        			h = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("height").getNodeValue());
		        			//System.out.println("h = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("height").getNodeValue());
		        			color = filterChildren1(typeNode, "y:Fill").getAttributes().getNamedItem("color").getNodeValue();
		        			//System.out.println("color = " + color);
		        			transparent = Boolean.valueOf(filterChildren1(typeNode, "y:Fill").getAttributes().getNamedItem("transparent").getNodeValue());
		        			//System.out.println("transparent = " + transparent);
		        			
		        		} else if ( (typeNode = filterChildren1Opt(dataNode, "y:SVGNode")) != null ) {
		        			Node svgModel = filterChildren1(typeNode, "y:SVGModel");
		        			String svgId  = filterChildren1(svgModel, "y:SVGContent").getAttributes().getNamedItem("refid").getNodeValue();
		        			type           = "yed-svg-node";
		        			toolSymbolName = lookupResource(svgId);
		        			
		        			NodeList resourceData = doc.getElementsByTagName("y:Resource");
		        			
		        			for(int j = 0; j < resourceData.getLength(); j++) {
		        				if(resourceData.item(j).getAttributes().getNamedItem("id").getNodeValue().equals(svgId)){
		        					sourceCode = readResourceData(resourceData.item(j));
		        					sourceCode.replaceAll("&lt;", "<");
		        					sourceCode.replaceAll("&gt;", ">");
		        				}
		        	        }  
		        			
		        			x = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("x").getNodeValue());
		        			//System.out.println("x = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("x").getNodeValue());
		        			y = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("y").getNodeValue());
		        			//System.out.println("y = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("y").getNodeValue());
		        			w = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("width").getNodeValue());
		        			//System.out.println("w = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("width").getNodeValue());
		        			h = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("height").getNodeValue());
		        			//System.out.println("h = " + filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("height").getNodeValue());
		        			color = filterChildren1(typeNode, "y:Fill").getAttributes().getNamedItem("color").getNodeValue();
		        			//System.out.println("color = " + color);
		        			transparent = Boolean.valueOf(filterChildren1(typeNode, "y:Fill").getAttributes().getNamedItem("transparent").getNodeValue());
		        			
		        		} else {
		        			typeNode = filterChildren1(dataNode, "y:ImageNode");
		        			Node imageModel = filterChildren1(typeNode, "y:Image");
		        			String refId  = imageModel.getAttributes().getNamedItem("refid").getNodeValue();
		        			type           = "yed-img-node";
		        			toolSymbolName = lookupResource(refId);		        			
		        		}

		        		text = extractText(typeNode);
		        		labels = extractNodeLabels(typeNode);
	        		} else {
	        			// The active attribute indicates which of the following group nodes is the
	        			// one that must be visualized (active is the "state" of the container, e.g. open vs. close).
	        			//   <y:Realizers active="0">
	        	        //      <y:GroupNode> ... </y:GroupNode> 
	        	        //      <y:GroupNode> ... </y:GroupNode>
	        			//   </y:Realizers> 
	        			Node realizersNode = getFirstTag(getFirstTag(dataNode));
	        			Node activeAttribute = realizersNode.getAttributes().getNamedItem("active");
	        			int  groupNodePosition = 0;
	        			if ( activeAttribute != null ) {
	        				groupNodePosition = Integer.parseInt(activeAttribute.getNodeValue());
	        			}
	        			
	        			Node typeNode = filterChildren(realizersNode, "y:GroupNode").get(groupNodePosition);
	        			type = "group";
	        			text = extractText(typeNode);
	        			labels = extractNodeLabels(typeNode);
	        			
	        			x = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("x").getNodeValue());
	        			y = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("y").getNodeValue());
	        			w = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("width").getNodeValue());
	        			h = Float.parseFloat(filterChildren1(typeNode, "y:Geometry").getAttributes().getNamedItem("height").getNodeValue());
	        			color = filterChildren1(typeNode, "y:Fill").getAttributes().getNamedItem("color").getNodeValue();
	        			transparent = Boolean.valueOf(filterChildren1(typeNode, "y:Fill").getAttributes().getNamedItem("transparent").getNodeValue());
	        			
	        			String containerTypeName = "yfiles.foldertype";
	        			toolSymbolName = extractGroupUniqueIdentifier(containerTypeName, typeNode);
	        		}

        		} else {
	        		Node dataNode = filterChildrenWithAttribute1(object, "data", "key", "d10");
	        		if ( dataNode == null ) throw new InvalidFormat("Expected <data key='d10'>");

	        		Node genericNode = filterChildren1Opt(dataNode, "y:GenericEdge");
	        		if ( genericNode == null ) {
	        			type = "yed-generic-polyline-edge";
	        			
	        			// TODO: Look at the graphical style to create directed connections
	        			//        or plain connections (now, everything is directed)
	        			//        This is another tip to decide whether to create a ref. or an association
	        			genericNode = filterChildren1Opt(dataNode, "y:PolyLineEdge");
	        			if ( genericNode != null ) {
	        				labels = filterChildren(genericNode, "y:EdgeLabel");
	        				text = extractText(genericNode);
	        				
	        				Node styleNode = filterChildren1Opt(genericNode, "y:LineStyle");
	        				
	        				if(styleNode != null){
	        					edgeProperties = GraphicPropertiesFactory.eINSTANCE.createEdgeProperties();
	        					
	        					String edgeColor = styleNode.getAttributes().getNamedItem("color").getNodeValue();
	        					edgeProperties.setColor(edgeColor);
	        					
	        					Float edgeWidth = Float.parseFloat(styleNode.getAttributes().getNamedItem("width").getNodeValue());
	        					edgeProperties.setWidth(Math.round(edgeWidth));
	        					
	        					String edgeType = styleNode.getAttributes().getNamedItem("type").getNodeValue();
	        					
	        					switch(edgeType){
	        						case "line" : edgeProperties.setLineType(LineType.LINE); break;
	        						case "dashed" : edgeProperties.setLineType(LineType.DASHED); break;
	        						case "dotted" : edgeProperties.setLineType(LineType.DOTTED); break;
	        						case "dashed_dotted" : edgeProperties.setLineType(LineType.DASHED_DOTTED); break;
	        						default : edgeProperties.setLineType(LineType.LINE); break;
	        					}
	        					
	        					
	        					Node arrowNode = filterChildren1Opt(genericNode, "y:Arrows");
	        					String srcArrow = arrowNode.getAttributes().getNamedItem("source").getNodeValue();
	        					String tarArrow = arrowNode.getAttributes().getNamedItem("target").getNodeValue();
	        					
	        					switch(srcArrow){
	        						case "circle" : edgeProperties.setSrcArrow(ArrowType.CIRCLE); break;
	        						case "concave" : edgeProperties.setSrcArrow(ArrowType.CONCAVE); break;
	        						case "convex" : edgeProperties.setSrcArrow(ArrowType.CONVEX); break;
	        						case "crows_foot_many" : edgeProperties.setSrcArrow(ArrowType.CROWS_FOOT_MANY); break;
	        						case "crows_foot_many_mandatory" : edgeProperties.setSrcArrow(ArrowType.CROWS_FOOT_MANY_MANDATORY); break;
	        						case "crows_foot_many_optional" : edgeProperties.setSrcArrow(ArrowType.CROWS_FOOT_MANY_OPTIONAL); break;
	        						case "crows_foot_one" : edgeProperties.setSrcArrow(ArrowType.CROWS_FOOT_ONE); break;
	        						case "crows_foot_one_mandatory" : edgeProperties.setSrcArrow(ArrowType.CROWS_FOOT_ONE_MANDATORY); break;
	        						case "crows_foot_one_optional" : edgeProperties.setSrcArrow(ArrowType.CROWS_FOOT_ONE_OPTIONAL); break;
	        						case "dash" : edgeProperties.setSrcArrow(ArrowType.DASH); break;
	        						case "delta" : edgeProperties.setSrcArrow(ArrowType.DELTA); break;
	        						case "diamond" : edgeProperties.setSrcArrow(ArrowType.DIAMOND); break;
	        						case "none" : edgeProperties.setSrcArrow(ArrowType.NONE); break;
	        						case "plain" : edgeProperties.setSrcArrow(ArrowType.PLAIN); break;
	        						case "short" : edgeProperties.setSrcArrow(ArrowType.SHORT); break;
	        						case "skewed_dash" : edgeProperties.setSrcArrow(ArrowType.SKEWED_DASH); break;
	        						case "standard" : edgeProperties.setSrcArrow(ArrowType.STANDARD); break;
	        						case "transparent_circle" : edgeProperties.setSrcArrow(ArrowType.TRANSPARENT_CIRCLE); break;
	        						case "t_shape" : edgeProperties.setSrcArrow(ArrowType.TSHAPE); break;
	        						case "white_delta" : edgeProperties.setSrcArrow(ArrowType.WHITE_DELTA); break;
	        						case "white_diamond" : edgeProperties.setSrcArrow(ArrowType.WHITE_DIAMOND); break;
	        						default : edgeProperties.setSrcArrow(ArrowType.NONE);
	        					}	
	        					
	        					switch(tarArrow){
	        						case "circle" : edgeProperties.setTgtArrow(ArrowType.CIRCLE); break;
	        						case "concave" : edgeProperties.setTgtArrow(ArrowType.CONCAVE); break;
	        						case "convex" : edgeProperties.setTgtArrow(ArrowType.CONVEX); break;
	        						case "crows_foot_many" : edgeProperties.setTgtArrow(ArrowType.CROWS_FOOT_MANY); break;
	        						case "crows_foot_many_mandatory" : edgeProperties.setTgtArrow(ArrowType.CROWS_FOOT_MANY_MANDATORY); break;
	        						case "crows_foot_many_optional" : edgeProperties.setTgtArrow(ArrowType.CROWS_FOOT_MANY_OPTIONAL); break;
	        						case "crows_foot_one" : edgeProperties.setTgtArrow(ArrowType.CROWS_FOOT_ONE); break;
	        						case "crows_foot_one_mandatory" : edgeProperties.setTgtArrow(ArrowType.CROWS_FOOT_ONE_MANDATORY); break;
	        						case "crows_foot_one_optional" : edgeProperties.setTgtArrow(ArrowType.CROWS_FOOT_ONE_OPTIONAL); break;
	        						case "dash" : edgeProperties.setTgtArrow(ArrowType.DASH); break;
	        						case "delta" : edgeProperties.setTgtArrow(ArrowType.DELTA); break;
	        						case "diamond" : edgeProperties.setTgtArrow(ArrowType.DIAMOND); break;
	        						case "none" : edgeProperties.setTgtArrow(ArrowType.NONE); break;
	        						case "plain" : edgeProperties.setTgtArrow(ArrowType.PLAIN); break;
	        						case "short" : edgeProperties.setTgtArrow(ArrowType.SHORT); break;
	        						case "skewed_dash" : edgeProperties.setTgtArrow(ArrowType.SKEWED_DASH); break;
	        						case "standard" : edgeProperties.setTgtArrow(ArrowType.STANDARD); break;
	        						case "transparent_circle" : edgeProperties.setTgtArrow(ArrowType.TRANSPARENT_CIRCLE); break;
	        						case "t_shape" : edgeProperties.setTgtArrow(ArrowType.TSHAPE); break;
	        						case "white_delta" : edgeProperties.setTgtArrow(ArrowType.WHITE_DELTA); break;
	        						case "white_diamond" : edgeProperties.setTgtArrow(ArrowType.WHITE_DIAMOND); break;
	        						default: edgeProperties.setTgtArrow(ArrowType.PLAIN);
	        					}
	        				}
	        				
	        				
	        				//if ( labels.size() > 0 ) {
	        				//	text = labels.get(0).getTextContent();
	        				//}
	        			}
	        			
	        		} else {
	        			type = genericNode.getAttributes().getNamedItem("configuration").getNodeValue();        			
		        		text = extractText(genericNode);
	        		}
	        		toolSymbolName = type;
	        		
	        		// TODO: Identify arrow directions (y:Arrows in PolyLineEdge)
        		}
        		
        		ObjectKind kind = decideType(object, type);
        		if ( kind == null ) continue;
        		
        		SketchElement element = kind.getElement();
        		if(edgeProperties != null) ((Connection)element).setGraphicProperties(edgeProperties);
        		// element.setSkechTypeInfo(extractRealType(type));
        		
        		if(type != null && toolSymbolName != null && kind != null){
        			kind.setSketchTypeInfo(type, toolSymbolName);
        		}
        		
        		if ( text != null && ! text.trim().isEmpty() ) {
        			element.setText(text);
            		element.setDebugName(text);
        		} else {
            		element.setDebugName(type);
        		}

        		if ( labels != null ) {
        			for (Node node : labels) {
						String labelText = node.getTextContent();
						List<metabup.sketches.Label> parsedLabels = createLabel(labelText);
						element.getLabels().addAll(parsedLabels);
					}
        		}
        		
        		if ( parent == null ) {
        			sketch.getElements().add(element);
        		} else {
        			((Container) parent).getElements().add(element);
        		}
        		
        		//geometry attributes
        		element.setX(x);        		
        		element.setY(y);
        		element.setWidth(w);
        		element.setHeight(h);
        		element.setSourceCode(sourceCode);
        		
        		//fill color attributes
        		element.setColor(color);
        		element.setTransparent(transparent);
        		
        		String id = object.getAttributes().getNamedItem("id").getNodeValue();
        		assert(id != null);
        		id2element.put(id, kind);
        		
    			// Read explicitly contained elements
        		if ( isContainer ) {
        			Node innerGraph = filterChildren1Opt(object, "graph");
        			if ( innerGraph != null )
        				sketch = readGraphContents(innerGraph, sketch, element);
        		}
        	}
        }
        
        return sketch;
	}



	/**
	 * Given a y:GroupNode generates a String that identifies a group 
	 * element using its style attributes:
	 * 
	 * <pre>
	 *        <y:Fill color="#CAECFF80" transparent="false"/>
     *        <y:BorderStyle color="#666699" type="dotted" width="1.0"/>
     *        <y:Shape type="roundrectangle"/>
	 * </pre>
	 * 
	 * This is needed because in yEd group elements are only identified because 
	 * there is an attribute called yfiles.foldertype (this string is passed as containerTypeName) 
	 * but we would like to use the same container with e.g., different colors.
	 * 
	 * @param containerTypeName 
	 * @param groupNode
	 * @return
	 */
	private String extractGroupUniqueIdentifier(String containerTypeName, Node groupNode) {
		String fillColor   = filterChildren1(groupNode, "y:Fill").getAttributes().getNamedItem("color").getNodeValue();
		String borderColor = filterChildren1(groupNode, "y:BorderStyle").getAttributes().getNamedItem("color").getNodeValue();
		String borderType  = filterChildren1(groupNode, "y:BorderStyle").getAttributes().getNamedItem("type").getNodeValue();
		String shapeType   = filterChildren1(groupNode, "y:Shape").getAttributes().getNamedItem("type").getNodeValue();
		return String.format("%s,fillColor=%s,borderColorColor=%s,borderType=%s,shapeType=%s", containerTypeName, fillColor, borderColor, borderType, shapeType);
	}

	private List<Node> extractNodeLabels(Node node) {
		return filterChildren(node, "y:NodeLabel");
	}

	private String lookupResource(String resourceId) {
		String resourceHash = resourcesHashes.get(resourceId);
		if ( resourceHash == null )
			throw new InvalidFormat("SVG resource with id " + resourceId + " could not be found.");
		return resourceHash;
	}
	
	private String extractText(Node node) {
		Node nodeLabel = filterChildren1Opt(node, "y:NodeLabel");
		if ( nodeLabel == null ) return null;
		
		return nodeLabel.getTextContent().trim();
	}
	
	class YedObject extends OBJECT {
		public YedObject(Node obj) {
			super(obj);
		}		
	}

	private static HashMap<String, String> bpmnTypes = new HashMap<String, String>();
	static {
		bpmnTypes.put("ARTIFACT_TYPE_DATA_OBJECT", "DataObject");
		bpmnTypes.put("ARTIFACT_TYPE_DATA_STORE", "DataObject");
		bpmnTypes.put("EVENT_TYPE_LINK", "DataObject");
		bpmnTypes.put("EVENT_TYPE_PARALLEL_MULTIPLE", "DataObject");
		bpmnTypes.put("ARTIFACT_TYPE_ANNOTATION", "Annotation");
		bpmnTypes.put("EVENT_TYPE_TIMER", "DataObject");
		bpmnTypes.put("EVENT_TYPE_LINK", "DataObject");
		bpmnTypes.put("EVENT_TYPE_PARALLEL_MULTIPLE", "DataObject");
		
	}	

	class YedBpmnObject extends OBJECT {
		public YedBpmnObject(Node obj) {
			super(obj);
		}				
	
		@Override
		public void setSketchTypeInfo(String type, String toolSymbolName) {
			super.setSketchTypeInfo(type, toolSymbolName);
			// node -> data -> genericNode -> find: tyleProperties
			Node bmpnStyle = filterChildren1(getFirstTag(node), "y:StyleProperties");
			Node typeOfNode = getFirstTag(bmpnStyle);
			String bpmnType = typeOfNode.getAttributes().getNamedItem("value").getTextContent();
			
			if ( bpmnTypes.containsKey(bpmnType) ) {
				element.setToolSymbolName(bpmnTypes.get(bpmnType));
			} else {
				throw new RuntimeException("Unsupported BPMN object type " + bpmnType);
			}

		}
		
	}

	class YedContainer extends CONTAINER {
		public YedContainer(Node obj) {
			super(obj);
		}		
	}

	class YedConnection extends CONNECTION {
		public YedConnection(Node obj) {
			super(obj);
		}
		
		@Override
		protected String getStartId() {
			return node.getAttributes().getNamedItem("source").getNodeValue();
		}

		@Override
		protected String getEndId() {
			return node.getAttributes().getNamedItem("target").getNodeValue();
		}
		
	}
	
	// TODO: Parameterize this with an external model(s)
	private static String[] connections = { 
		"yed-generic-edge", "yed-generic-polyline-edge", 
		"com.yworks.bpmn.Connection" 
	};
	private static String[] objects     = {
		 "yed-svg-node", "yed-img-node"
		,"com.yworks.flowchart.terminator"
		,"com.yworks.entityRelationship.small_entity"
		,"com.yworks.flowchart.cloud"
		, "com.yworks.bpmn.type",
		"com.yworks.flowchart.networkMessage",
		"com.yworks.flowchart.userMessage"

		// TODO: Rectangle is a type of ShapeNode... 
		,"rectangle", "roundrectangle"
	};
	
	private static String[] containers =  { "group" };

	private static String[] bmpnObjects     = {
		"com.yworks.bpmn.Activity.withShadow",
		"com.yworks.bpmn.Artifact.withShadow",
		"com.yworks.bpmn.Event.withShadow"
	};
	
	private static String[] filter =  {  };

	private ObjectKind decideType(Node obj, String type) {
		if ( Arrays.asList(connections).contains(type) ) return new YedConnection(obj);
		if ( Arrays.asList(objects).contains(type) ) return new YedObject(obj);		
		if ( Arrays.asList(bmpnObjects).contains(type) ) return new YedBpmnObject(obj);		
		if ( Arrays.asList(containers).contains(type) ) return new YedContainer(obj);		
		if ( Arrays.asList(filter).contains(type) ) return null;		
		
		throw new RuntimeException("Unsupported object type " + type);
	}

	
	
	@Override
	public void error(SAXParseException arg0) throws SAXException {
		//System.out.println(arg0);
	}

	@Override
	public void fatalError(SAXParseException arg0) throws SAXException {
		//System.out.println(arg0);	
	}

	@Override
	public void warning(SAXParseException arg0) throws SAXException {
		//System.out.println(arg0);		
	}
	

}
