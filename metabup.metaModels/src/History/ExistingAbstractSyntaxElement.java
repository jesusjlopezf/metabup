/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package History;

import org.eclipse.emf.ecore.EModelElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Existing Abstract Syntax Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link History.ExistingAbstractSyntaxElement#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see History.HistoryPackage#getExistingAbstractSyntaxElement()
 * @model
 * @generated
 */
public interface ExistingAbstractSyntaxElement extends AbstractSyntaxElement {
	/**
	 * Returns the value of the '<em><b>Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element</em>' reference.
	 * @see #setElement(EModelElement)
	 * @see History.HistoryPackage#getExistingAbstractSyntaxElement_Element()
	 * @model required="true"
	 * @generated
	 */
	EModelElement getElement();

	/**
	 * Sets the value of the '{@link History.ExistingAbstractSyntaxElement#getElement <em>Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element</em>' reference.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(EModelElement value);

} // ExistingAbstractSyntaxElement
