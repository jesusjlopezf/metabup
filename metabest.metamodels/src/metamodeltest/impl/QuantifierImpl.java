/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.Every;
import metamodeltest.LessThan;
import metamodeltest.MetamodeltestPackage;
import metamodeltest.MoreThan;
import metamodeltest.None;
import metamodeltest.Quantifier;
import metamodeltest.Some;
import metamodeltest.StrictNumber;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Quantifier</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class QuantifierImpl extends MinimalEObjectImpl.Container implements Quantifier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QuantifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.QUANTIFIER;
	}
	
	public int[] getQuantity(int naturalmax){
		int minmax[] = {0, 0};

		if(this instanceof None) return minmax;
		
		if(this instanceof Some){
			minmax[0] = 1;
			minmax[1] = naturalmax;
		}
		
		if(this instanceof Every){
			Every e = (Every)this;
			
			if(e.isNot()){
				minmax[0] = 0;
				if(naturalmax > 0) minmax[1] = naturalmax - 1;
			}else{
				minmax[0] = naturalmax;
				minmax[1] = naturalmax;
			}
			
			return minmax;
		}
		
		if(this instanceof metamodeltest.Number){
			if(this instanceof StrictNumber){
				minmax[0] = ((metamodeltest.Number) this).getN().getValue();
				minmax[1] = ((metamodeltest.Number) this).getN().getValue();
				return minmax;
			}
			
			if(this instanceof MoreThan){
				MoreThan mtq = (MoreThan)this;
				int n = mtq.getN().getValue();
				
				if(mtq.isOrEqual()) minmax[0] = n;
				else minmax[0] = n+1;
				
				if(mtq.getAnd() == null){
					minmax[1] = naturalmax;
					return minmax;
				}
				
				LessThan ltq = mtq.getAnd();
				int nMax = ltq.getN().getValue();
				if(ltq.isOrEqual()) minmax[1] = nMax;
				else minmax[1] = nMax-1;
				return minmax;
			}
			
			if(this instanceof LessThan){
				LessThan ltq = (LessThan)this;
				int n = ltq.getN().getValue();
				
				if(ltq.isOrEqual()) minmax[1] = n;
				else minmax[1] = n-1;
				
				if(ltq.getAnd() == null){
					minmax[0] = 0;
					return minmax;
				}
				
				MoreThan mtq = ltq.getAnd();
				int nMin = mtq.getN().getValue();
				if(mtq.isOrEqual()) minmax[0] = nMin;
				else minmax[0] = nMin+1;
				return minmax;
			}
			
			minmax[0] = ((metamodeltest.Number) this).getN().getValue();
			minmax[1] = naturalmax;
			return minmax;
		}
		
		return minmax;		
	}
} //QuantifierImpl
