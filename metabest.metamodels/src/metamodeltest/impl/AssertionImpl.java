/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import metamodeltest.Assertion;
import metamodeltest.BooleanParameter;
import metamodeltest.IntegerParameter;
import metamodeltest.MetamodeltestPackage;
import metamodeltest.Parameter;
import metamodeltest.ParameterStringValue;
import metamodeltest.ParameterValue;
import metamodeltest.Selector;
import metamodeltest.StringParameter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assertion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.AssertionImpl#getSelector <em>Selector</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AssertionImpl extends AssertionCallImpl implements Assertion {
	/**
	 * The cached value of the '{@link #getSelector() <em>Selector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelector()
	 * @generated
	 * @ordered
	 */
	protected Selector selector;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssertionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.ASSERTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Selector getSelector() {
		return selector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelector(Selector newSelector, NotificationChain msgs) {
		Selector oldSelector = selector;
		selector = newSelector;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ASSERTION__SELECTOR, oldSelector, newSelector);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelector(Selector newSelector) {
		if (newSelector != selector) {
			NotificationChain msgs = null;
			if (selector != null)
				msgs = ((InternalEObject)selector).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ASSERTION__SELECTOR, null, msgs);
			if (newSelector != null)
				msgs = ((InternalEObject)newSelector).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ASSERTION__SELECTOR, null, msgs);
			msgs = basicSetSelector(newSelector, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ASSERTION__SELECTOR, newSelector, newSelector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION__SELECTOR:
				return basicSetSelector(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION__SELECTOR:
				return getSelector();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION__SELECTOR:
				setSelector((Selector)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION__SELECTOR:
				setSelector((Selector)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ASSERTION__SELECTOR:
				return selector != null;
		}
		return super.eIsSet(featureID);
	}
	
	public List<String> putValuesOnParameters(HashMap<String, String> params){
		List<String> errorMessages = new ArrayList<String>();
		TreeIterator<EObject> it = this.eAllContents();
		
		while(it.hasNext()){
			EObject eo = it.next();
			
			if(eo instanceof Parameter){
				Parameter p = (Parameter)eo;
				
				String name = null;
				
				if(p.getParameterDefinition() != null){				
					if(p.getParameterReuse() != null) name = p.getParameterReuse().getName();
					else if(p.getParameterDefinition() != null) name = p.getParameterDefinition().getName();
					
					String paramObjectName = params.get(name);
					
					if(p instanceof StringParameter)((StringParameter) p).setValue(paramObjectName);
					else errorMessages.add("A non-string parameter was parsed");
				}
			}
		}
	
		return errorMessages;
	}
	
	public List<String> putValuesOnParameters(List<ParameterValue> params){
		List<String> errorMessages = new ArrayList<String>();
		TreeIterator<EObject> it = this.eAllContents();
		
		while(it.hasNext()){
			EObject eo = it.next();
			
			if(eo instanceof Parameter){
				Parameter p = (Parameter)eo;
				
				String name = null;
				
				if(p.getParameterDefinition() != null){				
					if(p.getParameterReuse() != null) name = p.getParameterReuse().getName();
					else if(p.getParameterDefinition() != null) name = p.getParameterDefinition().getName();
					
					for(ParameterValue pv : params){
						if(name.equals(pv.getParamDefinition().getName())){
							if(pv instanceof ParameterStringValue){
								String value = ((ParameterStringValue) pv).getValue();
								if(p instanceof IntegerParameter){								
									 try { 
									       Integer.parseInt(value); 
									       ((IntegerParameter) p).setValue(Integer.parseInt(value));
									    } catch(NumberFormatException e) { 
									    	errorMessages.add("The parameter \'" + p.getParameterDefinition().getName() +"\' should have an int value");
									    }															
								}else if(p instanceof StringParameter){
									((StringParameter) p).setValue(value);
								}else if(p instanceof BooleanParameter){
									if(value.equals("true")) ((BooleanParameter) p).setValue(true);
									else if(value.equals("false")) ((BooleanParameter) p).setValue(false);
									else errorMessages.add("The parameter \'" + p.getParameterDefinition().getName() +"\' should have a bool value");
								}
							}
						}
					}
				}
			}
		}
	
		return errorMessages;
	}
	
} //AssertionImpl
