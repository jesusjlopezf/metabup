/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package History;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tag Based</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link History.TagBased#getTags <em>Tags</em>}</li>
 * </ul>
 * </p>
 *
 * @see History.HistoryPackage#getTagBased()
 * @model
 * @generated
 */
public interface TagBased extends MetaInfo {
	/**
	 * Returns the value of the '<em><b>Tags</b></em>' containment reference list.
	 * The list contents are of type {@link History.Tag}.
	 * It is bidirectional and its opposite is '{@link History.Tag#getTagCollection <em>Tag Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tags</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tags</em>' containment reference list.
	 * @see History.HistoryPackage#getTagBased_Tags()
	 * @see History.Tag#getTagCollection
	 * @model opposite="tagCollection" containment="true"
	 * @generated
	 */
	EList<Tag> getTags();

} // TagBased
