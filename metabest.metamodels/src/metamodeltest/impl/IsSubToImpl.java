/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.BooleanParameter;
import metamodeltest.ClassSelector;
import metamodeltest.IsSubTo;
import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Is Sub To</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.IsSubToImpl#getClassSels <em>Class Sels</em>}</li>
 *   <li>{@link metamodeltest.impl.IsSubToImpl#getOrEqual <em>Or Equal</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IsSubToImpl extends InheritanceImpl implements IsSubTo {
	/**
	 * The cached value of the '{@link #getClassSels() <em>Class Sels</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassSels()
	 * @generated
	 * @ordered
	 */
	protected ClassSelector classSels;

	/**
	 * The cached value of the '{@link #getOrEqual() <em>Or Equal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrEqual()
	 * @generated
	 * @ordered
	 */
	protected BooleanParameter orEqual;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IsSubToImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.IS_SUB_TO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSelector getClassSels() {
		return classSels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassSels(ClassSelector newClassSels, NotificationChain msgs) {
		ClassSelector oldClassSels = classSels;
		classSels = newClassSels;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.IS_SUB_TO__CLASS_SELS, oldClassSels, newClassSels);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassSels(ClassSelector newClassSels) {
		if (newClassSels != classSels) {
			NotificationChain msgs = null;
			if (classSels != null)
				msgs = ((InternalEObject)classSels).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.IS_SUB_TO__CLASS_SELS, null, msgs);
			if (newClassSels != null)
				msgs = ((InternalEObject)newClassSels).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.IS_SUB_TO__CLASS_SELS, null, msgs);
			msgs = basicSetClassSels(newClassSels, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.IS_SUB_TO__CLASS_SELS, newClassSels, newClassSels));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanParameter getOrEqual() {
		return orEqual;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrEqual(BooleanParameter newOrEqual, NotificationChain msgs) {
		BooleanParameter oldOrEqual = orEqual;
		orEqual = newOrEqual;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.IS_SUB_TO__OR_EQUAL, oldOrEqual, newOrEqual);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrEqual(BooleanParameter newOrEqual) {
		if (newOrEqual != orEqual) {
			NotificationChain msgs = null;
			if (orEqual != null)
				msgs = ((InternalEObject)orEqual).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.IS_SUB_TO__OR_EQUAL, null, msgs);
			if (newOrEqual != null)
				msgs = ((InternalEObject)newOrEqual).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.IS_SUB_TO__OR_EQUAL, null, msgs);
			msgs = basicSetOrEqual(newOrEqual, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.IS_SUB_TO__OR_EQUAL, newOrEqual, newOrEqual));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.IS_SUB_TO__CLASS_SELS:
				return basicSetClassSels(null, msgs);
			case MetamodeltestPackage.IS_SUB_TO__OR_EQUAL:
				return basicSetOrEqual(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.IS_SUB_TO__CLASS_SELS:
				return getClassSels();
			case MetamodeltestPackage.IS_SUB_TO__OR_EQUAL:
				return getOrEqual();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.IS_SUB_TO__CLASS_SELS:
				setClassSels((ClassSelector)newValue);
				return;
			case MetamodeltestPackage.IS_SUB_TO__OR_EQUAL:
				setOrEqual((BooleanParameter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.IS_SUB_TO__CLASS_SELS:
				setClassSels((ClassSelector)null);
				return;
			case MetamodeltestPackage.IS_SUB_TO__OR_EQUAL:
				setOrEqual((BooleanParameter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.IS_SUB_TO__CLASS_SELS:
				return classSels != null;
			case MetamodeltestPackage.IS_SUB_TO__OR_EQUAL:
				return orEqual != null;
		}
		return super.eIsSet(featureID);
	}

} //IsSubToImpl
