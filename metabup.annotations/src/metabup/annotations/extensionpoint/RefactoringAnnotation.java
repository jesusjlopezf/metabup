/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.extensionpoint;

import java.util.List;

import org.eclipse.emf.ecore.EAnnotation;

import metabup.annotations.IRefactoringAnnotation;
import metabup.annotations.general.Annotation;
import metabup.metamodel.MetaModel;

public abstract class RefactoringAnnotation extends UserAnnotation implements IRefactoringAnnotation {
	public RefactoringAnnotation(){
		this.setPersistent(false);		
	}
	
	@Override
	public final boolean when() {
		return true;
	}
	
	@Override
	public MetaModel perform() {
		return this.getTargetMetaModel();
	}
	
	public List<EAnnotation> getEAnnotations(){
		return null;
	}
}
