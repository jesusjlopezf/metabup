/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver;

import java.io.File;
import java.io.IOException;

import metabest.solver.ui.preferences.PreferenceConstants;
import metabup.annotations.general.AnnotationFactory;
import metabup.metamodel.Attribute;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.Reference;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fragments.Fragment;

public class old_LayoutSolver extends ConstraintSolver {
	File legendFile;
	public static String geometryWords[] = {"x", "y", "width", "height"};
	
	public old_LayoutSolver(IProject project, MetaModel metamodel,
			Fragment baseFragment, File legendFile) throws IOException {
		
		this.mm = getGeometryMetaModel(metamodel);
		this.project = project;
		this.baseFragment = baseFragment;		
		this.legendFile = legendFile;
		//this.useModel = this.metamodel2Use(this.mm, this.baseFragment);
	}
	
	/*@Override
	protected File metamodel2Use(MetaModel metamodel, Fragment baseFragment) throws IOException{
		AnnotationFactory af = new AnnotationFactory();
		af.initializeFactory();
		
		MetaModel2Use transformation = new MetaModel2Use(metamodel, project, af, legendFile);
		
		File f = transformation.execute(baseFragment, 
										store.getInt(PreferenceConstants.P_MIN_DEFAULT_REFERENCE_CARDINALITY), 
										store.getInt(PreferenceConstants.P_MAX_DEFAULT_REFERENCE_CARDINALITY),
										true, false, false, true);
		
		this.nameSwitcher = transformation.getNameSwitch();
		
		return f;
	}*/
	
	private MetaModel getGeometryMetaModel(MetaModel mm) {
		MetaModel gMM = EcoreUtil.copy(mm);
		
		for(Reference r : gMM.getReferences()){
			if(r.isAnnotated("adjacency") || r.isAnnotated("overlapping")){
				//System.out.println(r.getName() + " is annotated " + r.getAnnotations().get(0));
				MetaClass mc = (MetaClass)r.eContainer();
				MetaClass tarmc = (MetaClass)r.getReference();
				//System.out.println("geometry words: " + geometryWords);
				
				for(String s : geometryWords){
					Attribute a = MetamodelFactory.eINSTANCE.createAttribute();
					a.setName(s);
					a.setMin(1);
					a.setMax(1);
					a.setPrimitiveType("IntType");					
					mc.getFeatures().add(a);
					
					Attribute a2 = EcoreUtil.copy(a);
					tarmc.getFeatures().add(a2);
				}				
				
				//System.out.println(mc.getName() + " has " + mc.getAttributes().size() + " attributes");
			}
		}
		
		return gMM;
	}
}
