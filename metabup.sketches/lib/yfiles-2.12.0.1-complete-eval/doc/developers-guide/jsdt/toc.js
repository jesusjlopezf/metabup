/******************************************************************************* * Copyright (c) 2015 IBM Corporation and others. * All rights reserved. This program and the accompanying materials * are made available under the terms of the Eclipse Public License v1.0 * which accompanies this distribution, and is available at * http://www.eclipse.org/legal/epl-v10.html * * Contributors: *     IBM Corporation - initial API and implementation *******************************************************************************/$(function() {
  // enable only for the first two hierarchy levels
  $('.toc > dl > dd, .toc > dl > dd > dl > dd').addClass('toc-hidden');
  $('.toc > dl > dt + dd, .toc > dl > dd > dl > dt + dd').each(function() {
    var dd = $(this);
    var dt = dd.prev();
    dt.addClass('closed');
    dt.click(function() {
      dd.toggleClass('toc-hidden');
      dt.toggleClass('closed').toggleClass('opened');
    });
    dt.hover(function() {
      $(this).css('cursor', 'auto');
    }, function() {
      $(this).css('cursor', 'pointer');
    });
  });
});