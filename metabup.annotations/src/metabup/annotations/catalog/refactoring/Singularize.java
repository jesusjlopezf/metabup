/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.refactoring;

import metabup.annotations.extensionpoint.RefactoringAnnotation;
import metabup.annotations.general.Annotation;
import metabup.lexicon.utils.LexicalInflector;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;

public class Singularize extends RefactoringAnnotation {
	
	public Singularize(){
		super.setName("singularize");
		super.setDescription("It turns a plurarly-named reference and turns it into singular, also setting 1 as its max multiplicity");
	}
	
	@Override
	public MetaModel perform() {
		if(element instanceof Reference){			
			Reference r = (Reference)element;
			if(LexicalInflector.isPlural(r.getName())) r.setName(LexicalInflector.singularize(r.getName()));
			r.setMax(1);
			return targetMetaModel;
		}else return null;
	}
}
