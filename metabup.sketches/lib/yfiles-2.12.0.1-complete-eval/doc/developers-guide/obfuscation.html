<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   
      <title>Appendix&nbsp;A.&nbsp;Obfuscation</title>
      <link rel="stylesheet" href="ystyle.css" type="text/css">
      <meta name="generator" content="DocBook XSL Stylesheets V1.65.1">
      <link rel="home" href="index.html" title="yFiles for Java Developer's Guide">
      <link rel="up" href="index.html" title="yFiles for Java Developer's Guide">
      <link rel="previous" href="export_usage.html" title="Using the yExport Extension Package">
      <link rel="next" href="yGuard_obfuscation.html" title="Name Obfuscation Using yGuard">
      <link rel="stylesheet" href="jsdt/toc.css" type="text/css"><script type="text/javascript" src="jsdt/jquery.min.js"></script><script type="text/javascript" src="jsdt/toc.js"></script><link type="text/css" rel="stylesheet" href="jssh/SyntaxHighlighter.css"><script type="text/javascript" src="jssh/shCore.js"></script><script type="text/javascript" src="jssh/shBrushJava.js"></script><script type="text/javascript" src="jssh/shBrushXml.js"></script><script type="text/javascript">
  function sh() {

    dp.SyntaxHighlighter.HighlightAll('programlisting', false, false, false, null, false);
  }
  </script></head>
   <body onload="sh()" bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
      <div class="navheader">
         <table width="100%" summary="Navigation header">
            <tr>
               <th colspan="3" align="center">Appendix&nbsp;A.&nbsp;Obfuscation</th>
            </tr>
            <tr>
               <td width="20%" align="left"><a accesskey="p" href="export_usage.html">Prev</a>&nbsp;
               </td>
               <th width="60%" align="center">&nbsp;</th>
               <td width="20%" align="right">&nbsp;<a accesskey="n" href="yGuard_obfuscation.html">Next</a></td>
            </tr>
         </table>
         <div class="navline"></div>
         <div style="display:none"><img src="figures/navbg.jpg" alt=""><img src="figures/navline.jpg" alt=""></div>
      </div>
      <div class="appendix" lang="en">
         <div class="titlepage">
            <div>
               <div>
                  <h2 class="title"><a name="obfuscation"></a>Appendix&nbsp;A.&nbsp;Obfuscation
                  </h2>
               </div>
            </div>
            <div></div>
         </div>
         <div class="toc">
            <p><b>Table of Contents</b></p>
            <dl>
               <dt><span class="section"><a href="obfuscation.html#why_obfuscation">Why Obfuscation Matters</a></span></dt>
               <dd>
                  <dl>
                     <dt><span class="section"><a href="obfuscation.html#obfuscation_benefits">Name Obfuscation and its Benefits</a></span></dt>
                  </dl>
               </dd>
               <dt><span class="section"><a href="yGuard_obfuscation.html">Name Obfuscation Using yGuard</a></span></dt>
               <dd>
                  <dl>
                     <dt><span class="section"><a href="yGuard_obfuscation.html#obfuscation_adjusting">Adjusting Names</a></span></dt>
                     <dt><span class="section"><a href="yGuard_obfuscation.html#obf_checking">Checking Obfuscation Success</a></span></dt>
                     <dt><span class="section"><a href="yGuard_obfuscation.html#obfuscation_tutorial">Tutorial Demo Code</a></span></dt>
                  </dl>
               </dd>
            </dl>
         </div><a class="indexterm" name="d0e39698"></a><a class="indexterm" name="d0e39701"></a><p>
            This appendix covers obfuscation of yFiles classes in particular, but also 
            obfuscation of Java code in general. 
            Obfuscation as discussed here means <span class="emphasis"><em>name obfuscation</em></span>. 
            
         </p>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h2 class="title" style="clear: both"><a name="why_obfuscation"></a>Why Obfuscation Matters
                     </h2>
                  </div>
               </div>
               <div></div>
            </div>
            <p>
               Generally, Java byte code shipped in .class files is inherently susceptible to 
               reverse-engineering by simple decompilation. 
               There are several Java byte code decompilers available that can reproduce Java 
               source code from given byte code quite accurately. 
               
            </p>
            <p>
               Performing name obfuscation makes decompiled Java byte code harder to read, if 
               not unreadable at all. 
               
            </p>
            <p>
               Apart from the need of protection for Java code in general, a yFiles licensee 
               in particular is bound to the code protection requirements as stated in the 
               license terms: the 
               <a href="http://www.yworks.com/en/products_yfiles_sla.html#RightsAndLimitations" target="_blank">yFiles license terms</a>
               explicitly require that all essential class, method, and field names of classes 
               belonging to the yFiles library are obfuscated. 
               The obfuscation's intended purpose, namely prevention of any unauthorized use 
               of the library's functionality via the publicly available yFiles API, is also 
               expressed. 
               
            </p>
            <p>
               Name obfuscation completely defeats any attempts to access yFiles functionality 
               that is part of an application via publicly available class or method names. 
               
            </p>
            <div class="important" style="margin-left: 0.5in; margin-right: 0.5in;">
               <h3 class="title">Important</h3>
               <p>
                  All <span class="emphasis"><em>private</em></span> and <span class="emphasis"><em>package private</em></span> 
                  yFiles classes, methods, and fields are already name-obfuscated as a factory 
                  default, since they cannot be used for software development anyway. 
                  
                  The remaining <span class="emphasis"><em>public</em></span> and <span class="emphasis"><em>protected</em></span> 
                  parts of the yFiles API must be obfuscated before a yFiles-based product can be 
                  released. 
                  
               </p>
            </div>
            <div class="section" lang="en">
               <div class="titlepage">
                  <div>
                     <div>
                        <h3 class="title"><a name="obfuscation_benefits"></a>Name Obfuscation and its Benefits
                        </h3>
                     </div>
                  </div>
                  <div></div>
               </div>
               <p>
                  Name obfuscation works by replacing names in Java byte code, e.g., package 
                  names, class, method, and field names by nonsensical new names. 
                  The replacement is done in a consistent way, so that the byte code still works 
                  as before. 
                  
               </p>
               <p>
                  <a href="obfuscation.html#ex_obfuscation" title="Example&nbsp;A.1.&nbsp;Method name obfuscation">Example&nbsp;A.1, &#8220;Method name obfuscation&#8221;</a> conceptually shows the effects of name 
                  obfuscation for method names and method signatures. 
                  Several methods with distinct signature are mapped to a single new name. 
                  
               </p>
               <div class="example"><a name="ex_obfuscation"></a><p class="title"><b>Example&nbsp;A.1.&nbsp;Method name obfuscation</b></p><pre class="programlisting java">
// Original method names/signatures. 
public void myMethod(MyCustomType type, String name, boolean enable){/* Code */}
public void anotherMethod(MyCustomType type){/* Code */}

// Method names/signatures after name obfuscation. 
public void a(f b, String c, boolean d){/* Code */} // Formerly 'myMethod'. 
public void a(f b){/* Code */} // Formerly 'anotherMethod'. 
</pre></div>
               <p>
                  By replacing different method names with a single new name, decompiled Java 
                  byte code is made rather incomprehensible to a human reader, making ad-hoc 
                  reverse-engineering attempts difficult. 
                  
                  Note that, as a side effect of the obfuscation process, the size of any Jars 
                  that bundle Java byte code files is significantly reduced, too. 
                  
               </p>
            </div>
         </div>
      </div>
      <table class="copyright" border="0" cellpadding="0" cellspacing="0" width="100%">
         <tbody>
            <tr>
               <td align="right">
                  <p class="copyright">Copyright &copy;2004-2015, yWorks GmbH. All rights reserved.</p>
               </td>
            </tr>
         </tbody>
      </table>
      <div class="navfooter">
         <div class="navline2"></div>
         <div style="display:none"><img src="figures/navline2.jpg" alt=""><img src="figures/navbg2.jpg" alt=""></div>
         <table width="100%" summary="Navigation footer">
            <tr>
               <td width="40%" align="left"><a accesskey="p" href="export_usage.html">Prev</a>&nbsp;
               </td>
               <td width="20%" align="center"><a accesskey="u" href="index.html" target="_top">Up</a></td>
               <td width="40%" align="right">&nbsp;<a accesskey="n" href="yGuard_obfuscation.html">Next</a></td>
            </tr>
            <tr>
               <td width="40%" align="left" valign="top">Using the yExport Extension Package&nbsp;</td>
               <td width="20%" align="center"><a accesskey="h" href="index.html" target="_top">Home</a></td>
               <td width="40%" align="right" valign="top">&nbsp;Name Obfuscation Using yGuard</td>
            </tr>
         </table>
      </div>
   </body>
</html>