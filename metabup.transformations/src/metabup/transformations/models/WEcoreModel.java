/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations.models;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.CommonPlugin;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * Wrapper for Ecore models (i.e., a metamodel in Ecore).
 * 
 * @author jesus
 *
 */
public class WEcoreModel extends BaseModel {

	public WEcoreModel(Resource resource) {
		super(resource);
	}

	public EPackage createPackage() {
		return (EPackage) addToResource( EcoreFactory.eINSTANCE.createEPackage() );
	}

	public EClass createEClass() {
		return EcoreFactory.eINSTANCE.createEClass();
		// return (EClass) addToResource( EcoreFactory.eINSTANCE.createEClass() );
	}

	public EAttribute createEAtribute() {
		return (EAttribute) addToResource( EcoreFactory.eINSTANCE.createEAttribute() );
	}	

	public EReference createEReference() {
		return (EReference) addToResource( EcoreFactory.eINSTANCE.createEReference() );
	}	
}
