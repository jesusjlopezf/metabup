<?xml version="1.0" encoding="ASCII"?>
<xmi:XMI xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:mm="metabup/metabup.metaModel">
  <mm:MetaModel name="Factory">
    <classes name="Generator" isAbstract="false" supers="/1">
      <features xsi:type="mm:Attribute" min="1" max="1" name="name"/>
      <features xsi:type="mm:Attribute" min="1" max="1" name="iAT"/>
    </classes>
    <classes name="Conveyor" isAbstract="false">
      <features xsi:type="mm:Attribute" min="1" max="1" name="velocity"/>
    </classes>
    <classes name="Assembler" isAbstract="false" supers="/1">
      <features xsi:type="mm:Reference" min="1" max="1" name="in"/>
      <features xsi:type="mm:Reference" min="1" max="1" name="out">
        <annotations name="generalize"/>
      </features>
    </classes>
  </mm:MetaModel>
  <mm:MetaClass name="out" isAbstract="true"/>
</xmi:XMI>
