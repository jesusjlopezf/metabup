/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.preferences;

import org.eclipse.jface.preference.IPreferenceStore;

import metabest.solver.Activator;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {
	public static final String P_MIN_OBJECT_NUMBER = "minObjectNumber";
	
	public static final String P_MAX_OBJECT_NUMBER = "maxObjectNumber";
	
	public static final String P_MIN_DEFAULT_REFERENCE_CARDINALITY = "minDefaultReferenceCardinality";
	
	public static final String P_MAX_DEFAULT_REFERENCE_CARDINALITY = "maxDefaultReferenceCardinality";
	
	public static final String P_ADD_ATTRIBUTES = "addAttributes";
	
	public static final String P_ADD_ASSOCIATIONS = "addAssociations";
	
	public static final String P_ADD_CONTAINEES = "addContainees";
	
	public static final String P_MAKE_UNREACHABLE = "makeUnreachable";
	
	public static String getPreferenceStringValue(String preferenceName){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(preferenceName);
	}
	
	public static boolean getPreferenceBooleanValue(String preferenceName){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getBoolean(preferenceName);
	}
	
	public static int getPreferenceIntegerValue(String preferenceName){
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getInt(preferenceName);
	}
	
	//public static final String P_PATH = "pathPreference";

	// public static final String P_FORZE_ZERO_TO_ONE = "forceZeroCardinalityPreference";

	//public static final String P_CHOICE = "choicePreference";

	//public static final String P_STRING = "stringPreference";
	
}
