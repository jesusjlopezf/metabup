/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.extensions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reach</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.extensions.Reach#getReachSelector <em>Reach Selector</em>}</li>
 *   <li>{@link test.extensions.Reach#getRefName <em>Ref Name</em>}</li>
 * </ul>
 *
 * @see test.extensions.ExtensionsPackage#getReach()
 * @model
 * @generated
 */
public interface Reach extends MetaClassQualifier, FragmentObjectQualifier {
	/**
	 * Returns the value of the '<em><b>Reach Selector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reach Selector</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reach Selector</em>' containment reference.
	 * @see #setReachSelector(Selector)
	 * @see test.extensions.ExtensionsPackage#getReach_ReachSelector()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Selector getReachSelector();

	/**
	 * Sets the value of the '{@link test.extensions.Reach#getReachSelector <em>Reach Selector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reach Selector</em>' containment reference.
	 * @see #getReachSelector()
	 * @generated
	 */
	void setReachSelector(Selector value);

	/**
	 * Returns the value of the '<em><b>Ref Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Name</em>' attribute.
	 * @see #setRefName(String)
	 * @see test.extensions.ExtensionsPackage#getReach_RefName()
	 * @model
	 * @generated
	 */
	String getRefName();

	/**
	 * Sets the value of the '{@link test.extensions.Reach#getRefName <em>Ref Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref Name</em>' attribute.
	 * @see #getRefName()
	 * @generated
	 */
	void setRefName(String value);

} // Reach
