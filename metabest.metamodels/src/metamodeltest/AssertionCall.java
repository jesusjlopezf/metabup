/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assertion Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.AssertionCall#getOCLcomparison <em>OC Lcomparison</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getAssertionCall()
 * @model abstract="true"
 * @generated
 */
public interface AssertionCall extends DescribedElement, AnnotatedElement {

	/**
	 * Returns the value of the '<em><b>OC Lcomparison</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>OC Lcomparison</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OC Lcomparison</em>' containment reference.
	 * @see #setOCLcomparison(OCLExpression)
	 * @see metamodeltest.MetamodeltestPackage#getAssertionCall_OCLcomparison()
	 * @model containment="true"
	 * @generated
	 */
	OCLExpression getOCLcomparison();

	/**
	 * Sets the value of the '{@link metamodeltest.AssertionCall#getOCLcomparison <em>OC Lcomparison</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OC Lcomparison</em>' containment reference.
	 * @see #getOCLcomparison()
	 * @generated
	 */
	void setOCLcomparison(OCLExpression value);
} // AssertionCall
