/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import test.checks.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see test.checks.ChecksPackage
 * @generated
 */
public class ChecksAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ChecksPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChecksAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ChecksPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChecksSwitch<Adapter> modelSwitch =
		new ChecksSwitch<Adapter>() {
			@Override
			public Adapter caseCheck(Check object) {
				return createCheckAdapter();
			}
			@Override
			public Adapter caseExistence(Existence object) {
				return createExistenceAdapter();
			}
			@Override
			public Adapter caseFeatureExistence(FeatureExistence object) {
				return createFeatureExistenceAdapter();
			}
			@Override
			public Adapter caseTypeExistence(TypeExistence object) {
				return createTypeExistenceAdapter();
			}
			@Override
			public Adapter caseAbstractTypeInstance(AbstractTypeInstance object) {
				return createAbstractTypeInstanceAdapter();
			}
			@Override
			public Adapter caseMissingAttribute(MissingAttribute object) {
				return createMissingAttributeAdapter();
			}
			@Override
			public Adapter caseMismatch(Mismatch object) {
				return createMismatchAdapter();
			}
			@Override
			public Adapter caseMultiplicityMismatch(MultiplicityMismatch object) {
				return createMultiplicityMismatchAdapter();
			}
			@Override
			public Adapter caseTypeMismatch(TypeMismatch object) {
				return createTypeMismatchAdapter();
			}
			@Override
			public Adapter caseFeatureNatureMismatch(FeatureNatureMismatch object) {
				return createFeatureNatureMismatchAdapter();
			}
			@Override
			public Adapter caseRestrictionViolation(RestrictionViolation object) {
				return createRestrictionViolationAdapter();
			}
			@Override
			public Adapter caseUncontainment(Uncontainment object) {
				return createUncontainmentAdapter();
			}
			@Override
			public Adapter caseUncontainmentByObject(UncontainmentByObject object) {
				return createUncontainmentByObjectAdapter();
			}
			@Override
			public Adapter caseUncontainmentByMetaClass(UncontainmentByMetaClass object) {
				return createUncontainmentByMetaClassAdapter();
			}
			@Override
			public Adapter caseMissingReference(MissingReference object) {
				return createMissingReferenceAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link test.checks.Check <em>Check</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.Check
	 * @generated
	 */
	public Adapter createCheckAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.Existence <em>Existence</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.Existence
	 * @generated
	 */
	public Adapter createExistenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.FeatureExistence <em>Feature Existence</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.FeatureExistence
	 * @generated
	 */
	public Adapter createFeatureExistenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.TypeExistence <em>Type Existence</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.TypeExistence
	 * @generated
	 */
	public Adapter createTypeExistenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.AbstractTypeInstance <em>Abstract Type Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.AbstractTypeInstance
	 * @generated
	 */
	public Adapter createAbstractTypeInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.MissingAttribute <em>Missing Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.MissingAttribute
	 * @generated
	 */
	public Adapter createMissingAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.Mismatch <em>Mismatch</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.Mismatch
	 * @generated
	 */
	public Adapter createMismatchAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.MultiplicityMismatch <em>Multiplicity Mismatch</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.MultiplicityMismatch
	 * @generated
	 */
	public Adapter createMultiplicityMismatchAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.TypeMismatch <em>Type Mismatch</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.TypeMismatch
	 * @generated
	 */
	public Adapter createTypeMismatchAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.FeatureNatureMismatch <em>Feature Nature Mismatch</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.FeatureNatureMismatch
	 * @generated
	 */
	public Adapter createFeatureNatureMismatchAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.RestrictionViolation <em>Restriction Violation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.RestrictionViolation
	 * @generated
	 */
	public Adapter createRestrictionViolationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.Uncontainment <em>Uncontainment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.Uncontainment
	 * @generated
	 */
	public Adapter createUncontainmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.UncontainmentByObject <em>Uncontainment By Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.UncontainmentByObject
	 * @generated
	 */
	public Adapter createUncontainmentByObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.UncontainmentByMetaClass <em>Uncontainment By Meta Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.UncontainmentByMetaClass
	 * @generated
	 */
	public Adapter createUncontainmentByMetaClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link test.checks.MissingReference <em>Missing Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see test.checks.MissingReference
	 * @generated
	 */
	public Adapter createMissingReferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ChecksAdapterFactory
