/****************************************************************************
 * This demo file is part of the yFiles extension package yExport-1.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * yExport demo files exhibit yExport functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a yExport version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own yExport powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the yExport
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.export;


import y.view.BackgroundRenderer;
import y.view.Graph2DView;

import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.Insets;
import java.awt.print.PageFormat;


/**
 * Renders the view background as greyed out area with a zoom level independent
 * white rectangle, whose size corresponds to that of a specified page
 * format, to achieve an impression similar to that known from document viewers
 * and word processors.
 *
 */
public class PageBackgroundRenderer implements BackgroundRenderer {
  private final Graph2DView view;
  private PageFormat pageFormat;

  /**
   * Constructs a new <code>PageBackgroundRenderer</code> for the specified
   * view.
   */
  public PageBackgroundRenderer( final Graph2DView view ) {
    this.view = view;
    pageFormat = new PageFormat();
    PageFormatHelper.assignMediaSize(
            pageFormat,
            MediaSize.getMediaSizeForName(MediaSizeName.ISO_A4)
    );
  }

  public void paint(Graphics2D gfx, int x, int y, int w, int h) {
    final Color oldColor = gfx.getColor();
    final Stroke oldStroke = gfx.getStroke();

    final Rectangle rect = gfx.getClipBounds();
    gfx.setColor(Color.LIGHT_GRAY);
    gfx.fillRect(rect.x, rect.y, rect.width, rect.height);

    final double z = view.getZoom();
    final int paperWidth = (int)Math.ceil(pageFormat.getWidth());
    final int paperHeight = (int)Math.ceil(pageFormat.getHeight());

    gfx.setColor(Color.WHITE);
    gfx.fillRect(0, 0, paperWidth, paperHeight);
    gfx.setColor(Color.BLACK);
    gfx.setStroke(new BasicStroke((float) (1.0 / z)));
    gfx.drawRect(0, 0, paperWidth, paperHeight);
    gfx.setColor(Color.LIGHT_GRAY);

    final Insets margin = PageFormatHelper.getMargin(pageFormat);
    if (margin.top > 0) {
      final int _y = margin.top;
      gfx.drawLine(margin.left, _y, paperWidth - margin.right, _y);
    }
    if (margin.bottom > 0) {
      final int _y = paperHeight - margin.bottom;
      gfx.drawLine(margin.left, _y, paperWidth - margin.right, _y);
    }
    if (margin.left > 0) {
      final int _x = margin.left;
      gfx.drawLine(_x, margin.top, _x, paperHeight - margin.bottom);
    }
    if (margin.right > 0) {
      final int _x = paperWidth - margin.right;
      gfx.drawLine(_x, margin.top, _x, paperHeight - margin.bottom);
    }

    gfx.setStroke(oldStroke);
    gfx.setColor(oldColor);
  }

  /**
   * Specifies the reference page format for this renderer.
   */
  public void setPageFormat( final PageFormat pageFormat ) {
    if (pageFormat == null) {
      throw new IllegalArgumentException("null");
    }

    this.pageFormat = pageFormat;

    if (view != null) {
      view.updateView();
    }
  }

  /**
   * Returns the reference page format for this renderer.
   */
  public PageFormat getPageFormat() {
    return pageFormat;
  }
}
