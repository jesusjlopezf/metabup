/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations.models;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import metabup.metamodel.Annotation;
import metabup.metamodel.AnnotationParam;
import metabup.metamodel.Attribute;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.MetamodelPackage;
import metabup.metamodel.Reference;

public class WMetamodelModel extends BaseModel {

	public WMetamodelModel(Resource resource) {
		super(resource);
	}

	public MetaModel rootMetamodel() {
		EList<EObject> contents = resource.getContents();
		for (EObject eObject : contents) {
			if ( eObject instanceof MetaModel )
				return (MetaModel) eObject;
		}
		throw new IllegalStateException("No metamodel found at root of resource");
	}

	public MetaModel createMetaModel() {
		return (MetaModel) addToResource( MetamodelFactory.eINSTANCE.createMetaModel() );
	}

	public MetaClass createMetaclass() {
		return (MetaClass) addToResource( MetamodelFactory.eINSTANCE.createMetaClass() );		
	}

	public Attribute createAttribute() {
		return (Attribute) addToResource( MetamodelFactory.eINSTANCE.createAttribute() );		
	}

	public Reference createReference() {
		return (Reference) addToResource( MetamodelFactory.eINSTANCE.createReference() );		
	}
	
	public Annotation createAnnotation() {
		return (Annotation) addToResource( MetamodelFactory.eINSTANCE.createAnnotation());
	}
	
	public AnnotationParam createAnnotationParam() {
		return (AnnotationParam) addToResource( MetamodelFactory.eINSTANCE.createAnnotationParam());
	}
	
	@SuppressWarnings("unchecked")
	public Collection<MetaClass> allMetaClasses() {
		return (Collection<MetaClass>) allObjectsOf(MetamodelPackage.eINSTANCE.getMetaClass());		
	}

}
