/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments.impl;

import org.eclipse.emf.ecore.EClass;

import fragments.Feature;
import fragments.FragmentsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class FeatureImpl extends TypedElementImpl implements Feature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FragmentsPackage.Literals.FEATURE;
	}

} //FeatureImpl
