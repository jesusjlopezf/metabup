/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.issues.lib;

import metabup.issues.general.categories.Automatic;
import metabup.metamodel.MetaClass;

public class UnsuffixMetaClass extends Automatic {
	public UnsuffixMetaClass(MetaClass mc){
		if(mc != null){
			super.elements.add(mc);
			super.setDescription(mc.getName() + " has the suffix \'Type\', which is normally unnecessary. Remove");
		}		
	}
	
	@Override
	public void run() {
		MetaClass mc = (MetaClass)elements.get(0);		
		mc.setName(mc.getName().substring(0, mc.getName().indexOf("Type")));			
	}
}
