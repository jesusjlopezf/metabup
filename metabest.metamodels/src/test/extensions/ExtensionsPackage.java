/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.extensions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see test.extensions.ExtensionsFactory
 * @model kind="package"
 * @generated
 */
public interface ExtensionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "extensions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/metabest.metamodels/model/test.ecore#//extensions";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "metabest.metamodels";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExtensionsPackage eINSTANCE = test.extensions.impl.ExtensionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link test.extensions.impl.SelectorImpl <em>Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.SelectorImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getSelector()
	 * @generated
	 */
	int SELECTOR = 8;

	/**
	 * The number of structural features of the '<em>Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTOR_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.extensions.impl.MetaClassSelectorImpl <em>Meta Class Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.MetaClassSelectorImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getMetaClassSelector()
	 * @generated
	 */
	int META_CLASS_SELECTOR = 0;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_SELECTOR__FILTER = SELECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Only New Elements</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_SELECTOR__ONLY_NEW_ELEMENTS = SELECTOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Quantifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_SELECTOR__QUANTIFIER = SELECTOR_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Metaclass Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_SELECTOR__METACLASS_NAME = SELECTOR_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Meta Class Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_SELECTOR_FEATURE_COUNT = SELECTOR_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Meta Class Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_SELECTOR_OPERATION_COUNT = SELECTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.extensions.impl.QualifierImpl <em>Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.QualifierImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getQualifier()
	 * @generated
	 */
	int QUALIFIER = 10;

	/**
	 * The number of structural features of the '<em>Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.extensions.impl.MetaClassQualifierImpl <em>Meta Class Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.MetaClassQualifierImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getMetaClassQualifier()
	 * @generated
	 */
	int META_CLASS_QUALIFIER = 1;

	/**
	 * The number of structural features of the '<em>Meta Class Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_QUALIFIER_FEATURE_COUNT = QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Meta Class Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_QUALIFIER_OPERATION_COUNT = QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.extensions.impl.AndQualifierImpl <em>And Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.AndQualifierImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getAndQualifier()
	 * @generated
	 */
	int AND_QUALIFIER = 2;

	/**
	 * The feature id for the '<em><b>Qualifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_QUALIFIER__QUALIFIERS = META_CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>And Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_QUALIFIER_FEATURE_COUNT = META_CLASS_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>And Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_QUALIFIER_OPERATION_COUNT = META_CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.extensions.impl.OrQualifierImpl <em>Or Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.OrQualifierImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getOrQualifier()
	 * @generated
	 */
	int OR_QUALIFIER = 3;

	/**
	 * The feature id for the '<em><b>Qualifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_QUALIFIER__QUALIFIERS = META_CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Or Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_QUALIFIER_FEATURE_COUNT = META_CLASS_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Or Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_QUALIFIER_OPERATION_COUNT = META_CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.extensions.impl.ReachImpl <em>Reach</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.ReachImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getReach()
	 * @generated
	 */
	int REACH = 4;

	/**
	 * The feature id for the '<em><b>Reach Selector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACH__REACH_SELECTOR = META_CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ref Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACH__REF_NAME = META_CLASS_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Reach</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACH_FEATURE_COUNT = META_CLASS_QUALIFIER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Reach</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REACH_OPERATION_COUNT = META_CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.extensions.impl.ContainsImpl <em>Contains</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.ContainsImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getContains()
	 * @generated
	 */
	int CONTAINS = 5;

	/**
	 * The feature id for the '<em><b>Reach Selector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__REACH_SELECTOR = REACH__REACH_SELECTOR;

	/**
	 * The feature id for the '<em><b>Ref Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__REF_NAME = REACH__REF_NAME;

	/**
	 * The number of structural features of the '<em>Contains</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS_FEATURE_COUNT = REACH_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Contains</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS_OPERATION_COUNT = REACH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.extensions.impl.AttributeValueImpl <em>Attribute Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.AttributeValueImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getAttributeValue()
	 * @generated
	 */
	int ATTRIBUTE_VALUE = 6;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_VALUE__STRING_VALUE = META_CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_VALUE__ATTRIBUTE_NAME = META_CLASS_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Int Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_VALUE__INT_VALUE = META_CLASS_QUALIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Boolean Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_VALUE__BOOLEAN_VALUE = META_CLASS_QUALIFIER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Attribute Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_VALUE_FEATURE_COUNT = META_CLASS_QUALIFIER_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Attribute Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_VALUE_OPERATION_COUNT = META_CLASS_QUALIFIER_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link test.extensions.impl.FragmentObjectSelectorImpl <em>Fragment Object Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.FragmentObjectSelectorImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getFragmentObjectSelector()
	 * @generated
	 */
	int FRAGMENT_OBJECT_SELECTOR = 7;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_OBJECT_SELECTOR__OBJECT = SELECTOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Fragment Object Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_OBJECT_SELECTOR_FEATURE_COUNT = SELECTOR_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Fragment Object Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_OBJECT_SELECTOR_OPERATION_COUNT = SELECTOR_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link test.extensions.impl.FragmentObjectQualifierImpl <em>Fragment Object Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.extensions.impl.FragmentObjectQualifierImpl
	 * @see test.extensions.impl.ExtensionsPackageImpl#getFragmentObjectQualifier()
	 * @generated
	 */
	int FRAGMENT_OBJECT_QUALIFIER = 9;

	/**
	 * The number of structural features of the '<em>Fragment Object Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_OBJECT_QUALIFIER_FEATURE_COUNT = QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fragment Object Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_OBJECT_QUALIFIER_OPERATION_COUNT = QUALIFIER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link test.extensions.MetaClassSelector <em>Meta Class Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Class Selector</em>'.
	 * @see test.extensions.MetaClassSelector
	 * @generated
	 */
	EClass getMetaClassSelector();

	/**
	 * Returns the meta object for the containment reference '{@link test.extensions.MetaClassSelector#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Filter</em>'.
	 * @see test.extensions.MetaClassSelector#getFilter()
	 * @see #getMetaClassSelector()
	 * @generated
	 */
	EReference getMetaClassSelector_Filter();

	/**
	 * Returns the meta object for the attribute '{@link test.extensions.MetaClassSelector#isOnlyNewElements <em>Only New Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Only New Elements</em>'.
	 * @see test.extensions.MetaClassSelector#isOnlyNewElements()
	 * @see #getMetaClassSelector()
	 * @generated
	 */
	EAttribute getMetaClassSelector_OnlyNewElements();

	/**
	 * Returns the meta object for the containment reference '{@link test.extensions.MetaClassSelector#getQuantifier <em>Quantifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Quantifier</em>'.
	 * @see test.extensions.MetaClassSelector#getQuantifier()
	 * @see #getMetaClassSelector()
	 * @generated
	 */
	EReference getMetaClassSelector_Quantifier();

	/**
	 * Returns the meta object for the attribute '{@link test.extensions.MetaClassSelector#getMetaclassName <em>Metaclass Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metaclass Name</em>'.
	 * @see test.extensions.MetaClassSelector#getMetaclassName()
	 * @see #getMetaClassSelector()
	 * @generated
	 */
	EAttribute getMetaClassSelector_MetaclassName();

	/**
	 * Returns the meta object for class '{@link test.extensions.MetaClassQualifier <em>Meta Class Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Class Qualifier</em>'.
	 * @see test.extensions.MetaClassQualifier
	 * @generated
	 */
	EClass getMetaClassQualifier();

	/**
	 * Returns the meta object for class '{@link test.extensions.AndQualifier <em>And Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Qualifier</em>'.
	 * @see test.extensions.AndQualifier
	 * @generated
	 */
	EClass getAndQualifier();

	/**
	 * Returns the meta object for the containment reference list '{@link test.extensions.AndQualifier#getQualifiers <em>Qualifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Qualifiers</em>'.
	 * @see test.extensions.AndQualifier#getQualifiers()
	 * @see #getAndQualifier()
	 * @generated
	 */
	EReference getAndQualifier_Qualifiers();

	/**
	 * Returns the meta object for class '{@link test.extensions.OrQualifier <em>Or Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Qualifier</em>'.
	 * @see test.extensions.OrQualifier
	 * @generated
	 */
	EClass getOrQualifier();

	/**
	 * Returns the meta object for the containment reference list '{@link test.extensions.OrQualifier#getQualifiers <em>Qualifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Qualifiers</em>'.
	 * @see test.extensions.OrQualifier#getQualifiers()
	 * @see #getOrQualifier()
	 * @generated
	 */
	EReference getOrQualifier_Qualifiers();

	/**
	 * Returns the meta object for class '{@link test.extensions.Reach <em>Reach</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reach</em>'.
	 * @see test.extensions.Reach
	 * @generated
	 */
	EClass getReach();

	/**
	 * Returns the meta object for the containment reference '{@link test.extensions.Reach#getReachSelector <em>Reach Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reach Selector</em>'.
	 * @see test.extensions.Reach#getReachSelector()
	 * @see #getReach()
	 * @generated
	 */
	EReference getReach_ReachSelector();

	/**
	 * Returns the meta object for the attribute '{@link test.extensions.Reach#getRefName <em>Ref Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ref Name</em>'.
	 * @see test.extensions.Reach#getRefName()
	 * @see #getReach()
	 * @generated
	 */
	EAttribute getReach_RefName();

	/**
	 * Returns the meta object for class '{@link test.extensions.Contains <em>Contains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contains</em>'.
	 * @see test.extensions.Contains
	 * @generated
	 */
	EClass getContains();

	/**
	 * Returns the meta object for class '{@link test.extensions.AttributeValue <em>Attribute Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Value</em>'.
	 * @see test.extensions.AttributeValue
	 * @generated
	 */
	EClass getAttributeValue();

	/**
	 * Returns the meta object for the attribute '{@link test.extensions.AttributeValue#getStringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Value</em>'.
	 * @see test.extensions.AttributeValue#getStringValue()
	 * @see #getAttributeValue()
	 * @generated
	 */
	EAttribute getAttributeValue_StringValue();

	/**
	 * Returns the meta object for the attribute '{@link test.extensions.AttributeValue#getAttributeName <em>Attribute Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Name</em>'.
	 * @see test.extensions.AttributeValue#getAttributeName()
	 * @see #getAttributeValue()
	 * @generated
	 */
	EAttribute getAttributeValue_AttributeName();

	/**
	 * Returns the meta object for the attribute '{@link test.extensions.AttributeValue#getIntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Int Value</em>'.
	 * @see test.extensions.AttributeValue#getIntValue()
	 * @see #getAttributeValue()
	 * @generated
	 */
	EAttribute getAttributeValue_IntValue();

	/**
	 * Returns the meta object for the attribute '{@link test.extensions.AttributeValue#getBooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Boolean Value</em>'.
	 * @see test.extensions.AttributeValue#getBooleanValue()
	 * @see #getAttributeValue()
	 * @generated
	 */
	EAttribute getAttributeValue_BooleanValue();

	/**
	 * Returns the meta object for class '{@link test.extensions.FragmentObjectSelector <em>Fragment Object Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fragment Object Selector</em>'.
	 * @see test.extensions.FragmentObjectSelector
	 * @generated
	 */
	EClass getFragmentObjectSelector();

	/**
	 * Returns the meta object for the reference '{@link test.extensions.FragmentObjectSelector#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see test.extensions.FragmentObjectSelector#getObject()
	 * @see #getFragmentObjectSelector()
	 * @generated
	 */
	EReference getFragmentObjectSelector_Object();

	/**
	 * Returns the meta object for class '{@link test.extensions.Selector <em>Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Selector</em>'.
	 * @see test.extensions.Selector
	 * @generated
	 */
	EClass getSelector();

	/**
	 * Returns the meta object for class '{@link test.extensions.FragmentObjectQualifier <em>Fragment Object Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fragment Object Qualifier</em>'.
	 * @see test.extensions.FragmentObjectQualifier
	 * @generated
	 */
	EClass getFragmentObjectQualifier();

	/**
	 * Returns the meta object for class '{@link test.extensions.Qualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Qualifier</em>'.
	 * @see test.extensions.Qualifier
	 * @generated
	 */
	EClass getQualifier();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ExtensionsFactory getExtensionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link test.extensions.impl.MetaClassSelectorImpl <em>Meta Class Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.MetaClassSelectorImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getMetaClassSelector()
		 * @generated
		 */
		EClass META_CLASS_SELECTOR = eINSTANCE.getMetaClassSelector();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_CLASS_SELECTOR__FILTER = eINSTANCE.getMetaClassSelector_Filter();

		/**
		 * The meta object literal for the '<em><b>Only New Elements</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute META_CLASS_SELECTOR__ONLY_NEW_ELEMENTS = eINSTANCE.getMetaClassSelector_OnlyNewElements();

		/**
		 * The meta object literal for the '<em><b>Quantifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_CLASS_SELECTOR__QUANTIFIER = eINSTANCE.getMetaClassSelector_Quantifier();

		/**
		 * The meta object literal for the '<em><b>Metaclass Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute META_CLASS_SELECTOR__METACLASS_NAME = eINSTANCE.getMetaClassSelector_MetaclassName();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.MetaClassQualifierImpl <em>Meta Class Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.MetaClassQualifierImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getMetaClassQualifier()
		 * @generated
		 */
		EClass META_CLASS_QUALIFIER = eINSTANCE.getMetaClassQualifier();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.AndQualifierImpl <em>And Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.AndQualifierImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getAndQualifier()
		 * @generated
		 */
		EClass AND_QUALIFIER = eINSTANCE.getAndQualifier();

		/**
		 * The meta object literal for the '<em><b>Qualifiers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND_QUALIFIER__QUALIFIERS = eINSTANCE.getAndQualifier_Qualifiers();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.OrQualifierImpl <em>Or Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.OrQualifierImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getOrQualifier()
		 * @generated
		 */
		EClass OR_QUALIFIER = eINSTANCE.getOrQualifier();

		/**
		 * The meta object literal for the '<em><b>Qualifiers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR_QUALIFIER__QUALIFIERS = eINSTANCE.getOrQualifier_Qualifiers();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.ReachImpl <em>Reach</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.ReachImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getReach()
		 * @generated
		 */
		EClass REACH = eINSTANCE.getReach();

		/**
		 * The meta object literal for the '<em><b>Reach Selector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REACH__REACH_SELECTOR = eINSTANCE.getReach_ReachSelector();

		/**
		 * The meta object literal for the '<em><b>Ref Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REACH__REF_NAME = eINSTANCE.getReach_RefName();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.ContainsImpl <em>Contains</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.ContainsImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getContains()
		 * @generated
		 */
		EClass CONTAINS = eINSTANCE.getContains();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.AttributeValueImpl <em>Attribute Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.AttributeValueImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getAttributeValue()
		 * @generated
		 */
		EClass ATTRIBUTE_VALUE = eINSTANCE.getAttributeValue();

		/**
		 * The meta object literal for the '<em><b>String Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_VALUE__STRING_VALUE = eINSTANCE.getAttributeValue_StringValue();

		/**
		 * The meta object literal for the '<em><b>Attribute Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_VALUE__ATTRIBUTE_NAME = eINSTANCE.getAttributeValue_AttributeName();

		/**
		 * The meta object literal for the '<em><b>Int Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_VALUE__INT_VALUE = eINSTANCE.getAttributeValue_IntValue();

		/**
		 * The meta object literal for the '<em><b>Boolean Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE_VALUE__BOOLEAN_VALUE = eINSTANCE.getAttributeValue_BooleanValue();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.FragmentObjectSelectorImpl <em>Fragment Object Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.FragmentObjectSelectorImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getFragmentObjectSelector()
		 * @generated
		 */
		EClass FRAGMENT_OBJECT_SELECTOR = eINSTANCE.getFragmentObjectSelector();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FRAGMENT_OBJECT_SELECTOR__OBJECT = eINSTANCE.getFragmentObjectSelector_Object();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.SelectorImpl <em>Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.SelectorImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getSelector()
		 * @generated
		 */
		EClass SELECTOR = eINSTANCE.getSelector();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.FragmentObjectQualifierImpl <em>Fragment Object Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.FragmentObjectQualifierImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getFragmentObjectQualifier()
		 * @generated
		 */
		EClass FRAGMENT_OBJECT_QUALIFIER = eINSTANCE.getFragmentObjectQualifier();

		/**
		 * The meta object literal for the '{@link test.extensions.impl.QualifierImpl <em>Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.extensions.impl.QualifierImpl
		 * @see test.extensions.impl.ExtensionsPackageImpl#getQualifier()
		 * @generated
		 */
		EClass QUALIFIER = eINSTANCE.getQualifier();

	}

} //ExtensionsPackage
