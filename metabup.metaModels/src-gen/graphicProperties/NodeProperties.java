/**
 */
package graphicProperties;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see graphicProperties.GraphicPropertiesPackage#getNodeProperties()
 * @model
 * @generated
 */
public interface NodeProperties extends GraphicProperties {
} // NodeProperties
