/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.properties;

import org.eclipse.core.runtime.QualifiedName;

import metabup.properties.MetaBupPersistentProperties;

public abstract class MetaBestPersistentProperties extends MetaBupPersistentProperties {
	public static String FOLDER_KEY_TESTCASE = "testcase";	
}
