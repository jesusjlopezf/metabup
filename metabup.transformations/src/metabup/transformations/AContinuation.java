/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations;

/**
 * Poor's map continuations.
 * 
 * @author jesus
 */
public abstract class AContinuation<T> {
	private Class<T> clazz;
	public AContinuation(Class<T> type) {
		this.clazz = type;
	}
	
	public abstract void execute(T data);
	public void executeObj(Object obj) {
		execute(clazz.cast(obj));
	}
}
