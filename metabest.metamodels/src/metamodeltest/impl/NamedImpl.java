/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.Case;
import metamodeltest.Length;
import metamodeltest.MetamodeltestPackage;
import metamodeltest.Named;
import metamodeltest.StringParameter;

import metamodeltest.WordNature;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Named</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.NamedImpl#getCase <em>Case</em>}</li>
 *   <li>{@link metamodeltest.impl.NamedImpl#getName <em>Name</em>}</li>
 *   <li>{@link metamodeltest.impl.NamedImpl#getSize <em>Size</em>}</li>
 *   <li>{@link metamodeltest.impl.NamedImpl#getWordNature <em>Word Nature</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NamedImpl extends ElementQualifierImpl implements Named {
	/**
	 * The default value of the '{@link #getCase() <em>Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCase()
	 * @generated
	 * @ordered
	 */
	protected static final Case CASE_EDEFAULT = Case.UPPER_CASE;

	/**
	 * The cached value of the '{@link #getCase() <em>Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCase()
	 * @generated
	 * @ordered
	 */
	protected Case case_ = CASE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected StringParameter name;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected Length size;

	/**
	 * The cached value of the '{@link #getWordNature() <em>Word Nature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWordNature()
	 * @generated
	 * @ordered
	 */
	protected WordNature wordNature;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NamedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.NAMED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Case getCase() {
		return case_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCase(Case newCase) {
		Case oldCase = case_;
		case_ = newCase == null ? CASE_EDEFAULT : newCase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.NAMED__CASE, oldCase, case_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringParameter getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetName(StringParameter newName, NotificationChain msgs) {
		StringParameter oldName = name;
		name = newName;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.NAMED__NAME, oldName, newName);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(StringParameter newName) {
		if (newName != name) {
			NotificationChain msgs = null;
			if (name != null)
				msgs = ((InternalEObject)name).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.NAMED__NAME, null, msgs);
			if (newName != null)
				msgs = ((InternalEObject)newName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.NAMED__NAME, null, msgs);
			msgs = basicSetName(newName, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.NAMED__NAME, newName, newName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Length getSize() {
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSize(Length newSize, NotificationChain msgs) {
		Length oldSize = size;
		size = newSize;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.NAMED__SIZE, oldSize, newSize);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSize(Length newSize) {
		if (newSize != size) {
			NotificationChain msgs = null;
			if (size != null)
				msgs = ((InternalEObject)size).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.NAMED__SIZE, null, msgs);
			if (newSize != null)
				msgs = ((InternalEObject)newSize).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.NAMED__SIZE, null, msgs);
			msgs = basicSetSize(newSize, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.NAMED__SIZE, newSize, newSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WordNature getWordNature() {
		return wordNature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWordNature(WordNature newWordNature, NotificationChain msgs) {
		WordNature oldWordNature = wordNature;
		wordNature = newWordNature;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.NAMED__WORD_NATURE, oldWordNature, newWordNature);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWordNature(WordNature newWordNature) {
		if (newWordNature != wordNature) {
			NotificationChain msgs = null;
			if (wordNature != null)
				msgs = ((InternalEObject)wordNature).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.NAMED__WORD_NATURE, null, msgs);
			if (newWordNature != null)
				msgs = ((InternalEObject)newWordNature).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.NAMED__WORD_NATURE, null, msgs);
			msgs = basicSetWordNature(newWordNature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.NAMED__WORD_NATURE, newWordNature, newWordNature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED__NAME:
				return basicSetName(null, msgs);
			case MetamodeltestPackage.NAMED__SIZE:
				return basicSetSize(null, msgs);
			case MetamodeltestPackage.NAMED__WORD_NATURE:
				return basicSetWordNature(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED__CASE:
				return getCase();
			case MetamodeltestPackage.NAMED__NAME:
				return getName();
			case MetamodeltestPackage.NAMED__SIZE:
				return getSize();
			case MetamodeltestPackage.NAMED__WORD_NATURE:
				return getWordNature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED__CASE:
				setCase((Case)newValue);
				return;
			case MetamodeltestPackage.NAMED__NAME:
				setName((StringParameter)newValue);
				return;
			case MetamodeltestPackage.NAMED__SIZE:
				setSize((Length)newValue);
				return;
			case MetamodeltestPackage.NAMED__WORD_NATURE:
				setWordNature((WordNature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED__CASE:
				setCase(CASE_EDEFAULT);
				return;
			case MetamodeltestPackage.NAMED__NAME:
				setName((StringParameter)null);
				return;
			case MetamodeltestPackage.NAMED__SIZE:
				setSize((Length)null);
				return;
			case MetamodeltestPackage.NAMED__WORD_NATURE:
				setWordNature((WordNature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.NAMED__CASE:
				return case_ != CASE_EDEFAULT;
			case MetamodeltestPackage.NAMED__NAME:
				return name != null;
			case MetamodeltestPackage.NAMED__SIZE:
				return size != null;
			case MetamodeltestPackage.NAMED__WORD_NATURE:
				return wordNature != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (case: ");
		result.append(case_);
		result.append(')');
		return result.toString();
	}

} //NamedImpl
