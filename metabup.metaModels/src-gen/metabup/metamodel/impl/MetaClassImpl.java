/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.metamodel.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import metabup.metamodel.*;
import metabup.metamodel.impl.AnnotatedElementImpl;
import metabup.metamodel.impl.ReferenceImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link metabup.metamodel.impl.MetaClassImpl#getName <em>Name</em>}</li>
 *   <li>{@link metabup.metamodel.impl.MetaClassImpl#getIsAbstract <em>Is Abstract</em>}</li>
 *   <li>{@link metabup.metamodel.impl.MetaClassImpl#getFeatures <em>Features</em>}</li>
 *   <li>{@link metabup.metamodel.impl.MetaClassImpl#getSupers <em>Supers</em>}</li>
 *   <li>{@link metabup.metamodel.impl.MetaClassImpl#getSubclasses <em>Subclasses</em>}</li>
 *   <li>{@link metabup.metamodel.impl.MetaClassImpl#getIncomingRefs <em>Incoming Refs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetaClassImpl extends AnnotatedElementImpl implements MetaClass {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getIsAbstract() <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_ABSTRACT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIsAbstract() <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsAbstract()
	 * @generated
	 * @ordered
	 */
	protected Boolean isAbstract = IS_ABSTRACT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFeatures() <em>Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<Feature> features;

	/**
	 * The cached value of the '{@link #getSupers() <em>Supers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSupers()
	 * @generated
	 * @ordered
	 */
	protected EList<MetaClass> supers;

	/**
	 * The cached value of the '{@link #getSubclasses() <em>Subclasses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubclasses()
	 * @generated
	 * @ordered
	 */
	protected EList<MetaClass> subclasses;

	/**
	 * The cached value of the '{@link #getIncomingRefs() <em>Incoming Refs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncomingRefs()
	 * @generated
	 * @ordered
	 */
	protected EList<Reference> incomingRefs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	protected MetaClassImpl() {
		super();
		this.setId(EcoreUtil.generateUUID());
		this.setIsAbstract(false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.META_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.META_CLASS__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsAbstract() {
		return isAbstract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsAbstract(Boolean newIsAbstract) {
		Boolean oldIsAbstract = isAbstract;
		isAbstract = newIsAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.META_CLASS__IS_ABSTRACT, oldIsAbstract, isAbstract));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Feature> getFeatures() {
		if (features == null) {
			features = new EObjectContainmentEList<Feature>(Feature.class, this, MetamodelPackage.META_CLASS__FEATURES);
		}
		return features;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MetaClass> getSupers() {
		if (supers == null) {
			supers = new EObjectWithInverseResolvingEList.ManyInverse<MetaClass>(MetaClass.class, this, MetamodelPackage.META_CLASS__SUPERS, MetamodelPackage.META_CLASS__SUBCLASSES);
		}
		return supers;
	}
	
	public List<MetaClass> getAllSupers(){
		List<MetaClass> nonvisited = new ArrayList<MetaClass>();
		List<MetaClass> supers = new ArrayList<MetaClass>();
		
		nonvisited.addAll(this.getSupers());
		
		while(!nonvisited.isEmpty()){
			MetaClass mc = nonvisited.get(0);
			if(!supers.contains(mc)){
				supers.add(mc);
				nonvisited.addAll(mc.getSupers());
			}
			nonvisited.remove(mc);
		}
		
		return supers;
	}
	
	public List<MetaClass> getAllSubs(){
		List<MetaClass> nonvisited = new ArrayList<MetaClass>();
		List<MetaClass> subs = new ArrayList<MetaClass>();
		
		nonvisited.addAll(this.getSubclasses());
		
		while(!nonvisited.isEmpty()){
			MetaClass mc = nonvisited.get(0);
			if(!subs.contains(mc)){
				subs.add(mc);
				nonvisited.addAll(mc.getSubclasses());
			}
			nonvisited.remove(mc);
		}
		
		return subs;
	}
	
	public List<MetaClass> getSupers(int minHeight, int maxHeight) {
		//return getSupers();
		List<MetaClass> nonvisited = new ArrayList<MetaClass>();
		List<MetaClass> supers = new ArrayList<MetaClass>();
		if(minHeight == 0) return supers;
		if(maxHeight == -1) maxHeight = Integer.MAX_VALUE;
		int level = 1;
		int cont = this.getSupers().size();
		
		nonvisited.addAll(this.getSupers());
		
		while(!nonvisited.isEmpty() && level <= maxHeight){
			MetaClass mc = nonvisited.get(0);
			
			if(!supers.contains(mc) && level >= minHeight) supers.add(mc);							
			nonvisited.addAll(mc.getSupers());
			nonvisited.remove(mc);
			cont--;
			
			if(cont == 0){
				cont = nonvisited.size();
				level++;				
			}
		}
		
		return supers;
	}
	
	public int getInheritancePathsTo(MetaClass targetmc, int minHeight, int maxHeight){
		int paths = 0;
		int level = 1;
		if(maxHeight == -1) maxHeight = Integer.MAX_VALUE;
		int cont = this.getSupers().size();		
		
		if(this.getAllSupers().contains(targetmc) && minHeight > 0){
			List<MetaClass> nonvisited = new ArrayList<MetaClass>();
			List<MetaClass> supers = new ArrayList<MetaClass>();
			
			nonvisited.addAll(this.getSupers());
			
			while(!nonvisited.isEmpty() && level >= minHeight && level <= maxHeight){
				MetaClass mc = nonvisited.get(0);
				if(mc.equals(targetmc)) paths++;
				if(!supers.contains(mc)){
					supers.add(mc);
					nonvisited.addAll(mc.getSupers());
				}
				nonvisited.remove(mc);
				cont--;
				
				if(cont == 0){
					cont = nonvisited.size();
					level++;				
				}
			}
		}
		
		return paths;
	}
	
	public int getInheritancePathsFrom(MetaClass targetmc){
		int paths = 0;
		
		if(this.getAllSubs().contains(targetmc)){
			List<MetaClass> nonvisited = new ArrayList<MetaClass>();
			List<MetaClass> subs = new ArrayList<MetaClass>();
			
			nonvisited.addAll(this.getSubclasses());
			
			while(!nonvisited.isEmpty()){
				MetaClass mc = nonvisited.get(0);
				if(mc.equals(targetmc)) paths++;
				if(!subs.contains(mc)){
					subs.add(mc);
					nonvisited.addAll(mc.getSubclasses());
				}
				nonvisited.remove(mc);
			}
		}
		
		return paths;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MetaClass> getSubclasses() {
		if (subclasses == null) {
			subclasses = new EObjectWithInverseResolvingEList.ManyInverse<MetaClass>(MetaClass.class, this, MetamodelPackage.META_CLASS__SUBCLASSES, MetamodelPackage.META_CLASS__SUPERS);
		}
		return subclasses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Reference> getIncomingRefs() {
		if (incomingRefs == null) {
			incomingRefs = new EObjectResolvingEList<Reference>(Reference.class, this, MetamodelPackage.META_CLASS__INCOMING_REFS);
		}
		return incomingRefs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodelPackage.META_CLASS__SUPERS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSupers()).basicAdd(otherEnd, msgs);
			case MetamodelPackage.META_CLASS__SUBCLASSES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSubclasses()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodelPackage.META_CLASS__FEATURES:
				return ((InternalEList<?>)getFeatures()).basicRemove(otherEnd, msgs);
			case MetamodelPackage.META_CLASS__SUPERS:
				return ((InternalEList<?>)getSupers()).basicRemove(otherEnd, msgs);
			case MetamodelPackage.META_CLASS__SUBCLASSES:
				return ((InternalEList<?>)getSubclasses()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodelPackage.META_CLASS__NAME:
				return getName();
			case MetamodelPackage.META_CLASS__IS_ABSTRACT:
				return getIsAbstract();
			case MetamodelPackage.META_CLASS__FEATURES:
				return getFeatures();
			case MetamodelPackage.META_CLASS__SUPERS:
				return getSupers();
			case MetamodelPackage.META_CLASS__SUBCLASSES:
				return getSubclasses();
			case MetamodelPackage.META_CLASS__INCOMING_REFS:
				return getIncomingRefs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodelPackage.META_CLASS__NAME:
				setName((String)newValue);
				return;
			case MetamodelPackage.META_CLASS__IS_ABSTRACT:
				setIsAbstract((Boolean)newValue);
				return;
			case MetamodelPackage.META_CLASS__FEATURES:
				getFeatures().clear();
				getFeatures().addAll((Collection<? extends Feature>)newValue);
				return;
			case MetamodelPackage.META_CLASS__SUPERS:
				getSupers().clear();
				getSupers().addAll((Collection<? extends MetaClass>)newValue);
				return;
			case MetamodelPackage.META_CLASS__SUBCLASSES:
				getSubclasses().clear();
				getSubclasses().addAll((Collection<? extends MetaClass>)newValue);
				return;
			case MetamodelPackage.META_CLASS__INCOMING_REFS:
				getIncomingRefs().clear();
				getIncomingRefs().addAll((Collection<? extends Reference>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodelPackage.META_CLASS__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MetamodelPackage.META_CLASS__IS_ABSTRACT:
				setIsAbstract(IS_ABSTRACT_EDEFAULT);
				return;
			case MetamodelPackage.META_CLASS__FEATURES:
				getFeatures().clear();
				return;
			case MetamodelPackage.META_CLASS__SUPERS:
				getSupers().clear();
				return;
			case MetamodelPackage.META_CLASS__SUBCLASSES:
				getSubclasses().clear();
				return;
			case MetamodelPackage.META_CLASS__INCOMING_REFS:
				getIncomingRefs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodelPackage.META_CLASS__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MetamodelPackage.META_CLASS__IS_ABSTRACT:
				return IS_ABSTRACT_EDEFAULT == null ? isAbstract != null : !IS_ABSTRACT_EDEFAULT.equals(isAbstract);
			case MetamodelPackage.META_CLASS__FEATURES:
				return features != null && !features.isEmpty();
			case MetamodelPackage.META_CLASS__SUPERS:
				return supers != null && !supers.isEmpty();
			case MetamodelPackage.META_CLASS__SUBCLASSES:
				return subclasses != null && !subclasses.isEmpty();
			case MetamodelPackage.META_CLASS__INCOMING_REFS:
				return incomingRefs != null && !incomingRefs.isEmpty();
		}
		return super.eIsSet(featureID);
	}


	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		return this.getName();
	}
	
	public boolean isAnnotated(String name){
		for(Annotation a : this.getAnnotations())
			if(a.getName().equals(name))
				return true;
		
		return false;
	}
	
	public List<Feature> getFeatures(boolean inherited){
		if(!inherited) return getFeatures();
		List<Feature> allFeatures = new ArrayList<Feature>();
		allFeatures.addAll(this.getFeatures());
			
		for(MetaClass mc : this.getAllSupers())
			allFeatures.addAll(mc.getFeatures());
			
		return allFeatures;
	}
	
	public List<Reference> getReferences(){
		List<Reference> references = new ArrayList<Reference>();
		
		for(Feature f : this.getFeatures())
			if(f instanceof Reference)
				references.add((Reference)f);
		
		return references;
	}
	
	public List<Reference> getReferences(boolean byInheritance, int refSort){
		List<Reference> allReferences = new ArrayList<Reference>();
		
		////System.out.println("refSort = " + refSort);
		
		if(refSort == ReferenceImpl.IRRELEVANT) allReferences.addAll(this.getReferences());
		else{
			for(Reference r : this.getReferences()){
				if(refSort == ReferenceImpl.ASSOCIATION && !r.isAnnotated("composition")) allReferences.add(r);
				else if(refSort == ReferenceImpl.CONTAINMENT && r.isAnnotated("composition")){
					////System.out.println(r.getName() + " is annotated containment");
					allReferences.add(r);
				}
			}
		}

		if(byInheritance) for(MetaClass mc : this.getAllSupers()) allReferences.addAll(mc.getReferences(byInheritance, refSort));
			
		return allReferences;
	}
	
	public List<Reference> getIncomingRefs(boolean byInheritance, boolean subs, int refSort){
		
		/* * Thye olde way. From when incoming references weren't stored
		 */ 

		/*List<Reference> references = new ArrayList<Reference>();
		MetaModel mm = (MetaModel)this.eContainer;		
		
		for(MetaClass mc :  mm.getClasses()){
			if(mc.straightReference(this, refSort, byInheritance, subs)){
				for(Reference r : mc.getReferences()){
					if(r.getReference().getId().equals(this.getId())) references.add(r);
					else{
						for(MetaClass supermc : this.getAllSupers())
							if(r.getReference().getId().equals(supermc.getId()))
								references.add(r);
					}
				}
			}
		}
		
		return references;*/		
		
		List<Reference> allReferences = new ArrayList<Reference>();
		
		if(refSort == ReferenceImpl.IRRELEVANT) allReferences.addAll(this.getIncomingRefs());
		else{
			for(Reference r : this.getIncomingRefs()){
				if(refSort == ReferenceImpl.ASSOCIATION && !r.isAnnotated("composition")) allReferences.add(r);
				else if(refSort == ReferenceImpl.CONTAINMENT && r.isAnnotated("composition")){
					////System.out.println(r.getName() + " is annotated containment");
					allReferences.add(r);
				}
			}
		}
		
		if(byInheritance) for(MetaClass mc : this.getAllSupers()) allReferences.addAll(mc.getIncomingRefs(byInheritance, subs, refSort));
			
		return allReferences;		
	}
	
	public boolean references(MetaClass targetmc){
		return references(targetmc, 1, 1, 0, ReferenceImpl.IRRELEVANT, true, false, null);
	}
	
	public boolean isReferencedFrom(MetaClass sourcemc, int min, int max, int i, int refSort, boolean byInheritance, boolean subs, List<MetaClass> visited){
		if((max != -1 && max < i) || (max==0)) return false;
		
		if(visited == null || visited.isEmpty()) visited = new ArrayList<MetaClass>();
		if(visited.contains(this)) return false;
		else visited.add(this);
		
		int locali = i+1;
		
		if(this.straightReferencedFrom(sourcemc, refSort, byInheritance, subs)){
			if(locali >= min) return true;
			else{
				//System.out.println("Look: " + locali + ", " + min);
				return false;
			}
		}else{
			if(this.getIncomingRefs(byInheritance, subs, refSort) == null || this.getIncomingRefs(byInheritance, subs, refSort).isEmpty()) return false;
			
			for(Reference r : this.getIncomingRefs(byInheritance, subs, refSort)){
				if(this.isReferencedFrom(r.getReference(), min, max, locali, refSort, byInheritance, subs, visited))
					return true;
			}
		}
		
		return false;	
	}
	
	public boolean references(MetaClass targetmc, int min, int max, int i, int refSort, boolean byInheritance, boolean subs, List<MetaClass> visited){
		if((max != -1 && max < i) || (max==0)) return false;
		
		if(visited == null || visited.isEmpty()) visited = new ArrayList<MetaClass>();
		if(visited.contains(this)) return false;
		else visited.add(this);
		
		int locali = i+1;
		
		if(this.straightReference(targetmc, refSort, byInheritance, subs)){
			if(locali >= min) return true;
			else{
				//System.out.println("Look: " + locali + ", " + min);
				return false;
			}
		}else{
			if(this.getReferences(byInheritance, refSort) == null || this.getReferences(byInheritance, refSort).isEmpty()) return false;
			
			for(Reference r : this.getReferences(byInheritance, refSort)){				
				if(r.getReference().references(targetmc, min, max, locali, refSort, byInheritance, subs, visited)) 
					return true;
			}
		}
		
		return false;		
	}
	
	public boolean straightReferencedFrom(MetaClass sourcemc, int refSort, boolean byInheritance, boolean subs){
		List<Reference> references = new ArrayList<Reference>();
		
		references = this.getIncomingRefs(byInheritance, subs, refSort);
		
		//if(subs) for(MetaClass mc : this.getSubclasses()) references.addAll(mc.getReferences(byInheritance, refSort));
		//if(byInheritance) for(MetaClass mc : this.getSupers()) references.addAll(mc.getReferences(byInheritance, refSort));
		
		for(Reference r : references){
			if(r.getReference().getId().equals(this.getId())) return true;
			else{
				if(subs){
					for(MetaClass mc : this.getSubclasses())
						if(r.getReference().getId().equals(mc.getId()))
							return true;
				}
				
				if(byInheritance){
					for(MetaClass mc : this.getSupers())
						if(r.getReference().getId().equals(mc.getId()))
							return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean straightReference(MetaClass targetmc, int refSort, boolean byInheritance, boolean subs){
		List<Reference> references = new ArrayList<Reference>();
		
		references = this.getReferences(byInheritance, refSort);
		
		if(subs) for(MetaClass mc : this.getSubclasses()) references.addAll(mc.getReferences(byInheritance, refSort));
		if(byInheritance) for(MetaClass mc : this.getSupers()) references.addAll(mc.getReferences(byInheritance, refSort));
		
		for(Reference r : references){
			if(r.getReference().getId().equals(targetmc.getId())) return true;
			else{
				if(subs){
					for(MetaClass mc : targetmc.getSubclasses())
						if(r.getReference().getId().equals(mc.getId()))
							return true;
				}
				
				if(byInheritance){
					for(MetaClass mc : targetmc.getSupers())
						if(r.getReference().getId().equals(mc.getId()))
							return true;
				}
			}
		}
		
		return false;
	}
	
	public List<Attribute> getAttributes(){
		List<Attribute> attributes = new ArrayList<Attribute>();
		
		for(Feature f : this.getFeatures())
			if(f instanceof Attribute)
				attributes.add((Attribute)f);
		
		return attributes;
	}
	
	public List<Attribute> getAttributes(boolean inherited){
		if(!inherited) return getAttributes();
		List<Attribute> allAttributes = new ArrayList<Attribute>();
		allAttributes.addAll(this.getAttributes());
			
		for(MetaClass mc : this.getAllSupers())
			allAttributes.addAll(mc.getAttributes());
			
		return allAttributes;
	}
	
	public boolean isTop(){
		if((this.getSupers() == null || this.getSupers().isEmpty()) /*&& (this.getSubclasses() != null && !this.getSubclasses().isEmpty())*/) return true;
		else return false;
	}
	
	public boolean isSuperTo(MetaClass submc){
		if(this.getSubclasses().contains(submc)) return true;
		if(submc.getSupers().contains(this)) return true;
		return false;
	}
	
	public boolean isSuperTo(MetaClass submc, int minHeight, int maxHeight){
		if(submc.getSupers(minHeight, maxHeight).contains(this)) return true;
		return false;
	}
	
	public boolean isSubTo(MetaClass supermc){
		if(this.getSupers().contains(supermc)) return true;		
		if(supermc.getSubclasses().contains(this)) return true;		
		return false;
	}
	
	public boolean isSubTo(MetaClass supermc, int minHeight, int maxHeight){
		if(this.getSupers(minHeight, maxHeight).contains(supermc)){
			//System.out.println(this.getName() + " is sub to " + supermc.getName() + " in [" + minHeight + ", " + maxHeight);
			return true;			
		}
		return false;
	}

	@Override
	public Feature getFeatureByName(String name, boolean takeInh) {
		for(Feature f : this.getFeatures(takeInh))
			if(f.getName().equals(name)) return f;
		
		return null;
	}

	@Override
	public Attribute getAttributeByName(String name, boolean inherited) {
		for(Attribute a : this.getAttributes(inherited))
			if(a.getName().equals(name)) return a;

		return null;
	}

	@Override
	public Reference getReferenceByName(String name, boolean inherited) {
		for(Reference r : this.getReferences(inherited, ReferenceImpl.IRRELEVANT))
			if(r.getName().equals(name)) return r;
		return null;
	}

} //MetaClassImpl
