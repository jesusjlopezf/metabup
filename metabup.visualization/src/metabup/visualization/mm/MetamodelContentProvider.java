/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.visualization.mm;

import java.util.LinkedList;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.gef4.graph.Node;
import org.eclipse.gef4.zest.fx.ui.jface.IGraphNodeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;

/**
 * It is a adapter to wrap a metamodel model to a Zest graph.
 * 
 * @author jesus
 *
 */
public class MetamodelContentProvider implements IGraphNodeContentProvider {

	private MetaModel metamodel;
	private Object input;
	private Map<Object,Node> map;
	
	
	public MetamodelContentProvider(MetaModel mm, Map<Object, Node> map) {
		this.metamodel = mm;
		this.map = map;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub	
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		input = newInput;
	}

	@Override
	public Object[] getConnectedTo(Object entity) {
		MetaClass m = (MetaClass) entity;
		EList<Feature> features = m.getFeatures();
		LinkedList<MetaClass> refs = new LinkedList<MetaClass>();
		
		for (Feature feature : features) {
			if ( feature instanceof Reference ) {
				Reference r = (Reference) feature;
				 if(map.containsKey(r.getReference()) && m != r.getReference()) refs.add( r.getReference() );
			}
		}
		
		refs.addAll(m.getSupers());
		
		//System.out.println(m.getName() + " connected to " + refs);
		return refs.toArray();
	}

	@Override
	public Object[] getNodes() {
		return metamodel.getClasses().toArray();
	}
}
