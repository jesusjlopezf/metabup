/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.fragments.resources;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.google.inject.Injector;
import com.jesusjlopezf.utils.eclipse.emf.EMFUtils;
import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;
import com.jesusjlopezf.utils.eclipse.xtext.XtextUtils;
import com.jesusjlopezf.utils.resources.FileUtils;

import fragments.Fragment;
import fragments.FragmentModel;
import metabup.FragmentsRuntimeModule;
import metabup.MetamodelingShellStandaloneSetup;
import metabup.fragments.export.Fragment2XMIExporter;
import metabup.fragments.properties.FragmentPersistentProperties;
import metabup.metamodel.MetaModel;
import metabup.shell.Command;
import metabup.shell.FragmentCommand;
import metabup.shell.ShellModel;

public class FragmentFolder implements ErrorHandler {
	private IProject project;
	public static String LEGEND_FOLDER_NAME = "fragments";
	private IFolder folder;
	
	public FragmentFolder(IProject project) {
		this.project = project;
		folder = this.project.getFolder(LEGEND_FOLDER_NAME); // should be obtained by a persistent property, not by its name
	}
	
	public boolean exists(){
		if(folder.exists()) return true;
		return false;
	}
	
	public void refresh(){
		try {
			folder.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}
	
	public boolean create(IProgressMonitor monitor) {
		if(folder == null) folder = this.project.getFolder(LEGEND_FOLDER_NAME);
		 
		try {
			if(!folder.exists()) folder.create(true, true, monitor);
			folder.setPersistentProperty(FragmentPersistentProperties.FOLDER_KEY, FragmentPersistentProperties.FOLDER_KEY_LEGEND);
		} catch (CoreException e) {
			return false;		
		}
				
		return true;		
	}
	
	public void addFragment(FragmentModel fm){
		fm.eResource().setModified(true);
		fm.eResource().setTrackingModification(true);
		String textModel = XtextUtils.serialize(new FragmentsRuntimeModule(), fm);
		File file = IFileUtils.IFile2File(folder.getFile(EcoreUtil.generateUUID() + ".mbupf"));
		
		// for some reason, it spells 'model' instead of 'shell'
		textModel = textModel.replaceFirst("model", "shell");
		
		FileUtils.writeFile(file, textModel);		
		this.refresh(); 
		
	}
	
	public void exportFragmentFolder(IFolder destFolder, File ecore, MetaModel mm, String extension) throws IOException, CoreException{
		List<Fragment> fragments = this.getFragments();		
		for(Fragment f: fragments) exportFragment(f, destFolder, ecore, extension);
	}
	
	public String exportFragment(Fragment fragment, IFolder destFolder, File ecore, String extension) {		
		String fileName = fragment.getName() + EcoreUtil.generateUUID() + "." + extension;
		if(destFolder.getFile(fileName).exists()) fileName = fragment.getName()  + "." + extension;
		File file = IFileUtils.IFile2File(destFolder.getFile(fileName));
		
		Fragment2XMIExporter exporter = new Fragment2XMIExporter(ecore, file, fragment);
		try {
			exporter.execute(null);
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
		}
				
		return "";
	}
	
	/*private EObject getEObjectByName(EList<EObject> objects, String name){
		for(int i = 0; i<objects.size(); i++){			
			for(EStructuralFeature esf : objects.get(i).eClass().getEAllStructuralFeatures()){
				if(esf.getName().equals("name"))
					if(objects.get(i).eGet(esf).equals(name)) return objects.get(i);
			}
		}
		
		return null;
	}
	
	private EList<EObject> getEObjectsByType(EList<EObject> objects, String type, boolean includeSubs){
		EList<EObject> rObjects = new BasicEList<EObject>();
		
		for(EObject eo : objects){
			if(eo.eClass().getName().equals(type)){
				rObjects.add(eo);
			}else if(includeSubs){
				for(EClass sub : eo.eClass().getEAllSuperTypes()){
					if(sub.getName().equals(type)){
						rObjects.add(eo);
					}
				}
			}
		}
		
		return rObjects;
	}
	
	private EList<EObject> getEObjectsByType(Collection<EObject> objects, String type, boolean includeSubs){
		EList<EObject> rObjects = new BasicEList<EObject>();
		
		for(EObject eo : objects){
			if(eo.eClass().getName().equals(type)){
				rObjects.add(eo);
			}else if(includeSubs){
				for(EClass sub : eo.eClass().getEAllSuperTypes()){
					if(sub.getName().equals(type)){
						rObjects.add(eo);
					}
				}
			}
		}
		
		return rObjects;
	}
	
	
	
	private EList<EClass> getAllClasses(File ecore){
		TreeIterator<EObject> ecoreAll = EMFUtils.getEcoreContents(ecore);
		EList<EClass> classes = new BasicEList<EClass>();
		
		while(ecoreAll.hasNext()){
			EObject obj = ecoreAll.next();
			
			if(obj instanceof EClass ){
				classes.add((EClass)obj);
			}		
		}
		
		return classes;
	}*/
	
	public List<Fragment> getFragments() throws CoreException, IOException{				
		if(folder == null || !folder.exists()) return null;
		List<Fragment> fList = new ArrayList<Fragment>();
		
		for(IResource r : folder.members()){
			if(r instanceof IFile){
				File file = IFileUtils.IFile2File((IFile)r);
				String textShell = FileUtils.readFile(file.getAbsolutePath().toString());
				System.out.println("exporting file: " + file.getName());
				Injector injector = new MetamodelingShellStandaloneSetup().createInjectorAndDoEMFRegistration();
				XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
				resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
				Resource resource = resourceSet.createResource(URI.createURI("dummy:/example.mbupf"));
				InputStream in = new ByteArrayInputStream(textShell.getBytes());
				resource.load(in, resourceSet.getLoadOptions());
				ShellModel model = (ShellModel) resource.getContents().get(0);
				
				for(Command c : model.getCommands()){
					if(c instanceof FragmentCommand){
						fList.add(((FragmentCommand) c).getFragment());
					}
				}
				
			}
		}
		
		return fList;
	}
	
	
	/*public String getElementPath(String name, String extension){
		if(folder == null || !folder.exists()) return null;
		
		try {
			for(IResource r : folder.members()){
				if(r instanceof IFile){
					String s1 = r.getName().substring(0, r.getName().lastIndexOf(".")).toLowerCase();
					String s2 = name.toLowerCase();
					String s3 = ((IFile)r).getFileExtension();
					String s4 = extension;
					
					if(s1.equals(s2) && (s3.equalsIgnoreCase(s4))) return r.getFullPath().toString();
					else System.out.println("NOT FOUND! " + " " + s1 + " " + s2 + " " +  "." + s3 + " " + s4);
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			return null;
		}
		
		return null;
	}*/

	@Override
	public void error(SAXParseException exception) throws SAXException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		// TODO Auto-generated method stub
		
	}

}
