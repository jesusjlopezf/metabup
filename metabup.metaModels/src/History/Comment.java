/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package History;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link History.Comment#getCommentedElement <em>Commented Element</em>}</li>
 *   <li>{@link History.Comment#isIncluded <em>Included</em>}</li>
 * </ul>
 * </p>
 *
 * @see History.HistoryPackage#getComment()
 * @model
 * @generated
 */
public interface Comment extends Collaboration {
	/**
	 * Returns the value of the '<em><b>Commented Element</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link History.Collaboration#getComments <em>Comments</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commented Element</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commented Element</em>' container reference.
	 * @see #setCommentedElement(Collaboration)
	 * @see History.HistoryPackage#getComment_CommentedElement()
	 * @see History.Collaboration#getComments
	 * @model opposite="comments" transient="false"
	 * @generated
	 */
	Collaboration getCommentedElement();

	/**
	 * Sets the value of the '{@link History.Comment#getCommentedElement <em>Commented Element</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Commented Element</em>' container reference.
	 * @see #getCommentedElement()
	 * @generated
	 */
	void setCommentedElement(Collaboration value);

	/**
	 * Returns the value of the '<em><b>Included</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Included</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Included</em>' attribute.
	 * @see #setIncluded(boolean)
	 * @see History.HistoryPackage#getComment_Included()
	 * @model
	 * @generated
	 */
	boolean isIncluded();

	/**
	 * Sets the value of the '{@link History.Comment#isIncluded <em>Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Included</em>' attribute.
	 * @see #isIncluded()
	 * @generated
	 */
	void setIncluded(boolean value);

} // Comment
