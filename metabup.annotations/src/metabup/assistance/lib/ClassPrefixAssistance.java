/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.lib;

import java.util.ArrayList;
import java.util.List;

import metabup.annotations.catalog.refactoring.Rename;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.general.AssistanceTip;
import metabup.assistance.general.Solution;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;

public class ClassPrefixAssistance extends AssistanceTip implements IAssistanceTip {
	public ClassPrefixAssistance(){
		super();
	}
	
	public boolean calculateSolutions() {		
		for(MetaClass mc : this.getTargetMetaModel().getClasses()){
			for(Feature f : mc.getFeatures()){				
				if(f.getName().toLowerCase().startsWith(mc.getName().toLowerCase()) && f.getName().length() > mc.getName().length()){
					String suggName = f.getName().substring(mc.getName().length());
					List<AnnotatedElement> set = new ArrayList<AnnotatedElement>();
					set.add(f);
					Solution sol = super.putSolution("The metaclass \'" + mc.getName() + "\' feature \'" + f.getName() + "\' uses its class name as prefix. Rename it \'" + suggName + "\'.", set, new Rename());
					sol.addParam("name", suggName);
				}																	
			}
		}
		
		if(super.solutions.isEmpty()) return false;
		else return true;
	}


	
}
