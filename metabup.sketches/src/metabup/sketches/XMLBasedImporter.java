/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import org.w3c.dom.Node;

import metabup.sketches.Connection;
import metabup.sketches.Container;
import metabup.sketches.KeyValueLabel;
import metabup.sketches.Label;
import metabup.sketches.PlainLabel;
import metabup.sketches.SingleElement;
import metabup.sketches.SketchElement;
import metabup.sketches.SketchFactory;

public class XMLBasedImporter {
	protected HashMap<String, ObjectKind> id2element = new HashMap<String, ObjectKind>();
	
	protected Node filterChildren1(Node node, String tag) {
		Node result = filterChildren1Opt(node, tag);
		if (result == null)
			throw new InvalidFormat("Tag " + tag + " not found in " + node);
		return result;
	}

	protected Node filterChildren1Opt(Node node, String tag) {
		if(node.getChildNodes() == null || node.getChildNodes().getLength() <= 0) return null;
		for (int i = 0; i < node.getChildNodes().getLength(); i++) {
			if (node.getChildNodes().item(i).getNodeName().equals(tag))
				return node.getChildNodes().item(i);
		}
		return null;
	}

	protected List<Node> filterChildren(Node node, String tag) {
		LinkedList<Node> result = new LinkedList<Node>();
		for (int i = 0; i < node.getChildNodes().getLength(); i++) {
			if (node.getChildNodes().item(i).getNodeName().equals(tag))
				result.add(node.getChildNodes().item(i));
		}
		return result;
	}

	protected Node filterChildrenWithAttribute1(Node node, String tag,
			String attName, String value) {
		List<Node> result = filterChildrenWithAttribute(node, tag, attName,
				value);
		if (result.size() > 0)
			return result.get(0);
		return null;
	}

	protected List<Node> filterChildrenWithAttribute(Node node, String tag,
			String attName, String value) {
		LinkedList<Node> result = new LinkedList<Node>();
		for (int i = 0; i < node.getChildNodes().getLength(); i++) {
			if (node.getChildNodes().item(i).getNodeName().equals(tag)) {
				Node att = node.getChildNodes().item(i).getAttributes()
						.getNamedItem(attName);
				if (att != null && att.getNodeValue().equals(value))
					result.add(node.getChildNodes().item(i));
			}
		}
		return result;
	}

	
	protected List<Label> createLabel(String labelText) {
		LinkedList<Label> result = new LinkedList<Label>();
		
		StringTokenizer tokenizer = new StringTokenizer(labelText, ";");

		while ( tokenizer.hasMoreElements() ) {
			String text = (String) tokenizer.nextElement();
			text = text.trim();
			
			//extraer anotaciones
			String words[] = text.split(" ");
			List<String> annotations = new ArrayList<String>();
			text = "";
			
			for(String w : words){
				if(w.startsWith("@")) annotations.add(w.substring(1));
				else text += w + " ";
			}
			
			text = text.trim();			
			
			Label l = parsePair(text);
			
			if ( l == null ) {
				PlainLabel p = SketchFactory.eINSTANCE.createPlainLabel();
				p.setValue(text);
				l = p;
			}
			
			System.out.println("annotations: " + annotations);
			l.getAnnotations().addAll(annotations);
			result.add(l);
		}
		
		return result;
	}

	private Label parsePair(String labelText) {
		String labText = labelText.trim().replace("\n", "");
		int idx  = -1;

		if ( (idx = labText.indexOf("=")) != -1 ) {
			String key = labText.substring(0, idx).trim();
			String value = labText.substring(idx + 1, labText.length()).trim();
			KeyValueLabel l = SketchFactory.eINSTANCE.createKeyValueLabel();
			l.setKey(key);
			System.out.println("l.setKey(" + key + ")");
			l.setValue(value);
			System.out.println("l.setValue(" + value + ")");
			return l;
		}
		return null;
	}

	
	protected Node getFirstTag(Node node) {
		return node.getFirstChild().getNextSibling();
	}


	protected abstract class ObjectKind<T extends SketchElement> {
		protected Node node;
		protected T element;

		public ObjectKind(Node node) {
			this.node = node;
		}

		public abstract T getElement();

		public abstract void setConnections();

		public void setSketchTypeInfo(String type, String toolSymbolName) {
			if(element != null){
	    		element.setSkechTypeInfo(type);
	    		element.setToolSymbolName(toolSymbolName);
			}
		}
	}

	protected abstract class OBJECT extends ObjectKind<SingleElement> {
		public OBJECT(Node obj) {
			super(obj);
		}

		@Override
		public SingleElement getElement() {
			if (element == null)
				element = SketchFactory.eINSTANCE.createSingleElement();
			return element;
		}

		@Override
		public void setConnections() {
		}
	}

	protected abstract class CONTAINER extends ObjectKind<Container> {
		public CONTAINER(Node obj) {
			super(obj);
		}

		@Override
		public Container getElement() {
			if (element == null)
				element = SketchFactory.eINSTANCE.createContainer();
			return element;
		}

		@Override
		public void setConnections() {
		}
	}

	protected abstract class CONNECTION extends ObjectKind<Connection> {
		public CONNECTION(Node obj) {
			super(obj);
		}

		@Override
		public Connection getElement() {
			if (element == null)
				element = SketchFactory.eINSTANCE.createLine();
			return element;
		}

		@Override
		public void setConnections() {
			try {
				element.setSrc( id2element.get(getStartId()).getElement() );
				element.setTgt( id2element.get(getEndId()).getElement() );
			} catch ( InvalidFormat e ) {
				// Ignore the error purposely...
				e.printStackTrace();
			}
		}
		
		protected abstract String getStartId();
		protected abstract String getEndId();	
	}

	
}