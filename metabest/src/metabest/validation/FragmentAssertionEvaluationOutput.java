/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.validation;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.util.EcoreUtil;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.Reference;
import fragments.TypedElement;
import fragments.impl.BooleanValueImpl;
import fragments.impl.IntegerValueImpl;
import fragments.impl.StringValueImpl;
import metabup.annotations.catalog.design.Composition;
import metabup.annotations.general.Annotation;
import metabup.annotations.general.AnnotationFactory;
import metabup.issues.general.IOpenIssue;
import metabup.metamodel.*;
import metabup.metamodel.impl.ReferenceImpl;
import test.Assertion;
import test.AssertionSet;
import test.TestModel;
import test.TestableFragment;
import test.checks.AbstractTypeInstance;
import test.checks.Check;
import test.checks.Existence;
import test.checks.FeatureExistence;
import test.checks.FeatureNatureMismatch;
import test.checks.Mismatch;
import test.checks.MissingAttribute;
import test.checks.MultiplicityMismatch;
import test.checks.RestrictionViolation;
import test.checks.TypeExistence;
import test.checks.TypeMismatch;
import test.checks.Uncontainment;
import test.checks.UncontainmentByMetaClass;
import test.checks.UncontainmentByObject;
import test.checks.expressions.FeatureWithinObjectExpression;
import test.checks.expressions.ObjectExpression;

public class FragmentAssertionEvaluationOutput {
	private Assertion assertion;
	private boolean output = false;
	private String rationale;
	//private String additionalInfo = null;
	
	public FragmentAssertionEvaluationOutput(Assertion a){
		this.assertion = a;
	}
	
	public boolean isTrue(){
		return output;
	}
	
	@Override
	public String toString(){
		Check c = this.assertion.getCheck();
		return this.getCheckDescription(c);
	}
	
	private String getCheckDescription(Check c){
		String description = null;
		if(c instanceof Existence) description = "unkown ";
		
		if(c instanceof FeatureExistence){
			FeatureExistence fe = (FeatureExistence) c;
			description += fe.getFeature().getObject().getName() + "." + fe.getFeature().getFeature().getName();
			return description;
		}
		
		if(c instanceof TypeExistence){
			TypeExistence te = (TypeExistence) c;
			description += "type of " + te.getType().getObject().getName();
			return description;
		}
		
		if(c instanceof AbstractTypeInstance){
			AbstractTypeInstance ati = (AbstractTypeInstance) c;
			description = "abstract type of " + ati.getType().getObject().getName();
			return description;
		}
		
		if(c instanceof MissingAttribute){
			MissingAttribute ma = (MissingAttribute) c;
			description = "missing " + ma.getName() + " from " + ma.getObject().getName();
			return description;
		}
		
		if(c instanceof Mismatch) description = "mismatch on ";
		
		if(c instanceof MultiplicityMismatch){
			MultiplicityMismatch mm = (MultiplicityMismatch) c;
			description += "multiplicity of " + mm.getFeatureMultiplicity().getFeature().getObject().getName() + "." + mm.getFeatureMultiplicity().getFeature().getFeature().getName();
			return description;
		}
		
		if(c instanceof TypeMismatch){
			TypeMismatch tm = (TypeMismatch) c;
			description += "type of " + tm.getFeatureType().getFeature().getObject().getName() + "." + tm.getFeatureType().getFeature().getFeature().getName();			
			return description;
		}
		
		if(c instanceof FeatureNatureMismatch){
			FeatureNatureMismatch fnm = (FeatureNatureMismatch) c;
			description += "nature of " + fnm.getFeatureNature().getFeature().getObject().getName() + "." + fnm.getFeatureNature().getFeature().getFeature().getName();
			return description;
		}
		
		if(c instanceof RestrictionViolation){
			RestrictionViolation rv = (RestrictionViolation) c;
			description = "restriction violation @" + rv.getRestrictionName() + " on \'";
			
			if(rv.getFragmentElement() instanceof FeatureWithinObjectExpression){
				FeatureWithinObjectExpression fwoe = (FeatureWithinObjectExpression) rv.getFragmentElement();
				description += fwoe.getObject().getName() + "." + fwoe.getFeature().getName() + "\'";
			}else if(rv.getFragmentElement() instanceof ObjectExpression){
				ObjectExpression oe = (ObjectExpression) rv.getFragmentElement();
				description += oe.getObject().getName() + "\'";
			}
			
			return description;
		}
		
		if(c instanceof Uncontainment){
			Uncontainment u = (Uncontainment) c;
			description = "\'" + u.getObject().getObject().getName() + "\' should be contained ";
			
			if(u instanceof UncontainmentByObject){
				UncontainmentByObject ubo = (UncontainmentByObject) u;
				description += "in " + "\'" + ubo.getContainer().getObject().getName() + "\'";
			}
			
			if(u instanceof UncontainmentByMetaClass){
				UncontainmentByMetaClass ubm = (UncontainmentByMetaClass) u;
				description += "in some " + "\'" + ubm.getMetaclass().getMetaclassName() + "\'";
			}
			
			return description;
		}
		
		return null;
  }
	
	public boolean evaluate(MetaModel mm){
		Check c = assertion.getCheck();
		rationale = "Unavailable explanation.";
		
		//System.out.println("here is the metamodel: " + mm.getClasses());
		
		if(c instanceof Uncontainment){		
			List<fragments.Reference> fragRefs = new ArrayList<fragments.Reference>();
			List<metabup.metamodel.Reference> mmRefs= new ArrayList<metabup.metamodel.Reference>();
			
			boolean fragContained = false;
			boolean mmContained = false;
			
			Uncontainment u = (Uncontainment) c;
			String objectType = u.getObject().getObject().getType();
			String objectName = u.getObject().getObject().getName();
			
			rationale = "\'" + objectName + "\'";
			
			if(!(u instanceof UncontainmentByObject) && !(u instanceof UncontainmentByMetaClass)){
				Fragment f = (Fragment)u.getObject().getObject().eContainer();			
				
				for(fragments.Object o : f.getObjects()){
					for(fragments.Feature fe : o.getFeatures()){
						if(fe instanceof fragments.Reference){
							Reference r = (Reference)fe;
							for(fragments.Annotation a : r.getAnnotation()){
								if(a.getName().toLowerCase().equals(Composition.NAME)){
									for(fragments.Object refo : r.getReference()){
										if(refo.getType().equals(objectType)) fragRefs.add(r);
									}
								}
							}
						}
					}
				}
				
				fragContained = !fragRefs.isEmpty();
				
				for(MetaClass mc : mm.getClasses()){
					for(metabup.metamodel.Reference r : mc.getReferences(true, ReferenceImpl.CONTAINMENT))
						if(r.getReference().getName().equals(objectType)) mmRefs.add(r);
						else{
							//if it doesn't reference this exact class, maybe it references one of its supers
							MetaClass referencedMc = mm.getClassByName(objectType);
							
							for(MetaClass supermc : referencedMc.getSupers())
								if(r.getReference().getName().equals(supermc.getName())) 
									mmRefs.add(r);
						}
				}

				mmContained = !mmRefs.isEmpty();
			}
			
			if(u instanceof UncontainmentByObject){
				UncontainmentByObject ubo = (UncontainmentByObject)u;
				fragments.Object container = ubo.getContainer().getObject();
				String containerType = container.getType();
				//String containerName = container.getName();				
				
				for(fragments.Feature f : container.getFeatures()){
					if(f instanceof fragments.Reference){
						fragments.Reference r = (Reference)f;
						for(fragments.Annotation a : r.getAnnotation()){
							if(a.getName().toLowerCase().equals(Composition.NAME)){
								for(fragments.Object refo : r.getReference()){
									if(refo.getType().equals(objectType)) fragRefs.add(r);
								}
							}
						}
					}
				}
				
				fragContained = !fragRefs.isEmpty();
				
				MetaClass contMc = mm.getClassByName(containerType);
				
				for(metabup.metamodel.Reference r : contMc.getReferences(true, ReferenceImpl.CONTAINMENT)){
					if(r.getReference().getName().equals(objectType)) mmRefs.add(r);
					else{
						//if it doesn't reference this exact class, maybe it references one of its supers
						MetaClass referencedMc = mm.getClassByName(objectType);
						
						for(MetaClass supermc : referencedMc.getSupers())
							if(r.getReference().getName().equals(supermc.getName())) 
								mmRefs.add(r);
					}
				}
				
				mmContained = !mmRefs.isEmpty();
			}
			
			if(u instanceof UncontainmentByMetaClass){
				UncontainmentByMetaClass ubm = (UncontainmentByMetaClass)u;
				String containerType = ubm.getMetaclass().getMetaclassName();
				MetaClass contMc = mm.getClassByName(containerType);
				
				if(contMc == null){
					rationale = "\'" + containerType + "\'" + " doesn't even exist in the MetaModel";
					return output = false;
				}
				
				Fragment f = (Fragment)u.getObject().getObject().eContainer();			
				
				for(fragments.Object o : f.getObjects(containerType)){
					for(fragments.Feature fe : o.getFeatures()){
						if(fe instanceof fragments.Reference){
							Reference r = (Reference)fe;
							for(fragments.Annotation a : r.getAnnotation()){
								if(a.getName().toLowerCase().equals(Composition.NAME)){
									for(fragments.Object refo : r.getReference()){
										if(refo.getType().equals(objectType)) fragRefs.add(r);
									}
								}
							}
						}
					}
				}
				
				fragContained = !fragRefs.isEmpty();
				
				for(metabup.metamodel.Reference r : contMc.getReferences(true, ReferenceImpl.CONTAINMENT)){
					if(r.getReference().getName().equals(objectType)) mmRefs.add(r);
					else{
						//if it doesn't reference this exact class, maybe it references one of its supers
						MetaClass referencedMc = mm.getClassByName(objectType);
						
						for(MetaClass supermc : referencedMc.getSupers())
							if(r.getReference().getName().equals(supermc.getName())) 
								mmRefs.add(r);
					}
				}
				
				mmContained = !mmRefs.isEmpty();				
			}						
			
			if(!fragContained && !mmContained){
				rationale += " is not suppossed to be contained and it is not indeed.";
				return output = false;
			}
			
			if(fragContained && !mmContained){
				rationale += " is not suppossed to be contained.";
				return output = false;
			}
			
			if(!fragContained && mmContained){
				List<MetaClass> mcs = new ArrayList<MetaClass>();	
				
				for(metabup.metamodel.Reference r : mmRefs){
					MetaClass mc = (MetaClass)r.eContainer();
					mcs.add(mc);
				}
				
				rationale += " should be contained in an object typed: " + mcs;
				return output = true;
			}
			
			if(fragContained && mmContained){
				for(fragments.Reference fr : fragRefs){
					for(metabup.metamodel.Reference mr : mmRefs){
						if(mr.getName().equals(fr.getName())){
							rationale += " is indeed contained.";
							return output = false;
						}
					}
				}
				
				// if not found, there are containment relations both in fragment and metamodel, but neither of them match any of the others
				List<MetaClass> mcs = new ArrayList<MetaClass>();	
				
				for(metabup.metamodel.Reference r : mmRefs){
					MetaClass mc = (MetaClass)r.eContainer();
					mcs.add(mc);
				}
				
				rationale += " should be contained in an object typed either: {" + mcs + "}.";
				return output = true;
			}
		}
		
		if(c instanceof FeatureExistence){
			FeatureExistence fe = (FeatureExistence) c;
			String className = fe.getFeature().getObject().getType();
			String featureName = fe.getFeature().getFeature().getName();
			
			rationale = "\'" + fe.getFeature().getObject().getName() + "." + fe.getFeature().getFeature().getName() + "\'";
			
			for(MetaClass mc : mm.getClasses()){
				if(mc.getName().equals(className)){
					  for(Feature f : mc.getFeatures(true)){
						  if(f.getName().equals(featureName)){
							  rationale += " actually exists in the metamodel.";
							  return output = false;
						  }
					  }
				  }
			  }				  
			
			 rationale += " doesn't exist in the metamodel.";
			 return output = true;
		  }		  		
		
		  if(c instanceof TypeExistence){
			  TypeExistence te = (TypeExistence) c;
			  String typeName = te.getType().getObject().getType();
			  
			  rationale = "The MetaClass \'" + te.getType().getObject().getType() + "\'";
			  
			  for(MetaClass mc : mm.getClasses()){
				  if(mc.getName().equals(typeName)){
					  rationale += " actually exists in the metamodel.";
					  return output = false;
				  }
			  }
			  			  
			  rationale += " doesn't exist in the metamodel.";
			  return output = true;
		  }
		  
		  if(c instanceof AbstractTypeInstance){
			  AbstractTypeInstance ati = (AbstractTypeInstance) c;
			  String typeName = ati.getType().getObject().getType();
			  
			  rationale = "The type of \'" + ati.getType().getObject().getName() + "\', \'" + ati.getType().getObject().getType() + "\'";
			  
			  for(MetaClass mc : mm.getClasses()){
				  if(mc.getName().equals(typeName) && !mc.getIsAbstract()){
					  rationale += " is not abstract.";
					  return output = false;
				  }
			  }
			  
			  rationale += " is indeed an abstract MetaClass in the metamodel.";
			  return output = true;
		  }
		  
		 if(c instanceof MissingAttribute){
			  MissingAttribute ma = (MissingAttribute) c;
			  fragments.Object object = ma.getObject();
			  String className = ma.getObject().getType();
			  String featureName = ma.getName();
			  
			  boolean inFragment = false;
			  boolean inMetamodel = false;
			  
			 
			  
			  for(fragments.Feature f : object.getFeatures())
				  if((f instanceof fragments.Attribute) && f.getName().equals(featureName))
					  inFragment = true;
			  
			  for(MetaClass mc : mm.getClasses()){
				  if(mc.getName().equals(className)){
					  for(Attribute a : mc.getAttributes(true)){
						  if(a.getName().equals(featureName)){
							  inMetamodel = true;
						  }
					  }
				  }
			  }
			  
			  rationale = "\'" + ma.getObject().getName() + "." + featureName + "\'";
			  
			  if(inFragment && inMetamodel){
				  rationale += " is not missing from the fragment.";
				  output = false;
			  }			  
			  else if(!inFragment && inMetamodel){
				  rationale += " is missing from the fragment.";
				  output = true;
			  }			  
			  else if(inFragment && !inMetamodel){
				  rationale += " is present in the fragment, although it actually shouldn't be.";
				  output = false;
			  }
			  else if(!inFragment && !inMetamodel){
				  rationale += " is not present neither in the fragment, nor the metamodel";
				  output = false;
			  }
			  
			  return output;
		 }
		  			 
		 if(c instanceof MultiplicityMismatch){
			 MultiplicityMismatch mmm = (MultiplicityMismatch) c;
			 String className = mmm.getFeatureMultiplicity().getFeature().getObject().getType();
			 String featureName = mmm.getFeatureMultiplicity().getFeature().getFeature().getName();
			 int mmMin = 0;
			 int mmMax = -1;
			 boolean found = false;
			 output = true;
			 
			 rationale = "\'" + mmm.getFeatureMultiplicity().getFeature().getObject().getName() + "." + featureName + "\'";
			 
			 for(MetaClass mc : mm.getClasses()){
				  if(mc.getName().equals(className)){
					  for(Feature f : mc.getFeatures()){
						  if(f.getName().equals(featureName)){
							  found = true;
							  mmMin = f.getMin();
							  mmMax = f.getMax();
						  }
					  }
				  }
			 }
			 
			 if(!found){
				 rationale += " doesn't even exist in the metamodel.";
				 return output = false;
			 }
			 
			 fragments.Feature f = mmm.getFeatureMultiplicity().getFeature().getFeature();
			 int featureSize;
			 
			 if(f instanceof fragments.Attribute){
				 fragments.Attribute a = (fragments.Attribute) f;
				 featureSize = a.getValues().size();
			 }else{
				 fragments.Reference r = (fragments.Reference) f;
				 featureSize = r.getReference().size();
			 }
			 
			 if(this.getAssertionContainer().getCompletion().toString().equals("isExample")){
				 /* evaluation in case the assertion is placed within an example */				 			 
				 
				 if(featureSize >= mmMin){
					 output = false;
					 rationale += " matches the minimum multiplicity (" + mmMin +")";
				 }else rationale += " doesn't match the minimum multiplicity (" + mmMin +")";
				 
				 rationale += " and ";				 				 
			 }
			 
			 /* evaluation in case the assertion is placed within a fragment */
			 
			 if(featureSize <= mmMax || mmMax == -1){
				 output = false;
				 rationale += " matches the maximum multiplicity (" + mmMax +")";
			 } else rationale += " doesn't match the maximum multiplicity (" + mmMax +")";
			 
			 return output;				 						 
		 }
		 
		 if(c instanceof TypeMismatch){
			 TypeMismatch tm = (TypeMismatch) c;
			 metabup.metamodel.Feature mmF = null;			 
			 fragments.Feature fF = tm.getFeatureType().getFeature().getFeature();
			 String className = tm.getFeatureType().getFeature().getObject().getType();
			 String objectName = tm.getFeatureType().getFeature().getObject().getName();
			 String featureName = tm.getFeatureType().getFeature().getFeature().getName();
			 
			 String featureType = null;
			 if(fF instanceof fragments.Reference){
				 fragments.Reference r = (fragments.Reference) fF;
				 featureType = r.getType();
			 }else{
				 fragments.Attribute a = (fragments.Attribute) fF;
				 for(fragments.PrimitiveValue t : a.getValues()) {
					 	
						if ( t instanceof StringValueImpl ) {
							featureType = DType.STRING_TYPE.toString();
						} else if ( t instanceof IntegerValueImpl ) {
							featureType = DType.INT_TYPE.toString();
						} else if ( t instanceof BooleanValueImpl ) {
							featureType = DType.BOOLEAN_TYPE.toString();
						}
					}
			 }
			 
			 boolean found = false;
			 
			 output = true;
			 
			 rationale = "\'" + objectName + "." + featureName + "\'";
			 
			 for(MetaClass mc : mm.getClasses()){
				  if(mc.getName().equals(className)){
					  for(Feature f : mc.getFeatures()){
						  if(f.getName().equals(featureName)){
							  mmF = f;
							  found = true;
						  }
					  }
				  }
			 }
			 
			 if(!found){
				 rationale += " doesn't even exist in the metamodel.";
				 return output = false;
			 }
			 
			 if(mmF instanceof metabup.metamodel.Attribute){
				 metabup.metamodel.Attribute a = (metabup.metamodel.Attribute) mmF;
				 
				 if(a.getPrimitiveType().equals(featureType)){
					 rationale += " has the proper type: " + featureType;
					 return output = false;
				 }else{
					 rationale += " doesn't match the type \'" + featureType + "\'. It should be \'" + a.getPrimitiveType()+ "\'";
					 return output = true;
				 }
			 }else if (mmF instanceof metabup.metamodel.Reference){
				 metabup.metamodel.Reference r = (metabup.metamodel.Reference) mmF;
				 				 
				 if(r.getReference().getName().equals(featureType)){
					 rationale += " has the proper type: " + featureType;
					 return output = false;
				 }else{
					 rationale += " doesn't match the type \'" + featureType + "\'. It should be \'" + r.getReference().getName() + "\'";
					 return output = true;
				 }
			 }
		 }
		 
		 if(c instanceof FeatureNatureMismatch){
			 FeatureNatureMismatch fnm = (FeatureNatureMismatch) c;
			 String className = fnm.getFeatureNature().getFeature().getObject().getType();
			 String objectName = fnm.getFeatureNature().getFeature().getObject().getName();
			 String featureName = fnm.getFeatureNature().getFeature().getFeature().getName();
			 
			 boolean found = false;
			 metabup.metamodel.Feature mmF = null;
			 fragments.Feature fF = fnm.getFeatureNature().getFeature().getFeature();
			 
			 output = true;
			 
			 rationale = "\'" + objectName + "." + featureName + "\'";
			 
			 for(MetaClass mc : mm.getClasses()){
				  if(mc.getName().equals(className)){
					  for(Feature f : mc.getFeatures()){
						  if(f.getName().equals(featureName)){
							  mmF = f;
							  found = true;
						  }
					  }
				  }
			 }
			 
			 if(!found){
				 rationale += " doesn't even exist in the metamodel.";
				 return output = false;
			 }
			 
			 if(mmF instanceof metabup.metamodel.Attribute && fF instanceof fragments.Attribute){
				 rationale += " is indeed an attribute";
				 return output = false;
			 }
			 
			 if(mmF instanceof metabup.metamodel.Reference && fF instanceof fragments.Reference){
				 rationale += " is indeed a reference";
				 return output = false;
			 }
			 
			 if(mmF instanceof metabup.metamodel.Reference){
				 rationale += " should be a reference";
				 return output = true;
			 }
			 
			 if(mmF instanceof metabup.metamodel.Attribute){
				 rationale += " should be an attribute";
				 return output = true;
			 }
		 }
		 
		 if(c instanceof RestrictionViolation){
			 RestrictionViolation rv = (RestrictionViolation)c;
			 String restrictionName = rv.getRestrictionName();
			 AnnotatedElement ae = null;
			 TypedElement te = null;
			 
			 if(rv.getFragmentElement() instanceof ObjectExpression){
					ObjectExpression oe = (ObjectExpression) rv.getFragmentElement();															
					String className = oe.getObject().getType();
					
					for(MetaClass mc : mm.getClasses()){
						if(mc.getName().equals(className)){
							for(metabup.metamodel.Annotation a : mc.getAnnotations()){
								if(a.getName().equals(restrictionName)){
									ae = mc;
									te = oe.getObject();
									break;
								}
							}
						}
					}
					
					if(ae == null){
						rationale = "The type of \'" + oe.getObject().getName() +"\' is not contrained by any \'@" + restrictionName + "\' restriction";
						return output = false;
					}
				} else if(rv.getFragmentElement() instanceof FeatureWithinObjectExpression){
					FeatureWithinObjectExpression fwoe = (FeatureWithinObjectExpression) rv.getFragmentElement();
					String className = fwoe.getObject().getType();
					String featureName = fwoe.getFeature().getName();
					
					for(MetaClass mc : mm.getClasses()){
						if(mc.getName().equals(className)){
							for(Feature f : mc.getFeatures()){
								if(f.getName().equals(featureName)){
									for(metabup.metamodel.Annotation a : f.getAnnotations()){
										if(a.getName().equals(restrictionName)){
											ae = f;
											te = fwoe.getFeature();
											break;
										}
									}
								}
							}
						}
					}
					
					if(ae == null){
						rationale = "\'" + className + "." + featureName + "\' is not constrained by any \'@" + restrictionName + "\' restriction";
						return output = false;
					}
				}
			 			 			 
			 try {								
				 AnnotationFactory af = new AnnotationFactory();
				 af.initializeFactory();
				 			 			 
				 Class<? extends Annotation> annotationClass = af.getAnnotation(restrictionName);
				 if (annotationClass == null){
					 rationale = "The restriction doesn't exist";
					 return output = false;
				 }
				 
				 Annotation ann = annotationClass.newInstance();				
				 fragments.FragmentModel fm = null;
				 
				 if(te instanceof fragments.Object){
					 fragments.Object o = (fragments.Object) te;
					 fragments.Fragment fr = (fragments.Fragment) o.eContainer();
					 fm = testModelToFragmentModel((test.TestModel)fr.eContainer());
				 }else if(te instanceof fragments.Feature){
					 fragments.Feature f = (fragments.Feature) te;
					 fragments.Object o = (fragments.Object) f.eContainer();
					 fm = testModelToFragmentModel((test.TestModel)o.eContainer());
				 }
				 
				 List<IOpenIssue> oil = (List<IOpenIssue>) ann.detectFragmentIssues(fm, ae);				 
				 if(oil != null && !oil.isEmpty()){
					 rationale = "The restriction indeed seems to be violated";
					 // this shall be on an hover or something (TODO)
					 return output = true;
				 }else{
					 rationale = "The restriction seems to be fulfilled since it triggers no issues";
					 return output = false;
				 }
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				rationale = "Problems found when treating the restriction";
				return output = false;
			}			 
		 }
		 
		 rationale = "No rationale";
		 return output = false;
	}
	
	private FragmentModel testModelToFragmentModel(TestModel tm){
		if(tm == null) return null;
		FragmentModel fm = fragments.FragmentsFactory.eINSTANCE.createFragmentModel();
		
		for(TestableFragment tf : tm.getTestableFragments()){
			Fragment f = EcoreUtil.copy((Fragment)tf);
			fm.getFragments().add(f);
		}
		
		return fm;
	}
	
	public String getRationale(){
		return this.rationale;
	}
	
	private TestableFragment getAssertionContainer(){
		if(assertion == null) return null;
		AssertionSet as = (AssertionSet)assertion.eContainer();
		TestableFragment tf = (TestableFragment)as.eContainer();
		return tf;
	}
}
