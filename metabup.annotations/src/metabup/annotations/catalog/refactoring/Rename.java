/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.refactoring;

import metabup.annotations.extensionpoint.RefactoringAnnotation;
import metabup.annotations.general.Annotation;
import metabup.metamodel.Attribute;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.ParamValue;
import metabup.metamodel.Reference;
import metabup.metamodel.StringValue;

public class Rename extends RefactoringAnnotation {
	
	public Rename(){
		super.setName("rename");
		super.setDescription("It sets an element's name");
	}
	
	@Override
	public MetaModel perform() {		
		ParamValue namePV = super.getAnnotationParamValueByParamName("name");		
		if(namePV instanceof StringValue) name = ((StringValue) namePV).getValue();
		else{
			super.putError("The \'name\' parameter must be a string value");
			return null;
		}
		
		if(name == null){
			super.putError("The new name must be specified via \'name\' parameter");
			return null;
		}
		
		if(element instanceof MetaClass){			
			MetaClass mc = (MetaClass) element;		
			mc.setName(name);
			return targetMetaModel;		
		}	
		
		if(element instanceof Feature){			
			Feature f = (Feature)element;
			f.setName(name);
			return targetMetaModel;			
		}
		
		else return null;		
	}

}
