package metabup.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import metabup.services.FragmentsGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalFragmentsParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'ref'", "'rel'", "'true'", "'false'", "'positive'", "'negative'", "'line'", "'dashed'", "'dashed-dotted'", "'dotted'", "'circle'", "'concave'", "'convex'", "'crows-foot-many'", "'crows-foot-many-mandatory'", "'crows-foot-many-optional'", "'crows-foot-one'", "'crows-foot-one-mandatory'", "'crows-foot-one-optional'", "'dash'", "'delta'", "'diamond'", "'none'", "'plain'", "'short'", "'skewed-dash'", "'standard'", "'transparent-circle'", "'t-shape'", "'white-delta'", "'white-diamond'", "'model'", "'fragment'", "'{'", "'}'", "'['", "']'", "':'", "';'", "'@'", "'('", "')'", "','", "'->'", "'.'", "'='", "'attr'", "'style'", "'color'", "'width'", "'source'", "'target'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__62=62;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalFragmentsParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalFragmentsParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalFragmentsParser.tokenNames; }
    public String getGrammarFileName() { return "../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g"; }


     
     	private FragmentsGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(FragmentsGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleFragmentModel"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:60:1: entryRuleFragmentModel : ruleFragmentModel EOF ;
    public final void entryRuleFragmentModel() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:61:1: ( ruleFragmentModel EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:62:1: ruleFragmentModel EOF
            {
             before(grammarAccess.getFragmentModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentModel_in_entryRuleFragmentModel61);
            ruleFragmentModel();

            state._fsp--;

             after(grammarAccess.getFragmentModelRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragmentModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFragmentModel"


    // $ANTLR start "ruleFragmentModel"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:69:1: ruleFragmentModel : ( ( rule__FragmentModel__Group__0 ) ) ;
    public final void ruleFragmentModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:73:2: ( ( ( rule__FragmentModel__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:74:1: ( ( rule__FragmentModel__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:74:1: ( ( rule__FragmentModel__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:75:1: ( rule__FragmentModel__Group__0 )
            {
             before(grammarAccess.getFragmentModelAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:76:1: ( rule__FragmentModel__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:76:2: rule__FragmentModel__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__0_in_ruleFragmentModel94);
            rule__FragmentModel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFragmentModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFragmentModel"


    // $ANTLR start "entryRuleFragment"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:88:1: entryRuleFragment : ruleFragment EOF ;
    public final void entryRuleFragment() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:89:1: ( ruleFragment EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:90:1: ruleFragment EOF
            {
             before(grammarAccess.getFragmentRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragment_in_entryRuleFragment121);
            ruleFragment();

            state._fsp--;

             after(grammarAccess.getFragmentRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFragment128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFragment"


    // $ANTLR start "ruleFragment"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:97:1: ruleFragment : ( ( rule__Fragment__Group__0 ) ) ;
    public final void ruleFragment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:101:2: ( ( ( rule__Fragment__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:102:1: ( ( rule__Fragment__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:102:1: ( ( rule__Fragment__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:103:1: ( rule__Fragment__Group__0 )
            {
             before(grammarAccess.getFragmentAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:104:1: ( rule__Fragment__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:104:2: rule__Fragment__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__0_in_ruleFragment154);
            rule__Fragment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFragment"


    // $ANTLR start "entryRuleFeature"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:116:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:117:1: ( ruleFeature EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:118:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_entryRuleFeature181);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeature188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:125:1: ruleFeature : ( ruleAttrOrReference ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:129:2: ( ( ruleAttrOrReference ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:130:1: ( ruleAttrOrReference )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:130:1: ( ruleAttrOrReference )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:131:1: ruleAttrOrReference
            {
             before(grammarAccess.getFeatureAccess().getAttrOrReferenceParserRuleCall()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttrOrReference_in_ruleFeature214);
            ruleAttrOrReference();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getAttrOrReferenceParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleAttrOrReference"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:144:1: entryRuleAttrOrReference : ruleAttrOrReference EOF ;
    public final void entryRuleAttrOrReference() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:145:1: ( ruleAttrOrReference EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:146:1: ruleAttrOrReference EOF
            {
             before(grammarAccess.getAttrOrReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttrOrReference_in_entryRuleAttrOrReference240);
            ruleAttrOrReference();

            state._fsp--;

             after(grammarAccess.getAttrOrReferenceRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttrOrReference247); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttrOrReference"


    // $ANTLR start "ruleAttrOrReference"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:153:1: ruleAttrOrReference : ( ( rule__AttrOrReference__Alternatives ) ) ;
    public final void ruleAttrOrReference() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:157:2: ( ( ( rule__AttrOrReference__Alternatives ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:158:1: ( ( rule__AttrOrReference__Alternatives ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:158:1: ( ( rule__AttrOrReference__Alternatives ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:159:1: ( rule__AttrOrReference__Alternatives )
            {
             before(grammarAccess.getAttrOrReferenceAccess().getAlternatives()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:160:1: ( rule__AttrOrReference__Alternatives )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:160:2: rule__AttrOrReference__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__AttrOrReference__Alternatives_in_ruleAttrOrReference273);
            rule__AttrOrReference__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAttrOrReferenceAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttrOrReference"


    // $ANTLR start "entryRuleEString"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:172:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:173:1: ( ruleEString EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:174:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString300);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString307); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:181:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:185:2: ( ( ( rule__EString__Alternatives ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:186:1: ( ( rule__EString__Alternatives ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:186:1: ( ( rule__EString__Alternatives ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:187:1: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:188:1: ( rule__EString__Alternatives )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:188:2: rule__EString__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__EString__Alternatives_in_ruleEString333);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleObject"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:200:1: entryRuleObject : ruleObject EOF ;
    public final void entryRuleObject() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:201:1: ( ruleObject EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:202:1: ruleObject EOF
            {
             before(grammarAccess.getObjectRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_entryRuleObject360);
            ruleObject();

            state._fsp--;

             after(grammarAccess.getObjectRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObject367); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObject"


    // $ANTLR start "ruleObject"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:209:1: ruleObject : ( ( rule__Object__Group__0 ) ) ;
    public final void ruleObject() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:213:2: ( ( ( rule__Object__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:214:1: ( ( rule__Object__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:214:1: ( ( rule__Object__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:215:1: ( rule__Object__Group__0 )
            {
             before(grammarAccess.getObjectAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:216:1: ( rule__Object__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:216:2: rule__Object__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__0_in_ruleObject393);
            rule__Object__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObject"


    // $ANTLR start "entryRuleAnnotation"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:228:1: entryRuleAnnotation : ruleAnnotation EOF ;
    public final void entryRuleAnnotation() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:229:1: ( ruleAnnotation EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:230:1: ruleAnnotation EOF
            {
             before(grammarAccess.getAnnotationRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_entryRuleAnnotation420);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getAnnotationRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotation427); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnnotation"


    // $ANTLR start "ruleAnnotation"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:237:1: ruleAnnotation : ( ( rule__Annotation__Group__0 ) ) ;
    public final void ruleAnnotation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:241:2: ( ( ( rule__Annotation__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:242:1: ( ( rule__Annotation__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:242:1: ( ( rule__Annotation__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:243:1: ( rule__Annotation__Group__0 )
            {
             before(grammarAccess.getAnnotationAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:244:1: ( rule__Annotation__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:244:2: rule__Annotation__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__0_in_ruleAnnotation453);
            rule__Annotation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnnotation"


    // $ANTLR start "entryRuleAnnotationParam"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:256:1: entryRuleAnnotationParam : ruleAnnotationParam EOF ;
    public final void entryRuleAnnotationParam() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:257:1: ( ruleAnnotationParam EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:258:1: ruleAnnotationParam EOF
            {
             before(grammarAccess.getAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam480);
            ruleAnnotationParam();

            state._fsp--;

             after(grammarAccess.getAnnotationParamRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAnnotationParam487); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnnotationParam"


    // $ANTLR start "ruleAnnotationParam"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:265:1: ruleAnnotationParam : ( ( rule__AnnotationParam__Group__0 ) ) ;
    public final void ruleAnnotationParam() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:269:2: ( ( ( rule__AnnotationParam__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:270:1: ( ( rule__AnnotationParam__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:270:1: ( ( rule__AnnotationParam__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:271:1: ( rule__AnnotationParam__Group__0 )
            {
             before(grammarAccess.getAnnotationParamAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:272:1: ( rule__AnnotationParam__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:272:2: rule__AnnotationParam__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__Group__0_in_ruleAnnotationParam513);
            rule__AnnotationParam__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationParamAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnnotationParam"


    // $ANTLR start "entryRuleParamValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:284:1: entryRuleParamValue : ruleParamValue EOF ;
    public final void entryRuleParamValue() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:285:1: ( ruleParamValue EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:286:1: ruleParamValue EOF
            {
             before(grammarAccess.getParamValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_entryRuleParamValue540);
            ruleParamValue();

            state._fsp--;

             after(grammarAccess.getParamValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleParamValue547); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParamValue"


    // $ANTLR start "ruleParamValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:293:1: ruleParamValue : ( ( rule__ParamValue__Alternatives ) ) ;
    public final void ruleParamValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:297:2: ( ( ( rule__ParamValue__Alternatives ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:298:1: ( ( rule__ParamValue__Alternatives ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:298:1: ( ( rule__ParamValue__Alternatives ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:299:1: ( rule__ParamValue__Alternatives )
            {
             before(grammarAccess.getParamValueAccess().getAlternatives()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:300:1: ( rule__ParamValue__Alternatives )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:300:2: rule__ParamValue__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__ParamValue__Alternatives_in_ruleParamValue573);
            rule__ParamValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getParamValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParamValue"


    // $ANTLR start "entryRuleObjectValueAnnotationParam"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:312:1: entryRuleObjectValueAnnotationParam : ruleObjectValueAnnotationParam EOF ;
    public final void entryRuleObjectValueAnnotationParam() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:313:1: ( ruleObjectValueAnnotationParam EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:314:1: ruleObjectValueAnnotationParam EOF
            {
             before(grammarAccess.getObjectValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam600);
            ruleObjectValueAnnotationParam();

            state._fsp--;

             after(grammarAccess.getObjectValueAnnotationParamRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam607); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObjectValueAnnotationParam"


    // $ANTLR start "ruleObjectValueAnnotationParam"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:321:1: ruleObjectValueAnnotationParam : ( ( rule__ObjectValueAnnotationParam__Group__0 ) ) ;
    public final void ruleObjectValueAnnotationParam() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:325:2: ( ( ( rule__ObjectValueAnnotationParam__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:326:1: ( ( rule__ObjectValueAnnotationParam__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:326:1: ( ( rule__ObjectValueAnnotationParam__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:327:1: ( rule__ObjectValueAnnotationParam__Group__0 )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:328:1: ( rule__ObjectValueAnnotationParam__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:328:2: rule__ObjectValueAnnotationParam__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__0_in_ruleObjectValueAnnotationParam633);
            rule__ObjectValueAnnotationParam__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjectValueAnnotationParam"


    // $ANTLR start "entryRulePrimitiveValueAnnotationParam"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:340:1: entryRulePrimitiveValueAnnotationParam : rulePrimitiveValueAnnotationParam EOF ;
    public final void entryRulePrimitiveValueAnnotationParam() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:341:1: ( rulePrimitiveValueAnnotationParam EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:342:1: rulePrimitiveValueAnnotationParam EOF
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam660);
            rulePrimitiveValueAnnotationParam();

            state._fsp--;

             after(grammarAccess.getPrimitiveValueAnnotationParamRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam667); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimitiveValueAnnotationParam"


    // $ANTLR start "rulePrimitiveValueAnnotationParam"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:349:1: rulePrimitiveValueAnnotationParam : ( ( rule__PrimitiveValueAnnotationParam__Group__0 ) ) ;
    public final void rulePrimitiveValueAnnotationParam() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:353:2: ( ( ( rule__PrimitiveValueAnnotationParam__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:354:1: ( ( rule__PrimitiveValueAnnotationParam__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:354:1: ( ( rule__PrimitiveValueAnnotationParam__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:355:1: ( rule__PrimitiveValueAnnotationParam__Group__0 )
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:356:1: ( rule__PrimitiveValueAnnotationParam__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:356:2: rule__PrimitiveValueAnnotationParam__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__Group__0_in_rulePrimitiveValueAnnotationParam693);
            rule__PrimitiveValueAnnotationParam__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPrimitiveValueAnnotationParamAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimitiveValueAnnotationParam"


    // $ANTLR start "entryRuleAttribute"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:368:1: entryRuleAttribute : ruleAttribute EOF ;
    public final void entryRuleAttribute() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:369:1: ( ruleAttribute EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:370:1: ruleAttribute EOF
            {
             before(grammarAccess.getAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_entryRuleAttribute720);
            ruleAttribute();

            state._fsp--;

             after(grammarAccess.getAttributeRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttribute727); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:377:1: ruleAttribute : ( ( rule__Attribute__Group__0 ) ) ;
    public final void ruleAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:381:2: ( ( ( rule__Attribute__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:382:1: ( ( rule__Attribute__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:382:1: ( ( rule__Attribute__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:383:1: ( rule__Attribute__Group__0 )
            {
             before(grammarAccess.getAttributeAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:384:1: ( rule__Attribute__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:384:2: rule__Attribute__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__0_in_ruleAttribute753);
            rule__Attribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleReference"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:396:1: entryRuleReference : ruleReference EOF ;
    public final void entryRuleReference() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:397:1: ( ruleReference EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:398:1: ruleReference EOF
            {
             before(grammarAccess.getReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReference_in_entryRuleReference780);
            ruleReference();

            state._fsp--;

             after(grammarAccess.getReferenceRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReference787); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:405:1: ruleReference : ( ( rule__Reference__Group__0 ) ) ;
    public final void ruleReference() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:409:2: ( ( ( rule__Reference__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:410:1: ( ( rule__Reference__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:410:1: ( ( rule__Reference__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:411:1: ( rule__Reference__Group__0 )
            {
             before(grammarAccess.getReferenceAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:412:1: ( rule__Reference__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:412:2: rule__Reference__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__0_in_ruleReference813);
            rule__Reference__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleEdgeProperties"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:424:1: entryRuleEdgeProperties : ruleEdgeProperties EOF ;
    public final void entryRuleEdgeProperties() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:425:1: ( ruleEdgeProperties EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:426:1: ruleEdgeProperties EOF
            {
             before(grammarAccess.getEdgePropertiesRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEdgeProperties_in_entryRuleEdgeProperties840);
            ruleEdgeProperties();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEdgeProperties847); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEdgeProperties"


    // $ANTLR start "ruleEdgeProperties"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:433:1: ruleEdgeProperties : ( ( rule__EdgeProperties__Group__0 ) ) ;
    public final void ruleEdgeProperties() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:437:2: ( ( ( rule__EdgeProperties__Group__0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:438:1: ( ( rule__EdgeProperties__Group__0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:438:1: ( ( rule__EdgeProperties__Group__0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:439:1: ( rule__EdgeProperties__Group__0 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getGroup()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:440:1: ( rule__EdgeProperties__Group__0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:440:2: rule__EdgeProperties__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__0_in_ruleEdgeProperties873);
            rule__EdgeProperties__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEdgeProperties"


    // $ANTLR start "entryRulePrimitiveValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:452:1: entryRulePrimitiveValue : rulePrimitiveValue EOF ;
    public final void entryRulePrimitiveValue() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:453:1: ( rulePrimitiveValue EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:454:1: rulePrimitiveValue EOF
            {
             before(grammarAccess.getPrimitiveValueRule()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue900);
            rulePrimitiveValue();

            state._fsp--;

             after(grammarAccess.getPrimitiveValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRulePrimitiveValue907); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePrimitiveValue"


    // $ANTLR start "rulePrimitiveValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:461:1: rulePrimitiveValue : ( ( rule__PrimitiveValue__Alternatives ) ) ;
    public final void rulePrimitiveValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:465:2: ( ( ( rule__PrimitiveValue__Alternatives ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:466:1: ( ( rule__PrimitiveValue__Alternatives ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:466:1: ( ( rule__PrimitiveValue__Alternatives ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:467:1: ( rule__PrimitiveValue__Alternatives )
            {
             before(grammarAccess.getPrimitiveValueAccess().getAlternatives()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:468:1: ( rule__PrimitiveValue__Alternatives )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:468:2: rule__PrimitiveValue__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValue__Alternatives_in_rulePrimitiveValue933);
            rule__PrimitiveValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPrimitiveValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePrimitiveValue"


    // $ANTLR start "entryRuleIntegerValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:480:1: entryRuleIntegerValue : ruleIntegerValue EOF ;
    public final void entryRuleIntegerValue() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:481:1: ( ruleIntegerValue EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:482:1: ruleIntegerValue EOF
            {
             before(grammarAccess.getIntegerValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue960);
            ruleIntegerValue();

            state._fsp--;

             after(grammarAccess.getIntegerValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleIntegerValue967); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIntegerValue"


    // $ANTLR start "ruleIntegerValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:489:1: ruleIntegerValue : ( ( rule__IntegerValue__ValueAssignment ) ) ;
    public final void ruleIntegerValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:493:2: ( ( ( rule__IntegerValue__ValueAssignment ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:494:1: ( ( rule__IntegerValue__ValueAssignment ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:494:1: ( ( rule__IntegerValue__ValueAssignment ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:495:1: ( rule__IntegerValue__ValueAssignment )
            {
             before(grammarAccess.getIntegerValueAccess().getValueAssignment()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:496:1: ( rule__IntegerValue__ValueAssignment )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:496:2: rule__IntegerValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__IntegerValue__ValueAssignment_in_ruleIntegerValue993);
            rule__IntegerValue__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getIntegerValueAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIntegerValue"


    // $ANTLR start "entryRuleStringValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:508:1: entryRuleStringValue : ruleStringValue EOF ;
    public final void entryRuleStringValue() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:509:1: ( ruleStringValue EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:510:1: ruleStringValue EOF
            {
             before(grammarAccess.getStringValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_entryRuleStringValue1020);
            ruleStringValue();

            state._fsp--;

             after(grammarAccess.getStringValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleStringValue1027); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringValue"


    // $ANTLR start "ruleStringValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:517:1: ruleStringValue : ( ( rule__StringValue__ValueAssignment ) ) ;
    public final void ruleStringValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:521:2: ( ( ( rule__StringValue__ValueAssignment ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:522:1: ( ( rule__StringValue__ValueAssignment ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:522:1: ( ( rule__StringValue__ValueAssignment ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:523:1: ( rule__StringValue__ValueAssignment )
            {
             before(grammarAccess.getStringValueAccess().getValueAssignment()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:524:1: ( rule__StringValue__ValueAssignment )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:524:2: rule__StringValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__StringValue__ValueAssignment_in_ruleStringValue1053);
            rule__StringValue__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getStringValueAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringValue"


    // $ANTLR start "entryRuleBooleanValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:536:1: entryRuleBooleanValue : ruleBooleanValue EOF ;
    public final void entryRuleBooleanValue() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:537:1: ( ruleBooleanValue EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:538:1: ruleBooleanValue EOF
            {
             before(grammarAccess.getBooleanValueRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue1080);
            ruleBooleanValue();

            state._fsp--;

             after(grammarAccess.getBooleanValueRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValue1087); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanValue"


    // $ANTLR start "ruleBooleanValue"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:545:1: ruleBooleanValue : ( ( rule__BooleanValue__ValueAssignment ) ) ;
    public final void ruleBooleanValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:549:2: ( ( ( rule__BooleanValue__ValueAssignment ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:550:1: ( ( rule__BooleanValue__ValueAssignment ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:550:1: ( ( rule__BooleanValue__ValueAssignment ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:551:1: ( rule__BooleanValue__ValueAssignment )
            {
             before(grammarAccess.getBooleanValueAccess().getValueAssignment()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:552:1: ( rule__BooleanValue__ValueAssignment )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:552:2: rule__BooleanValue__ValueAssignment
            {
            pushFollow(FollowSets000.FOLLOW_rule__BooleanValue__ValueAssignment_in_ruleBooleanValue1113);
            rule__BooleanValue__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getBooleanValueAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanValue"


    // $ANTLR start "entryRuleBooleanValueTerminal"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:564:1: entryRuleBooleanValueTerminal : ruleBooleanValueTerminal EOF ;
    public final void entryRuleBooleanValueTerminal() throws RecognitionException {
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:565:1: ( ruleBooleanValueTerminal EOF )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:566:1: ruleBooleanValueTerminal EOF
            {
             before(grammarAccess.getBooleanValueTerminalRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal1140);
            ruleBooleanValueTerminal();

            state._fsp--;

             after(grammarAccess.getBooleanValueTerminalRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleBooleanValueTerminal1147); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanValueTerminal"


    // $ANTLR start "ruleBooleanValueTerminal"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:573:1: ruleBooleanValueTerminal : ( ( rule__BooleanValueTerminal__Alternatives ) ) ;
    public final void ruleBooleanValueTerminal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:577:2: ( ( ( rule__BooleanValueTerminal__Alternatives ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:578:1: ( ( rule__BooleanValueTerminal__Alternatives ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:578:1: ( ( rule__BooleanValueTerminal__Alternatives ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:579:1: ( rule__BooleanValueTerminal__Alternatives )
            {
             before(grammarAccess.getBooleanValueTerminalAccess().getAlternatives()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:580:1: ( rule__BooleanValueTerminal__Alternatives )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:580:2: rule__BooleanValueTerminal__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__BooleanValueTerminal__Alternatives_in_ruleBooleanValueTerminal1173);
            rule__BooleanValueTerminal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBooleanValueTerminalAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanValueTerminal"


    // $ANTLR start "ruleFragmentType"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:593:1: ruleFragmentType : ( ( rule__FragmentType__Alternatives ) ) ;
    public final void ruleFragmentType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:597:1: ( ( ( rule__FragmentType__Alternatives ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:598:1: ( ( rule__FragmentType__Alternatives ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:598:1: ( ( rule__FragmentType__Alternatives ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:599:1: ( rule__FragmentType__Alternatives )
            {
             before(grammarAccess.getFragmentTypeAccess().getAlternatives()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:600:1: ( rule__FragmentType__Alternatives )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:600:2: rule__FragmentType__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentType__Alternatives_in_ruleFragmentType1210);
            rule__FragmentType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFragmentTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFragmentType"


    // $ANTLR start "ruleLineType"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:612:1: ruleLineType : ( ( rule__LineType__Alternatives ) ) ;
    public final void ruleLineType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:616:1: ( ( ( rule__LineType__Alternatives ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:617:1: ( ( rule__LineType__Alternatives ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:617:1: ( ( rule__LineType__Alternatives ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:618:1: ( rule__LineType__Alternatives )
            {
             before(grammarAccess.getLineTypeAccess().getAlternatives()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:619:1: ( rule__LineType__Alternatives )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:619:2: rule__LineType__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__LineType__Alternatives_in_ruleLineType1246);
            rule__LineType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLineTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLineType"


    // $ANTLR start "ruleArrowType"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:631:1: ruleArrowType : ( ( rule__ArrowType__Alternatives ) ) ;
    public final void ruleArrowType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:635:1: ( ( ( rule__ArrowType__Alternatives ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:636:1: ( ( rule__ArrowType__Alternatives ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:636:1: ( ( rule__ArrowType__Alternatives ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:637:1: ( rule__ArrowType__Alternatives )
            {
             before(grammarAccess.getArrowTypeAccess().getAlternatives()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:638:1: ( rule__ArrowType__Alternatives )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:638:2: rule__ArrowType__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__ArrowType__Alternatives_in_ruleArrowType1282);
            rule__ArrowType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getArrowTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleArrowType"


    // $ANTLR start "rule__AttrOrReference__Alternatives"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:649:1: rule__AttrOrReference__Alternatives : ( ( ruleAttribute ) | ( ruleReference ) );
    public final void rule__AttrOrReference__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:653:1: ( ( ruleAttribute ) | ( ruleReference ) )
            int alt1=2;
            alt1 = dfa1.predict(input);
            switch (alt1) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:654:1: ( ruleAttribute )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:654:1: ( ruleAttribute )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:655:1: ruleAttribute
                    {
                     before(grammarAccess.getAttrOrReferenceAccess().getAttributeParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_rule__AttrOrReference__Alternatives1317);
                    ruleAttribute();

                    state._fsp--;

                     after(grammarAccess.getAttrOrReferenceAccess().getAttributeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:660:6: ( ruleReference )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:660:6: ( ruleReference )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:661:1: ruleReference
                    {
                     before(grammarAccess.getAttrOrReferenceAccess().getReferenceParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleReference_in_rule__AttrOrReference__Alternatives1334);
                    ruleReference();

                    state._fsp--;

                     after(grammarAccess.getAttrOrReferenceAccess().getReferenceParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AttrOrReference__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:671:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:675:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:676:1: ( RULE_STRING )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:676:1: ( RULE_STRING )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:677:1: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__EString__Alternatives1366); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:682:6: ( RULE_ID )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:682:6: ( RULE_ID )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:683:1: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_rule__EString__Alternatives1383); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__ParamValue__Alternatives"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:693:1: rule__ParamValue__Alternatives : ( ( rulePrimitiveValueAnnotationParam ) | ( ruleObjectValueAnnotationParam ) );
    public final void rule__ParamValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:697:1: ( ( rulePrimitiveValueAnnotationParam ) | ( ruleObjectValueAnnotationParam ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==56) ) {
                alt3=1;
            }
            else if ( (LA3_0==54) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:698:1: ( rulePrimitiveValueAnnotationParam )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:698:1: ( rulePrimitiveValueAnnotationParam )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:699:1: rulePrimitiveValueAnnotationParam
                    {
                     before(grammarAccess.getParamValueAccess().getPrimitiveValueAnnotationParamParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_rulePrimitiveValueAnnotationParam_in_rule__ParamValue__Alternatives1415);
                    rulePrimitiveValueAnnotationParam();

                    state._fsp--;

                     after(grammarAccess.getParamValueAccess().getPrimitiveValueAnnotationParamParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:704:6: ( ruleObjectValueAnnotationParam )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:704:6: ( ruleObjectValueAnnotationParam )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:705:1: ruleObjectValueAnnotationParam
                    {
                     before(grammarAccess.getParamValueAccess().getObjectValueAnnotationParamParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleObjectValueAnnotationParam_in_rule__ParamValue__Alternatives1432);
                    ruleObjectValueAnnotationParam();

                    state._fsp--;

                     after(grammarAccess.getParamValueAccess().getObjectValueAnnotationParamParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParamValue__Alternatives"


    // $ANTLR start "rule__Reference__Alternatives_3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:715:1: rule__Reference__Alternatives_3 : ( ( 'ref' ) | ( 'rel' ) );
    public final void rule__Reference__Alternatives_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:719:1: ( ( 'ref' ) | ( 'rel' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==11) ) {
                alt4=1;
            }
            else if ( (LA4_0==12) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:720:1: ( 'ref' )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:720:1: ( 'ref' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:721:1: 'ref'
                    {
                     before(grammarAccess.getReferenceAccess().getRefKeyword_3_0()); 
                    match(input,11,FollowSets000.FOLLOW_11_in_rule__Reference__Alternatives_31465); 
                     after(grammarAccess.getReferenceAccess().getRefKeyword_3_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:728:6: ( 'rel' )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:728:6: ( 'rel' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:729:1: 'rel'
                    {
                     before(grammarAccess.getReferenceAccess().getRelKeyword_3_1()); 
                    match(input,12,FollowSets000.FOLLOW_12_in_rule__Reference__Alternatives_31485); 
                     after(grammarAccess.getReferenceAccess().getRelKeyword_3_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Alternatives_3"


    // $ANTLR start "rule__PrimitiveValue__Alternatives"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:741:1: rule__PrimitiveValue__Alternatives : ( ( ruleStringValue ) | ( ruleIntegerValue ) | ( ruleBooleanValue ) );
    public final void rule__PrimitiveValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:745:1: ( ( ruleStringValue ) | ( ruleIntegerValue ) | ( ruleBooleanValue ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                alt5=1;
                }
                break;
            case RULE_INT:
                {
                alt5=2;
                }
                break;
            case 13:
            case 14:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:746:1: ( ruleStringValue )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:746:1: ( ruleStringValue )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:747:1: ruleStringValue
                    {
                     before(grammarAccess.getPrimitiveValueAccess().getStringValueParserRuleCall_0()); 
                    pushFollow(FollowSets000.FOLLOW_ruleStringValue_in_rule__PrimitiveValue__Alternatives1519);
                    ruleStringValue();

                    state._fsp--;

                     after(grammarAccess.getPrimitiveValueAccess().getStringValueParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:752:6: ( ruleIntegerValue )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:752:6: ( ruleIntegerValue )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:753:1: ruleIntegerValue
                    {
                     before(grammarAccess.getPrimitiveValueAccess().getIntegerValueParserRuleCall_1()); 
                    pushFollow(FollowSets000.FOLLOW_ruleIntegerValue_in_rule__PrimitiveValue__Alternatives1536);
                    ruleIntegerValue();

                    state._fsp--;

                     after(grammarAccess.getPrimitiveValueAccess().getIntegerValueParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:758:6: ( ruleBooleanValue )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:758:6: ( ruleBooleanValue )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:759:1: ruleBooleanValue
                    {
                     before(grammarAccess.getPrimitiveValueAccess().getBooleanValueParserRuleCall_2()); 
                    pushFollow(FollowSets000.FOLLOW_ruleBooleanValue_in_rule__PrimitiveValue__Alternatives1553);
                    ruleBooleanValue();

                    state._fsp--;

                     after(grammarAccess.getPrimitiveValueAccess().getBooleanValueParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValue__Alternatives"


    // $ANTLR start "rule__BooleanValueTerminal__Alternatives"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:769:1: rule__BooleanValueTerminal__Alternatives : ( ( 'true' ) | ( 'false' ) );
    public final void rule__BooleanValueTerminal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:773:1: ( ( 'true' ) | ( 'false' ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==13) ) {
                alt6=1;
            }
            else if ( (LA6_0==14) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:774:1: ( 'true' )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:774:1: ( 'true' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:775:1: 'true'
                    {
                     before(grammarAccess.getBooleanValueTerminalAccess().getTrueKeyword_0()); 
                    match(input,13,FollowSets000.FOLLOW_13_in_rule__BooleanValueTerminal__Alternatives1586); 
                     after(grammarAccess.getBooleanValueTerminalAccess().getTrueKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:782:6: ( 'false' )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:782:6: ( 'false' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:783:1: 'false'
                    {
                     before(grammarAccess.getBooleanValueTerminalAccess().getFalseKeyword_1()); 
                    match(input,14,FollowSets000.FOLLOW_14_in_rule__BooleanValueTerminal__Alternatives1606); 
                     after(grammarAccess.getBooleanValueTerminalAccess().getFalseKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanValueTerminal__Alternatives"


    // $ANTLR start "rule__FragmentType__Alternatives"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:795:1: rule__FragmentType__Alternatives : ( ( ( 'positive' ) ) | ( ( 'negative' ) ) );
    public final void rule__FragmentType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:799:1: ( ( ( 'positive' ) ) | ( ( 'negative' ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==15) ) {
                alt7=1;
            }
            else if ( (LA7_0==16) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:800:1: ( ( 'positive' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:800:1: ( ( 'positive' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:801:1: ( 'positive' )
                    {
                     before(grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:802:1: ( 'positive' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:802:3: 'positive'
                    {
                    match(input,15,FollowSets000.FOLLOW_15_in_rule__FragmentType__Alternatives1641); 

                    }

                     after(grammarAccess.getFragmentTypeAccess().getPositiveEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:807:6: ( ( 'negative' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:807:6: ( ( 'negative' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:808:1: ( 'negative' )
                    {
                     before(grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:809:1: ( 'negative' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:809:3: 'negative'
                    {
                    match(input,16,FollowSets000.FOLLOW_16_in_rule__FragmentType__Alternatives1662); 

                    }

                     after(grammarAccess.getFragmentTypeAccess().getNegativeEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentType__Alternatives"


    // $ANTLR start "rule__LineType__Alternatives"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:819:1: rule__LineType__Alternatives : ( ( ( 'line' ) ) | ( ( 'dashed' ) ) | ( ( 'dashed-dotted' ) ) | ( ( 'dotted' ) ) );
    public final void rule__LineType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:823:1: ( ( ( 'line' ) ) | ( ( 'dashed' ) ) | ( ( 'dashed-dotted' ) ) | ( ( 'dotted' ) ) )
            int alt8=4;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt8=1;
                }
                break;
            case 18:
                {
                alt8=2;
                }
                break;
            case 19:
                {
                alt8=3;
                }
                break;
            case 20:
                {
                alt8=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:824:1: ( ( 'line' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:824:1: ( ( 'line' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:825:1: ( 'line' )
                    {
                     before(grammarAccess.getLineTypeAccess().getLineEnumLiteralDeclaration_0()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:826:1: ( 'line' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:826:3: 'line'
                    {
                    match(input,17,FollowSets000.FOLLOW_17_in_rule__LineType__Alternatives1698); 

                    }

                     after(grammarAccess.getLineTypeAccess().getLineEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:831:6: ( ( 'dashed' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:831:6: ( ( 'dashed' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:832:1: ( 'dashed' )
                    {
                     before(grammarAccess.getLineTypeAccess().getDashedEnumLiteralDeclaration_1()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:833:1: ( 'dashed' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:833:3: 'dashed'
                    {
                    match(input,18,FollowSets000.FOLLOW_18_in_rule__LineType__Alternatives1719); 

                    }

                     after(grammarAccess.getLineTypeAccess().getDashedEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:838:6: ( ( 'dashed-dotted' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:838:6: ( ( 'dashed-dotted' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:839:1: ( 'dashed-dotted' )
                    {
                     before(grammarAccess.getLineTypeAccess().getDashedDottedEnumLiteralDeclaration_2()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:840:1: ( 'dashed-dotted' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:840:3: 'dashed-dotted'
                    {
                    match(input,19,FollowSets000.FOLLOW_19_in_rule__LineType__Alternatives1740); 

                    }

                     after(grammarAccess.getLineTypeAccess().getDashedDottedEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:845:6: ( ( 'dotted' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:845:6: ( ( 'dotted' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:846:1: ( 'dotted' )
                    {
                     before(grammarAccess.getLineTypeAccess().getDottedEnumLiteralDeclaration_3()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:847:1: ( 'dotted' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:847:3: 'dotted'
                    {
                    match(input,20,FollowSets000.FOLLOW_20_in_rule__LineType__Alternatives1761); 

                    }

                     after(grammarAccess.getLineTypeAccess().getDottedEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LineType__Alternatives"


    // $ANTLR start "rule__ArrowType__Alternatives"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:857:1: rule__ArrowType__Alternatives : ( ( ( 'circle' ) ) | ( ( 'concave' ) ) | ( ( 'convex' ) ) | ( ( 'crows-foot-many' ) ) | ( ( 'crows-foot-many-mandatory' ) ) | ( ( 'crows-foot-many-optional' ) ) | ( ( 'crows-foot-one' ) ) | ( ( 'crows-foot-one-mandatory' ) ) | ( ( 'crows-foot-one-optional' ) ) | ( ( 'dash' ) ) | ( ( 'delta' ) ) | ( ( 'diamond' ) ) | ( ( 'none' ) ) | ( ( 'plain' ) ) | ( ( 'short' ) ) | ( ( 'skewed-dash' ) ) | ( ( 'standard' ) ) | ( ( 'transparent-circle' ) ) | ( ( 't-shape' ) ) | ( ( 'white-delta' ) ) | ( ( 'white-diamond' ) ) );
    public final void rule__ArrowType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:861:1: ( ( ( 'circle' ) ) | ( ( 'concave' ) ) | ( ( 'convex' ) ) | ( ( 'crows-foot-many' ) ) | ( ( 'crows-foot-many-mandatory' ) ) | ( ( 'crows-foot-many-optional' ) ) | ( ( 'crows-foot-one' ) ) | ( ( 'crows-foot-one-mandatory' ) ) | ( ( 'crows-foot-one-optional' ) ) | ( ( 'dash' ) ) | ( ( 'delta' ) ) | ( ( 'diamond' ) ) | ( ( 'none' ) ) | ( ( 'plain' ) ) | ( ( 'short' ) ) | ( ( 'skewed-dash' ) ) | ( ( 'standard' ) ) | ( ( 'transparent-circle' ) ) | ( ( 't-shape' ) ) | ( ( 'white-delta' ) ) | ( ( 'white-diamond' ) ) )
            int alt9=21;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt9=1;
                }
                break;
            case 22:
                {
                alt9=2;
                }
                break;
            case 23:
                {
                alt9=3;
                }
                break;
            case 24:
                {
                alt9=4;
                }
                break;
            case 25:
                {
                alt9=5;
                }
                break;
            case 26:
                {
                alt9=6;
                }
                break;
            case 27:
                {
                alt9=7;
                }
                break;
            case 28:
                {
                alt9=8;
                }
                break;
            case 29:
                {
                alt9=9;
                }
                break;
            case 30:
                {
                alt9=10;
                }
                break;
            case 31:
                {
                alt9=11;
                }
                break;
            case 32:
                {
                alt9=12;
                }
                break;
            case 33:
                {
                alt9=13;
                }
                break;
            case 34:
                {
                alt9=14;
                }
                break;
            case 35:
                {
                alt9=15;
                }
                break;
            case 36:
                {
                alt9=16;
                }
                break;
            case 37:
                {
                alt9=17;
                }
                break;
            case 38:
                {
                alt9=18;
                }
                break;
            case 39:
                {
                alt9=19;
                }
                break;
            case 40:
                {
                alt9=20;
                }
                break;
            case 41:
                {
                alt9=21;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:862:1: ( ( 'circle' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:862:1: ( ( 'circle' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:863:1: ( 'circle' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCircleEnumLiteralDeclaration_0()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:864:1: ( 'circle' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:864:3: 'circle'
                    {
                    match(input,21,FollowSets000.FOLLOW_21_in_rule__ArrowType__Alternatives1797); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCircleEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:869:6: ( ( 'concave' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:869:6: ( ( 'concave' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:870:1: ( 'concave' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getConcaveEnumLiteralDeclaration_1()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:871:1: ( 'concave' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:871:3: 'concave'
                    {
                    match(input,22,FollowSets000.FOLLOW_22_in_rule__ArrowType__Alternatives1818); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getConcaveEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:876:6: ( ( 'convex' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:876:6: ( ( 'convex' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:877:1: ( 'convex' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getConvexEnumLiteralDeclaration_2()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:878:1: ( 'convex' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:878:3: 'convex'
                    {
                    match(input,23,FollowSets000.FOLLOW_23_in_rule__ArrowType__Alternatives1839); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getConvexEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:883:6: ( ( 'crows-foot-many' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:883:6: ( ( 'crows-foot-many' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:884:1: ( 'crows-foot-many' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootManyEnumLiteralDeclaration_3()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:885:1: ( 'crows-foot-many' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:885:3: 'crows-foot-many'
                    {
                    match(input,24,FollowSets000.FOLLOW_24_in_rule__ArrowType__Alternatives1860); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootManyEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:890:6: ( ( 'crows-foot-many-mandatory' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:890:6: ( ( 'crows-foot-many-mandatory' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:891:1: ( 'crows-foot-many-mandatory' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootManyMandatoryEnumLiteralDeclaration_4()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:892:1: ( 'crows-foot-many-mandatory' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:892:3: 'crows-foot-many-mandatory'
                    {
                    match(input,25,FollowSets000.FOLLOW_25_in_rule__ArrowType__Alternatives1881); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootManyMandatoryEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:897:6: ( ( 'crows-foot-many-optional' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:897:6: ( ( 'crows-foot-many-optional' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:898:1: ( 'crows-foot-many-optional' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootManyOptionalEnumLiteralDeclaration_5()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:899:1: ( 'crows-foot-many-optional' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:899:3: 'crows-foot-many-optional'
                    {
                    match(input,26,FollowSets000.FOLLOW_26_in_rule__ArrowType__Alternatives1902); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootManyOptionalEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:904:6: ( ( 'crows-foot-one' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:904:6: ( ( 'crows-foot-one' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:905:1: ( 'crows-foot-one' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootOneEnumLiteralDeclaration_6()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:906:1: ( 'crows-foot-one' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:906:3: 'crows-foot-one'
                    {
                    match(input,27,FollowSets000.FOLLOW_27_in_rule__ArrowType__Alternatives1923); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootOneEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:911:6: ( ( 'crows-foot-one-mandatory' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:911:6: ( ( 'crows-foot-one-mandatory' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:912:1: ( 'crows-foot-one-mandatory' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootOneMandatoryEnumLiteralDeclaration_7()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:913:1: ( 'crows-foot-one-mandatory' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:913:3: 'crows-foot-one-mandatory'
                    {
                    match(input,28,FollowSets000.FOLLOW_28_in_rule__ArrowType__Alternatives1944); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootOneMandatoryEnumLiteralDeclaration_7()); 

                    }


                    }
                    break;
                case 9 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:918:6: ( ( 'crows-foot-one-optional' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:918:6: ( ( 'crows-foot-one-optional' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:919:1: ( 'crows-foot-one-optional' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getCrowsFootOneOptionalEnumLiteralDeclaration_8()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:920:1: ( 'crows-foot-one-optional' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:920:3: 'crows-foot-one-optional'
                    {
                    match(input,29,FollowSets000.FOLLOW_29_in_rule__ArrowType__Alternatives1965); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getCrowsFootOneOptionalEnumLiteralDeclaration_8()); 

                    }


                    }
                    break;
                case 10 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:925:6: ( ( 'dash' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:925:6: ( ( 'dash' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:926:1: ( 'dash' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getDashEnumLiteralDeclaration_9()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:927:1: ( 'dash' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:927:3: 'dash'
                    {
                    match(input,30,FollowSets000.FOLLOW_30_in_rule__ArrowType__Alternatives1986); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getDashEnumLiteralDeclaration_9()); 

                    }


                    }
                    break;
                case 11 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:932:6: ( ( 'delta' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:932:6: ( ( 'delta' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:933:1: ( 'delta' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getDeltaEnumLiteralDeclaration_10()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:934:1: ( 'delta' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:934:3: 'delta'
                    {
                    match(input,31,FollowSets000.FOLLOW_31_in_rule__ArrowType__Alternatives2007); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getDeltaEnumLiteralDeclaration_10()); 

                    }


                    }
                    break;
                case 12 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:939:6: ( ( 'diamond' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:939:6: ( ( 'diamond' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:940:1: ( 'diamond' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getDiamondEnumLiteralDeclaration_11()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:941:1: ( 'diamond' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:941:3: 'diamond'
                    {
                    match(input,32,FollowSets000.FOLLOW_32_in_rule__ArrowType__Alternatives2028); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getDiamondEnumLiteralDeclaration_11()); 

                    }


                    }
                    break;
                case 13 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:946:6: ( ( 'none' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:946:6: ( ( 'none' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:947:1: ( 'none' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getNoneEnumLiteralDeclaration_12()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:948:1: ( 'none' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:948:3: 'none'
                    {
                    match(input,33,FollowSets000.FOLLOW_33_in_rule__ArrowType__Alternatives2049); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getNoneEnumLiteralDeclaration_12()); 

                    }


                    }
                    break;
                case 14 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:953:6: ( ( 'plain' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:953:6: ( ( 'plain' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:954:1: ( 'plain' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getPlainEnumLiteralDeclaration_13()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:955:1: ( 'plain' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:955:3: 'plain'
                    {
                    match(input,34,FollowSets000.FOLLOW_34_in_rule__ArrowType__Alternatives2070); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getPlainEnumLiteralDeclaration_13()); 

                    }


                    }
                    break;
                case 15 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:960:6: ( ( 'short' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:960:6: ( ( 'short' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:961:1: ( 'short' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getShortEnumLiteralDeclaration_14()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:962:1: ( 'short' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:962:3: 'short'
                    {
                    match(input,35,FollowSets000.FOLLOW_35_in_rule__ArrowType__Alternatives2091); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getShortEnumLiteralDeclaration_14()); 

                    }


                    }
                    break;
                case 16 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:967:6: ( ( 'skewed-dash' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:967:6: ( ( 'skewed-dash' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:968:1: ( 'skewed-dash' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getSkewedDashEnumLiteralDeclaration_15()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:969:1: ( 'skewed-dash' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:969:3: 'skewed-dash'
                    {
                    match(input,36,FollowSets000.FOLLOW_36_in_rule__ArrowType__Alternatives2112); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getSkewedDashEnumLiteralDeclaration_15()); 

                    }


                    }
                    break;
                case 17 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:974:6: ( ( 'standard' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:974:6: ( ( 'standard' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:975:1: ( 'standard' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getStandardEnumLiteralDeclaration_16()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:976:1: ( 'standard' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:976:3: 'standard'
                    {
                    match(input,37,FollowSets000.FOLLOW_37_in_rule__ArrowType__Alternatives2133); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getStandardEnumLiteralDeclaration_16()); 

                    }


                    }
                    break;
                case 18 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:981:6: ( ( 'transparent-circle' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:981:6: ( ( 'transparent-circle' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:982:1: ( 'transparent-circle' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getTransparentCircleEnumLiteralDeclaration_17()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:983:1: ( 'transparent-circle' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:983:3: 'transparent-circle'
                    {
                    match(input,38,FollowSets000.FOLLOW_38_in_rule__ArrowType__Alternatives2154); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getTransparentCircleEnumLiteralDeclaration_17()); 

                    }


                    }
                    break;
                case 19 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:988:6: ( ( 't-shape' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:988:6: ( ( 't-shape' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:989:1: ( 't-shape' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getTShapeEnumLiteralDeclaration_18()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:990:1: ( 't-shape' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:990:3: 't-shape'
                    {
                    match(input,39,FollowSets000.FOLLOW_39_in_rule__ArrowType__Alternatives2175); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getTShapeEnumLiteralDeclaration_18()); 

                    }


                    }
                    break;
                case 20 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:995:6: ( ( 'white-delta' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:995:6: ( ( 'white-delta' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:996:1: ( 'white-delta' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getWhiteDeltaEnumLiteralDeclaration_19()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:997:1: ( 'white-delta' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:997:3: 'white-delta'
                    {
                    match(input,40,FollowSets000.FOLLOW_40_in_rule__ArrowType__Alternatives2196); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getWhiteDeltaEnumLiteralDeclaration_19()); 

                    }


                    }
                    break;
                case 21 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1002:6: ( ( 'white-diamond' ) )
                    {
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1002:6: ( ( 'white-diamond' ) )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1003:1: ( 'white-diamond' )
                    {
                     before(grammarAccess.getArrowTypeAccess().getWhiteDiamondEnumLiteralDeclaration_20()); 
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1004:1: ( 'white-diamond' )
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1004:3: 'white-diamond'
                    {
                    match(input,41,FollowSets000.FOLLOW_41_in_rule__ArrowType__Alternatives2217); 

                    }

                     after(grammarAccess.getArrowTypeAccess().getWhiteDiamondEnumLiteralDeclaration_20()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ArrowType__Alternatives"


    // $ANTLR start "rule__FragmentModel__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1016:1: rule__FragmentModel__Group__0 : rule__FragmentModel__Group__0__Impl rule__FragmentModel__Group__1 ;
    public final void rule__FragmentModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1020:1: ( rule__FragmentModel__Group__0__Impl rule__FragmentModel__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1021:2: rule__FragmentModel__Group__0__Impl rule__FragmentModel__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__0__Impl_in_rule__FragmentModel__Group__02250);
            rule__FragmentModel__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__1_in_rule__FragmentModel__Group__02253);
            rule__FragmentModel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__0"


    // $ANTLR start "rule__FragmentModel__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1028:1: rule__FragmentModel__Group__0__Impl : ( () ) ;
    public final void rule__FragmentModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1032:1: ( ( () ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1033:1: ( () )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1033:1: ( () )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1034:1: ()
            {
             before(grammarAccess.getFragmentModelAccess().getFragmentModelAction_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1035:1: ()
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1037:1: 
            {
            }

             after(grammarAccess.getFragmentModelAccess().getFragmentModelAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__0__Impl"


    // $ANTLR start "rule__FragmentModel__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1047:1: rule__FragmentModel__Group__1 : rule__FragmentModel__Group__1__Impl rule__FragmentModel__Group__2 ;
    public final void rule__FragmentModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1051:1: ( rule__FragmentModel__Group__1__Impl rule__FragmentModel__Group__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1052:2: rule__FragmentModel__Group__1__Impl rule__FragmentModel__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__1__Impl_in_rule__FragmentModel__Group__12311);
            rule__FragmentModel__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__2_in_rule__FragmentModel__Group__12314);
            rule__FragmentModel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__1"


    // $ANTLR start "rule__FragmentModel__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1059:1: rule__FragmentModel__Group__1__Impl : ( 'model' ) ;
    public final void rule__FragmentModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1063:1: ( ( 'model' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1064:1: ( 'model' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1064:1: ( 'model' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1065:1: 'model'
            {
             before(grammarAccess.getFragmentModelAccess().getModelKeyword_1()); 
            match(input,42,FollowSets000.FOLLOW_42_in_rule__FragmentModel__Group__1__Impl2342); 
             after(grammarAccess.getFragmentModelAccess().getModelKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__1__Impl"


    // $ANTLR start "rule__FragmentModel__Group__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1078:1: rule__FragmentModel__Group__2 : rule__FragmentModel__Group__2__Impl rule__FragmentModel__Group__3 ;
    public final void rule__FragmentModel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1082:1: ( rule__FragmentModel__Group__2__Impl rule__FragmentModel__Group__3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1083:2: rule__FragmentModel__Group__2__Impl rule__FragmentModel__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__2__Impl_in_rule__FragmentModel__Group__22373);
            rule__FragmentModel__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__3_in_rule__FragmentModel__Group__22376);
            rule__FragmentModel__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__2"


    // $ANTLR start "rule__FragmentModel__Group__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1090:1: rule__FragmentModel__Group__2__Impl : ( ( rule__FragmentModel__NameAssignment_2 ) ) ;
    public final void rule__FragmentModel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1094:1: ( ( ( rule__FragmentModel__NameAssignment_2 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1095:1: ( ( rule__FragmentModel__NameAssignment_2 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1095:1: ( ( rule__FragmentModel__NameAssignment_2 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1096:1: ( rule__FragmentModel__NameAssignment_2 )
            {
             before(grammarAccess.getFragmentModelAccess().getNameAssignment_2()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1097:1: ( rule__FragmentModel__NameAssignment_2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1097:2: rule__FragmentModel__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__NameAssignment_2_in_rule__FragmentModel__Group__2__Impl2403);
            rule__FragmentModel__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFragmentModelAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__2__Impl"


    // $ANTLR start "rule__FragmentModel__Group__3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1107:1: rule__FragmentModel__Group__3 : rule__FragmentModel__Group__3__Impl rule__FragmentModel__Group__4 ;
    public final void rule__FragmentModel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1111:1: ( rule__FragmentModel__Group__3__Impl rule__FragmentModel__Group__4 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1112:2: rule__FragmentModel__Group__3__Impl rule__FragmentModel__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__3__Impl_in_rule__FragmentModel__Group__32433);
            rule__FragmentModel__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__4_in_rule__FragmentModel__Group__32436);
            rule__FragmentModel__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__3"


    // $ANTLR start "rule__FragmentModel__Group__3__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1119:1: rule__FragmentModel__Group__3__Impl : ( ( rule__FragmentModel__FragmentsAssignment_3 ) ) ;
    public final void rule__FragmentModel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1123:1: ( ( ( rule__FragmentModel__FragmentsAssignment_3 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1124:1: ( ( rule__FragmentModel__FragmentsAssignment_3 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1124:1: ( ( rule__FragmentModel__FragmentsAssignment_3 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1125:1: ( rule__FragmentModel__FragmentsAssignment_3 )
            {
             before(grammarAccess.getFragmentModelAccess().getFragmentsAssignment_3()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1126:1: ( rule__FragmentModel__FragmentsAssignment_3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1126:2: rule__FragmentModel__FragmentsAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__FragmentsAssignment_3_in_rule__FragmentModel__Group__3__Impl2463);
            rule__FragmentModel__FragmentsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getFragmentModelAccess().getFragmentsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__3__Impl"


    // $ANTLR start "rule__FragmentModel__Group__4"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1136:1: rule__FragmentModel__Group__4 : rule__FragmentModel__Group__4__Impl ;
    public final void rule__FragmentModel__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1140:1: ( rule__FragmentModel__Group__4__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1141:2: rule__FragmentModel__Group__4__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__Group__4__Impl_in_rule__FragmentModel__Group__42493);
            rule__FragmentModel__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__4"


    // $ANTLR start "rule__FragmentModel__Group__4__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1147:1: rule__FragmentModel__Group__4__Impl : ( ( rule__FragmentModel__FragmentsAssignment_4 )* ) ;
    public final void rule__FragmentModel__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1151:1: ( ( ( rule__FragmentModel__FragmentsAssignment_4 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1152:1: ( ( rule__FragmentModel__FragmentsAssignment_4 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1152:1: ( ( rule__FragmentModel__FragmentsAssignment_4 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1153:1: ( rule__FragmentModel__FragmentsAssignment_4 )*
            {
             before(grammarAccess.getFragmentModelAccess().getFragmentsAssignment_4()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1154:1: ( rule__FragmentModel__FragmentsAssignment_4 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>=15 && LA10_0<=16)||LA10_0==43||LA10_0==50) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1154:2: rule__FragmentModel__FragmentsAssignment_4
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__FragmentModel__FragmentsAssignment_4_in_rule__FragmentModel__Group__4__Impl2520);
            	    rule__FragmentModel__FragmentsAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getFragmentModelAccess().getFragmentsAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__Group__4__Impl"


    // $ANTLR start "rule__Fragment__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1174:1: rule__Fragment__Group__0 : rule__Fragment__Group__0__Impl rule__Fragment__Group__1 ;
    public final void rule__Fragment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1178:1: ( rule__Fragment__Group__0__Impl rule__Fragment__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1179:2: rule__Fragment__Group__0__Impl rule__Fragment__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__0__Impl_in_rule__Fragment__Group__02561);
            rule__Fragment__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__1_in_rule__Fragment__Group__02564);
            rule__Fragment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__0"


    // $ANTLR start "rule__Fragment__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1186:1: rule__Fragment__Group__0__Impl : ( () ) ;
    public final void rule__Fragment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1190:1: ( ( () ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1191:1: ( () )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1191:1: ( () )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1192:1: ()
            {
             before(grammarAccess.getFragmentAccess().getFragmentAction_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1193:1: ()
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1195:1: 
            {
            }

             after(grammarAccess.getFragmentAccess().getFragmentAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__0__Impl"


    // $ANTLR start "rule__Fragment__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1205:1: rule__Fragment__Group__1 : rule__Fragment__Group__1__Impl rule__Fragment__Group__2 ;
    public final void rule__Fragment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1209:1: ( rule__Fragment__Group__1__Impl rule__Fragment__Group__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1210:2: rule__Fragment__Group__1__Impl rule__Fragment__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__1__Impl_in_rule__Fragment__Group__12622);
            rule__Fragment__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__2_in_rule__Fragment__Group__12625);
            rule__Fragment__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__1"


    // $ANTLR start "rule__Fragment__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1217:1: rule__Fragment__Group__1__Impl : ( ( rule__Fragment__Group_1__0 )? ) ;
    public final void rule__Fragment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1221:1: ( ( ( rule__Fragment__Group_1__0 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1222:1: ( ( rule__Fragment__Group_1__0 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1222:1: ( ( rule__Fragment__Group_1__0 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1223:1: ( rule__Fragment__Group_1__0 )?
            {
             before(grammarAccess.getFragmentAccess().getGroup_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1224:1: ( rule__Fragment__Group_1__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==50) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1224:2: rule__Fragment__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_1__0_in_rule__Fragment__Group__1__Impl2652);
                    rule__Fragment__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFragmentAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__1__Impl"


    // $ANTLR start "rule__Fragment__Group__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1234:1: rule__Fragment__Group__2 : rule__Fragment__Group__2__Impl rule__Fragment__Group__3 ;
    public final void rule__Fragment__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1238:1: ( rule__Fragment__Group__2__Impl rule__Fragment__Group__3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1239:2: rule__Fragment__Group__2__Impl rule__Fragment__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__2__Impl_in_rule__Fragment__Group__22683);
            rule__Fragment__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__3_in_rule__Fragment__Group__22686);
            rule__Fragment__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__2"


    // $ANTLR start "rule__Fragment__Group__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1246:1: rule__Fragment__Group__2__Impl : ( ( rule__Fragment__FtypeAssignment_2 )? ) ;
    public final void rule__Fragment__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1250:1: ( ( ( rule__Fragment__FtypeAssignment_2 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1251:1: ( ( rule__Fragment__FtypeAssignment_2 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1251:1: ( ( rule__Fragment__FtypeAssignment_2 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1252:1: ( rule__Fragment__FtypeAssignment_2 )?
            {
             before(grammarAccess.getFragmentAccess().getFtypeAssignment_2()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1253:1: ( rule__Fragment__FtypeAssignment_2 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=15 && LA12_0<=16)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1253:2: rule__Fragment__FtypeAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Fragment__FtypeAssignment_2_in_rule__Fragment__Group__2__Impl2713);
                    rule__Fragment__FtypeAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFragmentAccess().getFtypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__2__Impl"


    // $ANTLR start "rule__Fragment__Group__3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1263:1: rule__Fragment__Group__3 : rule__Fragment__Group__3__Impl rule__Fragment__Group__4 ;
    public final void rule__Fragment__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1267:1: ( rule__Fragment__Group__3__Impl rule__Fragment__Group__4 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1268:2: rule__Fragment__Group__3__Impl rule__Fragment__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__3__Impl_in_rule__Fragment__Group__32744);
            rule__Fragment__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__4_in_rule__Fragment__Group__32747);
            rule__Fragment__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__3"


    // $ANTLR start "rule__Fragment__Group__3__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1275:1: rule__Fragment__Group__3__Impl : ( 'fragment' ) ;
    public final void rule__Fragment__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1279:1: ( ( 'fragment' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1280:1: ( 'fragment' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1280:1: ( 'fragment' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1281:1: 'fragment'
            {
             before(grammarAccess.getFragmentAccess().getFragmentKeyword_3()); 
            match(input,43,FollowSets000.FOLLOW_43_in_rule__Fragment__Group__3__Impl2775); 
             after(grammarAccess.getFragmentAccess().getFragmentKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__3__Impl"


    // $ANTLR start "rule__Fragment__Group__4"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1294:1: rule__Fragment__Group__4 : rule__Fragment__Group__4__Impl rule__Fragment__Group__5 ;
    public final void rule__Fragment__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1298:1: ( rule__Fragment__Group__4__Impl rule__Fragment__Group__5 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1299:2: rule__Fragment__Group__4__Impl rule__Fragment__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__4__Impl_in_rule__Fragment__Group__42806);
            rule__Fragment__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__5_in_rule__Fragment__Group__42809);
            rule__Fragment__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__4"


    // $ANTLR start "rule__Fragment__Group__4__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1306:1: rule__Fragment__Group__4__Impl : ( ( rule__Fragment__Group_4__0 )? ) ;
    public final void rule__Fragment__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1310:1: ( ( ( rule__Fragment__Group_4__0 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1311:1: ( ( rule__Fragment__Group_4__0 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1311:1: ( ( rule__Fragment__Group_4__0 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1312:1: ( rule__Fragment__Group_4__0 )?
            {
             before(grammarAccess.getFragmentAccess().getGroup_4()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1313:1: ( rule__Fragment__Group_4__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==46) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1313:2: rule__Fragment__Group_4__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__0_in_rule__Fragment__Group__4__Impl2836);
                    rule__Fragment__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFragmentAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__4__Impl"


    // $ANTLR start "rule__Fragment__Group__5"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1323:1: rule__Fragment__Group__5 : rule__Fragment__Group__5__Impl rule__Fragment__Group__6 ;
    public final void rule__Fragment__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1327:1: ( rule__Fragment__Group__5__Impl rule__Fragment__Group__6 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1328:2: rule__Fragment__Group__5__Impl rule__Fragment__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__5__Impl_in_rule__Fragment__Group__52867);
            rule__Fragment__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__6_in_rule__Fragment__Group__52870);
            rule__Fragment__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__5"


    // $ANTLR start "rule__Fragment__Group__5__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1335:1: rule__Fragment__Group__5__Impl : ( ( rule__Fragment__NameAssignment_5 ) ) ;
    public final void rule__Fragment__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1339:1: ( ( ( rule__Fragment__NameAssignment_5 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1340:1: ( ( rule__Fragment__NameAssignment_5 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1340:1: ( ( rule__Fragment__NameAssignment_5 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1341:1: ( rule__Fragment__NameAssignment_5 )
            {
             before(grammarAccess.getFragmentAccess().getNameAssignment_5()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1342:1: ( rule__Fragment__NameAssignment_5 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1342:2: rule__Fragment__NameAssignment_5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__NameAssignment_5_in_rule__Fragment__Group__5__Impl2897);
            rule__Fragment__NameAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getNameAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__5__Impl"


    // $ANTLR start "rule__Fragment__Group__6"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1352:1: rule__Fragment__Group__6 : rule__Fragment__Group__6__Impl rule__Fragment__Group__7 ;
    public final void rule__Fragment__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1356:1: ( rule__Fragment__Group__6__Impl rule__Fragment__Group__7 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1357:2: rule__Fragment__Group__6__Impl rule__Fragment__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__6__Impl_in_rule__Fragment__Group__62927);
            rule__Fragment__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__7_in_rule__Fragment__Group__62930);
            rule__Fragment__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__6"


    // $ANTLR start "rule__Fragment__Group__6__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1364:1: rule__Fragment__Group__6__Impl : ( '{' ) ;
    public final void rule__Fragment__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1368:1: ( ( '{' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1369:1: ( '{' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1369:1: ( '{' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1370:1: '{'
            {
             before(grammarAccess.getFragmentAccess().getLeftCurlyBracketKeyword_6()); 
            match(input,44,FollowSets000.FOLLOW_44_in_rule__Fragment__Group__6__Impl2958); 
             after(grammarAccess.getFragmentAccess().getLeftCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__6__Impl"


    // $ANTLR start "rule__Fragment__Group__7"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1383:1: rule__Fragment__Group__7 : rule__Fragment__Group__7__Impl rule__Fragment__Group__8 ;
    public final void rule__Fragment__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1387:1: ( rule__Fragment__Group__7__Impl rule__Fragment__Group__8 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1388:2: rule__Fragment__Group__7__Impl rule__Fragment__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__7__Impl_in_rule__Fragment__Group__72989);
            rule__Fragment__Group__7__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__8_in_rule__Fragment__Group__72992);
            rule__Fragment__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__7"


    // $ANTLR start "rule__Fragment__Group__7__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1395:1: rule__Fragment__Group__7__Impl : ( ( rule__Fragment__ObjectsAssignment_7 ) ) ;
    public final void rule__Fragment__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1399:1: ( ( ( rule__Fragment__ObjectsAssignment_7 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1400:1: ( ( rule__Fragment__ObjectsAssignment_7 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1400:1: ( ( rule__Fragment__ObjectsAssignment_7 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1401:1: ( rule__Fragment__ObjectsAssignment_7 )
            {
             before(grammarAccess.getFragmentAccess().getObjectsAssignment_7()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1402:1: ( rule__Fragment__ObjectsAssignment_7 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1402:2: rule__Fragment__ObjectsAssignment_7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__ObjectsAssignment_7_in_rule__Fragment__Group__7__Impl3019);
            rule__Fragment__ObjectsAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getObjectsAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__7__Impl"


    // $ANTLR start "rule__Fragment__Group__8"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1412:1: rule__Fragment__Group__8 : rule__Fragment__Group__8__Impl rule__Fragment__Group__9 ;
    public final void rule__Fragment__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1416:1: ( rule__Fragment__Group__8__Impl rule__Fragment__Group__9 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1417:2: rule__Fragment__Group__8__Impl rule__Fragment__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__8__Impl_in_rule__Fragment__Group__83049);
            rule__Fragment__Group__8__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__9_in_rule__Fragment__Group__83052);
            rule__Fragment__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__8"


    // $ANTLR start "rule__Fragment__Group__8__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1424:1: rule__Fragment__Group__8__Impl : ( ( rule__Fragment__ObjectsAssignment_8 )* ) ;
    public final void rule__Fragment__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1428:1: ( ( ( rule__Fragment__ObjectsAssignment_8 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1429:1: ( ( rule__Fragment__ObjectsAssignment_8 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1429:1: ( ( rule__Fragment__ObjectsAssignment_8 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1430:1: ( rule__Fragment__ObjectsAssignment_8 )*
            {
             before(grammarAccess.getFragmentAccess().getObjectsAssignment_8()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1431:1: ( rule__Fragment__ObjectsAssignment_8 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=RULE_STRING && LA14_0<=RULE_ID)||LA14_0==50) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1431:2: rule__Fragment__ObjectsAssignment_8
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Fragment__ObjectsAssignment_8_in_rule__Fragment__Group__8__Impl3079);
            	    rule__Fragment__ObjectsAssignment_8();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getFragmentAccess().getObjectsAssignment_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__8__Impl"


    // $ANTLR start "rule__Fragment__Group__9"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1441:1: rule__Fragment__Group__9 : rule__Fragment__Group__9__Impl ;
    public final void rule__Fragment__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1445:1: ( rule__Fragment__Group__9__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1446:2: rule__Fragment__Group__9__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group__9__Impl_in_rule__Fragment__Group__93110);
            rule__Fragment__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__9"


    // $ANTLR start "rule__Fragment__Group__9__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1452:1: rule__Fragment__Group__9__Impl : ( '}' ) ;
    public final void rule__Fragment__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1456:1: ( ( '}' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1457:1: ( '}' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1457:1: ( '}' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1458:1: '}'
            {
             before(grammarAccess.getFragmentAccess().getRightCurlyBracketKeyword_9()); 
            match(input,45,FollowSets000.FOLLOW_45_in_rule__Fragment__Group__9__Impl3138); 
             after(grammarAccess.getFragmentAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group__9__Impl"


    // $ANTLR start "rule__Fragment__Group_1__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1491:1: rule__Fragment__Group_1__0 : rule__Fragment__Group_1__0__Impl rule__Fragment__Group_1__1 ;
    public final void rule__Fragment__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1495:1: ( rule__Fragment__Group_1__0__Impl rule__Fragment__Group_1__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1496:2: rule__Fragment__Group_1__0__Impl rule__Fragment__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_1__0__Impl_in_rule__Fragment__Group_1__03189);
            rule__Fragment__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_1__1_in_rule__Fragment__Group_1__03192);
            rule__Fragment__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_1__0"


    // $ANTLR start "rule__Fragment__Group_1__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1503:1: rule__Fragment__Group_1__0__Impl : ( ( rule__Fragment__AnnotationAssignment_1_0 ) ) ;
    public final void rule__Fragment__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1507:1: ( ( ( rule__Fragment__AnnotationAssignment_1_0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1508:1: ( ( rule__Fragment__AnnotationAssignment_1_0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1508:1: ( ( rule__Fragment__AnnotationAssignment_1_0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1509:1: ( rule__Fragment__AnnotationAssignment_1_0 )
            {
             before(grammarAccess.getFragmentAccess().getAnnotationAssignment_1_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1510:1: ( rule__Fragment__AnnotationAssignment_1_0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1510:2: rule__Fragment__AnnotationAssignment_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__AnnotationAssignment_1_0_in_rule__Fragment__Group_1__0__Impl3219);
            rule__Fragment__AnnotationAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getAnnotationAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_1__0__Impl"


    // $ANTLR start "rule__Fragment__Group_1__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1520:1: rule__Fragment__Group_1__1 : rule__Fragment__Group_1__1__Impl ;
    public final void rule__Fragment__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1524:1: ( rule__Fragment__Group_1__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1525:2: rule__Fragment__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_1__1__Impl_in_rule__Fragment__Group_1__13249);
            rule__Fragment__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_1__1"


    // $ANTLR start "rule__Fragment__Group_1__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1531:1: rule__Fragment__Group_1__1__Impl : ( ( rule__Fragment__AnnotationAssignment_1_1 )* ) ;
    public final void rule__Fragment__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1535:1: ( ( ( rule__Fragment__AnnotationAssignment_1_1 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1536:1: ( ( rule__Fragment__AnnotationAssignment_1_1 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1536:1: ( ( rule__Fragment__AnnotationAssignment_1_1 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1537:1: ( rule__Fragment__AnnotationAssignment_1_1 )*
            {
             before(grammarAccess.getFragmentAccess().getAnnotationAssignment_1_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1538:1: ( rule__Fragment__AnnotationAssignment_1_1 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==50) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1538:2: rule__Fragment__AnnotationAssignment_1_1
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Fragment__AnnotationAssignment_1_1_in_rule__Fragment__Group_1__1__Impl3276);
            	    rule__Fragment__AnnotationAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getFragmentAccess().getAnnotationAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_1__1__Impl"


    // $ANTLR start "rule__Fragment__Group_4__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1552:1: rule__Fragment__Group_4__0 : rule__Fragment__Group_4__0__Impl rule__Fragment__Group_4__1 ;
    public final void rule__Fragment__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1556:1: ( rule__Fragment__Group_4__0__Impl rule__Fragment__Group_4__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1557:2: rule__Fragment__Group_4__0__Impl rule__Fragment__Group_4__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__0__Impl_in_rule__Fragment__Group_4__03311);
            rule__Fragment__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__1_in_rule__Fragment__Group_4__03314);
            rule__Fragment__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__0"


    // $ANTLR start "rule__Fragment__Group_4__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1564:1: rule__Fragment__Group_4__0__Impl : ( '[' ) ;
    public final void rule__Fragment__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1568:1: ( ( '[' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1569:1: ( '[' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1569:1: ( '[' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1570:1: '['
            {
             before(grammarAccess.getFragmentAccess().getLeftSquareBracketKeyword_4_0()); 
            match(input,46,FollowSets000.FOLLOW_46_in_rule__Fragment__Group_4__0__Impl3342); 
             after(grammarAccess.getFragmentAccess().getLeftSquareBracketKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__0__Impl"


    // $ANTLR start "rule__Fragment__Group_4__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1583:1: rule__Fragment__Group_4__1 : rule__Fragment__Group_4__1__Impl rule__Fragment__Group_4__2 ;
    public final void rule__Fragment__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1587:1: ( rule__Fragment__Group_4__1__Impl rule__Fragment__Group_4__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1588:2: rule__Fragment__Group_4__1__Impl rule__Fragment__Group_4__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__1__Impl_in_rule__Fragment__Group_4__13373);
            rule__Fragment__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__2_in_rule__Fragment__Group_4__13376);
            rule__Fragment__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__1"


    // $ANTLR start "rule__Fragment__Group_4__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1595:1: rule__Fragment__Group_4__1__Impl : ( ( rule__Fragment__TypeAssignment_4_1 ) ) ;
    public final void rule__Fragment__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1599:1: ( ( ( rule__Fragment__TypeAssignment_4_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1600:1: ( ( rule__Fragment__TypeAssignment_4_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1600:1: ( ( rule__Fragment__TypeAssignment_4_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1601:1: ( rule__Fragment__TypeAssignment_4_1 )
            {
             before(grammarAccess.getFragmentAccess().getTypeAssignment_4_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1602:1: ( rule__Fragment__TypeAssignment_4_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1602:2: rule__Fragment__TypeAssignment_4_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__TypeAssignment_4_1_in_rule__Fragment__Group_4__1__Impl3403);
            rule__Fragment__TypeAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getFragmentAccess().getTypeAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__1__Impl"


    // $ANTLR start "rule__Fragment__Group_4__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1612:1: rule__Fragment__Group_4__2 : rule__Fragment__Group_4__2__Impl ;
    public final void rule__Fragment__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1616:1: ( rule__Fragment__Group_4__2__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1617:2: rule__Fragment__Group_4__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Fragment__Group_4__2__Impl_in_rule__Fragment__Group_4__23433);
            rule__Fragment__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__2"


    // $ANTLR start "rule__Fragment__Group_4__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1623:1: rule__Fragment__Group_4__2__Impl : ( ']' ) ;
    public final void rule__Fragment__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1627:1: ( ( ']' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1628:1: ( ']' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1628:1: ( ']' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1629:1: ']'
            {
             before(grammarAccess.getFragmentAccess().getRightSquareBracketKeyword_4_2()); 
            match(input,47,FollowSets000.FOLLOW_47_in_rule__Fragment__Group_4__2__Impl3461); 
             after(grammarAccess.getFragmentAccess().getRightSquareBracketKeyword_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__Group_4__2__Impl"


    // $ANTLR start "rule__Object__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1648:1: rule__Object__Group__0 : rule__Object__Group__0__Impl rule__Object__Group__1 ;
    public final void rule__Object__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1652:1: ( rule__Object__Group__0__Impl rule__Object__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1653:2: rule__Object__Group__0__Impl rule__Object__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__0__Impl_in_rule__Object__Group__03498);
            rule__Object__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__1_in_rule__Object__Group__03501);
            rule__Object__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__0"


    // $ANTLR start "rule__Object__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1660:1: rule__Object__Group__0__Impl : ( () ) ;
    public final void rule__Object__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1664:1: ( ( () ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1665:1: ( () )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1665:1: ( () )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1666:1: ()
            {
             before(grammarAccess.getObjectAccess().getObjectAction_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1667:1: ()
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1669:1: 
            {
            }

             after(grammarAccess.getObjectAccess().getObjectAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__0__Impl"


    // $ANTLR start "rule__Object__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1679:1: rule__Object__Group__1 : rule__Object__Group__1__Impl rule__Object__Group__2 ;
    public final void rule__Object__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1683:1: ( rule__Object__Group__1__Impl rule__Object__Group__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1684:2: rule__Object__Group__1__Impl rule__Object__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__1__Impl_in_rule__Object__Group__13559);
            rule__Object__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__2_in_rule__Object__Group__13562);
            rule__Object__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__1"


    // $ANTLR start "rule__Object__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1691:1: rule__Object__Group__1__Impl : ( ( rule__Object__Group_1__0 )? ) ;
    public final void rule__Object__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1695:1: ( ( ( rule__Object__Group_1__0 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1696:1: ( ( rule__Object__Group_1__0 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1696:1: ( ( rule__Object__Group_1__0 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1697:1: ( rule__Object__Group_1__0 )?
            {
             before(grammarAccess.getObjectAccess().getGroup_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1698:1: ( rule__Object__Group_1__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==50) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1698:2: rule__Object__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Object__Group_1__0_in_rule__Object__Group__1__Impl3589);
                    rule__Object__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getObjectAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__1__Impl"


    // $ANTLR start "rule__Object__Group__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1708:1: rule__Object__Group__2 : rule__Object__Group__2__Impl rule__Object__Group__3 ;
    public final void rule__Object__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1712:1: ( rule__Object__Group__2__Impl rule__Object__Group__3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1713:2: rule__Object__Group__2__Impl rule__Object__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__2__Impl_in_rule__Object__Group__23620);
            rule__Object__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__3_in_rule__Object__Group__23623);
            rule__Object__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__2"


    // $ANTLR start "rule__Object__Group__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1720:1: rule__Object__Group__2__Impl : ( ( rule__Object__NameAssignment_2 ) ) ;
    public final void rule__Object__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1724:1: ( ( ( rule__Object__NameAssignment_2 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1725:1: ( ( rule__Object__NameAssignment_2 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1725:1: ( ( rule__Object__NameAssignment_2 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1726:1: ( rule__Object__NameAssignment_2 )
            {
             before(grammarAccess.getObjectAccess().getNameAssignment_2()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1727:1: ( rule__Object__NameAssignment_2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1727:2: rule__Object__NameAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__NameAssignment_2_in_rule__Object__Group__2__Impl3650);
            rule__Object__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__2__Impl"


    // $ANTLR start "rule__Object__Group__3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1737:1: rule__Object__Group__3 : rule__Object__Group__3__Impl rule__Object__Group__4 ;
    public final void rule__Object__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1741:1: ( rule__Object__Group__3__Impl rule__Object__Group__4 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1742:2: rule__Object__Group__3__Impl rule__Object__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__3__Impl_in_rule__Object__Group__33680);
            rule__Object__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__4_in_rule__Object__Group__33683);
            rule__Object__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__3"


    // $ANTLR start "rule__Object__Group__3__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1749:1: rule__Object__Group__3__Impl : ( ':' ) ;
    public final void rule__Object__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1753:1: ( ( ':' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1754:1: ( ':' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1754:1: ( ':' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1755:1: ':'
            {
             before(grammarAccess.getObjectAccess().getColonKeyword_3()); 
            match(input,48,FollowSets000.FOLLOW_48_in_rule__Object__Group__3__Impl3711); 
             after(grammarAccess.getObjectAccess().getColonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__3__Impl"


    // $ANTLR start "rule__Object__Group__4"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1768:1: rule__Object__Group__4 : rule__Object__Group__4__Impl rule__Object__Group__5 ;
    public final void rule__Object__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1772:1: ( rule__Object__Group__4__Impl rule__Object__Group__5 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1773:2: rule__Object__Group__4__Impl rule__Object__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__4__Impl_in_rule__Object__Group__43742);
            rule__Object__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__5_in_rule__Object__Group__43745);
            rule__Object__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__4"


    // $ANTLR start "rule__Object__Group__4__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1780:1: rule__Object__Group__4__Impl : ( ( rule__Object__TypeAssignment_4 ) ) ;
    public final void rule__Object__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1784:1: ( ( ( rule__Object__TypeAssignment_4 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1785:1: ( ( rule__Object__TypeAssignment_4 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1785:1: ( ( rule__Object__TypeAssignment_4 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1786:1: ( rule__Object__TypeAssignment_4 )
            {
             before(grammarAccess.getObjectAccess().getTypeAssignment_4()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1787:1: ( rule__Object__TypeAssignment_4 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1787:2: rule__Object__TypeAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__TypeAssignment_4_in_rule__Object__Group__4__Impl3772);
            rule__Object__TypeAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getTypeAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__4__Impl"


    // $ANTLR start "rule__Object__Group__5"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1797:1: rule__Object__Group__5 : rule__Object__Group__5__Impl rule__Object__Group__6 ;
    public final void rule__Object__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1801:1: ( rule__Object__Group__5__Impl rule__Object__Group__6 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1802:2: rule__Object__Group__5__Impl rule__Object__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__5__Impl_in_rule__Object__Group__53802);
            rule__Object__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__6_in_rule__Object__Group__53805);
            rule__Object__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__5"


    // $ANTLR start "rule__Object__Group__5__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1809:1: rule__Object__Group__5__Impl : ( '{' ) ;
    public final void rule__Object__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1813:1: ( ( '{' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1814:1: ( '{' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1814:1: ( '{' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1815:1: '{'
            {
             before(grammarAccess.getObjectAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,44,FollowSets000.FOLLOW_44_in_rule__Object__Group__5__Impl3833); 
             after(grammarAccess.getObjectAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__5__Impl"


    // $ANTLR start "rule__Object__Group__6"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1828:1: rule__Object__Group__6 : rule__Object__Group__6__Impl rule__Object__Group__7 ;
    public final void rule__Object__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1832:1: ( rule__Object__Group__6__Impl rule__Object__Group__7 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1833:2: rule__Object__Group__6__Impl rule__Object__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__6__Impl_in_rule__Object__Group__63864);
            rule__Object__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__7_in_rule__Object__Group__63867);
            rule__Object__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__6"


    // $ANTLR start "rule__Object__Group__6__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1840:1: rule__Object__Group__6__Impl : ( ( rule__Object__Group_6__0 )? ) ;
    public final void rule__Object__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1844:1: ( ( ( rule__Object__Group_6__0 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1845:1: ( ( rule__Object__Group_6__0 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1845:1: ( ( rule__Object__Group_6__0 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1846:1: ( rule__Object__Group_6__0 )?
            {
             before(grammarAccess.getObjectAccess().getGroup_6()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1847:1: ( rule__Object__Group_6__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ((LA17_0>=11 && LA17_0<=12)||LA17_0==50||LA17_0==57) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1847:2: rule__Object__Group_6__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__0_in_rule__Object__Group__6__Impl3894);
                    rule__Object__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getObjectAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__6__Impl"


    // $ANTLR start "rule__Object__Group__7"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1857:1: rule__Object__Group__7 : rule__Object__Group__7__Impl ;
    public final void rule__Object__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1861:1: ( rule__Object__Group__7__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1862:2: rule__Object__Group__7__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group__7__Impl_in_rule__Object__Group__73925);
            rule__Object__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__7"


    // $ANTLR start "rule__Object__Group__7__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1868:1: rule__Object__Group__7__Impl : ( '}' ) ;
    public final void rule__Object__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1872:1: ( ( '}' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1873:1: ( '}' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1873:1: ( '}' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1874:1: '}'
            {
             before(grammarAccess.getObjectAccess().getRightCurlyBracketKeyword_7()); 
            match(input,45,FollowSets000.FOLLOW_45_in_rule__Object__Group__7__Impl3953); 
             after(grammarAccess.getObjectAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__7__Impl"


    // $ANTLR start "rule__Object__Group_1__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1903:1: rule__Object__Group_1__0 : rule__Object__Group_1__0__Impl rule__Object__Group_1__1 ;
    public final void rule__Object__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1907:1: ( rule__Object__Group_1__0__Impl rule__Object__Group_1__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1908:2: rule__Object__Group_1__0__Impl rule__Object__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_1__0__Impl_in_rule__Object__Group_1__04000);
            rule__Object__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_1__1_in_rule__Object__Group_1__04003);
            rule__Object__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_1__0"


    // $ANTLR start "rule__Object__Group_1__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1915:1: rule__Object__Group_1__0__Impl : ( ( rule__Object__AnnotationAssignment_1_0 ) ) ;
    public final void rule__Object__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1919:1: ( ( ( rule__Object__AnnotationAssignment_1_0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1920:1: ( ( rule__Object__AnnotationAssignment_1_0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1920:1: ( ( rule__Object__AnnotationAssignment_1_0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1921:1: ( rule__Object__AnnotationAssignment_1_0 )
            {
             before(grammarAccess.getObjectAccess().getAnnotationAssignment_1_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1922:1: ( rule__Object__AnnotationAssignment_1_0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1922:2: rule__Object__AnnotationAssignment_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__AnnotationAssignment_1_0_in_rule__Object__Group_1__0__Impl4030);
            rule__Object__AnnotationAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getAnnotationAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_1__0__Impl"


    // $ANTLR start "rule__Object__Group_1__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1932:1: rule__Object__Group_1__1 : rule__Object__Group_1__1__Impl ;
    public final void rule__Object__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1936:1: ( rule__Object__Group_1__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1937:2: rule__Object__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_1__1__Impl_in_rule__Object__Group_1__14060);
            rule__Object__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_1__1"


    // $ANTLR start "rule__Object__Group_1__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1943:1: rule__Object__Group_1__1__Impl : ( ( rule__Object__AnnotationAssignment_1_1 )* ) ;
    public final void rule__Object__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1947:1: ( ( ( rule__Object__AnnotationAssignment_1_1 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1948:1: ( ( rule__Object__AnnotationAssignment_1_1 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1948:1: ( ( rule__Object__AnnotationAssignment_1_1 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1949:1: ( rule__Object__AnnotationAssignment_1_1 )*
            {
             before(grammarAccess.getObjectAccess().getAnnotationAssignment_1_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1950:1: ( rule__Object__AnnotationAssignment_1_1 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==50) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1950:2: rule__Object__AnnotationAssignment_1_1
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Object__AnnotationAssignment_1_1_in_rule__Object__Group_1__1__Impl4087);
            	    rule__Object__AnnotationAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getObjectAccess().getAnnotationAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_1__1__Impl"


    // $ANTLR start "rule__Object__Group_6__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1964:1: rule__Object__Group_6__0 : rule__Object__Group_6__0__Impl rule__Object__Group_6__1 ;
    public final void rule__Object__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1968:1: ( rule__Object__Group_6__0__Impl rule__Object__Group_6__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1969:2: rule__Object__Group_6__0__Impl rule__Object__Group_6__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__0__Impl_in_rule__Object__Group_6__04122);
            rule__Object__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__1_in_rule__Object__Group_6__04125);
            rule__Object__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__0"


    // $ANTLR start "rule__Object__Group_6__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1976:1: rule__Object__Group_6__0__Impl : ( ( rule__Object__FeaturesAssignment_6_0 ) ) ;
    public final void rule__Object__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1980:1: ( ( ( rule__Object__FeaturesAssignment_6_0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1981:1: ( ( rule__Object__FeaturesAssignment_6_0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1981:1: ( ( rule__Object__FeaturesAssignment_6_0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1982:1: ( rule__Object__FeaturesAssignment_6_0 )
            {
             before(grammarAccess.getObjectAccess().getFeaturesAssignment_6_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1983:1: ( rule__Object__FeaturesAssignment_6_0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1983:2: rule__Object__FeaturesAssignment_6_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__FeaturesAssignment_6_0_in_rule__Object__Group_6__0__Impl4152);
            rule__Object__FeaturesAssignment_6_0();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getFeaturesAssignment_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__0__Impl"


    // $ANTLR start "rule__Object__Group_6__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1993:1: rule__Object__Group_6__1 : rule__Object__Group_6__1__Impl rule__Object__Group_6__2 ;
    public final void rule__Object__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1997:1: ( rule__Object__Group_6__1__Impl rule__Object__Group_6__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:1998:2: rule__Object__Group_6__1__Impl rule__Object__Group_6__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__1__Impl_in_rule__Object__Group_6__14182);
            rule__Object__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__2_in_rule__Object__Group_6__14185);
            rule__Object__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__1"


    // $ANTLR start "rule__Object__Group_6__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2005:1: rule__Object__Group_6__1__Impl : ( ( ';' )? ) ;
    public final void rule__Object__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2009:1: ( ( ( ';' )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2010:1: ( ( ';' )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2010:1: ( ( ';' )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2011:1: ( ';' )?
            {
             before(grammarAccess.getObjectAccess().getSemicolonKeyword_6_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2012:1: ( ';' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==49) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2013:2: ';'
                    {
                    match(input,49,FollowSets000.FOLLOW_49_in_rule__Object__Group_6__1__Impl4214); 

                    }
                    break;

            }

             after(grammarAccess.getObjectAccess().getSemicolonKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__1__Impl"


    // $ANTLR start "rule__Object__Group_6__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2024:1: rule__Object__Group_6__2 : rule__Object__Group_6__2__Impl ;
    public final void rule__Object__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2028:1: ( rule__Object__Group_6__2__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2029:2: rule__Object__Group_6__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6__2__Impl_in_rule__Object__Group_6__24247);
            rule__Object__Group_6__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__2"


    // $ANTLR start "rule__Object__Group_6__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2035:1: rule__Object__Group_6__2__Impl : ( ( rule__Object__Group_6_2__0 )* ) ;
    public final void rule__Object__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2039:1: ( ( ( rule__Object__Group_6_2__0 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2040:1: ( ( rule__Object__Group_6_2__0 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2040:1: ( ( rule__Object__Group_6_2__0 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2041:1: ( rule__Object__Group_6_2__0 )*
            {
             before(grammarAccess.getObjectAccess().getGroup_6_2()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2042:1: ( rule__Object__Group_6_2__0 )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( ((LA20_0>=11 && LA20_0<=12)||LA20_0==50||LA20_0==57) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2042:2: rule__Object__Group_6_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6_2__0_in_rule__Object__Group_6__2__Impl4274);
            	    rule__Object__Group_6_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);

             after(grammarAccess.getObjectAccess().getGroup_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6__2__Impl"


    // $ANTLR start "rule__Object__Group_6_2__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2058:1: rule__Object__Group_6_2__0 : rule__Object__Group_6_2__0__Impl rule__Object__Group_6_2__1 ;
    public final void rule__Object__Group_6_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2062:1: ( rule__Object__Group_6_2__0__Impl rule__Object__Group_6_2__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2063:2: rule__Object__Group_6_2__0__Impl rule__Object__Group_6_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6_2__0__Impl_in_rule__Object__Group_6_2__04311);
            rule__Object__Group_6_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6_2__1_in_rule__Object__Group_6_2__04314);
            rule__Object__Group_6_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6_2__0"


    // $ANTLR start "rule__Object__Group_6_2__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2070:1: rule__Object__Group_6_2__0__Impl : ( ( rule__Object__FeaturesAssignment_6_2_0 ) ) ;
    public final void rule__Object__Group_6_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2074:1: ( ( ( rule__Object__FeaturesAssignment_6_2_0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2075:1: ( ( rule__Object__FeaturesAssignment_6_2_0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2075:1: ( ( rule__Object__FeaturesAssignment_6_2_0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2076:1: ( rule__Object__FeaturesAssignment_6_2_0 )
            {
             before(grammarAccess.getObjectAccess().getFeaturesAssignment_6_2_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2077:1: ( rule__Object__FeaturesAssignment_6_2_0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2077:2: rule__Object__FeaturesAssignment_6_2_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__FeaturesAssignment_6_2_0_in_rule__Object__Group_6_2__0__Impl4341);
            rule__Object__FeaturesAssignment_6_2_0();

            state._fsp--;


            }

             after(grammarAccess.getObjectAccess().getFeaturesAssignment_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6_2__0__Impl"


    // $ANTLR start "rule__Object__Group_6_2__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2087:1: rule__Object__Group_6_2__1 : rule__Object__Group_6_2__1__Impl ;
    public final void rule__Object__Group_6_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2091:1: ( rule__Object__Group_6_2__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2092:2: rule__Object__Group_6_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Object__Group_6_2__1__Impl_in_rule__Object__Group_6_2__14371);
            rule__Object__Group_6_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6_2__1"


    // $ANTLR start "rule__Object__Group_6_2__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2098:1: rule__Object__Group_6_2__1__Impl : ( ( ';' )? ) ;
    public final void rule__Object__Group_6_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2102:1: ( ( ( ';' )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2103:1: ( ( ';' )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2103:1: ( ( ';' )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2104:1: ( ';' )?
            {
             before(grammarAccess.getObjectAccess().getSemicolonKeyword_6_2_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2105:1: ( ';' )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==49) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2106:2: ';'
                    {
                    match(input,49,FollowSets000.FOLLOW_49_in_rule__Object__Group_6_2__1__Impl4400); 

                    }
                    break;

            }

             after(grammarAccess.getObjectAccess().getSemicolonKeyword_6_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group_6_2__1__Impl"


    // $ANTLR start "rule__Annotation__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2121:1: rule__Annotation__Group__0 : rule__Annotation__Group__0__Impl rule__Annotation__Group__1 ;
    public final void rule__Annotation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2125:1: ( rule__Annotation__Group__0__Impl rule__Annotation__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2126:2: rule__Annotation__Group__0__Impl rule__Annotation__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__0__Impl_in_rule__Annotation__Group__04437);
            rule__Annotation__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__1_in_rule__Annotation__Group__04440);
            rule__Annotation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__0"


    // $ANTLR start "rule__Annotation__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2133:1: rule__Annotation__Group__0__Impl : ( '@' ) ;
    public final void rule__Annotation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2137:1: ( ( '@' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2138:1: ( '@' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2138:1: ( '@' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2139:1: '@'
            {
             before(grammarAccess.getAnnotationAccess().getCommercialAtKeyword_0()); 
            match(input,50,FollowSets000.FOLLOW_50_in_rule__Annotation__Group__0__Impl4468); 
             after(grammarAccess.getAnnotationAccess().getCommercialAtKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__0__Impl"


    // $ANTLR start "rule__Annotation__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2152:1: rule__Annotation__Group__1 : rule__Annotation__Group__1__Impl rule__Annotation__Group__2 ;
    public final void rule__Annotation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2156:1: ( rule__Annotation__Group__1__Impl rule__Annotation__Group__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2157:2: rule__Annotation__Group__1__Impl rule__Annotation__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__1__Impl_in_rule__Annotation__Group__14499);
            rule__Annotation__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__2_in_rule__Annotation__Group__14502);
            rule__Annotation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__1"


    // $ANTLR start "rule__Annotation__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2164:1: rule__Annotation__Group__1__Impl : ( ( rule__Annotation__NameAssignment_1 ) ) ;
    public final void rule__Annotation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2168:1: ( ( ( rule__Annotation__NameAssignment_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2169:1: ( ( rule__Annotation__NameAssignment_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2169:1: ( ( rule__Annotation__NameAssignment_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2170:1: ( rule__Annotation__NameAssignment_1 )
            {
             before(grammarAccess.getAnnotationAccess().getNameAssignment_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2171:1: ( rule__Annotation__NameAssignment_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2171:2: rule__Annotation__NameAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__NameAssignment_1_in_rule__Annotation__Group__1__Impl4529);
            rule__Annotation__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__1__Impl"


    // $ANTLR start "rule__Annotation__Group__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2181:1: rule__Annotation__Group__2 : rule__Annotation__Group__2__Impl ;
    public final void rule__Annotation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2185:1: ( rule__Annotation__Group__2__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2186:2: rule__Annotation__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group__2__Impl_in_rule__Annotation__Group__24559);
            rule__Annotation__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__2"


    // $ANTLR start "rule__Annotation__Group__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2192:1: rule__Annotation__Group__2__Impl : ( ( rule__Annotation__Group_2__0 )? ) ;
    public final void rule__Annotation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2196:1: ( ( ( rule__Annotation__Group_2__0 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2197:1: ( ( rule__Annotation__Group_2__0 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2197:1: ( ( rule__Annotation__Group_2__0 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2198:1: ( rule__Annotation__Group_2__0 )?
            {
             before(grammarAccess.getAnnotationAccess().getGroup_2()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2199:1: ( rule__Annotation__Group_2__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==51) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2199:2: rule__Annotation__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__0_in_rule__Annotation__Group__2__Impl4586);
                    rule__Annotation__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAnnotationAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group__2__Impl"


    // $ANTLR start "rule__Annotation__Group_2__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2215:1: rule__Annotation__Group_2__0 : rule__Annotation__Group_2__0__Impl rule__Annotation__Group_2__1 ;
    public final void rule__Annotation__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2219:1: ( rule__Annotation__Group_2__0__Impl rule__Annotation__Group_2__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2220:2: rule__Annotation__Group_2__0__Impl rule__Annotation__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__0__Impl_in_rule__Annotation__Group_2__04623);
            rule__Annotation__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__1_in_rule__Annotation__Group_2__04626);
            rule__Annotation__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__0"


    // $ANTLR start "rule__Annotation__Group_2__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2227:1: rule__Annotation__Group_2__0__Impl : ( '(' ) ;
    public final void rule__Annotation__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2231:1: ( ( '(' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2232:1: ( '(' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2232:1: ( '(' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2233:1: '('
            {
             before(grammarAccess.getAnnotationAccess().getLeftParenthesisKeyword_2_0()); 
            match(input,51,FollowSets000.FOLLOW_51_in_rule__Annotation__Group_2__0__Impl4654); 
             after(grammarAccess.getAnnotationAccess().getLeftParenthesisKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__0__Impl"


    // $ANTLR start "rule__Annotation__Group_2__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2246:1: rule__Annotation__Group_2__1 : rule__Annotation__Group_2__1__Impl rule__Annotation__Group_2__2 ;
    public final void rule__Annotation__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2250:1: ( rule__Annotation__Group_2__1__Impl rule__Annotation__Group_2__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2251:2: rule__Annotation__Group_2__1__Impl rule__Annotation__Group_2__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__1__Impl_in_rule__Annotation__Group_2__14685);
            rule__Annotation__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__2_in_rule__Annotation__Group_2__14688);
            rule__Annotation__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__1"


    // $ANTLR start "rule__Annotation__Group_2__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2258:1: rule__Annotation__Group_2__1__Impl : ( ( rule__Annotation__ParamsAssignment_2_1 ) ) ;
    public final void rule__Annotation__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2262:1: ( ( ( rule__Annotation__ParamsAssignment_2_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2263:1: ( ( rule__Annotation__ParamsAssignment_2_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2263:1: ( ( rule__Annotation__ParamsAssignment_2_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2264:1: ( rule__Annotation__ParamsAssignment_2_1 )
            {
             before(grammarAccess.getAnnotationAccess().getParamsAssignment_2_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2265:1: ( rule__Annotation__ParamsAssignment_2_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2265:2: rule__Annotation__ParamsAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__ParamsAssignment_2_1_in_rule__Annotation__Group_2__1__Impl4715);
            rule__Annotation__ParamsAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationAccess().getParamsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__1__Impl"


    // $ANTLR start "rule__Annotation__Group_2__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2275:1: rule__Annotation__Group_2__2 : rule__Annotation__Group_2__2__Impl rule__Annotation__Group_2__3 ;
    public final void rule__Annotation__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2279:1: ( rule__Annotation__Group_2__2__Impl rule__Annotation__Group_2__3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2280:2: rule__Annotation__Group_2__2__Impl rule__Annotation__Group_2__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__2__Impl_in_rule__Annotation__Group_2__24745);
            rule__Annotation__Group_2__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__3_in_rule__Annotation__Group_2__24748);
            rule__Annotation__Group_2__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__2"


    // $ANTLR start "rule__Annotation__Group_2__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2287:1: rule__Annotation__Group_2__2__Impl : ( ( rule__Annotation__Group_2_2__0 )* ) ;
    public final void rule__Annotation__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2291:1: ( ( ( rule__Annotation__Group_2_2__0 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2292:1: ( ( rule__Annotation__Group_2_2__0 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2292:1: ( ( rule__Annotation__Group_2_2__0 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2293:1: ( rule__Annotation__Group_2_2__0 )*
            {
             before(grammarAccess.getAnnotationAccess().getGroup_2_2()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2294:1: ( rule__Annotation__Group_2_2__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==53) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2294:2: rule__Annotation__Group_2_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2_2__0_in_rule__Annotation__Group_2__2__Impl4775);
            	    rule__Annotation__Group_2_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getAnnotationAccess().getGroup_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__2__Impl"


    // $ANTLR start "rule__Annotation__Group_2__3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2304:1: rule__Annotation__Group_2__3 : rule__Annotation__Group_2__3__Impl ;
    public final void rule__Annotation__Group_2__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2308:1: ( rule__Annotation__Group_2__3__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2309:2: rule__Annotation__Group_2__3__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2__3__Impl_in_rule__Annotation__Group_2__34806);
            rule__Annotation__Group_2__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__3"


    // $ANTLR start "rule__Annotation__Group_2__3__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2315:1: rule__Annotation__Group_2__3__Impl : ( ')' ) ;
    public final void rule__Annotation__Group_2__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2319:1: ( ( ')' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2320:1: ( ')' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2320:1: ( ')' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2321:1: ')'
            {
             before(grammarAccess.getAnnotationAccess().getRightParenthesisKeyword_2_3()); 
            match(input,52,FollowSets000.FOLLOW_52_in_rule__Annotation__Group_2__3__Impl4834); 
             after(grammarAccess.getAnnotationAccess().getRightParenthesisKeyword_2_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2__3__Impl"


    // $ANTLR start "rule__Annotation__Group_2_2__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2342:1: rule__Annotation__Group_2_2__0 : rule__Annotation__Group_2_2__0__Impl rule__Annotation__Group_2_2__1 ;
    public final void rule__Annotation__Group_2_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2346:1: ( rule__Annotation__Group_2_2__0__Impl rule__Annotation__Group_2_2__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2347:2: rule__Annotation__Group_2_2__0__Impl rule__Annotation__Group_2_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2_2__0__Impl_in_rule__Annotation__Group_2_2__04873);
            rule__Annotation__Group_2_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2_2__1_in_rule__Annotation__Group_2_2__04876);
            rule__Annotation__Group_2_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2_2__0"


    // $ANTLR start "rule__Annotation__Group_2_2__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2354:1: rule__Annotation__Group_2_2__0__Impl : ( ',' ) ;
    public final void rule__Annotation__Group_2_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2358:1: ( ( ',' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2359:1: ( ',' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2359:1: ( ',' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2360:1: ','
            {
             before(grammarAccess.getAnnotationAccess().getCommaKeyword_2_2_0()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__Annotation__Group_2_2__0__Impl4904); 
             after(grammarAccess.getAnnotationAccess().getCommaKeyword_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2_2__0__Impl"


    // $ANTLR start "rule__Annotation__Group_2_2__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2373:1: rule__Annotation__Group_2_2__1 : rule__Annotation__Group_2_2__1__Impl ;
    public final void rule__Annotation__Group_2_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2377:1: ( rule__Annotation__Group_2_2__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2378:2: rule__Annotation__Group_2_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__Group_2_2__1__Impl_in_rule__Annotation__Group_2_2__14935);
            rule__Annotation__Group_2_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2_2__1"


    // $ANTLR start "rule__Annotation__Group_2_2__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2384:1: rule__Annotation__Group_2_2__1__Impl : ( ( rule__Annotation__ParamsAssignment_2_2_1 ) ) ;
    public final void rule__Annotation__Group_2_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2388:1: ( ( ( rule__Annotation__ParamsAssignment_2_2_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2389:1: ( ( rule__Annotation__ParamsAssignment_2_2_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2389:1: ( ( rule__Annotation__ParamsAssignment_2_2_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2390:1: ( rule__Annotation__ParamsAssignment_2_2_1 )
            {
             before(grammarAccess.getAnnotationAccess().getParamsAssignment_2_2_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2391:1: ( rule__Annotation__ParamsAssignment_2_2_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2391:2: rule__Annotation__ParamsAssignment_2_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Annotation__ParamsAssignment_2_2_1_in_rule__Annotation__Group_2_2__1__Impl4962);
            rule__Annotation__ParamsAssignment_2_2_1();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationAccess().getParamsAssignment_2_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__Group_2_2__1__Impl"


    // $ANTLR start "rule__AnnotationParam__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2405:1: rule__AnnotationParam__Group__0 : rule__AnnotationParam__Group__0__Impl rule__AnnotationParam__Group__1 ;
    public final void rule__AnnotationParam__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2409:1: ( rule__AnnotationParam__Group__0__Impl rule__AnnotationParam__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2410:2: rule__AnnotationParam__Group__0__Impl rule__AnnotationParam__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__Group__0__Impl_in_rule__AnnotationParam__Group__04996);
            rule__AnnotationParam__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__Group__1_in_rule__AnnotationParam__Group__04999);
            rule__AnnotationParam__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__Group__0"


    // $ANTLR start "rule__AnnotationParam__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2417:1: rule__AnnotationParam__Group__0__Impl : ( ( rule__AnnotationParam__NameAssignment_0 ) ) ;
    public final void rule__AnnotationParam__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2421:1: ( ( ( rule__AnnotationParam__NameAssignment_0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2422:1: ( ( rule__AnnotationParam__NameAssignment_0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2422:1: ( ( rule__AnnotationParam__NameAssignment_0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2423:1: ( rule__AnnotationParam__NameAssignment_0 )
            {
             before(grammarAccess.getAnnotationParamAccess().getNameAssignment_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2424:1: ( rule__AnnotationParam__NameAssignment_0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2424:2: rule__AnnotationParam__NameAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__NameAssignment_0_in_rule__AnnotationParam__Group__0__Impl5026);
            rule__AnnotationParam__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationParamAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__Group__0__Impl"


    // $ANTLR start "rule__AnnotationParam__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2434:1: rule__AnnotationParam__Group__1 : rule__AnnotationParam__Group__1__Impl ;
    public final void rule__AnnotationParam__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2438:1: ( rule__AnnotationParam__Group__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2439:2: rule__AnnotationParam__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__Group__1__Impl_in_rule__AnnotationParam__Group__15056);
            rule__AnnotationParam__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__Group__1"


    // $ANTLR start "rule__AnnotationParam__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2445:1: rule__AnnotationParam__Group__1__Impl : ( ( rule__AnnotationParam__ParamValueAssignment_1 ) ) ;
    public final void rule__AnnotationParam__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2449:1: ( ( ( rule__AnnotationParam__ParamValueAssignment_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2450:1: ( ( rule__AnnotationParam__ParamValueAssignment_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2450:1: ( ( rule__AnnotationParam__ParamValueAssignment_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2451:1: ( rule__AnnotationParam__ParamValueAssignment_1 )
            {
             before(grammarAccess.getAnnotationParamAccess().getParamValueAssignment_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2452:1: ( rule__AnnotationParam__ParamValueAssignment_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2452:2: rule__AnnotationParam__ParamValueAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__AnnotationParam__ParamValueAssignment_1_in_rule__AnnotationParam__Group__1__Impl5083);
            rule__AnnotationParam__ParamValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAnnotationParamAccess().getParamValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__Group__1__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2466:1: rule__ObjectValueAnnotationParam__Group__0 : rule__ObjectValueAnnotationParam__Group__0__Impl rule__ObjectValueAnnotationParam__Group__1 ;
    public final void rule__ObjectValueAnnotationParam__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2470:1: ( rule__ObjectValueAnnotationParam__Group__0__Impl rule__ObjectValueAnnotationParam__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2471:2: rule__ObjectValueAnnotationParam__Group__0__Impl rule__ObjectValueAnnotationParam__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__0__Impl_in_rule__ObjectValueAnnotationParam__Group__05117);
            rule__ObjectValueAnnotationParam__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__1_in_rule__ObjectValueAnnotationParam__Group__05120);
            rule__ObjectValueAnnotationParam__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__0"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2478:1: rule__ObjectValueAnnotationParam__Group__0__Impl : ( '->' ) ;
    public final void rule__ObjectValueAnnotationParam__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2482:1: ( ( '->' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2483:1: ( '->' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2483:1: ( '->' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2484:1: '->'
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getHyphenMinusGreaterThanSignKeyword_0()); 
            match(input,54,FollowSets000.FOLLOW_54_in_rule__ObjectValueAnnotationParam__Group__0__Impl5148); 
             after(grammarAccess.getObjectValueAnnotationParamAccess().getHyphenMinusGreaterThanSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__0__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2497:1: rule__ObjectValueAnnotationParam__Group__1 : rule__ObjectValueAnnotationParam__Group__1__Impl rule__ObjectValueAnnotationParam__Group__2 ;
    public final void rule__ObjectValueAnnotationParam__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2501:1: ( rule__ObjectValueAnnotationParam__Group__1__Impl rule__ObjectValueAnnotationParam__Group__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2502:2: rule__ObjectValueAnnotationParam__Group__1__Impl rule__ObjectValueAnnotationParam__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__1__Impl_in_rule__ObjectValueAnnotationParam__Group__15179);
            rule__ObjectValueAnnotationParam__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__2_in_rule__ObjectValueAnnotationParam__Group__15182);
            rule__ObjectValueAnnotationParam__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__1"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2509:1: rule__ObjectValueAnnotationParam__Group__1__Impl : ( ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 ) ) ;
    public final void rule__ObjectValueAnnotationParam__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2513:1: ( ( ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2514:1: ( ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2514:1: ( ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2515:1: ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getObjectAssignment_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2516:1: ( rule__ObjectValueAnnotationParam__ObjectAssignment_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2516:2: rule__ObjectValueAnnotationParam__ObjectAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__ObjectAssignment_1_in_rule__ObjectValueAnnotationParam__Group__1__Impl5209);
            rule__ObjectValueAnnotationParam__ObjectAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getObjectAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__1__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2526:1: rule__ObjectValueAnnotationParam__Group__2 : rule__ObjectValueAnnotationParam__Group__2__Impl ;
    public final void rule__ObjectValueAnnotationParam__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2530:1: ( rule__ObjectValueAnnotationParam__Group__2__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2531:2: rule__ObjectValueAnnotationParam__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group__2__Impl_in_rule__ObjectValueAnnotationParam__Group__25239);
            rule__ObjectValueAnnotationParam__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__2"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2537:1: rule__ObjectValueAnnotationParam__Group__2__Impl : ( ( rule__ObjectValueAnnotationParam__Group_2__0 )? ) ;
    public final void rule__ObjectValueAnnotationParam__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2541:1: ( ( ( rule__ObjectValueAnnotationParam__Group_2__0 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2542:1: ( ( rule__ObjectValueAnnotationParam__Group_2__0 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2542:1: ( ( rule__ObjectValueAnnotationParam__Group_2__0 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2543:1: ( rule__ObjectValueAnnotationParam__Group_2__0 )?
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getGroup_2()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2544:1: ( rule__ObjectValueAnnotationParam__Group_2__0 )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==55) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2544:2: rule__ObjectValueAnnotationParam__Group_2__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group_2__0_in_rule__ObjectValueAnnotationParam__Group__2__Impl5266);
                    rule__ObjectValueAnnotationParam__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group__2__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group_2__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2560:1: rule__ObjectValueAnnotationParam__Group_2__0 : rule__ObjectValueAnnotationParam__Group_2__0__Impl rule__ObjectValueAnnotationParam__Group_2__1 ;
    public final void rule__ObjectValueAnnotationParam__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2564:1: ( rule__ObjectValueAnnotationParam__Group_2__0__Impl rule__ObjectValueAnnotationParam__Group_2__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2565:2: rule__ObjectValueAnnotationParam__Group_2__0__Impl rule__ObjectValueAnnotationParam__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group_2__0__Impl_in_rule__ObjectValueAnnotationParam__Group_2__05303);
            rule__ObjectValueAnnotationParam__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group_2__1_in_rule__ObjectValueAnnotationParam__Group_2__05306);
            rule__ObjectValueAnnotationParam__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group_2__0"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group_2__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2572:1: rule__ObjectValueAnnotationParam__Group_2__0__Impl : ( '.' ) ;
    public final void rule__ObjectValueAnnotationParam__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2576:1: ( ( '.' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2577:1: ( '.' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2577:1: ( '.' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2578:1: '.'
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getFullStopKeyword_2_0()); 
            match(input,55,FollowSets000.FOLLOW_55_in_rule__ObjectValueAnnotationParam__Group_2__0__Impl5334); 
             after(grammarAccess.getObjectValueAnnotationParamAccess().getFullStopKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group_2__0__Impl"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group_2__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2591:1: rule__ObjectValueAnnotationParam__Group_2__1 : rule__ObjectValueAnnotationParam__Group_2__1__Impl ;
    public final void rule__ObjectValueAnnotationParam__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2595:1: ( rule__ObjectValueAnnotationParam__Group_2__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2596:2: rule__ObjectValueAnnotationParam__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__Group_2__1__Impl_in_rule__ObjectValueAnnotationParam__Group_2__15365);
            rule__ObjectValueAnnotationParam__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group_2__1"


    // $ANTLR start "rule__ObjectValueAnnotationParam__Group_2__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2602:1: rule__ObjectValueAnnotationParam__Group_2__1__Impl : ( ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 ) ) ;
    public final void rule__ObjectValueAnnotationParam__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2606:1: ( ( ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2607:1: ( ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2607:1: ( ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2608:1: ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureAssignment_2_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2609:1: ( rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2609:2: rule__ObjectValueAnnotationParam__FeatureAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__ObjectValueAnnotationParam__FeatureAssignment_2_1_in_rule__ObjectValueAnnotationParam__Group_2__1__Impl5392);
            rule__ObjectValueAnnotationParam__FeatureAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__Group_2__1__Impl"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2623:1: rule__PrimitiveValueAnnotationParam__Group__0 : rule__PrimitiveValueAnnotationParam__Group__0__Impl rule__PrimitiveValueAnnotationParam__Group__1 ;
    public final void rule__PrimitiveValueAnnotationParam__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2627:1: ( rule__PrimitiveValueAnnotationParam__Group__0__Impl rule__PrimitiveValueAnnotationParam__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2628:2: rule__PrimitiveValueAnnotationParam__Group__0__Impl rule__PrimitiveValueAnnotationParam__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__Group__0__Impl_in_rule__PrimitiveValueAnnotationParam__Group__05426);
            rule__PrimitiveValueAnnotationParam__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__Group__1_in_rule__PrimitiveValueAnnotationParam__Group__05429);
            rule__PrimitiveValueAnnotationParam__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__Group__0"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2635:1: rule__PrimitiveValueAnnotationParam__Group__0__Impl : ( '=' ) ;
    public final void rule__PrimitiveValueAnnotationParam__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2639:1: ( ( '=' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2640:1: ( '=' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2640:1: ( '=' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2641:1: '='
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamAccess().getEqualsSignKeyword_0()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__PrimitiveValueAnnotationParam__Group__0__Impl5457); 
             after(grammarAccess.getPrimitiveValueAnnotationParamAccess().getEqualsSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__Group__0__Impl"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2654:1: rule__PrimitiveValueAnnotationParam__Group__1 : rule__PrimitiveValueAnnotationParam__Group__1__Impl ;
    public final void rule__PrimitiveValueAnnotationParam__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2658:1: ( rule__PrimitiveValueAnnotationParam__Group__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2659:2: rule__PrimitiveValueAnnotationParam__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__Group__1__Impl_in_rule__PrimitiveValueAnnotationParam__Group__15488);
            rule__PrimitiveValueAnnotationParam__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__Group__1"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2665:1: rule__PrimitiveValueAnnotationParam__Group__1__Impl : ( ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 ) ) ;
    public final void rule__PrimitiveValueAnnotationParam__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2669:1: ( ( ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2670:1: ( ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2670:1: ( ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2671:1: ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 )
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValueAssignment_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2672:1: ( rule__PrimitiveValueAnnotationParam__ValueAssignment_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2672:2: rule__PrimitiveValueAnnotationParam__ValueAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__PrimitiveValueAnnotationParam__ValueAssignment_1_in_rule__PrimitiveValueAnnotationParam__Group__1__Impl5515);
            rule__PrimitiveValueAnnotationParam__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2686:1: rule__Attribute__Group__0 : rule__Attribute__Group__0__Impl rule__Attribute__Group__1 ;
    public final void rule__Attribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2690:1: ( rule__Attribute__Group__0__Impl rule__Attribute__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2691:2: rule__Attribute__Group__0__Impl rule__Attribute__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__0__Impl_in_rule__Attribute__Group__05549);
            rule__Attribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__1_in_rule__Attribute__Group__05552);
            rule__Attribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0"


    // $ANTLR start "rule__Attribute__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2698:1: rule__Attribute__Group__0__Impl : ( () ) ;
    public final void rule__Attribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2702:1: ( ( () ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2703:1: ( () )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2703:1: ( () )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2704:1: ()
            {
             before(grammarAccess.getAttributeAccess().getAttributeAction_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2705:1: ()
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2707:1: 
            {
            }

             after(grammarAccess.getAttributeAccess().getAttributeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__0__Impl"


    // $ANTLR start "rule__Attribute__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2717:1: rule__Attribute__Group__1 : rule__Attribute__Group__1__Impl rule__Attribute__Group__2 ;
    public final void rule__Attribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2721:1: ( rule__Attribute__Group__1__Impl rule__Attribute__Group__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2722:2: rule__Attribute__Group__1__Impl rule__Attribute__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__1__Impl_in_rule__Attribute__Group__15610);
            rule__Attribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__2_in_rule__Attribute__Group__15613);
            rule__Attribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1"


    // $ANTLR start "rule__Attribute__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2729:1: rule__Attribute__Group__1__Impl : ( ( rule__Attribute__Group_1__0 )? ) ;
    public final void rule__Attribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2733:1: ( ( ( rule__Attribute__Group_1__0 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2734:1: ( ( rule__Attribute__Group_1__0 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2734:1: ( ( rule__Attribute__Group_1__0 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2735:1: ( rule__Attribute__Group_1__0 )?
            {
             before(grammarAccess.getAttributeAccess().getGroup_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2736:1: ( rule__Attribute__Group_1__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==50) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2736:2: rule__Attribute__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_1__0_in_rule__Attribute__Group__1__Impl5640);
                    rule__Attribute__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getAttributeAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__1__Impl"


    // $ANTLR start "rule__Attribute__Group__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2746:1: rule__Attribute__Group__2 : rule__Attribute__Group__2__Impl rule__Attribute__Group__3 ;
    public final void rule__Attribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2750:1: ( rule__Attribute__Group__2__Impl rule__Attribute__Group__3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2751:2: rule__Attribute__Group__2__Impl rule__Attribute__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__2__Impl_in_rule__Attribute__Group__25671);
            rule__Attribute__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__3_in_rule__Attribute__Group__25674);
            rule__Attribute__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2"


    // $ANTLR start "rule__Attribute__Group__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2758:1: rule__Attribute__Group__2__Impl : ( 'attr' ) ;
    public final void rule__Attribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2762:1: ( ( 'attr' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2763:1: ( 'attr' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2763:1: ( 'attr' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2764:1: 'attr'
            {
             before(grammarAccess.getAttributeAccess().getAttrKeyword_2()); 
            match(input,57,FollowSets000.FOLLOW_57_in_rule__Attribute__Group__2__Impl5702); 
             after(grammarAccess.getAttributeAccess().getAttrKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__2__Impl"


    // $ANTLR start "rule__Attribute__Group__3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2777:1: rule__Attribute__Group__3 : rule__Attribute__Group__3__Impl rule__Attribute__Group__4 ;
    public final void rule__Attribute__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2781:1: ( rule__Attribute__Group__3__Impl rule__Attribute__Group__4 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2782:2: rule__Attribute__Group__3__Impl rule__Attribute__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__3__Impl_in_rule__Attribute__Group__35733);
            rule__Attribute__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__4_in_rule__Attribute__Group__35736);
            rule__Attribute__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3"


    // $ANTLR start "rule__Attribute__Group__3__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2789:1: rule__Attribute__Group__3__Impl : ( ( rule__Attribute__NameAssignment_3 ) ) ;
    public final void rule__Attribute__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2793:1: ( ( ( rule__Attribute__NameAssignment_3 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2794:1: ( ( rule__Attribute__NameAssignment_3 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2794:1: ( ( rule__Attribute__NameAssignment_3 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2795:1: ( rule__Attribute__NameAssignment_3 )
            {
             before(grammarAccess.getAttributeAccess().getNameAssignment_3()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2796:1: ( rule__Attribute__NameAssignment_3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2796:2: rule__Attribute__NameAssignment_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__NameAssignment_3_in_rule__Attribute__Group__3__Impl5763);
            rule__Attribute__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__3__Impl"


    // $ANTLR start "rule__Attribute__Group__4"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2806:1: rule__Attribute__Group__4 : rule__Attribute__Group__4__Impl rule__Attribute__Group__5 ;
    public final void rule__Attribute__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2810:1: ( rule__Attribute__Group__4__Impl rule__Attribute__Group__5 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2811:2: rule__Attribute__Group__4__Impl rule__Attribute__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__4__Impl_in_rule__Attribute__Group__45793);
            rule__Attribute__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__5_in_rule__Attribute__Group__45796);
            rule__Attribute__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__4"


    // $ANTLR start "rule__Attribute__Group__4__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2818:1: rule__Attribute__Group__4__Impl : ( '=' ) ;
    public final void rule__Attribute__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2822:1: ( ( '=' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2823:1: ( '=' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2823:1: ( '=' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2824:1: '='
            {
             before(grammarAccess.getAttributeAccess().getEqualsSignKeyword_4()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__Attribute__Group__4__Impl5824); 
             after(grammarAccess.getAttributeAccess().getEqualsSignKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__4__Impl"


    // $ANTLR start "rule__Attribute__Group__5"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2837:1: rule__Attribute__Group__5 : rule__Attribute__Group__5__Impl rule__Attribute__Group__6 ;
    public final void rule__Attribute__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2841:1: ( rule__Attribute__Group__5__Impl rule__Attribute__Group__6 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2842:2: rule__Attribute__Group__5__Impl rule__Attribute__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__5__Impl_in_rule__Attribute__Group__55855);
            rule__Attribute__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__6_in_rule__Attribute__Group__55858);
            rule__Attribute__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__5"


    // $ANTLR start "rule__Attribute__Group__5__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2849:1: rule__Attribute__Group__5__Impl : ( ( rule__Attribute__ValuesAssignment_5 ) ) ;
    public final void rule__Attribute__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2853:1: ( ( ( rule__Attribute__ValuesAssignment_5 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2854:1: ( ( rule__Attribute__ValuesAssignment_5 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2854:1: ( ( rule__Attribute__ValuesAssignment_5 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2855:1: ( rule__Attribute__ValuesAssignment_5 )
            {
             before(grammarAccess.getAttributeAccess().getValuesAssignment_5()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2856:1: ( rule__Attribute__ValuesAssignment_5 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2856:2: rule__Attribute__ValuesAssignment_5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__ValuesAssignment_5_in_rule__Attribute__Group__5__Impl5885);
            rule__Attribute__ValuesAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getValuesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__5__Impl"


    // $ANTLR start "rule__Attribute__Group__6"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2866:1: rule__Attribute__Group__6 : rule__Attribute__Group__6__Impl ;
    public final void rule__Attribute__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2870:1: ( rule__Attribute__Group__6__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2871:2: rule__Attribute__Group__6__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group__6__Impl_in_rule__Attribute__Group__65915);
            rule__Attribute__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__6"


    // $ANTLR start "rule__Attribute__Group__6__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2877:1: rule__Attribute__Group__6__Impl : ( ( rule__Attribute__Group_6__0 )* ) ;
    public final void rule__Attribute__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2881:1: ( ( ( rule__Attribute__Group_6__0 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2882:1: ( ( rule__Attribute__Group_6__0 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2882:1: ( ( rule__Attribute__Group_6__0 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2883:1: ( rule__Attribute__Group_6__0 )*
            {
             before(grammarAccess.getAttributeAccess().getGroup_6()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2884:1: ( rule__Attribute__Group_6__0 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==53) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2884:2: rule__Attribute__Group_6__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_6__0_in_rule__Attribute__Group__6__Impl5942);
            	    rule__Attribute__Group_6__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getAttributeAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group__6__Impl"


    // $ANTLR start "rule__Attribute__Group_1__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2908:1: rule__Attribute__Group_1__0 : rule__Attribute__Group_1__0__Impl rule__Attribute__Group_1__1 ;
    public final void rule__Attribute__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2912:1: ( rule__Attribute__Group_1__0__Impl rule__Attribute__Group_1__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2913:2: rule__Attribute__Group_1__0__Impl rule__Attribute__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_1__0__Impl_in_rule__Attribute__Group_1__05987);
            rule__Attribute__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_1__1_in_rule__Attribute__Group_1__05990);
            rule__Attribute__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_1__0"


    // $ANTLR start "rule__Attribute__Group_1__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2920:1: rule__Attribute__Group_1__0__Impl : ( ( rule__Attribute__AnnotationAssignment_1_0 ) ) ;
    public final void rule__Attribute__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2924:1: ( ( ( rule__Attribute__AnnotationAssignment_1_0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2925:1: ( ( rule__Attribute__AnnotationAssignment_1_0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2925:1: ( ( rule__Attribute__AnnotationAssignment_1_0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2926:1: ( rule__Attribute__AnnotationAssignment_1_0 )
            {
             before(grammarAccess.getAttributeAccess().getAnnotationAssignment_1_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2927:1: ( rule__Attribute__AnnotationAssignment_1_0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2927:2: rule__Attribute__AnnotationAssignment_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__AnnotationAssignment_1_0_in_rule__Attribute__Group_1__0__Impl6017);
            rule__Attribute__AnnotationAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getAnnotationAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_1__0__Impl"


    // $ANTLR start "rule__Attribute__Group_1__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2937:1: rule__Attribute__Group_1__1 : rule__Attribute__Group_1__1__Impl ;
    public final void rule__Attribute__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2941:1: ( rule__Attribute__Group_1__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2942:2: rule__Attribute__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_1__1__Impl_in_rule__Attribute__Group_1__16047);
            rule__Attribute__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_1__1"


    // $ANTLR start "rule__Attribute__Group_1__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2948:1: rule__Attribute__Group_1__1__Impl : ( ( rule__Attribute__AnnotationAssignment_1_1 )* ) ;
    public final void rule__Attribute__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2952:1: ( ( ( rule__Attribute__AnnotationAssignment_1_1 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2953:1: ( ( rule__Attribute__AnnotationAssignment_1_1 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2953:1: ( ( rule__Attribute__AnnotationAssignment_1_1 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2954:1: ( rule__Attribute__AnnotationAssignment_1_1 )*
            {
             before(grammarAccess.getAttributeAccess().getAnnotationAssignment_1_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2955:1: ( rule__Attribute__AnnotationAssignment_1_1 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==50) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2955:2: rule__Attribute__AnnotationAssignment_1_1
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Attribute__AnnotationAssignment_1_1_in_rule__Attribute__Group_1__1__Impl6074);
            	    rule__Attribute__AnnotationAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getAttributeAccess().getAnnotationAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_1__1__Impl"


    // $ANTLR start "rule__Attribute__Group_6__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2969:1: rule__Attribute__Group_6__0 : rule__Attribute__Group_6__0__Impl rule__Attribute__Group_6__1 ;
    public final void rule__Attribute__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2973:1: ( rule__Attribute__Group_6__0__Impl rule__Attribute__Group_6__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2974:2: rule__Attribute__Group_6__0__Impl rule__Attribute__Group_6__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_6__0__Impl_in_rule__Attribute__Group_6__06109);
            rule__Attribute__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_6__1_in_rule__Attribute__Group_6__06112);
            rule__Attribute__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_6__0"


    // $ANTLR start "rule__Attribute__Group_6__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2981:1: rule__Attribute__Group_6__0__Impl : ( ',' ) ;
    public final void rule__Attribute__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2985:1: ( ( ',' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2986:1: ( ',' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2986:1: ( ',' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:2987:1: ','
            {
             before(grammarAccess.getAttributeAccess().getCommaKeyword_6_0()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__Attribute__Group_6__0__Impl6140); 
             after(grammarAccess.getAttributeAccess().getCommaKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_6__0__Impl"


    // $ANTLR start "rule__Attribute__Group_6__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3000:1: rule__Attribute__Group_6__1 : rule__Attribute__Group_6__1__Impl ;
    public final void rule__Attribute__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3004:1: ( rule__Attribute__Group_6__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3005:2: rule__Attribute__Group_6__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__Group_6__1__Impl_in_rule__Attribute__Group_6__16171);
            rule__Attribute__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_6__1"


    // $ANTLR start "rule__Attribute__Group_6__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3011:1: rule__Attribute__Group_6__1__Impl : ( ( rule__Attribute__ValuesAssignment_6_1 ) ) ;
    public final void rule__Attribute__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3015:1: ( ( ( rule__Attribute__ValuesAssignment_6_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3016:1: ( ( rule__Attribute__ValuesAssignment_6_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3016:1: ( ( rule__Attribute__ValuesAssignment_6_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3017:1: ( rule__Attribute__ValuesAssignment_6_1 )
            {
             before(grammarAccess.getAttributeAccess().getValuesAssignment_6_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3018:1: ( rule__Attribute__ValuesAssignment_6_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3018:2: rule__Attribute__ValuesAssignment_6_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Attribute__ValuesAssignment_6_1_in_rule__Attribute__Group_6__1__Impl6198);
            rule__Attribute__ValuesAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getAttributeAccess().getValuesAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__Group_6__1__Impl"


    // $ANTLR start "rule__Reference__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3032:1: rule__Reference__Group__0 : rule__Reference__Group__0__Impl rule__Reference__Group__1 ;
    public final void rule__Reference__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3036:1: ( rule__Reference__Group__0__Impl rule__Reference__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3037:2: rule__Reference__Group__0__Impl rule__Reference__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__0__Impl_in_rule__Reference__Group__06232);
            rule__Reference__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__1_in_rule__Reference__Group__06235);
            rule__Reference__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__0"


    // $ANTLR start "rule__Reference__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3044:1: rule__Reference__Group__0__Impl : ( () ) ;
    public final void rule__Reference__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3048:1: ( ( () ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3049:1: ( () )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3049:1: ( () )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3050:1: ()
            {
             before(grammarAccess.getReferenceAccess().getReferenceAction_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3051:1: ()
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3053:1: 
            {
            }

             after(grammarAccess.getReferenceAccess().getReferenceAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__0__Impl"


    // $ANTLR start "rule__Reference__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3063:1: rule__Reference__Group__1 : rule__Reference__Group__1__Impl rule__Reference__Group__2 ;
    public final void rule__Reference__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3067:1: ( rule__Reference__Group__1__Impl rule__Reference__Group__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3068:2: rule__Reference__Group__1__Impl rule__Reference__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__1__Impl_in_rule__Reference__Group__16293);
            rule__Reference__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__2_in_rule__Reference__Group__16296);
            rule__Reference__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__1"


    // $ANTLR start "rule__Reference__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3075:1: rule__Reference__Group__1__Impl : ( ( rule__Reference__Group_1__0 )? ) ;
    public final void rule__Reference__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3079:1: ( ( ( rule__Reference__Group_1__0 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3080:1: ( ( rule__Reference__Group_1__0 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3080:1: ( ( rule__Reference__Group_1__0 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3081:1: ( rule__Reference__Group_1__0 )?
            {
             before(grammarAccess.getReferenceAccess().getGroup_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3082:1: ( rule__Reference__Group_1__0 )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==50) ) {
                int LA28_1 = input.LA(2);

                if ( ((LA28_1>=RULE_STRING && LA28_1<=RULE_ID)) ) {
                    alt28=1;
                }
            }
            switch (alt28) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3082:2: rule__Reference__Group_1__0
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_1__0_in_rule__Reference__Group__1__Impl6323);
                    rule__Reference__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReferenceAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__1__Impl"


    // $ANTLR start "rule__Reference__Group__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3092:1: rule__Reference__Group__2 : rule__Reference__Group__2__Impl rule__Reference__Group__3 ;
    public final void rule__Reference__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3096:1: ( rule__Reference__Group__2__Impl rule__Reference__Group__3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3097:2: rule__Reference__Group__2__Impl rule__Reference__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__2__Impl_in_rule__Reference__Group__26354);
            rule__Reference__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__3_in_rule__Reference__Group__26357);
            rule__Reference__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__2"


    // $ANTLR start "rule__Reference__Group__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3104:1: rule__Reference__Group__2__Impl : ( ( rule__Reference__GraphicPropertiesAssignment_2 )? ) ;
    public final void rule__Reference__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3108:1: ( ( ( rule__Reference__GraphicPropertiesAssignment_2 )? ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3109:1: ( ( rule__Reference__GraphicPropertiesAssignment_2 )? )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3109:1: ( ( rule__Reference__GraphicPropertiesAssignment_2 )? )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3110:1: ( rule__Reference__GraphicPropertiesAssignment_2 )?
            {
             before(grammarAccess.getReferenceAccess().getGraphicPropertiesAssignment_2()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3111:1: ( rule__Reference__GraphicPropertiesAssignment_2 )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==50) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3111:2: rule__Reference__GraphicPropertiesAssignment_2
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Reference__GraphicPropertiesAssignment_2_in_rule__Reference__Group__2__Impl6384);
                    rule__Reference__GraphicPropertiesAssignment_2();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReferenceAccess().getGraphicPropertiesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__2__Impl"


    // $ANTLR start "rule__Reference__Group__3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3121:1: rule__Reference__Group__3 : rule__Reference__Group__3__Impl rule__Reference__Group__4 ;
    public final void rule__Reference__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3125:1: ( rule__Reference__Group__3__Impl rule__Reference__Group__4 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3126:2: rule__Reference__Group__3__Impl rule__Reference__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__3__Impl_in_rule__Reference__Group__36415);
            rule__Reference__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__4_in_rule__Reference__Group__36418);
            rule__Reference__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__3"


    // $ANTLR start "rule__Reference__Group__3__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3133:1: rule__Reference__Group__3__Impl : ( ( rule__Reference__Alternatives_3 ) ) ;
    public final void rule__Reference__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3137:1: ( ( ( rule__Reference__Alternatives_3 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3138:1: ( ( rule__Reference__Alternatives_3 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3138:1: ( ( rule__Reference__Alternatives_3 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3139:1: ( rule__Reference__Alternatives_3 )
            {
             before(grammarAccess.getReferenceAccess().getAlternatives_3()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3140:1: ( rule__Reference__Alternatives_3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3140:2: rule__Reference__Alternatives_3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Alternatives_3_in_rule__Reference__Group__3__Impl6445);
            rule__Reference__Alternatives_3();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getAlternatives_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__3__Impl"


    // $ANTLR start "rule__Reference__Group__4"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3150:1: rule__Reference__Group__4 : rule__Reference__Group__4__Impl rule__Reference__Group__5 ;
    public final void rule__Reference__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3154:1: ( rule__Reference__Group__4__Impl rule__Reference__Group__5 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3155:2: rule__Reference__Group__4__Impl rule__Reference__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__4__Impl_in_rule__Reference__Group__46475);
            rule__Reference__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__5_in_rule__Reference__Group__46478);
            rule__Reference__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__4"


    // $ANTLR start "rule__Reference__Group__4__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3162:1: rule__Reference__Group__4__Impl : ( ( rule__Reference__NameAssignment_4 ) ) ;
    public final void rule__Reference__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3166:1: ( ( ( rule__Reference__NameAssignment_4 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3167:1: ( ( rule__Reference__NameAssignment_4 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3167:1: ( ( rule__Reference__NameAssignment_4 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3168:1: ( rule__Reference__NameAssignment_4 )
            {
             before(grammarAccess.getReferenceAccess().getNameAssignment_4()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3169:1: ( rule__Reference__NameAssignment_4 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3169:2: rule__Reference__NameAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__NameAssignment_4_in_rule__Reference__Group__4__Impl6505);
            rule__Reference__NameAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getNameAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__4__Impl"


    // $ANTLR start "rule__Reference__Group__5"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3179:1: rule__Reference__Group__5 : rule__Reference__Group__5__Impl rule__Reference__Group__6 ;
    public final void rule__Reference__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3183:1: ( rule__Reference__Group__5__Impl rule__Reference__Group__6 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3184:2: rule__Reference__Group__5__Impl rule__Reference__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__5__Impl_in_rule__Reference__Group__56535);
            rule__Reference__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__6_in_rule__Reference__Group__56538);
            rule__Reference__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__5"


    // $ANTLR start "rule__Reference__Group__5__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3191:1: rule__Reference__Group__5__Impl : ( '=' ) ;
    public final void rule__Reference__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3195:1: ( ( '=' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3196:1: ( '=' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3196:1: ( '=' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3197:1: '='
            {
             before(grammarAccess.getReferenceAccess().getEqualsSignKeyword_5()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__Reference__Group__5__Impl6566); 
             after(grammarAccess.getReferenceAccess().getEqualsSignKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__5__Impl"


    // $ANTLR start "rule__Reference__Group__6"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3210:1: rule__Reference__Group__6 : rule__Reference__Group__6__Impl rule__Reference__Group__7 ;
    public final void rule__Reference__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3214:1: ( rule__Reference__Group__6__Impl rule__Reference__Group__7 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3215:2: rule__Reference__Group__6__Impl rule__Reference__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__6__Impl_in_rule__Reference__Group__66597);
            rule__Reference__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__7_in_rule__Reference__Group__66600);
            rule__Reference__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__6"


    // $ANTLR start "rule__Reference__Group__6__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3222:1: rule__Reference__Group__6__Impl : ( ( rule__Reference__ReferenceAssignment_6 ) ) ;
    public final void rule__Reference__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3226:1: ( ( ( rule__Reference__ReferenceAssignment_6 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3227:1: ( ( rule__Reference__ReferenceAssignment_6 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3227:1: ( ( rule__Reference__ReferenceAssignment_6 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3228:1: ( rule__Reference__ReferenceAssignment_6 )
            {
             before(grammarAccess.getReferenceAccess().getReferenceAssignment_6()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3229:1: ( rule__Reference__ReferenceAssignment_6 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3229:2: rule__Reference__ReferenceAssignment_6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__ReferenceAssignment_6_in_rule__Reference__Group__6__Impl6627);
            rule__Reference__ReferenceAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getReferenceAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__6__Impl"


    // $ANTLR start "rule__Reference__Group__7"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3239:1: rule__Reference__Group__7 : rule__Reference__Group__7__Impl ;
    public final void rule__Reference__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3243:1: ( rule__Reference__Group__7__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3244:2: rule__Reference__Group__7__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group__7__Impl_in_rule__Reference__Group__76657);
            rule__Reference__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__7"


    // $ANTLR start "rule__Reference__Group__7__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3250:1: rule__Reference__Group__7__Impl : ( ( rule__Reference__Group_7__0 )* ) ;
    public final void rule__Reference__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3254:1: ( ( ( rule__Reference__Group_7__0 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3255:1: ( ( rule__Reference__Group_7__0 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3255:1: ( ( rule__Reference__Group_7__0 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3256:1: ( rule__Reference__Group_7__0 )*
            {
             before(grammarAccess.getReferenceAccess().getGroup_7()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3257:1: ( rule__Reference__Group_7__0 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==53) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3257:2: rule__Reference__Group_7__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_7__0_in_rule__Reference__Group__7__Impl6684);
            	    rule__Reference__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

             after(grammarAccess.getReferenceAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group__7__Impl"


    // $ANTLR start "rule__Reference__Group_1__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3283:1: rule__Reference__Group_1__0 : rule__Reference__Group_1__0__Impl rule__Reference__Group_1__1 ;
    public final void rule__Reference__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3287:1: ( rule__Reference__Group_1__0__Impl rule__Reference__Group_1__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3288:2: rule__Reference__Group_1__0__Impl rule__Reference__Group_1__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_1__0__Impl_in_rule__Reference__Group_1__06731);
            rule__Reference__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_1__1_in_rule__Reference__Group_1__06734);
            rule__Reference__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_1__0"


    // $ANTLR start "rule__Reference__Group_1__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3295:1: rule__Reference__Group_1__0__Impl : ( ( rule__Reference__AnnotationAssignment_1_0 ) ) ;
    public final void rule__Reference__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3299:1: ( ( ( rule__Reference__AnnotationAssignment_1_0 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3300:1: ( ( rule__Reference__AnnotationAssignment_1_0 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3300:1: ( ( rule__Reference__AnnotationAssignment_1_0 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3301:1: ( rule__Reference__AnnotationAssignment_1_0 )
            {
             before(grammarAccess.getReferenceAccess().getAnnotationAssignment_1_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3302:1: ( rule__Reference__AnnotationAssignment_1_0 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3302:2: rule__Reference__AnnotationAssignment_1_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__AnnotationAssignment_1_0_in_rule__Reference__Group_1__0__Impl6761);
            rule__Reference__AnnotationAssignment_1_0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getAnnotationAssignment_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_1__0__Impl"


    // $ANTLR start "rule__Reference__Group_1__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3312:1: rule__Reference__Group_1__1 : rule__Reference__Group_1__1__Impl ;
    public final void rule__Reference__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3316:1: ( rule__Reference__Group_1__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3317:2: rule__Reference__Group_1__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_1__1__Impl_in_rule__Reference__Group_1__16791);
            rule__Reference__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_1__1"


    // $ANTLR start "rule__Reference__Group_1__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3323:1: rule__Reference__Group_1__1__Impl : ( ( rule__Reference__AnnotationAssignment_1_1 )* ) ;
    public final void rule__Reference__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3327:1: ( ( ( rule__Reference__AnnotationAssignment_1_1 )* ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3328:1: ( ( rule__Reference__AnnotationAssignment_1_1 )* )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3328:1: ( ( rule__Reference__AnnotationAssignment_1_1 )* )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3329:1: ( rule__Reference__AnnotationAssignment_1_1 )*
            {
             before(grammarAccess.getReferenceAccess().getAnnotationAssignment_1_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3330:1: ( rule__Reference__AnnotationAssignment_1_1 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==50) ) {
                    int LA31_1 = input.LA(2);

                    if ( ((LA31_1>=RULE_STRING && LA31_1<=RULE_ID)) ) {
                        alt31=1;
                    }


                }


                switch (alt31) {
            	case 1 :
            	    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3330:2: rule__Reference__AnnotationAssignment_1_1
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Reference__AnnotationAssignment_1_1_in_rule__Reference__Group_1__1__Impl6818);
            	    rule__Reference__AnnotationAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getReferenceAccess().getAnnotationAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_1__1__Impl"


    // $ANTLR start "rule__Reference__Group_7__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3344:1: rule__Reference__Group_7__0 : rule__Reference__Group_7__0__Impl rule__Reference__Group_7__1 ;
    public final void rule__Reference__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3348:1: ( rule__Reference__Group_7__0__Impl rule__Reference__Group_7__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3349:2: rule__Reference__Group_7__0__Impl rule__Reference__Group_7__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_7__0__Impl_in_rule__Reference__Group_7__06853);
            rule__Reference__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_7__1_in_rule__Reference__Group_7__06856);
            rule__Reference__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_7__0"


    // $ANTLR start "rule__Reference__Group_7__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3356:1: rule__Reference__Group_7__0__Impl : ( ',' ) ;
    public final void rule__Reference__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3360:1: ( ( ',' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3361:1: ( ',' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3361:1: ( ',' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3362:1: ','
            {
             before(grammarAccess.getReferenceAccess().getCommaKeyword_7_0()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__Reference__Group_7__0__Impl6884); 
             after(grammarAccess.getReferenceAccess().getCommaKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_7__0__Impl"


    // $ANTLR start "rule__Reference__Group_7__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3375:1: rule__Reference__Group_7__1 : rule__Reference__Group_7__1__Impl ;
    public final void rule__Reference__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3379:1: ( rule__Reference__Group_7__1__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3380:2: rule__Reference__Group_7__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__Group_7__1__Impl_in_rule__Reference__Group_7__16915);
            rule__Reference__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_7__1"


    // $ANTLR start "rule__Reference__Group_7__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3386:1: rule__Reference__Group_7__1__Impl : ( ( rule__Reference__ReferenceAssignment_7_1 ) ) ;
    public final void rule__Reference__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3390:1: ( ( ( rule__Reference__ReferenceAssignment_7_1 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3391:1: ( ( rule__Reference__ReferenceAssignment_7_1 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3391:1: ( ( rule__Reference__ReferenceAssignment_7_1 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3392:1: ( rule__Reference__ReferenceAssignment_7_1 )
            {
             before(grammarAccess.getReferenceAccess().getReferenceAssignment_7_1()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3393:1: ( rule__Reference__ReferenceAssignment_7_1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3393:2: rule__Reference__ReferenceAssignment_7_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reference__ReferenceAssignment_7_1_in_rule__Reference__Group_7__1__Impl6942);
            rule__Reference__ReferenceAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getReferenceAccess().getReferenceAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__Group_7__1__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3407:1: rule__EdgeProperties__Group__0 : rule__EdgeProperties__Group__0__Impl rule__EdgeProperties__Group__1 ;
    public final void rule__EdgeProperties__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3411:1: ( rule__EdgeProperties__Group__0__Impl rule__EdgeProperties__Group__1 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3412:2: rule__EdgeProperties__Group__0__Impl rule__EdgeProperties__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__0__Impl_in_rule__EdgeProperties__Group__06976);
            rule__EdgeProperties__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__1_in_rule__EdgeProperties__Group__06979);
            rule__EdgeProperties__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__0"


    // $ANTLR start "rule__EdgeProperties__Group__0__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3419:1: rule__EdgeProperties__Group__0__Impl : ( () ) ;
    public final void rule__EdgeProperties__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3423:1: ( ( () ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3424:1: ( () )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3424:1: ( () )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3425:1: ()
            {
             before(grammarAccess.getEdgePropertiesAccess().getEdgePropertiesAction_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3426:1: ()
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3428:1: 
            {
            }

             after(grammarAccess.getEdgePropertiesAccess().getEdgePropertiesAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__0__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3438:1: rule__EdgeProperties__Group__1 : rule__EdgeProperties__Group__1__Impl rule__EdgeProperties__Group__2 ;
    public final void rule__EdgeProperties__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3442:1: ( rule__EdgeProperties__Group__1__Impl rule__EdgeProperties__Group__2 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3443:2: rule__EdgeProperties__Group__1__Impl rule__EdgeProperties__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__1__Impl_in_rule__EdgeProperties__Group__17037);
            rule__EdgeProperties__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__2_in_rule__EdgeProperties__Group__17040);
            rule__EdgeProperties__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__1"


    // $ANTLR start "rule__EdgeProperties__Group__1__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3450:1: rule__EdgeProperties__Group__1__Impl : ( '@' ) ;
    public final void rule__EdgeProperties__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3454:1: ( ( '@' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3455:1: ( '@' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3455:1: ( '@' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3456:1: '@'
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommercialAtKeyword_1()); 
            match(input,50,FollowSets000.FOLLOW_50_in_rule__EdgeProperties__Group__1__Impl7068); 
             after(grammarAccess.getEdgePropertiesAccess().getCommercialAtKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__1__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3469:1: rule__EdgeProperties__Group__2 : rule__EdgeProperties__Group__2__Impl rule__EdgeProperties__Group__3 ;
    public final void rule__EdgeProperties__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3473:1: ( rule__EdgeProperties__Group__2__Impl rule__EdgeProperties__Group__3 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3474:2: rule__EdgeProperties__Group__2__Impl rule__EdgeProperties__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__2__Impl_in_rule__EdgeProperties__Group__27099);
            rule__EdgeProperties__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__3_in_rule__EdgeProperties__Group__27102);
            rule__EdgeProperties__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__2"


    // $ANTLR start "rule__EdgeProperties__Group__2__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3481:1: rule__EdgeProperties__Group__2__Impl : ( 'style' ) ;
    public final void rule__EdgeProperties__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3485:1: ( ( 'style' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3486:1: ( 'style' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3486:1: ( 'style' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3487:1: 'style'
            {
             before(grammarAccess.getEdgePropertiesAccess().getStyleKeyword_2()); 
            match(input,58,FollowSets000.FOLLOW_58_in_rule__EdgeProperties__Group__2__Impl7130); 
             after(grammarAccess.getEdgePropertiesAccess().getStyleKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__2__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3500:1: rule__EdgeProperties__Group__3 : rule__EdgeProperties__Group__3__Impl rule__EdgeProperties__Group__4 ;
    public final void rule__EdgeProperties__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3504:1: ( rule__EdgeProperties__Group__3__Impl rule__EdgeProperties__Group__4 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3505:2: rule__EdgeProperties__Group__3__Impl rule__EdgeProperties__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__3__Impl_in_rule__EdgeProperties__Group__37161);
            rule__EdgeProperties__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__4_in_rule__EdgeProperties__Group__37164);
            rule__EdgeProperties__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__3"


    // $ANTLR start "rule__EdgeProperties__Group__3__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3512:1: rule__EdgeProperties__Group__3__Impl : ( '(' ) ;
    public final void rule__EdgeProperties__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3516:1: ( ( '(' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3517:1: ( '(' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3517:1: ( '(' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3518:1: '('
            {
             before(grammarAccess.getEdgePropertiesAccess().getLeftParenthesisKeyword_3()); 
            match(input,51,FollowSets000.FOLLOW_51_in_rule__EdgeProperties__Group__3__Impl7192); 
             after(grammarAccess.getEdgePropertiesAccess().getLeftParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__3__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__4"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3531:1: rule__EdgeProperties__Group__4 : rule__EdgeProperties__Group__4__Impl rule__EdgeProperties__Group__5 ;
    public final void rule__EdgeProperties__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3535:1: ( rule__EdgeProperties__Group__4__Impl rule__EdgeProperties__Group__5 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3536:2: rule__EdgeProperties__Group__4__Impl rule__EdgeProperties__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__4__Impl_in_rule__EdgeProperties__Group__47223);
            rule__EdgeProperties__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__5_in_rule__EdgeProperties__Group__47226);
            rule__EdgeProperties__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__4"


    // $ANTLR start "rule__EdgeProperties__Group__4__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3543:1: rule__EdgeProperties__Group__4__Impl : ( 'color' ) ;
    public final void rule__EdgeProperties__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3547:1: ( ( 'color' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3548:1: ( 'color' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3548:1: ( 'color' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3549:1: 'color'
            {
             before(grammarAccess.getEdgePropertiesAccess().getColorKeyword_4()); 
            match(input,59,FollowSets000.FOLLOW_59_in_rule__EdgeProperties__Group__4__Impl7254); 
             after(grammarAccess.getEdgePropertiesAccess().getColorKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__4__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__5"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3562:1: rule__EdgeProperties__Group__5 : rule__EdgeProperties__Group__5__Impl rule__EdgeProperties__Group__6 ;
    public final void rule__EdgeProperties__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3566:1: ( rule__EdgeProperties__Group__5__Impl rule__EdgeProperties__Group__6 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3567:2: rule__EdgeProperties__Group__5__Impl rule__EdgeProperties__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__5__Impl_in_rule__EdgeProperties__Group__57285);
            rule__EdgeProperties__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__6_in_rule__EdgeProperties__Group__57288);
            rule__EdgeProperties__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__5"


    // $ANTLR start "rule__EdgeProperties__Group__5__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3574:1: rule__EdgeProperties__Group__5__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3578:1: ( ( '=' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3579:1: ( '=' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3579:1: ( '=' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3580:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_5()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__5__Impl7316); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__5__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__6"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3593:1: rule__EdgeProperties__Group__6 : rule__EdgeProperties__Group__6__Impl rule__EdgeProperties__Group__7 ;
    public final void rule__EdgeProperties__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3597:1: ( rule__EdgeProperties__Group__6__Impl rule__EdgeProperties__Group__7 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3598:2: rule__EdgeProperties__Group__6__Impl rule__EdgeProperties__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__6__Impl_in_rule__EdgeProperties__Group__67347);
            rule__EdgeProperties__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__7_in_rule__EdgeProperties__Group__67350);
            rule__EdgeProperties__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__6"


    // $ANTLR start "rule__EdgeProperties__Group__6__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3605:1: rule__EdgeProperties__Group__6__Impl : ( ( rule__EdgeProperties__ColorAssignment_6 ) ) ;
    public final void rule__EdgeProperties__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3609:1: ( ( ( rule__EdgeProperties__ColorAssignment_6 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3610:1: ( ( rule__EdgeProperties__ColorAssignment_6 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3610:1: ( ( rule__EdgeProperties__ColorAssignment_6 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3611:1: ( rule__EdgeProperties__ColorAssignment_6 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getColorAssignment_6()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3612:1: ( rule__EdgeProperties__ColorAssignment_6 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3612:2: rule__EdgeProperties__ColorAssignment_6
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__ColorAssignment_6_in_rule__EdgeProperties__Group__6__Impl7377);
            rule__EdgeProperties__ColorAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getColorAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__6__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__7"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3622:1: rule__EdgeProperties__Group__7 : rule__EdgeProperties__Group__7__Impl rule__EdgeProperties__Group__8 ;
    public final void rule__EdgeProperties__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3626:1: ( rule__EdgeProperties__Group__7__Impl rule__EdgeProperties__Group__8 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3627:2: rule__EdgeProperties__Group__7__Impl rule__EdgeProperties__Group__8
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__7__Impl_in_rule__EdgeProperties__Group__77407);
            rule__EdgeProperties__Group__7__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__8_in_rule__EdgeProperties__Group__77410);
            rule__EdgeProperties__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__7"


    // $ANTLR start "rule__EdgeProperties__Group__7__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3634:1: rule__EdgeProperties__Group__7__Impl : ( ',' ) ;
    public final void rule__EdgeProperties__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3638:1: ( ( ',' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3639:1: ( ',' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3639:1: ( ',' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3640:1: ','
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_7()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__EdgeProperties__Group__7__Impl7438); 
             after(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__7__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__8"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3653:1: rule__EdgeProperties__Group__8 : rule__EdgeProperties__Group__8__Impl rule__EdgeProperties__Group__9 ;
    public final void rule__EdgeProperties__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3657:1: ( rule__EdgeProperties__Group__8__Impl rule__EdgeProperties__Group__9 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3658:2: rule__EdgeProperties__Group__8__Impl rule__EdgeProperties__Group__9
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__8__Impl_in_rule__EdgeProperties__Group__87469);
            rule__EdgeProperties__Group__8__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__9_in_rule__EdgeProperties__Group__87472);
            rule__EdgeProperties__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__8"


    // $ANTLR start "rule__EdgeProperties__Group__8__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3665:1: rule__EdgeProperties__Group__8__Impl : ( 'width' ) ;
    public final void rule__EdgeProperties__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3669:1: ( ( 'width' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3670:1: ( 'width' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3670:1: ( 'width' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3671:1: 'width'
            {
             before(grammarAccess.getEdgePropertiesAccess().getWidthKeyword_8()); 
            match(input,60,FollowSets000.FOLLOW_60_in_rule__EdgeProperties__Group__8__Impl7500); 
             after(grammarAccess.getEdgePropertiesAccess().getWidthKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__8__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__9"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3684:1: rule__EdgeProperties__Group__9 : rule__EdgeProperties__Group__9__Impl rule__EdgeProperties__Group__10 ;
    public final void rule__EdgeProperties__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3688:1: ( rule__EdgeProperties__Group__9__Impl rule__EdgeProperties__Group__10 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3689:2: rule__EdgeProperties__Group__9__Impl rule__EdgeProperties__Group__10
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__9__Impl_in_rule__EdgeProperties__Group__97531);
            rule__EdgeProperties__Group__9__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__10_in_rule__EdgeProperties__Group__97534);
            rule__EdgeProperties__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__9"


    // $ANTLR start "rule__EdgeProperties__Group__9__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3696:1: rule__EdgeProperties__Group__9__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3700:1: ( ( '=' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3701:1: ( '=' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3701:1: ( '=' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3702:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_9()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__9__Impl7562); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__9__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__10"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3715:1: rule__EdgeProperties__Group__10 : rule__EdgeProperties__Group__10__Impl rule__EdgeProperties__Group__11 ;
    public final void rule__EdgeProperties__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3719:1: ( rule__EdgeProperties__Group__10__Impl rule__EdgeProperties__Group__11 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3720:2: rule__EdgeProperties__Group__10__Impl rule__EdgeProperties__Group__11
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__10__Impl_in_rule__EdgeProperties__Group__107593);
            rule__EdgeProperties__Group__10__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__11_in_rule__EdgeProperties__Group__107596);
            rule__EdgeProperties__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__10"


    // $ANTLR start "rule__EdgeProperties__Group__10__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3727:1: rule__EdgeProperties__Group__10__Impl : ( ( rule__EdgeProperties__WidthAssignment_10 ) ) ;
    public final void rule__EdgeProperties__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3731:1: ( ( ( rule__EdgeProperties__WidthAssignment_10 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3732:1: ( ( rule__EdgeProperties__WidthAssignment_10 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3732:1: ( ( rule__EdgeProperties__WidthAssignment_10 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3733:1: ( rule__EdgeProperties__WidthAssignment_10 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getWidthAssignment_10()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3734:1: ( rule__EdgeProperties__WidthAssignment_10 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3734:2: rule__EdgeProperties__WidthAssignment_10
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__WidthAssignment_10_in_rule__EdgeProperties__Group__10__Impl7623);
            rule__EdgeProperties__WidthAssignment_10();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getWidthAssignment_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__10__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__11"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3744:1: rule__EdgeProperties__Group__11 : rule__EdgeProperties__Group__11__Impl rule__EdgeProperties__Group__12 ;
    public final void rule__EdgeProperties__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3748:1: ( rule__EdgeProperties__Group__11__Impl rule__EdgeProperties__Group__12 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3749:2: rule__EdgeProperties__Group__11__Impl rule__EdgeProperties__Group__12
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__11__Impl_in_rule__EdgeProperties__Group__117653);
            rule__EdgeProperties__Group__11__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__12_in_rule__EdgeProperties__Group__117656);
            rule__EdgeProperties__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__11"


    // $ANTLR start "rule__EdgeProperties__Group__11__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3756:1: rule__EdgeProperties__Group__11__Impl : ( ',' ) ;
    public final void rule__EdgeProperties__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3760:1: ( ( ',' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3761:1: ( ',' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3761:1: ( ',' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3762:1: ','
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_11()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__EdgeProperties__Group__11__Impl7684); 
             after(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__11__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__12"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3775:1: rule__EdgeProperties__Group__12 : rule__EdgeProperties__Group__12__Impl rule__EdgeProperties__Group__13 ;
    public final void rule__EdgeProperties__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3779:1: ( rule__EdgeProperties__Group__12__Impl rule__EdgeProperties__Group__13 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3780:2: rule__EdgeProperties__Group__12__Impl rule__EdgeProperties__Group__13
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__12__Impl_in_rule__EdgeProperties__Group__127715);
            rule__EdgeProperties__Group__12__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__13_in_rule__EdgeProperties__Group__127718);
            rule__EdgeProperties__Group__13();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__12"


    // $ANTLR start "rule__EdgeProperties__Group__12__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3787:1: rule__EdgeProperties__Group__12__Impl : ( 'line' ) ;
    public final void rule__EdgeProperties__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3791:1: ( ( 'line' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3792:1: ( 'line' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3792:1: ( 'line' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3793:1: 'line'
            {
             before(grammarAccess.getEdgePropertiesAccess().getLineKeyword_12()); 
            match(input,17,FollowSets000.FOLLOW_17_in_rule__EdgeProperties__Group__12__Impl7746); 
             after(grammarAccess.getEdgePropertiesAccess().getLineKeyword_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__12__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__13"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3806:1: rule__EdgeProperties__Group__13 : rule__EdgeProperties__Group__13__Impl rule__EdgeProperties__Group__14 ;
    public final void rule__EdgeProperties__Group__13() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3810:1: ( rule__EdgeProperties__Group__13__Impl rule__EdgeProperties__Group__14 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3811:2: rule__EdgeProperties__Group__13__Impl rule__EdgeProperties__Group__14
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__13__Impl_in_rule__EdgeProperties__Group__137777);
            rule__EdgeProperties__Group__13__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__14_in_rule__EdgeProperties__Group__137780);
            rule__EdgeProperties__Group__14();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__13"


    // $ANTLR start "rule__EdgeProperties__Group__13__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3818:1: rule__EdgeProperties__Group__13__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__13__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3822:1: ( ( '=' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3823:1: ( '=' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3823:1: ( '=' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3824:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_13()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__13__Impl7808); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_13()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__13__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__14"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3837:1: rule__EdgeProperties__Group__14 : rule__EdgeProperties__Group__14__Impl rule__EdgeProperties__Group__15 ;
    public final void rule__EdgeProperties__Group__14() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3841:1: ( rule__EdgeProperties__Group__14__Impl rule__EdgeProperties__Group__15 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3842:2: rule__EdgeProperties__Group__14__Impl rule__EdgeProperties__Group__15
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__14__Impl_in_rule__EdgeProperties__Group__147839);
            rule__EdgeProperties__Group__14__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__15_in_rule__EdgeProperties__Group__147842);
            rule__EdgeProperties__Group__15();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__14"


    // $ANTLR start "rule__EdgeProperties__Group__14__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3849:1: rule__EdgeProperties__Group__14__Impl : ( ( rule__EdgeProperties__LineTypeAssignment_14 ) ) ;
    public final void rule__EdgeProperties__Group__14__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3853:1: ( ( ( rule__EdgeProperties__LineTypeAssignment_14 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3854:1: ( ( rule__EdgeProperties__LineTypeAssignment_14 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3854:1: ( ( rule__EdgeProperties__LineTypeAssignment_14 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3855:1: ( rule__EdgeProperties__LineTypeAssignment_14 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getLineTypeAssignment_14()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3856:1: ( rule__EdgeProperties__LineTypeAssignment_14 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3856:2: rule__EdgeProperties__LineTypeAssignment_14
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__LineTypeAssignment_14_in_rule__EdgeProperties__Group__14__Impl7869);
            rule__EdgeProperties__LineTypeAssignment_14();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getLineTypeAssignment_14()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__14__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__15"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3866:1: rule__EdgeProperties__Group__15 : rule__EdgeProperties__Group__15__Impl rule__EdgeProperties__Group__16 ;
    public final void rule__EdgeProperties__Group__15() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3870:1: ( rule__EdgeProperties__Group__15__Impl rule__EdgeProperties__Group__16 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3871:2: rule__EdgeProperties__Group__15__Impl rule__EdgeProperties__Group__16
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__15__Impl_in_rule__EdgeProperties__Group__157899);
            rule__EdgeProperties__Group__15__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__16_in_rule__EdgeProperties__Group__157902);
            rule__EdgeProperties__Group__16();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__15"


    // $ANTLR start "rule__EdgeProperties__Group__15__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3878:1: rule__EdgeProperties__Group__15__Impl : ( ',' ) ;
    public final void rule__EdgeProperties__Group__15__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3882:1: ( ( ',' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3883:1: ( ',' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3883:1: ( ',' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3884:1: ','
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_15()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__EdgeProperties__Group__15__Impl7930); 
             after(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_15()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__15__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__16"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3897:1: rule__EdgeProperties__Group__16 : rule__EdgeProperties__Group__16__Impl rule__EdgeProperties__Group__17 ;
    public final void rule__EdgeProperties__Group__16() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3901:1: ( rule__EdgeProperties__Group__16__Impl rule__EdgeProperties__Group__17 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3902:2: rule__EdgeProperties__Group__16__Impl rule__EdgeProperties__Group__17
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__16__Impl_in_rule__EdgeProperties__Group__167961);
            rule__EdgeProperties__Group__16__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__17_in_rule__EdgeProperties__Group__167964);
            rule__EdgeProperties__Group__17();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__16"


    // $ANTLR start "rule__EdgeProperties__Group__16__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3909:1: rule__EdgeProperties__Group__16__Impl : ( 'source' ) ;
    public final void rule__EdgeProperties__Group__16__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3913:1: ( ( 'source' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3914:1: ( 'source' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3914:1: ( 'source' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3915:1: 'source'
            {
             before(grammarAccess.getEdgePropertiesAccess().getSourceKeyword_16()); 
            match(input,61,FollowSets000.FOLLOW_61_in_rule__EdgeProperties__Group__16__Impl7992); 
             after(grammarAccess.getEdgePropertiesAccess().getSourceKeyword_16()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__16__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__17"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3928:1: rule__EdgeProperties__Group__17 : rule__EdgeProperties__Group__17__Impl rule__EdgeProperties__Group__18 ;
    public final void rule__EdgeProperties__Group__17() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3932:1: ( rule__EdgeProperties__Group__17__Impl rule__EdgeProperties__Group__18 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3933:2: rule__EdgeProperties__Group__17__Impl rule__EdgeProperties__Group__18
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__17__Impl_in_rule__EdgeProperties__Group__178023);
            rule__EdgeProperties__Group__17__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__18_in_rule__EdgeProperties__Group__178026);
            rule__EdgeProperties__Group__18();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__17"


    // $ANTLR start "rule__EdgeProperties__Group__17__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3940:1: rule__EdgeProperties__Group__17__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__17__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3944:1: ( ( '=' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3945:1: ( '=' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3945:1: ( '=' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3946:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_17()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__17__Impl8054); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_17()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__17__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__18"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3959:1: rule__EdgeProperties__Group__18 : rule__EdgeProperties__Group__18__Impl rule__EdgeProperties__Group__19 ;
    public final void rule__EdgeProperties__Group__18() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3963:1: ( rule__EdgeProperties__Group__18__Impl rule__EdgeProperties__Group__19 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3964:2: rule__EdgeProperties__Group__18__Impl rule__EdgeProperties__Group__19
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__18__Impl_in_rule__EdgeProperties__Group__188085);
            rule__EdgeProperties__Group__18__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__19_in_rule__EdgeProperties__Group__188088);
            rule__EdgeProperties__Group__19();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__18"


    // $ANTLR start "rule__EdgeProperties__Group__18__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3971:1: rule__EdgeProperties__Group__18__Impl : ( ( rule__EdgeProperties__SrcArrowAssignment_18 ) ) ;
    public final void rule__EdgeProperties__Group__18__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3975:1: ( ( ( rule__EdgeProperties__SrcArrowAssignment_18 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3976:1: ( ( rule__EdgeProperties__SrcArrowAssignment_18 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3976:1: ( ( rule__EdgeProperties__SrcArrowAssignment_18 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3977:1: ( rule__EdgeProperties__SrcArrowAssignment_18 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getSrcArrowAssignment_18()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3978:1: ( rule__EdgeProperties__SrcArrowAssignment_18 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3978:2: rule__EdgeProperties__SrcArrowAssignment_18
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__SrcArrowAssignment_18_in_rule__EdgeProperties__Group__18__Impl8115);
            rule__EdgeProperties__SrcArrowAssignment_18();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getSrcArrowAssignment_18()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__18__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__19"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3988:1: rule__EdgeProperties__Group__19 : rule__EdgeProperties__Group__19__Impl rule__EdgeProperties__Group__20 ;
    public final void rule__EdgeProperties__Group__19() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3992:1: ( rule__EdgeProperties__Group__19__Impl rule__EdgeProperties__Group__20 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:3993:2: rule__EdgeProperties__Group__19__Impl rule__EdgeProperties__Group__20
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__19__Impl_in_rule__EdgeProperties__Group__198145);
            rule__EdgeProperties__Group__19__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__20_in_rule__EdgeProperties__Group__198148);
            rule__EdgeProperties__Group__20();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__19"


    // $ANTLR start "rule__EdgeProperties__Group__19__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4000:1: rule__EdgeProperties__Group__19__Impl : ( ',' ) ;
    public final void rule__EdgeProperties__Group__19__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4004:1: ( ( ',' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4005:1: ( ',' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4005:1: ( ',' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4006:1: ','
            {
             before(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_19()); 
            match(input,53,FollowSets000.FOLLOW_53_in_rule__EdgeProperties__Group__19__Impl8176); 
             after(grammarAccess.getEdgePropertiesAccess().getCommaKeyword_19()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__19__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__20"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4019:1: rule__EdgeProperties__Group__20 : rule__EdgeProperties__Group__20__Impl rule__EdgeProperties__Group__21 ;
    public final void rule__EdgeProperties__Group__20() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4023:1: ( rule__EdgeProperties__Group__20__Impl rule__EdgeProperties__Group__21 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4024:2: rule__EdgeProperties__Group__20__Impl rule__EdgeProperties__Group__21
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__20__Impl_in_rule__EdgeProperties__Group__208207);
            rule__EdgeProperties__Group__20__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__21_in_rule__EdgeProperties__Group__208210);
            rule__EdgeProperties__Group__21();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__20"


    // $ANTLR start "rule__EdgeProperties__Group__20__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4031:1: rule__EdgeProperties__Group__20__Impl : ( 'target' ) ;
    public final void rule__EdgeProperties__Group__20__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4035:1: ( ( 'target' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4036:1: ( 'target' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4036:1: ( 'target' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4037:1: 'target'
            {
             before(grammarAccess.getEdgePropertiesAccess().getTargetKeyword_20()); 
            match(input,62,FollowSets000.FOLLOW_62_in_rule__EdgeProperties__Group__20__Impl8238); 
             after(grammarAccess.getEdgePropertiesAccess().getTargetKeyword_20()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__20__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__21"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4050:1: rule__EdgeProperties__Group__21 : rule__EdgeProperties__Group__21__Impl rule__EdgeProperties__Group__22 ;
    public final void rule__EdgeProperties__Group__21() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4054:1: ( rule__EdgeProperties__Group__21__Impl rule__EdgeProperties__Group__22 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4055:2: rule__EdgeProperties__Group__21__Impl rule__EdgeProperties__Group__22
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__21__Impl_in_rule__EdgeProperties__Group__218269);
            rule__EdgeProperties__Group__21__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__22_in_rule__EdgeProperties__Group__218272);
            rule__EdgeProperties__Group__22();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__21"


    // $ANTLR start "rule__EdgeProperties__Group__21__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4062:1: rule__EdgeProperties__Group__21__Impl : ( '=' ) ;
    public final void rule__EdgeProperties__Group__21__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4066:1: ( ( '=' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4067:1: ( '=' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4067:1: ( '=' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4068:1: '='
            {
             before(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_21()); 
            match(input,56,FollowSets000.FOLLOW_56_in_rule__EdgeProperties__Group__21__Impl8300); 
             after(grammarAccess.getEdgePropertiesAccess().getEqualsSignKeyword_21()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__21__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__22"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4081:1: rule__EdgeProperties__Group__22 : rule__EdgeProperties__Group__22__Impl rule__EdgeProperties__Group__23 ;
    public final void rule__EdgeProperties__Group__22() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4085:1: ( rule__EdgeProperties__Group__22__Impl rule__EdgeProperties__Group__23 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4086:2: rule__EdgeProperties__Group__22__Impl rule__EdgeProperties__Group__23
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__22__Impl_in_rule__EdgeProperties__Group__228331);
            rule__EdgeProperties__Group__22__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__23_in_rule__EdgeProperties__Group__228334);
            rule__EdgeProperties__Group__23();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__22"


    // $ANTLR start "rule__EdgeProperties__Group__22__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4093:1: rule__EdgeProperties__Group__22__Impl : ( ( rule__EdgeProperties__TgtArrowAssignment_22 ) ) ;
    public final void rule__EdgeProperties__Group__22__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4097:1: ( ( ( rule__EdgeProperties__TgtArrowAssignment_22 ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4098:1: ( ( rule__EdgeProperties__TgtArrowAssignment_22 ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4098:1: ( ( rule__EdgeProperties__TgtArrowAssignment_22 ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4099:1: ( rule__EdgeProperties__TgtArrowAssignment_22 )
            {
             before(grammarAccess.getEdgePropertiesAccess().getTgtArrowAssignment_22()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4100:1: ( rule__EdgeProperties__TgtArrowAssignment_22 )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4100:2: rule__EdgeProperties__TgtArrowAssignment_22
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__TgtArrowAssignment_22_in_rule__EdgeProperties__Group__22__Impl8361);
            rule__EdgeProperties__TgtArrowAssignment_22();

            state._fsp--;


            }

             after(grammarAccess.getEdgePropertiesAccess().getTgtArrowAssignment_22()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__22__Impl"


    // $ANTLR start "rule__EdgeProperties__Group__23"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4110:1: rule__EdgeProperties__Group__23 : rule__EdgeProperties__Group__23__Impl ;
    public final void rule__EdgeProperties__Group__23() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4114:1: ( rule__EdgeProperties__Group__23__Impl )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4115:2: rule__EdgeProperties__Group__23__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__EdgeProperties__Group__23__Impl_in_rule__EdgeProperties__Group__238391);
            rule__EdgeProperties__Group__23__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__23"


    // $ANTLR start "rule__EdgeProperties__Group__23__Impl"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4121:1: rule__EdgeProperties__Group__23__Impl : ( ')' ) ;
    public final void rule__EdgeProperties__Group__23__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4125:1: ( ( ')' ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4126:1: ( ')' )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4126:1: ( ')' )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4127:1: ')'
            {
             before(grammarAccess.getEdgePropertiesAccess().getRightParenthesisKeyword_23()); 
            match(input,52,FollowSets000.FOLLOW_52_in_rule__EdgeProperties__Group__23__Impl8419); 
             after(grammarAccess.getEdgePropertiesAccess().getRightParenthesisKeyword_23()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__Group__23__Impl"


    // $ANTLR start "rule__FragmentModel__NameAssignment_2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4189:1: rule__FragmentModel__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__FragmentModel__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4193:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4194:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4194:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4195:1: ruleEString
            {
             before(grammarAccess.getFragmentModelAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__FragmentModel__NameAssignment_28503);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFragmentModelAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__NameAssignment_2"


    // $ANTLR start "rule__FragmentModel__FragmentsAssignment_3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4204:1: rule__FragmentModel__FragmentsAssignment_3 : ( ruleFragment ) ;
    public final void rule__FragmentModel__FragmentsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4208:1: ( ( ruleFragment ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4209:1: ( ruleFragment )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4209:1: ( ruleFragment )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4210:1: ruleFragment
            {
             before(grammarAccess.getFragmentModelAccess().getFragmentsFragmentParserRuleCall_3_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragment_in_rule__FragmentModel__FragmentsAssignment_38534);
            ruleFragment();

            state._fsp--;

             after(grammarAccess.getFragmentModelAccess().getFragmentsFragmentParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__FragmentsAssignment_3"


    // $ANTLR start "rule__FragmentModel__FragmentsAssignment_4"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4219:1: rule__FragmentModel__FragmentsAssignment_4 : ( ruleFragment ) ;
    public final void rule__FragmentModel__FragmentsAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4223:1: ( ( ruleFragment ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4224:1: ( ruleFragment )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4224:1: ( ruleFragment )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4225:1: ruleFragment
            {
             before(grammarAccess.getFragmentModelAccess().getFragmentsFragmentParserRuleCall_4_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragment_in_rule__FragmentModel__FragmentsAssignment_48565);
            ruleFragment();

            state._fsp--;

             after(grammarAccess.getFragmentModelAccess().getFragmentsFragmentParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FragmentModel__FragmentsAssignment_4"


    // $ANTLR start "rule__Fragment__AnnotationAssignment_1_0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4234:1: rule__Fragment__AnnotationAssignment_1_0 : ( ruleAnnotation ) ;
    public final void rule__Fragment__AnnotationAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4238:1: ( ( ruleAnnotation ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4239:1: ( ruleAnnotation )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4239:1: ( ruleAnnotation )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4240:1: ruleAnnotation
            {
             before(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Fragment__AnnotationAssignment_1_08596);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__AnnotationAssignment_1_0"


    // $ANTLR start "rule__Fragment__AnnotationAssignment_1_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4249:1: rule__Fragment__AnnotationAssignment_1_1 : ( ruleAnnotation ) ;
    public final void rule__Fragment__AnnotationAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4253:1: ( ( ruleAnnotation ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4254:1: ( ruleAnnotation )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4254:1: ( ruleAnnotation )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4255:1: ruleAnnotation
            {
             before(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Fragment__AnnotationAssignment_1_18627);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__AnnotationAssignment_1_1"


    // $ANTLR start "rule__Fragment__FtypeAssignment_2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4264:1: rule__Fragment__FtypeAssignment_2 : ( ruleFragmentType ) ;
    public final void rule__Fragment__FtypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4268:1: ( ( ruleFragmentType ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4269:1: ( ruleFragmentType )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4269:1: ( ruleFragmentType )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4270:1: ruleFragmentType
            {
             before(grammarAccess.getFragmentAccess().getFtypeFragmentTypeEnumRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFragmentType_in_rule__Fragment__FtypeAssignment_28658);
            ruleFragmentType();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getFtypeFragmentTypeEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__FtypeAssignment_2"


    // $ANTLR start "rule__Fragment__TypeAssignment_4_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4279:1: rule__Fragment__TypeAssignment_4_1 : ( ruleEString ) ;
    public final void rule__Fragment__TypeAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4283:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4284:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4284:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4285:1: ruleEString
            {
             before(grammarAccess.getFragmentAccess().getTypeEStringParserRuleCall_4_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Fragment__TypeAssignment_4_18689);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getTypeEStringParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__TypeAssignment_4_1"


    // $ANTLR start "rule__Fragment__NameAssignment_5"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4294:1: rule__Fragment__NameAssignment_5 : ( ruleEString ) ;
    public final void rule__Fragment__NameAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4298:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4299:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4299:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4300:1: ruleEString
            {
             before(grammarAccess.getFragmentAccess().getNameEStringParserRuleCall_5_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Fragment__NameAssignment_58720);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getNameEStringParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__NameAssignment_5"


    // $ANTLR start "rule__Fragment__ObjectsAssignment_7"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4309:1: rule__Fragment__ObjectsAssignment_7 : ( ruleObject ) ;
    public final void rule__Fragment__ObjectsAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4313:1: ( ( ruleObject ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4314:1: ( ruleObject )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4314:1: ( ruleObject )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4315:1: ruleObject
            {
             before(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_7_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_rule__Fragment__ObjectsAssignment_78751);
            ruleObject();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__ObjectsAssignment_7"


    // $ANTLR start "rule__Fragment__ObjectsAssignment_8"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4324:1: rule__Fragment__ObjectsAssignment_8 : ( ruleObject ) ;
    public final void rule__Fragment__ObjectsAssignment_8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4328:1: ( ( ruleObject ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4329:1: ( ruleObject )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4329:1: ( ruleObject )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4330:1: ruleObject
            {
             before(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_8_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleObject_in_rule__Fragment__ObjectsAssignment_88782);
            ruleObject();

            state._fsp--;

             after(grammarAccess.getFragmentAccess().getObjectsObjectParserRuleCall_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Fragment__ObjectsAssignment_8"


    // $ANTLR start "rule__Object__AnnotationAssignment_1_0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4339:1: rule__Object__AnnotationAssignment_1_0 : ( ruleAnnotation ) ;
    public final void rule__Object__AnnotationAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4343:1: ( ( ruleAnnotation ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4344:1: ( ruleAnnotation )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4344:1: ( ruleAnnotation )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4345:1: ruleAnnotation
            {
             before(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Object__AnnotationAssignment_1_08813);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__AnnotationAssignment_1_0"


    // $ANTLR start "rule__Object__AnnotationAssignment_1_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4354:1: rule__Object__AnnotationAssignment_1_1 : ( ruleAnnotation ) ;
    public final void rule__Object__AnnotationAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4358:1: ( ( ruleAnnotation ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4359:1: ( ruleAnnotation )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4359:1: ( ruleAnnotation )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4360:1: ruleAnnotation
            {
             before(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Object__AnnotationAssignment_1_18844);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__AnnotationAssignment_1_1"


    // $ANTLR start "rule__Object__NameAssignment_2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4369:1: rule__Object__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Object__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4373:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4374:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4374:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4375:1: ruleEString
            {
             before(grammarAccess.getObjectAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Object__NameAssignment_28875);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__NameAssignment_2"


    // $ANTLR start "rule__Object__TypeAssignment_4"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4384:1: rule__Object__TypeAssignment_4 : ( ruleEString ) ;
    public final void rule__Object__TypeAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4388:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4389:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4389:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4390:1: ruleEString
            {
             before(grammarAccess.getObjectAccess().getTypeEStringParserRuleCall_4_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Object__TypeAssignment_48906);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getTypeEStringParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__TypeAssignment_4"


    // $ANTLR start "rule__Object__FeaturesAssignment_6_0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4399:1: rule__Object__FeaturesAssignment_6_0 : ( ruleFeature ) ;
    public final void rule__Object__FeaturesAssignment_6_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4403:1: ( ( ruleFeature ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4404:1: ( ruleFeature )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4404:1: ( ruleFeature )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4405:1: ruleFeature
            {
             before(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_rule__Object__FeaturesAssignment_6_08937);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__FeaturesAssignment_6_0"


    // $ANTLR start "rule__Object__FeaturesAssignment_6_2_0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4414:1: rule__Object__FeaturesAssignment_6_2_0 : ( ruleFeature ) ;
    public final void rule__Object__FeaturesAssignment_6_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4418:1: ( ( ruleFeature ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4419:1: ( ruleFeature )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4419:1: ( ruleFeature )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4420:1: ruleFeature
            {
             before(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_2_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_rule__Object__FeaturesAssignment_6_2_08968);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getObjectAccess().getFeaturesFeatureParserRuleCall_6_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__FeaturesAssignment_6_2_0"


    // $ANTLR start "rule__Annotation__NameAssignment_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4429:1: rule__Annotation__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Annotation__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4433:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4434:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4434:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4435:1: ruleEString
            {
             before(grammarAccess.getAnnotationAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Annotation__NameAssignment_18999);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAnnotationAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__NameAssignment_1"


    // $ANTLR start "rule__Annotation__ParamsAssignment_2_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4444:1: rule__Annotation__ParamsAssignment_2_1 : ( ruleAnnotationParam ) ;
    public final void rule__Annotation__ParamsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4448:1: ( ( ruleAnnotationParam ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4449:1: ( ruleAnnotationParam )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4449:1: ( ruleAnnotationParam )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4450:1: ruleAnnotationParam
            {
             before(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_rule__Annotation__ParamsAssignment_2_19030);
            ruleAnnotationParam();

            state._fsp--;

             after(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__ParamsAssignment_2_1"


    // $ANTLR start "rule__Annotation__ParamsAssignment_2_2_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4459:1: rule__Annotation__ParamsAssignment_2_2_1 : ( ruleAnnotationParam ) ;
    public final void rule__Annotation__ParamsAssignment_2_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4463:1: ( ( ruleAnnotationParam ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4464:1: ( ruleAnnotationParam )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4464:1: ( ruleAnnotationParam )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4465:1: ruleAnnotationParam
            {
             before(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_2_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotationParam_in_rule__Annotation__ParamsAssignment_2_2_19061);
            ruleAnnotationParam();

            state._fsp--;

             after(grammarAccess.getAnnotationAccess().getParamsAnnotationParamParserRuleCall_2_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Annotation__ParamsAssignment_2_2_1"


    // $ANTLR start "rule__AnnotationParam__NameAssignment_0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4474:1: rule__AnnotationParam__NameAssignment_0 : ( ruleEString ) ;
    public final void rule__AnnotationParam__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4478:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4479:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4479:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4480:1: ruleEString
            {
             before(grammarAccess.getAnnotationParamAccess().getNameEStringParserRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__AnnotationParam__NameAssignment_09092);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAnnotationParamAccess().getNameEStringParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__NameAssignment_0"


    // $ANTLR start "rule__AnnotationParam__ParamValueAssignment_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4489:1: rule__AnnotationParam__ParamValueAssignment_1 : ( ruleParamValue ) ;
    public final void rule__AnnotationParam__ParamValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4493:1: ( ( ruleParamValue ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4494:1: ( ruleParamValue )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4494:1: ( ruleParamValue )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4495:1: ruleParamValue
            {
             before(grammarAccess.getAnnotationParamAccess().getParamValueParamValueParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleParamValue_in_rule__AnnotationParam__ParamValueAssignment_19123);
            ruleParamValue();

            state._fsp--;

             after(grammarAccess.getAnnotationParamAccess().getParamValueParamValueParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AnnotationParam__ParamValueAssignment_1"


    // $ANTLR start "rule__ObjectValueAnnotationParam__ObjectAssignment_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4504:1: rule__ObjectValueAnnotationParam__ObjectAssignment_1 : ( ( ruleEString ) ) ;
    public final void rule__ObjectValueAnnotationParam__ObjectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4508:1: ( ( ( ruleEString ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4509:1: ( ( ruleEString ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4509:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4510:1: ( ruleEString )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectCrossReference_1_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4511:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4512:1: ruleEString
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectEStringParserRuleCall_1_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__ObjectValueAnnotationParam__ObjectAssignment_19158);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectEStringParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getObjectObjectCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__ObjectAssignment_1"


    // $ANTLR start "rule__ObjectValueAnnotationParam__FeatureAssignment_2_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4523:1: rule__ObjectValueAnnotationParam__FeatureAssignment_2_1 : ( ( ruleEString ) ) ;
    public final void rule__ObjectValueAnnotationParam__FeatureAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4527:1: ( ( ( ruleEString ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4528:1: ( ( ruleEString ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4528:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4529:1: ( ruleEString )
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureCrossReference_2_1_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4530:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4531:1: ruleEString
            {
             before(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureEStringParserRuleCall_2_1_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__ObjectValueAnnotationParam__FeatureAssignment_2_19197);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureEStringParserRuleCall_2_1_0_1()); 

            }

             after(grammarAccess.getObjectValueAnnotationParamAccess().getFeatureFeatureCrossReference_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectValueAnnotationParam__FeatureAssignment_2_1"


    // $ANTLR start "rule__PrimitiveValueAnnotationParam__ValueAssignment_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4542:1: rule__PrimitiveValueAnnotationParam__ValueAssignment_1 : ( rulePrimitiveValue ) ;
    public final void rule__PrimitiveValueAnnotationParam__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4546:1: ( ( rulePrimitiveValue ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4547:1: ( rulePrimitiveValue )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4547:1: ( rulePrimitiveValue )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4548:1: rulePrimitiveValue
            {
             before(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValuePrimitiveValueParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_rule__PrimitiveValueAnnotationParam__ValueAssignment_19232);
            rulePrimitiveValue();

            state._fsp--;

             after(grammarAccess.getPrimitiveValueAnnotationParamAccess().getValuePrimitiveValueParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PrimitiveValueAnnotationParam__ValueAssignment_1"


    // $ANTLR start "rule__Attribute__AnnotationAssignment_1_0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4557:1: rule__Attribute__AnnotationAssignment_1_0 : ( ruleAnnotation ) ;
    public final void rule__Attribute__AnnotationAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4561:1: ( ( ruleAnnotation ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4562:1: ( ruleAnnotation )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4562:1: ( ruleAnnotation )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4563:1: ruleAnnotation
            {
             before(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Attribute__AnnotationAssignment_1_09263);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__AnnotationAssignment_1_0"


    // $ANTLR start "rule__Attribute__AnnotationAssignment_1_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4572:1: rule__Attribute__AnnotationAssignment_1_1 : ( ruleAnnotation ) ;
    public final void rule__Attribute__AnnotationAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4576:1: ( ( ruleAnnotation ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4577:1: ( ruleAnnotation )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4577:1: ( ruleAnnotation )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4578:1: ruleAnnotation
            {
             before(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Attribute__AnnotationAssignment_1_19294);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__AnnotationAssignment_1_1"


    // $ANTLR start "rule__Attribute__NameAssignment_3"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4587:1: rule__Attribute__NameAssignment_3 : ( ruleEString ) ;
    public final void rule__Attribute__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4591:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4592:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4592:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4593:1: ruleEString
            {
             before(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_3_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Attribute__NameAssignment_39325);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__NameAssignment_3"


    // $ANTLR start "rule__Attribute__ValuesAssignment_5"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4602:1: rule__Attribute__ValuesAssignment_5 : ( rulePrimitiveValue ) ;
    public final void rule__Attribute__ValuesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4606:1: ( ( rulePrimitiveValue ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4607:1: ( rulePrimitiveValue )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4607:1: ( rulePrimitiveValue )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4608:1: rulePrimitiveValue
            {
             before(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_5_0()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_rule__Attribute__ValuesAssignment_59356);
            rulePrimitiveValue();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__ValuesAssignment_5"


    // $ANTLR start "rule__Attribute__ValuesAssignment_6_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4617:1: rule__Attribute__ValuesAssignment_6_1 : ( rulePrimitiveValue ) ;
    public final void rule__Attribute__ValuesAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4621:1: ( ( rulePrimitiveValue ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4622:1: ( rulePrimitiveValue )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4622:1: ( rulePrimitiveValue )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4623:1: rulePrimitiveValue
            {
             before(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_6_1_0()); 
            pushFollow(FollowSets000.FOLLOW_rulePrimitiveValue_in_rule__Attribute__ValuesAssignment_6_19387);
            rulePrimitiveValue();

            state._fsp--;

             after(grammarAccess.getAttributeAccess().getValuesPrimitiveValueParserRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Attribute__ValuesAssignment_6_1"


    // $ANTLR start "rule__Reference__AnnotationAssignment_1_0"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4632:1: rule__Reference__AnnotationAssignment_1_0 : ( ruleAnnotation ) ;
    public final void rule__Reference__AnnotationAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4636:1: ( ( ruleAnnotation ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4637:1: ( ruleAnnotation )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4637:1: ( ruleAnnotation )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4638:1: ruleAnnotation
            {
             before(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Reference__AnnotationAssignment_1_09418);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__AnnotationAssignment_1_0"


    // $ANTLR start "rule__Reference__AnnotationAssignment_1_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4647:1: rule__Reference__AnnotationAssignment_1_1 : ( ruleAnnotation ) ;
    public final void rule__Reference__AnnotationAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4651:1: ( ( ruleAnnotation ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4652:1: ( ruleAnnotation )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4652:1: ( ruleAnnotation )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4653:1: ruleAnnotation
            {
             before(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleAnnotation_in_rule__Reference__AnnotationAssignment_1_19449);
            ruleAnnotation();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getAnnotationAnnotationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__AnnotationAssignment_1_1"


    // $ANTLR start "rule__Reference__GraphicPropertiesAssignment_2"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4662:1: rule__Reference__GraphicPropertiesAssignment_2 : ( ruleEdgeProperties ) ;
    public final void rule__Reference__GraphicPropertiesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4666:1: ( ( ruleEdgeProperties ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4667:1: ( ruleEdgeProperties )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4667:1: ( ruleEdgeProperties )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4668:1: ruleEdgeProperties
            {
             before(grammarAccess.getReferenceAccess().getGraphicPropertiesEdgePropertiesParserRuleCall_2_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEdgeProperties_in_rule__Reference__GraphicPropertiesAssignment_29480);
            ruleEdgeProperties();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getGraphicPropertiesEdgePropertiesParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__GraphicPropertiesAssignment_2"


    // $ANTLR start "rule__Reference__NameAssignment_4"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4677:1: rule__Reference__NameAssignment_4 : ( ruleEString ) ;
    public final void rule__Reference__NameAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4681:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4682:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4682:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4683:1: ruleEString
            {
             before(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_4_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Reference__NameAssignment_49511);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__NameAssignment_4"


    // $ANTLR start "rule__Reference__ReferenceAssignment_6"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4692:1: rule__Reference__ReferenceAssignment_6 : ( ( ruleEString ) ) ;
    public final void rule__Reference__ReferenceAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4696:1: ( ( ( ruleEString ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4697:1: ( ( ruleEString ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4697:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4698:1: ( ruleEString )
            {
             before(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_6_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4699:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4700:1: ruleEString
            {
             before(grammarAccess.getReferenceAccess().getReferenceObjectEStringParserRuleCall_6_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Reference__ReferenceAssignment_69546);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getReferenceObjectEStringParserRuleCall_6_0_1()); 

            }

             after(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__ReferenceAssignment_6"


    // $ANTLR start "rule__Reference__ReferenceAssignment_7_1"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4711:1: rule__Reference__ReferenceAssignment_7_1 : ( ( ruleEString ) ) ;
    public final void rule__Reference__ReferenceAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4715:1: ( ( ( ruleEString ) ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4716:1: ( ( ruleEString ) )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4716:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4717:1: ( ruleEString )
            {
             before(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_7_1_0()); 
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4718:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4719:1: ruleEString
            {
             before(grammarAccess.getReferenceAccess().getReferenceObjectEStringParserRuleCall_7_1_0_1()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__Reference__ReferenceAssignment_7_19585);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReferenceAccess().getReferenceObjectEStringParserRuleCall_7_1_0_1()); 

            }

             after(grammarAccess.getReferenceAccess().getReferenceObjectCrossReference_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reference__ReferenceAssignment_7_1"


    // $ANTLR start "rule__EdgeProperties__ColorAssignment_6"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4730:1: rule__EdgeProperties__ColorAssignment_6 : ( ruleEString ) ;
    public final void rule__EdgeProperties__ColorAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4734:1: ( ( ruleEString ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4735:1: ( ruleEString )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4735:1: ( ruleEString )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4736:1: ruleEString
            {
             before(grammarAccess.getEdgePropertiesAccess().getColorEStringParserRuleCall_6_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_rule__EdgeProperties__ColorAssignment_69620);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesAccess().getColorEStringParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__ColorAssignment_6"


    // $ANTLR start "rule__EdgeProperties__WidthAssignment_10"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4745:1: rule__EdgeProperties__WidthAssignment_10 : ( RULE_INT ) ;
    public final void rule__EdgeProperties__WidthAssignment_10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4749:1: ( ( RULE_INT ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4750:1: ( RULE_INT )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4750:1: ( RULE_INT )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4751:1: RULE_INT
            {
             before(grammarAccess.getEdgePropertiesAccess().getWidthINTTerminalRuleCall_10_0()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__EdgeProperties__WidthAssignment_109651); 
             after(grammarAccess.getEdgePropertiesAccess().getWidthINTTerminalRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__WidthAssignment_10"


    // $ANTLR start "rule__EdgeProperties__LineTypeAssignment_14"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4760:1: rule__EdgeProperties__LineTypeAssignment_14 : ( ruleLineType ) ;
    public final void rule__EdgeProperties__LineTypeAssignment_14() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4764:1: ( ( ruleLineType ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4765:1: ( ruleLineType )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4765:1: ( ruleLineType )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4766:1: ruleLineType
            {
             before(grammarAccess.getEdgePropertiesAccess().getLineTypeLineTypeEnumRuleCall_14_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleLineType_in_rule__EdgeProperties__LineTypeAssignment_149682);
            ruleLineType();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesAccess().getLineTypeLineTypeEnumRuleCall_14_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__LineTypeAssignment_14"


    // $ANTLR start "rule__EdgeProperties__SrcArrowAssignment_18"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4775:1: rule__EdgeProperties__SrcArrowAssignment_18 : ( ruleArrowType ) ;
    public final void rule__EdgeProperties__SrcArrowAssignment_18() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4779:1: ( ( ruleArrowType ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4780:1: ( ruleArrowType )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4780:1: ( ruleArrowType )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4781:1: ruleArrowType
            {
             before(grammarAccess.getEdgePropertiesAccess().getSrcArrowArrowTypeEnumRuleCall_18_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleArrowType_in_rule__EdgeProperties__SrcArrowAssignment_189713);
            ruleArrowType();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesAccess().getSrcArrowArrowTypeEnumRuleCall_18_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__SrcArrowAssignment_18"


    // $ANTLR start "rule__EdgeProperties__TgtArrowAssignment_22"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4790:1: rule__EdgeProperties__TgtArrowAssignment_22 : ( ruleArrowType ) ;
    public final void rule__EdgeProperties__TgtArrowAssignment_22() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4794:1: ( ( ruleArrowType ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4795:1: ( ruleArrowType )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4795:1: ( ruleArrowType )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4796:1: ruleArrowType
            {
             before(grammarAccess.getEdgePropertiesAccess().getTgtArrowArrowTypeEnumRuleCall_22_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleArrowType_in_rule__EdgeProperties__TgtArrowAssignment_229744);
            ruleArrowType();

            state._fsp--;

             after(grammarAccess.getEdgePropertiesAccess().getTgtArrowArrowTypeEnumRuleCall_22_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EdgeProperties__TgtArrowAssignment_22"


    // $ANTLR start "rule__IntegerValue__ValueAssignment"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4805:1: rule__IntegerValue__ValueAssignment : ( RULE_INT ) ;
    public final void rule__IntegerValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4809:1: ( ( RULE_INT ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4810:1: ( RULE_INT )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4810:1: ( RULE_INT )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4811:1: RULE_INT
            {
             before(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0()); 
            match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_rule__IntegerValue__ValueAssignment9775); 
             after(grammarAccess.getIntegerValueAccess().getValueINTTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IntegerValue__ValueAssignment"


    // $ANTLR start "rule__StringValue__ValueAssignment"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4820:1: rule__StringValue__ValueAssignment : ( RULE_STRING ) ;
    public final void rule__StringValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4824:1: ( ( RULE_STRING ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4825:1: ( RULE_STRING )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4825:1: ( RULE_STRING )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4826:1: RULE_STRING
            {
             before(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 
            match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_rule__StringValue__ValueAssignment9806); 
             after(grammarAccess.getStringValueAccess().getValueSTRINGTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringValue__ValueAssignment"


    // $ANTLR start "rule__BooleanValue__ValueAssignment"
    // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4835:1: rule__BooleanValue__ValueAssignment : ( ruleBooleanValueTerminal ) ;
    public final void rule__BooleanValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4839:1: ( ( ruleBooleanValueTerminal ) )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4840:1: ( ruleBooleanValueTerminal )
            {
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4840:1: ( ruleBooleanValueTerminal )
            // ../metabup.fragments.texteditor.ui/src-gen/metabup/ui/contentassist/antlr/internal/InternalFragments.g:4841:1: ruleBooleanValueTerminal
            {
             before(grammarAccess.getBooleanValueAccess().getValueBooleanValueTerminalParserRuleCall_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleBooleanValueTerminal_in_rule__BooleanValue__ValueAssignment9837);
            ruleBooleanValueTerminal();

            state._fsp--;

             after(grammarAccess.getBooleanValueAccess().getValueBooleanValueTerminalParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanValue__ValueAssignment"

    // Delegated rules


    protected DFA1 dfa1 = new DFA1(this);
    static final String DFA1_eotS =
        "\103\uffff";
    static final String DFA1_eofS =
        "\103\uffff";
    static final String DFA1_minS =
        "\1\13\1\4\2\uffff\2\13\2\4\2\66\2\13\3\4\6\64\2\66\1\4\1\13\3\4"+
        "\2\66\10\64\3\4\1\13\1\4\6\64\2\66\2\64\3\4\10\64\1\4\2\64";
    static final String DFA1_maxS =
        "\1\71\1\72\2\uffff\2\71\1\5\1\72\2\70\2\71\1\16\2\5\4\65\2\67\2"+
        "\70\1\5\1\71\1\5\1\16\1\5\2\70\6\65\2\67\1\16\2\5\1\71\1\5\4\65"+
        "\2\67\2\70\2\65\1\5\1\16\1\5\6\65\2\67\1\5\2\65";
    static final String DFA1_acceptS =
        "\2\uffff\1\1\1\2\77\uffff";
    static final String DFA1_specialS =
        "\103\uffff}>";
    static final String[] DFA1_transitionS = {
            "\2\3\45\uffff\1\1\6\uffff\1\2",
            "\1\4\1\5\64\uffff\1\3",
            "",
            "",
            "\2\3\45\uffff\1\7\1\6\5\uffff\1\2",
            "\2\3\45\uffff\1\7\1\6\5\uffff\1\2",
            "\1\10\1\11",
            "\1\12\1\13\64\uffff\1\3",
            "\1\15\1\uffff\1\14",
            "\1\15\1\uffff\1\14",
            "\2\3\45\uffff\1\7\1\16\5\uffff\1\2",
            "\2\3\45\uffff\1\7\1\16\5\uffff\1\2",
            "\1\17\1\uffff\1\20\6\uffff\1\21\1\22",
            "\1\23\1\24",
            "\1\25\1\26",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27\1\uffff\1\31",
            "\1\30\1\27\1\uffff\1\31",
            "\1\33\1\uffff\1\32",
            "\1\33\1\uffff\1\32",
            "\1\34\1\35",
            "\2\3\45\uffff\1\7\6\uffff\1\2",
            "\1\36\1\37",
            "\1\40\1\uffff\1\41\6\uffff\1\42\1\43",
            "\1\44\1\45",
            "\1\47\1\uffff\1\46",
            "\1\47\1\uffff\1\46",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50\1\uffff\1\52",
            "\1\51\1\50\1\uffff\1\52",
            "\1\53\1\uffff\1\54\6\uffff\1\55\1\56",
            "\1\57\1\60",
            "\1\61\1\62",
            "\2\3\45\uffff\1\7\6\uffff\1\2",
            "\1\63\1\64",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\30\1\27\1\uffff\1\65",
            "\1\30\1\27\1\uffff\1\65",
            "\1\67\1\uffff\1\66",
            "\1\67\1\uffff\1\66",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\70\1\71",
            "\1\72\1\uffff\1\73\6\uffff\1\74\1\75",
            "\1\76\1\77",
            "\1\30\1\27",
            "\1\30\1\27",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50",
            "\1\51\1\50\1\uffff\1\100",
            "\1\51\1\50\1\uffff\1\100",
            "\1\101\1\102",
            "\1\51\1\50",
            "\1\51\1\50"
    };

    static final short[] DFA1_eot = DFA.unpackEncodedString(DFA1_eotS);
    static final short[] DFA1_eof = DFA.unpackEncodedString(DFA1_eofS);
    static final char[] DFA1_min = DFA.unpackEncodedStringToUnsignedChars(DFA1_minS);
    static final char[] DFA1_max = DFA.unpackEncodedStringToUnsignedChars(DFA1_maxS);
    static final short[] DFA1_accept = DFA.unpackEncodedString(DFA1_acceptS);
    static final short[] DFA1_special = DFA.unpackEncodedString(DFA1_specialS);
    static final short[][] DFA1_transition;

    static {
        int numStates = DFA1_transitionS.length;
        DFA1_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA1_transition[i] = DFA.unpackEncodedString(DFA1_transitionS[i]);
        }
    }

    class DFA1 extends DFA {

        public DFA1(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = DFA1_eot;
            this.eof = DFA1_eof;
            this.min = DFA1_min;
            this.max = DFA1_max;
            this.accept = DFA1_accept;
            this.special = DFA1_special;
            this.transition = DFA1_transition;
        }
        public String getDescription() {
            return "649:1: rule__AttrOrReference__Alternatives : ( ( ruleAttribute ) | ( ruleReference ) );";
        }
    }
 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleFragmentModel_in_entryRuleFragmentModel61 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragmentModel68 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__0_in_ruleFragmentModel94 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragment_in_entryRuleFragment121 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFragment128 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__0_in_ruleFragment154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature181 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeature188 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttrOrReference_in_ruleFeature214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttrOrReference_in_entryRuleAttrOrReference240 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttrOrReference247 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AttrOrReference__Alternatives_in_ruleAttrOrReference273 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString300 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString307 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EString__Alternatives_in_ruleEString333 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObject_in_entryRuleObject360 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObject367 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__0_in_ruleObject393 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_entryRuleAnnotation420 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotation427 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group__0_in_ruleAnnotation453 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_entryRuleAnnotationParam480 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAnnotationParam487 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__Group__0_in_ruleAnnotationParam513 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParamValue_in_entryRuleParamValue540 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleParamValue547 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ParamValue__Alternatives_in_ruleParamValue573 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_entryRuleObjectValueAnnotationParam600 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleObjectValueAnnotationParam607 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__0_in_ruleObjectValueAnnotationParam633 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_entryRulePrimitiveValueAnnotationParam660 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValueAnnotationParam667 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__Group__0_in_rulePrimitiveValueAnnotationParam693 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_entryRuleAttribute720 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttribute727 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__0_in_ruleAttribute753 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_entryRuleReference780 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReference787 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__0_in_ruleReference813 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEdgeProperties_in_entryRuleEdgeProperties840 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEdgeProperties847 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__0_in_ruleEdgeProperties873 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_entryRulePrimitiveValue900 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRulePrimitiveValue907 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValue__Alternatives_in_rulePrimitiveValue933 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_entryRuleIntegerValue960 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleIntegerValue967 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__IntegerValue__ValueAssignment_in_ruleIntegerValue993 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_entryRuleStringValue1020 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleStringValue1027 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__StringValue__ValueAssignment_in_ruleStringValue1053 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_entryRuleBooleanValue1080 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValue1087 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BooleanValue__ValueAssignment_in_ruleBooleanValue1113 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_entryRuleBooleanValueTerminal1140 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleBooleanValueTerminal1147 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__BooleanValueTerminal__Alternatives_in_ruleBooleanValueTerminal1173 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentType__Alternatives_in_ruleFragmentType1210 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__LineType__Alternatives_in_ruleLineType1246 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ArrowType__Alternatives_in_ruleArrowType1282 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_rule__AttrOrReference__Alternatives1317 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_rule__AttrOrReference__Alternatives1334 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__EString__Alternatives1366 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_rule__EString__Alternatives1383 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValueAnnotationParam_in_rule__ParamValue__Alternatives1415 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObjectValueAnnotationParam_in_rule__ParamValue__Alternatives1432 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_rule__Reference__Alternatives_31465 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_rule__Reference__Alternatives_31485 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleStringValue_in_rule__PrimitiveValue__Alternatives1519 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleIntegerValue_in_rule__PrimitiveValue__Alternatives1536 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValue_in_rule__PrimitiveValue__Alternatives1553 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_13_in_rule__BooleanValueTerminal__Alternatives1586 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_14_in_rule__BooleanValueTerminal__Alternatives1606 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_15_in_rule__FragmentType__Alternatives1641 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_16_in_rule__FragmentType__Alternatives1662 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__LineType__Alternatives1698 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_rule__LineType__Alternatives1719 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_19_in_rule__LineType__Alternatives1740 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_20_in_rule__LineType__Alternatives1761 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_21_in_rule__ArrowType__Alternatives1797 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_22_in_rule__ArrowType__Alternatives1818 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_rule__ArrowType__Alternatives1839 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_rule__ArrowType__Alternatives1860 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_25_in_rule__ArrowType__Alternatives1881 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_26_in_rule__ArrowType__Alternatives1902 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_27_in_rule__ArrowType__Alternatives1923 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_28_in_rule__ArrowType__Alternatives1944 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_29_in_rule__ArrowType__Alternatives1965 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_30_in_rule__ArrowType__Alternatives1986 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_31_in_rule__ArrowType__Alternatives2007 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_32_in_rule__ArrowType__Alternatives2028 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_33_in_rule__ArrowType__Alternatives2049 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_34_in_rule__ArrowType__Alternatives2070 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_35_in_rule__ArrowType__Alternatives2091 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_36_in_rule__ArrowType__Alternatives2112 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_37_in_rule__ArrowType__Alternatives2133 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_38_in_rule__ArrowType__Alternatives2154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_39_in_rule__ArrowType__Alternatives2175 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_40_in_rule__ArrowType__Alternatives2196 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_41_in_rule__ArrowType__Alternatives2217 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__0__Impl_in_rule__FragmentModel__Group__02250 = new BitSet(new long[]{0x0000040000000000L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__1_in_rule__FragmentModel__Group__02253 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__1__Impl_in_rule__FragmentModel__Group__12311 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__2_in_rule__FragmentModel__Group__12314 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_42_in_rule__FragmentModel__Group__1__Impl2342 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__2__Impl_in_rule__FragmentModel__Group__22373 = new BitSet(new long[]{0x0004080000018000L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__3_in_rule__FragmentModel__Group__22376 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentModel__NameAssignment_2_in_rule__FragmentModel__Group__2__Impl2403 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__3__Impl_in_rule__FragmentModel__Group__32433 = new BitSet(new long[]{0x0004080000018000L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__4_in_rule__FragmentModel__Group__32436 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentModel__FragmentsAssignment_3_in_rule__FragmentModel__Group__3__Impl2463 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentModel__Group__4__Impl_in_rule__FragmentModel__Group__42493 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__FragmentModel__FragmentsAssignment_4_in_rule__FragmentModel__Group__4__Impl2520 = new BitSet(new long[]{0x0004080000018002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__0__Impl_in_rule__Fragment__Group__02561 = new BitSet(new long[]{0x0004080000018000L});
        public static final BitSet FOLLOW_rule__Fragment__Group__1_in_rule__Fragment__Group__02564 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__1__Impl_in_rule__Fragment__Group__12622 = new BitSet(new long[]{0x0004080000018000L});
        public static final BitSet FOLLOW_rule__Fragment__Group__2_in_rule__Fragment__Group__12625 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_1__0_in_rule__Fragment__Group__1__Impl2652 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__2__Impl_in_rule__Fragment__Group__22683 = new BitSet(new long[]{0x0004080000018000L});
        public static final BitSet FOLLOW_rule__Fragment__Group__3_in_rule__Fragment__Group__22686 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__FtypeAssignment_2_in_rule__Fragment__Group__2__Impl2713 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__3__Impl_in_rule__Fragment__Group__32744 = new BitSet(new long[]{0x0000400000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__4_in_rule__Fragment__Group__32747 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_43_in_rule__Fragment__Group__3__Impl2775 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__4__Impl_in_rule__Fragment__Group__42806 = new BitSet(new long[]{0x0000400000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__5_in_rule__Fragment__Group__42809 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__0_in_rule__Fragment__Group__4__Impl2836 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__5__Impl_in_rule__Fragment__Group__52867 = new BitSet(new long[]{0x0000100000000000L});
        public static final BitSet FOLLOW_rule__Fragment__Group__6_in_rule__Fragment__Group__52870 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__NameAssignment_5_in_rule__Fragment__Group__5__Impl2897 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__6__Impl_in_rule__Fragment__Group__62927 = new BitSet(new long[]{0x0004000000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__7_in_rule__Fragment__Group__62930 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_44_in_rule__Fragment__Group__6__Impl2958 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__7__Impl_in_rule__Fragment__Group__72989 = new BitSet(new long[]{0x0004200000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__8_in_rule__Fragment__Group__72992 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__ObjectsAssignment_7_in_rule__Fragment__Group__7__Impl3019 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group__8__Impl_in_rule__Fragment__Group__83049 = new BitSet(new long[]{0x0004200000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group__9_in_rule__Fragment__Group__83052 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__ObjectsAssignment_8_in_rule__Fragment__Group__8__Impl3079 = new BitSet(new long[]{0x0004000000000032L});
        public static final BitSet FOLLOW_rule__Fragment__Group__9__Impl_in_rule__Fragment__Group__93110 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_45_in_rule__Fragment__Group__9__Impl3138 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_1__0__Impl_in_rule__Fragment__Group_1__03189 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__Fragment__Group_1__1_in_rule__Fragment__Group_1__03192 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__AnnotationAssignment_1_0_in_rule__Fragment__Group_1__0__Impl3219 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_1__1__Impl_in_rule__Fragment__Group_1__13249 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__AnnotationAssignment_1_1_in_rule__Fragment__Group_1__1__Impl3276 = new BitSet(new long[]{0x0004000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__0__Impl_in_rule__Fragment__Group_4__03311 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__1_in_rule__Fragment__Group_4__03314 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_46_in_rule__Fragment__Group_4__0__Impl3342 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__1__Impl_in_rule__Fragment__Group_4__13373 = new BitSet(new long[]{0x0000800000000000L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__2_in_rule__Fragment__Group_4__13376 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__TypeAssignment_4_1_in_rule__Fragment__Group_4__1__Impl3403 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Fragment__Group_4__2__Impl_in_rule__Fragment__Group_4__23433 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_47_in_rule__Fragment__Group_4__2__Impl3461 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__0__Impl_in_rule__Object__Group__03498 = new BitSet(new long[]{0x0004000000000030L});
        public static final BitSet FOLLOW_rule__Object__Group__1_in_rule__Object__Group__03501 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__1__Impl_in_rule__Object__Group__13559 = new BitSet(new long[]{0x0004000000000030L});
        public static final BitSet FOLLOW_rule__Object__Group__2_in_rule__Object__Group__13562 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_1__0_in_rule__Object__Group__1__Impl3589 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__2__Impl_in_rule__Object__Group__23620 = new BitSet(new long[]{0x0001000000000000L});
        public static final BitSet FOLLOW_rule__Object__Group__3_in_rule__Object__Group__23623 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__NameAssignment_2_in_rule__Object__Group__2__Impl3650 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__3__Impl_in_rule__Object__Group__33680 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Object__Group__4_in_rule__Object__Group__33683 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_48_in_rule__Object__Group__3__Impl3711 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__4__Impl_in_rule__Object__Group__43742 = new BitSet(new long[]{0x0000100000000000L});
        public static final BitSet FOLLOW_rule__Object__Group__5_in_rule__Object__Group__43745 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__TypeAssignment_4_in_rule__Object__Group__4__Impl3772 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__5__Impl_in_rule__Object__Group__53802 = new BitSet(new long[]{0x0204200000001800L});
        public static final BitSet FOLLOW_rule__Object__Group__6_in_rule__Object__Group__53805 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_44_in_rule__Object__Group__5__Impl3833 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__6__Impl_in_rule__Object__Group__63864 = new BitSet(new long[]{0x0204200000001800L});
        public static final BitSet FOLLOW_rule__Object__Group__7_in_rule__Object__Group__63867 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6__0_in_rule__Object__Group__6__Impl3894 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group__7__Impl_in_rule__Object__Group__73925 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_45_in_rule__Object__Group__7__Impl3953 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_1__0__Impl_in_rule__Object__Group_1__04000 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__Object__Group_1__1_in_rule__Object__Group_1__04003 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__AnnotationAssignment_1_0_in_rule__Object__Group_1__0__Impl4030 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_1__1__Impl_in_rule__Object__Group_1__14060 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__AnnotationAssignment_1_1_in_rule__Object__Group_1__1__Impl4087 = new BitSet(new long[]{0x0004000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6__0__Impl_in_rule__Object__Group_6__04122 = new BitSet(new long[]{0x0206000000001800L});
        public static final BitSet FOLLOW_rule__Object__Group_6__1_in_rule__Object__Group_6__04125 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__FeaturesAssignment_6_0_in_rule__Object__Group_6__0__Impl4152 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6__1__Impl_in_rule__Object__Group_6__14182 = new BitSet(new long[]{0x0206000000001800L});
        public static final BitSet FOLLOW_rule__Object__Group_6__2_in_rule__Object__Group_6__14185 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_49_in_rule__Object__Group_6__1__Impl4214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6__2__Impl_in_rule__Object__Group_6__24247 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6_2__0_in_rule__Object__Group_6__2__Impl4274 = new BitSet(new long[]{0x0204000000001802L});
        public static final BitSet FOLLOW_rule__Object__Group_6_2__0__Impl_in_rule__Object__Group_6_2__04311 = new BitSet(new long[]{0x0002000000000000L});
        public static final BitSet FOLLOW_rule__Object__Group_6_2__1_in_rule__Object__Group_6_2__04314 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__FeaturesAssignment_6_2_0_in_rule__Object__Group_6_2__0__Impl4341 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Object__Group_6_2__1__Impl_in_rule__Object__Group_6_2__14371 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_49_in_rule__Object__Group_6_2__1__Impl4400 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group__0__Impl_in_rule__Annotation__Group__04437 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Annotation__Group__1_in_rule__Annotation__Group__04440 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_50_in_rule__Annotation__Group__0__Impl4468 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group__1__Impl_in_rule__Annotation__Group__14499 = new BitSet(new long[]{0x0008000000000000L});
        public static final BitSet FOLLOW_rule__Annotation__Group__2_in_rule__Annotation__Group__14502 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__NameAssignment_1_in_rule__Annotation__Group__1__Impl4529 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group__2__Impl_in_rule__Annotation__Group__24559 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__0_in_rule__Annotation__Group__2__Impl4586 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__0__Impl_in_rule__Annotation__Group_2__04623 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__1_in_rule__Annotation__Group_2__04626 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_51_in_rule__Annotation__Group_2__0__Impl4654 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__1__Impl_in_rule__Annotation__Group_2__14685 = new BitSet(new long[]{0x0030000000000000L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__2_in_rule__Annotation__Group_2__14688 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__ParamsAssignment_2_1_in_rule__Annotation__Group_2__1__Impl4715 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__2__Impl_in_rule__Annotation__Group_2__24745 = new BitSet(new long[]{0x0030000000000000L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__3_in_rule__Annotation__Group_2__24748 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2_2__0_in_rule__Annotation__Group_2__2__Impl4775 = new BitSet(new long[]{0x0020000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2__3__Impl_in_rule__Annotation__Group_2__34806 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_52_in_rule__Annotation__Group_2__3__Impl4834 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2_2__0__Impl_in_rule__Annotation__Group_2_2__04873 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2_2__1_in_rule__Annotation__Group_2_2__04876 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__Annotation__Group_2_2__0__Impl4904 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__Group_2_2__1__Impl_in_rule__Annotation__Group_2_2__14935 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Annotation__ParamsAssignment_2_2_1_in_rule__Annotation__Group_2_2__1__Impl4962 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__Group__0__Impl_in_rule__AnnotationParam__Group__04996 = new BitSet(new long[]{0x0140000000000000L});
        public static final BitSet FOLLOW_rule__AnnotationParam__Group__1_in_rule__AnnotationParam__Group__04999 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__NameAssignment_0_in_rule__AnnotationParam__Group__0__Impl5026 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__Group__1__Impl_in_rule__AnnotationParam__Group__15056 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__AnnotationParam__ParamValueAssignment_1_in_rule__AnnotationParam__Group__1__Impl5083 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__0__Impl_in_rule__ObjectValueAnnotationParam__Group__05117 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__1_in_rule__ObjectValueAnnotationParam__Group__05120 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_54_in_rule__ObjectValueAnnotationParam__Group__0__Impl5148 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__1__Impl_in_rule__ObjectValueAnnotationParam__Group__15179 = new BitSet(new long[]{0x0080000000000000L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__2_in_rule__ObjectValueAnnotationParam__Group__15182 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__ObjectAssignment_1_in_rule__ObjectValueAnnotationParam__Group__1__Impl5209 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group__2__Impl_in_rule__ObjectValueAnnotationParam__Group__25239 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group_2__0_in_rule__ObjectValueAnnotationParam__Group__2__Impl5266 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group_2__0__Impl_in_rule__ObjectValueAnnotationParam__Group_2__05303 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group_2__1_in_rule__ObjectValueAnnotationParam__Group_2__05306 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_55_in_rule__ObjectValueAnnotationParam__Group_2__0__Impl5334 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__Group_2__1__Impl_in_rule__ObjectValueAnnotationParam__Group_2__15365 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__ObjectValueAnnotationParam__FeatureAssignment_2_1_in_rule__ObjectValueAnnotationParam__Group_2__1__Impl5392 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__Group__0__Impl_in_rule__PrimitiveValueAnnotationParam__Group__05426 = new BitSet(new long[]{0x0000000000006050L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__Group__1_in_rule__PrimitiveValueAnnotationParam__Group__05429 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__PrimitiveValueAnnotationParam__Group__0__Impl5457 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__Group__1__Impl_in_rule__PrimitiveValueAnnotationParam__Group__15488 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__PrimitiveValueAnnotationParam__ValueAssignment_1_in_rule__PrimitiveValueAnnotationParam__Group__1__Impl5515 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__0__Impl_in_rule__Attribute__Group__05549 = new BitSet(new long[]{0x0204000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__1_in_rule__Attribute__Group__05552 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__1__Impl_in_rule__Attribute__Group__15610 = new BitSet(new long[]{0x0204000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__2_in_rule__Attribute__Group__15613 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_1__0_in_rule__Attribute__Group__1__Impl5640 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__2__Impl_in_rule__Attribute__Group__25671 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Attribute__Group__3_in_rule__Attribute__Group__25674 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_57_in_rule__Attribute__Group__2__Impl5702 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__3__Impl_in_rule__Attribute__Group__35733 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__4_in_rule__Attribute__Group__35736 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__NameAssignment_3_in_rule__Attribute__Group__3__Impl5763 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__4__Impl_in_rule__Attribute__Group__45793 = new BitSet(new long[]{0x0000000000006050L});
        public static final BitSet FOLLOW_rule__Attribute__Group__5_in_rule__Attribute__Group__45796 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__Attribute__Group__4__Impl5824 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__5__Impl_in_rule__Attribute__Group__55855 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group__6_in_rule__Attribute__Group__55858 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__ValuesAssignment_5_in_rule__Attribute__Group__5__Impl5885 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group__6__Impl_in_rule__Attribute__Group__65915 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_6__0_in_rule__Attribute__Group__6__Impl5942 = new BitSet(new long[]{0x0020000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_1__0__Impl_in_rule__Attribute__Group_1__05987 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__Attribute__Group_1__1_in_rule__Attribute__Group_1__05990 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__AnnotationAssignment_1_0_in_rule__Attribute__Group_1__0__Impl6017 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_1__1__Impl_in_rule__Attribute__Group_1__16047 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__AnnotationAssignment_1_1_in_rule__Attribute__Group_1__1__Impl6074 = new BitSet(new long[]{0x0004000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_6__0__Impl_in_rule__Attribute__Group_6__06109 = new BitSet(new long[]{0x0000000000006050L});
        public static final BitSet FOLLOW_rule__Attribute__Group_6__1_in_rule__Attribute__Group_6__06112 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__Attribute__Group_6__0__Impl6140 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__Group_6__1__Impl_in_rule__Attribute__Group_6__16171 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Attribute__ValuesAssignment_6_1_in_rule__Attribute__Group_6__1__Impl6198 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__0__Impl_in_rule__Reference__Group__06232 = new BitSet(new long[]{0x0204000000001800L});
        public static final BitSet FOLLOW_rule__Reference__Group__1_in_rule__Reference__Group__06235 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__1__Impl_in_rule__Reference__Group__16293 = new BitSet(new long[]{0x0204000000001800L});
        public static final BitSet FOLLOW_rule__Reference__Group__2_in_rule__Reference__Group__16296 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_1__0_in_rule__Reference__Group__1__Impl6323 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__2__Impl_in_rule__Reference__Group__26354 = new BitSet(new long[]{0x0204000000001800L});
        public static final BitSet FOLLOW_rule__Reference__Group__3_in_rule__Reference__Group__26357 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__GraphicPropertiesAssignment_2_in_rule__Reference__Group__2__Impl6384 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__3__Impl_in_rule__Reference__Group__36415 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Reference__Group__4_in_rule__Reference__Group__36418 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Alternatives_3_in_rule__Reference__Group__3__Impl6445 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__4__Impl_in_rule__Reference__Group__46475 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__Reference__Group__5_in_rule__Reference__Group__46478 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__NameAssignment_4_in_rule__Reference__Group__4__Impl6505 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__5__Impl_in_rule__Reference__Group__56535 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Reference__Group__6_in_rule__Reference__Group__56538 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__Reference__Group__5__Impl6566 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__6__Impl_in_rule__Reference__Group__66597 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__Reference__Group__7_in_rule__Reference__Group__66600 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__ReferenceAssignment_6_in_rule__Reference__Group__6__Impl6627 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group__7__Impl_in_rule__Reference__Group__76657 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_7__0_in_rule__Reference__Group__7__Impl6684 = new BitSet(new long[]{0x0020000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_1__0__Impl_in_rule__Reference__Group_1__06731 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__Reference__Group_1__1_in_rule__Reference__Group_1__06734 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__AnnotationAssignment_1_0_in_rule__Reference__Group_1__0__Impl6761 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_1__1__Impl_in_rule__Reference__Group_1__16791 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__AnnotationAssignment_1_1_in_rule__Reference__Group_1__1__Impl6818 = new BitSet(new long[]{0x0004000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_7__0__Impl_in_rule__Reference__Group_7__06853 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__Reference__Group_7__1_in_rule__Reference__Group_7__06856 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__Reference__Group_7__0__Impl6884 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__Group_7__1__Impl_in_rule__Reference__Group_7__16915 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reference__ReferenceAssignment_7_1_in_rule__Reference__Group_7__1__Impl6942 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__0__Impl_in_rule__EdgeProperties__Group__06976 = new BitSet(new long[]{0x0004000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__1_in_rule__EdgeProperties__Group__06979 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__1__Impl_in_rule__EdgeProperties__Group__17037 = new BitSet(new long[]{0x0400000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__2_in_rule__EdgeProperties__Group__17040 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_50_in_rule__EdgeProperties__Group__1__Impl7068 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__2__Impl_in_rule__EdgeProperties__Group__27099 = new BitSet(new long[]{0x0008000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__3_in_rule__EdgeProperties__Group__27102 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_58_in_rule__EdgeProperties__Group__2__Impl7130 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__3__Impl_in_rule__EdgeProperties__Group__37161 = new BitSet(new long[]{0x0800000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__4_in_rule__EdgeProperties__Group__37164 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_51_in_rule__EdgeProperties__Group__3__Impl7192 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__4__Impl_in_rule__EdgeProperties__Group__47223 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__5_in_rule__EdgeProperties__Group__47226 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_59_in_rule__EdgeProperties__Group__4__Impl7254 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__5__Impl_in_rule__EdgeProperties__Group__57285 = new BitSet(new long[]{0x0000000000000030L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__6_in_rule__EdgeProperties__Group__57288 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__5__Impl7316 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__6__Impl_in_rule__EdgeProperties__Group__67347 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__7_in_rule__EdgeProperties__Group__67350 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__ColorAssignment_6_in_rule__EdgeProperties__Group__6__Impl7377 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__7__Impl_in_rule__EdgeProperties__Group__77407 = new BitSet(new long[]{0x1000000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__8_in_rule__EdgeProperties__Group__77410 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__EdgeProperties__Group__7__Impl7438 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__8__Impl_in_rule__EdgeProperties__Group__87469 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__9_in_rule__EdgeProperties__Group__87472 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_60_in_rule__EdgeProperties__Group__8__Impl7500 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__9__Impl_in_rule__EdgeProperties__Group__97531 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__10_in_rule__EdgeProperties__Group__97534 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__9__Impl7562 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__10__Impl_in_rule__EdgeProperties__Group__107593 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__11_in_rule__EdgeProperties__Group__107596 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__WidthAssignment_10_in_rule__EdgeProperties__Group__10__Impl7623 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__11__Impl_in_rule__EdgeProperties__Group__117653 = new BitSet(new long[]{0x0000000000020000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__12_in_rule__EdgeProperties__Group__117656 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__EdgeProperties__Group__11__Impl7684 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__12__Impl_in_rule__EdgeProperties__Group__127715 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__13_in_rule__EdgeProperties__Group__127718 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_17_in_rule__EdgeProperties__Group__12__Impl7746 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__13__Impl_in_rule__EdgeProperties__Group__137777 = new BitSet(new long[]{0x00000000001E0000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__14_in_rule__EdgeProperties__Group__137780 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__13__Impl7808 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__14__Impl_in_rule__EdgeProperties__Group__147839 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__15_in_rule__EdgeProperties__Group__147842 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__LineTypeAssignment_14_in_rule__EdgeProperties__Group__14__Impl7869 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__15__Impl_in_rule__EdgeProperties__Group__157899 = new BitSet(new long[]{0x2000000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__16_in_rule__EdgeProperties__Group__157902 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__EdgeProperties__Group__15__Impl7930 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__16__Impl_in_rule__EdgeProperties__Group__167961 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__17_in_rule__EdgeProperties__Group__167964 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_61_in_rule__EdgeProperties__Group__16__Impl7992 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__17__Impl_in_rule__EdgeProperties__Group__178023 = new BitSet(new long[]{0x000003FFFFE00000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__18_in_rule__EdgeProperties__Group__178026 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__17__Impl8054 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__18__Impl_in_rule__EdgeProperties__Group__188085 = new BitSet(new long[]{0x0020000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__19_in_rule__EdgeProperties__Group__188088 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__SrcArrowAssignment_18_in_rule__EdgeProperties__Group__18__Impl8115 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__19__Impl_in_rule__EdgeProperties__Group__198145 = new BitSet(new long[]{0x4000000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__20_in_rule__EdgeProperties__Group__198148 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_53_in_rule__EdgeProperties__Group__19__Impl8176 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__20__Impl_in_rule__EdgeProperties__Group__208207 = new BitSet(new long[]{0x0100000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__21_in_rule__EdgeProperties__Group__208210 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_62_in_rule__EdgeProperties__Group__20__Impl8238 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__21__Impl_in_rule__EdgeProperties__Group__218269 = new BitSet(new long[]{0x000003FFFFE00000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__22_in_rule__EdgeProperties__Group__218272 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_56_in_rule__EdgeProperties__Group__21__Impl8300 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__22__Impl_in_rule__EdgeProperties__Group__228331 = new BitSet(new long[]{0x0010000000000000L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__23_in_rule__EdgeProperties__Group__228334 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__TgtArrowAssignment_22_in_rule__EdgeProperties__Group__22__Impl8361 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__EdgeProperties__Group__23__Impl_in_rule__EdgeProperties__Group__238391 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_52_in_rule__EdgeProperties__Group__23__Impl8419 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__FragmentModel__NameAssignment_28503 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragment_in_rule__FragmentModel__FragmentsAssignment_38534 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragment_in_rule__FragmentModel__FragmentsAssignment_48565 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Fragment__AnnotationAssignment_1_08596 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Fragment__AnnotationAssignment_1_18627 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFragmentType_in_rule__Fragment__FtypeAssignment_28658 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Fragment__TypeAssignment_4_18689 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Fragment__NameAssignment_58720 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObject_in_rule__Fragment__ObjectsAssignment_78751 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleObject_in_rule__Fragment__ObjectsAssignment_88782 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Object__AnnotationAssignment_1_08813 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Object__AnnotationAssignment_1_18844 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Object__NameAssignment_28875 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Object__TypeAssignment_48906 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_rule__Object__FeaturesAssignment_6_08937 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleFeature_in_rule__Object__FeaturesAssignment_6_2_08968 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Annotation__NameAssignment_18999 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_rule__Annotation__ParamsAssignment_2_19030 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotationParam_in_rule__Annotation__ParamsAssignment_2_2_19061 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__AnnotationParam__NameAssignment_09092 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleParamValue_in_rule__AnnotationParam__ParamValueAssignment_19123 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__ObjectValueAnnotationParam__ObjectAssignment_19158 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__ObjectValueAnnotationParam__FeatureAssignment_2_19197 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_rule__PrimitiveValueAnnotationParam__ValueAssignment_19232 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Attribute__AnnotationAssignment_1_09263 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Attribute__AnnotationAssignment_1_19294 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Attribute__NameAssignment_39325 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_rule__Attribute__ValuesAssignment_59356 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rulePrimitiveValue_in_rule__Attribute__ValuesAssignment_6_19387 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Reference__AnnotationAssignment_1_09418 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAnnotation_in_rule__Reference__AnnotationAssignment_1_19449 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEdgeProperties_in_rule__Reference__GraphicPropertiesAssignment_29480 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Reference__NameAssignment_49511 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Reference__ReferenceAssignment_69546 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__Reference__ReferenceAssignment_7_19585 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_rule__EdgeProperties__ColorAssignment_69620 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__EdgeProperties__WidthAssignment_109651 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleLineType_in_rule__EdgeProperties__LineTypeAssignment_149682 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleArrowType_in_rule__EdgeProperties__SrcArrowAssignment_189713 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleArrowType_in_rule__EdgeProperties__TgtArrowAssignment_229744 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_INT_in_rule__IntegerValue__ValueAssignment9775 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_rule__StringValue__ValueAssignment9806 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleBooleanValueTerminal_in_rule__BooleanValue__ValueAssignment9837 = new BitSet(new long[]{0x0000000000000002L});
    }


}