/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assertion Test</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.AssertionTest#getMetamodelURI <em>Metamodel URI</em>}</li>
 *   <li>{@link metamodeltest.AssertionTest#getAssertions <em>Assertions</em>}</li>
 *   <li>{@link metamodeltest.AssertionTest#getDirURI <em>Dir URI</em>}</li>
 *   <li>{@link metamodeltest.AssertionTest#getCorrespondingEcoreURI <em>Corresponding Ecore URI</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getAssertionTest()
 * @model
 * @generated
 */
public interface AssertionTest extends AnnotatedElement, DescribedElement {
	/**
	 * Returns the value of the '<em><b>Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metamodel URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metamodel URI</em>' attribute.
	 * @see #setMetamodelURI(String)
	 * @see metamodeltest.MetamodeltestPackage#getAssertionTest_MetamodelURI()
	 * @model
	 * @generated
	 */
	String getMetamodelURI();

	/**
	 * Sets the value of the '{@link metamodeltest.AssertionTest#getMetamodelURI <em>Metamodel URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metamodel URI</em>' attribute.
	 * @see #getMetamodelURI()
	 * @generated
	 */
	void setMetamodelURI(String value);

	/**
	 * Returns the value of the '<em><b>Assertions</b></em>' containment reference list.
	 * The list contents are of type {@link metamodeltest.AssertionCall}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertions</em>' containment reference list.
	 * @see metamodeltest.MetamodeltestPackage#getAssertionTest_Assertions()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<AssertionCall> getAssertions();
	/**
	 * Returns the value of the '<em><b>Dir URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dir URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dir URI</em>' attribute.
	 * @see #setDirURI(String)
	 * @see metamodeltest.MetamodeltestPackage#getAssertionTest_DirURI()
	 * @model
	 * @generated
	 */
	String getDirURI();

	/**
	 * Sets the value of the '{@link metamodeltest.AssertionTest#getDirURI <em>Dir URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dir URI</em>' attribute.
	 * @see #getDirURI()
	 * @generated
	 */
	void setDirURI(String value);

	/**
	 * Returns the value of the '<em><b>Corresponding Ecore URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Corresponding Ecore URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Corresponding Ecore URI</em>' attribute.
	 * @see #setCorrespondingEcoreURI(String)
	 * @see metamodeltest.MetamodeltestPackage#getAssertionTest_CorrespondingEcoreURI()
	 * @model
	 * @generated
	 */
	String getCorrespondingEcoreURI();

	/**
	 * Sets the value of the '{@link metamodeltest.AssertionTest#getCorrespondingEcoreURI <em>Corresponding Ecore URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Corresponding Ecore URI</em>' attribute.
	 * @see #getCorrespondingEcoreURI()
	 * @generated
	 */
	void setCorrespondingEcoreURI(String value);

	public String toString();
} // AssertionTest
