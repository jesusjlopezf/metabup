/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments;

import java.util.List;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fragments.Object#getFeatures <em>Features</em>}</li>
 * </ul>
 *
 * @see fragments.FragmentsPackage#getObject()
 * @model
 * @generated
 */
public interface Object extends TypedElement {
	/**
	 * Returns the value of the '<em><b>Features</b></em>' containment reference list.
	 * The list contents are of type {@link fragments.Feature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Features</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Features</em>' containment reference list.
	 * @see fragments.FragmentsPackage#getObject_Features()
	 * @model containment="true"
	 * @generated
	 */
	EList<Feature> getFeatures();
	Reference getReferenceByName(String name);
	Attribute getAttributeByName(String name);
	List<Reference> getReferencesByAnnotation(String annotationName);
	List<Reference> getReferencesByAnnotation(String annotationName[]);
} // Object
