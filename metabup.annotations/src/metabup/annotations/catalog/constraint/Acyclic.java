/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.constraint;

import java.util.ArrayList;
import java.util.List;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.Object;
import metabup.annotations.extensionpoint.ConstraintAnnotation;
import metabup.issues.general.IOpenIssue;
import metabup.issues.lib.AcyclicReferenceCycle;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;

public class Acyclic extends ConstraintAnnotation {
	public Acyclic (){
		this.setName("acyclic");
		this.setDescription("The reference cannot incur cycles");
	}
	
	@Override
	public List<IOpenIssue> detectFragmentIssues(FragmentModel fm, AnnotatedElement ae){
		if(fm == null || ae == null || fm.getFragments() == null || fm.getFragments().isEmpty()) return null;
		List<IOpenIssue> issues = new ArrayList<IOpenIssue>();
		
		if(ae instanceof Reference){			
			Reference mmr = (Reference) ae;
			if(mmr.getReference() == null) return null;
			String name = mmr.getName();		
			
			List<fragments.Reference> references = new ArrayList<fragments.Reference>();
			boolean cyclic = false;
			
			for(Fragment f : fm.getFragments()){
				for(Object o : f.getObjects()){
					for(fragments.Reference fr : getReferences(o)){
						if(name.equals(fr.getName())){
							cyclic = isCyclic(fr, o);
							if(cyclic) references.add(fr);
						}
					}
				}
			}
			
			if(!references.isEmpty()){
				for(fragments.Reference r : references){
					AcyclicReferenceCycle arc = new AcyclicReferenceCycle(mmr, r, AcyclicReferenceCycle.DEPTH_INFINITE);
					issues.add(arc);
					return issues;
				}				
			}
		}
		
		return null;
	}
	
	private boolean isCyclic(fragments.Reference r, fragments.Object end) {
		if(r == null || end == null) return false;
		
		for(Object o : r.getReference()){			
			for(fragments.Reference or : getReferences(o)){
				if(or.getName().equals(r.getName())){
					
					for(fragments.Object oo : or.getReference()){
						if(oo.getType().equals(end.getType())) return true;
						else return isCyclic(or, end);
					}										
				}
			}
		}
		
		return false;
	}

	private List<fragments.Reference> getReferences(Object o){
		List<fragments.Reference> references = new ArrayList<fragments.Reference>();
		
		for(fragments.Feature f : o.getFeatures())
			if(f instanceof fragments.Reference)
				references.add((fragments.Reference)f);
		
		return references;
	}

	@Override
	public void updateConstraints() {
		if(element instanceof Reference){
			String REFSRC  = ((MetaClass)element.eContainer()).getName();
			String ELEMENT = ((Reference)element).getName();
			
			super.addConstraint(
				"acyclic", 
					"context " + REFSRC + " inv " + REFSRC + "_" + ELEMENT + "_acyclic : "
					+ "\n\tSet{self." + ELEMENT + "} -> iterate(i : Integer; reaches : Set(" + REFSRC + ") = Set{} |"
					+ "\n\t\treaches -> collect(d | reaches -> excludes(d) and d." + ELEMENT + " -> notEmpty()) -> asSet())->excludes(self)"
				);
		}
	}
}
