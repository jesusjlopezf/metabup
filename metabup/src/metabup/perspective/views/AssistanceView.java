/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.perspective.views;

import java.util.ArrayList;
import java.util.List;

import metabup.Activator;
import metabup.SessionState;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.ISolution;
import metabup.perspective.views.actions.UpdateMetamodelByAssistanceTip;
import metabup.ui.editor.editors.MetamodelInteractiveEditor;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;


/**
 * Shows a list of suggested refactorings over the active metamodel
 * 
 * @author jesusl
 */
// See TODO: http://wiki.eclipse.org/FAQ_How_to_decorate_a_TableViewer_or_TreeViewer_with_Columns%3F
public class AssistanceView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "metabup.perspective.views.AssistanceView";
	private TableViewer viewer;
	private Action doubleClickAction;
	private List<String> tips = new ArrayList<String>();
	// The connection with the editor
	protected SessionState session;
	
	/*
	 * The content provider class is responsible for
	 * providing objects to the view. It can wrap
	 * existing objects in adapters or simply return
	 * objects as-is. These objects may be sensitive
	 * to the current input of the view, or ignore
	 * it and always show the same content 
	 * (like Task List, for example).
	 */			
	class ViewContentProvider implements IStructuredContentProvider {
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {			
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			if ( session == null ) return new Object[] {};
			
			tips.clear();
			
			for(IAssistanceTip at : session.getTips()){
				List<ISolution> solutions = at.getSolutions(); 				
				for(ISolution s : solutions) tips.add(s.getDescription());			
			}
			
			if(!tips.isEmpty()) setPartName("Assistance (" + tips.size() + ")");			
			else setPartName("Assistance");
			
			return tips.toArray();
		}
	}
	
	class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			return getText(obj);
		}
		public Image getColumnImage(Object obj, int index) {
			return getImage(obj);
		}
		public Image getImage(Object obj) {
			ImageDescriptor desc = Activator.getImageDescriptor("icons/assistance_tip.gif");				
			if ( desc != null ) return desc.createImage();
			return null;
		}		
	}
	
	class NameSorter extends ViewerSorter { }	

	public AssistanceView() { }

	/**
	 * This is a callback that will allow us
	 * to create the viewer and initialize it.
	 */
	public void createPartControl(Composite parent) {
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setContentProvider(new ViewContentProvider());
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());

		// Create the help context id for the viewer's control
		PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(), "metabup.perspective.viewer");
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		contributeToActionBars();
		
		getSite().getWorkbenchWindow().getPartService().addPartListener(new AssistanceListener());
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				AssistanceView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		//manager.add(action1);
		//manager.add(new Separator());
		//manager.add(action2);
	}

	private void fillContextMenu(IMenuManager manager) {
		//manager.add(action1);
		//manager.add(action2);
		// Other plug-ins can contribute there actions here
		//manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}
	
	private void fillLocalToolBar(IToolBarManager manager) {
		//manager.add(action1);
		//manager.add(action2);
	}

	private void makeActions() {
		/*action1 = new Action() {
			public void run() {
				
			}
		};
		action1.setText("Apply annotation");
		action1.setToolTipText("Apply the suggested annotation over the indicated elements and refactor metamodel");
		action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));*/
		
		/*action2 = new Action() {
			public void run() {
			}
		};
		action2.setText("Action 2");
		action2.setToolTipText("Action 2 tooltip");
		action2.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
				getImageDescriptor(ISharedImages.IMG_OBJS_INFO_TSK));*/
		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection)selection).getFirstElement();
				//showMessage((String)obj);
				UpdateMetamodelByAssistanceTip action = new UpdateMetamodelByAssistanceTip();
				action.setEditor((MetamodelInteractiveEditor)PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor());
				//session.updateMetamodelByAssistanceTip((String)obj);
				action.run((String)obj);
			}
		};
				
		doubleClickAction.setToolTipText("Apply the suggested annotation over the indicated elements and refactor metamodel");		
	}
	
	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}
	private void showMessage(String message) {
		MessageDialog.openInformation(
			viewer.getControl().getShell(),
			"Assistance View",
			message);
	}
	
	public void refresh(SessionState session){
		if(session != null){
			viewer.refresh();
		}
	}
	
	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
	
	protected class AssistanceListener implements IPartListener {

			@Override
			public void partActivated(IWorkbenchPart part) {
				////System.out.println("Activated metamodel editor");
				if ( part instanceof MetamodelInteractiveEditor ) {
					MetamodelInteractiveEditor metamodelInteractiveEditor = (MetamodelInteractiveEditor) part;
					session = metamodelInteractiveEditor.getSession();
					viewer.refresh();
				}
			}

			@Override
			public void partBroughtToTop(IWorkbenchPart part) { }

			@Override
			public void partClosed(IWorkbenchPart part) { }

			@Override
			public void partDeactivated(IWorkbenchPart part) { }

			@Override
			public void partOpened(IWorkbenchPart part) { }		
		}
}