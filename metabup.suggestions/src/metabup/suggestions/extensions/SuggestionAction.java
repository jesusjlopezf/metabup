package metabup.suggestions.extensions;


import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;

import metabup.metamodel.AnnotatedElement;

public abstract class SuggestionAction extends Action implements ISuggestion{
	private String name = null;
	private List<AnnotatedElement> elements = new ArrayList<AnnotatedElement>();
	
	@Override
	public final void run() {
		execute(elements);
	}

	public final String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AnnotatedElement> getElements() {
		return elements;
	}

	public void setElements(List<AnnotatedElement> elements) {
		this.elements = elements;
	}
}
