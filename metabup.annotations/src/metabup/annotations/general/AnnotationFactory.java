/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.general;

import java.util.Collection;
import java.util.HashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

import metabup.annotations.IAnnotationFactory;
import metabup.annotations.extensionpoint.ConstraintAnnotation;
import metabup.annotations.extensionpoint.UserAnnotation;


public class AnnotationFactory implements IAnnotationFactory {
	private HashMap<String, Class<? extends Annotation>> annotations = new HashMap<String, Class<? extends Annotation>>();
	
	public boolean initializeFactory() {
		// add user annotations
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		
		// constraint annotations
		IConfigurationElement[] extensions = reg.getConfigurationElementsFor(metabup.annotations.Activator.ANNOTATION_CONSTRAINT_EXTENSION_ID);
		
		for(int i = 0; i<extensions.length; i++){
			UserAnnotation ann;
			
			try {
				ann = (UserAnnotation) extensions[i].createExecutableExtension("class");
				this.addAnnotation(ann.getClass());
				System.out.println("this is my annotation: " + ann.getName());
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// design annotations
		extensions = reg.getConfigurationElementsFor(metabup.annotations.Activator.ANNOTATION_DESIGN_EXTENSION_ID);
		
		for(int i = 0; i<extensions.length; i++){
			UserAnnotation ann;
			
			try {
				ann = (UserAnnotation) extensions[i].createExecutableExtension("class");
				this.addAnnotation(ann.getClass());
				System.out.println("this is my annotation: " + ann.getName());
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// design annotations
		extensions = reg.getConfigurationElementsFor(metabup.annotations.Activator.ANNOTATION_REFACTORING_EXTENSION_ID);
				
		for(int i = 0; i<extensions.length; i++){
			UserAnnotation ann;
					
			try {
				ann = (UserAnnotation) extensions[i].createExecutableExtension("class");
				this.addAnnotation(ann.getClass());
				System.out.println("this is my annotation: " + ann.getName());
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// add native annotations
		this.addAnnotation(metabup.annotations.catalog.constraint.Unique.class);
		this.addAnnotation(metabup.annotations.catalog.constraint.Acyclic.class);
		this.addAnnotation(metabup.annotations.catalog.constraint.Irreflexive.class);
		this.addAnnotation(metabup.annotations.catalog.constraint.Tree.class);
		this.addAnnotation(metabup.annotations.catalog.constraint.CycleWith.class);
		this.addAnnotation(metabup.annotations.catalog.constraint.Covering.class);
		this.addAnnotation(metabup.annotations.catalog.constraint.Constraint.class);
		this.addAnnotation(metabup.annotations.catalog.constraint.Root.class);
		
		this.addAnnotation(metabup.annotations.catalog.geometry.Overlapping.class);
		this.addAnnotation(metabup.annotations.catalog.geometry.Adjacency.class);
		this.addAnnotation(metabup.annotations.catalog.geometry.Containment.class);
		
		this.addAnnotation(metabup.annotations.catalog.design.Abstract.class);
		this.addAnnotation(metabup.annotations.catalog.design.Composition.class);
		this.addAnnotation(metabup.annotations.catalog.design.Opposite.class);
		
		//this.addAnnotation(metabup.annotations.catalog.refactoring.General.class);
		this.addAnnotation(metabup.annotations.catalog.refactoring.Group.class);
		this.addAnnotation(metabup.annotations.catalog.refactoring.InLine.class);
		this.addAnnotation(metabup.annotations.catalog.refactoring.Merge.class);
		this.addAnnotation(metabup.annotations.catalog.refactoring.Multiplicity.class);
		this.addAnnotation(metabup.annotations.catalog.refactoring.Remove.class);
		this.addAnnotation(metabup.annotations.catalog.refactoring.Pluralize.class);
		this.addAnnotation(metabup.annotations.catalog.refactoring.Singularize.class);
		this.addAnnotation(metabup.annotations.catalog.refactoring.Rename.class);	
		
		System.out.println("yop");
		for(String s : this.listAllAnnotationsNames()) System.out.println(s);
		return true;
		
	}
	
	public void addAnnotation(Class<? extends Annotation> annotation){
		annotations.put(annotation.getSimpleName().toLowerCase(), annotation);
	}
	
	public boolean existsAnnotation(String name){
		if(this.getAnnotation(name) != null) return true;
		else return false;
	}
	
	public Class<? extends Annotation> getAnnotation(String name){
		name = name.toLowerCase();
		Class<? extends Annotation> ann = annotations.get(name);
		if(ann != null) return ann;
		else return null;
	}
	
	public Class<? extends ConstraintAnnotation> getConstraintAnnotation(String name){
		name = name.toLowerCase();
		Class<? extends Annotation> ann = annotations.get(name);
		if(ann != null && ConstraintAnnotation.class.isAssignableFrom(ann)) return (Class<? extends ConstraintAnnotation>) ann;
		else return null;
	}
	
	public Collection<Class<? extends Annotation>> listAllAnnotations(){
		return annotations.values();
	}
	
	public String[] listAllAnnotationsNames(){
		return annotations.keySet().toArray(new String[annotations.size()]);
	}		
	
	/*
	private boolean removeAnnotation(String modelName){
		//eliminar anotaci�n de annotations (no parece necesario de momento)
	}
	*/
	
}
