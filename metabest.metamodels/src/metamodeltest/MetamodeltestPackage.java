/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see metamodeltest.MetamodeltestFactory
 * @model kind="package"
 * @generated
 */
public interface MetamodeltestPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "metamodeltest";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/metabest.metamodels/model/metamodeltest.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "metabest.metamodels";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MetamodeltestPackage eINSTANCE = metamodeltest.impl.MetamodeltestPackageImpl.init();

	/**
	 * The meta object id for the '{@link metamodeltest.impl.MetaModelTestImpl <em>Meta Model Test</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.MetaModelTestImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getMetaModelTest()
	 * @generated
	 */
	int META_MODEL_TEST = 0;

	/**
	 * The feature id for the '<em><b>Library</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_TEST__LIBRARY = 0;

	/**
	 * The feature id for the '<em><b>Tests</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_TEST__TESTS = 1;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_TEST__IMPORTS = 2;

	/**
	 * The number of structural features of the '<em>Meta Model Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_TEST_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Meta Model Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_MODEL_TEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.DescribedElementImpl <em>Described Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.DescribedElementImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getDescribedElement()
	 * @generated
	 */
	int DESCRIBED_ELEMENT = 105;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ELEMENT__DESCRIPTION = 0;

	/**
	 * The number of structural features of the '<em>Described Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Described Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIBED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AssertionCallImpl <em>Assertion Call</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AssertionCallImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertionCall()
	 * @generated
	 */
	int ASSERTION_CALL = 54;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_CALL__DESCRIPTION = DESCRIBED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_CALL__ANNOTATIONS = DESCRIBED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>OC Lcomparison</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_CALL__OC_LCOMPARISON = DESCRIBED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Assertion Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_CALL_FEATURE_COUNT = DESCRIBED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Assertion Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_CALL_OPERATION_COUNT = DESCRIBED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AssertionImpl <em>Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AssertionImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertion()
	 * @generated
	 */
	int ASSERTION = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__DESCRIPTION = ASSERTION_CALL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__ANNOTATIONS = ASSERTION_CALL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>OC Lcomparison</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__OC_LCOMPARISON = ASSERTION_CALL__OC_LCOMPARISON;

	/**
	 * The feature id for the '<em><b>Selector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION__SELECTOR = ASSERTION_CALL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_FEATURE_COUNT = ASSERTION_CALL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_OPERATION_COUNT = ASSERTION_CALL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.QuantifierImpl <em>Quantifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.QuantifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getQuantifier()
	 * @generated
	 */
	int QUANTIFIER = 2;

	/**
	 * The number of structural features of the '<em>Quantifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIFIER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Quantifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIFIER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.NoneImpl <em>None</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.NoneImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNone()
	 * @generated
	 */
	int NONE = 3;

	/**
	 * The number of structural features of the '<em>None</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NONE_FEATURE_COUNT = QUANTIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>None</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NONE_OPERATION_COUNT = QUANTIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.NumberImpl <em>Number</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.NumberImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNumber()
	 * @generated
	 */
	int NUMBER = 4;

	/**
	 * The feature id for the '<em><b>N</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER__N = QUANTIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_FEATURE_COUNT = QUANTIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_OPERATION_COUNT = QUANTIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.EveryImpl <em>Every</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.EveryImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getEvery()
	 * @generated
	 */
	int EVERY = 5;

	/**
	 * The feature id for the '<em><b>Not</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVERY__NOT = QUANTIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Every</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVERY_FEATURE_COUNT = QUANTIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Every</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVERY_OPERATION_COUNT = QUANTIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.LessThanImpl <em>Less Than</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.LessThanImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLessThan()
	 * @generated
	 */
	int LESS_THAN = 6;

	/**
	 * The feature id for the '<em><b>N</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN__N = NUMBER__N;

	/**
	 * The feature id for the '<em><b>And</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN__AND = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Or Equal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN__OR_EQUAL = NUMBER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Less Than</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Less Than</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LESS_THAN_OPERATION_COUNT = NUMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.MoreThanImpl <em>More Than</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.MoreThanImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getMoreThan()
	 * @generated
	 */
	int MORE_THAN = 7;

	/**
	 * The feature id for the '<em><b>N</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MORE_THAN__N = NUMBER__N;

	/**
	 * The feature id for the '<em><b>And</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MORE_THAN__AND = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Or Equal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MORE_THAN__OR_EQUAL = NUMBER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>More Than</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MORE_THAN_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>More Than</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MORE_THAN_OPERATION_COUNT = NUMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ElementAssertionImpl <em>Element Assertion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ElementAssertionImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementAssertion()
	 * @generated
	 */
	int ELEMENT_ASSERTION = 8;

	/**
	 * The number of structural features of the '<em>Element Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_ASSERTION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Element Assertion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_ASSERTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.NameImpl <em>Name</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.NameImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getName_()
	 * @generated
	 */
	int NAME = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME__NAME = 0;

	/**
	 * The number of structural features of the '<em>Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Name</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AbstractImpl <em>Abstract</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AbstractImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAbstract()
	 * @generated
	 */
	int ABSTRACT = 10;

	/**
	 * The number of structural features of the '<em>Abstract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Abstract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.FilterImpl <em>Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.FilterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFilter()
	 * @generated
	 */
	int FILTER = 11;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER__QUALIFIER = 0;

	/**
	 * The number of structural features of the '<em>Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ElementFilterImpl <em>Element Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ElementFilterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementFilter()
	 * @generated
	 */
	int ELEMENT_FILTER = 12;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FILTER__QUALIFIER = FILTER__QUALIFIER;

	/**
	 * The number of structural features of the '<em>Element Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FILTER_FEATURE_COUNT = FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Element Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FILTER_OPERATION_COUNT = FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ClassFilterImpl <em>Class Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ClassFilterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassFilter()
	 * @generated
	 */
	int CLASS_FILTER = 13;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILTER__QUALIFIER = ELEMENT_FILTER__QUALIFIER;

	/**
	 * The number of structural features of the '<em>Class Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILTER_FEATURE_COUNT = ELEMENT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Class Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_FILTER_OPERATION_COUNT = ELEMENT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.FeatureFilterImpl <em>Feature Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.FeatureFilterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureFilter()
	 * @generated
	 */
	int FEATURE_FILTER = 14;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FILTER__QUALIFIER = ELEMENT_FILTER__QUALIFIER;

	/**
	 * The number of structural features of the '<em>Feature Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FILTER_FEATURE_COUNT = ELEMENT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Feature Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FILTER_OPERATION_COUNT = ELEMENT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AttributeFilterImpl <em>Attribute Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AttributeFilterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAttributeFilter()
	 * @generated
	 */
	int ATTRIBUTE_FILTER = 15;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FILTER__QUALIFIER = FEATURE_FILTER__QUALIFIER;

	/**
	 * The number of structural features of the '<em>Attribute Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FILTER_FEATURE_COUNT = FEATURE_FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Attribute Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FILTER_OPERATION_COUNT = FEATURE_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ReferenceFilterImpl <em>Reference Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ReferenceFilterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getReferenceFilter()
	 * @generated
	 */
	int REFERENCE_FILTER = 16;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FILTER__QUALIFIER = FEATURE_FILTER__QUALIFIER;

	/**
	 * The number of structural features of the '<em>Reference Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FILTER_FEATURE_COUNT = FEATURE_FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Reference Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_FILTER_OPERATION_COUNT = FEATURE_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.QualifierImpl <em>Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.QualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getQualifier()
	 * @generated
	 */
	int QUALIFIER = 18;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER__NEGATIVE = 0;

	/**
	 * The number of structural features of the '<em>Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALIFIER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ElementQualifierImpl <em>Element Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ElementQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementQualifier()
	 * @generated
	 */
	int ELEMENT_QUALIFIER = 17;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_QUALIFIER__NEGATIVE = QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Element Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_QUALIFIER_FEATURE_COUNT = QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Element Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_QUALIFIER_OPERATION_COUNT = QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.NamedImpl <em>Named</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.NamedImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNamed()
	 * @generated
	 */
	int NAMED = 19;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED__NEGATIVE = ELEMENT_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED__CASE = ELEMENT_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED__NAME = ELEMENT_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED__SIZE = ELEMENT_QUALIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED__WORD_NATURE = ELEMENT_QUALIFIER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_FEATURE_COUNT = ELEMENT_QUALIFIER_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_OPERATION_COUNT = ELEMENT_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ClassQualifierImpl <em>Class Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ClassQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassQualifier()
	 * @generated
	 */
	int CLASS_QUALIFIER = 20;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_QUALIFIER__NEGATIVE = QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Class Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_QUALIFIER_FEATURE_COUNT = QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Class Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_QUALIFIER_OPERATION_COUNT = QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.FeatureQualifierImpl <em>Feature Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.FeatureQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureQualifier()
	 * @generated
	 */
	int FEATURE_QUALIFIER = 21;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_QUALIFIER__NEGATIVE = QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Feature Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_QUALIFIER_FEATURE_COUNT = QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Feature Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_QUALIFIER_OPERATION_COUNT = QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AttributeQualifierImpl <em>Attribute Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AttributeQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAttributeQualifier()
	 * @generated
	 */
	int ATTRIBUTE_QUALIFIER = 22;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_QUALIFIER__NEGATIVE = QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Attribute Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_QUALIFIER_FEATURE_COUNT = QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Attribute Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_QUALIFIER_OPERATION_COUNT = QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ReferenceQualifierImpl <em>Reference Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ReferenceQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getReferenceQualifier()
	 * @generated
	 */
	int REFERENCE_QUALIFIER = 23;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_QUALIFIER__NEGATIVE = QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Reference Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_QUALIFIER_FEATURE_COUNT = QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Reference Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_QUALIFIER_OPERATION_COUNT = QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AbstractionImpl <em>Abstraction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AbstractionImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAbstraction()
	 * @generated
	 */
	int ABSTRACTION = 24;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACTION__NEGATIVE = CLASS_QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Abstraction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACTION_FEATURE_COUNT = CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Abstraction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACTION_OPERATION_COUNT = CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.InheritanceImpl <em>Inheritance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.InheritanceImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInheritance()
	 * @generated
	 */
	int INHERITANCE = 45;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITANCE__NEGATIVE = CLASS_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Length</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITANCE__LENGTH = CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Paths</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITANCE__PATHS = CLASS_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Inheritance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITANCE_FEATURE_COUNT = CLASS_QUALIFIER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Inheritance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITANCE_OPERATION_COUNT = CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.IsSuperToImpl <em>Is Super To</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.IsSuperToImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getIsSuperTo()
	 * @generated
	 */
	int IS_SUPER_TO = 25;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUPER_TO__NEGATIVE = INHERITANCE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Length</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUPER_TO__LENGTH = INHERITANCE__LENGTH;

	/**
	 * The feature id for the '<em><b>Paths</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUPER_TO__PATHS = INHERITANCE__PATHS;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUPER_TO__CLASS_SELS = INHERITANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Or Equal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUPER_TO__OR_EQUAL = INHERITANCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Is Super To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUPER_TO_FEATURE_COUNT = INHERITANCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Is Super To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUPER_TO_OPERATION_COUNT = INHERITANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.IsSubToImpl <em>Is Sub To</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.IsSubToImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getIsSubTo()
	 * @generated
	 */
	int IS_SUB_TO = 26;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUB_TO__NEGATIVE = INHERITANCE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Length</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUB_TO__LENGTH = INHERITANCE__LENGTH;

	/**
	 * The feature id for the '<em><b>Paths</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUB_TO__PATHS = INHERITANCE__PATHS;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUB_TO__CLASS_SELS = INHERITANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Or Equal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUB_TO__OR_EQUAL = INHERITANCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Is Sub To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUB_TO_FEATURE_COUNT = INHERITANCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Is Sub To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IS_SUB_TO_OPERATION_COUNT = INHERITANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ClassRelationImpl <em>Class Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ClassRelationImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassRelation()
	 * @generated
	 */
	int CLASS_RELATION = 27;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_RELATION__NEGATIVE = CLASS_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_RELATION__CLASS_SELS = CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Jumps</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_RELATION__JUMPS = CLASS_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_RELATION__BY_INHERITANCE = CLASS_QUALIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Containment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_RELATION__CONTAINMENT = CLASS_QUALIFIER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Strict</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_RELATION__STRICT = CLASS_QUALIFIER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Class Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_RELATION_FEATURE_COUNT = CLASS_QUALIFIER_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Class Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_RELATION_OPERATION_COUNT = CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ElementRelationImpl <em>Element Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ElementRelationImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementRelation()
	 * @generated
	 */
	int ELEMENT_RELATION = 89;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_RELATION__NEGATIVE = CLASS_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_RELATION__BY_INHERITANCE = CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Element Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_RELATION_FEATURE_COUNT = CLASS_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Element Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_RELATION_OPERATION_COUNT = CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.FeatureContainmentImpl <em>Feature Containment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.FeatureContainmentImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureContainment()
	 * @generated
	 */
	int FEATURE_CONTAINMENT = 28;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONTAINMENT__NEGATIVE = ELEMENT_RELATION__NEGATIVE;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONTAINMENT__BY_INHERITANCE = ELEMENT_RELATION__BY_INHERITANCE;

	/**
	 * The feature id for the '<em><b>Feat Sels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONTAINMENT__FEAT_SELS = ELEMENT_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Feature Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONTAINMENT_FEATURE_COUNT = ELEMENT_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Feature Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONTAINMENT_OPERATION_COUNT = ELEMENT_RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.InheritedFeatureContainmentImpl <em>Inherited Feature Containment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.InheritedFeatureContainmentImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInheritedFeatureContainment()
	 * @generated
	 */
	int INHERITED_FEATURE_CONTAINMENT = 29;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITED_FEATURE_CONTAINMENT__NEGATIVE = FEATURE_CONTAINMENT__NEGATIVE;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITED_FEATURE_CONTAINMENT__BY_INHERITANCE = FEATURE_CONTAINMENT__BY_INHERITANCE;

	/**
	 * The feature id for the '<em><b>Feat Sels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITED_FEATURE_CONTAINMENT__FEAT_SELS = FEATURE_CONTAINMENT__FEAT_SELS;

	/**
	 * The number of structural features of the '<em>Inherited Feature Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITED_FEATURE_CONTAINMENT_FEATURE_COUNT = FEATURE_CONTAINMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Inherited Feature Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITED_FEATURE_CONTAINMENT_OPERATION_COUNT = FEATURE_CONTAINMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.InhLeafImpl <em>Inh Leaf</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.InhLeafImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInhLeaf()
	 * @generated
	 */
	int INH_LEAF = 30;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INH_LEAF__NEGATIVE = CLASS_QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Inh Leaf</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INH_LEAF_FEATURE_COUNT = CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Inh Leaf</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INH_LEAF_OPERATION_COUNT = CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.InhRootImpl <em>Inh Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.InhRootImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInhRoot()
	 * @generated
	 */
	int INH_ROOT = 31;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INH_ROOT__NEGATIVE = CLASS_QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Inh Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INH_ROOT_FEATURE_COUNT = CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Inh Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INH_ROOT_OPERATION_COUNT = CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.CompositionLeafImpl <em>Composition Leaf</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.CompositionLeafImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCompositionLeaf()
	 * @generated
	 */
	int COMPOSITION_LEAF = 32;

	/**
	 * The number of structural features of the '<em>Composition Leaf</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION_LEAF_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Composition Leaf</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION_LEAF_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.MaxMultiplicityImpl <em>Max Multiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.MaxMultiplicityImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getMaxMultiplicity()
	 * @generated
	 */
	int MAX_MULTIPLICITY = 33;

	/**
	 * The number of structural features of the '<em>Max Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_MULTIPLICITY_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Max Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_MULTIPLICITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.FixedBoundImpl <em>Fixed Bound</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.FixedBoundImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFixedBound()
	 * @generated
	 */
	int FIXED_BOUND = 34;

	/**
	 * The number of structural features of the '<em>Fixed Bound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_BOUND_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Fixed Bound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_BOUND_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.InfiniteImpl <em>Infinite</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.InfiniteImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInfinite()
	 * @generated
	 */
	int INFINITE = 35;

	/**
	 * The number of structural features of the '<em>Infinite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFINITE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Infinite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFINITE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ClassContainmentImpl <em>Class Containment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ClassContainmentImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassContainment()
	 * @generated
	 */
	int CLASS_CONTAINMENT = 36;

	/**
	 * The number of structural features of the '<em>Class Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_CONTAINMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Class Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_CONTAINMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ClassContaineeImpl <em>Class Containee</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ClassContaineeImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassContainee()
	 * @generated
	 */
	int CLASS_CONTAINEE = 37;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_CONTAINEE__NEGATIVE = FEATURE_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_CONTAINEE__CLASS_SEL = FEATURE_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_CONTAINEE__BY_INHERITANCE = FEATURE_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Class Containee</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_CONTAINEE_FEATURE_COUNT = FEATURE_QUALIFIER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Class Containee</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_CONTAINEE_OPERATION_COUNT = FEATURE_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.TypedImpl <em>Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.TypedImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getTyped()
	 * @generated
	 */
	int TYPED = 38;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED__NEGATIVE = ATTRIBUTE_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED__TYPE = ATTRIBUTE_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_FEATURE_COUNT = ATTRIBUTE_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_OPERATION_COUNT = ATTRIBUTE_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.SubstringImpl <em>Substring</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.SubstringImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSubstring()
	 * @generated
	 */
	int SUBSTRING = 49;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING__NEGATIVE = NAMED__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING__CASE = NAMED__CASE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING__SIZE = NAMED__SIZE;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING__WORD_NATURE = NAMED__WORD_NATURE;

	/**
	 * The number of structural features of the '<em>Substring</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING_FEATURE_COUNT = NAMED_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Substring</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING_OPERATION_COUNT = NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PrefixImpl <em>Prefix</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PrefixImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPrefix()
	 * @generated
	 */
	int PREFIX = 39;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX__NEGATIVE = SUBSTRING__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX__CASE = SUBSTRING__CASE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX__NAME = SUBSTRING__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX__SIZE = SUBSTRING__SIZE;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX__WORD_NATURE = SUBSTRING__WORD_NATURE;

	/**
	 * The number of structural features of the '<em>Prefix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX_FEATURE_COUNT = SUBSTRING_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Prefix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX_OPERATION_COUNT = SUBSTRING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.SuffixImpl <em>Suffix</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.SuffixImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSuffix()
	 * @generated
	 */
	int SUFFIX = 40;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUFFIX__NEGATIVE = SUBSTRING__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUFFIX__CASE = SUBSTRING__CASE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUFFIX__NAME = SUBSTRING__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUFFIX__SIZE = SUBSTRING__SIZE;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUFFIX__WORD_NATURE = SUBSTRING__WORD_NATURE;

	/**
	 * The number of structural features of the '<em>Suffix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUFFIX_FEATURE_COUNT = SUBSTRING_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Suffix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUFFIX_OPERATION_COUNT = SUBSTRING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ContainsImpl <em>Contains</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ContainsImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContains()
	 * @generated
	 */
	int CONTAINS = 41;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__NEGATIVE = SUBSTRING__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__CASE = SUBSTRING__CASE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__NAME = SUBSTRING__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__SIZE = SUBSTRING__SIZE;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS__WORD_NATURE = SUBSTRING__WORD_NATURE;

	/**
	 * The number of structural features of the '<em>Contains</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS_FEATURE_COUNT = SUBSTRING_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Contains</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINS_OPERATION_COUNT = SUBSTRING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.SideClassImpl <em>Side Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.SideClassImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSideClass()
	 * @generated
	 */
	int SIDE_CLASS = 42;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIDE_CLASS__NEGATIVE = REFERENCE_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIDE_CLASS__CLASS_SEL = REFERENCE_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIDE_CLASS__BY_INHERITANCE = REFERENCE_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Side Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIDE_CLASS_FEATURE_COUNT = REFERENCE_QUALIFIER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Side Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIDE_CLASS_OPERATION_COUNT = REFERENCE_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.FromToImpl <em>From To</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.FromToImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFromTo()
	 * @generated
	 */
	int FROM_TO = 43;

	/**
	 * The number of structural features of the '<em>From To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_TO_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>From To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FROM_TO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.StrictNumberImpl <em>Strict Number</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.StrictNumberImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getStrictNumber()
	 * @generated
	 */
	int STRICT_NUMBER = 44;

	/**
	 * The feature id for the '<em><b>N</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRICT_NUMBER__N = NUMBER__N;

	/**
	 * The number of structural features of the '<em>Strict Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRICT_NUMBER_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Strict Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRICT_NUMBER_OPERATION_COUNT = NUMBER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.FeatureMultiplicityImpl <em>Feature Multiplicity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.FeatureMultiplicityImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureMultiplicity()
	 * @generated
	 */
	int FEATURE_MULTIPLICITY = 46;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MULTIPLICITY__NEGATIVE = FEATURE_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Min</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MULTIPLICITY__MIN = FEATURE_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MULTIPLICITY__MAX = FEATURE_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MULTIPLICITY_FEATURE_COUNT = FEATURE_QUALIFIER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Feature Multiplicity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_MULTIPLICITY_OPERATION_COUNT = FEATURE_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.UpperCaseImpl <em>Upper Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.UpperCaseImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getUpperCase()
	 * @generated
	 */
	int UPPER_CASE = 47;

	/**
	 * The number of structural features of the '<em>Upper Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPPER_CASE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Upper Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UPPER_CASE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.LowerCaseImpl <em>Lower Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.LowerCaseImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLowerCase()
	 * @generated
	 */
	int LOWER_CASE = 48;

	/**
	 * The number of structural features of the '<em>Lower Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOWER_CASE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Lower Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOWER_CASE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.NamedElementImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 102;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AssertionLibraryImpl <em>Assertion Library</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AssertionLibraryImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertionLibrary()
	 * @generated
	 */
	int ASSERTION_LIBRARY = 50;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_LIBRARY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_LIBRARY__DESCRIPTION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Definitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_LIBRARY__DEFINITIONS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Assertion Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_LIBRARY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Assertion Library</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_LIBRARY_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AssertionTestImpl <em>Assertion Test</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AssertionTestImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertionTest()
	 * @generated
	 */
	int ASSERTION_TEST = 51;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AssertionDefinitionImpl <em>Assertion Definition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AssertionDefinitionImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertionDefinition()
	 * @generated
	 */
	int ASSERTION_DEFINITION = 52;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.LibraryAssertionCallImpl <em>Library Assertion Call</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.LibraryAssertionCallImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLibraryAssertionCall()
	 * @generated
	 */
	int LIBRARY_ASSERTION_CALL = 53;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ParameterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 55;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.BooleanParameterImpl <em>Boolean Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.BooleanParameterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getBooleanParameter()
	 * @generated
	 */
	int BOOLEAN_PARAMETER = 56;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.IntegerParameterImpl <em>Integer Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.IntegerParameterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getIntegerParameter()
	 * @generated
	 */
	int INTEGER_PARAMETER = 57;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.StringParameterImpl <em>String Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.StringParameterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getStringParameter()
	 * @generated
	 */
	int STRING_PARAMETER = 58;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.UserDefinedParameterImpl <em>User Defined Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.UserDefinedParameterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getUserDefinedParameter()
	 * @generated
	 */
	int USER_DEFINED_PARAMETER = 59;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PathFilterImpl <em>Path Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PathFilterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathFilter()
	 * @generated
	 */
	int PATH_FILTER = 60;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.LengthImpl <em>Length</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.LengthImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLength()
	 * @generated
	 */
	int LENGTH = 61;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.SourceClassImpl <em>Source Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.SourceClassImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSourceClass()
	 * @generated
	 */
	int SOURCE_CLASS = 62;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.TargetClassImpl <em>Target Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.TargetClassImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getTargetClass()
	 * @generated
	 */
	int TARGET_CLASS = 63;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ContainmentReferenceImpl <em>Containment Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ContainmentReferenceImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContainmentReference()
	 * @generated
	 */
	int CONTAINMENT_REFERENCE = 64;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PathQualifierImpl <em>Path Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PathQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathQualifier()
	 * @generated
	 */
	int PATH_QUALIFIER = 65;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PathSideImpl <em>Path Side</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PathSideImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathSide()
	 * @generated
	 */
	int PATH_SIDE = 66;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PathTargetImpl <em>Path Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PathTargetImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathTarget()
	 * @generated
	 */
	int PATH_TARGET = 67;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PathSourceImpl <em>Path Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PathSourceImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathSource()
	 * @generated
	 */
	int PATH_SOURCE = 68;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ElementFeaturementImpl <em>Element Featurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ElementFeaturementImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementFeaturement()
	 * @generated
	 */
	int ELEMENT_FEATUREMENT = 69;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.StepClassImpl <em>Step Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.StepClassImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getStepClass()
	 * @generated
	 */
	int STEP_CLASS = 70;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ExistanceImpl <em>Existance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ExistanceImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getExistance()
	 * @generated
	 */
	int EXISTANCE = 71;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.SelectorImpl <em>Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.SelectorImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSelector()
	 * @generated
	 */
	int SELECTOR = 72;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ElementSelectorImpl <em>Element Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ElementSelectorImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementSelector()
	 * @generated
	 */
	int ELEMENT_SELECTOR = 73;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ClassSelectorImpl <em>Class Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ClassSelectorImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassSelector()
	 * @generated
	 */
	int CLASS_SELECTOR = 74;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.FeatureSelectorImpl <em>Feature Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.FeatureSelectorImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureSelector()
	 * @generated
	 */
	int FEATURE_SELECTOR = 75;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AttributeSelectorImpl <em>Attribute Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AttributeSelectorImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAttributeSelector()
	 * @generated
	 */
	int ATTRIBUTE_SELECTOR = 76;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ReferenceSelectorImpl <em>Reference Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ReferenceSelectorImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getReferenceSelector()
	 * @generated
	 */
	int REFERENCE_SELECTOR = 77;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PathSelectorImpl <em>Path Selector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PathSelectorImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathSelector()
	 * @generated
	 */
	int PATH_SELECTOR = 78;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.CompoundQualifierImpl <em>Compound Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.CompoundQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCompoundQualifier()
	 * @generated
	 */
	int COMPOUND_QUALIFIER = 81;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AndQualifierImpl <em>And Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AndQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAndQualifier()
	 * @generated
	 */
	int AND_QUALIFIER = 79;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.OrQualifierImpl <em>Or Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.OrQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getOrQualifier()
	 * @generated
	 */
	int OR_QUALIFIER = 80;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.CompoundPathQualifierImpl <em>Compound Path Qualifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.CompoundPathQualifierImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCompoundPathQualifier()
	 * @generated
	 */
	int COMPOUND_PATH_QUALIFIER = 82;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PathAndImpl <em>Path And</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PathAndImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathAnd()
	 * @generated
	 */
	int PATH_AND = 83;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PathOrImpl <em>Path Or</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PathOrImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathOr()
	 * @generated
	 */
	int PATH_OR = 84;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AttributeContainmentImpl <em>Attribute Containment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AttributeContainmentImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAttributeContainment()
	 * @generated
	 */
	int ATTRIBUTE_CONTAINMENT = 85;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ReferenceContainmentImpl <em>Reference Containment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ReferenceContainmentImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getReferenceContainment()
	 * @generated
	 */
	int REFERENCE_CONTAINMENT = 86;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ContRootImpl <em>Cont Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ContRootImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContRoot()
	 * @generated
	 */
	int CONT_ROOT = 87;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ContLeafImpl <em>Cont Leaf</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ContLeafImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContLeaf()
	 * @generated
	 */
	int CONT_LEAF = 88;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ClassReachesImpl <em>Class Reaches</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ClassReachesImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassReaches()
	 * @generated
	 */
	int CLASS_REACHES = 90;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ClassReachedByImpl <em>Class Reached By</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ClassReachedByImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassReachedBy()
	 * @generated
	 */
	int CLASS_REACHED_BY = 91;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ContainmentPathImpl <em>Containment Path</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ContainmentPathImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContainmentPath()
	 * @generated
	 */
	int CONTAINMENT_PATH = 92;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.WithInheritancePathImpl <em>With Inheritance Path</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.WithInheritancePathImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getWithInheritancePath()
	 * @generated
	 */
	int WITH_INHERITANCE_PATH = 93;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.CyclicPathImpl <em>Cyclic Path</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.CyclicPathImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCyclicPath()
	 * @generated
	 */
	int CYCLIC_PATH = 94;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.SomeImpl <em>Some</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.SomeImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSome()
	 * @generated
	 */
	int SOME = 95;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.InheritedFeatureImpl <em>Inherited Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.InheritedFeatureImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInheritedFeature()
	 * @generated
	 */
	int INHERITED_FEATURE = 96;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ClassCollectsImpl <em>Class Collects</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ClassCollectsImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassCollects()
	 * @generated
	 */
	int CLASS_COLLECTS = 97;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AnnotatedImpl <em>Annotated</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AnnotatedImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAnnotated()
	 * @generated
	 */
	int ANNOTATED = 98;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ImportImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 99;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ParameterValueImpl <em>Parameter Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ParameterValueImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getParameterValue()
	 * @generated
	 */
	int PARAMETER_VALUE = 100;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.SelfInheritanceImpl <em>Self Inheritance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.SelfInheritanceImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSelfInheritance()
	 * @generated
	 */
	int SELF_INHERITANCE = 101;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AnnotationImpl <em>Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AnnotationImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAnnotation()
	 * @generated
	 */
	int ANNOTATION = 103;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AnnotatedElementImpl <em>Annotated Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AnnotatedElementImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAnnotatedElement()
	 * @generated
	 */
	int ANNOTATED_ELEMENT = 104;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_ELEMENT__ANNOTATIONS = 0;

	/**
	 * The number of structural features of the '<em>Annotated Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Annotated Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_TEST__ANNOTATIONS = ANNOTATED_ELEMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_TEST__DESCRIPTION = ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Metamodel URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_TEST__METAMODEL_URI = ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Assertions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_TEST__ASSERTIONS = ANNOTATED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Dir URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_TEST__DIR_URI = ANNOTATED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Corresponding Ecore URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_TEST__CORRESPONDING_ECORE_URI = ANNOTATED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Assertion Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_TEST_FEATURE_COUNT = ANNOTATED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Assertion Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_TEST_OPERATION_COUNT = ANNOTATED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_DEFINITION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_DEFINITION__DESCRIPTION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Assertion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_DEFINITION__ASSERTION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Assertion Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_DEFINITION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Assertion Definition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSERTION_DEFINITION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ASSERTION_CALL__DESCRIPTION = ASSERTION_CALL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ASSERTION_CALL__ANNOTATIONS = ASSERTION_CALL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>OC Lcomparison</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ASSERTION_CALL__OC_LCOMPARISON = ASSERTION_CALL__OC_LCOMPARISON;

	/**
	 * The feature id for the '<em><b>Referenced Assertion</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ASSERTION_CALL__REFERENCED_ASSERTION = ASSERTION_CALL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ASSERTION_CALL__PARAMETERS = ASSERTION_CALL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Library Assertion Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ASSERTION_CALL_FEATURE_COUNT = ASSERTION_CALL_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Library Assertion Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIBRARY_ASSERTION_CALL_OPERATION_COUNT = ASSERTION_CALL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__PARAMETER_DEFINITION = 0;

	/**
	 * The feature id for the '<em><b>Parameter Reuse</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__PARAMETER_REUSE = 1;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Parameter Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER__PARAMETER_DEFINITION = PARAMETER__PARAMETER_DEFINITION;

	/**
	 * The feature id for the '<em><b>Parameter Reuse</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER__PARAMETER_REUSE = PARAMETER__PARAMETER_REUSE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER__VALUE = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Boolean Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Boolean Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_PARAMETER_OPERATION_COUNT = PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER__PARAMETER_DEFINITION = PARAMETER__PARAMETER_DEFINITION;

	/**
	 * The feature id for the '<em><b>Parameter Reuse</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER__PARAMETER_REUSE = PARAMETER__PARAMETER_REUSE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER__VALUE = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Integer Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Integer Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTEGER_PARAMETER_OPERATION_COUNT = PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter Definition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER__PARAMETER_DEFINITION = PARAMETER__PARAMETER_DEFINITION;

	/**
	 * The feature id for the '<em><b>Parameter Reuse</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER__PARAMETER_REUSE = PARAMETER__PARAMETER_REUSE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER__VALUE = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>String Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_PARAMETER_OPERATION_COUNT = PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_PARAMETER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_PARAMETER__DESCRIPTION = 1;

	/**
	 * The number of structural features of the '<em>User Defined Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_PARAMETER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>User Defined Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_DEFINED_PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_FILTER__QUALIFIER = FILTER__QUALIFIER;

	/**
	 * The feature id for the '<em><b>Path Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_FILTER__PATH_QUALIFIER = FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Path Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_FILTER_FEATURE_COUNT = FILTER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Path Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_FILTER_OPERATION_COUNT = FILTER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Min</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENGTH__MIN = 0;

	/**
	 * The feature id for the '<em><b>Max</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENGTH__MAX = 1;

	/**
	 * The number of structural features of the '<em>Length</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENGTH_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Length</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LENGTH_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_CLASS__NEGATIVE = SIDE_CLASS__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_CLASS__CLASS_SEL = SIDE_CLASS__CLASS_SEL;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_CLASS__BY_INHERITANCE = SIDE_CLASS__BY_INHERITANCE;

	/**
	 * The number of structural features of the '<em>Source Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_CLASS_FEATURE_COUNT = SIDE_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Source Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_CLASS_OPERATION_COUNT = SIDE_CLASS_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_CLASS__NEGATIVE = SIDE_CLASS__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_CLASS__CLASS_SEL = SIDE_CLASS__CLASS_SEL;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_CLASS__BY_INHERITANCE = SIDE_CLASS__BY_INHERITANCE;

	/**
	 * The number of structural features of the '<em>Target Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_CLASS_FEATURE_COUNT = SIDE_CLASS_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Target Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TARGET_CLASS_OPERATION_COUNT = SIDE_CLASS_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINMENT_REFERENCE__NEGATIVE = REFERENCE_QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Containment Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINMENT_REFERENCE_FEATURE_COUNT = REFERENCE_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Containment Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINMENT_REFERENCE_OPERATION_COUNT = REFERENCE_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_QUALIFIER__NEGATIVE = QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Path Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_QUALIFIER_FEATURE_COUNT = QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Path Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_QUALIFIER_OPERATION_COUNT = QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SIDE__NEGATIVE = PATH_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SIDE__CLASS_SELS = PATH_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Path Side</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SIDE_FEATURE_COUNT = PATH_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Path Side</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SIDE_OPERATION_COUNT = PATH_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_TARGET__NEGATIVE = PATH_SIDE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_TARGET__CLASS_SELS = PATH_SIDE__CLASS_SELS;

	/**
	 * The number of structural features of the '<em>Path Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_TARGET_FEATURE_COUNT = PATH_SIDE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Path Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_TARGET_OPERATION_COUNT = PATH_SIDE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SOURCE__NEGATIVE = PATH_SIDE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SOURCE__CLASS_SELS = PATH_SIDE__CLASS_SELS;

	/**
	 * The number of structural features of the '<em>Path Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SOURCE_FEATURE_COUNT = PATH_SIDE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Path Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SOURCE_OPERATION_COUNT = PATH_SIDE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATUREMENT__NEGATIVE = PATH_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Element Sels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATUREMENT__ELEMENT_SELS = PATH_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Element Featurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATUREMENT_FEATURE_COUNT = PATH_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Element Featurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATUREMENT_OPERATION_COUNT = PATH_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_CLASS__NEGATIVE = PATH_SIDE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_CLASS__CLASS_SELS = PATH_SIDE__CLASS_SELS;

	/**
	 * The number of structural features of the '<em>Step Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_CLASS_FEATURE_COUNT = PATH_SIDE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Step Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STEP_CLASS_OPERATION_COUNT = PATH_SIDE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXISTANCE__NEGATIVE = ELEMENT_QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Existance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXISTANCE_FEATURE_COUNT = ELEMENT_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Existance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXISTANCE_OPERATION_COUNT = ELEMENT_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quantifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTOR__QUANTIFIER = 0;

	/**
	 * The number of structural features of the '<em>Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTOR_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELECTOR_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Quantifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_SELECTOR__QUANTIFIER = SELECTOR__QUANTIFIER;

	/**
	 * The feature id for the '<em><b>Element Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_SELECTOR__ELEMENT_FILTER = SELECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Element Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_SELECTOR__ELEMENT_CONDITIONER = SELECTOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Element Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_SELECTOR_FEATURE_COUNT = SELECTOR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Element Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_SELECTOR_OPERATION_COUNT = SELECTOR_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quantifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SELECTOR__QUANTIFIER = SELECTOR__QUANTIFIER;

	/**
	 * The feature id for the '<em><b>Class Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SELECTOR__CLASS_CONDITIONER = SELECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Class Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SELECTOR__CLASS_FILTER = SELECTOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Class Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SELECTOR_FEATURE_COUNT = SELECTOR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Class Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_SELECTOR_OPERATION_COUNT = SELECTOR_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quantifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_SELECTOR__QUANTIFIER = SELECTOR__QUANTIFIER;

	/**
	 * The feature id for the '<em><b>Feature Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_SELECTOR__FEATURE_FILTER = SELECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_SELECTOR__FEATURE_CONDITIONER = SELECTOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_SELECTOR_FEATURE_COUNT = SELECTOR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Feature Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_SELECTOR_OPERATION_COUNT = SELECTOR_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quantifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SELECTOR__QUANTIFIER = SELECTOR__QUANTIFIER;

	/**
	 * The feature id for the '<em><b>Attribute Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER = SELECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER = SELECTOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Attribute Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SELECTOR_FEATURE_COUNT = SELECTOR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Attribute Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_SELECTOR_OPERATION_COUNT = SELECTOR_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quantifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SELECTOR__QUANTIFIER = SELECTOR__QUANTIFIER;

	/**
	 * The feature id for the '<em><b>Reference Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SELECTOR__REFERENCE_FILTER = SELECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reference Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SELECTOR__REFERENCE_CONDITIONER = SELECTOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Reference Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SELECTOR_FEATURE_COUNT = SELECTOR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Reference Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_SELECTOR_OPERATION_COUNT = SELECTOR_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quantifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SELECTOR__QUANTIFIER = SELECTOR__QUANTIFIER;

	/**
	 * The feature id for the '<em><b>Path Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SELECTOR__PATH_FILTER = SELECTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Path Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SELECTOR__PATH_CONDITIONER = SELECTOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Path Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SELECTOR_FEATURE_COUNT = SELECTOR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Path Selector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_SELECTOR_OPERATION_COUNT = SELECTOR_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_QUALIFIER__NEGATIVE = QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_QUALIFIER__CHILDREN = QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Compound Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_QUALIFIER_FEATURE_COUNT = QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Compound Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_QUALIFIER_OPERATION_COUNT = QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_QUALIFIER__NEGATIVE = COMPOUND_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_QUALIFIER__CHILDREN = COMPOUND_QUALIFIER__CHILDREN;

	/**
	 * The number of structural features of the '<em>And Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_QUALIFIER_FEATURE_COUNT = COMPOUND_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>And Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_QUALIFIER_OPERATION_COUNT = COMPOUND_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_QUALIFIER__NEGATIVE = COMPOUND_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_QUALIFIER__CHILDREN = COMPOUND_QUALIFIER__CHILDREN;

	/**
	 * The number of structural features of the '<em>Or Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_QUALIFIER_FEATURE_COUNT = COMPOUND_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Or Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_QUALIFIER_OPERATION_COUNT = COMPOUND_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_PATH_QUALIFIER__NEGATIVE = PATH_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_PATH_QUALIFIER__CHILDREN = PATH_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Compound Path Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_PATH_QUALIFIER_FEATURE_COUNT = PATH_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Compound Path Qualifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOUND_PATH_QUALIFIER_OPERATION_COUNT = PATH_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_AND__NEGATIVE = COMPOUND_PATH_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_AND__CHILDREN = COMPOUND_PATH_QUALIFIER__CHILDREN;

	/**
	 * The number of structural features of the '<em>Path And</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_AND_FEATURE_COUNT = COMPOUND_PATH_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Path And</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_AND_OPERATION_COUNT = COMPOUND_PATH_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_OR__NEGATIVE = COMPOUND_PATH_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_OR__CHILDREN = COMPOUND_PATH_QUALIFIER__CHILDREN;

	/**
	 * The number of structural features of the '<em>Path Or</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_OR_FEATURE_COUNT = COMPOUND_PATH_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Path Or</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_OR_OPERATION_COUNT = COMPOUND_PATH_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTAINMENT__NEGATIVE = ELEMENT_RELATION__NEGATIVE;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTAINMENT__BY_INHERITANCE = ELEMENT_RELATION__BY_INHERITANCE;

	/**
	 * The feature id for the '<em><b>Att Sels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTAINMENT__ATT_SELS = ELEMENT_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTAINMENT_FEATURE_COUNT = ELEMENT_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Attribute Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTAINMENT_OPERATION_COUNT = ELEMENT_RELATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONTAINMENT__NEGATIVE = ELEMENT_RELATION__NEGATIVE;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONTAINMENT__BY_INHERITANCE = ELEMENT_RELATION__BY_INHERITANCE;

	/**
	 * The feature id for the '<em><b>Ref Sels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONTAINMENT__REF_SELS = ELEMENT_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONTAINMENT_FEATURE_COUNT = ELEMENT_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Reference Containment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONTAINMENT_OPERATION_COUNT = ELEMENT_RELATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONT_ROOT__NEGATIVE = CLASS_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Absolute</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONT_ROOT__ABSOLUTE = CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cont Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONT_ROOT_FEATURE_COUNT = CLASS_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Cont Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONT_ROOT_OPERATION_COUNT = CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONT_LEAF__NEGATIVE = CLASS_QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Cont Leaf</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONT_LEAF_FEATURE_COUNT = CLASS_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Cont Leaf</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONT_LEAF_OPERATION_COUNT = CLASS_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHES__NEGATIVE = CLASS_RELATION__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHES__CLASS_SELS = CLASS_RELATION__CLASS_SELS;

	/**
	 * The feature id for the '<em><b>Jumps</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHES__JUMPS = CLASS_RELATION__JUMPS;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHES__BY_INHERITANCE = CLASS_RELATION__BY_INHERITANCE;

	/**
	 * The feature id for the '<em><b>Containment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHES__CONTAINMENT = CLASS_RELATION__CONTAINMENT;

	/**
	 * The feature id for the '<em><b>Strict</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHES__STRICT = CLASS_RELATION__STRICT;

	/**
	 * The number of structural features of the '<em>Class Reaches</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHES_FEATURE_COUNT = CLASS_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Class Reaches</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHES_OPERATION_COUNT = CLASS_RELATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHED_BY__NEGATIVE = CLASS_RELATION__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHED_BY__CLASS_SELS = CLASS_RELATION__CLASS_SELS;

	/**
	 * The feature id for the '<em><b>Jumps</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHED_BY__JUMPS = CLASS_RELATION__JUMPS;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHED_BY__BY_INHERITANCE = CLASS_RELATION__BY_INHERITANCE;

	/**
	 * The feature id for the '<em><b>Containment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHED_BY__CONTAINMENT = CLASS_RELATION__CONTAINMENT;

	/**
	 * The feature id for the '<em><b>Strict</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHED_BY__STRICT = CLASS_RELATION__STRICT;

	/**
	 * The number of structural features of the '<em>Class Reached By</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHED_BY_FEATURE_COUNT = CLASS_RELATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Class Reached By</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_REACHED_BY_OPERATION_COUNT = CLASS_RELATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINMENT_PATH__NEGATIVE = PATH_QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Containment Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINMENT_PATH_FEATURE_COUNT = PATH_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Containment Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINMENT_PATH_OPERATION_COUNT = PATH_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITH_INHERITANCE_PATH__NEGATIVE = PATH_QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>With Inheritance Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITH_INHERITANCE_PATH_FEATURE_COUNT = PATH_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>With Inheritance Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WITH_INHERITANCE_PATH_OPERATION_COUNT = PATH_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CYCLIC_PATH__NEGATIVE = PATH_QUALIFIER__NEGATIVE;

	/**
	 * The number of structural features of the '<em>Cyclic Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CYCLIC_PATH_FEATURE_COUNT = PATH_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Cyclic Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CYCLIC_PATH_OPERATION_COUNT = PATH_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Some</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_FEATURE_COUNT = QUANTIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Some</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_OPERATION_COUNT = QUANTIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITED_FEATURE__NEGATIVE = FEATURE_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITED_FEATURE__CLASS_SEL = FEATURE_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Inherited Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITED_FEATURE_FEATURE_COUNT = FEATURE_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Inherited Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INHERITED_FEATURE_OPERATION_COUNT = FEATURE_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COLLECTS__NEGATIVE = CLASS_REACHES__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Class Sels</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COLLECTS__CLASS_SELS = CLASS_REACHES__CLASS_SELS;

	/**
	 * The feature id for the '<em><b>Jumps</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COLLECTS__JUMPS = CLASS_REACHES__JUMPS;

	/**
	 * The feature id for the '<em><b>By Inheritance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COLLECTS__BY_INHERITANCE = CLASS_REACHES__BY_INHERITANCE;

	/**
	 * The feature id for the '<em><b>Containment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COLLECTS__CONTAINMENT = CLASS_REACHES__CONTAINMENT;

	/**
	 * The feature id for the '<em><b>Strict</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COLLECTS__STRICT = CLASS_REACHES__STRICT;

	/**
	 * The feature id for the '<em><b>Collected Mult</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COLLECTS__COLLECTED_MULT = CLASS_REACHES_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Class Collects</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COLLECTS_FEATURE_COUNT = CLASS_REACHES_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Class Collects</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COLLECTS_OPERATION_COUNT = CLASS_REACHES_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED__NEGATIVE = ELEMENT_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED__NAME = ELEMENT_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Annotated</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_FEATURE_COUNT = ELEMENT_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Annotated</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_OPERATION_COUNT = ELEMENT_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__IMPORT_URI = 0;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Param Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE__PARAM_DEFINITION = 0;

	/**
	 * The number of structural features of the '<em>Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Parameter Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_VALUE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_INHERITANCE__NEGATIVE = INHERITANCE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Length</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_INHERITANCE__LENGTH = INHERITANCE__LENGTH;

	/**
	 * The feature id for the '<em><b>Paths</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_INHERITANCE__PATHS = INHERITANCE__PATHS;

	/**
	 * The number of structural features of the '<em>Self Inheritance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_INHERITANCE_FEATURE_COUNT = INHERITANCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Self Inheritance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SELF_INHERITANCE_OPERATION_COUNT = INHERITANCE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__NAME = 0;

	/**
	 * The number of structural features of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.WordNatureImpl <em>Word Nature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.WordNatureImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getWordNature()
	 * @generated
	 */
	int WORD_NATURE = 110;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORD_NATURE__NEGATIVE = NAMED__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORD_NATURE__CASE = NAMED__CASE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORD_NATURE__NAME = NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORD_NATURE__SIZE = NAMED__SIZE;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORD_NATURE__WORD_NATURE = NAMED__WORD_NATURE;

	/**
	 * The number of structural features of the '<em>Word Nature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORD_NATURE_FEATURE_COUNT = NAMED_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Word Nature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WORD_NATURE_OPERATION_COUNT = NAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.SynonymImpl <em>Synonym</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.SynonymImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSynonym()
	 * @generated
	 */
	int SYNONYM = 106;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNONYM__NEGATIVE = WORD_NATURE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNONYM__CASE = WORD_NATURE__CASE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNONYM__NAME = WORD_NATURE__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNONYM__SIZE = WORD_NATURE__SIZE;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNONYM__WORD_NATURE = WORD_NATURE__WORD_NATURE;

	/**
	 * The number of structural features of the '<em>Synonym</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNONYM_FEATURE_COUNT = WORD_NATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Synonym</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYNONYM_OPERATION_COUNT = WORD_NATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.VerbImpl <em>Verb</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.VerbImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getVerb()
	 * @generated
	 */
	int VERB = 107;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERB__NEGATIVE = WORD_NATURE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERB__CASE = WORD_NATURE__CASE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERB__NAME = WORD_NATURE__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERB__SIZE = WORD_NATURE__SIZE;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERB__WORD_NATURE = WORD_NATURE__WORD_NATURE;

	/**
	 * The number of structural features of the '<em>Verb</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERB_FEATURE_COUNT = WORD_NATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Verb</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERB_OPERATION_COUNT = WORD_NATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.NounImpl <em>Noun</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.NounImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNoun()
	 * @generated
	 */
	int NOUN = 108;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOUN__NEGATIVE = WORD_NATURE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOUN__CASE = WORD_NATURE__CASE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOUN__NAME = WORD_NATURE__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOUN__SIZE = WORD_NATURE__SIZE;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOUN__WORD_NATURE = WORD_NATURE__WORD_NATURE;

	/**
	 * The feature id for the '<em><b>Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOUN__NUM = WORD_NATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Noun</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOUN_FEATURE_COUNT = WORD_NATURE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Noun</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOUN_OPERATION_COUNT = WORD_NATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.AdjectiveImpl <em>Adjective</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.AdjectiveImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAdjective()
	 * @generated
	 */
	int ADJECTIVE = 109;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADJECTIVE__NEGATIVE = WORD_NATURE__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADJECTIVE__CASE = WORD_NATURE__CASE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADJECTIVE__NAME = WORD_NATURE__NAME;

	/**
	 * The feature id for the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADJECTIVE__SIZE = WORD_NATURE__SIZE;

	/**
	 * The feature id for the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADJECTIVE__WORD_NATURE = WORD_NATURE__WORD_NATURE;

	/**
	 * The number of structural features of the '<em>Adjective</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADJECTIVE_FEATURE_COUNT = WORD_NATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Adjective</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADJECTIVE_OPERATION_COUNT = WORD_NATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.PhraseNamedImpl <em>Phrase Named</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.PhraseNamedImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPhraseNamed()
	 * @generated
	 */
	int PHRASE_NAMED = 111;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHRASE_NAMED__NEGATIVE = ELEMENT_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Camelized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHRASE_NAMED__CAMELIZED = ELEMENT_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Words</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHRASE_NAMED__WORDS = ELEMENT_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Start</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHRASE_NAMED__START = ELEMENT_QUALIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>End</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHRASE_NAMED__END = ELEMENT_QUALIFIER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Pascal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHRASE_NAMED__PASCAL = ELEMENT_QUALIFIER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Phrase Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHRASE_NAMED_FEATURE_COUNT = ELEMENT_QUALIFIER_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Phrase Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PHRASE_NAMED_OPERATION_COUNT = ELEMENT_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.NamePartImpl <em>Name Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.NamePartImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNamePart()
	 * @generated
	 */
	int NAME_PART = 112;

	/**
	 * The number of structural features of the '<em>Name Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_PART_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Name Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAME_PART_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.LiteralImpl <em>Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.LiteralImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLiteral()
	 * @generated
	 */
	int LITERAL = 113;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL__LITERAL = NAME_PART_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_FEATURE_COUNT = NAME_PART_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_OPERATION_COUNT = NAME_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.TrivialStringImpl <em>Trivial String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.TrivialStringImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getTrivialString()
	 * @generated
	 */
	int TRIVIAL_STRING = 114;

	/**
	 * The number of structural features of the '<em>Trivial String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIVIAL_STRING_FEATURE_COUNT = NAME_PART_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Trivial String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIVIAL_STRING_OPERATION_COUNT = NAME_PART_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.OppositeToImpl <em>Opposite To</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.OppositeToImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getOppositeTo()
	 * @generated
	 */
	int OPPOSITE_TO = 115;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPPOSITE_TO__NEGATIVE = REFERENCE_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Ref Sel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPPOSITE_TO__REF_SEL = REFERENCE_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Opposite To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPPOSITE_TO_FEATURE_COUNT = REFERENCE_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Opposite To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPPOSITE_TO_OPERATION_COUNT = REFERENCE_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ParameterStringValueImpl <em>Parameter String Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ParameterStringValueImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getParameterStringValue()
	 * @generated
	 */
	int PARAMETER_STRING_VALUE = 116;

	/**
	 * The feature id for the '<em><b>Param Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_STRING_VALUE__PARAM_DEFINITION = PARAMETER_VALUE__PARAM_DEFINITION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_STRING_VALUE__VALUE = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_STRING_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Parameter String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_STRING_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.ParameterElementSetValueImpl <em>Parameter Element Set Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.ParameterElementSetValueImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getParameterElementSetValue()
	 * @generated
	 */
	int PARAMETER_ELEMENT_SET_VALUE = 117;

	/**
	 * The feature id for the '<em><b>Param Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ELEMENT_SET_VALUE__PARAM_DEFINITION = PARAMETER_VALUE__PARAM_DEFINITION;

	/**
	 * The feature id for the '<em><b>Selector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ELEMENT_SET_VALUE__SELECTOR = PARAMETER_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter Element Set Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ELEMENT_SET_VALUE_FEATURE_COUNT = PARAMETER_VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Parameter Element Set Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_ELEMENT_SET_VALUE_OPERATION_COUNT = PARAMETER_VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.NamedAfterImpl <em>Named After</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.NamedAfterImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNamedAfter()
	 * @generated
	 */
	int NAMED_AFTER = 118;

	/**
	 * The feature id for the '<em><b>Negative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_AFTER__NEGATIVE = ELEMENT_QUALIFIER__NEGATIVE;

	/**
	 * The feature id for the '<em><b>Element Sels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_AFTER__ELEMENT_SELS = ELEMENT_QUALIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Named After</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_AFTER_FEATURE_COUNT = ELEMENT_QUALIFIER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Named After</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_AFTER_OPERATION_COUNT = ELEMENT_QUALIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link metamodeltest.impl.OCLExpressionImpl <em>OCL Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.impl.OCLExpressionImpl
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getOCLExpression()
	 * @generated
	 */
	int OCL_EXPRESSION = 119;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_EXPRESSION__EXPRESSION = 0;

	/**
	 * The number of structural features of the '<em>OCL Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_EXPRESSION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>OCL Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OCL_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link metamodeltest.Case <em>Case</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.Case
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCase()
	 * @generated
	 */
	int CASE = 120;


	/**
	 * The meta object id for the '{@link metamodeltest.GrammaticalNumber <em>Grammatical Number</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see metamodeltest.GrammaticalNumber
	 * @see metamodeltest.impl.MetamodeltestPackageImpl#getGrammaticalNumber()
	 * @generated
	 */
	int GRAMMATICAL_NUMBER = 121;


	/**
	 * Returns the meta object for class '{@link metamodeltest.MetaModelTest <em>Meta Model Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Model Test</em>'.
	 * @see metamodeltest.MetaModelTest
	 * @generated
	 */
	EClass getMetaModelTest();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.MetaModelTest#getLibrary <em>Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Library</em>'.
	 * @see metamodeltest.MetaModelTest#getLibrary()
	 * @see #getMetaModelTest()
	 * @generated
	 */
	EReference getMetaModelTest_Library();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.MetaModelTest#getTests <em>Tests</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tests</em>'.
	 * @see metamodeltest.MetaModelTest#getTests()
	 * @see #getMetaModelTest()
	 * @generated
	 */
	EReference getMetaModelTest_Tests();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.MetaModelTest#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see metamodeltest.MetaModelTest#getImports()
	 * @see #getMetaModelTest()
	 * @generated
	 */
	EReference getMetaModelTest_Imports();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Assertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion</em>'.
	 * @see metamodeltest.Assertion
	 * @generated
	 */
	EClass getAssertion();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Assertion#getSelector <em>Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Selector</em>'.
	 * @see metamodeltest.Assertion#getSelector()
	 * @see #getAssertion()
	 * @generated
	 */
	EReference getAssertion_Selector();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Quantifier <em>Quantifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quantifier</em>'.
	 * @see metamodeltest.Quantifier
	 * @generated
	 */
	EClass getQuantifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.None <em>None</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>None</em>'.
	 * @see metamodeltest.None
	 * @generated
	 */
	EClass getNone();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Number <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number</em>'.
	 * @see metamodeltest.Number
	 * @generated
	 */
	EClass getNumber();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Number#getN <em>N</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>N</em>'.
	 * @see metamodeltest.Number#getN()
	 * @see #getNumber()
	 * @generated
	 */
	EReference getNumber_N();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Every <em>Every</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Every</em>'.
	 * @see metamodeltest.Every
	 * @generated
	 */
	EClass getEvery();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.Every#isNot <em>Not</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Not</em>'.
	 * @see metamodeltest.Every#isNot()
	 * @see #getEvery()
	 * @generated
	 */
	EAttribute getEvery_Not();

	/**
	 * Returns the meta object for class '{@link metamodeltest.LessThan <em>Less Than</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Less Than</em>'.
	 * @see metamodeltest.LessThan
	 * @generated
	 */
	EClass getLessThan();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.LessThan#getAnd <em>And</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>And</em>'.
	 * @see metamodeltest.LessThan#getAnd()
	 * @see #getLessThan()
	 * @generated
	 */
	EReference getLessThan_And();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.LessThan#isOrEqual <em>Or Equal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Or Equal</em>'.
	 * @see metamodeltest.LessThan#isOrEqual()
	 * @see #getLessThan()
	 * @generated
	 */
	EAttribute getLessThan_OrEqual();

	/**
	 * Returns the meta object for class '{@link metamodeltest.MoreThan <em>More Than</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>More Than</em>'.
	 * @see metamodeltest.MoreThan
	 * @generated
	 */
	EClass getMoreThan();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.MoreThan#getAnd <em>And</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>And</em>'.
	 * @see metamodeltest.MoreThan#getAnd()
	 * @see #getMoreThan()
	 * @generated
	 */
	EReference getMoreThan_And();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.MoreThan#isOrEqual <em>Or Equal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Or Equal</em>'.
	 * @see metamodeltest.MoreThan#isOrEqual()
	 * @see #getMoreThan()
	 * @generated
	 */
	EAttribute getMoreThan_OrEqual();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ElementAssertion <em>Element Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Assertion</em>'.
	 * @see metamodeltest.ElementAssertion
	 * @generated
	 */
	EClass getElementAssertion();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Name <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name</em>'.
	 * @see metamodeltest.Name
	 * @generated
	 */
	EClass getName_();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.Name#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodeltest.Name#getName()
	 * @see #getName_()
	 * @generated
	 */
	EAttribute getName_Name();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Abstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract</em>'.
	 * @see metamodeltest.Abstract
	 * @generated
	 */
	EClass getAbstract();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Filter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Filter</em>'.
	 * @see metamodeltest.Filter
	 * @generated
	 */
	EClass getFilter();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Filter#getQualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Qualifier</em>'.
	 * @see metamodeltest.Filter#getQualifier()
	 * @see #getFilter()
	 * @generated
	 */
	EReference getFilter_Qualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ElementFilter <em>Element Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Filter</em>'.
	 * @see metamodeltest.ElementFilter
	 * @generated
	 */
	EClass getElementFilter();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ClassFilter <em>Class Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Filter</em>'.
	 * @see metamodeltest.ClassFilter
	 * @generated
	 */
	EClass getClassFilter();

	/**
	 * Returns the meta object for class '{@link metamodeltest.FeatureFilter <em>Feature Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Filter</em>'.
	 * @see metamodeltest.FeatureFilter
	 * @generated
	 */
	EClass getFeatureFilter();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AttributeFilter <em>Attribute Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Filter</em>'.
	 * @see metamodeltest.AttributeFilter
	 * @generated
	 */
	EClass getAttributeFilter();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ReferenceFilter <em>Reference Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Filter</em>'.
	 * @see metamodeltest.ReferenceFilter
	 * @generated
	 */
	EClass getReferenceFilter();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ElementQualifier <em>Element Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Qualifier</em>'.
	 * @see metamodeltest.ElementQualifier
	 * @generated
	 */
	EClass getElementQualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Qualifier <em>Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Qualifier</em>'.
	 * @see metamodeltest.Qualifier
	 * @generated
	 */
	EClass getQualifier();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.Qualifier#isNegative <em>Negative</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Negative</em>'.
	 * @see metamodeltest.Qualifier#isNegative()
	 * @see #getQualifier()
	 * @generated
	 */
	EAttribute getQualifier_Negative();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Named <em>Named</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named</em>'.
	 * @see metamodeltest.Named
	 * @generated
	 */
	EClass getNamed();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.Named#getCase <em>Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Case</em>'.
	 * @see metamodeltest.Named#getCase()
	 * @see #getNamed()
	 * @generated
	 */
	EAttribute getNamed_Case();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Named#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name</em>'.
	 * @see metamodeltest.Named#getName()
	 * @see #getNamed()
	 * @generated
	 */
	EReference getNamed_Name();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Named#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Size</em>'.
	 * @see metamodeltest.Named#getSize()
	 * @see #getNamed()
	 * @generated
	 */
	EReference getNamed_Size();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Named#getWordNature <em>Word Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Word Nature</em>'.
	 * @see metamodeltest.Named#getWordNature()
	 * @see #getNamed()
	 * @generated
	 */
	EReference getNamed_WordNature();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ClassQualifier <em>Class Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Qualifier</em>'.
	 * @see metamodeltest.ClassQualifier
	 * @generated
	 */
	EClass getClassQualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.FeatureQualifier <em>Feature Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Qualifier</em>'.
	 * @see metamodeltest.FeatureQualifier
	 * @generated
	 */
	EClass getFeatureQualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AttributeQualifier <em>Attribute Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Qualifier</em>'.
	 * @see metamodeltest.AttributeQualifier
	 * @generated
	 */
	EClass getAttributeQualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ReferenceQualifier <em>Reference Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Qualifier</em>'.
	 * @see metamodeltest.ReferenceQualifier
	 * @generated
	 */
	EClass getReferenceQualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Abstraction <em>Abstraction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstraction</em>'.
	 * @see metamodeltest.Abstraction
	 * @generated
	 */
	EClass getAbstraction();

	/**
	 * Returns the meta object for class '{@link metamodeltest.IsSuperTo <em>Is Super To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Super To</em>'.
	 * @see metamodeltest.IsSuperTo
	 * @generated
	 */
	EClass getIsSuperTo();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.IsSuperTo#getClassSels <em>Class Sels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Sels</em>'.
	 * @see metamodeltest.IsSuperTo#getClassSels()
	 * @see #getIsSuperTo()
	 * @generated
	 */
	EReference getIsSuperTo_ClassSels();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.IsSuperTo#getOrEqual <em>Or Equal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Or Equal</em>'.
	 * @see metamodeltest.IsSuperTo#getOrEqual()
	 * @see #getIsSuperTo()
	 * @generated
	 */
	EReference getIsSuperTo_OrEqual();

	/**
	 * Returns the meta object for class '{@link metamodeltest.IsSubTo <em>Is Sub To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Is Sub To</em>'.
	 * @see metamodeltest.IsSubTo
	 * @generated
	 */
	EClass getIsSubTo();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.IsSubTo#getClassSels <em>Class Sels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Sels</em>'.
	 * @see metamodeltest.IsSubTo#getClassSels()
	 * @see #getIsSubTo()
	 * @generated
	 */
	EReference getIsSubTo_ClassSels();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.IsSubTo#getOrEqual <em>Or Equal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Or Equal</em>'.
	 * @see metamodeltest.IsSubTo#getOrEqual()
	 * @see #getIsSubTo()
	 * @generated
	 */
	EReference getIsSubTo_OrEqual();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ClassRelation <em>Class Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Relation</em>'.
	 * @see metamodeltest.ClassRelation
	 * @generated
	 */
	EClass getClassRelation();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.ClassRelation#getClassSels <em>Class Sels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class Sels</em>'.
	 * @see metamodeltest.ClassRelation#getClassSels()
	 * @see #getClassRelation()
	 * @generated
	 */
	EReference getClassRelation_ClassSels();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ClassRelation#getJumps <em>Jumps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Jumps</em>'.
	 * @see metamodeltest.ClassRelation#getJumps()
	 * @see #getClassRelation()
	 * @generated
	 */
	EReference getClassRelation_Jumps();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ClassRelation#getByInheritance <em>By Inheritance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>By Inheritance</em>'.
	 * @see metamodeltest.ClassRelation#getByInheritance()
	 * @see #getClassRelation()
	 * @generated
	 */
	EReference getClassRelation_ByInheritance();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ClassRelation#getContainment <em>Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Containment</em>'.
	 * @see metamodeltest.ClassRelation#getContainment()
	 * @see #getClassRelation()
	 * @generated
	 */
	EReference getClassRelation_Containment();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ClassRelation#getStrict <em>Strict</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Strict</em>'.
	 * @see metamodeltest.ClassRelation#getStrict()
	 * @see #getClassRelation()
	 * @generated
	 */
	EReference getClassRelation_Strict();

	/**
	 * Returns the meta object for class '{@link metamodeltest.FeatureContainment <em>Feature Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Containment</em>'.
	 * @see metamodeltest.FeatureContainment
	 * @generated
	 */
	EClass getFeatureContainment();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.FeatureContainment#getFeatSels <em>Feat Sels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Feat Sels</em>'.
	 * @see metamodeltest.FeatureContainment#getFeatSels()
	 * @see #getFeatureContainment()
	 * @generated
	 */
	EReference getFeatureContainment_FeatSels();

	/**
	 * Returns the meta object for class '{@link metamodeltest.InheritedFeatureContainment <em>Inherited Feature Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inherited Feature Containment</em>'.
	 * @see metamodeltest.InheritedFeatureContainment
	 * @generated
	 */
	EClass getInheritedFeatureContainment();

	/**
	 * Returns the meta object for class '{@link metamodeltest.InhLeaf <em>Inh Leaf</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inh Leaf</em>'.
	 * @see metamodeltest.InhLeaf
	 * @generated
	 */
	EClass getInhLeaf();

	/**
	 * Returns the meta object for class '{@link metamodeltest.InhRoot <em>Inh Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inh Root</em>'.
	 * @see metamodeltest.InhRoot
	 * @generated
	 */
	EClass getInhRoot();

	/**
	 * Returns the meta object for class '{@link metamodeltest.CompositionLeaf <em>Composition Leaf</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composition Leaf</em>'.
	 * @see metamodeltest.CompositionLeaf
	 * @generated
	 */
	EClass getCompositionLeaf();

	/**
	 * Returns the meta object for class '{@link metamodeltest.MaxMultiplicity <em>Max Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Max Multiplicity</em>'.
	 * @see metamodeltest.MaxMultiplicity
	 * @generated
	 */
	EClass getMaxMultiplicity();

	/**
	 * Returns the meta object for class '{@link metamodeltest.FixedBound <em>Fixed Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fixed Bound</em>'.
	 * @see metamodeltest.FixedBound
	 * @generated
	 */
	EClass getFixedBound();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Infinite <em>Infinite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Infinite</em>'.
	 * @see metamodeltest.Infinite
	 * @generated
	 */
	EClass getInfinite();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ClassContainment <em>Class Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Containment</em>'.
	 * @see metamodeltest.ClassContainment
	 * @generated
	 */
	EClass getClassContainment();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ClassContainee <em>Class Containee</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Containee</em>'.
	 * @see metamodeltest.ClassContainee
	 * @generated
	 */
	EClass getClassContainee();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ClassContainee#getClassSel <em>Class Sel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Sel</em>'.
	 * @see metamodeltest.ClassContainee#getClassSel()
	 * @see #getClassContainee()
	 * @generated
	 */
	EReference getClassContainee_ClassSel();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ClassContainee#getByInheritance <em>By Inheritance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>By Inheritance</em>'.
	 * @see metamodeltest.ClassContainee#getByInheritance()
	 * @see #getClassContainee()
	 * @generated
	 */
	EReference getClassContainee_ByInheritance();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Typed <em>Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Typed</em>'.
	 * @see metamodeltest.Typed
	 * @generated
	 */
	EClass getTyped();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Typed#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see metamodeltest.Typed#getType()
	 * @see #getTyped()
	 * @generated
	 */
	EReference getTyped_Type();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Prefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Prefix</em>'.
	 * @see metamodeltest.Prefix
	 * @generated
	 */
	EClass getPrefix();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Suffix <em>Suffix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Suffix</em>'.
	 * @see metamodeltest.Suffix
	 * @generated
	 */
	EClass getSuffix();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Contains <em>Contains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contains</em>'.
	 * @see metamodeltest.Contains
	 * @generated
	 */
	EClass getContains();

	/**
	 * Returns the meta object for class '{@link metamodeltest.SideClass <em>Side Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Side Class</em>'.
	 * @see metamodeltest.SideClass
	 * @generated
	 */
	EClass getSideClass();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.SideClass#getClassSel <em>Class Sel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Sel</em>'.
	 * @see metamodeltest.SideClass#getClassSel()
	 * @see #getSideClass()
	 * @generated
	 */
	EReference getSideClass_ClassSel();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.SideClass#getByInheritance <em>By Inheritance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>By Inheritance</em>'.
	 * @see metamodeltest.SideClass#getByInheritance()
	 * @see #getSideClass()
	 * @generated
	 */
	EReference getSideClass_ByInheritance();

	/**
	 * Returns the meta object for class '{@link metamodeltest.FromTo <em>From To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>From To</em>'.
	 * @see metamodeltest.FromTo
	 * @generated
	 */
	EClass getFromTo();

	/**
	 * Returns the meta object for class '{@link metamodeltest.StrictNumber <em>Strict Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Strict Number</em>'.
	 * @see metamodeltest.StrictNumber
	 * @generated
	 */
	EClass getStrictNumber();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Inheritance <em>Inheritance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inheritance</em>'.
	 * @see metamodeltest.Inheritance
	 * @generated
	 */
	EClass getInheritance();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Inheritance#getLength <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Length</em>'.
	 * @see metamodeltest.Inheritance#getLength()
	 * @see #getInheritance()
	 * @generated
	 */
	EReference getInheritance_Length();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Inheritance#getPaths <em>Paths</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Paths</em>'.
	 * @see metamodeltest.Inheritance#getPaths()
	 * @see #getInheritance()
	 * @generated
	 */
	EReference getInheritance_Paths();

	/**
	 * Returns the meta object for class '{@link metamodeltest.FeatureMultiplicity <em>Feature Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Multiplicity</em>'.
	 * @see metamodeltest.FeatureMultiplicity
	 * @generated
	 */
	EClass getFeatureMultiplicity();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.FeatureMultiplicity#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Min</em>'.
	 * @see metamodeltest.FeatureMultiplicity#getMin()
	 * @see #getFeatureMultiplicity()
	 * @generated
	 */
	EReference getFeatureMultiplicity_Min();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.FeatureMultiplicity#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Max</em>'.
	 * @see metamodeltest.FeatureMultiplicity#getMax()
	 * @see #getFeatureMultiplicity()
	 * @generated
	 */
	EReference getFeatureMultiplicity_Max();

	/**
	 * Returns the meta object for class '{@link metamodeltest.UpperCase <em>Upper Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Upper Case</em>'.
	 * @see metamodeltest.UpperCase
	 * @generated
	 */
	EClass getUpperCase();

	/**
	 * Returns the meta object for class '{@link metamodeltest.LowerCase <em>Lower Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lower Case</em>'.
	 * @see metamodeltest.LowerCase
	 * @generated
	 */
	EClass getLowerCase();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Substring <em>Substring</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Substring</em>'.
	 * @see metamodeltest.Substring
	 * @generated
	 */
	EClass getSubstring();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AssertionLibrary <em>Assertion Library</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion Library</em>'.
	 * @see metamodeltest.AssertionLibrary
	 * @generated
	 */
	EClass getAssertionLibrary();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.AssertionLibrary#getDefinitions <em>Definitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Definitions</em>'.
	 * @see metamodeltest.AssertionLibrary#getDefinitions()
	 * @see #getAssertionLibrary()
	 * @generated
	 */
	EReference getAssertionLibrary_Definitions();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AssertionTest <em>Assertion Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion Test</em>'.
	 * @see metamodeltest.AssertionTest
	 * @generated
	 */
	EClass getAssertionTest();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.AssertionTest#getMetamodelURI <em>Metamodel URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metamodel URI</em>'.
	 * @see metamodeltest.AssertionTest#getMetamodelURI()
	 * @see #getAssertionTest()
	 * @generated
	 */
	EAttribute getAssertionTest_MetamodelURI();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.AssertionTest#getAssertions <em>Assertions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Assertions</em>'.
	 * @see metamodeltest.AssertionTest#getAssertions()
	 * @see #getAssertionTest()
	 * @generated
	 */
	EReference getAssertionTest_Assertions();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.AssertionTest#getDirURI <em>Dir URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dir URI</em>'.
	 * @see metamodeltest.AssertionTest#getDirURI()
	 * @see #getAssertionTest()
	 * @generated
	 */
	EAttribute getAssertionTest_DirURI();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.AssertionTest#getCorrespondingEcoreURI <em>Corresponding Ecore URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Corresponding Ecore URI</em>'.
	 * @see metamodeltest.AssertionTest#getCorrespondingEcoreURI()
	 * @see #getAssertionTest()
	 * @generated
	 */
	EAttribute getAssertionTest_CorrespondingEcoreURI();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AssertionDefinition <em>Assertion Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion Definition</em>'.
	 * @see metamodeltest.AssertionDefinition
	 * @generated
	 */
	EClass getAssertionDefinition();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.AssertionDefinition#getAssertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Assertion</em>'.
	 * @see metamodeltest.AssertionDefinition#getAssertion()
	 * @see #getAssertionDefinition()
	 * @generated
	 */
	EReference getAssertionDefinition_Assertion();

	/**
	 * Returns the meta object for class '{@link metamodeltest.LibraryAssertionCall <em>Library Assertion Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Library Assertion Call</em>'.
	 * @see metamodeltest.LibraryAssertionCall
	 * @generated
	 */
	EClass getLibraryAssertionCall();

	/**
	 * Returns the meta object for the reference '{@link metamodeltest.LibraryAssertionCall#getReferencedAssertion <em>Referenced Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Assertion</em>'.
	 * @see metamodeltest.LibraryAssertionCall#getReferencedAssertion()
	 * @see #getLibraryAssertionCall()
	 * @generated
	 */
	EReference getLibraryAssertionCall_ReferencedAssertion();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.LibraryAssertionCall#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see metamodeltest.LibraryAssertionCall#getParameters()
	 * @see #getLibraryAssertionCall()
	 * @generated
	 */
	EReference getLibraryAssertionCall_Parameters();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AssertionCall <em>Assertion Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assertion Call</em>'.
	 * @see metamodeltest.AssertionCall
	 * @generated
	 */
	EClass getAssertionCall();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.AssertionCall#getOCLcomparison <em>OC Lcomparison</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>OC Lcomparison</em>'.
	 * @see metamodeltest.AssertionCall#getOCLcomparison()
	 * @see #getAssertionCall()
	 * @generated
	 */
	EReference getAssertionCall_OCLcomparison();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see metamodeltest.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Parameter#getParameterDefinition <em>Parameter Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameter Definition</em>'.
	 * @see metamodeltest.Parameter#getParameterDefinition()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_ParameterDefinition();

	/**
	 * Returns the meta object for the reference '{@link metamodeltest.Parameter#getParameterReuse <em>Parameter Reuse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter Reuse</em>'.
	 * @see metamodeltest.Parameter#getParameterReuse()
	 * @see #getParameter()
	 * @generated
	 */
	EReference getParameter_ParameterReuse();

	/**
	 * Returns the meta object for class '{@link metamodeltest.BooleanParameter <em>Boolean Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Parameter</em>'.
	 * @see metamodeltest.BooleanParameter
	 * @generated
	 */
	EClass getBooleanParameter();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.BooleanParameter#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see metamodeltest.BooleanParameter#isValue()
	 * @see #getBooleanParameter()
	 * @generated
	 */
	EAttribute getBooleanParameter_Value();

	/**
	 * Returns the meta object for class '{@link metamodeltest.IntegerParameter <em>Integer Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Integer Parameter</em>'.
	 * @see metamodeltest.IntegerParameter
	 * @generated
	 */
	EClass getIntegerParameter();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.IntegerParameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see metamodeltest.IntegerParameter#getValue()
	 * @see #getIntegerParameter()
	 * @generated
	 */
	EAttribute getIntegerParameter_Value();

	/**
	 * Returns the meta object for class '{@link metamodeltest.StringParameter <em>String Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Parameter</em>'.
	 * @see metamodeltest.StringParameter
	 * @generated
	 */
	EClass getStringParameter();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.StringParameter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see metamodeltest.StringParameter#getValue()
	 * @see #getStringParameter()
	 * @generated
	 */
	EAttribute getStringParameter_Value();

	/**
	 * Returns the meta object for class '{@link metamodeltest.UserDefinedParameter <em>User Defined Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Defined Parameter</em>'.
	 * @see metamodeltest.UserDefinedParameter
	 * @generated
	 */
	EClass getUserDefinedParameter();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.UserDefinedParameter#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodeltest.UserDefinedParameter#getName()
	 * @see #getUserDefinedParameter()
	 * @generated
	 */
	EAttribute getUserDefinedParameter_Name();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.UserDefinedParameter#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see metamodeltest.UserDefinedParameter#getDescription()
	 * @see #getUserDefinedParameter()
	 * @generated
	 */
	EAttribute getUserDefinedParameter_Description();

	/**
	 * Returns the meta object for class '{@link metamodeltest.PathFilter <em>Path Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Filter</em>'.
	 * @see metamodeltest.PathFilter
	 * @generated
	 */
	EClass getPathFilter();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.PathFilter#getPathQualifier <em>Path Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Path Qualifier</em>'.
	 * @see metamodeltest.PathFilter#getPathQualifier()
	 * @see #getPathFilter()
	 * @generated
	 */
	EReference getPathFilter_PathQualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Length <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Length</em>'.
	 * @see metamodeltest.Length
	 * @generated
	 */
	EClass getLength();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Length#getMin <em>Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Min</em>'.
	 * @see metamodeltest.Length#getMin()
	 * @see #getLength()
	 * @generated
	 */
	EReference getLength_Min();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Length#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Max</em>'.
	 * @see metamodeltest.Length#getMax()
	 * @see #getLength()
	 * @generated
	 */
	EReference getLength_Max();

	/**
	 * Returns the meta object for class '{@link metamodeltest.SourceClass <em>Source Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source Class</em>'.
	 * @see metamodeltest.SourceClass
	 * @generated
	 */
	EClass getSourceClass();

	/**
	 * Returns the meta object for class '{@link metamodeltest.TargetClass <em>Target Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Target Class</em>'.
	 * @see metamodeltest.TargetClass
	 * @generated
	 */
	EClass getTargetClass();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ContainmentReference <em>Containment Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Containment Reference</em>'.
	 * @see metamodeltest.ContainmentReference
	 * @generated
	 */
	EClass getContainmentReference();

	/**
	 * Returns the meta object for class '{@link metamodeltest.PathQualifier <em>Path Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Qualifier</em>'.
	 * @see metamodeltest.PathQualifier
	 * @generated
	 */
	EClass getPathQualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.PathSide <em>Path Side</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Side</em>'.
	 * @see metamodeltest.PathSide
	 * @generated
	 */
	EClass getPathSide();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.PathSide#getClassSels <em>Class Sels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Sels</em>'.
	 * @see metamodeltest.PathSide#getClassSels()
	 * @see #getPathSide()
	 * @generated
	 */
	EReference getPathSide_ClassSels();

	/**
	 * Returns the meta object for class '{@link metamodeltest.PathTarget <em>Path Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Target</em>'.
	 * @see metamodeltest.PathTarget
	 * @generated
	 */
	EClass getPathTarget();

	/**
	 * Returns the meta object for class '{@link metamodeltest.PathSource <em>Path Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Source</em>'.
	 * @see metamodeltest.PathSource
	 * @generated
	 */
	EClass getPathSource();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ElementFeaturement <em>Element Featurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Featurement</em>'.
	 * @see metamodeltest.ElementFeaturement
	 * @generated
	 */
	EClass getElementFeaturement();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.ElementFeaturement#getElementSels <em>Element Sels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Element Sels</em>'.
	 * @see metamodeltest.ElementFeaturement#getElementSels()
	 * @see #getElementFeaturement()
	 * @generated
	 */
	EReference getElementFeaturement_ElementSels();

	/**
	 * Returns the meta object for class '{@link metamodeltest.StepClass <em>Step Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Step Class</em>'.
	 * @see metamodeltest.StepClass
	 * @generated
	 */
	EClass getStepClass();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Existance <em>Existance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Existance</em>'.
	 * @see metamodeltest.Existance
	 * @generated
	 */
	EClass getExistance();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Selector <em>Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Selector</em>'.
	 * @see metamodeltest.Selector
	 * @generated
	 */
	EClass getSelector();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Selector#getQuantifier <em>Quantifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Quantifier</em>'.
	 * @see metamodeltest.Selector#getQuantifier()
	 * @see #getSelector()
	 * @generated
	 */
	EReference getSelector_Quantifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ElementSelector <em>Element Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Selector</em>'.
	 * @see metamodeltest.ElementSelector
	 * @generated
	 */
	EClass getElementSelector();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ElementSelector#getElementFilter <em>Element Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Element Filter</em>'.
	 * @see metamodeltest.ElementSelector#getElementFilter()
	 * @see #getElementSelector()
	 * @generated
	 */
	EReference getElementSelector_ElementFilter();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ElementSelector#getElementConditioner <em>Element Conditioner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Element Conditioner</em>'.
	 * @see metamodeltest.ElementSelector#getElementConditioner()
	 * @see #getElementSelector()
	 * @generated
	 */
	EReference getElementSelector_ElementConditioner();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ClassSelector <em>Class Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Selector</em>'.
	 * @see metamodeltest.ClassSelector
	 * @generated
	 */
	EClass getClassSelector();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ClassSelector#getClassConditioner <em>Class Conditioner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Conditioner</em>'.
	 * @see metamodeltest.ClassSelector#getClassConditioner()
	 * @see #getClassSelector()
	 * @generated
	 */
	EReference getClassSelector_ClassConditioner();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ClassSelector#getClassFilter <em>Class Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Filter</em>'.
	 * @see metamodeltest.ClassSelector#getClassFilter()
	 * @see #getClassSelector()
	 * @generated
	 */
	EReference getClassSelector_ClassFilter();

	/**
	 * Returns the meta object for class '{@link metamodeltest.FeatureSelector <em>Feature Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Selector</em>'.
	 * @see metamodeltest.FeatureSelector
	 * @generated
	 */
	EClass getFeatureSelector();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.FeatureSelector#getFeatureFilter <em>Feature Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Filter</em>'.
	 * @see metamodeltest.FeatureSelector#getFeatureFilter()
	 * @see #getFeatureSelector()
	 * @generated
	 */
	EReference getFeatureSelector_FeatureFilter();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.FeatureSelector#getFeatureConditioner <em>Feature Conditioner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Conditioner</em>'.
	 * @see metamodeltest.FeatureSelector#getFeatureConditioner()
	 * @see #getFeatureSelector()
	 * @generated
	 */
	EReference getFeatureSelector_FeatureConditioner();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AttributeSelector <em>Attribute Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Selector</em>'.
	 * @see metamodeltest.AttributeSelector
	 * @generated
	 */
	EClass getAttributeSelector();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.AttributeSelector#getAttributeFilter <em>Attribute Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Attribute Filter</em>'.
	 * @see metamodeltest.AttributeSelector#getAttributeFilter()
	 * @see #getAttributeSelector()
	 * @generated
	 */
	EReference getAttributeSelector_AttributeFilter();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.AttributeSelector#getAttributeConditioner <em>Attribute Conditioner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Attribute Conditioner</em>'.
	 * @see metamodeltest.AttributeSelector#getAttributeConditioner()
	 * @see #getAttributeSelector()
	 * @generated
	 */
	EReference getAttributeSelector_AttributeConditioner();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ReferenceSelector <em>Reference Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Selector</em>'.
	 * @see metamodeltest.ReferenceSelector
	 * @generated
	 */
	EClass getReferenceSelector();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ReferenceSelector#getReferenceFilter <em>Reference Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reference Filter</em>'.
	 * @see metamodeltest.ReferenceSelector#getReferenceFilter()
	 * @see #getReferenceSelector()
	 * @generated
	 */
	EReference getReferenceSelector_ReferenceFilter();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ReferenceSelector#getReferenceConditioner <em>Reference Conditioner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reference Conditioner</em>'.
	 * @see metamodeltest.ReferenceSelector#getReferenceConditioner()
	 * @see #getReferenceSelector()
	 * @generated
	 */
	EReference getReferenceSelector_ReferenceConditioner();

	/**
	 * Returns the meta object for class '{@link metamodeltest.PathSelector <em>Path Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Selector</em>'.
	 * @see metamodeltest.PathSelector
	 * @generated
	 */
	EClass getPathSelector();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.PathSelector#getPathFilter <em>Path Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Path Filter</em>'.
	 * @see metamodeltest.PathSelector#getPathFilter()
	 * @see #getPathSelector()
	 * @generated
	 */
	EReference getPathSelector_PathFilter();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.PathSelector#getPathConditioner <em>Path Conditioner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Path Conditioner</em>'.
	 * @see metamodeltest.PathSelector#getPathConditioner()
	 * @see #getPathSelector()
	 * @generated
	 */
	EReference getPathSelector_PathConditioner();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AndQualifier <em>And Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And Qualifier</em>'.
	 * @see metamodeltest.AndQualifier
	 * @generated
	 */
	EClass getAndQualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.OrQualifier <em>Or Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or Qualifier</em>'.
	 * @see metamodeltest.OrQualifier
	 * @generated
	 */
	EClass getOrQualifier();

	/**
	 * Returns the meta object for class '{@link metamodeltest.CompoundQualifier <em>Compound Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compound Qualifier</em>'.
	 * @see metamodeltest.CompoundQualifier
	 * @generated
	 */
	EClass getCompoundQualifier();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.CompoundQualifier#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see metamodeltest.CompoundQualifier#getChildren()
	 * @see #getCompoundQualifier()
	 * @generated
	 */
	EReference getCompoundQualifier_Children();

	/**
	 * Returns the meta object for class '{@link metamodeltest.CompoundPathQualifier <em>Compound Path Qualifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compound Path Qualifier</em>'.
	 * @see metamodeltest.CompoundPathQualifier
	 * @generated
	 */
	EClass getCompoundPathQualifier();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.CompoundPathQualifier#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see metamodeltest.CompoundPathQualifier#getChildren()
	 * @see #getCompoundPathQualifier()
	 * @generated
	 */
	EReference getCompoundPathQualifier_Children();

	/**
	 * Returns the meta object for class '{@link metamodeltest.PathAnd <em>Path And</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path And</em>'.
	 * @see metamodeltest.PathAnd
	 * @generated
	 */
	EClass getPathAnd();

	/**
	 * Returns the meta object for class '{@link metamodeltest.PathOr <em>Path Or</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path Or</em>'.
	 * @see metamodeltest.PathOr
	 * @generated
	 */
	EClass getPathOr();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AttributeContainment <em>Attribute Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Containment</em>'.
	 * @see metamodeltest.AttributeContainment
	 * @generated
	 */
	EClass getAttributeContainment();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.AttributeContainment#getAttSels <em>Att Sels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Att Sels</em>'.
	 * @see metamodeltest.AttributeContainment#getAttSels()
	 * @see #getAttributeContainment()
	 * @generated
	 */
	EReference getAttributeContainment_AttSels();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ReferenceContainment <em>Reference Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Containment</em>'.
	 * @see metamodeltest.ReferenceContainment
	 * @generated
	 */
	EClass getReferenceContainment();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.ReferenceContainment#getRefSels <em>Ref Sels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ref Sels</em>'.
	 * @see metamodeltest.ReferenceContainment#getRefSels()
	 * @see #getReferenceContainment()
	 * @generated
	 */
	EReference getReferenceContainment_RefSels();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ContRoot <em>Cont Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cont Root</em>'.
	 * @see metamodeltest.ContRoot
	 * @generated
	 */
	EClass getContRoot();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ContRoot#getAbsolute <em>Absolute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Absolute</em>'.
	 * @see metamodeltest.ContRoot#getAbsolute()
	 * @see #getContRoot()
	 * @generated
	 */
	EReference getContRoot_Absolute();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ContLeaf <em>Cont Leaf</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cont Leaf</em>'.
	 * @see metamodeltest.ContLeaf
	 * @generated
	 */
	EClass getContLeaf();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ElementRelation <em>Element Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element Relation</em>'.
	 * @see metamodeltest.ElementRelation
	 * @generated
	 */
	EClass getElementRelation();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ElementRelation#getByInheritance <em>By Inheritance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>By Inheritance</em>'.
	 * @see metamodeltest.ElementRelation#getByInheritance()
	 * @see #getElementRelation()
	 * @generated
	 */
	EReference getElementRelation_ByInheritance();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ClassReaches <em>Class Reaches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Reaches</em>'.
	 * @see metamodeltest.ClassReaches
	 * @generated
	 */
	EClass getClassReaches();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ClassReachedBy <em>Class Reached By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Reached By</em>'.
	 * @see metamodeltest.ClassReachedBy
	 * @generated
	 */
	EClass getClassReachedBy();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ContainmentPath <em>Containment Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Containment Path</em>'.
	 * @see metamodeltest.ContainmentPath
	 * @generated
	 */
	EClass getContainmentPath();

	/**
	 * Returns the meta object for class '{@link metamodeltest.WithInheritancePath <em>With Inheritance Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>With Inheritance Path</em>'.
	 * @see metamodeltest.WithInheritancePath
	 * @generated
	 */
	EClass getWithInheritancePath();

	/**
	 * Returns the meta object for class '{@link metamodeltest.CyclicPath <em>Cyclic Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cyclic Path</em>'.
	 * @see metamodeltest.CyclicPath
	 * @generated
	 */
	EClass getCyclicPath();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Some <em>Some</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Some</em>'.
	 * @see metamodeltest.Some
	 * @generated
	 */
	EClass getSome();

	/**
	 * Returns the meta object for class '{@link metamodeltest.InheritedFeature <em>Inherited Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Inherited Feature</em>'.
	 * @see metamodeltest.InheritedFeature
	 * @generated
	 */
	EClass getInheritedFeature();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.InheritedFeature#getClassSel <em>Class Sel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Class Sel</em>'.
	 * @see metamodeltest.InheritedFeature#getClassSel()
	 * @see #getInheritedFeature()
	 * @generated
	 */
	EReference getInheritedFeature_ClassSel();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ClassCollects <em>Class Collects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Collects</em>'.
	 * @see metamodeltest.ClassCollects
	 * @generated
	 */
	EClass getClassCollects();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ClassCollects#getCollectedMult <em>Collected Mult</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Collected Mult</em>'.
	 * @see metamodeltest.ClassCollects#getCollectedMult()
	 * @see #getClassCollects()
	 * @generated
	 */
	EReference getClassCollects_CollectedMult();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Annotated <em>Annotated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotated</em>'.
	 * @see metamodeltest.Annotated
	 * @generated
	 */
	EClass getAnnotated();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Annotated#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Name</em>'.
	 * @see metamodeltest.Annotated#getName()
	 * @see #getAnnotated()
	 * @generated
	 */
	EReference getAnnotated_Name();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see metamodeltest.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.Import#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see metamodeltest.Import#getImportURI()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_ImportURI();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ParameterValue <em>Parameter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Value</em>'.
	 * @see metamodeltest.ParameterValue
	 * @generated
	 */
	EClass getParameterValue();

	/**
	 * Returns the meta object for the reference '{@link metamodeltest.ParameterValue#getParamDefinition <em>Param Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Param Definition</em>'.
	 * @see metamodeltest.ParameterValue#getParamDefinition()
	 * @see #getParameterValue()
	 * @generated
	 */
	EReference getParameterValue_ParamDefinition();

	/**
	 * Returns the meta object for class '{@link metamodeltest.SelfInheritance <em>Self Inheritance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Self Inheritance</em>'.
	 * @see metamodeltest.SelfInheritance
	 * @generated
	 */
	EClass getSelfInheritance();

	/**
	 * Returns the meta object for class '{@link metamodeltest.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see metamodeltest.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodeltest.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see metamodeltest.Annotation
	 * @generated
	 */
	EClass getAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.Annotation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see metamodeltest.Annotation#getName()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Name();

	/**
	 * Returns the meta object for class '{@link metamodeltest.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotated Element</em>'.
	 * @see metamodeltest.AnnotatedElement
	 * @generated
	 */
	EClass getAnnotatedElement();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.AnnotatedElement#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotations</em>'.
	 * @see metamodeltest.AnnotatedElement#getAnnotations()
	 * @see #getAnnotatedElement()
	 * @generated
	 */
	EReference getAnnotatedElement_Annotations();

	/**
	 * Returns the meta object for class '{@link metamodeltest.DescribedElement <em>Described Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Described Element</em>'.
	 * @see metamodeltest.DescribedElement
	 * @generated
	 */
	EClass getDescribedElement();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.DescribedElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see metamodeltest.DescribedElement#getDescription()
	 * @see #getDescribedElement()
	 * @generated
	 */
	EAttribute getDescribedElement_Description();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Synonym <em>Synonym</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Synonym</em>'.
	 * @see metamodeltest.Synonym
	 * @generated
	 */
	EClass getSynonym();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Verb <em>Verb</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Verb</em>'.
	 * @see metamodeltest.Verb
	 * @generated
	 */
	EClass getVerb();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Noun <em>Noun</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Noun</em>'.
	 * @see metamodeltest.Noun
	 * @generated
	 */
	EClass getNoun();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.Noun#getNum <em>Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num</em>'.
	 * @see metamodeltest.Noun#getNum()
	 * @see #getNoun()
	 * @generated
	 */
	EAttribute getNoun_Num();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Adjective <em>Adjective</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Adjective</em>'.
	 * @see metamodeltest.Adjective
	 * @generated
	 */
	EClass getAdjective();

	/**
	 * Returns the meta object for class '{@link metamodeltest.WordNature <em>Word Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Word Nature</em>'.
	 * @see metamodeltest.WordNature
	 * @generated
	 */
	EClass getWordNature();

	/**
	 * Returns the meta object for class '{@link metamodeltest.PhraseNamed <em>Phrase Named</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Phrase Named</em>'.
	 * @see metamodeltest.PhraseNamed
	 * @generated
	 */
	EClass getPhraseNamed();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.PhraseNamed#isCamelized <em>Camelized</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Camelized</em>'.
	 * @see metamodeltest.PhraseNamed#isCamelized()
	 * @see #getPhraseNamed()
	 * @generated
	 */
	EAttribute getPhraseNamed_Camelized();

	/**
	 * Returns the meta object for the containment reference list '{@link metamodeltest.PhraseNamed#getWords <em>Words</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Words</em>'.
	 * @see metamodeltest.PhraseNamed#getWords()
	 * @see #getPhraseNamed()
	 * @generated
	 */
	EReference getPhraseNamed_Words();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.PhraseNamed#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Start</em>'.
	 * @see metamodeltest.PhraseNamed#getStart()
	 * @see #getPhraseNamed()
	 * @generated
	 */
	EReference getPhraseNamed_Start();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.PhraseNamed#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>End</em>'.
	 * @see metamodeltest.PhraseNamed#getEnd()
	 * @see #getPhraseNamed()
	 * @generated
	 */
	EReference getPhraseNamed_End();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.PhraseNamed#isPascal <em>Pascal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pascal</em>'.
	 * @see metamodeltest.PhraseNamed#isPascal()
	 * @see #getPhraseNamed()
	 * @generated
	 */
	EAttribute getPhraseNamed_Pascal();

	/**
	 * Returns the meta object for class '{@link metamodeltest.NamePart <em>Name Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Name Part</em>'.
	 * @see metamodeltest.NamePart
	 * @generated
	 */
	EClass getNamePart();

	/**
	 * Returns the meta object for class '{@link metamodeltest.Literal <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Literal</em>'.
	 * @see metamodeltest.Literal
	 * @generated
	 */
	EClass getLiteral();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.Literal#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see metamodeltest.Literal#getLiteral()
	 * @see #getLiteral()
	 * @generated
	 */
	EReference getLiteral_Literal();

	/**
	 * Returns the meta object for class '{@link metamodeltest.TrivialString <em>Trivial String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trivial String</em>'.
	 * @see metamodeltest.TrivialString
	 * @generated
	 */
	EClass getTrivialString();

	/**
	 * Returns the meta object for class '{@link metamodeltest.OppositeTo <em>Opposite To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Opposite To</em>'.
	 * @see metamodeltest.OppositeTo
	 * @generated
	 */
	EClass getOppositeTo();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.OppositeTo#getRefSel <em>Ref Sel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ref Sel</em>'.
	 * @see metamodeltest.OppositeTo#getRefSel()
	 * @see #getOppositeTo()
	 * @generated
	 */
	EReference getOppositeTo_RefSel();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ParameterStringValue <em>Parameter String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter String Value</em>'.
	 * @see metamodeltest.ParameterStringValue
	 * @generated
	 */
	EClass getParameterStringValue();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.ParameterStringValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see metamodeltest.ParameterStringValue#getValue()
	 * @see #getParameterStringValue()
	 * @generated
	 */
	EAttribute getParameterStringValue_Value();

	/**
	 * Returns the meta object for class '{@link metamodeltest.ParameterElementSetValue <em>Parameter Element Set Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Element Set Value</em>'.
	 * @see metamodeltest.ParameterElementSetValue
	 * @generated
	 */
	EClass getParameterElementSetValue();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.ParameterElementSetValue#getSelector <em>Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Selector</em>'.
	 * @see metamodeltest.ParameterElementSetValue#getSelector()
	 * @see #getParameterElementSetValue()
	 * @generated
	 */
	EReference getParameterElementSetValue_Selector();

	/**
	 * Returns the meta object for class '{@link metamodeltest.NamedAfter <em>Named After</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named After</em>'.
	 * @see metamodeltest.NamedAfter
	 * @generated
	 */
	EClass getNamedAfter();

	/**
	 * Returns the meta object for the containment reference '{@link metamodeltest.NamedAfter#getElementSels <em>Element Sels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Element Sels</em>'.
	 * @see metamodeltest.NamedAfter#getElementSels()
	 * @see #getNamedAfter()
	 * @generated
	 */
	EReference getNamedAfter_ElementSels();

	/**
	 * Returns the meta object for class '{@link metamodeltest.OCLExpression <em>OCL Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OCL Expression</em>'.
	 * @see metamodeltest.OCLExpression
	 * @generated
	 */
	EClass getOCLExpression();

	/**
	 * Returns the meta object for the attribute '{@link metamodeltest.OCLExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see metamodeltest.OCLExpression#getExpression()
	 * @see #getOCLExpression()
	 * @generated
	 */
	EAttribute getOCLExpression_Expression();

	/**
	 * Returns the meta object for enum '{@link metamodeltest.Case <em>Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Case</em>'.
	 * @see metamodeltest.Case
	 * @generated
	 */
	EEnum getCase();

	/**
	 * Returns the meta object for enum '{@link metamodeltest.GrammaticalNumber <em>Grammatical Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Grammatical Number</em>'.
	 * @see metamodeltest.GrammaticalNumber
	 * @generated
	 */
	EEnum getGrammaticalNumber();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MetamodeltestFactory getMetamodeltestFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link metamodeltest.impl.MetaModelTestImpl <em>Meta Model Test</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.MetaModelTestImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getMetaModelTest()
		 * @generated
		 */
		EClass META_MODEL_TEST = eINSTANCE.getMetaModelTest();

		/**
		 * The meta object literal for the '<em><b>Library</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL_TEST__LIBRARY = eINSTANCE.getMetaModelTest_Library();

		/**
		 * The meta object literal for the '<em><b>Tests</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL_TEST__TESTS = eINSTANCE.getMetaModelTest_Tests();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_MODEL_TEST__IMPORTS = eINSTANCE.getMetaModelTest_Imports();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AssertionImpl <em>Assertion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AssertionImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertion()
		 * @generated
		 */
		EClass ASSERTION = eINSTANCE.getAssertion();

		/**
		 * The meta object literal for the '<em><b>Selector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTION__SELECTOR = eINSTANCE.getAssertion_Selector();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.QuantifierImpl <em>Quantifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.QuantifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getQuantifier()
		 * @generated
		 */
		EClass QUANTIFIER = eINSTANCE.getQuantifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.NoneImpl <em>None</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.NoneImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNone()
		 * @generated
		 */
		EClass NONE = eINSTANCE.getNone();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.NumberImpl <em>Number</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.NumberImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNumber()
		 * @generated
		 */
		EClass NUMBER = eINSTANCE.getNumber();

		/**
		 * The meta object literal for the '<em><b>N</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NUMBER__N = eINSTANCE.getNumber_N();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.EveryImpl <em>Every</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.EveryImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getEvery()
		 * @generated
		 */
		EClass EVERY = eINSTANCE.getEvery();

		/**
		 * The meta object literal for the '<em><b>Not</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVERY__NOT = eINSTANCE.getEvery_Not();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.LessThanImpl <em>Less Than</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.LessThanImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLessThan()
		 * @generated
		 */
		EClass LESS_THAN = eINSTANCE.getLessThan();

		/**
		 * The meta object literal for the '<em><b>And</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LESS_THAN__AND = eINSTANCE.getLessThan_And();

		/**
		 * The meta object literal for the '<em><b>Or Equal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LESS_THAN__OR_EQUAL = eINSTANCE.getLessThan_OrEqual();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.MoreThanImpl <em>More Than</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.MoreThanImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getMoreThan()
		 * @generated
		 */
		EClass MORE_THAN = eINSTANCE.getMoreThan();

		/**
		 * The meta object literal for the '<em><b>And</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MORE_THAN__AND = eINSTANCE.getMoreThan_And();

		/**
		 * The meta object literal for the '<em><b>Or Equal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MORE_THAN__OR_EQUAL = eINSTANCE.getMoreThan_OrEqual();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ElementAssertionImpl <em>Element Assertion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ElementAssertionImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementAssertion()
		 * @generated
		 */
		EClass ELEMENT_ASSERTION = eINSTANCE.getElementAssertion();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.NameImpl <em>Name</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.NameImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getName_()
		 * @generated
		 */
		EClass NAME = eINSTANCE.getName_();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAME__NAME = eINSTANCE.getName_Name();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AbstractImpl <em>Abstract</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AbstractImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAbstract()
		 * @generated
		 */
		EClass ABSTRACT = eINSTANCE.getAbstract();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.FilterImpl <em>Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.FilterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFilter()
		 * @generated
		 */
		EClass FILTER = eINSTANCE.getFilter();

		/**
		 * The meta object literal for the '<em><b>Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILTER__QUALIFIER = eINSTANCE.getFilter_Qualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ElementFilterImpl <em>Element Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ElementFilterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementFilter()
		 * @generated
		 */
		EClass ELEMENT_FILTER = eINSTANCE.getElementFilter();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ClassFilterImpl <em>Class Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ClassFilterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassFilter()
		 * @generated
		 */
		EClass CLASS_FILTER = eINSTANCE.getClassFilter();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.FeatureFilterImpl <em>Feature Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.FeatureFilterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureFilter()
		 * @generated
		 */
		EClass FEATURE_FILTER = eINSTANCE.getFeatureFilter();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AttributeFilterImpl <em>Attribute Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AttributeFilterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAttributeFilter()
		 * @generated
		 */
		EClass ATTRIBUTE_FILTER = eINSTANCE.getAttributeFilter();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ReferenceFilterImpl <em>Reference Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ReferenceFilterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getReferenceFilter()
		 * @generated
		 */
		EClass REFERENCE_FILTER = eINSTANCE.getReferenceFilter();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ElementQualifierImpl <em>Element Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ElementQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementQualifier()
		 * @generated
		 */
		EClass ELEMENT_QUALIFIER = eINSTANCE.getElementQualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.QualifierImpl <em>Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.QualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getQualifier()
		 * @generated
		 */
		EClass QUALIFIER = eINSTANCE.getQualifier();

		/**
		 * The meta object literal for the '<em><b>Negative</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUALIFIER__NEGATIVE = eINSTANCE.getQualifier_Negative();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.NamedImpl <em>Named</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.NamedImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNamed()
		 * @generated
		 */
		EClass NAMED = eINSTANCE.getNamed();

		/**
		 * The meta object literal for the '<em><b>Case</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED__CASE = eINSTANCE.getNamed_Case();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMED__NAME = eINSTANCE.getNamed_Name();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMED__SIZE = eINSTANCE.getNamed_Size();

		/**
		 * The meta object literal for the '<em><b>Word Nature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMED__WORD_NATURE = eINSTANCE.getNamed_WordNature();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ClassQualifierImpl <em>Class Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ClassQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassQualifier()
		 * @generated
		 */
		EClass CLASS_QUALIFIER = eINSTANCE.getClassQualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.FeatureQualifierImpl <em>Feature Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.FeatureQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureQualifier()
		 * @generated
		 */
		EClass FEATURE_QUALIFIER = eINSTANCE.getFeatureQualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AttributeQualifierImpl <em>Attribute Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AttributeQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAttributeQualifier()
		 * @generated
		 */
		EClass ATTRIBUTE_QUALIFIER = eINSTANCE.getAttributeQualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ReferenceQualifierImpl <em>Reference Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ReferenceQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getReferenceQualifier()
		 * @generated
		 */
		EClass REFERENCE_QUALIFIER = eINSTANCE.getReferenceQualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AbstractionImpl <em>Abstraction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AbstractionImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAbstraction()
		 * @generated
		 */
		EClass ABSTRACTION = eINSTANCE.getAbstraction();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.IsSuperToImpl <em>Is Super To</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.IsSuperToImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getIsSuperTo()
		 * @generated
		 */
		EClass IS_SUPER_TO = eINSTANCE.getIsSuperTo();

		/**
		 * The meta object literal for the '<em><b>Class Sels</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IS_SUPER_TO__CLASS_SELS = eINSTANCE.getIsSuperTo_ClassSels();

		/**
		 * The meta object literal for the '<em><b>Or Equal</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IS_SUPER_TO__OR_EQUAL = eINSTANCE.getIsSuperTo_OrEqual();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.IsSubToImpl <em>Is Sub To</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.IsSubToImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getIsSubTo()
		 * @generated
		 */
		EClass IS_SUB_TO = eINSTANCE.getIsSubTo();

		/**
		 * The meta object literal for the '<em><b>Class Sels</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IS_SUB_TO__CLASS_SELS = eINSTANCE.getIsSubTo_ClassSels();

		/**
		 * The meta object literal for the '<em><b>Or Equal</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IS_SUB_TO__OR_EQUAL = eINSTANCE.getIsSubTo_OrEqual();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ClassRelationImpl <em>Class Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ClassRelationImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassRelation()
		 * @generated
		 */
		EClass CLASS_RELATION = eINSTANCE.getClassRelation();

		/**
		 * The meta object literal for the '<em><b>Class Sels</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_RELATION__CLASS_SELS = eINSTANCE.getClassRelation_ClassSels();

		/**
		 * The meta object literal for the '<em><b>Jumps</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_RELATION__JUMPS = eINSTANCE.getClassRelation_Jumps();

		/**
		 * The meta object literal for the '<em><b>By Inheritance</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_RELATION__BY_INHERITANCE = eINSTANCE.getClassRelation_ByInheritance();

		/**
		 * The meta object literal for the '<em><b>Containment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_RELATION__CONTAINMENT = eINSTANCE.getClassRelation_Containment();

		/**
		 * The meta object literal for the '<em><b>Strict</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_RELATION__STRICT = eINSTANCE.getClassRelation_Strict();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.FeatureContainmentImpl <em>Feature Containment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.FeatureContainmentImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureContainment()
		 * @generated
		 */
		EClass FEATURE_CONTAINMENT = eINSTANCE.getFeatureContainment();

		/**
		 * The meta object literal for the '<em><b>Feat Sels</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_CONTAINMENT__FEAT_SELS = eINSTANCE.getFeatureContainment_FeatSels();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.InheritedFeatureContainmentImpl <em>Inherited Feature Containment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.InheritedFeatureContainmentImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInheritedFeatureContainment()
		 * @generated
		 */
		EClass INHERITED_FEATURE_CONTAINMENT = eINSTANCE.getInheritedFeatureContainment();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.InhLeafImpl <em>Inh Leaf</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.InhLeafImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInhLeaf()
		 * @generated
		 */
		EClass INH_LEAF = eINSTANCE.getInhLeaf();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.InhRootImpl <em>Inh Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.InhRootImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInhRoot()
		 * @generated
		 */
		EClass INH_ROOT = eINSTANCE.getInhRoot();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.CompositionLeafImpl <em>Composition Leaf</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.CompositionLeafImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCompositionLeaf()
		 * @generated
		 */
		EClass COMPOSITION_LEAF = eINSTANCE.getCompositionLeaf();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.MaxMultiplicityImpl <em>Max Multiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.MaxMultiplicityImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getMaxMultiplicity()
		 * @generated
		 */
		EClass MAX_MULTIPLICITY = eINSTANCE.getMaxMultiplicity();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.FixedBoundImpl <em>Fixed Bound</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.FixedBoundImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFixedBound()
		 * @generated
		 */
		EClass FIXED_BOUND = eINSTANCE.getFixedBound();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.InfiniteImpl <em>Infinite</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.InfiniteImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInfinite()
		 * @generated
		 */
		EClass INFINITE = eINSTANCE.getInfinite();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ClassContainmentImpl <em>Class Containment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ClassContainmentImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassContainment()
		 * @generated
		 */
		EClass CLASS_CONTAINMENT = eINSTANCE.getClassContainment();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ClassContaineeImpl <em>Class Containee</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ClassContaineeImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassContainee()
		 * @generated
		 */
		EClass CLASS_CONTAINEE = eINSTANCE.getClassContainee();

		/**
		 * The meta object literal for the '<em><b>Class Sel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_CONTAINEE__CLASS_SEL = eINSTANCE.getClassContainee_ClassSel();

		/**
		 * The meta object literal for the '<em><b>By Inheritance</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_CONTAINEE__BY_INHERITANCE = eINSTANCE.getClassContainee_ByInheritance();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.TypedImpl <em>Typed</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.TypedImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getTyped()
		 * @generated
		 */
		EClass TYPED = eINSTANCE.getTyped();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPED__TYPE = eINSTANCE.getTyped_Type();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PrefixImpl <em>Prefix</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PrefixImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPrefix()
		 * @generated
		 */
		EClass PREFIX = eINSTANCE.getPrefix();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.SuffixImpl <em>Suffix</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.SuffixImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSuffix()
		 * @generated
		 */
		EClass SUFFIX = eINSTANCE.getSuffix();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ContainsImpl <em>Contains</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ContainsImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContains()
		 * @generated
		 */
		EClass CONTAINS = eINSTANCE.getContains();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.SideClassImpl <em>Side Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.SideClassImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSideClass()
		 * @generated
		 */
		EClass SIDE_CLASS = eINSTANCE.getSideClass();

		/**
		 * The meta object literal for the '<em><b>Class Sel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIDE_CLASS__CLASS_SEL = eINSTANCE.getSideClass_ClassSel();

		/**
		 * The meta object literal for the '<em><b>By Inheritance</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIDE_CLASS__BY_INHERITANCE = eINSTANCE.getSideClass_ByInheritance();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.FromToImpl <em>From To</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.FromToImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFromTo()
		 * @generated
		 */
		EClass FROM_TO = eINSTANCE.getFromTo();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.StrictNumberImpl <em>Strict Number</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.StrictNumberImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getStrictNumber()
		 * @generated
		 */
		EClass STRICT_NUMBER = eINSTANCE.getStrictNumber();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.InheritanceImpl <em>Inheritance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.InheritanceImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInheritance()
		 * @generated
		 */
		EClass INHERITANCE = eINSTANCE.getInheritance();

		/**
		 * The meta object literal for the '<em><b>Length</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INHERITANCE__LENGTH = eINSTANCE.getInheritance_Length();

		/**
		 * The meta object literal for the '<em><b>Paths</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INHERITANCE__PATHS = eINSTANCE.getInheritance_Paths();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.FeatureMultiplicityImpl <em>Feature Multiplicity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.FeatureMultiplicityImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureMultiplicity()
		 * @generated
		 */
		EClass FEATURE_MULTIPLICITY = eINSTANCE.getFeatureMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MULTIPLICITY__MIN = eINSTANCE.getFeatureMultiplicity_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_MULTIPLICITY__MAX = eINSTANCE.getFeatureMultiplicity_Max();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.UpperCaseImpl <em>Upper Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.UpperCaseImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getUpperCase()
		 * @generated
		 */
		EClass UPPER_CASE = eINSTANCE.getUpperCase();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.LowerCaseImpl <em>Lower Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.LowerCaseImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLowerCase()
		 * @generated
		 */
		EClass LOWER_CASE = eINSTANCE.getLowerCase();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.SubstringImpl <em>Substring</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.SubstringImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSubstring()
		 * @generated
		 */
		EClass SUBSTRING = eINSTANCE.getSubstring();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AssertionLibraryImpl <em>Assertion Library</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AssertionLibraryImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertionLibrary()
		 * @generated
		 */
		EClass ASSERTION_LIBRARY = eINSTANCE.getAssertionLibrary();

		/**
		 * The meta object literal for the '<em><b>Definitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTION_LIBRARY__DEFINITIONS = eINSTANCE.getAssertionLibrary_Definitions();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AssertionTestImpl <em>Assertion Test</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AssertionTestImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertionTest()
		 * @generated
		 */
		EClass ASSERTION_TEST = eINSTANCE.getAssertionTest();

		/**
		 * The meta object literal for the '<em><b>Metamodel URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTION_TEST__METAMODEL_URI = eINSTANCE.getAssertionTest_MetamodelURI();

		/**
		 * The meta object literal for the '<em><b>Assertions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTION_TEST__ASSERTIONS = eINSTANCE.getAssertionTest_Assertions();

		/**
		 * The meta object literal for the '<em><b>Dir URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTION_TEST__DIR_URI = eINSTANCE.getAssertionTest_DirURI();

		/**
		 * The meta object literal for the '<em><b>Corresponding Ecore URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSERTION_TEST__CORRESPONDING_ECORE_URI = eINSTANCE.getAssertionTest_CorrespondingEcoreURI();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AssertionDefinitionImpl <em>Assertion Definition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AssertionDefinitionImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertionDefinition()
		 * @generated
		 */
		EClass ASSERTION_DEFINITION = eINSTANCE.getAssertionDefinition();

		/**
		 * The meta object literal for the '<em><b>Assertion</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTION_DEFINITION__ASSERTION = eINSTANCE.getAssertionDefinition_Assertion();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.LibraryAssertionCallImpl <em>Library Assertion Call</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.LibraryAssertionCallImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLibraryAssertionCall()
		 * @generated
		 */
		EClass LIBRARY_ASSERTION_CALL = eINSTANCE.getLibraryAssertionCall();

		/**
		 * The meta object literal for the '<em><b>Referenced Assertion</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIBRARY_ASSERTION_CALL__REFERENCED_ASSERTION = eINSTANCE.getLibraryAssertionCall_ReferencedAssertion();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIBRARY_ASSERTION_CALL__PARAMETERS = eINSTANCE.getLibraryAssertionCall_Parameters();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AssertionCallImpl <em>Assertion Call</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AssertionCallImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAssertionCall()
		 * @generated
		 */
		EClass ASSERTION_CALL = eINSTANCE.getAssertionCall();

		/**
		 * The meta object literal for the '<em><b>OC Lcomparison</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSERTION_CALL__OC_LCOMPARISON = eINSTANCE.getAssertionCall_OCLcomparison();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ParameterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '<em><b>Parameter Definition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__PARAMETER_DEFINITION = eINSTANCE.getParameter_ParameterDefinition();

		/**
		 * The meta object literal for the '<em><b>Parameter Reuse</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER__PARAMETER_REUSE = eINSTANCE.getParameter_ParameterReuse();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.BooleanParameterImpl <em>Boolean Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.BooleanParameterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getBooleanParameter()
		 * @generated
		 */
		EClass BOOLEAN_PARAMETER = eINSTANCE.getBooleanParameter();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOLEAN_PARAMETER__VALUE = eINSTANCE.getBooleanParameter_Value();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.IntegerParameterImpl <em>Integer Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.IntegerParameterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getIntegerParameter()
		 * @generated
		 */
		EClass INTEGER_PARAMETER = eINSTANCE.getIntegerParameter();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTEGER_PARAMETER__VALUE = eINSTANCE.getIntegerParameter_Value();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.StringParameterImpl <em>String Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.StringParameterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getStringParameter()
		 * @generated
		 */
		EClass STRING_PARAMETER = eINSTANCE.getStringParameter();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_PARAMETER__VALUE = eINSTANCE.getStringParameter_Value();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.UserDefinedParameterImpl <em>User Defined Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.UserDefinedParameterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getUserDefinedParameter()
		 * @generated
		 */
		EClass USER_DEFINED_PARAMETER = eINSTANCE.getUserDefinedParameter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_DEFINED_PARAMETER__NAME = eINSTANCE.getUserDefinedParameter_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER_DEFINED_PARAMETER__DESCRIPTION = eINSTANCE.getUserDefinedParameter_Description();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PathFilterImpl <em>Path Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PathFilterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathFilter()
		 * @generated
		 */
		EClass PATH_FILTER = eINSTANCE.getPathFilter();

		/**
		 * The meta object literal for the '<em><b>Path Qualifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATH_FILTER__PATH_QUALIFIER = eINSTANCE.getPathFilter_PathQualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.LengthImpl <em>Length</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.LengthImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLength()
		 * @generated
		 */
		EClass LENGTH = eINSTANCE.getLength();

		/**
		 * The meta object literal for the '<em><b>Min</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENGTH__MIN = eINSTANCE.getLength_Min();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LENGTH__MAX = eINSTANCE.getLength_Max();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.SourceClassImpl <em>Source Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.SourceClassImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSourceClass()
		 * @generated
		 */
		EClass SOURCE_CLASS = eINSTANCE.getSourceClass();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.TargetClassImpl <em>Target Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.TargetClassImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getTargetClass()
		 * @generated
		 */
		EClass TARGET_CLASS = eINSTANCE.getTargetClass();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ContainmentReferenceImpl <em>Containment Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ContainmentReferenceImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContainmentReference()
		 * @generated
		 */
		EClass CONTAINMENT_REFERENCE = eINSTANCE.getContainmentReference();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PathQualifierImpl <em>Path Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PathQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathQualifier()
		 * @generated
		 */
		EClass PATH_QUALIFIER = eINSTANCE.getPathQualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PathSideImpl <em>Path Side</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PathSideImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathSide()
		 * @generated
		 */
		EClass PATH_SIDE = eINSTANCE.getPathSide();

		/**
		 * The meta object literal for the '<em><b>Class Sels</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATH_SIDE__CLASS_SELS = eINSTANCE.getPathSide_ClassSels();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PathTargetImpl <em>Path Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PathTargetImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathTarget()
		 * @generated
		 */
		EClass PATH_TARGET = eINSTANCE.getPathTarget();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PathSourceImpl <em>Path Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PathSourceImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathSource()
		 * @generated
		 */
		EClass PATH_SOURCE = eINSTANCE.getPathSource();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ElementFeaturementImpl <em>Element Featurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ElementFeaturementImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementFeaturement()
		 * @generated
		 */
		EClass ELEMENT_FEATUREMENT = eINSTANCE.getElementFeaturement();

		/**
		 * The meta object literal for the '<em><b>Element Sels</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_FEATUREMENT__ELEMENT_SELS = eINSTANCE.getElementFeaturement_ElementSels();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.StepClassImpl <em>Step Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.StepClassImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getStepClass()
		 * @generated
		 */
		EClass STEP_CLASS = eINSTANCE.getStepClass();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ExistanceImpl <em>Existance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ExistanceImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getExistance()
		 * @generated
		 */
		EClass EXISTANCE = eINSTANCE.getExistance();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.SelectorImpl <em>Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.SelectorImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSelector()
		 * @generated
		 */
		EClass SELECTOR = eINSTANCE.getSelector();

		/**
		 * The meta object literal for the '<em><b>Quantifier</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SELECTOR__QUANTIFIER = eINSTANCE.getSelector_Quantifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ElementSelectorImpl <em>Element Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ElementSelectorImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementSelector()
		 * @generated
		 */
		EClass ELEMENT_SELECTOR = eINSTANCE.getElementSelector();

		/**
		 * The meta object literal for the '<em><b>Element Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_SELECTOR__ELEMENT_FILTER = eINSTANCE.getElementSelector_ElementFilter();

		/**
		 * The meta object literal for the '<em><b>Element Conditioner</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_SELECTOR__ELEMENT_CONDITIONER = eINSTANCE.getElementSelector_ElementConditioner();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ClassSelectorImpl <em>Class Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ClassSelectorImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassSelector()
		 * @generated
		 */
		EClass CLASS_SELECTOR = eINSTANCE.getClassSelector();

		/**
		 * The meta object literal for the '<em><b>Class Conditioner</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_SELECTOR__CLASS_CONDITIONER = eINSTANCE.getClassSelector_ClassConditioner();

		/**
		 * The meta object literal for the '<em><b>Class Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_SELECTOR__CLASS_FILTER = eINSTANCE.getClassSelector_ClassFilter();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.FeatureSelectorImpl <em>Feature Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.FeatureSelectorImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getFeatureSelector()
		 * @generated
		 */
		EClass FEATURE_SELECTOR = eINSTANCE.getFeatureSelector();

		/**
		 * The meta object literal for the '<em><b>Feature Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_SELECTOR__FEATURE_FILTER = eINSTANCE.getFeatureSelector_FeatureFilter();

		/**
		 * The meta object literal for the '<em><b>Feature Conditioner</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_SELECTOR__FEATURE_CONDITIONER = eINSTANCE.getFeatureSelector_FeatureConditioner();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AttributeSelectorImpl <em>Attribute Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AttributeSelectorImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAttributeSelector()
		 * @generated
		 */
		EClass ATTRIBUTE_SELECTOR = eINSTANCE.getAttributeSelector();

		/**
		 * The meta object literal for the '<em><b>Attribute Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER = eINSTANCE.getAttributeSelector_AttributeFilter();

		/**
		 * The meta object literal for the '<em><b>Attribute Conditioner</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER = eINSTANCE.getAttributeSelector_AttributeConditioner();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ReferenceSelectorImpl <em>Reference Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ReferenceSelectorImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getReferenceSelector()
		 * @generated
		 */
		EClass REFERENCE_SELECTOR = eINSTANCE.getReferenceSelector();

		/**
		 * The meta object literal for the '<em><b>Reference Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_SELECTOR__REFERENCE_FILTER = eINSTANCE.getReferenceSelector_ReferenceFilter();

		/**
		 * The meta object literal for the '<em><b>Reference Conditioner</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_SELECTOR__REFERENCE_CONDITIONER = eINSTANCE.getReferenceSelector_ReferenceConditioner();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PathSelectorImpl <em>Path Selector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PathSelectorImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathSelector()
		 * @generated
		 */
		EClass PATH_SELECTOR = eINSTANCE.getPathSelector();

		/**
		 * The meta object literal for the '<em><b>Path Filter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATH_SELECTOR__PATH_FILTER = eINSTANCE.getPathSelector_PathFilter();

		/**
		 * The meta object literal for the '<em><b>Path Conditioner</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATH_SELECTOR__PATH_CONDITIONER = eINSTANCE.getPathSelector_PathConditioner();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AndQualifierImpl <em>And Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AndQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAndQualifier()
		 * @generated
		 */
		EClass AND_QUALIFIER = eINSTANCE.getAndQualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.OrQualifierImpl <em>Or Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.OrQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getOrQualifier()
		 * @generated
		 */
		EClass OR_QUALIFIER = eINSTANCE.getOrQualifier();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.CompoundQualifierImpl <em>Compound Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.CompoundQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCompoundQualifier()
		 * @generated
		 */
		EClass COMPOUND_QUALIFIER = eINSTANCE.getCompoundQualifier();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOUND_QUALIFIER__CHILDREN = eINSTANCE.getCompoundQualifier_Children();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.CompoundPathQualifierImpl <em>Compound Path Qualifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.CompoundPathQualifierImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCompoundPathQualifier()
		 * @generated
		 */
		EClass COMPOUND_PATH_QUALIFIER = eINSTANCE.getCompoundPathQualifier();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOUND_PATH_QUALIFIER__CHILDREN = eINSTANCE.getCompoundPathQualifier_Children();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PathAndImpl <em>Path And</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PathAndImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathAnd()
		 * @generated
		 */
		EClass PATH_AND = eINSTANCE.getPathAnd();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PathOrImpl <em>Path Or</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PathOrImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPathOr()
		 * @generated
		 */
		EClass PATH_OR = eINSTANCE.getPathOr();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AttributeContainmentImpl <em>Attribute Containment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AttributeContainmentImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAttributeContainment()
		 * @generated
		 */
		EClass ATTRIBUTE_CONTAINMENT = eINSTANCE.getAttributeContainment();

		/**
		 * The meta object literal for the '<em><b>Att Sels</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_CONTAINMENT__ATT_SELS = eINSTANCE.getAttributeContainment_AttSels();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ReferenceContainmentImpl <em>Reference Containment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ReferenceContainmentImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getReferenceContainment()
		 * @generated
		 */
		EClass REFERENCE_CONTAINMENT = eINSTANCE.getReferenceContainment();

		/**
		 * The meta object literal for the '<em><b>Ref Sels</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_CONTAINMENT__REF_SELS = eINSTANCE.getReferenceContainment_RefSels();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ContRootImpl <em>Cont Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ContRootImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContRoot()
		 * @generated
		 */
		EClass CONT_ROOT = eINSTANCE.getContRoot();

		/**
		 * The meta object literal for the '<em><b>Absolute</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONT_ROOT__ABSOLUTE = eINSTANCE.getContRoot_Absolute();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ContLeafImpl <em>Cont Leaf</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ContLeafImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContLeaf()
		 * @generated
		 */
		EClass CONT_LEAF = eINSTANCE.getContLeaf();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ElementRelationImpl <em>Element Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ElementRelationImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getElementRelation()
		 * @generated
		 */
		EClass ELEMENT_RELATION = eINSTANCE.getElementRelation();

		/**
		 * The meta object literal for the '<em><b>By Inheritance</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT_RELATION__BY_INHERITANCE = eINSTANCE.getElementRelation_ByInheritance();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ClassReachesImpl <em>Class Reaches</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ClassReachesImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassReaches()
		 * @generated
		 */
		EClass CLASS_REACHES = eINSTANCE.getClassReaches();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ClassReachedByImpl <em>Class Reached By</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ClassReachedByImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassReachedBy()
		 * @generated
		 */
		EClass CLASS_REACHED_BY = eINSTANCE.getClassReachedBy();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ContainmentPathImpl <em>Containment Path</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ContainmentPathImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getContainmentPath()
		 * @generated
		 */
		EClass CONTAINMENT_PATH = eINSTANCE.getContainmentPath();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.WithInheritancePathImpl <em>With Inheritance Path</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.WithInheritancePathImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getWithInheritancePath()
		 * @generated
		 */
		EClass WITH_INHERITANCE_PATH = eINSTANCE.getWithInheritancePath();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.CyclicPathImpl <em>Cyclic Path</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.CyclicPathImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCyclicPath()
		 * @generated
		 */
		EClass CYCLIC_PATH = eINSTANCE.getCyclicPath();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.SomeImpl <em>Some</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.SomeImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSome()
		 * @generated
		 */
		EClass SOME = eINSTANCE.getSome();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.InheritedFeatureImpl <em>Inherited Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.InheritedFeatureImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getInheritedFeature()
		 * @generated
		 */
		EClass INHERITED_FEATURE = eINSTANCE.getInheritedFeature();

		/**
		 * The meta object literal for the '<em><b>Class Sel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INHERITED_FEATURE__CLASS_SEL = eINSTANCE.getInheritedFeature_ClassSel();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ClassCollectsImpl <em>Class Collects</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ClassCollectsImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getClassCollects()
		 * @generated
		 */
		EClass CLASS_COLLECTS = eINSTANCE.getClassCollects();

		/**
		 * The meta object literal for the '<em><b>Collected Mult</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_COLLECTS__COLLECTED_MULT = eINSTANCE.getClassCollects_CollectedMult();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AnnotatedImpl <em>Annotated</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AnnotatedImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAnnotated()
		 * @generated
		 */
		EClass ANNOTATED = eINSTANCE.getAnnotated();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATED__NAME = eINSTANCE.getAnnotated_Name();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ImportImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__IMPORT_URI = eINSTANCE.getImport_ImportURI();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ParameterValueImpl <em>Parameter Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ParameterValueImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getParameterValue()
		 * @generated
		 */
		EClass PARAMETER_VALUE = eINSTANCE.getParameterValue();

		/**
		 * The meta object literal for the '<em><b>Param Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_VALUE__PARAM_DEFINITION = eINSTANCE.getParameterValue_ParamDefinition();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.SelfInheritanceImpl <em>Self Inheritance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.SelfInheritanceImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSelfInheritance()
		 * @generated
		 */
		EClass SELF_INHERITANCE = eINSTANCE.getSelfInheritance();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.NamedElementImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AnnotationImpl <em>Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AnnotationImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAnnotation()
		 * @generated
		 */
		EClass ANNOTATION = eINSTANCE.getAnnotation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__NAME = eINSTANCE.getAnnotation_Name();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AnnotatedElementImpl <em>Annotated Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AnnotatedElementImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAnnotatedElement()
		 * @generated
		 */
		EClass ANNOTATED_ELEMENT = eINSTANCE.getAnnotatedElement();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATED_ELEMENT__ANNOTATIONS = eINSTANCE.getAnnotatedElement_Annotations();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.DescribedElementImpl <em>Described Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.DescribedElementImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getDescribedElement()
		 * @generated
		 */
		EClass DESCRIBED_ELEMENT = eINSTANCE.getDescribedElement();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIBED_ELEMENT__DESCRIPTION = eINSTANCE.getDescribedElement_Description();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.SynonymImpl <em>Synonym</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.SynonymImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getSynonym()
		 * @generated
		 */
		EClass SYNONYM = eINSTANCE.getSynonym();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.VerbImpl <em>Verb</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.VerbImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getVerb()
		 * @generated
		 */
		EClass VERB = eINSTANCE.getVerb();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.NounImpl <em>Noun</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.NounImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNoun()
		 * @generated
		 */
		EClass NOUN = eINSTANCE.getNoun();

		/**
		 * The meta object literal for the '<em><b>Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOUN__NUM = eINSTANCE.getNoun_Num();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.AdjectiveImpl <em>Adjective</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.AdjectiveImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getAdjective()
		 * @generated
		 */
		EClass ADJECTIVE = eINSTANCE.getAdjective();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.WordNatureImpl <em>Word Nature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.WordNatureImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getWordNature()
		 * @generated
		 */
		EClass WORD_NATURE = eINSTANCE.getWordNature();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.PhraseNamedImpl <em>Phrase Named</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.PhraseNamedImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getPhraseNamed()
		 * @generated
		 */
		EClass PHRASE_NAMED = eINSTANCE.getPhraseNamed();

		/**
		 * The meta object literal for the '<em><b>Camelized</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHRASE_NAMED__CAMELIZED = eINSTANCE.getPhraseNamed_Camelized();

		/**
		 * The meta object literal for the '<em><b>Words</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHRASE_NAMED__WORDS = eINSTANCE.getPhraseNamed_Words();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHRASE_NAMED__START = eINSTANCE.getPhraseNamed_Start();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PHRASE_NAMED__END = eINSTANCE.getPhraseNamed_End();

		/**
		 * The meta object literal for the '<em><b>Pascal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PHRASE_NAMED__PASCAL = eINSTANCE.getPhraseNamed_Pascal();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.NamePartImpl <em>Name Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.NamePartImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNamePart()
		 * @generated
		 */
		EClass NAME_PART = eINSTANCE.getNamePart();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.LiteralImpl <em>Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.LiteralImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getLiteral()
		 * @generated
		 */
		EClass LITERAL = eINSTANCE.getLiteral();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LITERAL__LITERAL = eINSTANCE.getLiteral_Literal();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.TrivialStringImpl <em>Trivial String</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.TrivialStringImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getTrivialString()
		 * @generated
		 */
		EClass TRIVIAL_STRING = eINSTANCE.getTrivialString();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.OppositeToImpl <em>Opposite To</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.OppositeToImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getOppositeTo()
		 * @generated
		 */
		EClass OPPOSITE_TO = eINSTANCE.getOppositeTo();

		/**
		 * The meta object literal for the '<em><b>Ref Sel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPPOSITE_TO__REF_SEL = eINSTANCE.getOppositeTo_RefSel();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ParameterStringValueImpl <em>Parameter String Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ParameterStringValueImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getParameterStringValue()
		 * @generated
		 */
		EClass PARAMETER_STRING_VALUE = eINSTANCE.getParameterStringValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARAMETER_STRING_VALUE__VALUE = eINSTANCE.getParameterStringValue_Value();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.ParameterElementSetValueImpl <em>Parameter Element Set Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.ParameterElementSetValueImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getParameterElementSetValue()
		 * @generated
		 */
		EClass PARAMETER_ELEMENT_SET_VALUE = eINSTANCE.getParameterElementSetValue();

		/**
		 * The meta object literal for the '<em><b>Selector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_ELEMENT_SET_VALUE__SELECTOR = eINSTANCE.getParameterElementSetValue_Selector();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.NamedAfterImpl <em>Named After</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.NamedAfterImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getNamedAfter()
		 * @generated
		 */
		EClass NAMED_AFTER = eINSTANCE.getNamedAfter();

		/**
		 * The meta object literal for the '<em><b>Element Sels</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NAMED_AFTER__ELEMENT_SELS = eINSTANCE.getNamedAfter_ElementSels();

		/**
		 * The meta object literal for the '{@link metamodeltest.impl.OCLExpressionImpl <em>OCL Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.impl.OCLExpressionImpl
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getOCLExpression()
		 * @generated
		 */
		EClass OCL_EXPRESSION = eINSTANCE.getOCLExpression();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OCL_EXPRESSION__EXPRESSION = eINSTANCE.getOCLExpression_Expression();

		/**
		 * The meta object literal for the '{@link metamodeltest.Case <em>Case</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.Case
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getCase()
		 * @generated
		 */
		EEnum CASE = eINSTANCE.getCase();

		/**
		 * The meta object literal for the '{@link metamodeltest.GrammaticalNumber <em>Grammatical Number</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see metamodeltest.GrammaticalNumber
		 * @see metamodeltest.impl.MetamodeltestPackageImpl#getGrammaticalNumber()
		 * @generated
		 */
		EEnum GRAMMATICAL_NUMBER = eINSTANCE.getGrammaticalNumber();

	}

} //MetamodeltestPackage
