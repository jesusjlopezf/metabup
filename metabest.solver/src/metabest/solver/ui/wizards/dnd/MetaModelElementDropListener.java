/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.dnd;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaModel;


public class MetaModelElementDropListener extends ViewerDropAdapter {
	private final DropMetaModelElementTableTreeViewer viewer;
	private MetaModel metamodel = null;
	
	protected MetaModelElementDropListener(DropMetaModelElementTableTreeViewer viewer, MetaModel metamodel) {
		super(viewer);
		this.viewer = viewer;
		this.metamodel = metamodel;
	}

	@Override
	public boolean validateDrop(Object target, int operation,
			TransferData transferType) {
		return true;
	}

	 @Override
	 public void drop(DropTargetEvent event) {
	    int location = this.determineLocation(event);
	    String target = (String) determineTarget(event);
	    
	    String translatedLocation ="";
	    switch (location){
		    case 1 :
		      translatedLocation = "Dropped before the target ";
		      break;
		    case 2 :
		      translatedLocation = "Dropped after the target ";
		      break;
		    case 3 :
		      translatedLocation = "Dropped on the target ";
		      break;
		    case 4 :
		      translatedLocation = "Dropped into nothing ";
		      break;
	    }
	    super.drop(event);
	 }
	 
	 @Override
	 public boolean performDrop(Object data) {
		 AnnotatedElement element = null;
		 
		 for(EObject eo : metamodel.getAllElements()){
			 if (eo instanceof AnnotatedElement){
				 if(((AnnotatedElement) eo).getId().equals(data)){
					 element = (AnnotatedElement)eo;
				 }
			 }
		 }
		 		 
		 viewer.add(element);
		 System.out.println("drop: " + data.toString() + " " + data.getClass());
		 return true;
	 }
}
