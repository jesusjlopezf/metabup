/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.wizards;

import metabup.builder.MetaBupProjectNature;
import metabup.fragments.resources.FragmentFolder;
import metabup.metamodel.MetaModel;
import metabup.sketches.resources.LegendFolder;
import metabup.transformations.models.WMetamodelModel;
import metabup.ui.editor.editors.MetamodelInteractiveEditor;

import org.eclipse.jface.viewers.IStructuredSelection;

import java.awt.*;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.core.runtime.*;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.operation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.core.resources.*;

import java.io.*;

import org.eclipse.ui.*;
import org.eclipse.ui.ide.IDE;

// TODO: Auto-generated Javadoc
/**
 * The Class TransformNewWizard.
 */
public class NewMetaBupProjectFromEcoreWizard extends Wizard implements INewWizard {
	
	/** The page. */
	private NewMetaBupProjectWizardPage page1;
	//private NewInteractiveMetaModelWizardPage page2;
	private NewMetaBupProjectFromEcoreWizardPage page3;
	
	/** The selection. */
	private ISelection selection;
	private File ecoreFile = null;
	/**
	 * Constructor for TransformNewWizard.
	 */
	public NewMetaBupProjectFromEcoreWizard() {
		super();
		setNeedsProgressMonitor(true);
	}
	
	public NewMetaBupProjectFromEcoreWizard(File ecore) {
		super();
		setNeedsProgressMonitor(true);
		this.ecoreFile = ecore;
	}
	
	/**
	 * Adding the page to the wizard.
	 */
	public void addPages() {
		page1 = new NewMetaBupProjectWizardPage(selection);
		if(ecoreFile == null) page3 = new NewMetaBupProjectFromEcoreWizardPage(selection);
		addPage(page1);
		if(ecoreFile == null) addPage(page3);
	}

	/**
	 * This method is called when 'Finish' button is pressed in
	 * the wizard. We will create an operation and run it
	 * using wizard as execution context.
	 *
	 * @return true, if successful
	 */
	public boolean performFinish() {
		//final String containerName = page.getContainerName();
		final String folderName = page1.getFolderName();
		final Object result[] = page1.getResult();	
		//final String fileName = page2.getFileName();
		final File ecoreFile;
		if(this.ecoreFile == null) ecoreFile = new File(page3.getEcoreFile());
		else ecoreFile = this.ecoreFile;
		
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					try {
						doFinish(folderName, monitor, result, ecoreFile);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (AWTException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		
		try {
			getContainer().run(true, false, op);			
		} catch (InterruptedException e) {
			MessageDialog.openError(getShell(), "Error", e.getMessage());
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * The worker method. It will find the container, create the
	 * file if it's missing or just replace its contents, and open
	 * the editor on the newly created file.
	 *
	 * @param folderName the folder name
	 * @param monitor the monitor
	 * @param result the result
	 * @param ecoreFile 
	 * @throws CoreException the core exception
	 * @throws InterruptedException the interrupted exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws AWTException the aWT exception
	 */
	private void doFinish(String folderName, IProgressMonitor monitor, Object result[], final File ecoreFile)
		throws CoreException, InterruptedException, IOException, AWTException {
		// create a sample file
		monitor.beginTask("Creating " + folderName, 2);		
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();		
		IProject project  = root.getProject(folderName);
		
		project.create(monitor);
		project.open(monitor);
		
		// assigns metabup.project.nature to the new project
		IProjectDescription description = project.getDescription();		
		ArrayList<String> newIds = new ArrayList<String>();
		newIds.add(MetaBupProjectNature.NATURE_ID);
		description.setNatureIds((String[]) newIds.toArray(new String[newIds.size()]));
		project.setDescription(description, monitor);
					
		LegendFolder folder = new LegendFolder(project);
		folder.create(monitor);
		monitor.worked(1);
		
		FragmentFolder fFolder = new FragmentFolder(project);
		fFolder.create(monitor);
		monitor.worked(1);
		
		// refresh project so new files are present in workspace
		project.refreshLocal(IProject.DEPTH_INFINITE, monitor);
		
		getShell().getDisplay().asyncExec(new Runnable() {			
			public void run() {
				@SuppressWarnings("unused")
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			}			
		});			
		
		monitor.worked(1);
		
		String fileName = ecoreFile.getName().substring(0, ecoreFile.getName().indexOf('.'));	
		final IFile mbupFile = project.getFile(fileName + ".mbup");
		
		try {
			InputStream stream = openContentStream();			
			mbupFile.create(stream, true, null);
			stream.close();
		} catch (IOException e) {
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getShell().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbenchPage page =
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					MetamodelInteractiveEditor editor = (MetamodelInteractiveEditor)IDE.openEditor(page, mbupFile, true);
										
					MetaModel mm = editor.getSession().getMetamodelRoot();
					WMetamodelModel wmmm = editor.getSession().getMetamodelModel();					
															
					Resource rs = editor.getSession().getMetamodelResource();
					rs.save(null);
					editor.getSession().getMmFile().refreshLocal(1, null);
					
					editor.getSession().loadMetamodel(ecoreFile);
					editor.getViewer().refresh();
					
				} catch (PartInitException e) {
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		monitor.done();
	}	
	
	
	/**
	 * We will initialize file contents with a sample text.
	 */

	private InputStream openContentStream() {
		String contents =
			"shell new_metamodel" +
			"\n" +
			"\nfragment f{" +
			"\n" +
			"\n}";
		return new ByteArrayInputStream(contents.getBytes());
	}

	/**
	 * We will accept the selection in the workbench to see if
	 * we can initialize from it.
	 *
	 * @param workbench the workbench
	 * @param selection the selection
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}