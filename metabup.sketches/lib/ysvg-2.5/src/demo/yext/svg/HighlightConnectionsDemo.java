/****************************************************************************
 * This demo file is part of the yFiles extension package ySVG-2.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * ySVG demo files exhibit ySVG functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a ySVG version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own ySVG powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the ySVG
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.svg;

import yext.svg.io.SVGDOMEnhancer;
import yext.svg.io.SVGIOHandler;

import y.base.Edge;
import y.base.EdgeCursor;
import y.base.EdgeMap;
import y.base.Node;
import y.base.NodeCursor;
import y.util.Maps;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import y.view.LineType;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics2D;


/** 
 * Demonstrates how to write SVG that highlights all incoming and outgoing
 * edges at a node. The highlight effect gets triggered when the mouse is over
 * a node.
 * @see <a href="http://docs.yworks.com/yfiles/doc/developers-guide/svg_customization.html#svg_customization">Section Customizing SVG Content</a> in the yFiles for Java Developer's Guide
 */
public class HighlightConnectionsDemo extends SVGExportDemo
{
  /**
   * Creates and configures the SVGIOHandlers to be used.
   * This method will add a specialized SVGDOMEnhancer to
   * the IOHandler.
   */
  protected SVGIOHandler createSVGIOHandler(boolean svgz)
  {
    SVGIOHandler ioh = super.createSVGIOHandler(svgz);
    SVGDOMEnhancer enhancer = new HighlightConnections();
    ioh.setSVGGraph2DRenderer(enhancer);
    return ioh;
  }
  
 /**
  * Enhances the SVG DOM. Whenever the mouse is over a node highlight
  * its incoming and outgoing edges.
  */
  class HighlightConnections extends SVGDOMEnhancer
  {
    private EdgeMap edgeType;

    /**
     * Override a callback method. Before the graph will be written to the SVG DOM 
     * we add an additional ecmacript node to the definition block of the SVG. 
     * The script will be used to highlight the edges of a node on mouseover events.
     */ 
    protected void initializeDOM()
    {
      Graph2D graph = view.getGraph2D();
      edgeType = Maps.createIndexEdgeMap(new Object[graph.E()]);
      addToSVGDefinition(createScript());
    }
    
    /**
     * Paint edges in three differetn variants. The first variant gets
     * shown if the edge is not highlighted. The second variant
     * gets shown when the edge gets highlighted as an outgoing edge
     * of a node. The last variant gets shown when the edge gets highlighted 
     * as an incoming edge.
     * The type information of the currently painted will be stored in the 
     * edgeType map. 
     */
    protected void paint(Graphics2D gfx, EdgeRealizer r, boolean sloppyMode)
    {
      Edge e = r.getEdge();
      Color lineColor = r.getLineColor();
      LineType lineType = r.getLineType();

      edgeType.set(e, Color.black);
      super.paint(gfx, r, sloppyMode);  
     
      r.setLineColor(Color.red);
      r.setLineType(LineType.LINE_3);
      edgeType.set(e, Color.red);
      super.paint(gfx, r, sloppyMode);
      
      
      r.setLineColor(Color.green);
      r.setLineType(LineType.LINE_3);
      edgeType.set(e, Color.green);
      super.paint(gfx, r, sloppyMode);

      r.setLineType(lineType);
      r.setLineColor(lineColor);
    }
    
    /**
     * Mark each variant of the edge with a different group id.
     */
    protected String createGroupID(Edge e)
    {
      if(edgeType.get(e) == Color.green)
      {
        return "green.y.edge." + e.index();
      }
      else if(edgeType.get(e) == Color.red)
      {
        return "red.y.edge." + e.index();
      }
      else 
      {
        return "black.y.edge." + e.index();
      }
    }
    
    
    /**
     * Override a callback method. Add mouseover and mouseout events to
     * the nodes of the graph. The events trigger the scripts for
     * activating and deactivating the highlight effects.
     */
    protected void nodeAddedToDOM(Node yNode, org.w3c.dom.Element element)
    {
      element.setAttribute(
        "onmouseover", 
        "highlightEdges(evt,'" + yNode.index() + "')");
      element.setAttribute(
        "onmouseout", 
        "lowlightEdges(evt,'" + yNode.index() + "')");
    }
    
    /**
     * Override a callback method. Sets the initial visibility state on the
     * edges. Only the lowlighted edges will be visible in the beginning.
     */
    protected void edgeAddedToDOM(Edge yEdge, org.w3c.dom.Element element)
    {
      if(edgeType.get(yEdge) != Color.black)
      {
        element.setAttribute("visibility", "hidden");
      }
    }
    
    /**
     * Creates a DOM element that defines an ecmascript function responsible
     * for highlighting the adjacent edges. In the script we also
     * add the static arrays inEdges and outEdges that tell us which
     * edges belong to which nodes.
     */
    private org.w3c.dom.Element createScript()
    {
      org.w3c.dom.Element script = createElement("script");
      script.setAttribute("type","text/ecmascript");
      
      Graph2D graph = view.getGraph2D();
     
      //construct inEdges and outEdges array. inEdges[i]  will contain 
      //all incoming edges at the node with index i. outEdges[i] is similar.
      StringBuffer varInEdges = new StringBuffer("var inEdges = [");
      StringBuffer varOutEdges = new StringBuffer("var outEdges = [");
      for(NodeCursor nc = graph.nodes(); nc.ok(); nc.next())
      {
        Node v = nc.node();
        varInEdges.append("[");
        String del = "";
        for(EdgeCursor ec = v.inEdges(); ec.ok(); ec.next())
        {
          varInEdges.append(del).append(ec.edge().index());
          del = ",";
        }
        varInEdges.append("]");
        
        varOutEdges.append("[");
        del = "";
        for(EdgeCursor ec = v.outEdges(); ec.ok(); ec.next())
        {
          varOutEdges.append(del).append(ec.edge().index());
          del = ",";
        }
        varOutEdges.append("]");
        
        if(v.index() < graph.N()-1)
        {
          varInEdges.append(",");
          varOutEdges.append(",");
        }
      }
      varInEdges.append("];");
      varOutEdges.append("];");

      // Javascript code that works with both Adobe SVG Viewer Plugin
      // but also with native SVG support in modern browsers.
      String highlightEdges =
      "function highlightEdges(evt, nodeId) { \n" +
        " var document = evt.target.ownerDocument; \n" +
        " edgeIds = inEdges[nodeId]; \n" +
        " for(i = 0; i < edgeIds.length; i++) { \n" +
        "  var red   = document.getElementById('red.y.edge.' + edgeIds[i]); \n" +
        "  var green = document.getElementById('green.y.edge.' + edgeIds[i]); \n" +
        "  var black = document.getElementById('black.y.edge.' + edgeIds[i]); \n" +
        "  red.setAttributeNS(null, 'style'," + "'visibility:visible'); \n" +
        "  green.setAttributeNS(null, 'style'," + "'visibility:hidden'); \n" +
        "  black.setAttributeNS(null, 'style'," + "'visibility:hidden'); \n" +
        " } \n" +
        " edgeIds = outEdges[nodeId]; \n" +
        " for(i = 0; i < edgeIds.length; i++) { \n" +
        "  var red   = document.getElementById('red.y.edge.' + edgeIds[i]); \n" +
        "  var green = document.getElementById('green.y.edge.' + edgeIds[i]); \n" +
        "  var black = document.getElementById('black.y.edge.' + edgeIds[i]); \n" +
        "  red.setAttributeNS(null, 'style'," + "'visibility:hidden'); \n" +
        "  green.setAttributeNS(null, 'style'," + "'visibility:visible'); \n" +
        "  black.setAttributeNS(null, 'style'," + "'visibility:hidden'); \n" +
        " } \n" +
        "};";

      // Javascript code that works with both Adobe SVG Viewer Plugin
      // but also with native SVG support in modern browsers.
      String lowlightEdges =
      "function lowlightEdges(evt, nodeId) { \n" +
        " var document = evt.target.ownerDocument; \n" +
        " edgeIds = inEdges[nodeId]; \n" +
        " for(i = 0; i < edgeIds.length; i++) { \n" +
        "  var red   = document.getElementById('red.y.edge.' + edgeIds[i]); \n" +
        "  var green = document.getElementById('green.y.edge.' + edgeIds[i]); \n" +
        "  var black = document.getElementById('black.y.edge.' + edgeIds[i]); \n" +
        "  red.setAttributeNS(null, 'style'," + "'visibility:hidden'); \n" +
        "  green.setAttributeNS(null, 'style'," + "'visibility:hidden'); \n" +
        "  black.setAttributeNS(null, 'style'," + "'visibility:visible'); \n" +
        " } \n" +
        " edgeIds = outEdges[nodeId]; \n" +
        " for(i = 0; i < edgeIds.length; i++) { \n" +
        "  var red   = document.getElementById('red.y.edge.' + edgeIds[i]); \n" +
        "  var green = document.getElementById('green.y.edge.' + edgeIds[i]); \n" +
        "  var black = document.getElementById('black.y.edge.' + edgeIds[i]); \n" +
        "  red.setAttributeNS(null, 'style'," + "'visibility:hidden'); \n" +
        "  green.setAttributeNS(null, 'style'," + "'visibility:hidden'); \n" +
        "  black.setAttributeNS(null, 'style'," + "'visibility:visible'); \n" +
        " } \n" +
        "};";

      script.appendChild(createCDATASection(
        varInEdges + "\n" +
        varOutEdges + "\n" +
        highlightEdges + "\n" +
        lowlightEdges
        ));

      return script;
    }
  }

  /**
   * Launches this demo.
   * @param args ignored.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        initLnF();
        (new HighlightConnectionsDemo()).start();
      }
    });
  }
}