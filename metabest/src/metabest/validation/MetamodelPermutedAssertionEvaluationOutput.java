/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.jesusjlopezf.utils.structures.tree.TreeParent;

import metabest.preferences.MetaBestPreferenceConstants;
import metabest.ui.views.MetamodelAssertionEvaluationViewTreeParent;
import metabup.metamodel.Attribute;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;
import metamodeltest.Assertion;
import metamodeltest.AttributeSelector;
import metamodeltest.ClassSelector;
import metamodeltest.ElementSelector;
import metamodeltest.Every;
import metamodeltest.FeatureSelector;
import metamodeltest.ParameterElementSetValue;
import metamodeltest.Qualifier;
import metamodeltest.ReferenceSelector;
import metamodeltest.Selector;
import metamodeltest.Some;

public class MetamodelPermutedAssertionEvaluationOutput extends
		MetamodelAssertionEvaluationOutput {
	
	EvaluationTree positiveET = null;
	EvaluationTree negativeET = null;	
	
	public MetamodelPermutedAssertionEvaluationOutput(Assertion a, String name) {
		super(a, name);
	}
	
	public MetamodelPermutedAssertionEvaluationOutput(Assertion a, String name, boolean warning) {
		super(a, name, warning);
	}
	
	public boolean evaluate(MetaModel mm, List<ParameterElementSetValue> params) {	
		super.metamodel = mm;
		long startingTime = System.currentTimeMillis();
		List<String> paramNames = new ArrayList<String>();
		List<Integer> paramSelectors = new ArrayList<Integer>();
		CombinaSeries cs = new CombinaSeries();
		List<List<String>> pos = new ArrayList<List<String>>();
		
		for(ParameterElementSetValue pesv : params){
			List<String> paramValuesNames = new ArrayList<String>();
			if(this.getParamValues(pesv).isEmpty() || this.getParamValues(pesv) == null) return output = true;
				
			for(EObject eo : this.getParamValues(pesv)){
				if(eo instanceof MetaClass) paramValuesNames.add(((MetaClass)eo).getName());
				if(eo instanceof Feature) paramValuesNames.add(((Feature)eo).getName());
			}
				
			pos.add(paramValuesNames);
			cs.addSerie(this.getParamValues(pesv).toArray(new EObject[this.getParamValues(pesv).size()]));
			paramNames.add(pesv.getParamDefinition().getName());
			if(pesv.getSelector().getQuantifier() instanceof Some) paramSelectors.add(0);
			else if(pesv.getSelector().getQuantifier() instanceof Every) paramSelectors.add(1);
			else paramSelectors.add(-1);						
		}

		if(pos.isEmpty()){
			rationale = "The input parameter selection is empty";
			//System.out.println(rationale);
			return output = true;
		}
		
		positiveET = new EvaluationTree(paramSelectors.toArray(new Integer[paramSelectors.size()]), pos);
		negativeET = new EvaluationTree(paramSelectors.toArray(new Integer[paramSelectors.size()]), pos);
		output = false;
		
		do {			
			Assertion a = this.getParametrizedAssertion(paramNames, cs.getCurrentEObject());
			MetamodelAssertionEvaluationOutput maeo = new MetamodelAssertionEvaluationOutput(a, this.name);
			
			if(maeo.evaluate(this.metamodel)){
				output = positiveET.addChildren(cs.getCurrentString());								
				//System.out.println(cs.getCurrentString() + " returned true 1");
				if(fastcheck && output) return output = true;
			}else{
				//System.out.println(cs.getCurrentString() + " returned false 1");
				negativeET.addChildren(cs.getCurrentString());
				if(fastcheck && paramNames.size() == 1 && paramSelectors.get(0) == 1) return output = false;
			}
			
			cs.next();
		} while (cs.hasNext());
		
		if(cs.getCurrentEObject() != null){
			Assertion a = this.getParametrizedAssertion(paramNames, cs.getCurrentEObject());
			MetamodelAssertionEvaluationOutput maeo = new MetamodelAssertionEvaluationOutput(a, this.name);
			
			if(maeo.evaluate(this.metamodel)){
				output = positiveET.addChildren(cs.getCurrentString());	
				//System.out.println(cs.getCurrentString() + " returned true 2");
				if(fastcheck && output) return output = true;
			}else{
				//System.out.println(cs.getCurrentString() + " returned false 2");
				negativeET.addChildren(cs.getCurrentString());
			}		
		}
		
		this.runningTime = System.currentTimeMillis() - startingTime;
		return output;
	}
	
	public EvaluationTree getPermutationMatchTree(){
		if(output) return positiveET;
		else return negativeET;
	}
	
	public boolean isFastEvaluation(){
		return fastcheck;
	}
	
	public class EvaluationTree{
		private Node root;
		private int height;
		private List<Integer> ops = new ArrayList<Integer>(); // 0 = some ,, 1 = every
		private HashMap<Integer, List<String>> possibles = new HashMap<Integer, List<String>>();
		
		public EvaluationTree(Integer ops[], List<List<String>> pos){
			root = new RootNode(ops[0]);
			
			for(int i = 1; i < ops.length; i++) this.ops.add(ops[i]);
			this.ops.add(-1);
			
			this.height = this.ops.size();
			
			int i = 0;
			
			for(List<String> list : pos){
				HashSet hs = new HashSet();
				hs.addAll(list);
				hs.clear();
				list.addAll(hs);
				possibles.put(i, list);
				i++;
			}
		}
		
		public Node getRoot(){
			return this.root;
		}
		
		public boolean addChildren(List<String> objs){
			if(objs.size() == height){
				int level = 0;
				Node pivot = root;
				
				for(String o : objs){
					if(!pivot.getChildrenObjects().contains(o)) pivot.addChild(o, this.ops.get(level), possibles.get(level));															
					if(level == objs.size()-1) pivot.setStatus(true);
					pivot = pivot.getChild(o);
					level++;										
				}				
				
				pivot.setStatus(true);
				pivot = pivot.getParent();
				level--;
				
				do{
					if(pivot.isSome()){
						boolean found = false;
						
						if((pivot.getChildren() == null || pivot.getChildren().isEmpty()) && (possibles == null || possibles.size() == 0)) found = true;
						else{						
							for(Node n : pivot.getChildren())
								for(String p : possibles.get(level))
									if(p.equals(n.getObject()) && n.isValid())
										found = true;
						}
						
						if(found){
							pivot.setStatus(true);
							pivot = pivot.getParent();
							level--;
						}else{
							pivot.setStatus(false);
							pivot = null;
						}
					}else if(pivot.isEvery()){
							List<String> included = new ArrayList<String>();
							//if((pivot.getChildren() == null || pivot.getChildren().isEmpty()) && (possibles == null || possibles.size() == 0)) allIncluded = true;
							
							for(String s : possibles.get(level))
								for(Node n : pivot.getChildren())
									if(n.getObject().equals(s) && n.isValid())
										included.add(s);
							
							boolean allIncluded = (included.size() == possibles.get(level).size());
							
							if(allIncluded){								
								pivot.setStatus(true);														
								pivot = pivot.getParent();
								level--;
							}else{
								pivot.setStatus(false);
								pivot = null;								
							}
					}
				}while(pivot != null && level >= 0);								
			}
			
			return this.isValid();			
		}
		
		public boolean isValid(){
			return this.root.status;
		}
		
		public class Node{
			private boolean status = false;
			private String object;
			private int op = -1;
			private List<Node> children = new ArrayList<Node>();
			private Node parent;
			MetamodelAssertionEvaluationViewTreeParent treeParent = null;
			//private TreeParent treeParent = null;
			
			public Node(String object, int op){
				this.object = object;
				this.op = op;
				this.treeParent = new MetamodelAssertionEvaluationViewTreeParent(object);
			}
			
			public List<Node> getChildren(){
				return children;
			}
			
			public MetamodelAssertionEvaluationViewTreeParent getViewTree(){
				return this.treeParent;
			}
			
			public List<String> getChildrenObjects(){
				List<String> objs = new ArrayList<String>();
				
				for(Node n : this.getChildren())
					objs.add(n.getObject());
					
				return objs;
			}
			
			public Node getChild(String obj){
				for(Node n : this.getChildren())
					if(n.getObject().equals(obj))
						return n;
				
				return null;
			}
			
			public String getObject(){
				return this.object; 
			}
			
			public Node addChild(String child, int op, List<String> possibles){
				Node n = null;
				
				if(!this.getChildren().contains(child)){
					n = new Node(child, op);
					this.getChildren().add(n);
					n.setParent(this);
					treeParent.addChild(new MetamodelAssertionEvaluationViewTreeParent(child));
				}								
												
				return n;				
			}
			
			private boolean isSome(){
				if(this.op == 0) return true;
				else return false;
			}
			
			private boolean isEvery(){
				if(this.op == 1) return true;
				else return false;
			}
			
			private boolean isLeaf(){
				if(this.op == -1 && this.getChildren().isEmpty()) return true;
				else return false;
			}
			
			private List<Node> getBrothers(){
				if(this.getParent() != null){
					return this.getParent().getChildren();
				}
				
				return null;
			}
			
			private void setParent(Node n){
				this.parent = n;
			}
			
			private Node getParent(){
				return this.parent;
			}
			
			private void setStatus(boolean status){
				/*if(this.getParent() == null) this.status = status;
				else if(this.getParent().getStatus() == true) this.status = status;
				else this.status = false;*/
				this.status = status;
			}
			
			public boolean isValid(){
				return this.status;
			}
		}
		
		public class RootNode extends Node{
			RootNode(int op){
				super(null, op);
			}
		}
	}

	public class InvalidOpIdentifierException extends Exception {
		private static final long serialVersionUID = 1L;

		public InvalidOpIdentifierException(){
			super("Permutted evaluation tree detected an invalid operation id. Only 0 and 1 allowed");
		}
	}
	
	private Assertion getParametrizedAssertion(List<String> paramNames, List<EObject> candidates){
		HashMap<String, String> map = new HashMap<String, String>();
		
		for(int i = 0; i<paramNames.size(); i++){
			String name = null;
			if(candidates.get(i) instanceof MetaClass) name = ((MetaClass)candidates.get(i)).getName();
			else if(candidates.get(i) instanceof Feature) name = ((Feature)candidates.get(i)).getName();
			if(name == null) return null;
			map.put(paramNames.get(i), name);
		}
		
		Assertion newa = EcoreUtil.copy(this.assertion);
		newa.putValuesOnParameters(map);
		
		return newa;
	}

	class Serie {
		private List<EObject> content = new ArrayList<EObject>();
		private int index = 0;
		private Serie anterior;
		
		public Serie(EObject... elems) {
			this.content.addAll(Arrays.asList(elems));
		}
		
		public void setPrevia(Serie s) {
			this.anterior = s;
		}
		
		public EObject next() {
			if (this.content.size()==0) return null;
			if ( this.index < this.content.size()-1 ) {
				this.index++;
			}
			else {
				this.index=0;
				if(this.anterior != null) this.anterior.next();
			}
			return this.content.get(index);
		}
		public boolean hasNext() { return this.index < this.content.size()-1; }
		public EObject getCurrent() { return this.content.get(index); }
	}

	class CombinaSeries {
		private List<Serie> series = new ArrayList<Serie>();
		private Serie ultimaSerie = null;
		
		public void addSerie(EObject... elems) {
			Serie s = new Serie(elems);
			s.setPrevia(this.ultimaSerie);
			this.ultimaSerie = s;
			this.series.add(this.ultimaSerie);
		}
		
		public List<String> getCurrentString() {
			List<String> state = new ArrayList<String>();
			
			for (Serie s : this.series){
				if(s.getCurrent() instanceof MetaClass) state.add(((MetaClass)s.getCurrent()).getName());
				if(s.getCurrent() instanceof Feature) state.add(((Feature)s.getCurrent()).getName());
			}
						
			return state;
		}
		
		public List<EObject> getCurrentEObject() {
			List<EObject> state = new ArrayList<EObject>();
			for (Serie s : this.series) state.add(s.getCurrent());					
			return state;
		}
		
		public void next() {
			this.ultimaSerie.next();
		}
		
		public boolean hasNext() {
			for (Serie s: this.series) 
				if (s.hasNext()) return true;
			return false;
		}
	}
	
	private List<EObject> getParamValues(ParameterElementSetValue pesv){
		Selector s = pesv.getSelector();
		//Quantifier q = s.getQuantifier();
		
		if(s instanceof ElementSelector){
			ElementSelector es = (ElementSelector)s;
			if(es.getElementFilter() != null) return filterElements(metamodel.getAllElements(), (Qualifier)es.getElementFilter().getQualifier());
			else return metamodel.getAllElements();
		}
		
		if(s instanceof ClassSelector){
			ClassSelector es = (ClassSelector)s;
			
			List<EObject> classes = new ArrayList<EObject>();			
			for(EObject eo : metamodel.getClasses())
				if(eo instanceof MetaClass)
					classes.add(eo);
			
			if(es.getClassFilter() != null) return filterClasses(classes, (Qualifier)es.getClassFilter().getQualifier());
			else return classes;
		}
		
		if(s instanceof FeatureSelector){
			FeatureSelector es = (FeatureSelector)s;
			
			List<EObject> features = new ArrayList<EObject>();			
			for(EObject eo : metamodel.getClasses()){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					for(Feature f : mc.getFeatures()) features.add(f);
				}					
			}
			
			if(es.getFeatureFilter() != null) return filterFeatures(features, (Qualifier)es.getFeatureFilter().getQualifier());
			else return features;
		}
		
		if(s instanceof AttributeSelector){
			AttributeSelector es = (AttributeSelector)s;
			
			List<EObject> attributes = new ArrayList<EObject>();			
			for(EObject eo : metamodel.getClasses()){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					for(Attribute a : mc.getAttributes()) attributes.add(a);
				}					
			}
			
			if(es.getAttributeFilter() != null) return filterAttributes(attributes, (Qualifier)es.getAttributeFilter().getQualifier());
			else return attributes;
		}
		
		if(s instanceof ReferenceSelector){
			ReferenceSelector es = (ReferenceSelector)s;
			
			List<EObject> references = new ArrayList<EObject>();			
			for(EObject eo : metamodel.getClasses()){
				if(eo instanceof MetaClass){
					MetaClass mc = (MetaClass)eo;
					for(Reference r : mc.getReferences()) references.add(r);
				}					
			}
			
			if(es.getReferenceFilter() != null) return filterReferences(references, (Qualifier)es.getReferenceFilter().getQualifier());
			else return references;
		}
		
		return null;
	}
}
