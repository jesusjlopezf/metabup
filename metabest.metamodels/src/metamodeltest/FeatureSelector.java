/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Selector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.FeatureSelector#getFeatureFilter <em>Feature Filter</em>}</li>
 *   <li>{@link metamodeltest.FeatureSelector#getFeatureConditioner <em>Feature Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getFeatureSelector()
 * @model
 * @generated
 */
public interface FeatureSelector extends Selector {
	/**
	 * Returns the value of the '<em><b>Feature Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Filter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Filter</em>' containment reference.
	 * @see #setFeatureFilter(FeatureFilter)
	 * @see metamodeltest.MetamodeltestPackage#getFeatureSelector_FeatureFilter()
	 * @model containment="true"
	 * @generated
	 */
	FeatureFilter getFeatureFilter();

	/**
	 * Sets the value of the '{@link metamodeltest.FeatureSelector#getFeatureFilter <em>Feature Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Filter</em>' containment reference.
	 * @see #getFeatureFilter()
	 * @generated
	 */
	void setFeatureFilter(FeatureFilter value);

	/**
	 * Returns the value of the '<em><b>Feature Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Conditioner</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Conditioner</em>' containment reference.
	 * @see #setFeatureConditioner(FeatureFilter)
	 * @see metamodeltest.MetamodeltestPackage#getFeatureSelector_FeatureConditioner()
	 * @model containment="true"
	 * @generated
	 */
	FeatureFilter getFeatureConditioner();

	/**
	 * Sets the value of the '{@link metamodeltest.FeatureSelector#getFeatureConditioner <em>Feature Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Conditioner</em>' containment reference.
	 * @see #getFeatureConditioner()
	 * @generated
	 */
	void setFeatureConditioner(FeatureFilter value);
} // FeatureSelector
