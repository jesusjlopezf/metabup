/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see test.checks.ChecksFactory
 * @model kind="package"
 * @generated
 */
public interface ChecksPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "checks";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/metabest.metamodels/model/test.ecore#//checks";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "metabest.metamodels";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ChecksPackage eINSTANCE = test.checks.impl.ChecksPackageImpl.init();

	/**
	 * The meta object id for the '{@link test.checks.impl.CheckImpl <em>Check</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.CheckImpl
	 * @see test.checks.impl.ChecksPackageImpl#getCheck()
	 * @generated
	 */
	int CHECK = 0;

	/**
	 * The number of structural features of the '<em>Check</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Check</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.ExistenceImpl <em>Existence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.ExistenceImpl
	 * @see test.checks.impl.ChecksPackageImpl#getExistence()
	 * @generated
	 */
	int EXISTENCE = 1;

	/**
	 * The number of structural features of the '<em>Existence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXISTENCE_FEATURE_COUNT = CHECK_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Existence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXISTENCE_OPERATION_COUNT = CHECK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.FeatureExistenceImpl <em>Feature Existence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.FeatureExistenceImpl
	 * @see test.checks.impl.ChecksPackageImpl#getFeatureExistence()
	 * @generated
	 */
	int FEATURE_EXISTENCE = 2;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXISTENCE__FEATURE = EXISTENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Feature Existence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXISTENCE_FEATURE_COUNT = EXISTENCE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Feature Existence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_EXISTENCE_OPERATION_COUNT = EXISTENCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.TypeExistenceImpl <em>Type Existence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.TypeExistenceImpl
	 * @see test.checks.impl.ChecksPackageImpl#getTypeExistence()
	 * @generated
	 */
	int TYPE_EXISTENCE = 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_EXISTENCE__TYPE = EXISTENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type Existence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_EXISTENCE_FEATURE_COUNT = EXISTENCE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Type Existence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_EXISTENCE_OPERATION_COUNT = EXISTENCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.AbstractTypeInstanceImpl <em>Abstract Type Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.AbstractTypeInstanceImpl
	 * @see test.checks.impl.ChecksPackageImpl#getAbstractTypeInstance()
	 * @generated
	 */
	int ABSTRACT_TYPE_INSTANCE = 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TYPE_INSTANCE__TYPE = CHECK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Abstract Type Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TYPE_INSTANCE_FEATURE_COUNT = CHECK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Abstract Type Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TYPE_INSTANCE_OPERATION_COUNT = CHECK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.MissingAttributeImpl <em>Missing Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.MissingAttributeImpl
	 * @see test.checks.impl.ChecksPackageImpl#getMissingAttribute()
	 * @generated
	 */
	int MISSING_ATTRIBUTE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISSING_ATTRIBUTE__NAME = CHECK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISSING_ATTRIBUTE__OBJECT = CHECK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Missing Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISSING_ATTRIBUTE_FEATURE_COUNT = CHECK_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Missing Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISSING_ATTRIBUTE_OPERATION_COUNT = CHECK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.MismatchImpl <em>Mismatch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.MismatchImpl
	 * @see test.checks.impl.ChecksPackageImpl#getMismatch()
	 * @generated
	 */
	int MISMATCH = 6;

	/**
	 * The number of structural features of the '<em>Mismatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISMATCH_FEATURE_COUNT = CHECK_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Mismatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISMATCH_OPERATION_COUNT = CHECK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.MultiplicityMismatchImpl <em>Multiplicity Mismatch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.MultiplicityMismatchImpl
	 * @see test.checks.impl.ChecksPackageImpl#getMultiplicityMismatch()
	 * @generated
	 */
	int MULTIPLICITY_MISMATCH = 7;

	/**
	 * The feature id for the '<em><b>Feature Multiplicity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY = MISMATCH_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Multiplicity Mismatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_MISMATCH_FEATURE_COUNT = MISMATCH_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Multiplicity Mismatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_MISMATCH_OPERATION_COUNT = MISMATCH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.TypeMismatchImpl <em>Type Mismatch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.TypeMismatchImpl
	 * @see test.checks.impl.ChecksPackageImpl#getTypeMismatch()
	 * @generated
	 */
	int TYPE_MISMATCH = 8;

	/**
	 * The feature id for the '<em><b>Feature Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MISMATCH__FEATURE_TYPE = MISMATCH_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Type Mismatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MISMATCH_FEATURE_COUNT = MISMATCH_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Type Mismatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_MISMATCH_OPERATION_COUNT = MISMATCH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.FeatureNatureMismatchImpl <em>Feature Nature Mismatch</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.FeatureNatureMismatchImpl
	 * @see test.checks.impl.ChecksPackageImpl#getFeatureNatureMismatch()
	 * @generated
	 */
	int FEATURE_NATURE_MISMATCH = 9;

	/**
	 * The feature id for the '<em><b>Feature Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NATURE_MISMATCH__FEATURE_NATURE = MISMATCH_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Feature Nature Mismatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NATURE_MISMATCH_FEATURE_COUNT = MISMATCH_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Feature Nature Mismatch</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NATURE_MISMATCH_OPERATION_COUNT = MISMATCH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.RestrictionViolationImpl <em>Restriction Violation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.RestrictionViolationImpl
	 * @see test.checks.impl.ChecksPackageImpl#getRestrictionViolation()
	 * @generated
	 */
	int RESTRICTION_VIOLATION = 10;

	/**
	 * The feature id for the '<em><b>Fragment Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTION_VIOLATION__FRAGMENT_ELEMENT = CHECK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Restriction Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTION_VIOLATION__RESTRICTION_NAME = CHECK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Restriction Violation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTION_VIOLATION_FEATURE_COUNT = CHECK_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Restriction Violation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESTRICTION_VIOLATION_OPERATION_COUNT = CHECK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.UncontainmentImpl <em>Uncontainment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.UncontainmentImpl
	 * @see test.checks.impl.ChecksPackageImpl#getUncontainment()
	 * @generated
	 */
	int UNCONTAINMENT = 11;

	/**
	 * The feature id for the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT__OBJECT = CHECK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Uncontainment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_FEATURE_COUNT = CHECK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Uncontainment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_OPERATION_COUNT = CHECK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.UncontainmentByObjectImpl <em>Uncontainment By Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.UncontainmentByObjectImpl
	 * @see test.checks.impl.ChecksPackageImpl#getUncontainmentByObject()
	 * @generated
	 */
	int UNCONTAINMENT_BY_OBJECT = 12;

	/**
	 * The feature id for the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_BY_OBJECT__OBJECT = UNCONTAINMENT__OBJECT;

	/**
	 * The feature id for the '<em><b>Container</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_BY_OBJECT__CONTAINER = UNCONTAINMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Uncontainment By Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_BY_OBJECT_FEATURE_COUNT = UNCONTAINMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Uncontainment By Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_BY_OBJECT_OPERATION_COUNT = UNCONTAINMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.impl.UncontainmentByMetaClassImpl <em>Uncontainment By Meta Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.UncontainmentByMetaClassImpl
	 * @see test.checks.impl.ChecksPackageImpl#getUncontainmentByMetaClass()
	 * @generated
	 */
	int UNCONTAINMENT_BY_META_CLASS = 13;

	/**
	 * The feature id for the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_BY_META_CLASS__OBJECT = UNCONTAINMENT__OBJECT;

	/**
	 * The feature id for the '<em><b>Metaclass</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_BY_META_CLASS__METACLASS = UNCONTAINMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Uncontainment By Meta Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_BY_META_CLASS_FEATURE_COUNT = UNCONTAINMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Uncontainment By Meta Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNCONTAINMENT_BY_META_CLASS_OPERATION_COUNT = UNCONTAINMENT_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link test.checks.impl.MissingReferenceImpl <em>Missing Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.impl.MissingReferenceImpl
	 * @see test.checks.impl.ChecksPackageImpl#getMissingReference()
	 * @generated
	 */
	int MISSING_REFERENCE = 14;

	/**
	 * The feature id for the '<em><b>Ref Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISSING_REFERENCE__REF_NAME = CHECK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISSING_REFERENCE__FROM = CHECK_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>To</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISSING_REFERENCE__TO = CHECK_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Missing Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISSING_REFERENCE_FEATURE_COUNT = CHECK_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Missing Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISSING_REFERENCE_OPERATION_COUNT = CHECK_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link test.checks.Check <em>Check</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Check</em>'.
	 * @see test.checks.Check
	 * @generated
	 */
	EClass getCheck();

	/**
	 * Returns the meta object for class '{@link test.checks.Existence <em>Existence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Existence</em>'.
	 * @see test.checks.Existence
	 * @generated
	 */
	EClass getExistence();

	/**
	 * Returns the meta object for class '{@link test.checks.FeatureExistence <em>Feature Existence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Existence</em>'.
	 * @see test.checks.FeatureExistence
	 * @generated
	 */
	EClass getFeatureExistence();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.FeatureExistence#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature</em>'.
	 * @see test.checks.FeatureExistence#getFeature()
	 * @see #getFeatureExistence()
	 * @generated
	 */
	EReference getFeatureExistence_Feature();

	/**
	 * Returns the meta object for class '{@link test.checks.TypeExistence <em>Type Existence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Existence</em>'.
	 * @see test.checks.TypeExistence
	 * @generated
	 */
	EClass getTypeExistence();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.TypeExistence#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see test.checks.TypeExistence#getType()
	 * @see #getTypeExistence()
	 * @generated
	 */
	EReference getTypeExistence_Type();

	/**
	 * Returns the meta object for class '{@link test.checks.AbstractTypeInstance <em>Abstract Type Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Type Instance</em>'.
	 * @see test.checks.AbstractTypeInstance
	 * @generated
	 */
	EClass getAbstractTypeInstance();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.AbstractTypeInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Type</em>'.
	 * @see test.checks.AbstractTypeInstance#getType()
	 * @see #getAbstractTypeInstance()
	 * @generated
	 */
	EReference getAbstractTypeInstance_Type();

	/**
	 * Returns the meta object for class '{@link test.checks.MissingAttribute <em>Missing Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Missing Attribute</em>'.
	 * @see test.checks.MissingAttribute
	 * @generated
	 */
	EClass getMissingAttribute();

	/**
	 * Returns the meta object for the attribute '{@link test.checks.MissingAttribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see test.checks.MissingAttribute#getName()
	 * @see #getMissingAttribute()
	 * @generated
	 */
	EAttribute getMissingAttribute_Name();

	/**
	 * Returns the meta object for the reference '{@link test.checks.MissingAttribute#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see test.checks.MissingAttribute#getObject()
	 * @see #getMissingAttribute()
	 * @generated
	 */
	EReference getMissingAttribute_Object();

	/**
	 * Returns the meta object for class '{@link test.checks.Mismatch <em>Mismatch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mismatch</em>'.
	 * @see test.checks.Mismatch
	 * @generated
	 */
	EClass getMismatch();

	/**
	 * Returns the meta object for class '{@link test.checks.MultiplicityMismatch <em>Multiplicity Mismatch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplicity Mismatch</em>'.
	 * @see test.checks.MultiplicityMismatch
	 * @generated
	 */
	EClass getMultiplicityMismatch();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.MultiplicityMismatch#getFeatureMultiplicity <em>Feature Multiplicity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Multiplicity</em>'.
	 * @see test.checks.MultiplicityMismatch#getFeatureMultiplicity()
	 * @see #getMultiplicityMismatch()
	 * @generated
	 */
	EReference getMultiplicityMismatch_FeatureMultiplicity();

	/**
	 * Returns the meta object for class '{@link test.checks.TypeMismatch <em>Type Mismatch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Mismatch</em>'.
	 * @see test.checks.TypeMismatch
	 * @generated
	 */
	EClass getTypeMismatch();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.TypeMismatch#getFeatureType <em>Feature Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Type</em>'.
	 * @see test.checks.TypeMismatch#getFeatureType()
	 * @see #getTypeMismatch()
	 * @generated
	 */
	EReference getTypeMismatch_FeatureType();

	/**
	 * Returns the meta object for class '{@link test.checks.FeatureNatureMismatch <em>Feature Nature Mismatch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Nature Mismatch</em>'.
	 * @see test.checks.FeatureNatureMismatch
	 * @generated
	 */
	EClass getFeatureNatureMismatch();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.FeatureNatureMismatch#getFeatureNature <em>Feature Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature Nature</em>'.
	 * @see test.checks.FeatureNatureMismatch#getFeatureNature()
	 * @see #getFeatureNatureMismatch()
	 * @generated
	 */
	EReference getFeatureNatureMismatch_FeatureNature();

	/**
	 * Returns the meta object for class '{@link test.checks.RestrictionViolation <em>Restriction Violation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Restriction Violation</em>'.
	 * @see test.checks.RestrictionViolation
	 * @generated
	 */
	EClass getRestrictionViolation();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.RestrictionViolation#getFragmentElement <em>Fragment Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fragment Element</em>'.
	 * @see test.checks.RestrictionViolation#getFragmentElement()
	 * @see #getRestrictionViolation()
	 * @generated
	 */
	EReference getRestrictionViolation_FragmentElement();

	/**
	 * Returns the meta object for the attribute '{@link test.checks.RestrictionViolation#getRestrictionName <em>Restriction Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Restriction Name</em>'.
	 * @see test.checks.RestrictionViolation#getRestrictionName()
	 * @see #getRestrictionViolation()
	 * @generated
	 */
	EAttribute getRestrictionViolation_RestrictionName();

	/**
	 * Returns the meta object for class '{@link test.checks.Uncontainment <em>Uncontainment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uncontainment</em>'.
	 * @see test.checks.Uncontainment
	 * @generated
	 */
	EClass getUncontainment();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.Uncontainment#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object</em>'.
	 * @see test.checks.Uncontainment#getObject()
	 * @see #getUncontainment()
	 * @generated
	 */
	EReference getUncontainment_Object();

	/**
	 * Returns the meta object for class '{@link test.checks.UncontainmentByObject <em>Uncontainment By Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uncontainment By Object</em>'.
	 * @see test.checks.UncontainmentByObject
	 * @generated
	 */
	EClass getUncontainmentByObject();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.UncontainmentByObject#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Container</em>'.
	 * @see test.checks.UncontainmentByObject#getContainer()
	 * @see #getUncontainmentByObject()
	 * @generated
	 */
	EReference getUncontainmentByObject_Container();

	/**
	 * Returns the meta object for class '{@link test.checks.UncontainmentByMetaClass <em>Uncontainment By Meta Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Uncontainment By Meta Class</em>'.
	 * @see test.checks.UncontainmentByMetaClass
	 * @generated
	 */
	EClass getUncontainmentByMetaClass();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.UncontainmentByMetaClass#getMetaclass <em>Metaclass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Metaclass</em>'.
	 * @see test.checks.UncontainmentByMetaClass#getMetaclass()
	 * @see #getUncontainmentByMetaClass()
	 * @generated
	 */
	EReference getUncontainmentByMetaClass_Metaclass();

	/**
	 * Returns the meta object for class '{@link test.checks.MissingReference <em>Missing Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Missing Reference</em>'.
	 * @see test.checks.MissingReference
	 * @generated
	 */
	EClass getMissingReference();

	/**
	 * Returns the meta object for the attribute '{@link test.checks.MissingReference#getRefName <em>Ref Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ref Name</em>'.
	 * @see test.checks.MissingReference#getRefName()
	 * @see #getMissingReference()
	 * @generated
	 */
	EAttribute getMissingReference_RefName();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.MissingReference#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>From</em>'.
	 * @see test.checks.MissingReference#getFrom()
	 * @see #getMissingReference()
	 * @generated
	 */
	EReference getMissingReference_From();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.MissingReference#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>To</em>'.
	 * @see test.checks.MissingReference#getTo()
	 * @see #getMissingReference()
	 * @generated
	 */
	EReference getMissingReference_To();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ChecksFactory getChecksFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link test.checks.impl.CheckImpl <em>Check</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.CheckImpl
		 * @see test.checks.impl.ChecksPackageImpl#getCheck()
		 * @generated
		 */
		EClass CHECK = eINSTANCE.getCheck();

		/**
		 * The meta object literal for the '{@link test.checks.impl.ExistenceImpl <em>Existence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.ExistenceImpl
		 * @see test.checks.impl.ChecksPackageImpl#getExistence()
		 * @generated
		 */
		EClass EXISTENCE = eINSTANCE.getExistence();

		/**
		 * The meta object literal for the '{@link test.checks.impl.FeatureExistenceImpl <em>Feature Existence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.FeatureExistenceImpl
		 * @see test.checks.impl.ChecksPackageImpl#getFeatureExistence()
		 * @generated
		 */
		EClass FEATURE_EXISTENCE = eINSTANCE.getFeatureExistence();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_EXISTENCE__FEATURE = eINSTANCE.getFeatureExistence_Feature();

		/**
		 * The meta object literal for the '{@link test.checks.impl.TypeExistenceImpl <em>Type Existence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.TypeExistenceImpl
		 * @see test.checks.impl.ChecksPackageImpl#getTypeExistence()
		 * @generated
		 */
		EClass TYPE_EXISTENCE = eINSTANCE.getTypeExistence();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_EXISTENCE__TYPE = eINSTANCE.getTypeExistence_Type();

		/**
		 * The meta object literal for the '{@link test.checks.impl.AbstractTypeInstanceImpl <em>Abstract Type Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.AbstractTypeInstanceImpl
		 * @see test.checks.impl.ChecksPackageImpl#getAbstractTypeInstance()
		 * @generated
		 */
		EClass ABSTRACT_TYPE_INSTANCE = eINSTANCE.getAbstractTypeInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_TYPE_INSTANCE__TYPE = eINSTANCE.getAbstractTypeInstance_Type();

		/**
		 * The meta object literal for the '{@link test.checks.impl.MissingAttributeImpl <em>Missing Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.MissingAttributeImpl
		 * @see test.checks.impl.ChecksPackageImpl#getMissingAttribute()
		 * @generated
		 */
		EClass MISSING_ATTRIBUTE = eINSTANCE.getMissingAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MISSING_ATTRIBUTE__NAME = eINSTANCE.getMissingAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MISSING_ATTRIBUTE__OBJECT = eINSTANCE.getMissingAttribute_Object();

		/**
		 * The meta object literal for the '{@link test.checks.impl.MismatchImpl <em>Mismatch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.MismatchImpl
		 * @see test.checks.impl.ChecksPackageImpl#getMismatch()
		 * @generated
		 */
		EClass MISMATCH = eINSTANCE.getMismatch();

		/**
		 * The meta object literal for the '{@link test.checks.impl.MultiplicityMismatchImpl <em>Multiplicity Mismatch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.MultiplicityMismatchImpl
		 * @see test.checks.impl.ChecksPackageImpl#getMultiplicityMismatch()
		 * @generated
		 */
		EClass MULTIPLICITY_MISMATCH = eINSTANCE.getMultiplicityMismatch();

		/**
		 * The meta object literal for the '<em><b>Feature Multiplicity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY = eINSTANCE.getMultiplicityMismatch_FeatureMultiplicity();

		/**
		 * The meta object literal for the '{@link test.checks.impl.TypeMismatchImpl <em>Type Mismatch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.TypeMismatchImpl
		 * @see test.checks.impl.ChecksPackageImpl#getTypeMismatch()
		 * @generated
		 */
		EClass TYPE_MISMATCH = eINSTANCE.getTypeMismatch();

		/**
		 * The meta object literal for the '<em><b>Feature Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE_MISMATCH__FEATURE_TYPE = eINSTANCE.getTypeMismatch_FeatureType();

		/**
		 * The meta object literal for the '{@link test.checks.impl.FeatureNatureMismatchImpl <em>Feature Nature Mismatch</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.FeatureNatureMismatchImpl
		 * @see test.checks.impl.ChecksPackageImpl#getFeatureNatureMismatch()
		 * @generated
		 */
		EClass FEATURE_NATURE_MISMATCH = eINSTANCE.getFeatureNatureMismatch();

		/**
		 * The meta object literal for the '<em><b>Feature Nature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_NATURE_MISMATCH__FEATURE_NATURE = eINSTANCE.getFeatureNatureMismatch_FeatureNature();

		/**
		 * The meta object literal for the '{@link test.checks.impl.RestrictionViolationImpl <em>Restriction Violation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.RestrictionViolationImpl
		 * @see test.checks.impl.ChecksPackageImpl#getRestrictionViolation()
		 * @generated
		 */
		EClass RESTRICTION_VIOLATION = eINSTANCE.getRestrictionViolation();

		/**
		 * The meta object literal for the '<em><b>Fragment Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESTRICTION_VIOLATION__FRAGMENT_ELEMENT = eINSTANCE.getRestrictionViolation_FragmentElement();

		/**
		 * The meta object literal for the '<em><b>Restriction Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESTRICTION_VIOLATION__RESTRICTION_NAME = eINSTANCE.getRestrictionViolation_RestrictionName();

		/**
		 * The meta object literal for the '{@link test.checks.impl.UncontainmentImpl <em>Uncontainment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.UncontainmentImpl
		 * @see test.checks.impl.ChecksPackageImpl#getUncontainment()
		 * @generated
		 */
		EClass UNCONTAINMENT = eINSTANCE.getUncontainment();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNCONTAINMENT__OBJECT = eINSTANCE.getUncontainment_Object();

		/**
		 * The meta object literal for the '{@link test.checks.impl.UncontainmentByObjectImpl <em>Uncontainment By Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.UncontainmentByObjectImpl
		 * @see test.checks.impl.ChecksPackageImpl#getUncontainmentByObject()
		 * @generated
		 */
		EClass UNCONTAINMENT_BY_OBJECT = eINSTANCE.getUncontainmentByObject();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNCONTAINMENT_BY_OBJECT__CONTAINER = eINSTANCE.getUncontainmentByObject_Container();

		/**
		 * The meta object literal for the '{@link test.checks.impl.UncontainmentByMetaClassImpl <em>Uncontainment By Meta Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.UncontainmentByMetaClassImpl
		 * @see test.checks.impl.ChecksPackageImpl#getUncontainmentByMetaClass()
		 * @generated
		 */
		EClass UNCONTAINMENT_BY_META_CLASS = eINSTANCE.getUncontainmentByMetaClass();

		/**
		 * The meta object literal for the '<em><b>Metaclass</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNCONTAINMENT_BY_META_CLASS__METACLASS = eINSTANCE.getUncontainmentByMetaClass_Metaclass();

		/**
		 * The meta object literal for the '{@link test.checks.impl.MissingReferenceImpl <em>Missing Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.impl.MissingReferenceImpl
		 * @see test.checks.impl.ChecksPackageImpl#getMissingReference()
		 * @generated
		 */
		EClass MISSING_REFERENCE = eINSTANCE.getMissingReference();

		/**
		 * The meta object literal for the '<em><b>Ref Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MISSING_REFERENCE__REF_NAME = eINSTANCE.getMissingReference_RefName();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MISSING_REFERENCE__FROM = eINSTANCE.getMissingReference_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MISSING_REFERENCE__TO = eINSTANCE.getMissingReference_To();

	}

} //ChecksPackage
