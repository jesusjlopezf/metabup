/*******************************************************************************
 * Copyright (c) 2006, 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.importers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import metabup.metamodel.MetaModel;
import metabup.transformations.models.WMetamodelModel;
import metabup.ui.editor.editors.MetamodelInteractiveEditor;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.emf.ecore.resource.Resource;

public class EcoreFolderImportWizard extends Wizard implements IImportWizard {
	
	EcoreFolderImportWizardPage mainPage;

	public EcoreFolderImportWizard() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	public boolean performFinish() {
		final List<IFile> ecoreFiles;
		final IProject project;
		try {
			ecoreFiles = mainPage.getEcoreFilesFromFolder();
			project = mainPage.getTargetProject();
			if (ecoreFiles == null || ecoreFiles.isEmpty() || project == null) return false;
			
			for(IFile ecf : ecoreFiles){
				final IFile ecoreFile = ecf;				
				String fileName = ecoreFile.getName().substring(0, ecoreFile.getName().indexOf('.'));	
				//System.out.println("file: " + fileName);

				final IFile mbupFile = project.getFile(fileName + ".mbup");
				
				if(mbupFile.exists()){
					if(!MessageDialog.openQuestion(this.getShell(), "Overwrite file?", fileName + ".mbup already exists in the project. Would you like to overwrite it?")){
						try {
							ecoreFile.delete(true, null);
						} catch (CoreException e) {					
						}
						return false;
					}
				}
				
				try {
					InputStream stream = openContentStream();			
					mbupFile.create(stream, true, null);
					stream.close();
				} catch (IOException e) {
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				getShell().getDisplay().asyncExec(new Runnable() {
					public void run() {
						IWorkbenchPage page =
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
						try {
							MetamodelInteractiveEditor editor = (MetamodelInteractiveEditor)IDE.openEditor(page, mbupFile, true);
							
							//MetamodelInteractiveEditor editor = new MetamodelInteractiveEditor();	

							editor.getSession().setFile(mbupFile);
							editor.getSession().updateAssistance();
							MetaModel mm = editor.getSession().getMetamodelRoot();
							WMetamodelModel wmmm = editor.getSession().getMetamodelModel();
							Resource rs = editor.getSession().getMetamodelResource();
							rs.save(null);
							editor.getSession().getMmFile().refreshLocal(1, null);
							editor.getSession().loadMetamodel(ifileToFile(ecoreFile));
							editor.getViewer().refresh();

							//editor.getEditorSite().getPage().closeEditor(editor, true);
							mbupFile.getProject().refreshLocal(IResource.DEPTH_INFINITE, null);														
							
							/*Thread.sleep(1500);
							
							Robot r = new Robot();
							
							for(int i = 0; i<ecoreFiles.size()*2; i++){
								r.keyPress(KeyEvent.VK_N);
								r.keyPress(KeyEvent.VK_N);
							}*/
														
						} catch (PartInitException e) {
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (CoreException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});		
			}
			
			
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return true;
	}
	 
	private InputStream openContentStream() {
		String contents =
			"This is the initial file contents for *.mbup file that should be word-sorted in the Preview page of the multi-page editor";
		return new ByteArrayInputStream(contents.getBytes());
	}
	
	/**
	 * Static method for turning a file's IFile representation into File.
	 *
	 * @param ifile the ifile
	 * @return IFile	File instance.
	 */
	public static File ifileToFile(IFile ifile){
		File file = new File(ifile.getLocation().toString());
		return file;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("File Import Wizard"); //NON-NLS-1
		setNeedsProgressMonitor(true);
		mainPage = new EcoreFolderImportWizardPage(selection); //NON-NLS-1		
	}
	
	/* (non-Javadoc)
     * @see org.eclipse.jface.wizard.IWizard#addPages()
     */
    public void addPages() {
        super.addPages(); 
        addPage(mainPage);
    }

}
