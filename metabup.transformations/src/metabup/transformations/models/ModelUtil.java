/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations.models;

import java.util.LinkedList;
import java.util.List;

public class ModelUtil {

	public static <T> List<T> select(List<?> list, Class<T> type) {
		LinkedList<T> result = new LinkedList<T>();
		for (Object t : list) {
			if ( type.isInstance(t) ) {
				result.add(type.cast(t));
			}
		}
		return result;	
	}		
}
