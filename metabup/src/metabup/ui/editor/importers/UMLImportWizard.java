/*******************************************************************************
 * Copyright (c) 2006, 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.importers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import metabup.metamodel.MetaModel;
import metabup.transformations.models.WMetamodelModel;
import metabup.ui.editor.editors.MetamodelInteractiveEditor;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.emf.ecore.resource.Resource;

public class UMLImportWizard extends Wizard implements IImportWizard {
	
	UMLImportWizardPage mainPage;

	public UMLImportWizard() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	public boolean performFinish() {
		final IFile umlFile = mainPage.createNewFile();
		if (umlFile == null)
            return false;
		
		/*final IFile collaboroHistoryFile = secondPage.createNewFile();
		if (collaboroHistoryFile == null)
            return false;*/
		
		String fileName = umlFile.getName().substring(0, umlFile.getName().indexOf('.'));	
		
		IProject project = umlFile.getProject();
		final IFile mbupFile = project.getFile(fileName + ".mbup");
		
		if(mbupFile.exists()){
			if(!MessageDialog.openQuestion(this.getShell(), "Overwrite file?", fileName + ".mbup already exists in the project. Would you like to overwrite it?")){
				try {
					umlFile.delete(true, null);
				} catch (CoreException e) {					
				}
				return false;
			}
		}
		
		try {
			InputStream stream = openContentStream();			
			mbupFile.create(stream, true, null);
			stream.close();
		} catch (IOException e) {
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		getShell().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbenchPage page =
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				try {
					MetamodelInteractiveEditor editor = (MetamodelInteractiveEditor)IDE.openEditor(page, mbupFile, true);
										
					MetaModel mm = editor.getSession().getMetamodelRoot();
					WMetamodelModel wmmm = editor.getSession().getMetamodelModel();					
															
					Resource rs = editor.getSession().getMetamodelResource();
					rs.save(null);
					editor.getSession().getMmFile().refreshLocal(1, null);
					
					//editor.getSession().loadUMLModel(ifileToFile(umlFile));
					editor.getViewer().refresh();
					
				} catch (PartInitException e) {
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		/*
		try {
			ecoreFile.delete(true, null);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
        return true;
	}
	 
	private InputStream openContentStream() {
		String contents =
			"This is the initial file contents for *.mbup file that should be word-sorted in the Preview page of the multi-page editor";
		return new ByteArrayInputStream(contents.getBytes());
	}
	
	/**
	 * Static method for turning a file's IFile representation into File.
	 *
	 * @param ifile the ifile
	 * @return IFile	File instance.
	 */
	public static File ifileToFile(IFile ifile){
		File file = new File(ifile.getLocation().toString());
		return file;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle("File Import Wizard"); //NON-NLS-1
		setNeedsProgressMonitor(true);
		mainPage = new UMLImportWizardPage("Import UML model as interactive metamodel",selection); //NON-NLS-1		
	}
	
	/* (non-Javadoc)
     * @see org.eclipse.jface.wizard.IWizard#addPages()
     */
    public void addPages() {
        super.addPages(); 
        addPage(mainPage);
    }

}
