/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.constraint;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.Object;
import metabup.annotations.extensionpoint.ConstraintAnnotation;
import metabup.annotations.extensionpoint.DesignAnnotation;
import metabup.issues.general.IOpenIssue;
import metabup.issues.lib.AcyclicReferenceCycle;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Annotation;
import metabup.metamodel.AnnotationParam;
import metabup.metamodel.MetaClass;
import metabup.metamodel.ParamValue;
import metabup.metamodel.Reference;
import metabup.metamodel.StringValue;

public class Constraint extends ConstraintAnnotation {
	public Constraint (){
		this.setName("constraint");
		this.setDescription("For setting a specific constraint");
	}
	
	@Override
	public List<IOpenIssue> detectFragmentIssues(FragmentModel fm, AnnotatedElement ae){
		return null;
	}
	
	@Override
	public void updateConstraints() {
		if(element instanceof MetaClass){
			String ELEMENT = ((MetaClass)element).getName();
			
			List<ParamValue> params = new ArrayList<ParamValue>();
			Annotation ann = null;
			
			for(Annotation a : element.getAnnotations()){
				if(a.getName().toLowerCase().equals(this.getName().toLowerCase())){
					ann = a;
					break;
				}
			}
			
			for(AnnotationParam param : ann.getParams()){
				if(param.getValue() instanceof StringValue)
					super.addConstraint(param.getName(), "context " + ELEMENT + " inv " + param.getName() +" : " + ((StringValue)param.getValue()).getValue());
			}
		}
	}
}
