/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Collects</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.ClassCollects#getCollectedMult <em>Collected Mult</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getClassCollects()
 * @model
 * @generated
 */
public interface ClassCollects extends ClassReaches {

	/**
	 * Returns the value of the '<em><b>Collected Mult</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collected Mult</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collected Mult</em>' containment reference.
	 * @see #setCollectedMult(metamodeltest.Number)
	 * @see metamodeltest.MetamodeltestPackage#getClassCollects_CollectedMult()
	 * @model containment="true" required="true"
	 * @generated
	 */
	metamodeltest.Number getCollectedMult();

	/**
	 * Sets the value of the '{@link metamodeltest.ClassCollects#getCollectedMult <em>Collected Mult</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collected Mult</em>' containment reference.
	 * @see #getCollectedMult()
	 * @generated
	 */
	void setCollectedMult(metamodeltest.Number value);
} // ClassCollects
