/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance;

import java.util.HashMap;
import java.util.List;

import metabup.annotations.IAnnotation;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaModel;
import metabup.transformations.models.WMetamodelModel;

public interface IAssistanceTip{	
	boolean let(WMetamodelModel sourceMetaModel, IAnnotation ann);
	boolean calculateSolutions();
	
	public List<ISolution> getSolutions();
	public HashMap<String,Object> getPreferences();
	
	public String getName();
}
