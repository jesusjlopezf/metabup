package metabup.suggestions.views.model;

import java.io.Serializable;

import org.eclipse.core.runtime.IAdaptable;

public class TreeObject implements IAdaptable, Serializable{
	private static final long serialVersionUID = 1L;
	
	protected String name = "";
	private TreeParent parent;
	
	public TreeObject(String name){
		this.name = name;
	}
	
	/*private ISemanticElement element;
	
	public ISemanticElement getSemanticElement() {
		return element;
	}

	public void setSemanticElement(ISemanticElement element) {
		this.element = element;
	}*/
	
	public TreeObject(Object element){
		/*if(element instanceof ISemanticElement){
			this.name = this.name + ((ISemanticElement) element).getName();
			this.element = (ISemanticElement) element;
		}*/
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setParent(TreeParent parent) {
		this.parent = parent;
	}
	
	public TreeParent getParent() {
		return parent;
	}
	public String toString() {
		return getName();
	}
	public <T> T getAdapter(Class<T> key) {
		return null;
	}
}
