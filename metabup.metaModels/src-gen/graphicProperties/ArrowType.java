/**
 */
package graphicProperties;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Arrow Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see graphicProperties.GraphicPropertiesPackage#getArrowType()
 * @model
 * @generated
 */
public enum ArrowType implements Enumerator {
	/**
	 * The '<em><b>None</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NONE_VALUE
	 * @generated
	 * @ordered
	 */
	NONE(0, "None", "None"),

	/**
	 * The '<em><b>Standard</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STANDARD_VALUE
	 * @generated
	 * @ordered
	 */
	STANDARD(1, "Standard", "Standard"),

	/**
	 * The '<em><b>Delta</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DELTA_VALUE
	 * @generated
	 * @ordered
	 */
	DELTA(2, "Delta", "Delta"),

	/**
	 * The '<em><b>White Delta</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WHITE_DELTA_VALUE
	 * @generated
	 * @ordered
	 */
	WHITE_DELTA(3, "WhiteDelta", "WhiteDelta"),

	/**
	 * The '<em><b>Diamond</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIAMOND_VALUE
	 * @generated
	 * @ordered
	 */
	DIAMOND(4, "Diamond", "Diamond"),

	/**
	 * The '<em><b>White Diamond</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WHITE_DIAMOND_VALUE
	 * @generated
	 * @ordered
	 */
	WHITE_DIAMOND(5, "WhiteDiamond", "WhiteDiamond"),

	/**
	 * The '<em><b>Short</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SHORT_VALUE
	 * @generated
	 * @ordered
	 */
	SHORT(6, "Short", "Short"),

	/**
	 * The '<em><b>Plain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLAIN_VALUE
	 * @generated
	 * @ordered
	 */
	PLAIN(7, "Plain", "Plain"),

	/**
	 * The '<em><b>Concave</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONCAVE_VALUE
	 * @generated
	 * @ordered
	 */
	CONCAVE(8, "Concave", "Concave"),

	/**
	 * The '<em><b>Convex</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONVEX_VALUE
	 * @generated
	 * @ordered
	 */
	CONVEX(9, "Convex", "Convex"),

	/**
	 * The '<em><b>Circle</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CIRCLE_VALUE
	 * @generated
	 * @ordered
	 */
	CIRCLE(10, "Circle", "Circle"),

	/**
	 * The '<em><b>Transparent Circle</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRANSPARENT_CIRCLE_VALUE
	 * @generated
	 * @ordered
	 */
	TRANSPARENT_CIRCLE(11, "TransparentCircle", "TransparentCircle"),

	/**
	 * The '<em><b>Dash</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DASH_VALUE
	 * @generated
	 * @ordered
	 */
	DASH(12, "Dash", "Dash"),

	/**
	 * The '<em><b>Skewed Dash</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SKEWED_DASH_VALUE
	 * @generated
	 * @ordered
	 */
	SKEWED_DASH(13, "SkewedDash", "SkewedDash"),

	/**
	 * The '<em><b>TShape</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TSHAPE_VALUE
	 * @generated
	 * @ordered
	 */
	TSHAPE(14, "TShape", "TShape"),

	/**
	 * The '<em><b>Crows Foot One Mandatory</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_ONE_MANDATORY_VALUE
	 * @generated
	 * @ordered
	 */
	CROWS_FOOT_ONE_MANDATORY(15, "CrowsFootOneMandatory", "CrowsFootOneMandatory"),

	/**
	 * The '<em><b>Crows Foot Many Mandatory</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_MANY_MANDATORY_VALUE
	 * @generated
	 * @ordered
	 */
	CROWS_FOOT_MANY_MANDATORY(16, "CrowsFootManyMandatory", "CrowsFootManyMandatory"),

	/**
	 * The '<em><b>Crows Foot One Optional</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_ONE_OPTIONAL_VALUE
	 * @generated
	 * @ordered
	 */
	CROWS_FOOT_ONE_OPTIONAL(17, "CrowsFootOneOptional", "CrowsFootOneOptional"),

	/**
	 * The '<em><b>Crows Foot Many Optional</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_MANY_OPTIONAL_VALUE
	 * @generated
	 * @ordered
	 */
	CROWS_FOOT_MANY_OPTIONAL(18, "CrowsFootManyOptional", "CrowsFootManyOptional"),

	/**
	 * The '<em><b>Crows Foot One</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_ONE_VALUE
	 * @generated
	 * @ordered
	 */
	CROWS_FOOT_ONE(19, "CrowsFootOne", "CrowsFootOne"),

	/**
	 * The '<em><b>Crows Foot Many</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_MANY_VALUE
	 * @generated
	 * @ordered
	 */
	CROWS_FOOT_MANY(20, "CrowsFootMany", "CrowsFootMany");

	/**
	 * The '<em><b>None</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>None</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NONE
	 * @model name="None"
	 * @generated
	 * @ordered
	 */
	public static final int NONE_VALUE = 0;

	/**
	 * The '<em><b>Standard</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Standard</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STANDARD
	 * @model name="Standard"
	 * @generated
	 * @ordered
	 */
	public static final int STANDARD_VALUE = 1;

	/**
	 * The '<em><b>Delta</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Delta</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DELTA
	 * @model name="Delta"
	 * @generated
	 * @ordered
	 */
	public static final int DELTA_VALUE = 2;

	/**
	 * The '<em><b>White Delta</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>White Delta</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WHITE_DELTA
	 * @model name="WhiteDelta"
	 * @generated
	 * @ordered
	 */
	public static final int WHITE_DELTA_VALUE = 3;

	/**
	 * The '<em><b>Diamond</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diamond</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIAMOND
	 * @model name="Diamond"
	 * @generated
	 * @ordered
	 */
	public static final int DIAMOND_VALUE = 4;

	/**
	 * The '<em><b>White Diamond</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>White Diamond</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WHITE_DIAMOND
	 * @model name="WhiteDiamond"
	 * @generated
	 * @ordered
	 */
	public static final int WHITE_DIAMOND_VALUE = 5;

	/**
	 * The '<em><b>Short</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Short</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHORT
	 * @model name="Short"
	 * @generated
	 * @ordered
	 */
	public static final int SHORT_VALUE = 6;

	/**
	 * The '<em><b>Plain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Plain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLAIN
	 * @model name="Plain"
	 * @generated
	 * @ordered
	 */
	public static final int PLAIN_VALUE = 7;

	/**
	 * The '<em><b>Concave</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Concave</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONCAVE
	 * @model name="Concave"
	 * @generated
	 * @ordered
	 */
	public static final int CONCAVE_VALUE = 8;

	/**
	 * The '<em><b>Convex</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Convex</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONVEX
	 * @model name="Convex"
	 * @generated
	 * @ordered
	 */
	public static final int CONVEX_VALUE = 9;

	/**
	 * The '<em><b>Circle</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Circle</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CIRCLE
	 * @model name="Circle"
	 * @generated
	 * @ordered
	 */
	public static final int CIRCLE_VALUE = 10;

	/**
	 * The '<em><b>Transparent Circle</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Transparent Circle</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRANSPARENT_CIRCLE
	 * @model name="TransparentCircle"
	 * @generated
	 * @ordered
	 */
	public static final int TRANSPARENT_CIRCLE_VALUE = 11;

	/**
	 * The '<em><b>Dash</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Dash</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DASH
	 * @model name="Dash"
	 * @generated
	 * @ordered
	 */
	public static final int DASH_VALUE = 12;

	/**
	 * The '<em><b>Skewed Dash</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Skewed Dash</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SKEWED_DASH
	 * @model name="SkewedDash"
	 * @generated
	 * @ordered
	 */
	public static final int SKEWED_DASH_VALUE = 13;

	/**
	 * The '<em><b>TShape</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TShape</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TSHAPE
	 * @model name="TShape"
	 * @generated
	 * @ordered
	 */
	public static final int TSHAPE_VALUE = 14;

	/**
	 * The '<em><b>Crows Foot One Mandatory</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Crows Foot One Mandatory</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_ONE_MANDATORY
	 * @model name="CrowsFootOneMandatory"
	 * @generated
	 * @ordered
	 */
	public static final int CROWS_FOOT_ONE_MANDATORY_VALUE = 15;

	/**
	 * The '<em><b>Crows Foot Many Mandatory</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Crows Foot Many Mandatory</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_MANY_MANDATORY
	 * @model name="CrowsFootManyMandatory"
	 * @generated
	 * @ordered
	 */
	public static final int CROWS_FOOT_MANY_MANDATORY_VALUE = 16;

	/**
	 * The '<em><b>Crows Foot One Optional</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Crows Foot One Optional</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_ONE_OPTIONAL
	 * @model name="CrowsFootOneOptional"
	 * @generated
	 * @ordered
	 */
	public static final int CROWS_FOOT_ONE_OPTIONAL_VALUE = 17;

	/**
	 * The '<em><b>Crows Foot Many Optional</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Crows Foot Many Optional</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_MANY_OPTIONAL
	 * @model name="CrowsFootManyOptional"
	 * @generated
	 * @ordered
	 */
	public static final int CROWS_FOOT_MANY_OPTIONAL_VALUE = 18;

	/**
	 * The '<em><b>Crows Foot One</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Crows Foot One</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_ONE
	 * @model name="CrowsFootOne"
	 * @generated
	 * @ordered
	 */
	public static final int CROWS_FOOT_ONE_VALUE = 19;

	/**
	 * The '<em><b>Crows Foot Many</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Crows Foot Many</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CROWS_FOOT_MANY
	 * @model name="CrowsFootMany"
	 * @generated
	 * @ordered
	 */
	public static final int CROWS_FOOT_MANY_VALUE = 20;

	/**
	 * An array of all the '<em><b>Arrow Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ArrowType[] VALUES_ARRAY =
		new ArrowType[] {
			NONE,
			STANDARD,
			DELTA,
			WHITE_DELTA,
			DIAMOND,
			WHITE_DIAMOND,
			SHORT,
			PLAIN,
			CONCAVE,
			CONVEX,
			CIRCLE,
			TRANSPARENT_CIRCLE,
			DASH,
			SKEWED_DASH,
			TSHAPE,
			CROWS_FOOT_ONE_MANDATORY,
			CROWS_FOOT_MANY_MANDATORY,
			CROWS_FOOT_ONE_OPTIONAL,
			CROWS_FOOT_MANY_OPTIONAL,
			CROWS_FOOT_ONE,
			CROWS_FOOT_MANY,
		};

	/**
	 * A public read-only list of all the '<em><b>Arrow Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ArrowType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Arrow Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ArrowType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ArrowType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Arrow Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ArrowType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ArrowType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Arrow Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ArrowType get(int value) {
		switch (value) {
			case NONE_VALUE: return NONE;
			case STANDARD_VALUE: return STANDARD;
			case DELTA_VALUE: return DELTA;
			case WHITE_DELTA_VALUE: return WHITE_DELTA;
			case DIAMOND_VALUE: return DIAMOND;
			case WHITE_DIAMOND_VALUE: return WHITE_DIAMOND;
			case SHORT_VALUE: return SHORT;
			case PLAIN_VALUE: return PLAIN;
			case CONCAVE_VALUE: return CONCAVE;
			case CONVEX_VALUE: return CONVEX;
			case CIRCLE_VALUE: return CIRCLE;
			case TRANSPARENT_CIRCLE_VALUE: return TRANSPARENT_CIRCLE;
			case DASH_VALUE: return DASH;
			case SKEWED_DASH_VALUE: return SKEWED_DASH;
			case TSHAPE_VALUE: return TSHAPE;
			case CROWS_FOOT_ONE_MANDATORY_VALUE: return CROWS_FOOT_ONE_MANDATORY;
			case CROWS_FOOT_MANY_MANDATORY_VALUE: return CROWS_FOOT_MANY_MANDATORY;
			case CROWS_FOOT_ONE_OPTIONAL_VALUE: return CROWS_FOOT_ONE_OPTIONAL;
			case CROWS_FOOT_MANY_OPTIONAL_VALUE: return CROWS_FOOT_MANY_OPTIONAL;
			case CROWS_FOOT_ONE_VALUE: return CROWS_FOOT_ONE;
			case CROWS_FOOT_MANY_VALUE: return CROWS_FOOT_MANY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ArrowType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ArrowType
