/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import metamodeltest.AssertionDefinition;
import metamodeltest.LibraryAssertionCall;
import metamodeltest.MetamodeltestPackage;
import metamodeltest.ParameterElementSetValue;
import metamodeltest.ParameterValue;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Library Assertion Call</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.LibraryAssertionCallImpl#getReferencedAssertion <em>Referenced Assertion</em>}</li>
 *   <li>{@link metamodeltest.impl.LibraryAssertionCallImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LibraryAssertionCallImpl extends AssertionCallImpl implements LibraryAssertionCall {
	/**
	 * The cached value of the '{@link #getReferencedAssertion() <em>Referenced Assertion</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedAssertion()
	 * @generated
	 * @ordered
	 */
	protected AssertionDefinition referencedAssertion;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterValue> parameters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LibraryAssertionCallImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.LIBRARY_ASSERTION_CALL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionDefinition getReferencedAssertion() {
		if (referencedAssertion != null && referencedAssertion.eIsProxy()) {
			InternalEObject oldReferencedAssertion = (InternalEObject)referencedAssertion;
			referencedAssertion = (AssertionDefinition)eResolveProxy(oldReferencedAssertion);
			if (referencedAssertion != oldReferencedAssertion) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MetamodeltestPackage.LIBRARY_ASSERTION_CALL__REFERENCED_ASSERTION, oldReferencedAssertion, referencedAssertion));
			}
		}
		return referencedAssertion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssertionDefinition basicGetReferencedAssertion() {
		return referencedAssertion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencedAssertion(AssertionDefinition newReferencedAssertion) {
		AssertionDefinition oldReferencedAssertion = referencedAssertion;
		referencedAssertion = newReferencedAssertion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.LIBRARY_ASSERTION_CALL__REFERENCED_ASSERTION, oldReferencedAssertion, referencedAssertion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterValue> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<ParameterValue>(ParameterValue.class, this, MetamodeltestPackage.LIBRARY_ASSERTION_CALL__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL__REFERENCED_ASSERTION:
				if (resolve) return getReferencedAssertion();
				return basicGetReferencedAssertion();
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL__PARAMETERS:
				return getParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL__REFERENCED_ASSERTION:
				setReferencedAssertion((AssertionDefinition)newValue);
				return;
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends ParameterValue>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL__REFERENCED_ASSERTION:
				setReferencedAssertion((AssertionDefinition)null);
				return;
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL__PARAMETERS:
				getParameters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL__REFERENCED_ASSERTION:
				return referencedAssertion != null;
			case MetamodeltestPackage.LIBRARY_ASSERTION_CALL__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public String toString(){
		String str = this.getReferencedAssertion().getName();
		str += ": " + this.getReferencedAssertion().toString();
		return str;
	}

	@Override
	public List<ParameterElementSetValue> getPermutedParameters() {
		List<ParameterElementSetValue> esParams = new ArrayList<ParameterElementSetValue>();
		
		for(ParameterValue pv : this.getParameters())
			if(pv instanceof ParameterElementSetValue)
				esParams.add((ParameterElementSetValue)pv);
		
		return esParams;
	}
} //LibraryAssertionCallImpl
