/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.change.collaboro;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import metabup.change.trace.ChangeTrace;
import metabup.change.trace.ChangeTraceInheritanceItem;
import metabup.change.trace.ChangeTraceItem;
import metabup.preferences.PreferenceConstants;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.IPreferenceStore;

import History.*;

public class CollaboroChangeTrace extends ChangeTrace {

	public void exportToCollaboro(IFile chFile){		
		ResourceSetImpl rs = new ResourceSetImpl();
    	rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
    	Resource chResource = new XMIResourceImpl(URI.createURI(chFile.getLocationURI().toString()));
    	
    	//IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
    	IProject project = chFile.getProject();
		IFile ecoreFile = project.getFile(chFile.getName().substring(0, chFile.getName().indexOf(".")) + ".ecore");
		Resource ecoreResource = null;
		
    	if(ecoreFile.exists()){
    		ecoreResource = new XMIResourceImpl(URI.createURI(ecoreFile.getLocationURI().toString()));
    		rs.getResources().add(ecoreResource);
    	}else //System.out.println("No ecore file found. It might carry future problems with serialization");
    	
    	rs.getResources().add(chResource);
    	
    	if(!chFile.exists()){       		        	
        	History collHistory = HistoryFactory.eINSTANCE.createHistory();
        	
        	String username = metabup.preferences.PreferenceConstants.P_COLLABORO_USERNAME;
        	
        	if(username.equals("")){
	        	InputDialog ipDialog = new InputDialog(null, "Collaboro username required", "Enter your collaboro username: ", PreferenceConstants.P_COLLABORO_USERNAME, null);	        	
				if(ipDialog.open() == InputDialog.OK){
					username = ipDialog.getValue();
					IPreferenceStore store = metabup.preferences.Activator.getDefault().getPreferenceStore();
					store.setDefault(PreferenceConstants.P_COLLABORO_USERNAME, username);
				}
        	}
        	
			User user = HistoryFactory.eINSTANCE.createUser();
			user.setId(username);				
			
        	VersionHistory vh = HistoryFactory.eINSTANCE.createVersionHistory();
        	vh.setType(VersionHistoryType.TRUNK);
        	
        	Version version = HistoryFactory.eINSTANCE.createVersion();
        	version.setId("1.0");
        	
        	Proposal proposal = HistoryFactory.eINSTANCE.createProposal();
        	proposal.setProposedBy(user);
        	proposal.setRationale("metabup-based proposal");
        	proposal.setId("n0");
        	
        	Solution solution = HistoryFactory.eINSTANCE.createSolution();
        	solution.setProposedBy(user);        	        	
        	
        	// root ePackage generation in case there's no one available 
        	if(ePackage == null){
        		InputDialog ipDialog = new InputDialog(null, "Main EPackage name required", "Name the EPackage that will contain your Metaclasses", chFile.getName().substring(0, chFile.getName().lastIndexOf(".")), null);	        	
				if(ipDialog.open() == InputDialog.OK){
					ePackage = EcoreFactory.eINSTANCE.createEPackage();
					ePackage.setName(ipDialog.getValue());
					
					Add add = HistoryFactory.eINSTANCE.createAdd();
					
					NewAbstractSyntaxElement nase = HistoryFactory.eINSTANCE.createNewAbstractSyntaxElement();    			
        			nase.setElement(ePackage);    		        			
        			add.setTarget(nase);
        			
        			add.setSolution(solution);								    			
        			solution.getChanges().add(add);     
				}
        	}
        	
        	solution.getChanges().addAll(this.getCollaboroAdds(solution));        	       
        	solution.getChanges().addAll(this.getCollaboroDeletes(solution));        	
        	solution.getChanges().addAll(this.getCollaboroUpdates(solution)); 
        	
        	proposal.getSols().add(solution);
        	version.getProposals().add(proposal);
        	vh.getVersions().add(version);
        	collHistory.getUsers().add(user);
        	collHistory.getHistories().add(vh);
        	
        	chResource.getContents().add(collHistory);	 
        	
        	try {
				chFile.create(null, true, null);
				chFile.refreshLocal(1, null);
	        	chResource.save(null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}         	        	
        	catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}else{
    		//WindowDialog.openError();
    		//System.out.println("The file already exists");
    	}
	}


	protected List<Add> getCollaboroAdds(Solution solution){
		List<Add> adds = new ArrayList<Add>();
		
    	for(ChangeTraceItem i : this.getAdds()){
    		Add add = HistoryFactory.eINSTANCE.createAdd();			
    		EModelElement eme = null;
    		ExistingAbstractSyntaxElement ease = HistoryFactory.eINSTANCE.createExistingAbstractSyntaxElement();
    		
    		/* EClass elements addition */
    		if(i.getEcoreElement() instanceof EClass){
    			eme = EcoreFactory.eINSTANCE.createEClass();        			
    			eme = (EClass) i.getEcoreElement();        		
    			
        		ease.setElement(ePackage);	
        		add.setReferredElement(ease);            		        		        		        			
    		}else{
    			EClass contClass = EcoreFactory.eINSTANCE.createEClass();    			    			
    			
    			if(i.getEcoreElement() instanceof EReference){
        			eme = EcoreFactory.eINSTANCE.createEReference();
        			eme = (EReference) i.getEcoreElement();             			
        			((EReference) eme).setEType((EClass)this.getInitializerByID(((metabup.metamodel.Reference)i.getMmElement()).getReference().getId()).getEcoreElement());        			            		        			            		
    			} else{
    				if(i.getEcoreElement() instanceof EAttribute){
	        			eme = EcoreFactory.eINSTANCE.createEAttribute();
	        			eme = (EAttribute) i.getEcoreElement();        				        				        				            			
        			}  
    			}
    			
        		// assign the container metaclass as the referred element
    			contClass = (EClass)this.getInitializerByID(((String)i.getReferredElementId())).getEcoreElement();    			      			        			      	
        		ease.setElement(contClass);        			
        		add.setReferredElement(ease);
    			
    		}
    		
    		NewAbstractSyntaxElement nase = HistoryFactory.eINSTANCE.createNewAbstractSyntaxElement();    			
			nase.setElement(eme);    		        			
			add.setTarget(nase);
			
			add.setSolution(solution);	
			adds.add(add);
    	}    	    	
    	
    	return adds;
	}
	
	protected List<Delete> getCollaboroDeletes(Solution solution){
		List<Delete> dels = new ArrayList<Delete>();
		
    	for(ChangeTraceItem i : this.getDeletes()){
    		Delete del = HistoryFactory.eINSTANCE.createDelete();    		
    		EModelElement eme = null;
    		ExistingAbstractSyntaxElement ease = HistoryFactory.eINSTANCE.createExistingAbstractSyntaxElement();
    		
    		/* EClass elements deletion */
    		if(i.getEcoreElement() instanceof EClass){
    			eme = EcoreFactory.eINSTANCE.createEClass();        			
    			eme = (EClass) i.getEcoreElement();        		
    			
        		ease.setElement(ePackage);	
        		del.setReferredElement(ease);            		        		        		        			
    		}else{
    			EClass contClass = EcoreFactory.eINSTANCE.createEClass();    			    			
    			
    			if(i.getEcoreElement() instanceof EReference){
        			eme = EcoreFactory.eINSTANCE.createEReference();
        			eme = (EReference) i.getEcoreElement();             			
        			((EReference) eme).setEType((EClass)this.getInitializerByID(((metabup.metamodel.Reference)i.getMmElement()).getReference().getId()).getEcoreElement());        			            	        			            	
    			} else{
    				if(i.getEcoreElement() instanceof EAttribute){
	        			eme = EcoreFactory.eINSTANCE.createEAttribute();
	        			eme = (EAttribute) i.getEcoreElement();        				        				        				            			
        			}  
    			}
    			
        		// assign the container metaclass as the referred element    			    			    			    			
    			contClass = (EClass)this.getInitializerByID(((String)i.getReferredElementId())).getEcoreElement();
    			ease.setElement(contClass);  			
            	del.setReferredElement(ease);    			        		    		
    		}
    		
    		ExistingAbstractSyntaxElement tarEase = HistoryFactory.eINSTANCE.createExistingAbstractSyntaxElement();    			
			tarEase.setElement(eme);
			del.setTarget(tarEase);
			
			del.setSolution(solution);	
			dels.add(del);
    	}
    	
    	return dels;
	}
	
	
	protected List<Update> getCollaboroUpdates(Solution solution){
		List<Update> upds = new ArrayList<Update>();
		
		for(ChangeTraceItem i : this.getUpdates()){
    		Update upd = HistoryFactory.eINSTANCE.createUpdate();			
    		EModelElement eme2 = null;
    		ExistingAbstractSyntaxElement ease = HistoryFactory.eINSTANCE.createExistingAbstractSyntaxElement();
    		
    		/* EClass elements update */
    		if(i.getEcoreElement() instanceof EClass){
    			eme2 = EcoreFactory.eINSTANCE.createEClass();        			
    			eme2 = (EClass) i.getEcoreElement();        		
    			
        		ease.setElement(ePackage);	
        		upd.setReferredElement(ease);            		        		        		        			
    		}else{
    			EClass contClass = EcoreFactory.eINSTANCE.createEClass();    			    			
    			
    			if(i.getEcoreElement() instanceof EReference){
        			eme2 = EcoreFactory.eINSTANCE.createEReference();
        			eme2 = (EReference) i.getEcoreElement();             			
        			((EReference) eme2).setEType((EClass)this.getInitializerByID(((metabup.metamodel.Reference)i.getMmElement()).getReference().getId()).getEcoreElement());
    			} else{
    				if(i.getEcoreElement() instanceof EAttribute){
	        			eme2 = EcoreFactory.eINSTANCE.createEAttribute();
	        			eme2 = (EAttribute) i.getEcoreElement();        				        				        				            			
        			}  
    			}
    			
        		// assign the container metaclass as the referred element
    			contClass = (EClass)this.getInitializerByID(((String)i.getReferredElementId())).getEcoreElement();		        			        			      	
        		ease.setElement(contClass);        		
        		upd.setReferredElement(ease);
    			
    		}
    		
    		NewAbstractSyntaxElement nase = HistoryFactory.eINSTANCE.createNewAbstractSyntaxElement();    			
			nase.setElement(eme2);    		        			
			upd.setTarget(nase);
			
			upd.setSolution(solution);	
			upds.add(upd);
    	}
    	
		for(ChangeTraceInheritanceItem i : this.getInheritanceAdds()){
    		Update upd = HistoryFactory.eINSTANCE.createUpdate();
    		
    		ExistingAbstractSyntaxElement ease = HistoryFactory.eINSTANCE.createExistingAbstractSyntaxElement();
    		ease.setElement(this.getInitializerByID(i.getSource().getId()).getEcoreElement().eClass().getEStructuralFeature("eSuperTypes"));    		
    		upd.setReferredElement(ease);
    		    		    		
    		EClass eSource = (EClass)this.getInitializerByID(i.getSource().getId()).getEcoreElement();    		
    		
    		if(this.getInitializerByID(i.getSource().getId()).isLoad()){
    			ExistingAbstractSyntaxElement ease2 = HistoryFactory.eINSTANCE.createExistingAbstractSyntaxElement();
    			ease2.setElement(eSource);
    			upd.setSource(ease2);
    		}else{
    			NewAbstractSyntaxElement ease2 = HistoryFactory.eINSTANCE.createNewAbstractSyntaxElement();
    			ease2.setElement(eSource);
    			upd.setSource(ease2);
    		}
    		
    		EClass eTarget = (EClass)this.getInitializerByID(i.getTarget().getId()).getEcoreElement();
    		
    		if(this.getInitializerByID(i.getTarget().getId()).isLoad()){
    			ExistingAbstractSyntaxElement ease2 = HistoryFactory.eINSTANCE.createExistingAbstractSyntaxElement();
    			ease2.setElement(eTarget);
    			upd.setTarget(ease2);
    		}else{    			
    			NewAbstractSyntaxElement ease2 = HistoryFactory.eINSTANCE.createNewAbstractSyntaxElement();    			
    			ease2.setElement(eTarget);
    			upd.setTarget(ease2);
    		}
    		
    		upd.setSolution(solution);
    		upds.add(upd);
    	}
		
    	return upds;
	}
	
}
