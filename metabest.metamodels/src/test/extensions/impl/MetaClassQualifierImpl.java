/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.extensions.impl;

import org.eclipse.emf.ecore.EClass;
import test.extensions.ExtensionsPackage;
import test.extensions.MetaClassQualifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Class Qualifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class MetaClassQualifierImpl extends QualifierImpl implements MetaClassQualifier {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetaClassQualifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExtensionsPackage.Literals.META_CLASS_QUALIFIER;
	}

} //MetaClassQualifierImpl
