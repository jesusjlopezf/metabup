/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.expressions.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import test.checks.expressions.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExpressionsFactoryImpl extends EFactoryImpl implements ExpressionsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExpressionsFactory init() {
		try {
			ExpressionsFactory theExpressionsFactory = (ExpressionsFactory)EPackage.Registry.INSTANCE.getEFactory(ExpressionsPackage.eNS_URI);
			if (theExpressionsFactory != null) {
				return theExpressionsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ExpressionsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ExpressionsPackage.OBJECT_TYPE_EXPRESSION: return createObjectTypeExpression();
			case ExpressionsPackage.FEATURE_TYPE_EXPRESSION: return createFeatureTypeExpression();
			case ExpressionsPackage.MULTIPLICITY_EXPRESSION: return createMultiplicityExpression();
			case ExpressionsPackage.FEATURE_NATURE_EXPRESSION: return createFeatureNatureExpression();
			case ExpressionsPackage.FEATURE_WITHIN_OBJECT_EXPRESSION: return createFeatureWithinObjectExpression();
			case ExpressionsPackage.OBJECT_EXPRESSION: return createObjectExpression();
			case ExpressionsPackage.META_CLASS_EXPRESSION: return createMetaClassExpression();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectTypeExpression createObjectTypeExpression() {
		ObjectTypeExpressionImpl objectTypeExpression = new ObjectTypeExpressionImpl();
		return objectTypeExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureTypeExpression createFeatureTypeExpression() {
		FeatureTypeExpressionImpl featureTypeExpression = new FeatureTypeExpressionImpl();
		return featureTypeExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityExpression createMultiplicityExpression() {
		MultiplicityExpressionImpl multiplicityExpression = new MultiplicityExpressionImpl();
		return multiplicityExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureNatureExpression createFeatureNatureExpression() {
		FeatureNatureExpressionImpl featureNatureExpression = new FeatureNatureExpressionImpl();
		return featureNatureExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureWithinObjectExpression createFeatureWithinObjectExpression() {
		FeatureWithinObjectExpressionImpl featureWithinObjectExpression = new FeatureWithinObjectExpressionImpl();
		return featureWithinObjectExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectExpression createObjectExpression() {
		ObjectExpressionImpl objectExpression = new ObjectExpressionImpl();
		return objectExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaClassExpression createMetaClassExpression() {
		MetaClassExpressionImpl metaClassExpression = new MetaClassExpressionImpl();
		return metaClassExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionsPackage getExpressionsPackage() {
		return (ExpressionsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ExpressionsPackage getPackage() {
		return ExpressionsPackage.eINSTANCE;
	}

} //ExpressionsFactoryImpl
