/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.lib;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import metabup.annotations.catalog.refactoring.General;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.general.AssistanceTip;
import metabup.lexicon.de.colibri.lib.Concept;
import metabup.lexicon.de.colibri.lib.HybridLattice;
import metabup.lexicon.de.colibri.lib.Lattice;
import metabup.lexicon.de.colibri.lib.Relation;
import metabup.lexicon.de.colibri.lib.Traversal;
import metabup.lexicon.de.colibri.lib.TreeRelation;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Attribute;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;

public class PullUpFeaturesAssistance extends AssistanceTip implements IAssistanceTip {	
	public PullUpFeaturesAssistance(){
		super();
		super.setName("Pull up common features to general MetaClass");
		this.setAnnotation(new General());
	}
	
	public boolean calculateSolutions() {
		/* 
		 * this might be parametrized: 
		 * 		- Traversal.TOP_OBJSIZE will priorize concepts having more objects
		 * 		- Traversal.BOTTOM_ATTRSIZE will priorize concepts having more attributes
		 * 		- ...
		 */				
		
		calculateFCASolution(Traversal.TOP_OBJSIZE);
		calculateFCASolution(Traversal.TOP_ATTRSIZE);
		calculateFCASolution(Traversal.TOP_ATTR);
		calculateFCASolution(Traversal.TOP_DEPTHFIRST);
		calculateFCASolution(Traversal.TOP_OBJ);		
		calculateFCASolution(Traversal.BOTTOM_ATTR);
		calculateFCASolution(Traversal.BOTTOM_ATTRSIZE);
		calculateFCASolution(Traversal.BOTTOM_DEPTHFIRST);
		calculateFCASolution(Traversal.BOTTOM_OBJ);
		calculateFCASolution(Traversal.BOTTOM_OBJSIZE);
		
		
		if(super.solutions.isEmpty()) return false;
		else return true;
	}
	
	private void calculateFCASolution(Traversal mode){
		Relation rel = new TreeRelation();
		List<MetaClass> mcs = this.getTargetMetaModel().getClasses();
		
		for(MetaClass mc : mcs){
			for(Feature f : mc.getFeatures()){
				String attributeToken = f.getName();
				
				/* introduce a string token to make sure the feature are exactly the same */
				if(f instanceof Attribute){
					attributeToken += "-att";
					if(((Attribute)f).getPrimitiveType() != null) attributeToken += "-" + ((Attribute)f).getPrimitiveType(); 
				}else{
					if(f instanceof Reference){
						Reference r = (Reference)f;
						attributeToken += "-ref";
						if(r.getReference() != null) attributeToken += "-" + r.getReference().getName() + "-" + r.getMin() + "-" + r.getMax();												
					}
				}
				
				rel.add(mc.getName(), attributeToken);
				////System.out.println("TOKEN: "+ mc.getName() + ", " + attributeToken);
			}
		}	
		
		Lattice lattice = new HybridLattice(rel);
		Iterator<Concept> it = null;
		
		it = lattice.conceptIterator(mode);
		
		if(it.hasNext()){
			Concept c = it.next();
			
			List<AnnotatedElement> set = new ArrayList<AnnotatedElement>();
			
			if(c.getObjects() != null && !c.getAttributes().isEmpty() && c.getObjects().size() > 1){			
				for(Object o : c.getObjects()){
					MetaClass mc = null;
					if((mc = super.getMetaClassByName((String)o)) != null) set.add(mc);
				}
								
				String message = "The MetaClasses: {";
				
				for(AnnotatedElement ae : set){
					MetaClass mc = (MetaClass)ae;
					message += "\'"+ mc.getName() +"\', ";				
				}
				
				message = message.substring(0, message.lastIndexOf("\', "));
				message += "} are suitable to be generalized as they share a set of features";
				
				if(super.getSolution(message) == null) super.putSolution(message, set, new General());
			}
		}
	}

	
}
