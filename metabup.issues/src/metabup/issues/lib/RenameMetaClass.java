/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.issues.lib;

import org.eclipse.jface.dialogs.InputDialog;

import metabup.issues.general.categories.Automatic;
import metabup.metamodel.MetaClass;

public class RenameMetaClass extends Automatic {
	public RenameMetaClass(MetaClass mc){
		if(mc != null){
			super.elements.add(mc);
			super.setDescription("Rename MetaClass suggested" + mc.getName());
		}		
	}
	
	@Override
	public void run() {
		MetaClass mc = (MetaClass)elements.get(0);		
		InputDialog id = new InputDialog(null, "Rename MetaClass", "Type a new name for " + mc.getName(), mc.getName(), null);		
		if(id.open() == InputDialog.OK) mc.setName(id.getValue());			
	}

}
