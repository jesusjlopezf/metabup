/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.constraint;

import java.util.ArrayList;
import java.util.List;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.Object;
import metabup.annotations.extensionpoint.ConstraintAnnotation;
import metabup.annotations.extensionpoint.DesignAnnotation;
import metabup.issues.general.IOpenIssue;
import metabup.issues.lib.ViolatedTreeStructure;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Reference;

public class Tree extends ConstraintAnnotation {
	public Tree (){
		this.setName("tree");
		this.setDescription("The reference spans a tree");
	}
	
	@Override
	public List<IOpenIssue> detectFragmentIssues(FragmentModel fm, AnnotatedElement ae){
		if(fm == null || ae == null || fm.getFragments() == null || fm.getFragments().isEmpty()) return null;
		List<IOpenIssue> issues = new ArrayList<IOpenIssue>();
		
		if(ae instanceof Reference){			
			Reference mmr = (Reference) ae;
			if(mmr.getReference() == null) return null;
			String name = mmr.getName();		
			
			List<fragments.Reference> visited = new ArrayList<fragments.Reference>();						
			List<fragments.Reference> trouble = new ArrayList<fragments.Reference>();
			
			for(Fragment f : fm.getFragments()){
				for(Object o : f.getObjects()){
					for(fragments.Reference fr : getReferences(o)){												
						if(!visited.contains(fr) && name.equals(fr.getName())){
							List<fragments.Object> nodes = new ArrayList<fragments.Object>();							
							isTree(fr, nodes, visited, trouble);								
						}
					}
				}
			}
			
			if(!trouble.isEmpty()){
				ViolatedTreeStructure vts = new ViolatedTreeStructure(mmr, trouble);
				issues.add(vts);
				
				return issues;
			}			
		}
		
		return null;
	}
	
	private boolean isTree(fragments.Reference r, List<fragments.Object> nodes, List<fragments.Reference> visited, List<fragments.Reference> trouble){
		visited.add(r);
		nodes.add((Object)r.eContainer());
		
		for(fragments.Object o : r.getReference()){
			if(nodes.contains(o)){
				if(!trouble.contains(r)) trouble.add(r);
			}
			else{				
				for(fragments.Reference or : getReferences(o)){
					return isTree(or, nodes, visited, trouble);
				}
			}
		}
		
		if(trouble.isEmpty()) return true;
		else return false;
	}
	
	private List<fragments.Reference> getReferences(Object o){
		List<fragments.Reference> references = new ArrayList<fragments.Reference>();
		
		for(fragments.Feature f : o.getFeatures())
			if(f instanceof fragments.Reference)
				references.add((fragments.Reference)f);
		
		return references;
	}

	@Override
	public void updateConstraints() {
		// TODO Auto-generated method stub
		
	}
}
