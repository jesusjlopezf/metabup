/****************************************************************************
 * This demo file is part of the yFiles extension package ySVG-2.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * ySVG demo files exhibit ySVG functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a ySVG version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own ySVG powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the ySVG
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.svg;

import yext.svg.view.SVGNodeRealizer;

import y.base.Edge;
import y.base.Node;
import y.base.NodeCursor;
import y.geom.OrientedRectangle;
import y.geom.YRectangle;
import y.layout.EdgeLabelModel;
import y.option.RealizerCellRenderer;
import y.view.DropSupport;
import y.view.EdgeLabel;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import y.view.Graph2DView;
import y.view.NodeRealizer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


/**
 * Demonstrates how to display SVG documents using {@link SVGNodeRealizer}.
 * @see <a href="http://docs.yworks.com/yfiles/doc/developers-guide/svg_usage.html#svg_content">Section Using the ySVG Extension Package</a> in the yFiles for Java Developer's Guide
 */
public class SVGNodeRealizerDemo extends SVGExportDemo {
  /**
   * Initializes a new instance of <code>SVGNodeRealizerDemo</code>.
   * A sample graph displaying a couple of SVG nodes is created and
   * laid out.
   */
  public SVGNodeRealizerDemo() {
    // create a graph with a couple of nodes that display SVG icons
    createGraph();

    // notify the view of the changes made to its graph
    view.fitContent();
    view.updateView();

    // some gui stuff to setup a template list for SVGNodeRealizers
    createContent();
  }

  /**
   * Creates the GUI components.
   */
  private void createContent() {
    final String[] tmp = {
            "camera.svg",
            "cdrom.svg",
            "computer.svg",
            "floppy.svg",
            "harddisk.svg",
            "ipod.svg",
            "network.svg",
            "printer.svg",
            "scanner.svg",
            "zipdisk.svg"
    };
    final ArrayList realizers = new ArrayList();
    for (int i = 0; i < tmp.length; ++i) {
      final SVGNodeRealizer snr = createSvgRealizer(tmp[i]);
      if (snr.getSVGURL() != null) {
        realizers.add(snr);
      }
    }

    if (realizers.isEmpty()) {
      JOptionPane.showMessageDialog(
              this,
              "Could not find SVG resources.",
              "Warning",
              JOptionPane.WARNING_MESSAGE);
      System.err.println("Could not find SVG resources.");
      return;
    }

    final DragAndDropSupport templates = new DragAndDropSupport(realizers, view);

    remove(view);
    add(new JSplitPane(
            JSplitPane.HORIZONTAL_SPLIT,
            true,
            new JScrollPane(templates.getList()),
            view),
        BorderLayout.CENTER);
  }

  /**
   * Creates the initial graph structure.
   */
  private void createGraph() {
    final Graph2D graph = view.getGraph2D();
    final Node computer = graph.createNode(createSvgRealizer("computer.svg"));
    final Node cdrom = graph.createNode(createSvgRealizer("cdrom.svg"));
    final Node hd = graph.createNode(createSvgRealizer("harddisk.svg"));
    final Node printer = graph.createNode(createSvgRealizer("printer.svg"));
    final Node scanner = graph.createNode(createSvgRealizer("scanner.svg"));
    final Node ipod = graph.createNode(createSvgRealizer("ipod.svg"));

    // ... graphs without edges are boring
    final Edge hasCdrom = graph.createEdge(computer, cdrom);
    final Edge hasHd = graph.createEdge(computer, hd);
    final Edge hasPrinter = graph.createEdge(computer, printer);
    final Edge hasScanner = graph.createEdge(computer, scanner);
    graph.createEdge(computer, ipod);

    // layout the graph (in a very simple manner) for improved viewing pleasure
    double maxW = 0;
    double maxH = 0;

    NodeRealizer nr;
    for (NodeCursor nc = graph.nodes(); nc.ok(); nc.next()) {
      nr = graph.getRealizer(nc.node());
      if (maxW < nr.getWidth()) {
        maxW = nr.getWidth();
      }
      if (maxH < nr.getHeight()) {
        maxH = nr.getHeight();
      }
    }

    nr = graph.getRealizer(computer);
    nr.setCenter(0.0, 0.0);

    nr = graph.getRealizer(cdrom);
    nr.setCenter(maxW, 3 * maxH);
    nr = graph.getRealizer(hd);
    nr.setCenter(-maxW, 3 * maxH);

    nr = graph.getRealizer(printer);
    nr.setCenter(3 * maxW, maxH);
    nr = graph.getRealizer(scanner);
    nr.setCenter(3 * maxW, -maxH);

    nr = graph.getRealizer(ipod);
    nr.setCenter(-3 * maxW, 0.0);

    EdgeRealizer er;
    er = graph.getRealizer(hasCdrom);
    er.appendBend(0.0, 2 * maxH);
    er.appendBend(maxW, 2 * maxH);

    er = graph.getRealizer(hasHd);
    er.appendBend(0.0, 2 * maxH);
    er.appendBend(-maxW, 2 * maxH);
    er.setLabelText("Storage Devices");
    double[] size;
    size = getLabelSize(er);
    setLabelPosition(er, -size[0] * 0.5, 2 * maxH + 5);

    er = graph.getRealizer(hasPrinter);
    er.appendBend(2 * maxW, 0.0);
    er.appendBend(2 * maxW, maxH);

    er = graph.getRealizer(hasScanner);
    er.appendBend(2 * maxW, 0.0);
    er.appendBend(2 * maxW, -maxH);
    er.setLabelText("Periphery");
    size = getLabelSize(er);
    setLabelPosition(er, 2 * maxW - size[0] - 5, -size[1] - 5);
  }

  /**
   * Determines the size of the default edge label associated with the
   * specified edge realizer. Returns an array of length 2, with the first
   * item being label width and the second item label height.
   */
  private static double[] getLabelSize( final EdgeRealizer er ) {
    final EdgeLabel label = er.getLabel();
    final YRectangle bounds = label.getBox();
    final double w = bounds.getWidth();
    final double h = bounds.getHeight();
    return new double[]{w, h};
  }

  /**
   * Specifies the position (i.e. coordinates of the upper left corner) for
   * the default edge label of the specified edge realizer.
   */
  private static void setLabelPosition(
          final EdgeRealizer er, final double x, final double y
  ) {
    final Edge edge = er.getEdge();
    final Graph2D graph = (Graph2D) edge.getGraph();
    final EdgeLabel label = er.getLabel();

    final YRectangle oldBounds = label.getBox();
    final double h = oldBounds.getHeight();
    final OrientedRectangle newBounds =
            new OrientedRectangle(x, y + h, oldBounds.getWidth(), h);

    label.setModel(EdgeLabel.FREE);
    final EdgeLabelModel model = label.getLabelModel();
    final Object param =
            model.createModelParameter(newBounds, er,
                    graph.getRealizer(edge.source()),
                    graph.getRealizer(edge.target()));
    label.setModelParameter(param);
  }

  /**
   * Creates a new <code>SVGNodeRealizer</code> displaying the icon at
   * <code>"resource/svg/" + icon</code>.
   */
  private static SVGNodeRealizer createSvgRealizer( final String icon ) {
    URL url = null;
    try {
      url = SVGNodeRealizerDemo.class.getResource("resource/svg/" + icon);
    } catch (Exception ex) {
      System.err.println("Can't find SVG resource resource/svg/" + icon);
      ex.printStackTrace(System.err);
      url = null;
    }

    if (url != null) {
      final SVGNodeRealizer nr = new SVGNodeRealizer(url);
      // using the visual bounds ensures that edges are not clipped at the
      // node's rectangular bounding box but rather at the visual bounds of
      // the displayed vector graphic
      nr.setUsingVisualBounds(true);
      return nr;
    } else {
      final SVGNodeRealizer nr = new SVGNodeRealizer();
      nr.setLabelText(icon);
      return nr;
    }
  }


  /**
   * Launches this demo.
   * @param args ignored.
   */
  public static void main( final String[] args ) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        initLnF();
        (new SVGNodeRealizerDemo()).start();
      }
    });
  }


  /*
   * #####################################################################
   * nested types
   * #####################################################################
   */

  /**
   * Support class that be used to create a JList that contains NodeRealizers
   * that can be dragged and dropped onto the given Graph2DView object.
   */
  public static class DragAndDropSupport {
    private final JList realizerList;
    private final DropSupport dropSupport;

    protected DragAndDropSupport( Collection nodeRealizers, final Graph2DView view ) {
      this(nodeRealizers.toArray(), view);
    }

    protected DragAndDropSupport( Object[] nodeRealizers, final Graph2DView view ) {
      dropSupport = new DropSupport(view);
      dropSupport.setPreviewEnabled(true);


      // create a nice GUI for displaying NodeRealizers
      final Dimension size = preferredSize(nodeRealizers);
      realizerList = new JList(nodeRealizers);
      realizerList.setCellRenderer(new RealizerCellRenderer(size.width, size.height));

      // set the currently selected NodeRealizer as default nodeRealizer
      realizerList.addListSelectionListener(new ListSelectionListener() {
        public void valueChanged( final ListSelectionEvent e ) {
          if (realizerList.getSelectedValue() instanceof NodeRealizer) {
            nodeRealizerSelected(view);
          }
        }
      });

      realizerList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      realizerList.setSelectedIndex(0);


      // define the realizer list to be the drag source
      // use the string-valued name of the realizer as transferable
      final DragSource dragSource = new DragSource();
      dragSource.createDefaultDragGestureRecognizer(realizerList, DnDConstants.ACTION_MOVE,
          new DragGestureListener() {
            public void dragGestureRecognized( final DragGestureEvent e ) {
              final Object value = realizerList.getSelectedValue();
              if (value instanceof NodeRealizer) {
                NodeRealizer nr = (NodeRealizer) value;
                // use the drop support class to initialize the drag and drop operation.
                dropSupport.startDrag(dragSource, nr, e, DragSource.DefaultMoveDrop);
              }
            }
          });
    }

    /**
     * Callback method that is triggered whenever the selection changes in the JList.
     * This method sets the given NodeRealizer as the view's graph default node realizer.
     */
    protected void nodeRealizerSelected( final Graph2DView view ) {
      view.getGraph2D().setDefaultNodeRealizer((NodeRealizer) realizerList.getSelectedValue());
    }

    /**
     * Return the JList that has been configured by this support class.
     */
    public JList getList() {
      return realizerList;
    }

    private static Dimension preferredSize( final Object[] nodeRealizers ) {
      double maxW = 90;
      double maxH = 60;
      for (int i = 0; i < nodeRealizers.length; ++i) {
        if (nodeRealizers[i] instanceof NodeRealizer) {
          final double w = ((NodeRealizer) nodeRealizers[i]).getWidth();
          if (maxW < w) {
            maxW = w;
          }
          final double h = ((NodeRealizer) nodeRealizers[i]).getHeight();
          if (maxH < h) {
            maxH = h;
          }
        }
      }
      final int w = (int) Math.ceil(maxW);
      final int h = (int) Math.ceil(maxH);
      return new Dimension(w + 10 - (w % 10), h + 5 - (h % 5));
    }
  }
}
