import java.util.List;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaClass;
import metabup.suggestions.extensions.SuggestionAction;

public class UnsuffixSuggestionAction extends SuggestionAction {

	public UnsuffixSuggestionAction() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void execute(List<AnnotatedElement> annotatedElements) {
		MetaClass mc = (MetaClass)annotatedElements.get(0);		
		mc.setName(mc.getName().substring(0, mc.getName().indexOf("Type")));
	}

}
