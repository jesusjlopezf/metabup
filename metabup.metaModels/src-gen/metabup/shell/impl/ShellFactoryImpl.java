/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package metabup.shell.impl;

import metabup.shell.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ShellFactoryImpl extends EFactoryImpl implements ShellFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ShellFactory init() {
		try {
			ShellFactory theShellFactory = (ShellFactory)EPackage.Registry.INSTANCE.getEFactory(ShellPackage.eNS_URI);
			if (theShellFactory != null) {
				return theShellFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ShellFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShellFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ShellPackage.SHELL_MODEL: return createShellModel();
			case ShellPackage.FRAGMENT_COMMAND: return createFragmentCommand();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShellModel createShellModel() {
		ShellModelImpl shellModel = new ShellModelImpl();
		return shellModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FragmentCommand createFragmentCommand() {
		FragmentCommandImpl fragmentCommand = new FragmentCommandImpl();
		return fragmentCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShellPackage getShellPackage() {
		return (ShellPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ShellPackage getPackage() {
		return ShellPackage.eINSTANCE;
	}

} //ShellFactoryImpl
