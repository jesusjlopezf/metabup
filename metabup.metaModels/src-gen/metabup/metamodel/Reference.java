/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.metamodel;

import graphicProperties.EdgeProperties;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metabup.metamodel.Reference#getReference <em>Reference</em>}</li>
 *   <li>{@link metabup.metamodel.Reference#getGraphicProperties <em>Graphic Properties</em>}</li>
 * </ul>
 *
 * @see metabup.metamodel.MetamodelPackage#getReference()
 * @model
 * @generated
 */
public interface Reference extends Feature {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' reference.
	 * @see #setReference(MetaClass)
	 * @see metabup.metamodel.MetamodelPackage#getReference_Reference()
	 * @model
	 * @generated
	 */
	MetaClass getReference();

	/**
	 * Sets the value of the '{@link metabup.metamodel.Reference#getReference <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' reference.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(MetaClass value);
	
	/**
	 * Returns the value of the '<em><b>Graphic Properties</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Graphic Properties</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Graphic Properties</em>' containment reference.
	 * @see #setGraphicProperties(EdgeProperties)
	 * @see metabup.metamodel.MetamodelPackage#getReference_GraphicProperties()
	 * @model containment="true"
	 * @generated
	 */
	EdgeProperties getGraphicProperties();

	/**
	 * Sets the value of the '{@link metabup.metamodel.Reference#getGraphicProperties <em>Graphic Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Graphic Properties</em>' containment reference.
	 * @see #getGraphicProperties()
	 * @generated
	 */
	void setGraphicProperties(EdgeProperties value);

	public Reference getOpposite();
	public String toString();
} // Reference
