/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.wizards;

import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one (mbup).
 */

public class NewMetaBupProjectFromEcoreWizardPage extends WizardPage {
	private ISelection selection;
	private Text ecoreFileText;

	
	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param pageName
	 */
	public NewMetaBupProjectFromEcoreWizardPage(ISelection selection) {
		super("wizardPage");
		setTitle("Select an Ecore file");
		setDescription("Please select an Ecore file from which the new metaBup meta-model should be created.");
		this.selection = selection;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		final Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;		
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		
		Label label = new Label(container, SWT.NULL);
		label.setText("Ecore file:");
	    
	    ecoreFileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		ecoreFileText.setLayoutData(gd);		
		ecoreFileText.setEnabled(true);
				
		final Button button4 = new Button(container, SWT.PUSH);
		button4.setText("Browse...");
		button4.setEnabled(true);
		button4.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				handleFragmentLegendBrowse(container);
			}
		});
		
		//initialize();
		dialogChanged();
		setControl(container);
	}
	
	
	protected void handleFragmentLegendBrowse(Composite container) {
		FileDialog dialog = new FileDialog(container.getShell());
		dialog.setText("Select a metabup.sketches.legend file...");
		dialog.setFilterExtensions(new String[] { "*.ecore" } );
		String fileName = dialog.open();
		
		if(fileName != null){
			Path path = new Path(fileName);
			ecoreFileText.setText(path.toOSString());
		}			
	}

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {			
		String fileName = getEcoreFile();
		
		if (fileName.length() == 0) {
			//updateStatus("File name must be specified");
			return;
		}
		
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}
	
	public String getEcoreFile(){
		return ecoreFileText.getText();
	}
}