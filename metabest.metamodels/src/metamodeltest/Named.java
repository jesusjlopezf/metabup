/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Named</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.Named#getCase <em>Case</em>}</li>
 *   <li>{@link metamodeltest.Named#getName <em>Name</em>}</li>
 *   <li>{@link metamodeltest.Named#getSize <em>Size</em>}</li>
 *   <li>{@link metamodeltest.Named#getWordNature <em>Word Nature</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getNamed()
 * @model
 * @generated
 */
public interface Named extends ElementQualifier {
	/**
	 * Returns the value of the '<em><b>Case</b></em>' attribute.
	 * The literals are from the enumeration {@link metamodeltest.Case}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Case</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Case</em>' attribute.
	 * @see metamodeltest.Case
	 * @see #setCase(Case)
	 * @see metamodeltest.MetamodeltestPackage#getNamed_Case()
	 * @model
	 * @generated
	 */
	Case getCase();

	/**
	 * Sets the value of the '{@link metamodeltest.Named#getCase <em>Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Case</em>' attribute.
	 * @see metamodeltest.Case
	 * @see #getCase()
	 * @generated
	 */
	void setCase(Case value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' containment reference.
	 * @see #setName(StringParameter)
	 * @see metamodeltest.MetamodeltestPackage#getNamed_Name()
	 * @model containment="true"
	 * @generated
	 */
	StringParameter getName();

	/**
	 * Sets the value of the '{@link metamodeltest.Named#getName <em>Name</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' containment reference.
	 * @see #getName()
	 * @generated
	 */
	void setName(StringParameter value);

	/**
	 * Returns the value of the '<em><b>Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' containment reference.
	 * @see #setSize(Length)
	 * @see metamodeltest.MetamodeltestPackage#getNamed_Size()
	 * @model containment="true"
	 * @generated
	 */
	Length getSize();

	/**
	 * Sets the value of the '{@link metamodeltest.Named#getSize <em>Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' containment reference.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(Length value);

	/**
	 * Returns the value of the '<em><b>Word Nature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Word Nature</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Word Nature</em>' containment reference.
	 * @see #setWordNature(WordNature)
	 * @see metamodeltest.MetamodeltestPackage#getNamed_WordNature()
	 * @model containment="true"
	 * @generated
	 */
	WordNature getWordNature();

	/**
	 * Sets the value of the '{@link metamodeltest.Named#getWordNature <em>Word Nature</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Word Nature</em>' containment reference.
	 * @see #getWordNature()
	 * @generated
	 */
	void setWordNature(WordNature value);

} // Named
