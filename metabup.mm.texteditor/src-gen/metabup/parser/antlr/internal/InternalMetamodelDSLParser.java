/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import metabup.services.MetamodelDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMetamodelDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'metamodel'", "'class'", "'<'", "','", "'{'", "';'", "'}'", "'attr'", "':'", "'['", "'..'", "']'", "'ref'", "'-'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMetamodelDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMetamodelDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMetamodelDSLParser.tokenNames; }
    public String getGrammarFileName() { return "../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g"; }



     	private MetamodelDSLGrammarAccess grammarAccess;
     	
        public InternalMetamodelDSLParser(TokenStream input, MetamodelDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "MetaModel";	
       	}
       	
       	@Override
       	protected MetamodelDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleMetaModel"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:67:1: entryRuleMetaModel returns [EObject current=null] : iv_ruleMetaModel= ruleMetaModel EOF ;
    public final EObject entryRuleMetaModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMetaModel = null;


        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:68:2: (iv_ruleMetaModel= ruleMetaModel EOF )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:69:2: iv_ruleMetaModel= ruleMetaModel EOF
            {
             newCompositeNode(grammarAccess.getMetaModelRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaModel_in_entryRuleMetaModel75);
            iv_ruleMetaModel=ruleMetaModel();

            state._fsp--;

             current =iv_ruleMetaModel; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMetaModel"


    // $ANTLR start "ruleMetaModel"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:76:1: ruleMetaModel returns [EObject current=null] : ( () otherlv_1= 'metamodel' ( (lv_name_2_0= ruleEString ) ) ( (lv_classes_3_0= ruleMetaClass ) ) ( (lv_classes_4_0= ruleMetaClass ) )* ) ;
    public final EObject ruleMetaModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_classes_3_0 = null;

        EObject lv_classes_4_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:79:28: ( ( () otherlv_1= 'metamodel' ( (lv_name_2_0= ruleEString ) ) ( (lv_classes_3_0= ruleMetaClass ) ) ( (lv_classes_4_0= ruleMetaClass ) )* ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:80:1: ( () otherlv_1= 'metamodel' ( (lv_name_2_0= ruleEString ) ) ( (lv_classes_3_0= ruleMetaClass ) ) ( (lv_classes_4_0= ruleMetaClass ) )* )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:80:1: ( () otherlv_1= 'metamodel' ( (lv_name_2_0= ruleEString ) ) ( (lv_classes_3_0= ruleMetaClass ) ) ( (lv_classes_4_0= ruleMetaClass ) )* )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:80:2: () otherlv_1= 'metamodel' ( (lv_name_2_0= ruleEString ) ) ( (lv_classes_3_0= ruleMetaClass ) ) ( (lv_classes_4_0= ruleMetaClass ) )*
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:80:2: ()
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:81:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getMetaModelAccess().getMetaModelAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,11,FollowSets000.FOLLOW_11_in_ruleMetaModel131); 

                	newLeafNode(otherlv_1, grammarAccess.getMetaModelAccess().getMetamodelKeyword_1());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:90:1: ( (lv_name_2_0= ruleEString ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:91:1: (lv_name_2_0= ruleEString )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:91:1: (lv_name_2_0= ruleEString )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:92:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getMetaModelAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleMetaModel152);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMetaModelRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:108:2: ( (lv_classes_3_0= ruleMetaClass ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:109:1: (lv_classes_3_0= ruleMetaClass )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:109:1: (lv_classes_3_0= ruleMetaClass )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:110:3: lv_classes_3_0= ruleMetaClass
            {
             
            	        newCompositeNode(grammarAccess.getMetaModelAccess().getClassesMetaClassParserRuleCall_3_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleMetaClass_in_ruleMetaModel173);
            lv_classes_3_0=ruleMetaClass();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMetaModelRule());
            	        }
                   		add(
                   			current, 
                   			"classes",
                    		lv_classes_3_0, 
                    		"MetaClass");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:126:2: ( (lv_classes_4_0= ruleMetaClass ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:127:1: (lv_classes_4_0= ruleMetaClass )
            	    {
            	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:127:1: (lv_classes_4_0= ruleMetaClass )
            	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:128:3: lv_classes_4_0= ruleMetaClass
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMetaModelAccess().getClassesMetaClassParserRuleCall_4_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleMetaClass_in_ruleMetaModel194);
            	    lv_classes_4_0=ruleMetaClass();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMetaModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"classes",
            	            		lv_classes_4_0, 
            	            		"MetaClass");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMetaModel"


    // $ANTLR start "entryRuleFeature"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:152:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:153:2: (iv_ruleFeature= ruleFeature EOF )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:154:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleFeature_in_entryRuleFeature231);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleFeature241); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:161:1: ruleFeature returns [EObject current=null] : (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        EObject this_Attribute_0 = null;

        EObject this_Reference_1 = null;


         enterRule(); 
            
        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:164:28: ( (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:165:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:165:1: (this_Attribute_0= ruleAttribute | this_Reference_1= ruleReference )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==18) ) {
                alt2=1;
            }
            else if ( (LA2_0==23) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:166:5: this_Attribute_0= ruleAttribute
                    {
                     
                            newCompositeNode(grammarAccess.getFeatureAccess().getAttributeParserRuleCall_0()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_ruleFeature288);
                    this_Attribute_0=ruleAttribute();

                    state._fsp--;

                     
                            current = this_Attribute_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:176:5: this_Reference_1= ruleReference
                    {
                     
                            newCompositeNode(grammarAccess.getFeatureAccess().getReferenceParserRuleCall_1()); 
                        
                    pushFollow(FollowSets000.FOLLOW_ruleReference_in_ruleFeature315);
                    this_Reference_1=ruleReference();

                    state._fsp--;

                     
                            current = this_Reference_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleMetaClass"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:192:1: entryRuleMetaClass returns [EObject current=null] : iv_ruleMetaClass= ruleMetaClass EOF ;
    public final EObject entryRuleMetaClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMetaClass = null;


        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:193:2: (iv_ruleMetaClass= ruleMetaClass EOF )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:194:2: iv_ruleMetaClass= ruleMetaClass EOF
            {
             newCompositeNode(grammarAccess.getMetaClassRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleMetaClass_in_entryRuleMetaClass350);
            iv_ruleMetaClass=ruleMetaClass();

            state._fsp--;

             current =iv_ruleMetaClass; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleMetaClass360); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMetaClass"


    // $ANTLR start "ruleMetaClass"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:201:1: ruleMetaClass returns [EObject current=null] : ( () otherlv_1= 'class' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '<' ( (otherlv_4= RULE_ID ) ) (otherlv_5= ',' ( (otherlv_6= RULE_ID ) ) )* )? otherlv_7= '{' ( ( (lv_features_8_0= ruleFeature ) ) (otherlv_9= ';' )? )* otherlv_10= '}' ) ;
    public final EObject ruleMetaClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_features_8_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:204:28: ( ( () otherlv_1= 'class' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '<' ( (otherlv_4= RULE_ID ) ) (otherlv_5= ',' ( (otherlv_6= RULE_ID ) ) )* )? otherlv_7= '{' ( ( (lv_features_8_0= ruleFeature ) ) (otherlv_9= ';' )? )* otherlv_10= '}' ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:205:1: ( () otherlv_1= 'class' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '<' ( (otherlv_4= RULE_ID ) ) (otherlv_5= ',' ( (otherlv_6= RULE_ID ) ) )* )? otherlv_7= '{' ( ( (lv_features_8_0= ruleFeature ) ) (otherlv_9= ';' )? )* otherlv_10= '}' )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:205:1: ( () otherlv_1= 'class' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '<' ( (otherlv_4= RULE_ID ) ) (otherlv_5= ',' ( (otherlv_6= RULE_ID ) ) )* )? otherlv_7= '{' ( ( (lv_features_8_0= ruleFeature ) ) (otherlv_9= ';' )? )* otherlv_10= '}' )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:205:2: () otherlv_1= 'class' ( (lv_name_2_0= ruleEString ) ) (otherlv_3= '<' ( (otherlv_4= RULE_ID ) ) (otherlv_5= ',' ( (otherlv_6= RULE_ID ) ) )* )? otherlv_7= '{' ( ( (lv_features_8_0= ruleFeature ) ) (otherlv_9= ';' )? )* otherlv_10= '}'
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:205:2: ()
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:206:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getMetaClassAccess().getMetaClassAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,12,FollowSets000.FOLLOW_12_in_ruleMetaClass406); 

                	newLeafNode(otherlv_1, grammarAccess.getMetaClassAccess().getClassKeyword_1());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:215:1: ( (lv_name_2_0= ruleEString ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:216:1: (lv_name_2_0= ruleEString )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:216:1: (lv_name_2_0= ruleEString )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:217:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getMetaClassAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleMetaClass427);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getMetaClassRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:233:2: (otherlv_3= '<' ( (otherlv_4= RULE_ID ) ) (otherlv_5= ',' ( (otherlv_6= RULE_ID ) ) )* )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==13) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:233:4: otherlv_3= '<' ( (otherlv_4= RULE_ID ) ) (otherlv_5= ',' ( (otherlv_6= RULE_ID ) ) )*
                    {
                    otherlv_3=(Token)match(input,13,FollowSets000.FOLLOW_13_in_ruleMetaClass440); 

                        	newLeafNode(otherlv_3, grammarAccess.getMetaClassAccess().getLessThanSignKeyword_3_0());
                        
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:237:1: ( (otherlv_4= RULE_ID ) )
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:238:1: (otherlv_4= RULE_ID )
                    {
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:238:1: (otherlv_4= RULE_ID )
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:239:3: otherlv_4= RULE_ID
                    {

                    			if (current==null) {
                    	            current = createModelElement(grammarAccess.getMetaClassRule());
                    	        }
                            
                    otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleMetaClass460); 

                    		newLeafNode(otherlv_4, grammarAccess.getMetaClassAccess().getSupersMetaClassCrossReference_3_1_0()); 
                    	

                    }


                    }

                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:250:2: (otherlv_5= ',' ( (otherlv_6= RULE_ID ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==14) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:250:4: otherlv_5= ',' ( (otherlv_6= RULE_ID ) )
                    	    {
                    	    otherlv_5=(Token)match(input,14,FollowSets000.FOLLOW_14_in_ruleMetaClass473); 

                    	        	newLeafNode(otherlv_5, grammarAccess.getMetaClassAccess().getCommaKeyword_3_2_0());
                    	        
                    	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:254:1: ( (otherlv_6= RULE_ID ) )
                    	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:255:1: (otherlv_6= RULE_ID )
                    	    {
                    	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:255:1: (otherlv_6= RULE_ID )
                    	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:256:3: otherlv_6= RULE_ID
                    	    {

                    	    			if (current==null) {
                    	    	            current = createModelElement(grammarAccess.getMetaClassRule());
                    	    	        }
                    	            
                    	    otherlv_6=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleMetaClass493); 

                    	    		newLeafNode(otherlv_6, grammarAccess.getMetaClassAccess().getSupersMetaClassCrossReference_3_2_1_0()); 
                    	    	

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,15,FollowSets000.FOLLOW_15_in_ruleMetaClass509); 

                	newLeafNode(otherlv_7, grammarAccess.getMetaClassAccess().getLeftCurlyBracketKeyword_4());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:271:1: ( ( (lv_features_8_0= ruleFeature ) ) (otherlv_9= ';' )? )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==18||LA6_0==23) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:271:2: ( (lv_features_8_0= ruleFeature ) ) (otherlv_9= ';' )?
            	    {
            	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:271:2: ( (lv_features_8_0= ruleFeature ) )
            	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:272:1: (lv_features_8_0= ruleFeature )
            	    {
            	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:272:1: (lv_features_8_0= ruleFeature )
            	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:273:3: lv_features_8_0= ruleFeature
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getMetaClassAccess().getFeaturesFeatureParserRuleCall_5_0_0()); 
            	    	    
            	    pushFollow(FollowSets000.FOLLOW_ruleFeature_in_ruleMetaClass531);
            	    lv_features_8_0=ruleFeature();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getMetaClassRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"features",
            	            		lv_features_8_0, 
            	            		"Feature");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }

            	    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:289:2: (otherlv_9= ';' )?
            	    int alt5=2;
            	    int LA5_0 = input.LA(1);

            	    if ( (LA5_0==16) ) {
            	        alt5=1;
            	    }
            	    switch (alt5) {
            	        case 1 :
            	            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:289:4: otherlv_9= ';'
            	            {
            	            otherlv_9=(Token)match(input,16,FollowSets000.FOLLOW_16_in_ruleMetaClass544); 

            	                	newLeafNode(otherlv_9, grammarAccess.getMetaClassAccess().getSemicolonKeyword_5_1());
            	                

            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            otherlv_10=(Token)match(input,17,FollowSets000.FOLLOW_17_in_ruleMetaClass560); 

                	newLeafNode(otherlv_10, grammarAccess.getMetaClassAccess().getRightCurlyBracketKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMetaClass"


    // $ANTLR start "entryRuleAttribute"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:305:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:306:2: (iv_ruleAttribute= ruleAttribute EOF )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:307:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleAttribute_in_entryRuleAttribute596);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleAttribute606); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:314:1: ruleAttribute returns [EObject current=null] : ( () otherlv_1= 'attr' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_primitiveType_4_0= ruleEString ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= ruleEInt ) ) otherlv_9= ']' ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token lv_min_6_0=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_primitiveType_4_0 = null;

        AntlrDatatypeRuleToken lv_max_8_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:317:28: ( ( () otherlv_1= 'attr' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_primitiveType_4_0= ruleEString ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= ruleEInt ) ) otherlv_9= ']' ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:318:1: ( () otherlv_1= 'attr' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_primitiveType_4_0= ruleEString ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= ruleEInt ) ) otherlv_9= ']' )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:318:1: ( () otherlv_1= 'attr' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_primitiveType_4_0= ruleEString ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= ruleEInt ) ) otherlv_9= ']' )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:318:2: () otherlv_1= 'attr' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (lv_primitiveType_4_0= ruleEString ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= ruleEInt ) ) otherlv_9= ']'
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:318:2: ()
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:319:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getAttributeAccess().getAttributeAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,18,FollowSets000.FOLLOW_18_in_ruleAttribute652); 

                	newLeafNode(otherlv_1, grammarAccess.getAttributeAccess().getAttrKeyword_1());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:328:1: ( (lv_name_2_0= ruleEString ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:329:1: (lv_name_2_0= ruleEString )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:329:1: (lv_name_2_0= ruleEString )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:330:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAttribute673);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleAttribute685); 

                	newLeafNode(otherlv_3, grammarAccess.getAttributeAccess().getColonKeyword_3());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:350:1: ( (lv_primitiveType_4_0= ruleEString ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:351:1: (lv_primitiveType_4_0= ruleEString )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:351:1: (lv_primitiveType_4_0= ruleEString )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:352:3: lv_primitiveType_4_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getPrimitiveTypeEStringParserRuleCall_4_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleAttribute706);
            lv_primitiveType_4_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		set(
                   			current, 
                   			"primitiveType",
                    		lv_primitiveType_4_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_5=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleAttribute718); 

                	newLeafNode(otherlv_5, grammarAccess.getAttributeAccess().getLeftSquareBracketKeyword_5());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:372:1: ( (lv_min_6_0= RULE_INT ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:373:1: (lv_min_6_0= RULE_INT )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:373:1: (lv_min_6_0= RULE_INT )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:374:3: lv_min_6_0= RULE_INT
            {
            lv_min_6_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleAttribute735); 

            			newLeafNode(lv_min_6_0, grammarAccess.getAttributeAccess().getMinINTTerminalRuleCall_6_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAttributeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"min",
                    		lv_min_6_0, 
                    		"INT");
            	    

            }


            }

            otherlv_7=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleAttribute752); 

                	newLeafNode(otherlv_7, grammarAccess.getAttributeAccess().getFullStopFullStopKeyword_7());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:394:1: ( (lv_max_8_0= ruleEInt ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:395:1: (lv_max_8_0= ruleEInt )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:395:1: (lv_max_8_0= ruleEInt )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:396:3: lv_max_8_0= ruleEInt
            {
             
            	        newCompositeNode(grammarAccess.getAttributeAccess().getMaxEIntParserRuleCall_8_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_ruleAttribute773);
            lv_max_8_0=ruleEInt();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getAttributeRule());
            	        }
                   		set(
                   			current, 
                   			"max",
                    		lv_max_8_0, 
                    		"EInt");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_9=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleAttribute785); 

                	newLeafNode(otherlv_9, grammarAccess.getAttributeAccess().getRightSquareBracketKeyword_9());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleReference"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:424:1: entryRuleReference returns [EObject current=null] : iv_ruleReference= ruleReference EOF ;
    public final EObject entryRuleReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReference = null;


        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:425:2: (iv_ruleReference= ruleReference EOF )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:426:2: iv_ruleReference= ruleReference EOF
            {
             newCompositeNode(grammarAccess.getReferenceRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReference_in_entryRuleReference821);
            iv_ruleReference=ruleReference();

            state._fsp--;

             current =iv_ruleReference; 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReference831); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReference"


    // $ANTLR start "ruleReference"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:433:1: ruleReference returns [EObject current=null] : ( () otherlv_1= 'ref' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= RULE_INT ) ) otherlv_9= ']' ) ;
    public final EObject ruleReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token lv_min_6_0=null;
        Token otherlv_7=null;
        Token lv_max_8_0=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;


         enterRule(); 
            
        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:436:28: ( ( () otherlv_1= 'ref' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= RULE_INT ) ) otherlv_9= ']' ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:437:1: ( () otherlv_1= 'ref' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= RULE_INT ) ) otherlv_9= ']' )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:437:1: ( () otherlv_1= 'ref' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= RULE_INT ) ) otherlv_9= ']' )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:437:2: () otherlv_1= 'ref' ( (lv_name_2_0= ruleEString ) ) otherlv_3= ':' ( (otherlv_4= RULE_ID ) ) otherlv_5= '[' ( (lv_min_6_0= RULE_INT ) ) otherlv_7= '..' ( (lv_max_8_0= RULE_INT ) ) otherlv_9= ']'
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:437:2: ()
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:438:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getReferenceAccess().getReferenceAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,23,FollowSets000.FOLLOW_23_in_ruleReference877); 

                	newLeafNode(otherlv_1, grammarAccess.getReferenceAccess().getRefKeyword_1());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:447:1: ( (lv_name_2_0= ruleEString ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:448:1: (lv_name_2_0= ruleEString )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:448:1: (lv_name_2_0= ruleEString )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:449:3: lv_name_2_0= ruleEString
            {
             
            	        newCompositeNode(grammarAccess.getReferenceAccess().getNameEStringParserRuleCall_2_0()); 
            	    
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_ruleReference898);
            lv_name_2_0=ruleEString();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getReferenceRule());
            	        }
                   		set(
                   			current, 
                   			"name",
                    		lv_name_2_0, 
                    		"EString");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_3=(Token)match(input,19,FollowSets000.FOLLOW_19_in_ruleReference910); 

                	newLeafNode(otherlv_3, grammarAccess.getReferenceAccess().getColonKeyword_3());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:469:1: ( (otherlv_4= RULE_ID ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:470:1: (otherlv_4= RULE_ID )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:470:1: (otherlv_4= RULE_ID )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:471:3: otherlv_4= RULE_ID
            {

            			if (current==null) {
            	            current = createModelElement(grammarAccess.getReferenceRule());
            	        }
                    
            otherlv_4=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleReference930); 

            		newLeafNode(otherlv_4, grammarAccess.getReferenceAccess().getReferenceMetaClassCrossReference_4_0()); 
            	

            }


            }

            otherlv_5=(Token)match(input,20,FollowSets000.FOLLOW_20_in_ruleReference942); 

                	newLeafNode(otherlv_5, grammarAccess.getReferenceAccess().getLeftSquareBracketKeyword_5());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:486:1: ( (lv_min_6_0= RULE_INT ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:487:1: (lv_min_6_0= RULE_INT )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:487:1: (lv_min_6_0= RULE_INT )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:488:3: lv_min_6_0= RULE_INT
            {
            lv_min_6_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleReference959); 

            			newLeafNode(lv_min_6_0, grammarAccess.getReferenceAccess().getMinINTTerminalRuleCall_6_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReferenceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"min",
                    		lv_min_6_0, 
                    		"INT");
            	    

            }


            }

            otherlv_7=(Token)match(input,21,FollowSets000.FOLLOW_21_in_ruleReference976); 

                	newLeafNode(otherlv_7, grammarAccess.getReferenceAccess().getFullStopFullStopKeyword_7());
                
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:508:1: ( (lv_max_8_0= RULE_INT ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:509:1: (lv_max_8_0= RULE_INT )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:509:1: (lv_max_8_0= RULE_INT )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:510:3: lv_max_8_0= RULE_INT
            {
            lv_max_8_0=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleReference993); 

            			newLeafNode(lv_max_8_0, grammarAccess.getReferenceAccess().getMaxINTTerminalRuleCall_8_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReferenceRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"max",
                    		lv_max_8_0, 
                    		"INT");
            	    

            }


            }

            otherlv_9=(Token)match(input,22,FollowSets000.FOLLOW_22_in_ruleReference1010); 

                	newLeafNode(otherlv_9, grammarAccess.getReferenceAccess().getRightSquareBracketKeyword_9());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReference"


    // $ANTLR start "entryRuleEString"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:538:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:539:2: (iv_ruleEString= ruleEString EOF )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:540:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEString_in_entryRuleEString1047);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEString1058); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:547:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;

         enterRule(); 
            
        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:550:28: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:551:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:551:1: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_STRING) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_ID) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:551:6: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FollowSets000.FOLLOW_RULE_STRING_in_ruleEString1098); 

                    		current.merge(this_STRING_0);
                        
                     
                        newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                        

                    }
                    break;
                case 2 :
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:559:10: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FollowSets000.FOLLOW_RULE_ID_in_ruleEString1124); 

                    		current.merge(this_ID_1);
                        
                     
                        newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEInt"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:574:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:575:2: (iv_ruleEInt= ruleEInt EOF )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:576:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEInt_in_entryRuleEInt1170);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEInt1181); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:583:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;

         enterRule(); 
            
        try {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:586:28: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:587:1: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:587:1: ( (kw= '-' )? this_INT_1= RULE_INT )
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:587:2: (kw= '-' )? this_INT_1= RULE_INT
            {
            // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:587:2: (kw= '-' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==24) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../metabup.mm.texteditor/src-gen/metabup/parser/antlr/internal/InternalMetamodelDSL.g:588:2: kw= '-'
                    {
                    kw=(Token)match(input,24,FollowSets000.FOLLOW_24_in_ruleEInt1220); 

                            current.merge(kw);
                            newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0()); 
                        

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FollowSets000.FOLLOW_RULE_INT_in_ruleEInt1237); 

            		current.merge(this_INT_1);
                
             
                newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1()); 
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleMetaModel_in_entryRuleMetaModel75 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaModel85 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_11_in_ruleMetaModel131 = new BitSet(new long[]{0x0000000000000050L});
        public static final BitSet FOLLOW_ruleEString_in_ruleMetaModel152 = new BitSet(new long[]{0x0000000000001000L});
        public static final BitSet FOLLOW_ruleMetaClass_in_ruleMetaModel173 = new BitSet(new long[]{0x0000000000001002L});
        public static final BitSet FOLLOW_ruleMetaClass_in_ruleMetaModel194 = new BitSet(new long[]{0x0000000000001002L});
        public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature231 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleFeature241 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_ruleFeature288 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_ruleFeature315 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleMetaClass_in_entryRuleMetaClass350 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleMetaClass360 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_12_in_ruleMetaClass406 = new BitSet(new long[]{0x0000000000000050L});
        public static final BitSet FOLLOW_ruleEString_in_ruleMetaClass427 = new BitSet(new long[]{0x000000000000A000L});
        public static final BitSet FOLLOW_13_in_ruleMetaClass440 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleMetaClass460 = new BitSet(new long[]{0x000000000000C000L});
        public static final BitSet FOLLOW_14_in_ruleMetaClass473 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleMetaClass493 = new BitSet(new long[]{0x000000000000C000L});
        public static final BitSet FOLLOW_15_in_ruleMetaClass509 = new BitSet(new long[]{0x0000000000860000L});
        public static final BitSet FOLLOW_ruleFeature_in_ruleMetaClass531 = new BitSet(new long[]{0x0000000000870000L});
        public static final BitSet FOLLOW_16_in_ruleMetaClass544 = new BitSet(new long[]{0x0000000000860000L});
        public static final BitSet FOLLOW_17_in_ruleMetaClass560 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleAttribute_in_entryRuleAttribute596 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleAttribute606 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_18_in_ruleAttribute652 = new BitSet(new long[]{0x0000000000000050L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAttribute673 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_19_in_ruleAttribute685 = new BitSet(new long[]{0x0000000000000050L});
        public static final BitSet FOLLOW_ruleEString_in_ruleAttribute706 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_20_in_ruleAttribute718 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleAttribute735 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleAttribute752 = new BitSet(new long[]{0x0000000001000020L});
        public static final BitSet FOLLOW_ruleEInt_in_ruleAttribute773 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_22_in_ruleAttribute785 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleReference_in_entryRuleReference821 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReference831 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_23_in_ruleReference877 = new BitSet(new long[]{0x0000000000000050L});
        public static final BitSet FOLLOW_ruleEString_in_ruleReference898 = new BitSet(new long[]{0x0000000000080000L});
        public static final BitSet FOLLOW_19_in_ruleReference910 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleReference930 = new BitSet(new long[]{0x0000000000100000L});
        public static final BitSet FOLLOW_20_in_ruleReference942 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleReference959 = new BitSet(new long[]{0x0000000000200000L});
        public static final BitSet FOLLOW_21_in_ruleReference976 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleReference993 = new BitSet(new long[]{0x0000000000400000L});
        public static final BitSet FOLLOW_22_in_ruleReference1010 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEString_in_entryRuleEString1047 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEString1058 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_STRING_in_ruleEString1098 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_ID_in_ruleEString1124 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEInt_in_entryRuleEInt1170 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEInt1181 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_24_in_ruleEInt1220 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_RULE_INT_in_ruleEInt1237 = new BitSet(new long[]{0x0000000000000002L});
    }


}