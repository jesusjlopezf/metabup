/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.dnd;


import org.eclipse.jface.viewers.LabelProvider;

import metabup.metamodel.Attribute;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;


public class TreeLabelProvider extends LabelProvider {
	@Override
	public String getText(Object element) {
		String s = "";
		if(element instanceof MetaClass) s = ((MetaClass) element).getName();
		if(element instanceof Attribute) s = ((Feature) element).getName() + " : " 
												+ ((Attribute)element).getPrimitiveType() + " ("
												+ ((Attribute)element).getMin() + ", " 
												+  ((Attribute)element).getMax() + ")";
		if(element instanceof Reference) s = ((Reference) element).getName() + " -> " 
				+ ((Reference)element).getReference().getName() + " ("
				+ ((Reference)element).getMin() + ", " 
				+ ((Reference)element).getMax() + ")";
		
		return s;
	}
} 
