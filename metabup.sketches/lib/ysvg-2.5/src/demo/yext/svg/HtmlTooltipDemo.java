/****************************************************************************
 * This demo file is part of the yFiles extension package ySVG-2.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * ySVG demo files exhibit ySVG functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a ySVG version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own ySVG powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the ySVG
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.svg;

import yext.svg.io.SVGDOMEnhancer;
import yext.svg.io.SVGIOHandler;

import y.base.Node;
import y.view.NodeRealizer;
import y.view.ViewMode;

import javax.swing.JPanel;
import javax.swing.JToolTip;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.EventQueue;
import java.awt.geom.AffineTransform;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Demo that shows how to add HTML formatted tooltips for nodes to a generated
 * SVG file. The text of the node's default label is used as tooltip text.
 * @see <a href="http://docs.yworks.com/yfiles/doc/developers-guide/svg_customization.html#svg_customization">Section Customizing SVG Content</a> in the yFiles for Java Developer's Guide
 */
public class HtmlTooltipDemo extends SVGExportDemo
{

  /**
   * Add a view mode that displays a tooltip for each node
   */
  public HtmlTooltipDemo() {
    view.addViewMode(new TooltipViewMode());
  }

  /**
   * Creates and configures the SVGIOHandlers to be used.
   * This method will add a specialized SVGDOMEnhancer to
   * the IOHandler.
   */
  protected SVGIOHandler createSVGIOHandler(boolean svgz)
  {
    SVGIOHandler ioh = super.createSVGIOHandler(svgz);
    SVGDOMEnhancer enhancer = new HtmlTooltipDemo.TooltipEnhancer();
    //draw edges behind nodes so that the node rollover does not get spoiled.
    enhancer.setDrawEdgesFirst(true);
    ioh.setSVGGraph2DRenderer(enhancer);
    ioh.setCanvasSizeAssigned(false); //do not assign canvas size to avoid tooltip clipping
    return ioh;
  }

  /**
   * Creates the HTML formatted tooltip to be displayed for a graph node.
   * @param node the graph node for which the tooltip is created.
   * @return a HTML formatted tooltip for specified node.
   */
  String createHTMLDescription(Node node) {
    return "<html><head>" +
    "<style><!--\n" +
    ".tooltip {\n" +
    "  font-size: 10pt;\n" +
    "  position: absolute;\n" +
    "  display: none;\n" +
    "  background-color: #EDECF4;\n" +
    "  border-width: 1px;\n" +
    "  border-style: solid;\n" +
    "  border-color: #E2860E; }\n" +
    "\n" +
    ".tooltip .head {\n" +
    "  font-weight: bold;\n" +
    "  background-color: #E2860E;\n" +
    "  color: #EDECF4;\n" +
    "  padding: 2px; }\n" +
    "\n" +
    ".tooltip .content {\n" +
    "  color: #666666;\n" +
    "  font-weight: bold;\n" +
    "  padding: 4px; }\n" +
    "--></style>\n" +
    "</head>\n" +
    "<div class=\"tooltip\">\n" +
      "<div class=\"head\">Description</div>\n" +
      "<div class=\"content\">Node " + node + "</div>\n" +
    "</div>";
  }

  /**
   * Viewmode that displays a HTML formatted tooltip for each node
   */
  class TooltipViewMode extends ViewMode {

    public void mouseMoved(double x, double y) {
      Node hitNode = getHitInfo(x,y).getHitNode();
      if(hitNode != null) {
        String tipText = createHTMLDescription(hitNode);
        if(tipText != null) {
          view.setToolTipText(tipText);
        }
      }
    }
  }

  /**
   * Specialized SVGDOMEnhancer that adds the necessary data and functionality to
   * display tooltips from within an SVG file.
   */
  class TooltipEnhancer extends SVGDOMEnhancer
  {
    JPanel labelContainer = new JPanel();

    /**
     * Override a callback method. Before the graph will be written to the SVG DOM
     * we add an additional javascript node to the definition block of the SVG.
     * The script will be used to show and hide tooltip elements
     */
    protected void initializeDOM()
    {
      addToSVGDefinition(createScript());
    }

    /**
     * Creates a DOM element that defines a ecmascript function responsible
     * for showing and hiding appropriate tooltips at the right position.
     */
    private Element createScript() {
      Element script = createElement("script");
      script.setAttribute("type", "text/ecmascript");

      String sourceCode =
              "      var SVGDocument = null;\n" +
              "      var SVGRoot = null;\n" +
              "      var toolTip = null;\n" +
              "      var TrueCoords = null;\n" +
              "      var lastElement = null;\n" +
              "      var initialized = null;\n" +
              "      var tipGroup;\n" +
              "      function Init(evt)\n" +
              "      {\n" +
              "         SVGDocument = evt.target.ownerDocument;\n" +
              "         SVGRoot = SVGDocument.documentElement;\n" +
              "         TrueCoords = SVGRoot.createSVGPoint();\n" +
              "         initialized = evt;\n" +
              "      };\n" +
              "      function GetTrueCoords(evt)\n" +
              "      {\n" +
              "         var newScale = SVGRoot.currentScale;\n" +
              "         var translation = SVGRoot.currentTranslate;\n" +
              "         TrueCoords.x = (evt.clientX - translation.x)/newScale;\n" +
              "         TrueCoords.y = (evt.clientY - translation.y)/newScale;\n" +
              "      };\n" +
              "      function HideTooltip( evt )\n" +
              "      {\n" +
              "         if(initialized == null) {\n" +
              "           Init(evt);\n" +
              "         }\n" +
              "         if(tipGroup != null) {\n" +
              "           tipGroup.setAttributeNS(null, 'visibility', 'hidden');\n" +
              "         }\n" +
              "      };\n" +
              "      function ShowTooltip( evt )\n" +
              "      {\n" +
              "         if(initialized == null) {\n" +
              "           Init(evt);\n" +
              "         }\n" +
              "         GetTrueCoords( evt );\n" +
              "         var tipScale = 1/SVGRoot.currentScale;\n" +
              "         var textWidth = 0;\n" +
              "         var tspanWidth = 0;\n" +
              "         var boxHeight = 20;\n" +
              "         var targetElement = evt.currentTarget;\n" +
              "         if ( lastElement != targetElement )\n" +
              "         {\n" +
              "            var targetId = targetElement.getAttributeNS(null, 'id');\n" +
              "            var tipId = 'tooltip.' + targetId;\n" +
              "            tipGroup = SVGDocument.getElementById(tipId);\n" +
              "            var xPos = TrueCoords.x + (10 * tipScale);\n" +
              "            var yPos = TrueCoords.y + (10 * tipScale);\n" +
              "            tipGroup.setAttributeNS(null, 'transform', 'translate(' + xPos + ',' + yPos + ') scale(' + tipScale + ')');\n" +
              "            tipGroup.setAttributeNS(null, 'visibility', 'visible');\n" +
              "         }\n" +
              "      };\n";


      script.appendChild(createCDATASection(sourceCode));

      return script;
    }

    protected void paint(Graphics2D gfx, NodeRealizer r, boolean sloppyMode) {
      super.paint(gfx,r,sloppyMode);
      paintDescription(gfx, r.getNode(), createGroupID(r.getNode()));
    }

    /**
     * Override a callback method.
     * Adds onmouseover and onmouseout attributes to the element that represents
     * a given yNode in SVG. These attributes trigger the javascript function defined
     * above.
     */
    protected void nodeAddedToDOM(Node yNode, Element element) {
      String desc = createHTMLDescription(yNode);
      if (!isEmpty(desc)) {
        element.setAttribute(
            "onmousemove",
            "ShowTooltip(evt)");
        element.setAttribute(
            "onmouseout",
            "HideTooltip(evt)");
      }
    }

    private void paintDescription(Graphics2D gfx, Node node, String objectId) {
      String desc = createHTMLDescription(node);
      if (!isEmpty(desc)) {
        Document doc = getSVGDocument();
        Element topLevelGroup = getTopLevelGroup();
        Element tipGroup = doc.createElement("g");
        tipGroup.setAttribute("visibility", "hidden");
        tipGroup.setAttribute("pointer-events", "none");
        tipGroup.setAttribute("id", "tooltip." + objectId);
        topLevelGroup.appendChild(tipGroup);

        setTopLevelGroup(tipGroup);

        JToolTip label = new JToolTip();
        label.setTipText(desc);
        Dimension dim = label.getPreferredSize();
        label.setSize(dim.width, dim.height);

        AffineTransform origTrans = gfx.getTransform();
        gfx.setTransform(new AffineTransform());
        SwingUtilities.paintComponent(gfx, label, labelContainer, 0, 0, dim.width, dim.height);
        gfx.setTransform(origTrans);
        setTopLevelGroup(topLevelGroup);
      }
    }

    private boolean isEmpty( final String desc ) {
      return desc == null || desc.trim().length() == 0;
    }
  }

  /**
   * Launches this demo.
   * @param args ignored.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        initLnF();
        (new HtmlTooltipDemo()).start();
      }
    });
  }
}
