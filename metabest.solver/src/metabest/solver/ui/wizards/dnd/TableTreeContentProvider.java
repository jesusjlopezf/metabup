/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.dnd;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;


public class TableTreeContentProvider implements IStructuredContentProvider, ITreeContentProvider {
	private static Object[] EMPTY_ARRAY = new Object[0]; 
	
	public TableTreeContentProvider(){}
	
  @Override
  public Object[] getChildren(Object parentElement) {
	  //System.out.println("getChildren");
	  if(parentElement instanceof List<?>){
		  List<AnnotatedElement> list = (List<AnnotatedElement>) parentElement;
		  return  list.toArray();
	  }
	  
	  return EMPTY_ARRAY;
  }

  @Override
  public Object getParent(Object element) {
	  return EMPTY_ARRAY;
  }

  @Override
  public boolean hasChildren(Object element) {
	  return false;
  }

  @Override
  public Object[] getElements(Object inputElement) {
	  return getChildren(inputElement);
  }
	  
	  
	@Override
	public void dispose() {
	}
	
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}
} 
