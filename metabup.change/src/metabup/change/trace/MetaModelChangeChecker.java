/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.change.trace;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Attribute;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;

public class MetaModelChangeChecker {
	private MetaModel src;
	private MetaModel tar;
	
	public MetaModelChangeChecker(MetaModel src, MetaModel tar){
		this.src = src;
		this.tar = tar;
	}
	
	public List<AnnotatedElement> getNew(){
		if(src == null || tar == null) return null;
		List<AnnotatedElement> newElements = new ArrayList<AnnotatedElement>();				
		
		List<MetaClass> tarmmcc = tar.getClasses();
		List<MetaClass> srcmmcc = src.getClasses();		
		
		for(MetaClass tarmc : tarmmcc){
			boolean mcFound = false;
			
			for(MetaClass srcmc : srcmmcc){
				if(tarmc.getId().equals(srcmc.getId())){
					/* Features */
					mcFound = true;	
					
					List<Feature> srcmcff = srcmc.getFeatures();
					List<Feature> tarmcff = tarmc.getFeatures();
					
					for(Feature tarf : tarmcff){
						boolean fFound = false;
						
						for(Feature srcf : srcmcff){
							if(tarf.getId().equals(srcf.getId())) fFound = true;
						}
						
						if(!fFound) newElements.add(tarf);
					}															
				}
			}
			
			if (!mcFound){
				newElements.add(tarmc);
				newElements.addAll(tarmc.getFeatures());
			}
		}
		
		return newElements;
	}
	
	public List<MetaClass[]> getNewInheritances(){
		if(src == null || tar == null) return null;
		List<MetaClass[]> newElements = new ArrayList<MetaClass[]>();				
		
		List<MetaClass> tarmmcc = tar.getClasses();
		List<MetaClass> srcmmcc = src.getClasses();		
		
		for(MetaClass tarmc : tarmmcc){
			for(MetaClass srcmc : srcmmcc){
				if(tarmc.getId().equals(srcmc.getId())){
					// subs are taken for granted at least for now
					List<MetaClass> srcmcSupers = srcmc.getSupers();
					List<MetaClass> tarmcSupers = tarmc.getSupers();																		
						
					for(MetaClass tarmcSuper : tarmcSupers){
						boolean found = false;
						for(MetaClass srcmcSuper : srcmcSupers) if(srcmcSuper.getId().equals(tarmcSuper.getId())) found = true;						
						if(!found){
							MetaClass in[] = {srcmc, tarmcSuper};
							newElements.add(in);						
						}
					}
				}
			}
		}
		
		return newElements;
	}
	
	public List<AnnotatedElement> getDeleted(){
		if(src == null || tar == null) return null;
		List<AnnotatedElement> delElements = new ArrayList<AnnotatedElement>();				
		
		List<MetaClass> srcmmcc = src.getClasses();
		List<MetaClass> tarmmcc = tar.getClasses();		
		
		for(MetaClass srcmc : srcmmcc){
			boolean mcFound = false;
			
			for(MetaClass tarmc : tarmmcc){
				if(srcmc.getId().equals(tarmc.getId())){
					mcFound = true;	
					
					List<Feature> tarmcff = tarmc.getFeatures();
					List<Feature> srcmcff = srcmc.getFeatures();
					
					for(Feature srcf : srcmcff){
						boolean fFound = false;
						
						for(Feature tarf : tarmcff){
							if(srcf.getId().equals(tarf.getId())) fFound = true;
						}
						
						if(!fFound) delElements.add(srcf);
					}					
				}
			}
			
			if (!mcFound){
				delElements.add(srcmc);
				delElements.addAll(srcmc.getFeatures());
			}
		}
		
		return delElements;
	}
	
	/* 
	 * it returns a list featuring pairs of metaclasses whose inheritance 
	 * relationship was removed. The tuple contains:
	 * [0] - subclass
	 * [1] - superclass
	 */
	public List<MetaClass[]> getDeletedInheritances(){
		if(src == null || tar == null) return null;
		List<MetaClass[]> delElements = new ArrayList<MetaClass[]>();				
		
		List<MetaClass> tarmmcc = tar.getClasses();
		List<MetaClass> srcmmcc = src.getClasses();		
		
		for(MetaClass tarmc : tarmmcc){
			for(MetaClass srcmc : srcmmcc){
				if(srcmc.getId().equals(tarmc.getId())){
					// subs are taken for granted at least for now
					List<MetaClass> srcmcSupers = srcmc.getSupers();
					List<MetaClass> tarmcSupers = tarmc.getSupers();																		
						
					for(MetaClass srcmcSuper : srcmcSupers){
						boolean found = false;
						for(MetaClass tarmcSuper : tarmcSupers) if(srcmcSuper.getId().equals(srcmcSuper.getId())) found = true;						
						if(!found){
							MetaClass in[] = {srcmc, srcmcSuper};
							delElements.add(in);
						}
					}
				}				
			}
		}
		
		return delElements;
	}
	
	public List<AnnotatedElement> getRenamed(){
		if(src == null || tar == null) return null;
		List<AnnotatedElement> updElements = new ArrayList<AnnotatedElement>();
		
		List<MetaClass> tarmmcc = tar.getClasses();
		List<MetaClass> srcmmcc = src.getClasses();
		
		/* renamed MetaClasses */
		for(MetaClass srcmc : srcmmcc){
			for(MetaClass tarmc : tarmmcc){
				if(!updElements.contains(tarmc) && tarmc.getId().equals(srcmc.getId())){
					if(!tarmc.getName().equals(srcmc.getName())) updElements.add(tarmc);					
				}
				
				/* renamed features*/
				List<Feature> srcmcff = srcmc.getFeatures();
				List<Feature> tarmcff = tarmc.getFeatures();
				
				for(Feature srcmcf : srcmcff){
					for(Feature tarmcf: tarmcff){
						if(!updElements.contains(tarmcf) && srcmcf.getId().equals(tarmcf.getId())){
							if(!tarmcf.getName().equals(srcmcf.getName())) updElements.add(tarmcf);												
						}
					}
				}
			}
		}
		
		return updElements;
	}
	
	// including renamed
	public List<AnnotatedElement> getUpdated(boolean includeSubs){
		if(src == null || tar == null) return null;
		List<AnnotatedElement> updElements = new ArrayList<AnnotatedElement>();
		
		List<MetaClass> tarmmcc = tar.getClasses();
		List<MetaClass> srcmmcc = src.getClasses();		
		
		/* updated MetaClasses */
		for(MetaClass srcmc : srcmmcc){
			for(MetaClass tarmc : tarmmcc){
				if(!updElements.contains(tarmc) && tarmc.getId().equals(srcmc.getId())){
					if(!tarmc.getName().equals(srcmc.getName())) updElements.add(tarmc);					
					else if(tarmc.getIsAbstract() != srcmc.getIsAbstract()) updElements.add(tarmc);										
				}
				
				/* updated features*/
				List<Feature> srcmcff = srcmc.getFeatures();
				List<Feature> tarmcff = tarmc.getFeatures();
				
				for(Feature srcmcf : srcmcff){
					for(Feature tarmcf: tarmcff){
						if(!updElements.contains(tarmcf) && srcmcf.getId().equals(tarmcf.getId())){
							if(!tarmcf.getName().equals(srcmcf.getName())) updElements.add(tarmcf);
							/* updated attributes */
							else if(tarmcf instanceof Attribute){
								String tarPT = ((Attribute)tarmcf).getPrimitiveType();
								String srcPT = ((Attribute)srcmcf).getPrimitiveType();
								if(!tarPT.equals(srcPT)) updElements.add(tarmcf);
							/* updated references */
							}else if(tarmcf instanceof Reference){
								MetaClass tarRefMC = ((Reference)tarmcf).getReference();
								MetaClass srcRefMC = ((Reference)srcmcf).getReference();								
								if(tarRefMC != null && srcRefMC != null && !tarRefMC.getId().equals(srcRefMC.getId())) updElements.add(tarmcf);
							}							
						}
					}
				}
			}
		}
		
		return updElements;
	}
	
	public boolean changed(){
		if(this.getDeleted() != null && this.getDeleted().size() > 0) return true;		
		if(this.getDeletedInheritances() != null && this.getDeletedInheritances().size() > 0) return true;
		if(this.getNew() != null && this.getNew().size() > 0) return true;
		if(this.getNewInheritances() != null && this.getNewInheritances().size() > 0) return true;
		if(this.getUpdated(false) != null && this.getUpdated(false).size() > 0) return true;
		return false;
	}
}
