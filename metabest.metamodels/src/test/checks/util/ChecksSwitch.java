/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import test.checks.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see test.checks.ChecksPackage
 * @generated
 */
public class ChecksSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ChecksPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChecksSwitch() {
		if (modelPackage == null) {
			modelPackage = ChecksPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ChecksPackage.CHECK: {
				Check check = (Check)theEObject;
				T result = caseCheck(check);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.EXISTENCE: {
				Existence existence = (Existence)theEObject;
				T result = caseExistence(existence);
				if (result == null) result = caseCheck(existence);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.FEATURE_EXISTENCE: {
				FeatureExistence featureExistence = (FeatureExistence)theEObject;
				T result = caseFeatureExistence(featureExistence);
				if (result == null) result = caseExistence(featureExistence);
				if (result == null) result = caseCheck(featureExistence);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.TYPE_EXISTENCE: {
				TypeExistence typeExistence = (TypeExistence)theEObject;
				T result = caseTypeExistence(typeExistence);
				if (result == null) result = caseExistence(typeExistence);
				if (result == null) result = caseCheck(typeExistence);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.ABSTRACT_TYPE_INSTANCE: {
				AbstractTypeInstance abstractTypeInstance = (AbstractTypeInstance)theEObject;
				T result = caseAbstractTypeInstance(abstractTypeInstance);
				if (result == null) result = caseCheck(abstractTypeInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.MISSING_ATTRIBUTE: {
				MissingAttribute missingAttribute = (MissingAttribute)theEObject;
				T result = caseMissingAttribute(missingAttribute);
				if (result == null) result = caseCheck(missingAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.MISMATCH: {
				Mismatch mismatch = (Mismatch)theEObject;
				T result = caseMismatch(mismatch);
				if (result == null) result = caseCheck(mismatch);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.MULTIPLICITY_MISMATCH: {
				MultiplicityMismatch multiplicityMismatch = (MultiplicityMismatch)theEObject;
				T result = caseMultiplicityMismatch(multiplicityMismatch);
				if (result == null) result = caseMismatch(multiplicityMismatch);
				if (result == null) result = caseCheck(multiplicityMismatch);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.TYPE_MISMATCH: {
				TypeMismatch typeMismatch = (TypeMismatch)theEObject;
				T result = caseTypeMismatch(typeMismatch);
				if (result == null) result = caseMismatch(typeMismatch);
				if (result == null) result = caseCheck(typeMismatch);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.FEATURE_NATURE_MISMATCH: {
				FeatureNatureMismatch featureNatureMismatch = (FeatureNatureMismatch)theEObject;
				T result = caseFeatureNatureMismatch(featureNatureMismatch);
				if (result == null) result = caseMismatch(featureNatureMismatch);
				if (result == null) result = caseCheck(featureNatureMismatch);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.RESTRICTION_VIOLATION: {
				RestrictionViolation restrictionViolation = (RestrictionViolation)theEObject;
				T result = caseRestrictionViolation(restrictionViolation);
				if (result == null) result = caseCheck(restrictionViolation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.UNCONTAINMENT: {
				Uncontainment uncontainment = (Uncontainment)theEObject;
				T result = caseUncontainment(uncontainment);
				if (result == null) result = caseCheck(uncontainment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.UNCONTAINMENT_BY_OBJECT: {
				UncontainmentByObject uncontainmentByObject = (UncontainmentByObject)theEObject;
				T result = caseUncontainmentByObject(uncontainmentByObject);
				if (result == null) result = caseUncontainment(uncontainmentByObject);
				if (result == null) result = caseCheck(uncontainmentByObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.UNCONTAINMENT_BY_META_CLASS: {
				UncontainmentByMetaClass uncontainmentByMetaClass = (UncontainmentByMetaClass)theEObject;
				T result = caseUncontainmentByMetaClass(uncontainmentByMetaClass);
				if (result == null) result = caseUncontainment(uncontainmentByMetaClass);
				if (result == null) result = caseCheck(uncontainmentByMetaClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ChecksPackage.MISSING_REFERENCE: {
				MissingReference missingReference = (MissingReference)theEObject;
				T result = caseMissingReference(missingReference);
				if (result == null) result = caseCheck(missingReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Check</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Check</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCheck(Check object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Existence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Existence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExistence(Existence object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Existence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Existence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureExistence(FeatureExistence object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Existence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Existence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeExistence(TypeExistence object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Type Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Type Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractTypeInstance(AbstractTypeInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Missing Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Missing Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMissingAttribute(MissingAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mismatch</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mismatch</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMismatch(Mismatch object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multiplicity Mismatch</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multiplicity Mismatch</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiplicityMismatch(MultiplicityMismatch object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Mismatch</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Mismatch</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypeMismatch(TypeMismatch object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Nature Mismatch</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Nature Mismatch</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureNatureMismatch(FeatureNatureMismatch object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Restriction Violation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Restriction Violation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRestrictionViolation(RestrictionViolation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uncontainment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uncontainment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUncontainment(Uncontainment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uncontainment By Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uncontainment By Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUncontainmentByObject(UncontainmentByObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Uncontainment By Meta Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Uncontainment By Meta Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUncontainmentByMetaClass(UncontainmentByMetaClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Missing Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Missing Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMissingReference(MissingReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ChecksSwitch
