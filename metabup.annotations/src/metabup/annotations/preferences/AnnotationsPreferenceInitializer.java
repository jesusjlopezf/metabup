/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.preferences;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import metabup.annotations.Activator;
import metabup.annotations.IAnnotation;
import metabup.annotations.general.AnnotationFactory;

/**
 * Class used to initialize default preference values.
 */
public class AnnotationsPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();		
		AnnotationFactory af = new AnnotationFactory();
		af.initializeFactory();
		
		for(String ann : af.listAllAnnotationsNames()){
			Class<? extends IAnnotation> annotationClass = af.getAnnotation(ann);
			try {
				IAnnotation annotation = annotationClass.newInstance();
				HashMap<String, Object> preferences = annotation.getPreferences();
				
				if(preferences != null && !preferences.isEmpty()){
					
					Iterator it = preferences.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry e = (Map.Entry)it.next();
						if(e.getValue() instanceof String ) store.setDefault((String) e.getKey(), (String)e.getValue()); 
						else if(e.getValue() instanceof Boolean ) store.setDefault((String) e.getKey(), (Boolean)e.getValue());
					}									
				}											
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}				
		}
	}

}
