/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test;

import test.extensions.MetaClassQualifier;
import test.extensions.MetaClassSelector;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Class Extension Assertion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.MetaClassExtensionAssertion#getSelector <em>Selector</em>}</li>
 *   <li>{@link test.MetaClassExtensionAssertion#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @see test.TestPackage#getMetaClassExtensionAssertion()
 * @model
 * @generated
 */
public interface MetaClassExtensionAssertion extends ExtensionAssertion {
	/**
	 * Returns the value of the '<em><b>Selector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selector</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selector</em>' containment reference.
	 * @see #setSelector(MetaClassSelector)
	 * @see test.TestPackage#getMetaClassExtensionAssertion_Selector()
	 * @model containment="true" required="true"
	 * @generated
	 */
	MetaClassSelector getSelector();

	/**
	 * Sets the value of the '{@link test.MetaClassExtensionAssertion#getSelector <em>Selector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selector</em>' containment reference.
	 * @see #getSelector()
	 * @generated
	 */
	void setSelector(MetaClassSelector value);

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(MetaClassQualifier)
	 * @see test.TestPackage#getMetaClassExtensionAssertion_Condition()
	 * @model containment="true"
	 * @generated
	 */
	MetaClassQualifier getCondition();

	/**
	 * Sets the value of the '{@link test.MetaClassExtensionAssertion#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(MetaClassQualifier value);

} // MetaClassExtensionAssertion
