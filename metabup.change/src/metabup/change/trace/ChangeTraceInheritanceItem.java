/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.change.trace;

import org.eclipse.emf.ecore.EModelElement;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaClass;

public class ChangeTraceInheritanceItem {
	private String op = null; // operation code {"add", "delete"}
	private MetaClass source = null; // source metaclass within the relationship
	private MetaClass target = null; // target metaclass within the relationship	
	
	public ChangeTraceInheritanceItem(String op, MetaClass source, MetaClass target){		
		this.setOp(op);
		this.setSource(source);
		this.setTarget(target);				
	}
	
	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		if(op.equals("add") || op.equals("delete")) this.op = op;
		else this.op = "";
	}

	private void setSource(MetaClass source){
		this.source = source;
	}
	
	private void setTarget(MetaClass target){
		this.target = target;
	}
	
	public MetaClass getSource(){
		return source;
	}
	
	public MetaClass getTarget(){
		return target;
	}
	
	public boolean isAdd(){
		if(this.op.equals("add")) return true;
		else return false;
	}
	
	public boolean isDelete(){
		if(this.op.equals("delete")) return true;
		else return false;
	}		
}
