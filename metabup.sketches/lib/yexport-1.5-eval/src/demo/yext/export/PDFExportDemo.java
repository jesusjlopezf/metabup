/****************************************************************************
 * This demo file is part of the yFiles extension package yExport-1.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * yExport demo files exhibit yExport functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a yExport version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own yExport powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the yExport
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.export;


import y.view.YRenderingHints;
import yext.export.io.PDFOutputHandler;

import y.base.EdgeCursor;
import y.base.NodeCursor;
import y.option.ConstraintManager;
import y.option.OptionGroup;
import y.option.OptionHandler;
import y.option.OptionItem;
import y.geom.YPoint;
import y.view.BackgroundRenderer;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import y.view.Graph2DView;
import y.view.NodeRealizer;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.net.URL;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.MediaSizeName;


/**
 * Demonstrates how to export a graph to PDF format.
 * Additionally, the demo allows to specify a preferred page format for
 * the PDF export.
 * It uses a custom background renderer to provide an indication on whether the
 * graph to be exported will fit on the specified page.
 * @see <a href="http://docs.yworks.com/yfiles/doc/developers-guide/export_usage.html#export_usage">Using the yExport Extension Package</a> in the yFiles for Java Developer's Guide
 */
public class PDFExportDemo extends AbstractExportDemo {
  private static final String ACTIVATE_TILING = "Activate Tiling";
  private static final String RENDER_TEXT_AS_SHAPES = "Render Text as Shapes";
  private static final String EMBED_FONTS = "Embed Fonts";

  private static final String LANDSCAPE = "Landscape";
  private static final String PORTRAIT  = "Portrait";
  private static final String PAGE  = "Page";

  private static final String MARGIN_TOP    = "Margin Top";
  private static final String MARGIN_LEFT   = "Margin Left";
  private static final String MARGIN_BOTTOM = "Margin Bottom";
  private static final String MARGIN_RIGHT  = "Margin Right";


  PageFormat pageFormat;
  PDFOutputHandler outputHandler;

  public PDFExportDemo() {
    pageFormat = new PageFormat();
    PageFormatHelper.assignMediaSize(
            pageFormat,
            MediaSize.getMediaSizeForName(MediaSizeName.ISO_A4)
    );
    PageFormatHelper.setMargin(pageFormat, new Insets(0, 0, 0, 0));
    PageBackgroundRenderer pbr = new PageBackgroundRenderer(view);
    pbr.setPageFormat(pageFormat);
    getOutputHandler().setPageFormat(pageFormat);

    final double w = pageFormat.getWidth();
    final double h = pageFormat.getHeight();
    Dimension ps;
    ps = view.getPreferredSize();
    ps = new Dimension(ps.width, (int)Math.ceil(ps.width * h/w));

    view.setBackgroundRenderer(pbr);
    view.setPreferredSize(ps);
    view.zoomToArea(-10, -10, w + 20, h + 20);
    view.updateView();
  }

  /**
   * Creates a toolbar for this demo.
   */
  protected JToolBar createToolBar() {
    JToolBar jtb = super.createToolBar();
    jtb.addSeparator();

    jtb.add(new JButton(new PDFExportAction("PDF Export", getOutputHandler())));
    jtb.add(new JButton(new PageFormatAction()));
    jtb.add(new JButton(new CenterGraphOnPageAction()));

    return jtb;
  }

  protected void loadGraph(final URL resource, final boolean updateView) {
    super.loadGraph(resource, false);
    final Action a = new CenterGraphOnPageAction();
    a.actionPerformed(null);
    view.fitContent();
    view.updateView();
  }

  private PDFOutputHandler getOutputHandler() {
    if (outputHandler == null) {
      outputHandler = new PDFOutputHandler();
      outputHandler.setPageOutlinesEnabled(false);
    }
    return outputHandler;
  }


  /**
   * Action that provides a means to configure page formats for PDF generation.
   */
  class PageFormatAction extends AbstractAction {
    OptionHandler op;
    MediaSizeName[] mediaNames = {
        MediaSizeName.ISO_A0,
        MediaSizeName.ISO_A1,
        MediaSizeName.ISO_A2,
        MediaSizeName.ISO_A3,
        MediaSizeName.ISO_A4,
        MediaSizeName.ISO_A5,
        MediaSizeName.ISO_A6
      };

    public PageFormatAction() {
      final String name = "Page Format";
      putValue(Action.NAME, name);
      putValue(Action.SMALL_ICON, createIcon("resource/PageFormat16.gif"));
      putValue(Action.SHORT_DESCRIPTION, name);

      op = new OptionHandler(name);
      final OptionGroup group = new OptionGroup();
      group.setAttribute(OptionGroup.ATTRIBUTE_TITLE, name + " Options");
      group.addItem(op.addEnum("Paper Size", mediaNames, 4));
      group.addItem(op.addEnum("Orientation", new String[]{PORTRAIT, LANDSCAPE},0));
      OptionItem item;
      item = op.addDouble(MARGIN_TOP, 0, 0, 4);
      item.setTipText("Top margin in inch.");
      group.addItem(item);
      item = op.addDouble(MARGIN_LEFT, 0, 0, 4);
      item.setTipText("Left margin in inch.");
      group.addItem(item);
      item = op.addDouble(MARGIN_BOTTOM, 0, 0, 4);
      item.setTipText("Bottom margin in inch.");
      group.addItem(item);
      item = op.addDouble(MARGIN_RIGHT, 0, 0, 4);
      item.setTipText("Right margin in inch.");
      group.addItem(item);
    }

    public void actionPerformed(ActionEvent e) {
      if(op.showEditor(view.getFrame())) {
        MediaSizeName mediaName = (MediaSizeName) op.get("Paper Size");
        MediaSize mediaSize = MediaSize.getMediaSizeForName(mediaName);
        PageFormat pageFormat = new PageFormat();
        PageFormatHelper.assignMediaSize(pageFormat, mediaSize);
        final double dpi = 72;
        PageFormatHelper.setMargin(
                pageFormat,
                new Insets((int)Math.ceil(dpi*op.getDouble(MARGIN_TOP)),
                           (int)Math.ceil(dpi*op.getDouble(MARGIN_LEFT)),
                           (int)Math.ceil(dpi*op.getDouble(MARGIN_BOTTOM)),
                           (int)Math.ceil(dpi*op.getDouble(MARGIN_RIGHT)))
        );
        if (LANDSCAPE.equals(op.get("Orientation"))) {
          pageFormat.setOrientation(PageFormat.LANDSCAPE);
        }
        else {
          pageFormat.setOrientation(PageFormat.PORTRAIT);
        }
        updatePageFormat(pageFormat);
        view.updateView();
      }
    }

    private void updatePageFormat( final PageFormat pageFormat ) {
      PDFExportDemo.this.pageFormat = pageFormat;
      PDFExportDemo.this.getOutputHandler().setPageFormat(pageFormat);
      final BackgroundRenderer renderer = view.getBackgroundRenderer();
      if (renderer instanceof PageBackgroundRenderer) {
        ((PageBackgroundRenderer)renderer).setPageFormat(pageFormat);
      }
    }
  }

  /**
   * Action that centers the current graph on the background page indicator.
   */
  class CenterGraphOnPageAction extends AbstractAction {
    CenterGraphOnPageAction() {
      final String name = "Center Graph on Page";
      putValue(Action.NAME, name);
      putValue(Action.SMALL_ICON, createIcon("resource/CenterOnPage16.gif"));
      putValue(Action.SHORT_DESCRIPTION, name);
    }

    public void actionPerformed( final ActionEvent e ) {
      final Graph2D graph = view.getGraph2D();
      final Rectangle bbx = graph.getBoundingBox();

      final double dx = pageFormat.getWidth()  * 0.5 - bbx.getCenterX();
      final double dy = pageFormat.getHeight() * 0.5 - bbx.getCenterY();

      for (NodeCursor nc = graph.nodes(); nc.ok(); nc.next()) {
        final NodeRealizer nr = graph.getRealizer(nc.node());
        nr.setCenter(nr.getCenterX() + dx, nr.getCenterY() + dy);
      }

      for (EdgeCursor ec = graph.edges(); ec.ok(); ec.next()) {
        final EdgeRealizer er = graph.getRealizer(ec.edge());
        for (int i = er.pointCount(); i --> 0;) {
          final YPoint p = er.getPoint(i);
          er.setPoint(i, p.getX() + dx, p.getY() + dy);
        }
      }

      view.setCenter(bbx.getCenterX() + dx, bbx.getCenterY() + dy);
      view.updateView();
    }
  }

  /**
   * Action that exports the given graph to PDF.
   */
  class PDFExportAction extends ExportAction
  {
    PDFExportAction( String name, PDFOutputHandler outputHandler ) {
      super(name, outputHandler);

      final String[] sizes = {
              "Use Original Size", USE_CUSTOM_WIDTH, USE_CUSTOM_HEIGHT
      };
      final String[] clipRegions = {
              GRAPH, PAGE, VIEW
      };

      // set up an OptionHandler to allow for user customizations
      options = new OptionHandler(name);
      final OptionGroup group = new OptionGroup();
      group.setAttribute(OptionGroup.ATTRIBUTE_TITLE, name + " Options");
      group.addItem(options.addEnum(SIZE, sizes, 0));
      group.addItem(options.addInt(CUSTOM_WIDTH,  500));
      group.addItem(options.addInt(CUSTOM_HEIGHT, 500));
      group.addItem(options.addInt(EMPTY_BORDER_WIDTH, 10));
      group.addItem(options.addEnum(CLIP_REGION, clipRegions, 0));
      group.addItem(options.addBool(SELECTION_PAINTING, false));
      final OptionItem item = options.addBool(ACTIVATE_TILING, false);
      item.setTipText("Splits large graphs over multiple pages.");
      group.addItem(item);
      group.addItem(options.addBool(RENDER_TEXT_AS_SHAPES, false));
      group.addItem(options.addBool(EMBED_FONTS, false));

      final ConstraintManager cm = new ConstraintManager(options);
      cm.setEnabledOnValueEquals(SIZE, USE_CUSTOM_WIDTH, CUSTOM_WIDTH);
      cm.setEnabledOnValueEquals(SIZE, USE_CUSTOM_HEIGHT, CUSTOM_HEIGHT);
      cm.setEnabledOnValueEquals(CLIP_REGION, GRAPH, EMPTY_BORDER_WIDTH);
      cm.setEnabledOnValueEquals(CLIP_REGION, PAGE, ACTIVATE_TILING, true);
      cm.setEnabledOnValueEquals(RENDER_TEXT_AS_SHAPES, Boolean.FALSE, EMBED_FONTS);
    }

    void configureViewPort( final Graph2DView viewPort ) {
      Graph2D graph = view.getGraph2D();

      double width  = options.getInt(CUSTOM_WIDTH);
      double height = options.getInt(CUSTOM_HEIGHT);
      Point viewPoint = viewPort.getViewPoint();
      double zoom = 1.0;

      if (GRAPH.equals(options.get(CLIP_REGION))) {
        Rectangle box = graph.getBoundingBox();
        int border = options.getInt(EMPTY_BORDER_WIDTH);
        box.width  += 2*border;
        box.height += 2*border;
        box.x -= border;
        box.y -= border;

        if (USE_CUSTOM_HEIGHT.equals(options.get(SIZE))) {
          width = height*box.getWidth()/box.getHeight();
        } else if (USE_CUSTOM_WIDTH.equals(options.get(SIZE))) {
          height = width*box.getHeight()/box.getWidth();
        } else {
          width =  box.getWidth();
          height = box.getHeight();
        }
        zoom = width/box.getWidth();
        viewPoint = new Point(box.x, box.y);
      } else if (VIEW.equals(options.get(CLIP_REGION))) {
        if (USE_CUSTOM_HEIGHT.equals(options.get(SIZE))) {
          width = height*view.getWidth()/view.getHeight();
        } else if (USE_CUSTOM_WIDTH.equals(options.get(SIZE))) {
          height = width*view.getHeight()/view.getWidth();
        } else {
          width =  view.getWidth();
          height = view.getHeight();
        }
        viewPoint = view.getViewPoint();
        zoom = view.getZoom()*width/view.getWidth();
      } else if (PAGE.equals(options.get(CLIP_REGION))) {
        final Insets margin = PageFormatHelper.getMargin(pageFormat);
        viewPoint = new Point(margin.left, margin.top);
        width = pageFormat.getWidth() - margin.left - margin.right;
        height = pageFormat.getHeight() - margin.top - margin.bottom;
      }

      viewPort.setZoom(zoom);
      viewPort.setSize((int)Math.ceil(width), (int)Math.ceil(height));
      viewPort.setViewPoint(viewPoint.x, viewPoint.y);
    }

    public void actionPerformed( final ActionEvent e ) {
      // show our OptionHandler to allow the user to set up the export view
      // according to his or her wishes
      if (!options.showEditor(view.getFrame())) {
        return;
      }

      if (options.getBool(SELECTION_PAINTING)) {
        outputHandler.removeRenderingHint(YRenderingHints.KEY_SELECTION_PAINTING);
      } else {
        outputHandler.addRenderingHint(
                YRenderingHints.KEY_SELECTION_PAINTING,
                YRenderingHints.VALUE_SELECTION_PAINTING_OFF);
      }

      ((PDFOutputHandler)outputHandler).setMultiPageTilingEnabled(
              options.getBool(ACTIVATE_TILING) &&
              !PAGE.equals(options.get(CLIP_REGION)));
      final byte policy;
      if (options.getBool(RENDER_TEXT_AS_SHAPES)) {
        policy = PDFOutputHandler.VECTORIZED_TEXT;
      } else if (options.getBool(EMBED_FONTS)) {
        policy = PDFOutputHandler.PLAIN_TEXT_WITH_EMBEDDED_FONTS;
      } else {
        policy = PDFOutputHandler.PLAIN_TEXT;
      }
      ((PDFOutputHandler)outputHandler).setTextRenderingPolicy(policy);

      Graph2D graph = view.getGraph2D();
      Graph2DView viewPort = outputHandler.createDefaultGraph2DView(graph);

      // configure the export view according to the options specified by the
      // user
      configureViewPort(viewPort);
      graph.setCurrentView(viewPort);

      // ask for the destination file and finally write the graph to the chosen
      // destination
      export(outputHandler);

      graph.removeView(viewPort);
    }
  }

  /**
   * Launches this demo.
   */
  public static void main(String[] args) {
    initLnF();
    (new PDFExportDemo()).start();
  }
}