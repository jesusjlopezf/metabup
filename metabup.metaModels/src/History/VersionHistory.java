/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package History;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Version History</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link History.VersionHistory#getType <em>Type</em>}</li>
 *   <li>{@link History.VersionHistory#getVersions <em>Versions</em>}</li>
 * </ul>
 * </p>
 *
 * @see History.HistoryPackage#getVersionHistory()
 * @model
 * @generated
 */
public interface VersionHistory extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link History.VersionHistoryType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see History.VersionHistoryType
	 * @see #setType(VersionHistoryType)
	 * @see History.HistoryPackage#getVersionHistory_Type()
	 * @model required="true"
	 * @generated
	 */
	VersionHistoryType getType();

	/**
	 * Sets the value of the '{@link History.VersionHistory#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see History.VersionHistoryType
	 * @see #getType()
	 * @generated
	 */
	void setType(VersionHistoryType value);

	/**
	 * Returns the value of the '<em><b>Versions</b></em>' containment reference list.
	 * The list contents are of type {@link History.Version}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Versions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Versions</em>' containment reference list.
	 * @see History.HistoryPackage#getVersionHistory_Versions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Version> getVersions();

} // VersionHistory
