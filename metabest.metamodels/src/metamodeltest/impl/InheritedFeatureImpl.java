/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.ClassSelector;
import metamodeltest.InheritedFeature;
import metamodeltest.MetamodeltestPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inherited Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.InheritedFeatureImpl#getClassSel <em>Class Sel</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InheritedFeatureImpl extends FeatureQualifierImpl implements InheritedFeature {
	/**
	 * The cached value of the '{@link #getClassSel() <em>Class Sel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassSel()
	 * @generated
	 * @ordered
	 */
	protected ClassSelector classSel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InheritedFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.INHERITED_FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSelector getClassSel() {
		return classSel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassSel(ClassSelector newClassSel, NotificationChain msgs) {
		ClassSelector oldClassSel = classSel;
		classSel = newClassSel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.INHERITED_FEATURE__CLASS_SEL, oldClassSel, newClassSel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassSel(ClassSelector newClassSel) {
		if (newClassSel != classSel) {
			NotificationChain msgs = null;
			if (classSel != null)
				msgs = ((InternalEObject)classSel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.INHERITED_FEATURE__CLASS_SEL, null, msgs);
			if (newClassSel != null)
				msgs = ((InternalEObject)newClassSel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.INHERITED_FEATURE__CLASS_SEL, null, msgs);
			msgs = basicSetClassSel(newClassSel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.INHERITED_FEATURE__CLASS_SEL, newClassSel, newClassSel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.INHERITED_FEATURE__CLASS_SEL:
				return basicSetClassSel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.INHERITED_FEATURE__CLASS_SEL:
				return getClassSel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.INHERITED_FEATURE__CLASS_SEL:
				setClassSel((ClassSelector)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.INHERITED_FEATURE__CLASS_SEL:
				setClassSel((ClassSelector)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.INHERITED_FEATURE__CLASS_SEL:
				return classSel != null;
		}
		return super.eIsSet(featureID);
	}

} //InheritedFeatureImpl
