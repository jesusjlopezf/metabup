/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import test.checks.ChecksPackage;
import test.checks.UncontainmentByMetaClass;
import test.checks.expressions.MetaClassExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Uncontainment By Meta Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link test.checks.impl.UncontainmentByMetaClassImpl#getMetaclass <em>Metaclass</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UncontainmentByMetaClassImpl extends UncontainmentImpl implements UncontainmentByMetaClass {
	/**
	 * The cached value of the '{@link #getMetaclass() <em>Metaclass</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetaclass()
	 * @generated
	 * @ordered
	 */
	protected MetaClassExpression metaclass;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UncontainmentByMetaClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChecksPackage.Literals.UNCONTAINMENT_BY_META_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaClassExpression getMetaclass() {
		return metaclass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMetaclass(MetaClassExpression newMetaclass, NotificationChain msgs) {
		MetaClassExpression oldMetaclass = metaclass;
		metaclass = newMetaclass;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ChecksPackage.UNCONTAINMENT_BY_META_CLASS__METACLASS, oldMetaclass, newMetaclass);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetaclass(MetaClassExpression newMetaclass) {
		if (newMetaclass != metaclass) {
			NotificationChain msgs = null;
			if (metaclass != null)
				msgs = ((InternalEObject)metaclass).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.UNCONTAINMENT_BY_META_CLASS__METACLASS, null, msgs);
			if (newMetaclass != null)
				msgs = ((InternalEObject)newMetaclass).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.UNCONTAINMENT_BY_META_CLASS__METACLASS, null, msgs);
			msgs = basicSetMetaclass(newMetaclass, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChecksPackage.UNCONTAINMENT_BY_META_CLASS__METACLASS, newMetaclass, newMetaclass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ChecksPackage.UNCONTAINMENT_BY_META_CLASS__METACLASS:
				return basicSetMetaclass(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChecksPackage.UNCONTAINMENT_BY_META_CLASS__METACLASS:
				return getMetaclass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChecksPackage.UNCONTAINMENT_BY_META_CLASS__METACLASS:
				setMetaclass((MetaClassExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChecksPackage.UNCONTAINMENT_BY_META_CLASS__METACLASS:
				setMetaclass((MetaClassExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChecksPackage.UNCONTAINMENT_BY_META_CLASS__METACLASS:
				return metaclass != null;
		}
		return super.eIsSet(featureID);
	}

} //UncontainmentByMetaClassImpl
