/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.expressions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see test.checks.expressions.ExpressionsFactory
 * @model kind="package"
 * @generated
 */
public interface ExpressionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "expressions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/metabest.metamodels/model/test.ecore#//checks/expressions";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "metabest.metamodels";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExpressionsPackage eINSTANCE = test.checks.expressions.impl.ExpressionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.TypeExpressionImpl <em>Type Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.TypeExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getTypeExpression()
	 * @generated
	 */
	int TYPE_EXPRESSION = 0;

	/**
	 * The number of structural features of the '<em>Type Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Type Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.ObjectTypeExpressionImpl <em>Object Type Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.ObjectTypeExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getObjectTypeExpression()
	 * @generated
	 */
	int OBJECT_TYPE_EXPRESSION = 1;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_TYPE_EXPRESSION__OBJECT = TYPE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Object Type Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_TYPE_EXPRESSION_FEATURE_COUNT = TYPE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Object Type Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_TYPE_EXPRESSION_OPERATION_COUNT = TYPE_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.FeatureTypeExpressionImpl <em>Feature Type Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.FeatureTypeExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getFeatureTypeExpression()
	 * @generated
	 */
	int FEATURE_TYPE_EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_EXPRESSION__FEATURE = TYPE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Feature Type Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_EXPRESSION_FEATURE_COUNT = TYPE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Feature Type Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TYPE_EXPRESSION_OPERATION_COUNT = TYPE_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.MultiplicityExpressionImpl <em>Multiplicity Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.MultiplicityExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getMultiplicityExpression()
	 * @generated
	 */
	int MULTIPLICITY_EXPRESSION = 3;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_EXPRESSION__FEATURE = 0;

	/**
	 * The number of structural features of the '<em>Multiplicity Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_EXPRESSION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Multiplicity Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.FeatureNatureExpressionImpl <em>Feature Nature Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.FeatureNatureExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getFeatureNatureExpression()
	 * @generated
	 */
	int FEATURE_NATURE_EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NATURE_EXPRESSION__FEATURE = 0;

	/**
	 * The number of structural features of the '<em>Feature Nature Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NATURE_EXPRESSION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Feature Nature Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_NATURE_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.FragmentElementExpressionImpl <em>Fragment Element Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.FragmentElementExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getFragmentElementExpression()
	 * @generated
	 */
	int FRAGMENT_ELEMENT_EXPRESSION = 7;

	/**
	 * The number of structural features of the '<em>Fragment Element Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_ELEMENT_EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Fragment Element Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FRAGMENT_ELEMENT_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.FeatureWithinObjectExpressionImpl <em>Feature Within Object Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.FeatureWithinObjectExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getFeatureWithinObjectExpression()
	 * @generated
	 */
	int FEATURE_WITHIN_OBJECT_EXPRESSION = 5;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_WITHIN_OBJECT_EXPRESSION__OBJECT = FRAGMENT_ELEMENT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_WITHIN_OBJECT_EXPRESSION__FEATURE = FRAGMENT_ELEMENT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature Within Object Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_WITHIN_OBJECT_EXPRESSION_FEATURE_COUNT = FRAGMENT_ELEMENT_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Feature Within Object Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_WITHIN_OBJECT_EXPRESSION_OPERATION_COUNT = FRAGMENT_ELEMENT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.ObjectExpressionImpl <em>Object Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.ObjectExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getObjectExpression()
	 * @generated
	 */
	int OBJECT_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXPRESSION__OBJECT = FRAGMENT_ELEMENT_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Object Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXPRESSION_FEATURE_COUNT = FRAGMENT_ELEMENT_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Object Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_EXPRESSION_OPERATION_COUNT = FRAGMENT_ELEMENT_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.ObjectOrMetaClassExpressionImpl <em>Object Or Meta Class Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.ObjectOrMetaClassExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getObjectOrMetaClassExpression()
	 * @generated
	 */
	int OBJECT_OR_META_CLASS_EXPRESSION = 9;

	/**
	 * The number of structural features of the '<em>Object Or Meta Class Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_OR_META_CLASS_EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Object Or Meta Class Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_OR_META_CLASS_EXPRESSION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link test.checks.expressions.impl.MetaClassExpressionImpl <em>Meta Class Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see test.checks.expressions.impl.MetaClassExpressionImpl
	 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getMetaClassExpression()
	 * @generated
	 */
	int META_CLASS_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Metaclass Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_EXPRESSION__METACLASS_NAME = OBJECT_OR_META_CLASS_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Meta Class Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_EXPRESSION_FEATURE_COUNT = OBJECT_OR_META_CLASS_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Meta Class Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_CLASS_EXPRESSION_OPERATION_COUNT = OBJECT_OR_META_CLASS_EXPRESSION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link test.checks.expressions.TypeExpression <em>Type Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Expression</em>'.
	 * @see test.checks.expressions.TypeExpression
	 * @generated
	 */
	EClass getTypeExpression();

	/**
	 * Returns the meta object for class '{@link test.checks.expressions.ObjectTypeExpression <em>Object Type Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Type Expression</em>'.
	 * @see test.checks.expressions.ObjectTypeExpression
	 * @generated
	 */
	EClass getObjectTypeExpression();

	/**
	 * Returns the meta object for the reference '{@link test.checks.expressions.ObjectTypeExpression#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see test.checks.expressions.ObjectTypeExpression#getObject()
	 * @see #getObjectTypeExpression()
	 * @generated
	 */
	EReference getObjectTypeExpression_Object();

	/**
	 * Returns the meta object for class '{@link test.checks.expressions.FeatureTypeExpression <em>Feature Type Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Type Expression</em>'.
	 * @see test.checks.expressions.FeatureTypeExpression
	 * @generated
	 */
	EClass getFeatureTypeExpression();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.expressions.FeatureTypeExpression#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature</em>'.
	 * @see test.checks.expressions.FeatureTypeExpression#getFeature()
	 * @see #getFeatureTypeExpression()
	 * @generated
	 */
	EReference getFeatureTypeExpression_Feature();

	/**
	 * Returns the meta object for class '{@link test.checks.expressions.MultiplicityExpression <em>Multiplicity Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplicity Expression</em>'.
	 * @see test.checks.expressions.MultiplicityExpression
	 * @generated
	 */
	EClass getMultiplicityExpression();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.expressions.MultiplicityExpression#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature</em>'.
	 * @see test.checks.expressions.MultiplicityExpression#getFeature()
	 * @see #getMultiplicityExpression()
	 * @generated
	 */
	EReference getMultiplicityExpression_Feature();

	/**
	 * Returns the meta object for class '{@link test.checks.expressions.FeatureNatureExpression <em>Feature Nature Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Nature Expression</em>'.
	 * @see test.checks.expressions.FeatureNatureExpression
	 * @generated
	 */
	EClass getFeatureNatureExpression();

	/**
	 * Returns the meta object for the containment reference '{@link test.checks.expressions.FeatureNatureExpression#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Feature</em>'.
	 * @see test.checks.expressions.FeatureNatureExpression#getFeature()
	 * @see #getFeatureNatureExpression()
	 * @generated
	 */
	EReference getFeatureNatureExpression_Feature();

	/**
	 * Returns the meta object for class '{@link test.checks.expressions.FeatureWithinObjectExpression <em>Feature Within Object Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Within Object Expression</em>'.
	 * @see test.checks.expressions.FeatureWithinObjectExpression
	 * @generated
	 */
	EClass getFeatureWithinObjectExpression();

	/**
	 * Returns the meta object for the reference '{@link test.checks.expressions.FeatureWithinObjectExpression#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see test.checks.expressions.FeatureWithinObjectExpression#getObject()
	 * @see #getFeatureWithinObjectExpression()
	 * @generated
	 */
	EReference getFeatureWithinObjectExpression_Object();

	/**
	 * Returns the meta object for the reference '{@link test.checks.expressions.FeatureWithinObjectExpression#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see test.checks.expressions.FeatureWithinObjectExpression#getFeature()
	 * @see #getFeatureWithinObjectExpression()
	 * @generated
	 */
	EReference getFeatureWithinObjectExpression_Feature();

	/**
	 * Returns the meta object for class '{@link test.checks.expressions.ObjectExpression <em>Object Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Expression</em>'.
	 * @see test.checks.expressions.ObjectExpression
	 * @generated
	 */
	EClass getObjectExpression();

	/**
	 * Returns the meta object for the reference '{@link test.checks.expressions.ObjectExpression#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see test.checks.expressions.ObjectExpression#getObject()
	 * @see #getObjectExpression()
	 * @generated
	 */
	EReference getObjectExpression_Object();

	/**
	 * Returns the meta object for class '{@link test.checks.expressions.FragmentElementExpression <em>Fragment Element Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fragment Element Expression</em>'.
	 * @see test.checks.expressions.FragmentElementExpression
	 * @generated
	 */
	EClass getFragmentElementExpression();

	/**
	 * Returns the meta object for class '{@link test.checks.expressions.MetaClassExpression <em>Meta Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Class Expression</em>'.
	 * @see test.checks.expressions.MetaClassExpression
	 * @generated
	 */
	EClass getMetaClassExpression();

	/**
	 * Returns the meta object for the attribute '{@link test.checks.expressions.MetaClassExpression#getMetaclassName <em>Metaclass Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metaclass Name</em>'.
	 * @see test.checks.expressions.MetaClassExpression#getMetaclassName()
	 * @see #getMetaClassExpression()
	 * @generated
	 */
	EAttribute getMetaClassExpression_MetaclassName();

	/**
	 * Returns the meta object for class '{@link test.checks.expressions.ObjectOrMetaClassExpression <em>Object Or Meta Class Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Or Meta Class Expression</em>'.
	 * @see test.checks.expressions.ObjectOrMetaClassExpression
	 * @generated
	 */
	EClass getObjectOrMetaClassExpression();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ExpressionsFactory getExpressionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.TypeExpressionImpl <em>Type Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.TypeExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getTypeExpression()
		 * @generated
		 */
		EClass TYPE_EXPRESSION = eINSTANCE.getTypeExpression();

		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.ObjectTypeExpressionImpl <em>Object Type Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.ObjectTypeExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getObjectTypeExpression()
		 * @generated
		 */
		EClass OBJECT_TYPE_EXPRESSION = eINSTANCE.getObjectTypeExpression();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_TYPE_EXPRESSION__OBJECT = eINSTANCE.getObjectTypeExpression_Object();

		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.FeatureTypeExpressionImpl <em>Feature Type Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.FeatureTypeExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getFeatureTypeExpression()
		 * @generated
		 */
		EClass FEATURE_TYPE_EXPRESSION = eINSTANCE.getFeatureTypeExpression();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_TYPE_EXPRESSION__FEATURE = eINSTANCE.getFeatureTypeExpression_Feature();

		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.MultiplicityExpressionImpl <em>Multiplicity Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.MultiplicityExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getMultiplicityExpression()
		 * @generated
		 */
		EClass MULTIPLICITY_EXPRESSION = eINSTANCE.getMultiplicityExpression();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MULTIPLICITY_EXPRESSION__FEATURE = eINSTANCE.getMultiplicityExpression_Feature();

		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.FeatureNatureExpressionImpl <em>Feature Nature Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.FeatureNatureExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getFeatureNatureExpression()
		 * @generated
		 */
		EClass FEATURE_NATURE_EXPRESSION = eINSTANCE.getFeatureNatureExpression();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_NATURE_EXPRESSION__FEATURE = eINSTANCE.getFeatureNatureExpression_Feature();

		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.FeatureWithinObjectExpressionImpl <em>Feature Within Object Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.FeatureWithinObjectExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getFeatureWithinObjectExpression()
		 * @generated
		 */
		EClass FEATURE_WITHIN_OBJECT_EXPRESSION = eINSTANCE.getFeatureWithinObjectExpression();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_WITHIN_OBJECT_EXPRESSION__OBJECT = eINSTANCE.getFeatureWithinObjectExpression_Object();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_WITHIN_OBJECT_EXPRESSION__FEATURE = eINSTANCE.getFeatureWithinObjectExpression_Feature();

		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.ObjectExpressionImpl <em>Object Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.ObjectExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getObjectExpression()
		 * @generated
		 */
		EClass OBJECT_EXPRESSION = eINSTANCE.getObjectExpression();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_EXPRESSION__OBJECT = eINSTANCE.getObjectExpression_Object();

		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.FragmentElementExpressionImpl <em>Fragment Element Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.FragmentElementExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getFragmentElementExpression()
		 * @generated
		 */
		EClass FRAGMENT_ELEMENT_EXPRESSION = eINSTANCE.getFragmentElementExpression();

		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.MetaClassExpressionImpl <em>Meta Class Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.MetaClassExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getMetaClassExpression()
		 * @generated
		 */
		EClass META_CLASS_EXPRESSION = eINSTANCE.getMetaClassExpression();

		/**
		 * The meta object literal for the '<em><b>Metaclass Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute META_CLASS_EXPRESSION__METACLASS_NAME = eINSTANCE.getMetaClassExpression_MetaclassName();

		/**
		 * The meta object literal for the '{@link test.checks.expressions.impl.ObjectOrMetaClassExpressionImpl <em>Object Or Meta Class Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see test.checks.expressions.impl.ObjectOrMetaClassExpressionImpl
		 * @see test.checks.expressions.impl.ExpressionsPackageImpl#getObjectOrMetaClassExpression()
		 * @generated
		 */
		EClass OBJECT_OR_META_CLASS_EXPRESSION = eINSTANCE.getObjectOrMetaClassExpression();

	}

} //ExpressionsPackage
