/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.general;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import metabup.assistance.lib.*;


public class AssistanceTipsFactory {
	private HashMap<String, Class<? extends AssistanceTip>> assistanceTips = new HashMap<String, Class<? extends AssistanceTip>>();
	
	public boolean initializeFactory(){
		//tomar todas las anotaciones de la librer�a y cargarlas en assistanceTips				
		
		this.addAssistanceTip(ReplaceClassByIntAssistance.class);
		this.addAssistanceTip(InLineClassAssistance.class);
		this.addAssistanceTip(PullUpFeaturesAssistance.class);
		this.addAssistanceTip(PluralReferenceNameAssistance.class);
		this.addAssistanceTip(SingularReferenceNameAssistance.class);
		this.addAssistanceTip(RemoveFeaturelessAbstractClassAssistance.class);
		this.addAssistanceTip(ClassPrefixAssistance.class);
		return true;
		
		/*Bundle plugin = Activator.getDefault().getBundle();
		IPath relativePagePath = new Path(PreferenceConstants.P_ASSISTANCE_TIPS_LIB_FILES);		
		URL fileInPlugin = FileLocator.find(plugin, relativePagePath, null);
					
		if(fileInPlugin != null){
			// the file exists			
			URL fileUrl;			
			try {
				fileUrl = FileLocator.toFileURL(fileInPlugin);
				File file = new File(fileUrl.getPath());
				
				if(!file.exists()){
					Activator.log(IStatus.WARNING, "Generic annotation list couldn't be loaded 0");
					return false;
				}
				
				Set<Class<? extends AssistanceTip>> list = getClassesFromLib(file.getPath(), "metabup.annotations.assistance.lib");
				if(list.isEmpty()){
					Activator.log(IStatus.WARNING, "Generic annotation list couldn't be loaded 1");
					return false;
				}
				else{
					for(Class<? extends AssistanceTip> ann : list){						
						this.addAssistanceTip(ann);
						//System.out.println("putting " + ann.getSimpleName() + " in the assistance tips factory...");
					}
					
					return true;
				}		
			} catch (IOException e) {
				Activator.log(IStatus.WARNING, "Generic annotation list couldn't be loaded 2");
			} catch ( ClassNotFoundException e) {
				Activator.log(IStatus.WARNING, "Generic annotation list couldn't be loaded 2");
			}

				
		}
		return false;*/
	}
	
	public void addAssistanceTip(Class<? extends AssistanceTip> tip){
		////System.out.println("Adding tip: " + tip.getSimpleName());
		assistanceTips.put(tip.getSimpleName().toLowerCase(), tip);
	}
	
	public Class<? extends AssistanceTip> getAssistanceTip(String name){
		name = name.toLowerCase();
		Class<? extends AssistanceTip> ann = assistanceTips.get(name);
		if(ann != null) return ann;
		else return null;
	}
	
	public Collection<Class<? extends AssistanceTip>> listAllAssitanceTips(){
		return assistanceTips.values();
	}
	
	public String[] listAllAssistanceTipNames(){
		return assistanceTips.keySet().toArray(new String[assistanceTips.size()]);
	}		
	
	/*
	private boolean removeAnnotation(String name){
		//eliminar anotaci�n de assistanceTips (no parece necesario de momento)
	}
	*/
	static Set<Class<? extends AssistanceTip>> getClassesFromLib(String jar, String packageName) throws ClassNotFoundException, FileNotFoundException, IOException {
		   Set<Class<? extends AssistanceTip>> classes = new HashSet<Class<? extends AssistanceTip>>();
		   JarInputStream jarFile = new JarInputStream(new FileInputStream(jar));
		   JarEntry jarEntry;
		   packageName = packageName.replace('.', '/');		   
		   do {
		      jarEntry = jarFile.getNextJarEntry();		      
		      if (jarEntry != null) {
		         String className = jarEntry.getName();		         
		         if (className.endsWith(".class")) {
		            className = className.substring(0, className.indexOf(".class"));
		            String onlyClassName = className.substring(packageName.length()+1);
		            if(!onlyClassName.contains("/")) {
		            	className = className.replace('/', '.');
		            	Class<?> clas = Class.forName(className);
		            	classes.add((Class<? extends AssistanceTip>)clas);		            	
		            }
		         }
		      }
		   } while (jarEntry != null);
		   return classes;
		}
}
