/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.validation;

import java.util.ArrayList;
import java.util.List;

import metabup.annotations.general.AnnotationFactory;
import metabup.change.trace.MetaModelChangeChecker;
import metabup.issues.general.categories.Conflict;
import metabup.metamodel.*;
import metabup.transformations.InduceMetaModel;
import metabup.transformations.models.WFragmentModel;
import metabup.transformations.models.WMetamodelModel;

import org.eclipse.emf.ecore.resource.impl.ResourceImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.FragmentsFactory;

public class ValidateFragment {	
	private Fragment fragment;
	private boolean valid = false;
	
	private List<String> explanation = new ArrayList<String>();
	
	public ValidateFragment(Fragment fragment){
		this.fragment = fragment;
	}
	
	public boolean validate(MetaModel metamodel){
		if(metamodel == null){
			//System.out.println("No metamodel to evaluate");
			return false;
		}
		
		MetaModel auxMetamodel = EcoreUtil.copy(metamodel);
		
		FragmentModel fm = FragmentsFactory.eINSTANCE.createFragmentModel();
		fm.getFragments().add(fragment);
		ResourceImpl frgResource = new ResourceImpl();
		frgResource.getContents().add(fm);
		
		ResourceImpl metamodelResource = new ResourceImpl();
		metamodelResource.getContents().add(auxMetamodel);
		
		MetaModelChangeChecker changeChecker = new MetaModelChangeChecker(metamodel, auxMetamodel);
		AnnotationFactory af = new AnnotationFactory();
		af.initializeFactory();
		InduceMetaModel im = new InduceMetaModel(new WFragmentModel(frgResource), new WMetamodelModel(metamodelResource), af);
		im.execute(false);
		explanation.clear();
		
		boolean conflicts;
		if(im.getConflictIssues() != null && !im.getConflictIssues().isEmpty()){
			conflicts = true;
			explainConflicts(im.getConflictIssues());
		}else conflicts = false;
		
		boolean changed = changeChecker.changed();
		if(changed) explainChanges(changeChecker);
		
		boolean abstractInst = instancesFromAbstract(fragment, auxMetamodel);
		
		if(!conflicts && !changed && !abstractInst) this.valid = true;
		else this.valid = false;
		
		return valid;
	}	
		
	private void explainConflicts(List<Conflict> conflictIssues) {
		for(Conflict c : conflictIssues) explanation.add(c.getDescription());
	}

	private boolean instancesFromAbstract(Fragment fragment, MetaModel auxMetamodel){
		List<fragments.Object> abstractInstances = new ArrayList<fragments.Object>();
		
		for(fragments.Object o : fragment.getObjects()){
			String type = o.getType();
			for(MetaClass mc : auxMetamodel.getClasses())
				if(mc.getName().equals(type) && mc.getIsAbstract()) abstractInstances.add(o);						
		}
		
		if(abstractInstances.isEmpty()) return false;
		else{
			for(fragments.Object o : abstractInstances){
				String line = "The object \'" + o.getName() + "\' instances an abstract MetaClass";
				explanation.add(line);
			}
			
			return true;
		}
	}	
		
	private void explainChanges(MetaModelChangeChecker changeChecker) {				
		for(AnnotatedElement ae : changeChecker.getDeleted()){
			String line = "The ";
			if(ae instanceof MetaClass) line += "metaclass " + ((MetaClass)ae).getName();
			else if(ae instanceof Reference) line += "reference " + ((Reference)ae).getName() + " from metaclass " + ((MetaClass)ae.eContainer()).getName();
			else if(ae instanceof Attribute) line += "attribute " + ((Attribute)ae).getName() + " from metaclass " + ((MetaClass)ae.eContainer()).getName();
			line += " would be removed from the meta-model.";
			explanation.add(line);
		}
		
		for(MetaClass mcs[] : changeChecker.getDeletedInheritances()){
			String line = "The inheritance reference from " + mcs[0].getName() + " with " + mcs[1].getName() + " would be removed from the meta-model.";
			explanation.add(line);			
		}
		
		for(AnnotatedElement ae : changeChecker.getNew()){
			String line = "The ";
			if(ae instanceof MetaClass) line += "metaclass " + ((MetaClass)ae).getName();
			else if(ae instanceof Reference) line += "reference " + ((Reference)ae).getName() + " from metaclass " + ((MetaClass)ae.eContainer()).getName();
			else if(ae instanceof Attribute) line += "attribute " + ((Attribute)ae).getName() + " from metaclass " + ((MetaClass)ae.eContainer()).getName();
			line += " is not currently present in the meta-model.";
			explanation.add(line);
		}
		
		for(MetaClass mcs[] : changeChecker.getNewInheritances()){
			String line = "The inheritance reference from " + mcs[0].getName() + " with " + mcs[1].getName() + " is not currently present in the meta-model.";
			explanation.add(line);			
		}
		
		for(AnnotatedElement ae : changeChecker.getUpdated(false)){
			String line = "The ";
			if(ae instanceof MetaClass) line += "metaclass " + ((MetaClass)ae).getName();
			else if(ae instanceof Reference) line += "reference " + ((Reference)ae).getName() + " from metaclass " + ((MetaClass)ae.eContainer()).getName();
			else if(ae instanceof Attribute) line += "attribute " + ((Attribute)ae).getName() + " from metaclass " + ((MetaClass)ae.eContainer()).getName();
			line += " would introduce changes in its properties.";
			explanation.add(line);
		}
	}
	
	public List<String> getExplanation(){
		return this.explanation;
	}
	
	public boolean isValid(){
		if(fragment.getFtype().getLiteral().equals("positive")) return this.valid;
		else return !this.valid;
	}
	
	@Override
	public String toString(){
		String pon = null;
		if(fragment.getFtype().getLiteral().equals("positive")) pon = " (+)";
		else pon = " (-)";
		return fragment.getName() + pon;
	}
}
