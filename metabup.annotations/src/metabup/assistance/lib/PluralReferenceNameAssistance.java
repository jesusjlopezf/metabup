/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.assistance.lib;

import java.util.ArrayList;
import java.util.List;

import metabup.annotations.catalog.refactoring.Pluralize;
import metabup.assistance.IAssistanceTip;
import metabup.assistance.general.AssistanceTip;
import metabup.lexicon.utils.LexicalInflector;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.Reference;

public class PluralReferenceNameAssistance extends AssistanceTip implements IAssistanceTip {
	public PluralReferenceNameAssistance(){
		super();
		//this.setAnnotation(new Pluralize());
	}
	
	public boolean calculateSolutions() {		
		////System.out.println("hey there");
		for(MetaClass mc : this.getTargetMetaModel().getClasses()){
			////System.out.println("I'm checking class " + mc.getName());
			for(Feature f : mc.getFeatures()){				
				if(f instanceof Reference){
					////System.out.println("I'm checking reference " + f.getName());
					Reference r = (Reference)f;
					
					if(LexicalInflector.isSingular(r.getName()) && (r.getMax() == -1 || r.getMax() > 1)){						
						List<AnnotatedElement> set = new ArrayList<AnnotatedElement>();
						set.add(r);
						super.putSolution("The reference \'" + r.getName() + "\' has a singular name and plural multiplicity. It could be renamed \'" + LexicalInflector.pluralize(r.getName()) + "\'", set, new Pluralize());
					}				
				}
			}
		}
		
		if(super.solutions.isEmpty()) return false;
		else return true;
	}


	
}
