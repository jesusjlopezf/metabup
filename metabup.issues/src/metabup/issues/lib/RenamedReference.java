/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.issues.lib;

import metabup.issues.general.categories.Automatic;
import metabup.metamodel.Reference;

public class RenamedReference extends Automatic {
	private String oldName;
	
	public RenamedReference(Reference r, String oldName){
		if(r != null){
			super.elements.add(r);
			this.oldName = oldName;
			super.setDescription("Reference was renamed: \'" + r.getName() + "\'. Undo this action");
		}		
	}
	
	@Override
	public void run() {
		Reference r = (Reference)elements.get(0);				
		r.setName(oldName);			
	}

}
