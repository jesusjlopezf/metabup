/****************************************************************************
 * This demo file is part of the yFiles extension package ySVG-2.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * ySVG demo files exhibit ySVG functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a ySVG version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own ySVG powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the ySVG
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.svg;

import yext.svg.view.SVGNodeRealizer;

import y.view.Arrow;

import java.awt.EventQueue;
import java.io.File;
import java.net.URL;

/**
 *  Demonstrates the usage of a {@link SVGNodeRealizer} which displays a node
 *  as a scalable vector graphic.
 *  <p>
 *  The SVG file describing the graphics of the node to be displayed
 *  can be given on the commandline. See the resource folder for 
 *  examples of SVG files.
 *  </p>
 * @see <a href="http://docs.yworks.com/yfiles/doc/developers-guide/svg_usage.html#svg_content">Section Using the ySVG Extension Package</a> in the yFiles for Java Developer's Guide
 */
public class SimpleSVGNodeRealizerDemo extends SVGExportDemo
{
  /**
   * Initializes a new instance of <code>SimpleSVGNodeRealizerDemo</code>.
   * @param svgURL  an <code>URL</code> pointing to a SVG resource.
   */
  public SimpleSVGNodeRealizerDemo(URL svgURL)
  {
    if (svgURL != null) {
      view.getGraph2D().setDefaultNodeRealizer(new SVGNodeRealizer(svgURL));
    }
    view.getGraph2D().getDefaultEdgeRealizer().setArrow(Arrow.WHITE_DELTA);
  }

  /**
   * Launches this demo.
   * @param args may be used to specify a resource path to a SVG document
   * that is used as the default node representation.
   */
  public static void main(final String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        initLnF();
        (new SimpleSVGNodeRealizerDemo(
                args.length > 0
                ? getURL(args[0])
                : getURL("resource/svg/logo.svg"))).start();
      }
    });
  }

  static URL getURL(String res)
  {
    try {
      URL url = SimpleSVGNodeRealizerDemo.class.getResource(res);
      if (url == null) {
        url = new File(res).toURL();
      }
      return url;
    } catch(Exception ex) {
      System.err.println("Cannot find SVG resource " + res);
      ex.printStackTrace(System.err);
      return null;
    }
  }
}
