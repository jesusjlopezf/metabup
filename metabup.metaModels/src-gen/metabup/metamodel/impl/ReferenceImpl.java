/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metabup.metamodel.impl;

import metabup.metamodel.*;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import graphicProperties.EdgeProperties;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link metabup.metamodel.impl.ReferenceImpl#getReference <em>Reference</em>}</li>
 *   <li>{@link metabup.metamodel.impl.ReferenceImpl#getGraphicProperties <em>Graphic Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReferenceImpl extends FeatureImpl implements Reference {
	public static int IRRELEVANT = -1;
	public static int ASSOCIATION = 0;
	public static int CONTAINMENT = 1;
	
	/**
	 * The cached value of the '{@link #getReference() <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReference()
	 * @generated
	 * @ordered
	 */
	protected MetaClass reference;

	/**
	 * The cached value of the '{@link #getGraphicProperties() <em>Graphic Properties</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGraphicProperties()
	 * @generated
	 * @ordered
	 */
	protected EdgeProperties graphicProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	protected ReferenceImpl() {
		super();
		this.setId(EcoreUtil.generateUUID());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodelPackage.Literals.REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaClass getReference() {
		if (reference != null && reference.eIsProxy()) {
			InternalEObject oldReference = (InternalEObject)reference;
			reference = (MetaClass)eResolveProxy(oldReference);
			if (reference != oldReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MetamodelPackage.REFERENCE__REFERENCE, oldReference, reference));
			}
		}
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaClass basicGetReference() {
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReference(MetaClass newReference) {
		MetaClass oldReference = reference;
		reference = newReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.REFERENCE__REFERENCE, oldReference, reference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EdgeProperties getGraphicProperties() {
		return graphicProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGraphicProperties(EdgeProperties newGraphicProperties, NotificationChain msgs) {
		EdgeProperties oldGraphicProperties = graphicProperties;
		graphicProperties = newGraphicProperties;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodelPackage.REFERENCE__GRAPHIC_PROPERTIES, oldGraphicProperties, newGraphicProperties);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGraphicProperties(EdgeProperties newGraphicProperties) {
		if (newGraphicProperties != graphicProperties) {
			NotificationChain msgs = null;
			if (graphicProperties != null)
				msgs = ((InternalEObject)graphicProperties).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodelPackage.REFERENCE__GRAPHIC_PROPERTIES, null, msgs);
			if (newGraphicProperties != null)
				msgs = ((InternalEObject)newGraphicProperties).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodelPackage.REFERENCE__GRAPHIC_PROPERTIES, null, msgs);
			msgs = basicSetGraphicProperties(newGraphicProperties, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodelPackage.REFERENCE__GRAPHIC_PROPERTIES, newGraphicProperties, newGraphicProperties));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodelPackage.REFERENCE__GRAPHIC_PROPERTIES:
				return basicSetGraphicProperties(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodelPackage.REFERENCE__REFERENCE:
				if (resolve) return getReference();
				return basicGetReference();
			case MetamodelPackage.REFERENCE__GRAPHIC_PROPERTIES:
				return getGraphicProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodelPackage.REFERENCE__REFERENCE:
				setReference((MetaClass)newValue);
				return;
			case MetamodelPackage.REFERENCE__GRAPHIC_PROPERTIES:
				setGraphicProperties((EdgeProperties)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodelPackage.REFERENCE__REFERENCE:
				setReference((MetaClass)null);
				return;
			case MetamodelPackage.REFERENCE__GRAPHIC_PROPERTIES:
				setGraphicProperties((EdgeProperties)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodelPackage.REFERENCE__REFERENCE:
				return reference != null;
			case MetamodelPackage.REFERENCE__GRAPHIC_PROPERTIES:
				return graphicProperties != null;
		}
		return super.eIsSet(featureID);
	}
	
	public boolean isAnnotated(String name){
		for(Annotation a : this.getAnnotations())
			if(a.getName().equals(name))
				return true;
		
		return false;
	}
	
	@Override
	public String toString(){
		String parentClass = ((MetaClass)this.eContainer).getName();
		String targetClass = this.getReference().getName();
		String str = parentClass + " : " + this.getName() + " -> " + targetClass;
		return str;
	}

	@Override
	public Reference getOpposite() {
		if(this.isAnnotated("opposite")){
			for(Annotation ann : this.getAnnotations()){
				if(ann.getName().equals("opposite")){
					for(AnnotationParam ap : ann.getParams()){
						if(ap.getName().equals("opp")){
							ElementValue ev = (ElementValue)ap.getValue();
							return (Reference)ev.getValue();
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public void annotate(String name){
		if(!this.isAnnotated(name)){
			metabup.metamodel.Annotation ann = MetamodelFactory.eINSTANCE.createAnnotation();
			ann.setName(name);
			this.getAnnotations().add(ann);
		}
	}
	
	public Annotation getAnnotationByName(String name){
		for(Annotation ann : this.getAnnotations())
			if(ann.getName().toLowerCase().equals(name.toLowerCase()))
				return ann;
				
		return null;
	}
} //ReferenceImpl