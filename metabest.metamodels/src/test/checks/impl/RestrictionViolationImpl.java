/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import test.checks.ChecksPackage;
import test.checks.RestrictionViolation;

import test.checks.expressions.FragmentElementExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Restriction Violation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link test.checks.impl.RestrictionViolationImpl#getFragmentElement <em>Fragment Element</em>}</li>
 *   <li>{@link test.checks.impl.RestrictionViolationImpl#getRestrictionName <em>Restriction Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RestrictionViolationImpl extends CheckImpl implements RestrictionViolation {
	/**
	 * The cached value of the '{@link #getFragmentElement() <em>Fragment Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFragmentElement()
	 * @generated
	 * @ordered
	 */
	protected FragmentElementExpression fragmentElement;

	/**
	 * The default value of the '{@link #getRestrictionName() <em>Restriction Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRestrictionName()
	 * @generated
	 * @ordered
	 */
	protected static final String RESTRICTION_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRestrictionName() <em>Restriction Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRestrictionName()
	 * @generated
	 * @ordered
	 */
	protected String restrictionName = RESTRICTION_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RestrictionViolationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChecksPackage.Literals.RESTRICTION_VIOLATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FragmentElementExpression getFragmentElement() {
		return fragmentElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFragmentElement(FragmentElementExpression newFragmentElement, NotificationChain msgs) {
		FragmentElementExpression oldFragmentElement = fragmentElement;
		fragmentElement = newFragmentElement;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ChecksPackage.RESTRICTION_VIOLATION__FRAGMENT_ELEMENT, oldFragmentElement, newFragmentElement);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFragmentElement(FragmentElementExpression newFragmentElement) {
		if (newFragmentElement != fragmentElement) {
			NotificationChain msgs = null;
			if (fragmentElement != null)
				msgs = ((InternalEObject)fragmentElement).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.RESTRICTION_VIOLATION__FRAGMENT_ELEMENT, null, msgs);
			if (newFragmentElement != null)
				msgs = ((InternalEObject)newFragmentElement).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.RESTRICTION_VIOLATION__FRAGMENT_ELEMENT, null, msgs);
			msgs = basicSetFragmentElement(newFragmentElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChecksPackage.RESTRICTION_VIOLATION__FRAGMENT_ELEMENT, newFragmentElement, newFragmentElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRestrictionName() {
		return restrictionName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRestrictionName(String newRestrictionName) {
		String oldRestrictionName = restrictionName;
		restrictionName = newRestrictionName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChecksPackage.RESTRICTION_VIOLATION__RESTRICTION_NAME, oldRestrictionName, restrictionName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ChecksPackage.RESTRICTION_VIOLATION__FRAGMENT_ELEMENT:
				return basicSetFragmentElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChecksPackage.RESTRICTION_VIOLATION__FRAGMENT_ELEMENT:
				return getFragmentElement();
			case ChecksPackage.RESTRICTION_VIOLATION__RESTRICTION_NAME:
				return getRestrictionName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChecksPackage.RESTRICTION_VIOLATION__FRAGMENT_ELEMENT:
				setFragmentElement((FragmentElementExpression)newValue);
				return;
			case ChecksPackage.RESTRICTION_VIOLATION__RESTRICTION_NAME:
				setRestrictionName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChecksPackage.RESTRICTION_VIOLATION__FRAGMENT_ELEMENT:
				setFragmentElement((FragmentElementExpression)null);
				return;
			case ChecksPackage.RESTRICTION_VIOLATION__RESTRICTION_NAME:
				setRestrictionName(RESTRICTION_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChecksPackage.RESTRICTION_VIOLATION__FRAGMENT_ELEMENT:
				return fragmentElement != null;
			case ChecksPackage.RESTRICTION_VIOLATION__RESTRICTION_NAME:
				return RESTRICTION_NAME_EDEFAULT == null ? restrictionName != null : !RESTRICTION_NAME_EDEFAULT.equals(restrictionName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (restrictionName: ");
		result.append(restrictionName);
		result.append(')');
		return result.toString();
	}

} //RestrictionViolationImpl
