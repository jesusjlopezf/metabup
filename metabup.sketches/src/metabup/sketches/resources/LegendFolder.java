/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches.resources;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import metabup.sketches.legend.Legend;
import metabup.sketches.legend.LegendElement;
import metabup.sketches.legend.LegendFactory;
import metabup.sketches.properties.SketchPersistentProperties;

public class LegendFolder implements ErrorHandler {
	private IProject project;
	public static String LEGEND_FOLDER_NAME = "legend";
	public static String JPG_EXTENSION = ".jpg";
	public static String SVG_EXTENSION = ".svg";
	private IFolder folder;
	
	public LegendFolder(IProject project) {
		this.project = project;
		folder = this.project.getFolder(LEGEND_FOLDER_NAME);
	}
	
	public boolean exists(){
		if(folder.exists()) return true;
		return false;
	}
	
	public void refresh(){
		try {
			folder.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}
	
	public boolean create(IProgressMonitor monitor) {
		if(folder == null) folder = this.project.getFolder(LEGEND_FOLDER_NAME);
		 
		try {
			if(!folder.exists()) folder.create(true, true, monitor);
			folder.setPersistentProperty(SketchPersistentProperties.FOLDER_KEY, SketchPersistentProperties.FOLDER_KEY_LEGEND);
		} catch (CoreException e) {
			return false;		
		}		
		
		return true;		
	}
	
	public Legend getLegend(){
		Legend legend = LegendFactory.eINSTANCE.createLegend();
		
		if(folder == null || !folder.exists()) return null;
		
		try {
			for(IResource r : folder.members()){
				if(r instanceof IFile){
					LegendElement element = LegendFactory.eINSTANCE.createLegendElement();
					element.setFile(r.getLocation().toOSString());
					//System.out.println("I'm here: " + element.getFile());
					element.setOriginalSymbolName(r.getName().substring(0, r.getName().lastIndexOf(".")));
					element.setNewSymbolName(r.getName().substring(0, r.getName().lastIndexOf(".")));
					legend.getElements().add(element);
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return legend;
	}
	
	/*public void convertAllSvg2Jpg(){
		try {
			for(IResource r : folder.members()){
				if(r instanceof IFile){
					if(("." + ((IFile)r).getFileExtension()).equalsIgnoreCase(SVG_EXTENSION)){
						File svgFile = EclipseIResources.IFile2File((IFile)r);
						File jpgFile = new File(svgFile.getAbsolutePath().replace(SVG_EXTENSION, JPG_EXTENSION));
						ImageUtils.svg2jpg(svgFile, jpgFile);
					}
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block			
		}
		
		try {
			folder.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e1) {
			// TODO Auto-generated catch block
		}
				
	}	*/
	
	public String getElementPath(String name, String extension){
		if(folder == null || !folder.exists()) return null;
		
		try {
			for(IResource r : folder.members()){
				if(r instanceof IFile){
					String s1 = r.getName().substring(0, r.getName().lastIndexOf(".")).toLowerCase();
					String s2 = name.toLowerCase();
					String s3 = ((IFile)r).getFileExtension();
					String s4 = extension;
					
					if(s1.equals(s2) && (s3.equalsIgnoreCase(s4.replaceFirst(".", "")))) return r.getFullPath().toString();
					else System.out.println("NOT FOUND! " + " " + s1 + " " + s2 + " " +  " " + s3 + " " + s4);
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			return null;
		}
		
		return null;
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		// TODO Auto-generated method stub
		
	}

}
