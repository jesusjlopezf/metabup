/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Phrase Named</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.PhraseNamed#isCamelized <em>Camelized</em>}</li>
 *   <li>{@link metamodeltest.PhraseNamed#getWords <em>Words</em>}</li>
 *   <li>{@link metamodeltest.PhraseNamed#getStart <em>Start</em>}</li>
 *   <li>{@link metamodeltest.PhraseNamed#getEnd <em>End</em>}</li>
 *   <li>{@link metamodeltest.PhraseNamed#isPascal <em>Pascal</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getPhraseNamed()
 * @model
 * @generated
 */
public interface PhraseNamed extends ElementQualifier {
	/**
	 * Returns the value of the '<em><b>Camelized</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Camelized</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Camelized</em>' attribute.
	 * @see #setCamelized(boolean)
	 * @see metamodeltest.MetamodeltestPackage#getPhraseNamed_Camelized()
	 * @model default="false"
	 * @generated
	 */
	boolean isCamelized();

	/**
	 * Sets the value of the '{@link metamodeltest.PhraseNamed#isCamelized <em>Camelized</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Camelized</em>' attribute.
	 * @see #isCamelized()
	 * @generated
	 */
	void setCamelized(boolean value);

	/**
	 * Returns the value of the '<em><b>Words</b></em>' containment reference list.
	 * The list contents are of type {@link metamodeltest.NamePart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Words</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Words</em>' containment reference list.
	 * @see metamodeltest.MetamodeltestPackage#getPhraseNamed_Words()
	 * @model containment="true"
	 * @generated
	 */
	EList<NamePart> getWords();

	/**
	 * Returns the value of the '<em><b>Start</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' containment reference.
	 * @see #setStart(PhraseNamed)
	 * @see metamodeltest.MetamodeltestPackage#getPhraseNamed_Start()
	 * @model containment="true"
	 * @generated
	 */
	PhraseNamed getStart();

	/**
	 * Sets the value of the '{@link metamodeltest.PhraseNamed#getStart <em>Start</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' containment reference.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(PhraseNamed value);

	/**
	 * Returns the value of the '<em><b>End</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' containment reference.
	 * @see #setEnd(PhraseNamed)
	 * @see metamodeltest.MetamodeltestPackage#getPhraseNamed_End()
	 * @model containment="true"
	 * @generated
	 */
	PhraseNamed getEnd();

	/**
	 * Sets the value of the '{@link metamodeltest.PhraseNamed#getEnd <em>End</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End</em>' containment reference.
	 * @see #getEnd()
	 * @generated
	 */
	void setEnd(PhraseNamed value);

	/**
	 * Returns the value of the '<em><b>Pascal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pascal</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pascal</em>' attribute.
	 * @see #setPascal(boolean)
	 * @see metamodeltest.MetamodeltestPackage#getPhraseNamed_Pascal()
	 * @model
	 * @generated
	 */
	boolean isPascal();

	/**
	 * Sets the value of the '{@link metamodeltest.PhraseNamed#isPascal <em>Pascal</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pascal</em>' attribute.
	 * @see #isPascal()
	 * @generated
	 */
	void setPascal(boolean value);

} // PhraseNamed
