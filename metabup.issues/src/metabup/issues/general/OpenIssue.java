/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.issues.general;

import java.util.ArrayList;
import java.util.List;

//import metabup.suggestions.extensions.ISuggestion;

public abstract class OpenIssue implements IOpenIssue {
	private String description;
	protected List<metabup.metamodel.AnnotatedElement> elements = new ArrayList<metabup.metamodel.AnnotatedElement>();
		
	/* (non-Javadoc)
	 * @see metabup.issues.lib.general.IOpenIssue#setDescription(java.lang.String)
	 */
	
	@Override
	public void setDescription(String description){
		this.description = description;
	}
	
	public String getDescription(){
		return description;
	}
	
	public String toString(){
		return description;
	}
}
