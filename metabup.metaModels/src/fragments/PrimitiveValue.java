/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fragments.FragmentsPackage#getPrimitiveValue()
 * @model abstract="true"
 * @generated
 */
public interface PrimitiveValue extends EObject {
} // PrimitiveValue
