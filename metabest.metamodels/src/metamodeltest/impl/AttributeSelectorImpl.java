/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.AttributeFilter;
import metamodeltest.AttributeSelector;
import metamodeltest.MetamodeltestPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Selector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.AttributeSelectorImpl#getAttributeFilter <em>Attribute Filter</em>}</li>
 *   <li>{@link metamodeltest.impl.AttributeSelectorImpl#getAttributeConditioner <em>Attribute Conditioner</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AttributeSelectorImpl extends SelectorImpl implements AttributeSelector {
	/**
	 * The cached value of the '{@link #getAttributeFilter() <em>Attribute Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeFilter()
	 * @generated
	 * @ordered
	 */
	protected AttributeFilter attributeFilter;

	/**
	 * The cached value of the '{@link #getAttributeConditioner() <em>Attribute Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributeConditioner()
	 * @generated
	 * @ordered
	 */
	protected AttributeFilter attributeConditioner;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeSelectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.ATTRIBUTE_SELECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeFilter getAttributeFilter() {
		return attributeFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttributeFilter(AttributeFilter newAttributeFilter, NotificationChain msgs) {
		AttributeFilter oldAttributeFilter = attributeFilter;
		attributeFilter = newAttributeFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER, oldAttributeFilter, newAttributeFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeFilter(AttributeFilter newAttributeFilter) {
		if (newAttributeFilter != attributeFilter) {
			NotificationChain msgs = null;
			if (attributeFilter != null)
				msgs = ((InternalEObject)attributeFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER, null, msgs);
			if (newAttributeFilter != null)
				msgs = ((InternalEObject)newAttributeFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER, null, msgs);
			msgs = basicSetAttributeFilter(newAttributeFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER, newAttributeFilter, newAttributeFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeFilter getAttributeConditioner() {
		return attributeConditioner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAttributeConditioner(AttributeFilter newAttributeConditioner, NotificationChain msgs) {
		AttributeFilter oldAttributeConditioner = attributeConditioner;
		attributeConditioner = newAttributeConditioner;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER, oldAttributeConditioner, newAttributeConditioner);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeConditioner(AttributeFilter newAttributeConditioner) {
		if (newAttributeConditioner != attributeConditioner) {
			NotificationChain msgs = null;
			if (attributeConditioner != null)
				msgs = ((InternalEObject)attributeConditioner).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER, null, msgs);
			if (newAttributeConditioner != null)
				msgs = ((InternalEObject)newAttributeConditioner).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER, null, msgs);
			msgs = basicSetAttributeConditioner(newAttributeConditioner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER, newAttributeConditioner, newAttributeConditioner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER:
				return basicSetAttributeFilter(null, msgs);
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER:
				return basicSetAttributeConditioner(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER:
				return getAttributeFilter();
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER:
				return getAttributeConditioner();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER:
				setAttributeFilter((AttributeFilter)newValue);
				return;
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER:
				setAttributeConditioner((AttributeFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER:
				setAttributeFilter((AttributeFilter)null);
				return;
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER:
				setAttributeConditioner((AttributeFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_FILTER:
				return attributeFilter != null;
			case MetamodeltestPackage.ATTRIBUTE_SELECTOR__ATTRIBUTE_CONDITIONER:
				return attributeConditioner != null;
		}
		return super.eIsSet(featureID);
	}

} //AttributeSelectorImpl
