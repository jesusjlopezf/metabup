/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import test.checks.ChecksPackage;
import test.checks.MultiplicityMismatch;

import test.checks.expressions.MultiplicityExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Multiplicity Mismatch</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link test.checks.impl.MultiplicityMismatchImpl#getFeatureMultiplicity <em>Feature Multiplicity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MultiplicityMismatchImpl extends MismatchImpl implements MultiplicityMismatch {
	/**
	 * The cached value of the '{@link #getFeatureMultiplicity() <em>Feature Multiplicity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureMultiplicity()
	 * @generated
	 * @ordered
	 */
	protected MultiplicityExpression featureMultiplicity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MultiplicityMismatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChecksPackage.Literals.MULTIPLICITY_MISMATCH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityExpression getFeatureMultiplicity() {
		return featureMultiplicity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFeatureMultiplicity(MultiplicityExpression newFeatureMultiplicity, NotificationChain msgs) {
		MultiplicityExpression oldFeatureMultiplicity = featureMultiplicity;
		featureMultiplicity = newFeatureMultiplicity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ChecksPackage.MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY, oldFeatureMultiplicity, newFeatureMultiplicity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeatureMultiplicity(MultiplicityExpression newFeatureMultiplicity) {
		if (newFeatureMultiplicity != featureMultiplicity) {
			NotificationChain msgs = null;
			if (featureMultiplicity != null)
				msgs = ((InternalEObject)featureMultiplicity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY, null, msgs);
			if (newFeatureMultiplicity != null)
				msgs = ((InternalEObject)newFeatureMultiplicity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ChecksPackage.MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY, null, msgs);
			msgs = basicSetFeatureMultiplicity(newFeatureMultiplicity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChecksPackage.MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY, newFeatureMultiplicity, newFeatureMultiplicity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ChecksPackage.MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY:
				return basicSetFeatureMultiplicity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChecksPackage.MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY:
				return getFeatureMultiplicity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChecksPackage.MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY:
				setFeatureMultiplicity((MultiplicityExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChecksPackage.MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY:
				setFeatureMultiplicity((MultiplicityExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChecksPackage.MULTIPLICITY_MISMATCH__FEATURE_MULTIPLICITY:
				return featureMultiplicity != null;
		}
		return super.eIsSet(featureID);
	}

} //MultiplicityMismatchImpl
