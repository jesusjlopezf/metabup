/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.refactoring;

import java.util.List;

import metabup.annotations.extensionpoint.RefactoringAnnotation;
import metabup.annotations.general.Annotation;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;

public class Group extends RefactoringAnnotation {
	
	public Group(){
		// does this really do something ? 
		super.setName("group");
		super.setDescription("Take the annotated reference within a MetaClass, seek those equally named, and makes a single one of them, set as * multiplicity.");
	}
	
	@Override
	public MetaModel perform() {
		if(element instanceof Reference){
			MetaClass mc = (MetaClass) element.eContainer();
			List<Reference> refs = super.allReferencesFromMetaClassWithName(((Reference) element).getName(), mc);
			
			for(Reference r : refs){
				super.removeFeatureWithName(mc, r.getName());
			}
			
			((Reference)element).setMax(-1);
			mc.getFeatures().add((Reference)element);
			
			return targetMetaModel;
		}else return null;		
	}

}
