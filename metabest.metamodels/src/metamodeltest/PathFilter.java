/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.PathFilter#getPathQualifier <em>Path Qualifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getPathFilter()
 * @model
 * @generated
 */
public interface PathFilter extends Filter {
	/**
	 * Returns the value of the '<em><b>Path Qualifier</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Path Qualifier</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Path Qualifier</em>' containment reference.
	 * @see #setPathQualifier(PathQualifier)
	 * @see metamodeltest.MetamodeltestPackage#getPathFilter_PathQualifier()
	 * @model containment="true"
	 * @generated
	 */
	PathQualifier getPathQualifier();

	/**
	 * Sets the value of the '{@link metamodeltest.PathFilter#getPathQualifier <em>Path Qualifier</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Path Qualifier</em>' containment reference.
	 * @see #getPathQualifier()
	 * @generated
	 */
	void setPathQualifier(PathQualifier value);

} // PathFilter
