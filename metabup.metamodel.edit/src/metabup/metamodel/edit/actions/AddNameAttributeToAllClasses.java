/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.metamodel.edit.actions;

import metabup.annotations.catalog.constraint.Root;
import metabup.annotations.catalog.design.Composition;
import metabup.extensionpoints.MetaBupMetaModelEditActionContribution;
import metabup.lexicon.wordnet.Wordnet;
import metabup.metamodel.Annotation;
import metabup.metamodel.Attribute;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.Reference;
import metabup.metamodel.impl.ReferenceImpl;

public class AddNameAttributeToAllClasses extends MetaBupMetaModelEditActionContribution {

	@Override
	public boolean checkConditions(MetaModel mm) {
		if(mm != null) return true;
		return false;
	}

	@Override
	public MetaModel editMetaModel(MetaModel mm) {
		for(MetaClass mc : mm.getClasses()){
			if(mc.isTop() && mc.getFeatureByName("name", true) == null){
				Attribute a = MetamodelFactory.eINSTANCE.createAttribute();
				a.setName("name");
				a.setMin(1);
				a.setMax(1);
				a.setPrimitiveType("StringType");				
				mc.getFeatures().add(a);
			}
		}
		
		for(MetaClass mc : mm.getClasses()){
			if(!mc.isTop() && mc.getFeatureByName("name", true) == null){
				Attribute a = MetamodelFactory.eINSTANCE.createAttribute();
				a.setName("name");
				a.setMin(1);
				a.setMax(1);
				a.setPrimitiveType("StringType");				
				mc.getFeatures().add(a);
			}
		}
		
		return mm;
	}

}
