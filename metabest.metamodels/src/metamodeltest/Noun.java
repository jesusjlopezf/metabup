/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Noun</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.Noun#getNum <em>Num</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getNoun()
 * @model
 * @generated
 */
public interface Noun extends WordNature {

	/**
	 * Returns the value of the '<em><b>Num</b></em>' attribute.
	 * The literals are from the enumeration {@link metamodeltest.GrammaticalNumber}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num</em>' attribute.
	 * @see metamodeltest.GrammaticalNumber
	 * @see #setNum(GrammaticalNumber)
	 * @see metamodeltest.MetamodeltestPackage#getNoun_Num()
	 * @model
	 * @generated
	 */
	GrammaticalNumber getNum();

	/**
	 * Sets the value of the '{@link metamodeltest.Noun#getNum <em>Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num</em>' attribute.
	 * @see metamodeltest.GrammaticalNumber
	 * @see #getNum()
	 * @generated
	 */
	void setNum(GrammaticalNumber value);
} // Noun
