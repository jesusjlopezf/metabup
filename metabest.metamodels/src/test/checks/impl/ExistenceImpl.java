/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.impl;

import org.eclipse.emf.ecore.EClass;

import test.checks.ChecksPackage;
import test.checks.Existence;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Existence</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ExistenceImpl extends CheckImpl implements Existence {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExistenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChecksPackage.Literals.EXISTENCE;
	}

} //ExistenceImpl
