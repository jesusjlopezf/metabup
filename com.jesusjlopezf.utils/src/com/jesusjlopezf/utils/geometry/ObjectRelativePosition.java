package com.jesusjlopezf.utils.geometry;

public class ObjectRelativePosition{
	public static final int UNIDENTIFIED = -1;
	public static final int NONE = 0;
	public static final int TOP = 1;
	public static final int RIGHT = 2;
	public static final int BOTTOM = 3;
	public static final int LEFT = 4;
	
	public static int getOppositePosition(int pos){
		if(pos == TOP) return BOTTOM;
		if(pos == BOTTOM) return TOP;
		if(pos == LEFT) return RIGHT;
		if(pos == RIGHT) return LEFT;
		else return UNIDENTIFIED;
	}
	
	public static String getPositionString(int pos){
		switch (pos){
		case ObjectRelativePosition.BOTTOM:
			return "bottom";
		case ObjectRelativePosition.LEFT:
			return "left";
		case ObjectRelativePosition.RIGHT:
			return "right";
			
		case ObjectRelativePosition.TOP:
			return "top";
			
		default: return null;
		}
	}
}
