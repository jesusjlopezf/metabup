/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Element Featurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.ElementFeaturement#getElementSels <em>Element Sels</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getElementFeaturement()
 * @model
 * @generated
 */
public interface ElementFeaturement extends PathQualifier {
	/**
	 * Returns the value of the '<em><b>Element Sels</b></em>' containment reference list.
	 * The list contents are of type {@link metamodeltest.ElementFilter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Sels</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Sels</em>' containment reference list.
	 * @see metamodeltest.MetamodeltestPackage#getElementFeaturement_ElementSels()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ElementFilter> getElementSels();

} // ElementFeaturement
