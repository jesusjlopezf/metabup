/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards;

import metabup.metamodel.MetaModel;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.*;
import org.eclipse.jface.operation.*;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;

import java.io.*;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.ui.*;
import org.xml.sax.SAXException;

import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;

import choco.ContradictionException;
import metabest.solver.ui.wizards.pages.GenerateGraphMLWizardPage;
import metabest.transformations.Fragment2GraphML;

public class GenerateGraphMLWizard extends Wizard implements INewWizard {
	private GenerateGraphMLWizardPage page;
	private ISelection selection;
	private IProject project;

	/**
	 * Constructor for SampleNewWizard.
	 */
	public GenerateGraphMLWizard(IProject project, MetaModel mm) {
		super();
		setNeedsProgressMonitor(true);
		this.project = project;
	}
	
	/**
	 * Adding the page to the wizard.
	 */

	public void addPages() {
		page = new GenerateGraphMLWizardPage(selection);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in
	 * the wizard. We will create an operation and run it
	 * using wizard as execution context.
	 */
	public boolean performFinish() {
		final String fragmentFile = page.getFragmentFile();
		final String legendFile = page.getLegendFile();
		final String container = page.getContainerName();
		final String fileName = page.getFileName();
		
		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				doFinish(project, monitor, fragmentFile, legendFile, container, fileName);									
				monitor.done();
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error", realException.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * The worker method. It will find the container, create the
	 * file if missing or just replace its contents, and open
	 * the editor on the newly created file.
	 */

	private void doFinish(IProject project, IProgressMonitor monitor, String fragmentFileStr, String legendFileStr, String containerStr, String fileNameStr){
		// create a sample file
		monitor.beginTask("Generating graph...", 2);
		
		File fragmentFile = new File(fragmentFileStr);
		File legendFile = new File(legendFileStr);
		
		if(fragmentFile.exists() && legendFile.exists()){
			// whether or not to add geometry properties, should probably be asked
			Fragment2GraphML transformation = new Fragment2GraphML(fragmentFile, legendFile, project, true);				
			
			monitor.worked(1);
			
			try {
				IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
				IResource resource = root.findMember(new Path(containerStr));
				//if (!resource.exists() || !(resource instanceof IContainer)) throwCoreException("Container \"" + containerStr + "\" does not exist.");
				IContainer container = (IContainer) resource;
				final IFile file = container.getFile(new Path(fileNameStr));				
				transformation.execute(IFileUtils.IFile2File(file));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			monitor.worked(1);
		}else{
			System.out.println(fragmentFile.exists());
			System.out.println(legendFile.exists());
			monitor.setCanceled(true);
		}
	}

	/**
	 * We will accept the selection in the workbench to see if
	 * we can initialize from it.
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}