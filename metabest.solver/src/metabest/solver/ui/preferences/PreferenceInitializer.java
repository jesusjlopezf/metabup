/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import metabest.solver.Activator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.P_MIN_OBJECT_NUMBER, 1);
		store.setDefault(PreferenceConstants.P_MAX_OBJECT_NUMBER, 10);
		store.setDefault(PreferenceConstants.P_MIN_DEFAULT_REFERENCE_CARDINALITY, 0);
		store.setDefault(PreferenceConstants.P_MAX_DEFAULT_REFERENCE_CARDINALITY, 10);
		store.setDefault(PreferenceConstants.P_ADD_ATTRIBUTES, true);
		store.setDefault(PreferenceConstants.P_ADD_ASSOCIATIONS, true);
		store.setDefault(PreferenceConstants.P_ADD_CONTAINEES, true);
		store.setDefault(PreferenceConstants.P_MAKE_UNREACHABLE, false);
	}
}
