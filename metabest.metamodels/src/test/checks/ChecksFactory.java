/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see test.checks.ChecksPackage
 * @generated
 */
public interface ChecksFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ChecksFactory eINSTANCE = test.checks.impl.ChecksFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Feature Existence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Existence</em>'.
	 * @generated
	 */
	FeatureExistence createFeatureExistence();

	/**
	 * Returns a new object of class '<em>Type Existence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Existence</em>'.
	 * @generated
	 */
	TypeExistence createTypeExistence();

	/**
	 * Returns a new object of class '<em>Abstract Type Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract Type Instance</em>'.
	 * @generated
	 */
	AbstractTypeInstance createAbstractTypeInstance();

	/**
	 * Returns a new object of class '<em>Missing Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Missing Attribute</em>'.
	 * @generated
	 */
	MissingAttribute createMissingAttribute();

	/**
	 * Returns a new object of class '<em>Multiplicity Mismatch</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multiplicity Mismatch</em>'.
	 * @generated
	 */
	MultiplicityMismatch createMultiplicityMismatch();

	/**
	 * Returns a new object of class '<em>Type Mismatch</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Mismatch</em>'.
	 * @generated
	 */
	TypeMismatch createTypeMismatch();

	/**
	 * Returns a new object of class '<em>Feature Nature Mismatch</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Nature Mismatch</em>'.
	 * @generated
	 */
	FeatureNatureMismatch createFeatureNatureMismatch();

	/**
	 * Returns a new object of class '<em>Restriction Violation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Restriction Violation</em>'.
	 * @generated
	 */
	RestrictionViolation createRestrictionViolation();

	/**
	 * Returns a new object of class '<em>Uncontainment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uncontainment</em>'.
	 * @generated
	 */
	Uncontainment createUncontainment();

	/**
	 * Returns a new object of class '<em>Uncontainment By Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uncontainment By Object</em>'.
	 * @generated
	 */
	UncontainmentByObject createUncontainmentByObject();

	/**
	 * Returns a new object of class '<em>Uncontainment By Meta Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Uncontainment By Meta Class</em>'.
	 * @generated
	 */
	UncontainmentByMetaClass createUncontainmentByMetaClass();

	/**
	 * Returns a new object of class '<em>Missing Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Missing Reference</em>'.
	 * @generated
	 */
	MissingReference createMissingReference();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ChecksPackage getChecksPackage();

} //ChecksFactory
