/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import fragments.Attribute;
import fragments.Feature;
import fragments.FragmentsPackage;
import fragments.Reference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fragments.impl.ObjectImpl#getFeatures <em>Features</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObjectImpl extends TypedElementImpl implements fragments.Object {
	/**
	 * The cached value of the '{@link #getFeatures() <em>Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<Feature> features;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FragmentsPackage.Literals.OBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Feature> getFeatures() {
		if (features == null) {
			features = new EObjectContainmentEList<Feature>(Feature.class, this, FragmentsPackage.OBJECT__FEATURES);
		}
		return features;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FragmentsPackage.OBJECT__FEATURES:
				return ((InternalEList<?>)getFeatures()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FragmentsPackage.OBJECT__FEATURES:
				return getFeatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FragmentsPackage.OBJECT__FEATURES:
				getFeatures().clear();
				getFeatures().addAll((Collection<? extends Feature>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FragmentsPackage.OBJECT__FEATURES:
				getFeatures().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FragmentsPackage.OBJECT__FEATURES:
				return features != null && !features.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	@Override
	public Reference getReferenceByName(String name) {
		for(Feature f : this.getFeatures()){
			if(f instanceof Reference){
				if(f.getName().toLowerCase().equals(name.toLowerCase())) return (Reference)f;
			}
		}
		
		return null;
	}
	
	@Override
	public Attribute getAttributeByName(String name) {
		for(Feature f : this.getFeatures()){
			if(f instanceof Attribute){
				if(f.getName().toLowerCase().equals(name.toLowerCase())) return (Attribute)f;
			}
		}
		
		return null;
	}
	
	public String toString(){
		return this.getName();
	}

	@Override
	public List<Reference> getReferencesByAnnotation(String annotationName) {
		List<Reference> features = new ArrayList<Reference>();
		
		for(Feature f : this.getFeatures()){
			if(f.isAnnotated(annotationName) && f instanceof Reference){
				features.add((Reference)f);
			}
		}
		
		if(features.isEmpty()) return null;
		else return features;
	}

	@Override
	public List<Reference> getReferencesByAnnotation(String[] annotationName) {
		List<Reference> features = new ArrayList<Reference>();
		
		for(Feature f : this.getFeatures()){
			if(f instanceof Reference){
				for(String ann : annotationName){
					if(f.isAnnotated(ann)) features.add((Reference)f);

				}
			}
		}
		
		if(features.isEmpty()) return null;
		else return features;
	}

} //ObjectImpl
