/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see metamodeltest.MetamodeltestPackage
 * @generated
 */
public interface MetamodeltestFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MetamodeltestFactory eINSTANCE = metamodeltest.impl.MetamodeltestFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Meta Model Test</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Meta Model Test</em>'.
	 * @generated
	 */
	MetaModelTest createMetaModelTest();

	/**
	 * Returns a new object of class '<em>Assertion</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assertion</em>'.
	 * @generated
	 */
	Assertion createAssertion();

	/**
	 * Returns a new object of class '<em>None</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>None</em>'.
	 * @generated
	 */
	None createNone();

	/**
	 * Returns a new object of class '<em>Number</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Number</em>'.
	 * @generated
	 */
	Number createNumber();

	/**
	 * Returns a new object of class '<em>Every</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Every</em>'.
	 * @generated
	 */
	Every createEvery();

	/**
	 * Returns a new object of class '<em>Less Than</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Less Than</em>'.
	 * @generated
	 */
	LessThan createLessThan();

	/**
	 * Returns a new object of class '<em>More Than</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>More Than</em>'.
	 * @generated
	 */
	MoreThan createMoreThan();

	/**
	 * Returns a new object of class '<em>Element Assertion</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Assertion</em>'.
	 * @generated
	 */
	ElementAssertion createElementAssertion();

	/**
	 * Returns a new object of class '<em>Name</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Name</em>'.
	 * @generated
	 */
	Name createName();

	/**
	 * Returns a new object of class '<em>Abstract</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstract</em>'.
	 * @generated
	 */
	Abstract createAbstract();

	/**
	 * Returns a new object of class '<em>Element Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Filter</em>'.
	 * @generated
	 */
	ElementFilter createElementFilter();

	/**
	 * Returns a new object of class '<em>Class Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Filter</em>'.
	 * @generated
	 */
	ClassFilter createClassFilter();

	/**
	 * Returns a new object of class '<em>Feature Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Filter</em>'.
	 * @generated
	 */
	FeatureFilter createFeatureFilter();

	/**
	 * Returns a new object of class '<em>Attribute Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Filter</em>'.
	 * @generated
	 */
	AttributeFilter createAttributeFilter();

	/**
	 * Returns a new object of class '<em>Reference Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference Filter</em>'.
	 * @generated
	 */
	ReferenceFilter createReferenceFilter();

	/**
	 * Returns a new object of class '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Named</em>'.
	 * @generated
	 */
	Named createNamed();

	/**
	 * Returns a new object of class '<em>Abstraction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Abstraction</em>'.
	 * @generated
	 */
	Abstraction createAbstraction();

	/**
	 * Returns a new object of class '<em>Is Super To</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Super To</em>'.
	 * @generated
	 */
	IsSuperTo createIsSuperTo();

	/**
	 * Returns a new object of class '<em>Is Sub To</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Is Sub To</em>'.
	 * @generated
	 */
	IsSubTo createIsSubTo();

	/**
	 * Returns a new object of class '<em>Feature Containment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Containment</em>'.
	 * @generated
	 */
	FeatureContainment createFeatureContainment();

	/**
	 * Returns a new object of class '<em>Inherited Feature Containment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inherited Feature Containment</em>'.
	 * @generated
	 */
	InheritedFeatureContainment createInheritedFeatureContainment();

	/**
	 * Returns a new object of class '<em>Inh Leaf</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inh Leaf</em>'.
	 * @generated
	 */
	InhLeaf createInhLeaf();

	/**
	 * Returns a new object of class '<em>Inh Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inh Root</em>'.
	 * @generated
	 */
	InhRoot createInhRoot();

	/**
	 * Returns a new object of class '<em>Composition Leaf</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composition Leaf</em>'.
	 * @generated
	 */
	CompositionLeaf createCompositionLeaf();

	/**
	 * Returns a new object of class '<em>Max Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Max Multiplicity</em>'.
	 * @generated
	 */
	MaxMultiplicity createMaxMultiplicity();

	/**
	 * Returns a new object of class '<em>Fixed Bound</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fixed Bound</em>'.
	 * @generated
	 */
	FixedBound createFixedBound();

	/**
	 * Returns a new object of class '<em>Infinite</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Infinite</em>'.
	 * @generated
	 */
	Infinite createInfinite();

	/**
	 * Returns a new object of class '<em>Class Containment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Containment</em>'.
	 * @generated
	 */
	ClassContainment createClassContainment();

	/**
	 * Returns a new object of class '<em>Class Containee</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Containee</em>'.
	 * @generated
	 */
	ClassContainee createClassContainee();

	/**
	 * Returns a new object of class '<em>Typed</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Typed</em>'.
	 * @generated
	 */
	Typed createTyped();

	/**
	 * Returns a new object of class '<em>Prefix</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Prefix</em>'.
	 * @generated
	 */
	Prefix createPrefix();

	/**
	 * Returns a new object of class '<em>Suffix</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Suffix</em>'.
	 * @generated
	 */
	Suffix createSuffix();

	/**
	 * Returns a new object of class '<em>Contains</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Contains</em>'.
	 * @generated
	 */
	Contains createContains();

	/**
	 * Returns a new object of class '<em>Strict Number</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Strict Number</em>'.
	 * @generated
	 */
	StrictNumber createStrictNumber();

	/**
	 * Returns a new object of class '<em>Feature Multiplicity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Multiplicity</em>'.
	 * @generated
	 */
	FeatureMultiplicity createFeatureMultiplicity();

	/**
	 * Returns a new object of class '<em>Upper Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Upper Case</em>'.
	 * @generated
	 */
	UpperCase createUpperCase();

	/**
	 * Returns a new object of class '<em>Lower Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lower Case</em>'.
	 * @generated
	 */
	LowerCase createLowerCase();

	/**
	 * Returns a new object of class '<em>Assertion Library</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assertion Library</em>'.
	 * @generated
	 */
	AssertionLibrary createAssertionLibrary();

	/**
	 * Returns a new object of class '<em>Assertion Test</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assertion Test</em>'.
	 * @generated
	 */
	AssertionTest createAssertionTest();

	/**
	 * Returns a new object of class '<em>Assertion Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Assertion Definition</em>'.
	 * @generated
	 */
	AssertionDefinition createAssertionDefinition();

	/**
	 * Returns a new object of class '<em>Library Assertion Call</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Library Assertion Call</em>'.
	 * @generated
	 */
	LibraryAssertionCall createLibraryAssertionCall();

	/**
	 * Returns a new object of class '<em>Boolean Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Boolean Parameter</em>'.
	 * @generated
	 */
	BooleanParameter createBooleanParameter();

	/**
	 * Returns a new object of class '<em>Integer Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Integer Parameter</em>'.
	 * @generated
	 */
	IntegerParameter createIntegerParameter();

	/**
	 * Returns a new object of class '<em>String Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Parameter</em>'.
	 * @generated
	 */
	StringParameter createStringParameter();

	/**
	 * Returns a new object of class '<em>User Defined Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Defined Parameter</em>'.
	 * @generated
	 */
	UserDefinedParameter createUserDefinedParameter();

	/**
	 * Returns a new object of class '<em>Path Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path Filter</em>'.
	 * @generated
	 */
	PathFilter createPathFilter();

	/**
	 * Returns a new object of class '<em>Length</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Length</em>'.
	 * @generated
	 */
	Length createLength();

	/**
	 * Returns a new object of class '<em>Source Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Source Class</em>'.
	 * @generated
	 */
	SourceClass createSourceClass();

	/**
	 * Returns a new object of class '<em>Target Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Target Class</em>'.
	 * @generated
	 */
	TargetClass createTargetClass();

	/**
	 * Returns a new object of class '<em>Containment Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Containment Reference</em>'.
	 * @generated
	 */
	ContainmentReference createContainmentReference();

	/**
	 * Returns a new object of class '<em>Path Target</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path Target</em>'.
	 * @generated
	 */
	PathTarget createPathTarget();

	/**
	 * Returns a new object of class '<em>Path Source</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path Source</em>'.
	 * @generated
	 */
	PathSource createPathSource();

	/**
	 * Returns a new object of class '<em>Element Featurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Featurement</em>'.
	 * @generated
	 */
	ElementFeaturement createElementFeaturement();

	/**
	 * Returns a new object of class '<em>Step Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Step Class</em>'.
	 * @generated
	 */
	StepClass createStepClass();

	/**
	 * Returns a new object of class '<em>Existance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Existance</em>'.
	 * @generated
	 */
	Existance createExistance();

	/**
	 * Returns a new object of class '<em>Element Selector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Selector</em>'.
	 * @generated
	 */
	ElementSelector createElementSelector();

	/**
	 * Returns a new object of class '<em>Class Selector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Selector</em>'.
	 * @generated
	 */
	ClassSelector createClassSelector();

	/**
	 * Returns a new object of class '<em>Feature Selector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Selector</em>'.
	 * @generated
	 */
	FeatureSelector createFeatureSelector();

	/**
	 * Returns a new object of class '<em>Attribute Selector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Selector</em>'.
	 * @generated
	 */
	AttributeSelector createAttributeSelector();

	/**
	 * Returns a new object of class '<em>Reference Selector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference Selector</em>'.
	 * @generated
	 */
	ReferenceSelector createReferenceSelector();

	/**
	 * Returns a new object of class '<em>Path Selector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path Selector</em>'.
	 * @generated
	 */
	PathSelector createPathSelector();

	/**
	 * Returns a new object of class '<em>And Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>And Qualifier</em>'.
	 * @generated
	 */
	AndQualifier createAndQualifier();

	/**
	 * Returns a new object of class '<em>Or Qualifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Or Qualifier</em>'.
	 * @generated
	 */
	OrQualifier createOrQualifier();

	/**
	 * Returns a new object of class '<em>Path And</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path And</em>'.
	 * @generated
	 */
	PathAnd createPathAnd();

	/**
	 * Returns a new object of class '<em>Path Or</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Path Or</em>'.
	 * @generated
	 */
	PathOr createPathOr();

	/**
	 * Returns a new object of class '<em>Attribute Containment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attribute Containment</em>'.
	 * @generated
	 */
	AttributeContainment createAttributeContainment();

	/**
	 * Returns a new object of class '<em>Reference Containment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference Containment</em>'.
	 * @generated
	 */
	ReferenceContainment createReferenceContainment();

	/**
	 * Returns a new object of class '<em>Cont Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cont Root</em>'.
	 * @generated
	 */
	ContRoot createContRoot();

	/**
	 * Returns a new object of class '<em>Cont Leaf</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cont Leaf</em>'.
	 * @generated
	 */
	ContLeaf createContLeaf();

	/**
	 * Returns a new object of class '<em>Element Relation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Element Relation</em>'.
	 * @generated
	 */
	ElementRelation createElementRelation();

	/**
	 * Returns a new object of class '<em>Class Reaches</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Reaches</em>'.
	 * @generated
	 */
	ClassReaches createClassReaches();

	/**
	 * Returns a new object of class '<em>Class Reached By</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Reached By</em>'.
	 * @generated
	 */
	ClassReachedBy createClassReachedBy();

	/**
	 * Returns a new object of class '<em>Containment Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Containment Path</em>'.
	 * @generated
	 */
	ContainmentPath createContainmentPath();

	/**
	 * Returns a new object of class '<em>With Inheritance Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>With Inheritance Path</em>'.
	 * @generated
	 */
	WithInheritancePath createWithInheritancePath();

	/**
	 * Returns a new object of class '<em>Cyclic Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cyclic Path</em>'.
	 * @generated
	 */
	CyclicPath createCyclicPath();

	/**
	 * Returns a new object of class '<em>Some</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Some</em>'.
	 * @generated
	 */
	Some createSome();

	/**
	 * Returns a new object of class '<em>Inherited Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Inherited Feature</em>'.
	 * @generated
	 */
	InheritedFeature createInheritedFeature();

	/**
	 * Returns a new object of class '<em>Class Collects</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Collects</em>'.
	 * @generated
	 */
	ClassCollects createClassCollects();

	/**
	 * Returns a new object of class '<em>Annotated</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotated</em>'.
	 * @generated
	 */
	Annotated createAnnotated();

	/**
	 * Returns a new object of class '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Import</em>'.
	 * @generated
	 */
	Import createImport();

	/**
	 * Returns a new object of class '<em>Self Inheritance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Self Inheritance</em>'.
	 * @generated
	 */
	SelfInheritance createSelfInheritance();

	/**
	 * Returns a new object of class '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotation</em>'.
	 * @generated
	 */
	Annotation createAnnotation();

	/**
	 * Returns a new object of class '<em>Described Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Described Element</em>'.
	 * @generated
	 */
	DescribedElement createDescribedElement();

	/**
	 * Returns a new object of class '<em>Synonym</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Synonym</em>'.
	 * @generated
	 */
	Synonym createSynonym();

	/**
	 * Returns a new object of class '<em>Verb</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Verb</em>'.
	 * @generated
	 */
	Verb createVerb();

	/**
	 * Returns a new object of class '<em>Noun</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Noun</em>'.
	 * @generated
	 */
	Noun createNoun();

	/**
	 * Returns a new object of class '<em>Adjective</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Adjective</em>'.
	 * @generated
	 */
	Adjective createAdjective();

	/**
	 * Returns a new object of class '<em>Phrase Named</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Phrase Named</em>'.
	 * @generated
	 */
	PhraseNamed createPhraseNamed();

	/**
	 * Returns a new object of class '<em>Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Literal</em>'.
	 * @generated
	 */
	Literal createLiteral();

	/**
	 * Returns a new object of class '<em>Trivial String</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trivial String</em>'.
	 * @generated
	 */
	TrivialString createTrivialString();

	/**
	 * Returns a new object of class '<em>Opposite To</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Opposite To</em>'.
	 * @generated
	 */
	OppositeTo createOppositeTo();

	/**
	 * Returns a new object of class '<em>Parameter String Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter String Value</em>'.
	 * @generated
	 */
	ParameterStringValue createParameterStringValue();

	/**
	 * Returns a new object of class '<em>Parameter Element Set Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter Element Set Value</em>'.
	 * @generated
	 */
	ParameterElementSetValue createParameterElementSetValue();

	/**
	 * Returns a new object of class '<em>Named After</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Named After</em>'.
	 * @generated
	 */
	NamedAfter createNamedAfter();

	/**
	 * Returns a new object of class '<em>OCL Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OCL Expression</em>'.
	 * @generated
	 */
	OCLExpression createOCLExpression();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MetamodeltestPackage getMetamodeltestPackage();

} //MetamodeltestFactory
