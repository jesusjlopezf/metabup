/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks;

import test.checks.expressions.ObjectOrMetaClassExpression;



/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Missing Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.MissingReference#getRefName <em>Ref Name</em>}</li>
 *   <li>{@link test.checks.MissingReference#getFrom <em>From</em>}</li>
 *   <li>{@link test.checks.MissingReference#getTo <em>To</em>}</li>
 * </ul>
 *
 * @see test.checks.ChecksPackage#getMissingReference()
 * @model
 * @generated
 */
public interface MissingReference extends Check {
	/**
	 * Returns the value of the '<em><b>Ref Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ref Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ref Name</em>' attribute.
	 * @see #setRefName(String)
	 * @see test.checks.ChecksPackage#getMissingReference_RefName()
	 * @model
	 * @generated
	 */
	String getRefName();

	/**
	 * Sets the value of the '{@link test.checks.MissingReference#getRefName <em>Ref Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ref Name</em>' attribute.
	 * @see #getRefName()
	 * @generated
	 */
	void setRefName(String value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' containment reference.
	 * @see #setFrom(ObjectOrMetaClassExpression)
	 * @see test.checks.ChecksPackage#getMissingReference_From()
	 * @model containment="true"
	 * @generated
	 */
	ObjectOrMetaClassExpression getFrom();

	/**
	 * Sets the value of the '{@link test.checks.MissingReference#getFrom <em>From</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' containment reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(ObjectOrMetaClassExpression value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' containment reference.
	 * @see #setTo(ObjectOrMetaClassExpression)
	 * @see test.checks.ChecksPackage#getMissingReference_To()
	 * @model containment="true"
	 * @generated
	 */
	ObjectOrMetaClassExpression getTo();

	/**
	 * Sets the value of the '{@link test.checks.MissingReference#getTo <em>To</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' containment reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(ObjectOrMetaClassExpression value);

} // MissingReference
