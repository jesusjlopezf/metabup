/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest.impl;

import metamodeltest.ClassSelector;
import metamodeltest.MetamodeltestPackage;
import metamodeltest.PathSide;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Path Side</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link metamodeltest.impl.PathSideImpl#getClassSels <em>Class Sels</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class PathSideImpl extends PathQualifierImpl implements PathSide {
	/**
	 * The cached value of the '{@link #getClassSels() <em>Class Sels</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassSels()
	 * @generated
	 * @ordered
	 */
	protected ClassSelector classSels;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PathSideImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetamodeltestPackage.Literals.PATH_SIDE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassSelector getClassSels() {
		return classSels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetClassSels(ClassSelector newClassSels, NotificationChain msgs) {
		ClassSelector oldClassSels = classSels;
		classSels = newClassSels;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PATH_SIDE__CLASS_SELS, oldClassSels, newClassSels);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassSels(ClassSelector newClassSels) {
		if (newClassSels != classSels) {
			NotificationChain msgs = null;
			if (classSels != null)
				msgs = ((InternalEObject)classSels).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PATH_SIDE__CLASS_SELS, null, msgs);
			if (newClassSels != null)
				msgs = ((InternalEObject)newClassSels).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MetamodeltestPackage.PATH_SIDE__CLASS_SELS, null, msgs);
			msgs = basicSetClassSels(newClassSels, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MetamodeltestPackage.PATH_SIDE__CLASS_SELS, newClassSels, newClassSels));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SIDE__CLASS_SELS:
				return basicSetClassSels(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SIDE__CLASS_SELS:
				return getClassSels();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SIDE__CLASS_SELS:
				setClassSels((ClassSelector)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SIDE__CLASS_SELS:
				setClassSels((ClassSelector)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MetamodeltestPackage.PATH_SIDE__CLASS_SELS:
				return classSels != null;
		}
		return super.eIsSet(featureID);
	}

} //PathSideImpl
