/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.compilers.xtext;

import metabup.annotations.IAnnotationFactory;
import metabup.transformations.models.WEcoreModel;
import metabup.transformations.models.WMetamodelModel;

public class XTextCompiler extends MetamodelCompiler {
	public XTextCompiler(WMetamodelModel input, WEcoreModel ecore, IAnnotationFactory af) {
		super(input, ecore, af);
	}
	
	public void execute() {
		super.execute();
		
	}
	
}
