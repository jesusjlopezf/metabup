#*******************************************************************************
# Copyright (c) 2015 IBM Corporation and others.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     IBM Corporation - initial API and implementation
#*******************************************************************************
# $Id: pkgIndex.tcl,v 1.2 2002/03/30 18:49:10 cthuang Exp $
package ifneeded TclScript 1.0 \
[list load [file join $dir TclScript.dll]]\n[list source [file join $dir TclScript.itcl]]
