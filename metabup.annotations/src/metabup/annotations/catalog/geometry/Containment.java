/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.geometry;

import metabup.annotations.catalog.design.Composition;
import metabup.annotations.extensionpoint.DesignAnnotation;
import metabup.metamodel.Annotation;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;

public class Containment extends DesignAnnotation {
	public static final String NAME = "containment";
	
	public Containment (){
		this.setName(NAME);
		this.setDescription("Denoting a certain reference represents the target element is inside the source.");
	}
	
	@Override
	public MetaModel perform() {	
		if(!element.isAnnotated(Composition.NAME)){
			Annotation ann = MetamodelFactory.eINSTANCE.createAnnotation();	
			ann.setName(Composition.NAME);
			element.getAnnotations().add(ann);			
		}
		
		return targetMetaModel;
	}
}
