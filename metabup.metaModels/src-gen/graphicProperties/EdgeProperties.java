/**
 */
package graphicProperties;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edge Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link graphicProperties.EdgeProperties#getColor <em>Color</em>}</li>
 *   <li>{@link graphicProperties.EdgeProperties#getWidth <em>Width</em>}</li>
 *   <li>{@link graphicProperties.EdgeProperties#getLineType <em>Line Type</em>}</li>
 *   <li>{@link graphicProperties.EdgeProperties#getSrcArrow <em>Src Arrow</em>}</li>
 *   <li>{@link graphicProperties.EdgeProperties#getTgtArrow <em>Tgt Arrow</em>}</li>
 * </ul>
 *
 * @see graphicProperties.GraphicPropertiesPackage#getEdgeProperties()
 * @model
 * @generated
 */
public interface EdgeProperties extends GraphicProperties {
	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(String)
	 * @see graphicProperties.GraphicPropertiesPackage#getEdgeProperties_Color()
	 * @model
	 * @generated
	 */
	String getColor();

	/**
	 * Sets the value of the '{@link graphicProperties.EdgeProperties#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(String value);

	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #setWidth(int)
	 * @see graphicProperties.GraphicPropertiesPackage#getEdgeProperties_Width()
	 * @model
	 * @generated
	 */
	int getWidth();

	/**
	 * Sets the value of the '{@link graphicProperties.EdgeProperties#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(int value);

	/**
	 * Returns the value of the '<em><b>Line Type</b></em>' attribute.
	 * The literals are from the enumeration {@link graphicProperties.LineType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Line Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Line Type</em>' attribute.
	 * @see graphicProperties.LineType
	 * @see #setLineType(LineType)
	 * @see graphicProperties.GraphicPropertiesPackage#getEdgeProperties_LineType()
	 * @model
	 * @generated
	 */
	LineType getLineType();

	/**
	 * Sets the value of the '{@link graphicProperties.EdgeProperties#getLineType <em>Line Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Line Type</em>' attribute.
	 * @see graphicProperties.LineType
	 * @see #getLineType()
	 * @generated
	 */
	void setLineType(LineType value);

	/**
	 * Returns the value of the '<em><b>Src Arrow</b></em>' attribute.
	 * The literals are from the enumeration {@link graphicProperties.ArrowType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Src Arrow</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Src Arrow</em>' attribute.
	 * @see graphicProperties.ArrowType
	 * @see #setSrcArrow(ArrowType)
	 * @see graphicProperties.GraphicPropertiesPackage#getEdgeProperties_SrcArrow()
	 * @model
	 * @generated
	 */
	ArrowType getSrcArrow();

	/**
	 * Sets the value of the '{@link graphicProperties.EdgeProperties#getSrcArrow <em>Src Arrow</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Src Arrow</em>' attribute.
	 * @see graphicProperties.ArrowType
	 * @see #getSrcArrow()
	 * @generated
	 */
	void setSrcArrow(ArrowType value);

	/**
	 * Returns the value of the '<em><b>Tgt Arrow</b></em>' attribute.
	 * The literals are from the enumeration {@link graphicProperties.ArrowType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tgt Arrow</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tgt Arrow</em>' attribute.
	 * @see graphicProperties.ArrowType
	 * @see #setTgtArrow(ArrowType)
	 * @see graphicProperties.GraphicPropertiesPackage#getEdgeProperties_TgtArrow()
	 * @model
	 * @generated
	 */
	ArrowType getTgtArrow();

	/**
	 * Sets the value of the '{@link graphicProperties.EdgeProperties#getTgtArrow <em>Tgt Arrow</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tgt Arrow</em>' attribute.
	 * @see graphicProperties.ArrowType
	 * @see #getTgtArrow()
	 * @generated
	 */
	void setTgtArrow(ArrowType value);

	boolean equals(EdgeProperties prop);

} // EdgeProperties
