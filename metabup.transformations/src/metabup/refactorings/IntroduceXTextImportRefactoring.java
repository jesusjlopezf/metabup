/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.refactorings;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import metabup.metamodel.Attribute;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;
import metabup.transformations.models.WMetamodelModel;

import static metabup.transformations.models.ModelUtil.select;

public class IntroduceXTextImportRefactoring extends MetamodelRefactoring {
	
	private MetaClass rootMetaclass;

	public IntroduceXTextImportRefactoring(WMetamodelModel metamodel, MetaClass rootClass) {
		super(metamodel);
		this.rootMetaclass = rootClass;
	}

	@Override
	public boolean execute() {
		MetaClass importMetaclass = metamodel.createMetaclass();
		importMetaclass.setName("Import");
		
		Attribute attr = metamodel.createAttribute();
		attr.setName("importedNamespace");
		attr.setPrimitiveType("String");
		importMetaclass.getFeatures().add(attr);
		
		Reference ref = metamodel.createReference();
		ref.setName("imports");
		ref.setMax(-1);
		ref.setReference(importMetaclass);
		rootMetaclass.getFeatures().add(ref);
		
		metamodel.rootMetamodel().getClasses().add(importMetaclass);
		
		return true;
	}
}
