/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.jesusjlopezf.utils.geometry.GeometricObject;
import com.jesusjlopezf.utils.geometry.ObjectRelativePosition;
import com.jesusjlopezf.utils.resources.FileUtils;

import fragments.*;
import fragments.Object;
import graphicProperties.impl.GraphicPropertiesImpl;
import metabup.annotations.catalog.design.Composition;
import metabup.annotations.catalog.geometry.Adjacency;
import metabup.annotations.catalog.geometry.Containment;
import metabup.annotations.catalog.geometry.Overlapping;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.sketches.Connection;
import metabup.sketches.Container;
import metabup.sketches.KeyValueLabel;
import metabup.sketches.Label;
import metabup.sketches.PlainLabel;
import metabup.sketches.SingleElement;
import metabup.sketches.Sketch;
import metabup.sketches.SketchElement;
import metabup.sketches.SketchElementsContainer;
import metabup.sketches.legend.Legend;
import metabup.sketches.legend.LegendElement;
import metabup.sketches.preferences.SketchPreferenceConstants;

public class Sketch2Fragment {
	
	private HashMap<String, Integer> uniqueIds    = new HashMap<String, Integer>();
	private HashMap<SketchElement, EObject> trace = new HashMap<SketchElement, EObject>(); 
	private HashMap<EObject, SketchElement> traceReverse = new HashMap<EObject, SketchElement>(); 
	
	private Fragment fragment;
	private Sketch sketch;
	private Legend legend;
	private MetaModel metamodel;

	public Sketch2Fragment(Sketch sketch, Legend legend, MetaModel metaModel) {
		this.sketch = sketch;				
		this.legend = legend;
		this.metamodel = metaModel;
	}
	
	public Sketch2Fragment(Sketch sketch) {
		this(sketch, null, null);
	}
	
	public Fragment execute() {		
		fragment = FragmentsFactory.eINSTANCE.createFragment();
		fragment.setName(sketch.getName());
		transformElements(sketch);
		
		// estas tres habr�a que ponerlas como preferencias
		fragment = handleSpaceConflicts(fragment);
		fragment = handleSpaceRelationshipPreferences(fragment);
		fragment = handleContainmentAsComposition(fragment);
		
		// Removes empty references in the fragment. This is never eligible
		fragment = removeEmptyReferences(fragment);

		return fragment;
	}
	
	private Fragment removeEmptyReferences(Fragment fragment) {
		List<fragments.Feature> remove = new ArrayList<fragments.Feature>();
		
		for(fragments.Object o : fragment.getObjects()){
			for(Feature f : o.getFeatures()){
				if(f instanceof Reference){
					Reference r = (Reference)f;
					if(r.getReference() == null || r.getReference().isEmpty()){
						remove.add(f);
					}
				}
			}
		}
		
		for(Feature f : remove){		
			for(fragments.Object o : fragment.getObjects()){
				if(o.getFeatures().contains(f)) o.getFeatures().remove(f);
			}
		}
		
		return fragment;
	}

	private Fragment handleSpaceRelationshipPreferences(Fragment fragment) {
		for(fragments.Object o : fragment.getObjects()){
			List<fragments.Reference> remO = new ArrayList<fragments.Reference>();

			for(Feature f : o.getFeatures()){
				if(f instanceof Reference){
					Reference r = (Reference)f;

					if(r.isAnnotated(Overlapping.NAME) || r.isAnnotated(Adjacency.NAME)){
						List<fragments.Object> remTarO = new ArrayList<fragments.Object>();
						
						for(fragments.Object taro : r.getReference()){
							for(Feature tarf : taro.getFeatures()){
								if(tarf instanceof Reference){
									Reference tarr = (Reference)tarf;
									if((tarr.isAnnotated(Overlapping.NAME) && r.isAnnotated(Overlapping.NAME)) ||
											tarr.isAnnotated(Overlapping.NAME) && r.isAnnotated(Overlapping.NAME)){
										if(tarr.getReference().contains(o)){
											SketchElement oSk = traceReverse.get(o);
											SketchElement taroSk = traceReverse.get(taro);
											
											GeometricObject oGO = new GeometricObject(Math.round(oSk.getX()), 
																		  Math.round(oSk.getY()), 
																		  Math.round(oSk.getWidth()),
																		  Math.round(oSk.getHeight()));
		
											GeometricObject taroGO = new GeometricObject(Math.round(taroSk.getX()), 
																	  Math.round(taroSk.getY()), 
																	  Math.round(taroSk.getWidth()),
																	  Math.round(taroSk.getHeight()));
											
											if(oGO.isGreaterThan(taroGO)){
												// if we want the reference FROM smaller objects
												//remTarO.add(taro);
												
												// if we want the reference FROM bigger objects
												remO.add(tarr);
												
												// if we want the reference from both
												// do nothing
											}
										}
									}
								}
							}
						}
						
						for(fragments.Object taro : remTarO)
							r.getReference().remove(taro);
					}
				}
			}
			
			for(fragments.Reference r : remO){
				r.getReference().remove(o);
			}
		}
		
		
		return fragment;
	}

	private Fragment handleContainmentAsComposition(Fragment fragment) {		
		for(fragments.Object o : fragment.getObjects()){
			for(Feature f : o.getFeatures()){
				if(f.isAnnotated(Containment.NAME)){
					f.annotate(Composition.NAME);
				}
			}
		}
		
		return fragment;
	}
	
	// elimina todas las relaciones de composici�n: �mantener s�lo la que sea con el objeto m�s grande?
	private Fragment handleSpaceConflicts(Fragment fragment) {		
		for(fragments.Object o : fragment.getObjects()){
			List<fragments.Object> contained = new ArrayList<fragments.Object>();
			List<fragments.Object> removeCont = new ArrayList<fragments.Object>();
			List<Reference> contRefs = o.getReferencesByAnnotation(Containment.NAME);
						
			if(contRefs != null){				
				for(Reference r : contRefs){
					for(fragments.Object c : r.getReference()){
						contained.add(c);
					}
				}
				
				for(fragments.Object conto : contained){
					List<Reference> spaceRefs = conto.getReferencesByAnnotation(new String[]{Containment.NAME, Adjacency.NAME, Overlapping.NAME});
					
					if(spaceRefs == null) continue;
					
					for(Reference r : spaceRefs){
						for(fragments.Object oo : r.getReference()){
							if(contained.contains(oo) && !removeCont.contains(oo)) {
								SketchElement contoSk = traceReverse.get(conto);
								SketchElement ooSk = traceReverse.get(oo);
								
								GeometricObject contoGO = new GeometricObject(Math.round(contoSk.getX()), 
																			  Math.round(contoSk.getY()), 
																			  Math.round(contoSk.getWidth()),
																			  Math.round(contoSk.getHeight()));
								
								GeometricObject ooGO = new GeometricObject(Math.round(ooSk.getX()), 
										  Math.round(ooSk.getY()), 
										  Math.round(ooSk.getWidth()),
										  Math.round(ooSk.getHeight()));
								
								if(contoGO.isGreaterThan(ooGO)) removeCont.add(oo);
								else removeCont.add(conto);
							}
						}
					}
				}
				
				List<Reference> removeRefs = new ArrayList<Reference>();
				
				for(Reference r : contRefs){
					for(fragments.Object ro : removeCont){
						if(r.getReference().contains(ro)) r.getReference().remove(ro);
						if(r.getReference() == null || r.getReference().size() == 0) removeRefs.add(r);
					}
				}
				
				if(!removeRefs.isEmpty()) o.getFeatures().removeAll(removeRefs);
			}
		}
		
		return fragment;
	}
	
	protected void transformElements(SketchElementsContainer sketchContainer) {
		
		EList<SketchElement> elements = sketchContainer.getElements();

		for (SketchElement sketchElement : elements) {
			if ( sketchElement instanceof SingleElement || sketchElement instanceof Container ) {
				fragments.Object o = FragmentsFactory.eINSTANCE.createObject();
				o.setName(inferPossibleName(sketchElement));
				o.setType(inferType(sketchElement));
				
				// Add to the fragment and set a trace link
				fragment.getObjects().add(o);
				trace.put(sketchElement, o);	
				traceReverse.put(o, sketchElement);
			}
		}
		
		// Create objects
		for (SketchElement sketchElement : elements) {
			if ( sketchElement instanceof SingleElement || sketchElement instanceof Container ) {
				fragments.Object o = (Object) trace.get(sketchElement);
				
				// get annotations
				for(Label l :  sketchElement.getLabels())
					if (!( l instanceof KeyValueLabel ))
						if(l.getAnnotations() != null && !l.getAnnotations().isEmpty())
							for(String ann : l.getAnnotations())
								o.getAnnotation().add(createAnnotation(ann));
				
				// Create attributes from labels if needed
				createAttributes(sketchElement, o);
				
				
				
				if ( sketchElement instanceof Container ) {
					o.getAnnotation().add(createAnnotation("container"));					
					transformElements((Container) sketchElement);
				}
				
				if ( sketchContainer instanceof Container ) {
					o.getAnnotation().add(createAnnotation("containee"));										
				}								
			}
		}
		
		// check whether overlapping elements exist
		List<String> check = new ArrayList<String>();
		
		for (SketchElement sketchElement : elements) {
			if(sketchElement instanceof Connection) continue;
			for(SketchElement otherElement : elements){
				if(otherElement instanceof Connection) continue;
				if(otherElement != sketchElement){
					//System.out.println("returning " + check.get(sketchElement));					
					if(sketchElement.isOverlapping(otherElement, true)){
						String inferredSrcRefName = "overlapping_" + inferReferenceName(otherElement);
						String inferredTgtRefName = "overlapping_" + inferReferenceName(sketchElement);
						
						fragments.Object src = ((fragments.Object)trace.get(sketchElement));
						fragments.Object tgt = ((fragments.Object)trace.get(otherElement));
						
						if(check.contains(src.getName() + tgt.getName())) continue;
						
						Reference ref = src.getReferenceByName(inferredSrcRefName);
						
						if(ref == null){
							ref = FragmentsFactory.eINSTANCE.createReference();
							ref.setName(inferredSrcRefName);
							src.getFeatures().add(ref);
						}
										
						Reference oppRef = tgt.getReferenceByName(inferredTgtRefName);
						
						if(oppRef == null){
							oppRef = FragmentsFactory.eINSTANCE.createReference();
							oppRef.setName(inferredTgtRefName);
							tgt.getFeatures().add(oppRef);
						}
						
						if(!ref.getReference().contains(tgt)) ref.getReference().add(tgt);
						if(!oppRef.getReference().contains(src)) oppRef.getReference().add(src);
						
						Annotation a, a2;
						
						if(!ref.isAnnotated("overlapping")) a = this.createAnnotation("overlapping");
						else a = ref.getAnnotationByName("overlapping");
						
						if(!oppRef.isAnnotated("overlapping")) a2 = this.createAnnotation("overlapping");
						else a2 = oppRef.getAnnotationByName("overlapping");
						
						/*AnnotationParam ap = FragmentsFactory.eINSTANCE.createAnnotationParam();
						ap.setName("opp" + a.getParams().size());
						ObjectValueAnnotationParam pv = FragmentsFactory.eINSTANCE.createObjectValueAnnotationParam();
						pv.setObject(tgt);
						pv.setFeature(oppRef); 
						ap.setParamValue(pv);
						a.getParams().add(ap);*/
						if(!ref.isAnnotated("overlapping")) ref.getAnnotation().add(a);
												
						/* AnnotationParam ap2 = FragmentsFactory.eINSTANCE.createAnnotationParam();
						ap2.setName("opp" + a2.getParams().size());
						ObjectValueAnnotationParam pv2 = FragmentsFactory.eINSTANCE.createObjectValueAnnotationParam();
						pv2.setObject(src);
						pv2.setFeature(ref); 
						ap2.setParamValue(pv2);
						a2.getParams().add(ap2);*/
						if(!oppRef.isAnnotated("overlapping")) oppRef.getAnnotation().add(a2);
																	
						check.add(src.getName() + tgt.getName());
						check.add(tgt.getName() + src.getName());
						
						//System.out.println("how many overlapping: " + ref.howManyAnnotations("overlapping"));
					}	
				}
			}
		}
		
		// check adjacency
		
		check.clear();
		
		for (SketchElement sketchElement : elements) {
			if(sketchElement instanceof Connection) continue;
			for(SketchElement otherElement : elements){
				if(otherElement instanceof Connection) continue;
				if(otherElement != sketchElement){
					
					if(sketchElement.isAdjacent(otherElement, 0, true)){
						// check adjacency side
						int side = sketchElement.getAdjacencySide(otherElement, 0, true);
						int alignment = sketchElement.getAlignmentSide(otherElement, 0, true);
						String sideStr = "";
						String oppSideStr = "";
							
						switch (side){
							case ObjectRelativePosition.BOTTOM:
								sideStr = "bottom";
								oppSideStr = "top";
							break;
								
							case ObjectRelativePosition.LEFT:
								sideStr = "left";
								oppSideStr = "right";
							break;
								
							case ObjectRelativePosition.RIGHT:
								sideStr = "right";
								oppSideStr = "left";
							break;
								
							case ObjectRelativePosition.TOP:
								sideStr = "top";
								oppSideStr = "bottom";
							break;
						}
						
						String inferredSrcRefName = sideStr + "adjacentTo_" + inferReferenceName(otherElement);
						String inferredTgtRefName = oppSideStr + "adjacentTo_" + inferReferenceName(sketchElement);
						
						fragments.Object src = ((fragments.Object)trace.get(sketchElement));
						fragments.Object tgt = ((fragments.Object)trace.get(otherElement));
						
						if(check.contains(src.getName() + tgt.getName())) continue;
						
						Reference ref = src.getReferenceByName(inferredSrcRefName);
						
						if(ref == null){
							ref = FragmentsFactory.eINSTANCE.createReference();
							ref.setName(inferredSrcRefName);
							src.getFeatures().add(ref);
						}
										
						Reference oppRef = tgt.getReferenceByName(inferredTgtRefName);
						
						if(oppRef == null){
							oppRef = FragmentsFactory.eINSTANCE.createReference();
							oppRef.setName(inferredTgtRefName);
							tgt.getFeatures().add(oppRef);
						}
						
						if(!ref.getReference().contains(tgt)) ref.getReference().add(tgt);
						if(!oppRef.getReference().contains(src)) oppRef.getReference().add(src);
						
						Annotation a, a2;
						
						if(!ref.isAnnotated("adjacency")) a = this.createAnnotation("adjacency");
						else a = ref.getAnnotationByName("adjacency");
						
						if(!oppRef.isAnnotated("adjaceny")) a2 = this.createAnnotation("adjacency");
						else a2 = oppRef.getAnnotationByName("adjacency");
						
						if(side != ObjectRelativePosition.NONE && side != ObjectRelativePosition.UNIDENTIFIED){
							AnnotationParam ap = FragmentsFactory.eINSTANCE.createAnnotationParam();
							ap.setName("side");
							PrimitiveValueAnnotationParam pv = FragmentsFactory.eINSTANCE.createPrimitiveValueAnnotationParam();
							StringValue sv = FragmentsFactory.eINSTANCE.createStringValue();
							sv.setValue(sideStr);
							pv.setValue(sv);
							ap.setParamValue(pv);
							a.getParams().add(ap);
							
							
							
							if(alignment != ObjectRelativePosition.NONE && alignment != ObjectRelativePosition.UNIDENTIFIED){
								AnnotationParam ap2 = FragmentsFactory.eINSTANCE.createAnnotationParam();
								ap2.setName("alignment");
								PrimitiveValueAnnotationParam pv2 = FragmentsFactory.eINSTANCE.createPrimitiveValueAnnotationParam();
								StringValue sv2 = FragmentsFactory.eINSTANCE.createStringValue();
								sv2.setValue(ObjectRelativePosition.getPositionString(alignment));
								pv2.setValue(sv2);
								ap2.setParamValue(pv2);
								a.getParams().add(ap2);
							}
						}
						
						/*AnnotationParam ap = FragmentsFactory.eINSTANCE.createAnnotationParam();
						ap.setName("opp" + a.getParams().size());
						ObjectValueAnnotationParam pv = FragmentsFactory.eINSTANCE.createObjectValueAnnotationParam();
						pv.setObject(tgt);
						pv.setFeature(oppRef); 
						ap.setParamValue(pv);
						a.getParams().add(ap);*/
						if(!ref.isAnnotated("adjacency")) ref.getAnnotation().add(a);
						
						if(side != ObjectRelativePosition.NONE && side != ObjectRelativePosition.UNIDENTIFIED){
							AnnotationParam ap = FragmentsFactory.eINSTANCE.createAnnotationParam();
							ap.setName("side");
							PrimitiveValueAnnotationParam pv = FragmentsFactory.eINSTANCE.createPrimitiveValueAnnotationParam();
							StringValue sv = FragmentsFactory.eINSTANCE.createStringValue();
							sv.setValue(oppSideStr);
							pv.setValue(sv);
							ap.setParamValue(pv);
							a2.getParams().add(ap);
							
							if(alignment != ObjectRelativePosition.NONE && alignment != ObjectRelativePosition.UNIDENTIFIED){
								AnnotationParam ap2 = FragmentsFactory.eINSTANCE.createAnnotationParam();
								ap2.setName("alignment");
								PrimitiveValueAnnotationParam pv2 = FragmentsFactory.eINSTANCE.createPrimitiveValueAnnotationParam();
								StringValue sv2 = FragmentsFactory.eINSTANCE.createStringValue();
								sv2.setValue(ObjectRelativePosition.getPositionString(alignment));
								pv2.setValue(sv2);
								ap2.setParamValue(pv2);
								a2.getParams().add(ap2);
							}
						}
						
						/* AnnotationParam ap2 = FragmentsFactory.eINSTANCE.createAnnotationParam();
						ap2.setName("opp" + a2.getParams().size());
						ObjectValueAnnotationParam pv2 = FragmentsFactory.eINSTANCE.createObjectValueAnnotationParam();
						pv2.setObject(src);
						pv2.setFeature(ref); 
						ap2.setParamValue(pv2);
						a2.getParams().add(ap2);*/
						if(!oppRef.isAnnotated("adjacency")) oppRef.getAnnotation().add(a2);
						
						
						
						check.add(src.getName() + tgt.getName());
						check.add(tgt.getName() + src.getName());
					}	
				}
			}
		}
		
		// check containment
		
		check.clear();
				
		for (SketchElement sketchElement : elements) {			
			if(sketchElement instanceof Connection) continue;
				for(SketchElement otherElement : elements){
					if(otherElement instanceof Connection) continue;
					if(otherElement != sketchElement){
						if(sketchElement.contains(otherElement)){							
							String inferredSrcRefName = "contains_" + inferReferenceName(otherElement);
								
							fragments.Object src = ((fragments.Object)trace.get(sketchElement));
							fragments.Object tgt = ((fragments.Object)trace.get(otherElement));
								
							if(check.contains(src.getName() + tgt.getName())) continue;
								
							Reference ref = src.getReferenceByName(inferredSrcRefName);
								
							if(ref == null){
								ref = FragmentsFactory.eINSTANCE.createReference();
								ref.setName(inferredSrcRefName);
								src.getFeatures().add(ref);
							}
												
								/*Reference oppRef = tgt.getReferenceByName(inferredTgtRefName);
								
								if(oppRef == null){
									oppRef = FragmentsFactory.eINSTANCE.createReference();
									oppRef.setName(inferredTgtRefName);
									tgt.getFeatures().add(oppRef);
								}*/
								
							if(!ref.getReference().contains(tgt)) ref.getReference().add(tgt);
							//if(!oppRef.getReference().contains(src)) oppRef.getReference().add(src);
								
							Annotation a, a2;
								
							if(!ref.isAnnotated(Containment.NAME)) a = this.createAnnotation(Containment.NAME);
							else a = ref.getAnnotationByName(Containment.NAME);
								
							/*if(!oppRef.isAnnotated("adjaceny")) a2 = this.createAnnotation("adjacency");
							else a2 = oppRef.getAnnotationByName("adjacency");*/
								
								/*AnnotationParam ap = FragmentsFactory.eINSTANCE.createAnnotationParam();
								ap.setName("opp" + a.getParams().size());
								ObjectValueAnnotationParam pv = FragmentsFactory.eINSTANCE.createObjectValueAnnotationParam();
								pv.setObject(tgt);
								pv.setFeature(oppRef); 
								ap.setParamValue(pv);
								a.getParams().add(ap);*/
								if(!ref.isAnnotated(Containment.NAME)) ref.getAnnotation().add(a);
								
								
								/* AnnotationParam ap2 = FragmentsFactory.eINSTANCE.createAnnotationParam();
								ap2.setName("opp" + a2.getParams().size());
								ObjectValueAnnotationParam pv2 = FragmentsFactory.eINSTANCE.createObjectValueAnnotationParam();
								pv2.setObject(src);
								pv2.setFeature(ref); 
								ap2.setParamValue(pv2);
								a2.getParams().add(ap2);*/
								//if(!oppRef.isAnnotated("adjacency")) oppRef.getAnnotation().add(a2);
								
								check.add(src.getName() + tgt.getName());
								//check.add(tgt.getName() + src.getName());
							}	
						}
					}
				}
		
		// Create connections. Two types: 
		//    - reference connections: mapped to a plain reference
		//    - entity connections: mapped to an entity (i.e., an object acting as an association)
		for (SketchElement sketchElementConnection : elements) {			
			if ( isReferenceConnection(sketchElementConnection) ) {
				fragments.Object src = (fragments.Object) trace.get(((Connection) sketchElementConnection).getSrc());
				fragments.Object tgt = (fragments.Object) trace.get(((Connection) sketchElementConnection).getTgt());
				
				if ( src == null ) {
					//System.out.println("No src: " + ((Connection) sketchElementConnection).getSrc());
					continue;
				}
				
				if ( tgt == null ) {
					//System.out.println("No tgt: " + ((Connection) sketchElementConnection).getTgt());
					continue;
				}				
				
				Reference ref = FragmentsFactory.eINSTANCE.createReference();
				String name = findFirstNameLabel(sketchElementConnection.getLabels());
				if ( name != null ) {
					ref.setName(name);
				} else {
					if(Activator.getDefault().getPreferenceStore().getString(SketchPreferenceConstants.AssociationNamingCriteriaPreference).equals(SketchPreferenceConstants.OBJECT))
						ref.setName(inferReferenceName(((Connection) sketchElementConnection).getTgt()));
					else{
						Connection c = (Connection)sketchElementConnection;
						if(c.getGraphicProperties() == null) ref.setName("unstyled");
						else{
							ref.setName(c.getGraphicProperties().toString());
						}
					}
				}
				
				// here we try to use the stored on edges for deducing a reference name
				
				if(this.metamodel != null){
					
					Connection c = (Connection)sketchElementConnection;
					
					if(c.getGraphicProperties() != null){						
						MetaClass srcMC = this.metamodel.getClassByName(src.getType());
						ref.getReference().add(tgt);
						List<metabup.metamodel.Reference> candidateRefs;
						
						if(Activator.getDefault().getPreferenceStore().getString(SketchPreferenceConstants.AssociationNamingCriteriaPreference).equals(SketchPreferenceConstants.OBJECT)){
							candidateRefs = ref.getAnalogousReferences(srcMC);
							if(candidateRefs != null && candidateRefs.size() > 0){
								ref.setName(candidateRefs.get(0).getName());
							}
						}else{
							candidateRefs = this.metamodel.getReferences();
							
							if(candidateRefs != null && !candidateRefs.isEmpty()){
								for(metabup.metamodel.Reference r : candidateRefs){
									if(r.getGraphicProperties() != null && r.getGraphicProperties().equals(c.getGraphicProperties())){
										List<MetaClass> tars = r.getReference().getAllSubs();
										tars.add(r.getReference());
										
										if(ref.getType() == null || tars == null) break;
										
										for(MetaClass tar : tars) if(ref.getType().toLowerCase().equals(tar.getName().toLowerCase())){
											ref.setName(r.getName());
											break;
										}									
									}
								}
							}
						}
						
						//if(candidateRefs != null)System.out.println("there are candidateRefs: " + candidateRefs.size());
						
						
						
						ref.getReference().remove(tgt);
						
						if(Activator.getDefault().getPreferenceStore().getString(SketchPreferenceConstants.AssociationNamingCriteriaPreference).equals(SketchPreferenceConstants.STYLE)){
							ref.setGraphicProperties(EcoreUtil.copy(c.getGraphicProperties()));
						}
					}
				}
				
				// get annotations
				for(Label l : sketchElementConnection.getLabels())
					if(l.getAnnotations() != null && !l.getAnnotations().isEmpty()){
						for(String ann : l.getAnnotations()){
							ref.getAnnotation().add(createAnnotation(ann));
						}
					}
				
				// check whether the reference outta be multiple
				Reference equalRef = null;
				
				for(fragments.Feature f : ((fragments.Object) src).getFeatures()){
					if(f instanceof Reference){
						Reference r = (Reference)f;						
						for(fragments.Object o : r.getReference())
							if(r.getName().equals(ref.getName())) equalRef = r;
					}
				}
				
				if(equalRef == null){
					((fragments.Object) src).getFeatures().add(ref);
					ref.getReference().add(tgt);
					
					for(Label l : sketchElementConnection.getLabels())
						if(l.getAnnotations() != null && !l.getAnnotations().isEmpty()){
							for(String ann : l.getAnnotations()){
								ref.getAnnotation().add(createAnnotation(ann));
							}
						}
					
				}else{
					equalRef.getReference().add(tgt);
					
					for(Label l : sketchElementConnection.getLabels())
						if(l.getAnnotations() != null && !l.getAnnotations().isEmpty()){
							for(String ann : l.getAnnotations()){
								equalRef.getAnnotation().add(createAnnotation(ann));
							}
						}
				}								
			}			
			else if ( isEntityConnection(sketchElementConnection) ) {
				fragments.Object o = FragmentsFactory.eINSTANCE.createObject();

				// Create attributes from labels if needed
				createAttributes(sketchElementConnection, o);
				
				// Create source and target references
				fragments.Object src = (fragments.Object) trace.get(((Connection) sketchElementConnection).getSrc());
				fragments.Object tgt = (fragments.Object) trace.get(((Connection) sketchElementConnection).getTgt());

				Reference refSrc = FragmentsFactory.eINSTANCE.createReference();
				refSrc.setName(inferReferenceName(((Connection) sketchElementConnection).getSrc()));												
				refSrc.getReference().add(src);
				o.getFeatures().add(refSrc);								
				
				Reference refTgt = FragmentsFactory.eINSTANCE.createReference();
				refTgt.setName(inferReferenceName(((Connection) sketchElementConnection).getTgt()));
				refTgt.getReference().add(tgt);
				o.getFeatures().add(refTgt);
				
				o.setName(inferPossibleName(sketchElementConnection));
				o.setType(refSrc.getName() + "_" + refTgt.getName());								
				// TODO: To get a proper type, allow edges in the metabup.sketches.legend, 
				//        and look into the style properties to differentiate
				
				// Create an @connection annotation
				o.getAnnotation().add(createAnnotation("connector"));
				
				fragment.getObjects().add(o);
			}
		}
		
		// Generate references from container objects to containees
		for (SketchElement sketchElement : elements) {
			if ( sketchElement instanceof Container ) {
				Container container = (Container) sketchElement;
				if ( container.getElements().isEmpty() ) 
					continue;
				
				fragments.Object objContainer = (fragments.Object) trace.get(sketchElement);
				
				Reference ref = FragmentsFactory.eINSTANCE.createReference();
				objContainer.getFeatures().add(ref);					

				for (SketchElement inner : container.getElements()) {
					ref.getReference().add( (fragments.Object) trace.get(inner) );
				}
				
				// TODO: Infer a better name
				String name = container.getElements().size() == 1 ? "element" : "elements";
				ref.setName(name); 
			}
		}
	}

	private String findFirstNameLabel(EList<Label> labels) {
		for (Label label : labels) {
			if ( label instanceof PlainLabel ) {
				String value = ((PlainLabel) label).getValue();
				value = value.trim();
				if ( ! value.isEmpty() )
					return value;
			}
		}
		return null;
	}

	private Annotation createAnnotation(String name) {
		Annotation annotation = FragmentsFactory.eINSTANCE.createAnnotation();
		annotation.setName(name);
		return annotation;
	}
	
	private List<KeyValueLabel> getPairLabels(List<Label> labels) {
		ArrayList<KeyValueLabel> result = new ArrayList<KeyValueLabel>();
		for (Label l: labels) {
			if ( l instanceof KeyValueLabel ) {
				result.add((KeyValueLabel) l);
			}
		}
		return result;
	}
	
	private boolean isEntityConnection(SketchElement e) {
		return e instanceof Connection && getPairLabels(e.getLabels()).size() > 0;
	}

	private boolean isReferenceConnection(SketchElement e) {
		return e instanceof Connection && getPairLabels(e.getLabels()).size() == 0;
	}

	private void createAttributes(SketchElement sketchElement, fragments.Object o) {
		EList<Label> labels = sketchElement.getLabels();
		for (Label label : labels) {
			if ( label instanceof KeyValueLabel ) {
				Attribute attr = FragmentsFactory.eINSTANCE.createAttribute();
				attr.setName(((KeyValueLabel) label).getKey().trim());
				attr.getValues().add( createValueFromString(((KeyValueLabel) label).getValue()) );
				
				if(label.getAnnotations() != null && !label.getAnnotations().isEmpty())
					for(String ann : label.getAnnotations())				
						attr.getAnnotation().add(createAnnotation(ann));
				
				o.getFeatures().add(attr);
			} else {
				// TODO: Report warnings through the shell with comments??
				//System.out.println("Plain label '" + ((PlainLabel) label).getValue() + "' not interpreted");
			}
		}
	}

	private PrimitiveValue createValueFromString(String value) {
		try { 
			int v = Integer.parseInt(value);
			IntegerValue iv = FragmentsFactory.eINSTANCE.createIntegerValue();
			iv.setValue(v);
			return iv;
		} catch ( Exception e ) {	} // TODO: Refine the exception... 
		
		if ( value.equals("true") || value.equals("false") ) {
			try { 
				boolean v = Boolean.parseBoolean(value);
				BooleanValue bv = FragmentsFactory.eINSTANCE.createBooleanValue();
				bv.setValue(v);
				return bv;
			} catch ( Exception e ) {	}
		}
				
		StringValue sv = FragmentsFactory.eINSTANCE.createStringValue();
		sv.setValue(value);		
		return sv;
	}

	public Fragment execute(FragmentModel model, Resource resource) {
		execute();
		resource.getContents().add(fragment);		
		model.getFragments().add(fragment);
		return fragment;
	}


	private String inferReferenceName(SketchElement tgtElement) {
		String type = inferType(tgtElement);
		return Character.toLowerCase(type.charAt(0)) + type.substring(1, type.length());
	}
	
	private String inferType(SketchElement sketchElement) {
		if ( legend != null ) {
			EList<LegendElement> elements = legend.getElements();
			for (LegendElement legendElement : elements) {
				if (legendElement.getFile() != null && !legendElement.getFile().isEmpty() && (new File(legendElement.getFile())).exists()){
					if(sketchElement.getSourceCode() != null && !sketchElement.getSourceCode().isEmpty()){
						String legendFileStr;
						try {
							legendFileStr = FileUtils.readFile(legendElement.getFile());
							
							/*double ratio = StringUtils.compareStrings(legendFileStr, sketchElement.getSourceCode());
							//System.out.println("ratio: " + ratio);
							if(ratio > 90) return legendElement.getOriginalSymbolName();*/
							
							if(legendFileStr.trim().equals(sketchElement.getSourceCode().trim())) return legendElement.getOriginalSymbolName();
						} catch (IOException e) {
							// TODO Auto-generated catch block
						}
					}
				}else{
					if ( legendElement.getOriginalSymbolName().equals(sketchElement.getToolSymbolName()) ) {
						return legendElement.getNewSymbolName();
					}
				}
			}
		}
		return sketchElement.getSkechTypeInfo();
	}
	
	private String inferPossibleName(SketchElement sketchElement) {
		String name = findFirstNameLabel(sketchElement.getLabels());
		if ( name != null ) {
			return name;
		}

		if ( sketchElement.getText() != null ) {
			return sketchElement.getText().replaceAll(" ", "_").replaceAll("\n", "_");
		} else {
			String type = inferType(sketchElement);
			if ( ! uniqueIds.containsKey(type) ) {
				uniqueIds.put(type, 0);
			}
			
			Integer id = uniqueIds.get(type) + 1; 			
			uniqueIds.put(type, id);
			return type + "_" + id.toString();
		}
	}
}
