<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   
      <title>Animations for Graph Elements</title>
      <link rel="stylesheet" href="ystyle.css" type="text/css">
      <meta name="generator" content="DocBook XSL Stylesheets V1.65.1">
      <link rel="home" href="index.html" title="yFiles for Java Developer's Guide">
      <link rel="up" href="view.html" title="Chapter&nbsp;6.&nbsp;Displaying and Editing Graphs">
      <link rel="previous" href="printing.html" title="Printing a Graph's Visual Representation">
      <link rel="next" href="advanced_stuff.html" title="Advanced Application Logic">
      <link rel="stylesheet" href="jsdt/toc.css" type="text/css"><script type="text/javascript" src="jsdt/jquery.min.js"></script><script type="text/javascript" src="jsdt/toc.js"></script><link type="text/css" rel="stylesheet" href="jssh/SyntaxHighlighter.css"><script type="text/javascript" src="jssh/shCore.js"></script><script type="text/javascript" src="jssh/shBrushJava.js"></script><script type="text/javascript" src="jssh/shBrushXml.js"></script><script type="text/javascript">
  function sh() {

    dp.SyntaxHighlighter.HighlightAll('programlisting', false, false, false, null, false);
  }
  </script></head>
   <body onload="sh()" bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF">
      <div class="navheader">
         <table width="100%" summary="Navigation header">
            <tr>
               <th colspan="3" align="center">Animations for Graph Elements</th>
            </tr>
            <tr>
               <td width="20%" align="left"><a accesskey="p" href="printing.html">Prev</a>&nbsp;
               </td>
               <th width="60%" align="center">Chapter&nbsp;6.&nbsp;Displaying and Editing Graphs</th>
               <td width="20%" align="right">&nbsp;<a accesskey="n" href="advanced_stuff.html">Next</a></td>
            </tr>
         </table>
         <div class="navline"></div>
         <div style="display:none"><img src="figures/navbg.jpg" alt=""><img src="figures/navline.jpg" alt=""></div>
      </div>
      <div class="section" lang="en">
         <div class="titlepage">
            <div>
               <div>
                  <h2 class="title" style="clear: both"><a name="animation"></a>Animations for Graph Elements
                  </h2>
               </div>
            </div>
            <div></div>
         </div><a class="indexterm" name="d0e31221"></a><p>
            The yFiles graph visualization library enables high-quality animations and 
            visual effects for graph elements, more precisely their visual representations. 
            Node realizers, edge realizers, but also drawables can be subject for 
            arbitrary effects. 
            
            
            
            Predefined generic animations and effects are, for example: 
            
            
         </p>
         <div class="itemizedlist">
            <ul type="disc">
               <li>
                  moving, resizing, and scaling of nodes 
                  
               </li>
               <li>
                  color transformations with nodes and making nodes blink 
                  
               </li>
               <li>
                  fading in and out, blurring, and explosions that can be used to "decorate" 
                  creation and/or removal of both nodes and edges 
                  
                  
               </li>
            </ul>
         </div>
         <p>
            
            
            These effects and animations build upon the general animation framework defined 
            in package y.anim, which enables processing of animations both in parallel and 
            in sequence, allows repetitions of animations, and also pausing between 
            animations. 
            
         </p>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h3 class="title"><a name="animation_structural"></a>Animation Framework
                     </h3>
                  </div>
               </div>
               <div></div>
            </div>
            <p>
               Package <a href="../api/y/anim/package-summary.html" title="Link to API documentation" target="_top">y.anim<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> 
               contains the classes that constitute a general animation framework. 
               
               Class <a href="../api/y/anim/AnimationPlayer.html" title="Link to API documentation" target="_top">AnimationPlayer<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> is the central 
               instance that governs an animation process. 
               
               It is given a so-called animation object, an implementation of interface 
               <a href="../api/y/anim/AnimationObject.html" title="Link to API documentation" target="_top">AnimationObject<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>, which encapsulates an 
               actual animation respectively a compound of animations. 
               
               
            </p>
            <p>
               Effectively, an animation object provides a sequence of so-called "frames" that 
               represent the animation's state as it evolves over time. 
               
               
               
               AnimationPlayer progresses the overall animation process by continuously 
               triggering the given animation object to calculate a single new frame that 
               corresponds to the supplied (strictly monotonic) increasing time value. 
               
               
               
               Whenever a new frame has been calculated, the animation player notifies all 
               registered animation listeners, i.e., implementations of interface 
               <a href="../api/y/anim/AnimationListener.html" title="Link to API documentation" target="_top">AnimationListener<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>, to update their 
               associated view so that the new frame is displayed. 
               
               
               
               
               
               
               
            </p>
            <p>
               
               AnimationPlayer also triggers initialization of an AnimationObject before the 
               animation process and any necessary cleanup thereafter by calling appropriate 
               methods. 
               <a href="animation.html#fig_animation_central" title="Figure&nbsp;6.67.&nbsp;Animation framework central classes">Figure&nbsp;6.67, &#8220;Animation framework central classes&#8221;</a> shows the role of class 
               AnimationPlayer. 
               
            </p>
            <div class="figure"><a name="fig_animation_central"></a><p class="title"><b>Figure&nbsp;6.67.&nbsp;Animation framework central classes</b></p>
               <div class="informaltable">
                  <center>
                     <table border="0">
                        <colgroup>
                           <col width="100%">
                        </colgroup>
                        <tbody>
                           <tr>
                              <td>
                                 <div class="mediaobject" align="center"><img src="figures/animation_central.jpg" align="middle" alt="Animation framework central classes."></div>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
            <p>
               Setup for processing a given animation object and having the progress of the 
               (compound) animation displayed in a view is demonstrated in 
               <a href="animation.html#ex_animation" title="Example&nbsp;6.52.&nbsp;Playing a (compound) animation">Example&nbsp;6.52, &#8220;Playing a (compound) animation&#8221;</a>. 
               
               
               
               
            </p>
            <div class="example"><a name="ex_animation"></a><p class="title"><b>Example&nbsp;6.52.&nbsp;Playing a (compound) animation</b></p><pre class="programlisting java">
void startAnimation(AnimationListener al, AnimationObject ao)
{
  // Create a new AnimationPlayer that processes the given (compound) animation. 
  AnimationPlayer player = new AnimationPlayer();
  // Register the given animation listener that is to display the (compound) 
  // animation. Usually, Graph2DView or Graph2DViewRepaintManager are provided 
  // to this end. 
  player.addAnimationListener(al);
  // Play the given (compound) animation. 
  player.animate(ao);
}
</pre></div>
            <p>
               
               An animation listener is responsible for updating its associated view that 
               hosts an animation. 
               It triggers a repainting of the view each time the animation progresses, i.e., 
               each time a new frame has been calculated. 
               
               
               
               
               Predefined implementations for interface AnimationListener are classes 
               Graph2DView and 
               <a href="../api/y/view/Graph2DViewRepaintManager.html" title="Link to API documentation" target="_top">Graph2DViewRepaintManager<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>, both from 
               package y.view. 
               
               
            </p>
            <p>
               When using Graph2DView directly as an animation listener, the entire viewport 
               is repainted to display the frames of an animation. 
               Graph2DViewRepaintManager, in contrast, allows fine-grained control over the 
               repaint area. 
               It provides registration methods for realizers and drawables to advertise their 
               participation in an upcoming animation. 
               Subsequently, Graph2DViewRepaintManager invokes repaints on only the union of 
               bounds of these realizers and drawables, which results in performance benefits 
               especially for animations that are confined to a small region.
               
               
               
               
            </p>
            <p>
               
               Animation objects are the actual provider of an animation. 
               
               
               Their task is to generate a sequence of frames that represent the animation's 
               state as it evolves over time.
               An animation's time is defined to run in the range 0.0 to 1.0 from animation 
               start to animation end. 
               
            </p>
            <p>
               To create animation objects, the factory classes 
               <a href="../api/y/anim/AnimationFactory.html" title="Link to API documentation" target="_top">AnimationFactory<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> and 
               <a href="../api/y/view/ViewAnimationFactory.html" title="Link to API documentation" target="_top">ViewAnimationFactory<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> can be used. 
               
               The former class returns animation objects that primarily address structural 
               concerns, more precisely, it returns animation objects that can be used to 
               define animations that: 
               
            </p>
            <div class="itemizedlist">
               <ul type="disc">
                  <li>
                     can be processed concurrently 
                     
                  </li>
                  <li>
                     can be processed sequentially 
                     
                  </li>
                  <li>
                     are processed repeatedly 
                     
                  </li>
                  <li>
                     do nothing, i.e., that effectively introduce a pause 
                     
                  </li>
               </ul>
            </div>
            <p>
               
               
            </p>
            <p><a name="p_compound_animations"></a>
               To achieve concurrent and sequential processing of a set of animations, 
               interface <a href="../api/y/anim/CompositeAnimationObject.html" title="Link to API documentation" target="_top">CompositeAnimationObject<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a> 
               provides the necessary methods to add or remove single animation objects to 
               form compound animations. 
               
               
               The following methods from class AnimationFactory can be used to define the structure
               of an animation process that is composed of multiple animation objects:
               
            </p>
            <div class="techn_templ">
               <center>
                  <table width="100%" border="1">
                     <colgroup>
                        <col width="16%" align="left">
                        <col width="84%" align="left">
                     </colgroup>
                     <tbody valign="top">
                        <tr class="techn_api">
                           <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/anim/CompositeAnimationObject.html#createConcurrency()" title="Link to API documentation" target="_top">static CompositeAnimationObject createConcurrency()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                        </tr>
                        <tr class="spacing">
                           <td align="left">Description</td>
                           <td align="left">Processing animations concurrently.</td>
                        </tr>
                        <tr class="techn_api">
                           <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/anim/CompositeAnimationObject.html#createSequence()" title="Link to API documentation" target="_top">static CompositeAnimationObject createSequence()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                        </tr>
                        <tr class="spacing">
                           <td align="left">Description</td>
                           <td align="left">Processing animations sequentially.</td>
                        </tr>
                        <tr class="techn_api">
                           <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/anim/CompositeAnimationObject.html#createRepetition(y.anim.AnimationObject, int, boolean)" title="Link to API documentation" target="_top">static AnimationObject createRepetition(AnimationObject ao,
                                        int repetitions,
                                        boolean initDisposeRepeatedly)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                        </tr>
                        <tr class="spacing">
                           <td align="left">Description</td>
                           <td align="left">Repeating an animation.</td>
                        </tr>
                        <tr class="techn_api">
                           <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/anim/CompositeAnimationObject.html#createPause(long)" title="Link to API documentation" target="_top">static AnimationObject createPause(long preferredDuration)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                        </tr>
                        <tr class="spacing">
                           <td align="left">Description</td>
                           <td align="left">Pausing an animation.</td>
                        </tr>
                     </tbody>
                  </table>
               </center>
            </div>
            <p>
               Class AnimationFactory also provides a set of animation objects that can be 
               used as "decorators" for other animations, namely these are ease in and ease 
               out effects. 
               An ease in effect applies a configurable acceleration at the beginning of a 
               given animation, likewise, an ease out effect applies a configurable 
               deceleration at the end. 
               
               <a href="animation.html#ex_ease_decoration" title="Example&nbsp;6.53.&nbsp;Layout morphing using ease in and ease out effects">Example&nbsp;6.53, &#8220;Layout morphing using ease in and ease out effects&#8221;</a> shows how both ease in and ease out 
               effects can be used to smooth the morphing between two graph layouts. 
               
            </p>
            <div class="example"><a name="ex_ease_decoration"></a><p class="title"><b>Example&nbsp;6.53.&nbsp;Layout morphing using ease in and ease out effects</b></p><pre class="programlisting java">
void morphLayoutUsingEaseInEaseOutDecoration(Graph2DView view, GraphLayout gl)
{
  // Create a regular LayoutMorpher and subsequently "decorate" it using ease 
  // in and ease out effects. 
  LayoutMorpher plainLM = new LayoutMorpher(view, gl);
  AnimationObject easedLM = AnimationFactory.createEasedAnimation(plainLM);
  
  // Play the animation, i.e., perform smooth layout morphing. 
  AnimationPlayer player = new AnimationPlayer();
  player.setFps(240);
  player.addAnimationListener(view);
  player.animate(easedLM);
}
</pre></div>
            <p>
               Class ViewAnimationFactory, in contrast, can be used to return a variety of 
               high-quality animations and visual effects for graph elements, more precisely 
               their visual representations. 
               
               
               
               <a href="animation.html#fig_animation_objects" title="Figure&nbsp;6.68.&nbsp;Classes that create animation objects">Figure&nbsp;6.68, &#8220;Classes that create animation objects&#8221;</a> shows the classes that are involved in 
               creating animation objects. 
               
            </p>
            <div class="figure"><a name="fig_animation_objects"></a><p class="title"><b>Figure&nbsp;6.68.&nbsp;Classes that create animation objects</b></p>
               <div class="informaltable">
                  <center>
                     <table border="0">
                        <colgroup>
                           <col width="100%">
                        </colgroup>
                        <tbody>
                           <tr>
                              <td>
                                 <div class="mediaobject" align="center"><img src="figures/animation_objects.jpg" align="middle" alt="Classes that create animation objects."></div>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </center>
               </div>
            </div>
            <p>
               By default, ViewAnimationFactory is configured to return animations that trade 
               animation quality for speed. 
               Using the following methods and constants, the quality for animations and effects
               can conveniently be controlled:
               
            </p>
            <div class="techn_templ">
               <center>
                  <table width="100%" border="1">
                     <colgroup>
                        <col width="16%" align="left">
                        <col width="84%" align="left">
                     </colgroup>
                     <tbody valign="top">
                        <tr class="techn_api">
                           <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/ViewAnimationFactory.html#DEFAULT" title="Link to API documentation" target="_top">static final AnimationQuality DEFAULT<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/ViewAnimationFactory.html#HIGH_QUALITY" title="Link to API documentation" target="_top">static final AnimationQuality HIGH_QUALITY<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/ViewAnimationFactory.html#HIGH_PERFORMANCE" title="Link to API documentation" target="_top">static final AnimationQuality HIGH_PERFORMANCE<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                        </tr>
                        <tr class="spacing">
                           <td align="left">Description</td>
                           <td align="left">Animation quality constants.</td>
                        </tr>
                        <tr class="techn_api">
                           <td colspan="2" align="left"><pre class="programlisting ignore"><a href="../api/y/view/ViewAnimationFactory.html#getQuality()" title="Link to API documentation" target="_top">AnimationQuality getQuality()<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
<a href="../api/y/view/ViewAnimationFactory.html#setQuality(y.view.ViewAnimationFactory.AnimationQuality)" title="Link to API documentation" target="_top">void setQuality(AnimationQuality quality)<sup xmlns="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format"><img src="figures/icon_api.gif" align="bottom" border="0"></img></sup></a>
</pre></td>
                        </tr>
                        <tr class="spacing">
                           <td align="left">Description</td>
                           <td align="left">Getter/setter methods to control the quality of animations and effects.</td>
                        </tr>
                     </tbody>
                  </table>
               </center>
            </div>
            <p>
               <a href="animation.html#ex_repetition" title="Example&nbsp;6.54.&nbsp;Creating animations that repeat">Example&nbsp;6.54, &#8220;Creating animations that repeat&#8221;</a> shows how both classes AnimationFactory and 
               ViewAnimationFactory can be used to create an animation object that combines 
               structural aspect (repetition) and visual effect (blinking). 
               
            </p>
            <div class="example"><a name="ex_repetition"></a><p class="title"><b>Example&nbsp;6.54.&nbsp;Creating animations that repeat</b></p><pre class="programlisting java">
AnimationObject createRepeatedBlinking(NodeRealizer nr, 
                                       Graph2DViewRepaintManager rm, int times)
{
  // Create the view animation factory and register the given repaint manager 
  // as the responsible animation listener. 
  ViewAnimationFactory vaf = new ViewAnimationFactory(rm);
  // The node realizer is automatically added to the repaint manager's list of 
  // objects to update. 
  // Note that the "blink" animation itself lasts 100 milliseconds. 
  AnimationObject blink = vaf.blink(nr, 100);
  
  // Return an animation object that repeats the "blink" animation for the 
  // specified number of times. 
  return AnimationFactory.createRepetition(blink, times, true);
}
</pre></div>
         </div>
         <div class="section" lang="en">
            <div class="titlepage">
               <div>
                  <div>
                     <h3 class="title"><a name="animation_tutorial"></a>Tutorial Demo Code
                     </h3>
                  </div>
               </div>
               <div></div>
            </div>
            <p>
               The tutorial demo applications 
               
            </p>
            <div class="itemizedlist">
               <ul type="disc">
                  <li><a href="../../src/demo/view/anim/EaseInEaseOutDemo.java" title="Link to demo code" target="_top">EaseInEaseOutDemo.java</a>, 
                     
                  </li>
                  <li><a href="../../src/demo/view/anim/FadeInFadeOutDemo.java" title="Link to demo code" target="_top">FadeInFadeOutDemo.java</a>, and 
                     
                  </li>
                  <li><a href="../../src/demo/view/anim/AnimationEffectsDemo.java" title="Link to demo code" target="_top">AnimationEffectsDemo.java</a> (together with 
                     <a href="../../src/demo/view/anim/AnimationEffectsDemoBase.java" title="Link to demo code" target="_top">AnimationEffectsDemoBase.java</a>) 
                     
                  </li>
               </ul>
            </div>
            <p>
               
               demonstrate usage of the animation framework and also many of the effects 
               provided by factory class ViewAnimationFactory. 
               
               <a href="../../src/demo/view/anim/AnimatedStructuralChangesDemo.java" title="Link to demo code" target="_top">AnimatedStructuralChangesDemo.java</a> shows 
               how to highlight structural changes of a graph.
               
            </p>
         </div>
      </div>
      <table class="copyright" border="0" cellpadding="0" cellspacing="0" width="100%">
         <tbody>
            <tr>
               <td align="right">
                  <p class="copyright">Copyright &copy;2004-2015, yWorks GmbH. All rights reserved.</p>
               </td>
            </tr>
         </tbody>
      </table>
      <div class="navfooter">
         <div class="navline2"></div>
         <div style="display:none"><img src="figures/navline2.jpg" alt=""><img src="figures/navbg2.jpg" alt=""></div>
         <table width="100%" summary="Navigation footer">
            <tr>
               <td width="40%" align="left"><a accesskey="p" href="printing.html">Prev</a>&nbsp;
               </td>
               <td width="20%" align="center"><a accesskey="u" href="view.html">Up</a></td>
               <td width="40%" align="right">&nbsp;<a accesskey="n" href="advanced_stuff.html">Next</a></td>
            </tr>
            <tr>
               <td width="40%" align="left" valign="top">Printing a Graph's Visual Representation&nbsp;</td>
               <td width="20%" align="center"><a accesskey="h" href="index.html" target="_top">Home</a></td>
               <td width="40%" align="right" valign="top">&nbsp;Advanced Application Logic</td>
            </tr>
         </table>
      </div>
   </body>
</html>