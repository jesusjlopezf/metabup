/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package fragments;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Value Annotation Param</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fragments.PrimitiveValueAnnotationParam#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fragments.FragmentsPackage#getPrimitiveValueAnnotationParam()
 * @model
 * @generated
 */
public interface PrimitiveValueAnnotationParam extends ParamValue {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(PrimitiveValue)
	 * @see fragments.FragmentsPackage#getPrimitiveValueAnnotationParam_Value()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PrimitiveValue getValue();

	/**
	 * Sets the value of the '{@link fragments.PrimitiveValueAnnotationParam#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(PrimitiveValue value);

} // PrimitiveValueAnnotationParam
