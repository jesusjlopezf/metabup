/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Named After</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.NamedAfter#getElementSels <em>Element Sels</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getNamedAfter()
 * @model
 * @generated
 */
public interface NamedAfter extends ElementQualifier {
	/**
	 * Returns the value of the '<em><b>Element Sels</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element Sels</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element Sels</em>' containment reference.
	 * @see #setElementSels(Selector)
	 * @see metamodeltest.MetamodeltestPackage#getNamedAfter_ElementSels()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Selector getElementSels();

	/**
	 * Sets the value of the '{@link metamodeltest.NamedAfter#getElementSels <em>Element Sels</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element Sels</em>' containment reference.
	 * @see #getElementSels()
	 * @generated
	 */
	void setElementSels(Selector value);

} // NamedAfter
