/****************************************************************************
 * This demo file is part of the yFiles extension package ySVG-2.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * ySVG demo files exhibit ySVG functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a ySVG version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own ySVG powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the ySVG
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.svg;

import y.view.YRenderingHints;
import yext.svg.io.SVGDOMEnhancer;
import yext.svg.io.SVGIOHandler;

import y.base.Edge;
import y.base.EdgeMap;
import y.base.Node;
import y.util.Maps;
import y.view.EdgeRealizer;
import y.view.Graph2D;
import y.view.Graph2DView;

import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.Point;

/** 
 * Demonstrates how to write SVG that shows rollover effects when the mouse
 * is over nodes or edges of the graph. When the mouse is over a node the node will
 * be drawn twice its original size. When the mouse is over an edge the edge
 * will be drawn as if it were in a selected state.
 * @see <a href="http://docs.yworks.com/yfiles/doc/developers-guide/svg_customization.html#svg_customization">Section Customizing SVG Content</a> in the yFiles for Java Developer's Guide
 */
public class RolloverDemo extends SVGExportDemo
{

  /**
   * Creates and configures the SVGIOHandlers to be used.
   * This method will add a specialized SVGDOMEnhancer to
   * the IOHandler.
   */
  protected SVGIOHandler createSVGIOHandler(boolean svgz)
  {
    SVGIOHandler ioh = super.createSVGIOHandler(svgz);
    SVGDOMEnhancer enhancer = new RolloverEnhancer();
    //draw edges behind nodes so that the node rollover does not get spoiled.
    enhancer.setDrawEdgesFirst(true); 
    ioh.setSVGGraph2DRenderer(enhancer);
    return ioh;
  }
  
  /**
   * A custom SVGDOMEnhancer. This class will do two thing to the SVG:
   * First, it will add hyperlink statements around each node, node label, edge and
   * edge label for which the user has given a URL, and second it will
   * add code that scales the nodes on mouse over events by a factor of 2.
   */
  class RolloverEnhancer extends SVGDOMEnhancer
  {
    private double mouseOverScaleFactor = 1.25;
    private EdgeMap highlightType;
    double viewZoom;
    Point viewPoint;
    
    /**
     * Override a callback method. Before the graph will be written to the SVG DOM 
     * we add an additional javascript node to the definition block of the SVG. 
     * The script will be used to scale the nodes on mouseover events.
     */ 
    protected void initializeDOM()
    {
      Graph2DView activeView = getSVGIOHandler().getActiveGraph2DView();
      Graph2D graph = activeView.getGraph2D();
      viewZoom = activeView.getZoom();
      viewPoint = activeView.getViewPoint();
      
      highlightType = Maps.createIndexEdgeMap(new boolean[graph.E()]);
      addToSVGDefinition(createScript());
    }
    
    protected void paint(Graphics2D gfx, EdgeRealizer r, boolean sloppyMode)
    {
      boolean disabled = !YRenderingHints.isSelectionPaintingEnabled(gfx);
      if (disabled) {
        gfx.setRenderingHint(YRenderingHints.KEY_SELECTION_PAINTING, null);
      }

      Edge e = r.getEdge();
      boolean isSelected = r.isSelected();
     
      highlightType.setBool(e, true);
      r.setSelected(true);
      super.paint(gfx, r, sloppyMode);
      
      highlightType.setBool(e, false);
      r.setSelected(false);
      super.paint(gfx, r, sloppyMode);
      
      r.setSelected(isSelected);

      if (disabled) {
        gfx.setRenderingHint(
                YRenderingHints.KEY_SELECTION_PAINTING,
                YRenderingHints.VALUE_SELECTION_PAINTING_OFF);
      }
    }
    
    protected String createGroupID(Edge e)
    {
      if(highlightType.getBool(e))
      {
        return "y.edge." + e.index() + ".highlight";
      }
      else
      {
        return "y.edge." + e.index();
      }
    }
        
    /**
     * Override a callback method. 
     * Adds onmouseover and onmouseout attributes to the element that represents
     * a given yNode in SVG. These attributes trigger the javascript function defined
     * above.
     */
    protected void nodeAddedToDOM(Node yNode, org.w3c.dom.Element element)
    {
      Graph2D graph = (Graph2D)yNode.getGraph();
      int x = (int)((graph.getCenterX(yNode) - viewPoint.x)*viewZoom);
      int y = (int)((graph.getCenterY(yNode) - viewPoint.y)*viewZoom);
      String id = element.getAttribute("id");
      element.setAttribute(
        "onmouseover", 
        "scaleNode(evt,'" + id + "'," + x + "," + y + "," + mouseOverScaleFactor + ")");
      element.setAttribute(
        "onmouseout", 
        "scaleNode(evt,'" + id + "'," + x + "," + y + ",1)");  
    }
    
    /**
     * Override a callback method. Hyperlink the given edge.
     */
    protected void edgeAddedToDOM(Edge yEdge, org.w3c.dom.Element element)
    {
      String id = "y.edge." + yEdge.index();
      if(highlightType.getBool(yEdge))
      {
        element.setAttribute("style", "visibility:hidden");
        element.setAttribute("onmouseover", "selectEdge(evt,'" + id + "')");
        element.setAttribute("onmouseout", "deselectEdge(evt,'" + id + "')");
      } 
      else
      {
        element.setAttribute("style", "visibility:visible");
        element.setAttribute("onmouseover", "selectEdge(evt,'" + id + "')");
        element.setAttribute("onmouseout", "deselectEdge(evt,'" + id + "')");

      }
    }
    
    ////////////////////////////////////////////////////////////
    // stuff needed for the "scale node on mouse over" effect //
    ////////////////////////////////////////////////////////////
    
    /**
     * Creates a DOM element that defines a ecmascript functions responsible
     * for scaling named DOM groups by a given factor.
     * @return the ecmascript function definitions for scaling named DOM groups.
     */
    private org.w3c.dom.Element createScript()
    {
      org.w3c.dom.Element script = createElement("script");
      script.setAttribute("type","text/ecmascript");
      
      // Javascript code that works with both Adobe SVG Viewer plugin
      // but also with native SVG support in modern browsers.
      String selectEdge =
        "function selectEdge(evt, elementId) { \n" +
        "var document = evt.target.ownerDocument; \n" +
        "var element = document.getElementById(elementId); \n" +
        "element.setAttributeNS(null, 'style', 'visibility:hidden'); \n" +
        "element = document.getElementById(elementId + '.highlight'); \n" +
        "element.setAttributeNS(null, 'style', 'visibility:visible'); \n" +
        "}";

      String deselectEdge =
        "function deselectEdge(evt, elementId) { \n" +
        "var document = evt.target.ownerDocument; \n" +
        "var element = document.getElementById(elementId); \n" +
        "element.setAttributeNS(null, 'style', 'visibility:visible'); \n" +
        "element = document.getElementById(elementId + '.highlight'); \n" +
        "element.setAttributeNS(null, 'style', 'visibility:hidden'); \n" +
        "}";

      String scaleNode =
        "function scaleNode(evt, elementId, x, y, factor) { \n" +
        "var document = evt.target.ownerDocument; \n" +
        "var element = document.getElementById(elementId); \n" +
        "var newtransform = \"translate(\" + x + \",\" + y + \") scale(\" + factor + \") translate(-\" + x + \",-\" + y + \")\"; \n" +
        "element.setAttributeNS(null, 'transform', newtransform); \n" +
        "}";
      
      script.appendChild(createCDATASection(
        selectEdge + '\n' + 
        deselectEdge + '\n' + 
        scaleNode
        ));
      
      return script;
    }
    
  }

  /**
   * Launches this demo.
   * @param args ignored.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        initLnF();
        (new RolloverDemo()).start();
      }
    });
  }
}