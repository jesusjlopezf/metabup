/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.extensionpoints;

import org.eclipse.jface.dialogs.MessageDialog;

import metabup.metamodel.MetaModel;
import metabup.ui.editor.popup.actions.MetaBupEditorAction;

public abstract class MetaBupMetaModelEditActionContribution extends MetaBupEditorAction implements IMetaBupMetaModelEditActionContribution {
	private MetaModel mm;
	private String ERROR_MESSAGE = "The metamodel doesn't meet the conditions for applying this rule";
	
	@Override
	public final void run(){
		mm = super.getSession().getMetamodelRoot();
		if(checkConditions(mm)){
			mm = editMetaModel(mm);
			editor.getSession().saveChanges();
			editor.refreshViewer();		
		}
		else MessageDialog.openError(null, "Action couldn't be performed", ERROR_MESSAGE);	
	}
	
	protected void setErrorMessage(String msg){
		this.ERROR_MESSAGE = msg;
	}
	
}
