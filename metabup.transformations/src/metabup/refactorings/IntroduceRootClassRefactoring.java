/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.refactorings;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import metabup.metamodel.Annotation;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;
import metabup.transformations.models.WMetamodelModel;

import static metabup.transformations.models.ModelUtil.select;

public class IntroduceRootClassRefactoring extends MetamodelRefactoring {
	
	private MetaClass rootMetaclass;

	public IntroduceRootClassRefactoring(WMetamodelModel metamodel) {
		super(metamodel);
	}

	@Override
	public boolean execute() {
		HashSet<MetaClass> nonReferencedClasses = new HashSet<MetaClass>();
		MetaModel mm = metamodel.rootMetamodel();
		nonReferencedClasses.addAll(mm.getClasses());
		
		EList<MetaClass> classes = mm.getClasses();
		for (MetaClass metaClass : classes) {
			nonReferencedClasses.removeAll(metaClass.getSubclasses());
			List<Reference> references = select(metaClass.getFeatures(), Reference.class);
			for (Reference reference : references) {
				/*
				// heuristic: container classes should be part of the root, even if they are reference
				// TODO: Refine by looking for inclusions (if available)
				EList<AssistanceTip> annotations = reference.getReference().getAnnotations();
				for (AssistanceTip annotation : annotations) {
					if ( annotation.getName().startsWith("container") ) 
						continue;
				}	
				*/
				
				nonReferencedClasses.remove(reference.getReference());
			}
		}
		
		// Cannot infer -> require user intervention
		if ( nonReferencedClasses.size() == 0 ) 
			return false;
		
		MetaClass rootMetaclass = metamodel.createMetaclass();
		this.rootMetaclass = rootMetaclass;
		rootMetaclass.setName("Root");
		metamodel.rootMetamodel().getClasses().add(rootMetaclass);
		for (MetaClass metaClass : nonReferencedClasses) {
			Reference reference = metamodel.createReference();
			reference.setReference(metaClass);
			reference.setName(metaClass.getName() + "s");
			reference.setMax(-1);
			
			reference.getAnnotations().add(createContainmentAnnotation());
			
			rootMetaclass.getFeatures().add(reference);
		}
		
		return true;
	}

	private Annotation createContainmentAnnotation() {
		Annotation annotation = metamodel.createAnnotation();
		annotation.setName("composition");		
		return annotation;
	}

	public MetaClass getRootMetaclass() {
		return rootMetaclass;
	}
	
}
