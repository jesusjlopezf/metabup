/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.popup.actions;

import metabest.ui.views.FragmentAssertionEvaluationView;
import metabest.ui.views.MetamodelAssertionEvaluationView;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;

import test.TestModel;
import metamodeltest.AssertionTest;
import metamodeltest.MetaModelTest;

public class MetaBestMetamodelValidationAction implements IObjectActionDelegate {

	private Shell shell;
	protected ISelection selection;
	
	/**
	 * Constructor for Action1.
	 */
	public MetaBestMetamodelValidationAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if(selection instanceof IStructuredSelection){
			Object obj[] = ((IStructuredSelection) selection).toArray();
			IFile file = IFileUtils.convertObjectToIFile(obj[0]);
			ResourceSet rs = new ResourceSetImpl();
			
			org.eclipse.emf.common.util.URI uri = org.eclipse.emf.common.util.URI.createURI(file.getLocationURI().toString());
			Resource r = rs.getResource(uri, true);
			
			try {								
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(MetamodelAssertionEvaluationView.ID);				
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				page.hideView(page.findView(FragmentAssertionEvaluationView.ID));
				
				IViewReference viewReferences[] = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
				MetamodelAssertionEvaluationView av = null;
				
				for (int i = 0; i < viewReferences.length; i++) {
					if (MetamodelAssertionEvaluationView.ID.equals(viewReferences[i].getId())){
						av = (MetamodelAssertionEvaluationView)viewReferences[i].getView(true);
						av.refresh(r, null);
					}
				}		
			} catch (PartInitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
		
	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

}
