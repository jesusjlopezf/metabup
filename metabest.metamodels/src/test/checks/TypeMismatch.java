/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks;

import test.checks.expressions.FeatureTypeExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Mismatch</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.TypeMismatch#getFeatureType <em>Feature Type</em>}</li>
 * </ul>
 *
 * @see test.checks.ChecksPackage#getTypeMismatch()
 * @model
 * @generated
 */
public interface TypeMismatch extends Mismatch {
	/**
	 * Returns the value of the '<em><b>Feature Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Type</em>' containment reference.
	 * @see #setFeatureType(FeatureTypeExpression)
	 * @see test.checks.ChecksPackage#getTypeMismatch_FeatureType()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FeatureTypeExpression getFeatureType();

	/**
	 * Sets the value of the '{@link test.checks.TypeMismatch#getFeatureType <em>Feature Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature Type</em>' containment reference.
	 * @see #getFeatureType()
	 * @generated
	 */
	void setFeatureType(FeatureTypeExpression value);

} // TypeMismatch
