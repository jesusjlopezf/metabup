/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.popup.actions;

import metabup.SessionState;
import metabup.metamodel.AnnotatedElement;
import metabup.ui.editor.editors.MetamodelInteractiveEditor;

import java.util.List;

import org.eclipse.jface.action.Action;

public abstract class MetaBupEditorAction extends Action {
	protected MetamodelInteractiveEditor editor = null;
	List<AnnotatedElement> selected = null;

	public void setEditor(MetamodelInteractiveEditor editor) {
		this.editor = editor;
	};
	
	public SessionState getSession() {
		return editor.getSession();
	}
	
	public AnnotatedElement getSelection(){
		return editor.getSelection();
	}

}
