/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver.ui.wizards.dnd;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;


public class TreeContentProvider implements IStructuredContentProvider, ITreeContentProvider {
	private static Object[] EMPTY_ARRAY = new Object[0]; 
	
	public TreeContentProvider(){}
	
  @Override
  public Object[] getChildren(Object parentElement) {
	  //System.out.println("getChildren");
	  if(parentElement instanceof List<?>){
		  List<AnnotatedElement> list = (List<AnnotatedElement>) parentElement;
		  return  list.toArray();
	  }
	  if(parentElement instanceof MetaModel){
		  MetaModel metamodel = (MetaModel) parentElement;
		  
		  List<MetaClass> list = new ArrayList<MetaClass>();
		  
		  for(MetaClass mc : metamodel.getClasses())
			  if(!mc.getIsAbstract()) 
				  list.add(mc);
		  
		  
		  return list.toArray();
	  }
	  
	  if(parentElement instanceof MetaClass){
		  MetaClass mc = (MetaClass) parentElement;
		  return mc.getFeatures(true).toArray();
	  }
	  
	  return EMPTY_ARRAY;
  }

  @Override
  public Object getParent(Object element) {
	  //System.out.println("getParent");
	  if(element instanceof MetaClass){
		  MetaClass mc = (MetaClass) element;
		  return mc.eContainer();
	  }
	  if(element instanceof Feature){
		  Feature feature = (Feature) element;
		  return feature.eContainer();
	  }

	  return EMPTY_ARRAY;
  }

  @Override
  public boolean hasChildren(Object element) {
	  //System.out.println("hasChildren");
	  if(element instanceof MetaModel){
		  MetaModel metamodel = (MetaModel) element;
		  return (metamodel.getClasses().size() > 0);
	  }
	  if(element instanceof MetaClass){
		  MetaClass mc = (MetaClass) element;
		  return (mc.getFeatures(true).size() > 0);
	  }
	  
	  return false;
  }

	  @Override
	  public Object[] getElements(Object inputElement) {
		  //System.out.println("getElements");
		  return getChildren(inputElement);
	  }
	  
	  
	@Override
	public void dispose() {
	}
	
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
		
	}
} 
