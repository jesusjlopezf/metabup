/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.extensionpoint;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EAnnotation;

import metabup.annotations.IConstraintAnnotation;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.Reference;

public abstract class ConstraintAnnotation extends UserAnnotation implements IConstraintAnnotation {
	private HashMap<String, String> constraints = new HashMap<String, String>();

	
	public ConstraintAnnotation(){
		this.setPersistent(true);		
	}
	
	@Override
	public final boolean when() {
		return true;
	}
	
	@Override
	public final MetaModel perform() {
		return this.getTargetMetaModel();
	}
	
	public List<EAnnotation> getEAnnotations(){
		return null;
	}
	
	@Override
	public void setElement(AnnotatedElement element) {
		super.setElement(element);
		updateConstraints();
	}
	
	public final HashMap<String, String> getConstraints(){
		if(!this.isPersistent()) return null;
		//System.out.println("annotated element: " + element);		
		
		if(element != null){
			Iterator it = constraints.entrySet().iterator();
			
			String name = "";
			String refsrc = "";
			String reftgt = "";
			
			if(element instanceof MetaClass) name = ((MetaClass) element).getName();
			else if(element instanceof Feature) name = ((Feature) element).getName();
			
			if(element instanceof Reference){
				Reference r = (Reference)element;
				refsrc = ((MetaClass)r.eContainer()).getName();
				reftgt = r.getReference().getName();
			}
			
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry)it.next();
				
				constraints.put(e.getKey().toString(), ((String)e.getValue()).replaceAll("ELEMENT", name));
				constraints.put(e.getKey().toString(), ((String)e.getValue()).replaceAll("REFSRC", refsrc));
				constraints.put(e.getKey().toString(), ((String)e.getValue()).replaceAll("REFTGT", reftgt));
				//if(!constraint.startsWith("context")) constraint = "context " + mc.getName() + " " + constraint;
			}
		}
		
		return this.constraints;
	}
	
	/*
	 * A series of tokens found in the value string will be furtherly interpreted when generating OCL code, being:
	 * ELEMENT - The annotated element's name
	 * REFSRC - The containing class' name, in case of annotating a reference
	 * REFTGT - The target class' name, in case of annotating a reference
	 */
	protected final void addConstraint(String key, String value){
		if(key != null && value != null && this.isPersistent()) this.constraints.put(key, value);
	}
}
