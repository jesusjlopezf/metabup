/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.ui.wizards.pages;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.internal.resources.Container;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.eclipse.ui.dialogs.ResourceListSelectionDialog;

/**
 * The "New" wizard page allows setting the container for the new file as well
 * as the file name. The page will only accept file name without the extension
 * OR with the extension that matches the expected one (mbest).
 */

public class NewMetaBestTestFragmentPage extends WizardPage {
	private Text containerText;
	private Text targetMetamodelText;
	private Text fileText;
	
	private Text baseFragmentText;
	private Text fragmentLegendText;
	private String fragmentType = null; // null, "dia" or "graphml"
	
	private IProject project = null;

	private ISelection selection;

	/**
	 * Constructor for SampleNewWizardPage.
	 * 
	 * @param pageName
	 */
	public NewMetaBestTestFragmentPage(ISelection selection) {
		super("wizardPage");
		setTitle("Multi-page Editor File");
		setDescription("This wizard creates a new file with *.mbest extension that can be opened by a multi-page editor.");
		this.selection = selection;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		final Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();		
		container.setLayout(layout);
		layout.numColumns = 3;
		layout.verticalSpacing = 9;				
		
		Label label = new Label(container, SWT.NULL);
		label.setText("&Container:");

		containerText = new Text(container, SWT.BORDER | SWT.SINGLE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		containerText.setLayoutData(gd);
		containerText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});

		Button button = new Button(container, SWT.PUSH);
		button.setText("Browse...");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				handleBrowse();
			}
		});
		
		label = new Label(container, SWT.NULL);
		label.setText("&Target metamodel:");
		
		targetMetamodelText = new Text(container, SWT.BORDER | SWT.SINGLE);
		targetMetamodelText.setLayoutData(gd);		
		targetMetamodelText.setEnabled(false);

		Button button2 = new Button(container, SWT.PUSH);
		button2.setText("Browse...");
		button2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				try {
					handleMetamodelBrowse();
				} catch (CoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		label = new Label(container, SWT.NULL);
		label.setText("&File name:");

		fileText = new Text(container, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		fileText.setLayoutData(gd);
		fileText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				dialogChanged();
			}
		});
		
		label = new Label(container, SWT.NULL);
		label.setText("");
		
		Group g = new Group(container, 0);
	    g.setSize(200, 100);
	    g.setText("Base fragment:");
	    
	    Button b1 = new Button(g, SWT.RADIO);
	    Button b2 = new Button(g, SWT.RADIO);	    
	    Button b3 = new Button(g, SWT.RADIO);
	    
	    b1.setBounds(10, 20, 200, 25);	    
	    b1.setText("Blank fragment");
	    b1.setSelection(true); // default selection

	    b2.setBounds(10, 45, 200, 25);
	    b2.setText("Import Dia fragment");	    
	    
	    b3.setBounds(10, 70, 200, 25);
	    b3.setText("Import yED fragment");
		
	    label = new Label(container, SWT.NULL);
		label.setText("");
		label = new Label(container, SWT.NULL);
		label.setText("");
		
		label = new Label(container, SWT.NULL);
		label.setText("Fragment file:");
	    
	    baseFragmentText = new Text(container, SWT.BORDER | SWT.SINGLE);
		baseFragmentText.setLayoutData(gd);		
		baseFragmentText.setEnabled(false);
				
		final Button button3 = new Button(container, SWT.PUSH);
		button3.setText("Browse...");
		button3.setEnabled(false);
		button3.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				handleBaseFragmentBrowse(container);
			}
		});
		
		label = new Label(container, SWT.NULL);
		label.setText("Legend file:");
	    
	    fragmentLegendText = new Text(container, SWT.BORDER | SWT.SINGLE);
		fragmentLegendText.setLayoutData(gd);		
		fragmentLegendText.setEnabled(false);
				
		final Button button4 = new Button(container, SWT.PUSH);
		button4.setText("Browse...");
		button4.setEnabled(false);
		button4.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				handleFragmentLegendBrowse(container);
			}
		});
		
		b1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				if(button3.isEnabled()) button3.setEnabled(false);
				if(button4.isEnabled()) button4.setEnabled(false);
				baseFragmentText.setText("");
				fragmentLegendText.setText("");
				fragmentType = null;
			}
		});
		
		b2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				if(!button3.isEnabled()) button3.setEnabled(true);
				if(!button4.isEnabled()) button4.setEnabled(true);
				baseFragmentText.setText("");
				fragmentLegendText.setText("");
				fragmentType = "dia";
			}
		});
		
		b3.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e){
				if(!button3.isEnabled()) button3.setEnabled(true);
				if(!button4.isEnabled()) button4.setEnabled(true);
				baseFragmentText.setText("");
				fragmentLegendText.setText("");
				fragmentType = "graphml";
			}
		});
		
		initialize();
		dialogChanged();
		setControl(container);
	}
	
	protected void handleMetamodelBrowse() throws CoreException {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		String projectName = containerText.getText().substring(1);
		System.out.println(projectName);
		if(projectName.contains("/")) projectName = projectName.substring(0, projectName.indexOf('/'));
		IProject folder = root.getProject(projectName);		
		List<IFile> metamodels = getMetamodelsFromFolder(folder);
		
		if(metamodels != null){										
			ResourceListSelectionDialog dialog = new ResourceListSelectionDialog(null, metamodels.toArray(new IFile[]{}));
			dialog.setTitle("Please select a target metamodel for the testcase");
			
			if(dialog.open() == ResourceListSelectionDialog.OK){
				Object[] result = dialog.getResult();
				if (result.length == 1){
					targetMetamodelText.setText(((IFile) result[0]).getFullPath().toString());
					this.project = folder;
				}
			}
		}
	}
	
	protected void handleBaseFragmentBrowse(Composite container) {
		FileDialog dialog = new FileDialog(container.getShell());
		dialog.setText("Select a " + fragmentType + "file...");
		dialog.setFilterExtensions(new String[] { "*." + this.fragmentType } );
		String fileName = dialog.open();
		
		if(fileName != null){
			Path path = new Path(fileName);
			baseFragmentText.setText(path.toOSString());
		}			
	}
	
	protected void handleFragmentLegendBrowse(Composite container) {
		FileDialog dialog = new FileDialog(container.getShell());
		dialog.setText("Select a " + fragmentType + "file...");
		dialog.setFilterExtensions(new String[] { "*." + this.fragmentType } );
		String fileName = dialog.open();
		
		if(fileName != null){
			Path path = new Path(fileName);
			fragmentLegendText.setText(path.toOSString());
		}			
	}

	private List<IFile> getMetamodelsFromFolder(IContainer container) throws CoreException{				
		IResource folderMembers[] = container.members();
		List<IFile> queries = new ArrayList<IFile>();						
		
		for(int i=0; i<folderMembers.length; i++){
			if((folderMembers[i].getFileExtension() != null) && (folderMembers[i].getFileExtension().equals("mbup"))){
				queries.add((IFile)folderMembers[i]);				
			}else{
				if(folderMembers[i].getFileExtension() == null){
					List<IFile> moreQueries = getMetamodelsFromFolder((IContainer)folderMembers[i]);
					
					if(!moreQueries.isEmpty()){
						queries.addAll(moreQueries);
					}
				}
			}
		}
		
		return queries;
	}	
	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */

	private void initialize() {
		if (selection != null && selection.isEmpty() == false
				&& selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			if (ssel.size() > 1)
				return;
			Object obj = ssel.getFirstElement();
			if (obj instanceof IResource) {
				IContainer container;
				if (obj instanceof IContainer)
					container = (IContainer) obj;
				else
					container = ((IResource) obj).getParent();
				containerText.setText(container.getFullPath().toString());
			}
		}
		fileText.setText("new_testcase.mbf");
	}

	/**
	 * Uses the standard container selection dialog to choose the new value for
	 * the container field.
	 */

	private void handleBrowse() {
		ContainerSelectionDialog dialog = new ContainerSelectionDialog(
				getShell(), ResourcesPlugin.getWorkspace().getRoot(), false,
				"Select new file container");
		if (dialog.open() == ContainerSelectionDialog.OK) {
			Object[] result = dialog.getResult();
			if (result.length == 1) {
				containerText.setText(((Path) result[0]).toString());
			}
		}
	}

	/**
	 * Ensures that both text fields are set.
	 */

	private void dialogChanged() {
		IResource container = ResourcesPlugin.getWorkspace().getRoot()
				.findMember(new Path(getContainerName()));
		String fileName = getFileName();

		if (getContainerName().length() == 0) {
			updateStatus("File container must be specified");
			this.targetMetamodelText.setEnabled(false);
			return;
		}
		if (container == null
				|| (container.getType() & (IResource.PROJECT | IResource.FOLDER)) == 0) {
			updateStatus("File container must exist");
			this.targetMetamodelText.setEnabled(false);
			return;
		}
		if (!container.isAccessible()) {
			updateStatus("Project must be writable");
			this.targetMetamodelText.setEnabled(false);
			return;
		}
		if (fileName.length() == 0) {
			updateStatus("File name must be specified");
			this.targetMetamodelText.setEnabled(false);
			return;
		}
		if (fileName.replace('\\', '/').indexOf('/', 1) > 0) {
			updateStatus("File name must be valid");
			this.targetMetamodelText.setEnabled(false);
			return;
		}
		int dotLoc = fileName.lastIndexOf('.');
		if (dotLoc != -1) {
			String ext = fileName.substring(dotLoc + 1);
			if (ext.equalsIgnoreCase("mbf") == false) {
				updateStatus("File extension must be \"mbf\"");
				this.targetMetamodelText.setEnabled(false);
				return;
			}
		}
		
		this.targetMetamodelText.setEnabled(true);
		updateStatus(null);
	}

	private void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}

	public String getContainerName() {
		return containerText.getText();
	}
	
	public String getTargetMetamodelName(){
		if(this.targetMetamodelText.isEnabled()) return targetMetamodelText.getText();
		else return null;
	}
	
	public String getFileName() {
		return fileText.getText();
	}
	
	public String getFragmentType(){
		return this.fragmentType;
	}
	
	public String getFragmentFile(){
		return baseFragmentText.getText();
	}
	
	public String getLegendFile(){
		return fragmentLegendText.getText();
	}
	

	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}
}