/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package metabup.shell;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import fragments.Fragment;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link metabup.shell.ShellModel#getName <em>Name</em>}</li>
 *   <li>{@link metabup.shell.ShellModel#getCommands <em>Commands</em>}</li>
 * </ul>
 *
 * @see metabup.shell.ShellPackage#getShellModel()
 * @model
 * @generated
 */
public interface ShellModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see metabup.shell.ShellPackage#getShellModel_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link metabup.shell.ShellModel#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
	 * The list contents are of type {@link metabup.shell.Command}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commands</em>' containment reference list.
	 * @see metabup.shell.ShellPackage#getShellModel_Commands()
	 * @model containment="true"
	 * @generated
	 */
	EList<Command> getCommands();

} // ShellModel
