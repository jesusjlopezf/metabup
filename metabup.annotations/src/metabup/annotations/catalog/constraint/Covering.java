/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.catalog.constraint;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import fragments.Fragment;
import fragments.FragmentModel;
import fragments.Object;
import metabup.annotations.extensionpoint.ConstraintAnnotation;
import metabup.annotations.extensionpoint.DesignAnnotation;
import metabup.issues.general.IOpenIssue;
import metabup.issues.lib.AcyclicReferenceCycle;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Annotation;
import metabup.metamodel.AnnotationParam;
import metabup.metamodel.ElementValue;
import metabup.metamodel.MetaClass;
import metabup.metamodel.ParamValue;
import metabup.metamodel.Reference;

public class Covering extends ConstraintAnnotation {
	public Covering (){
		this.setName("covering");
		this.setDescription("A given set of references pointing to the same class A is jointly surjective: each A object receives some reference from the set of references.");
	}
	
	@Override
	public List<IOpenIssue> detectFragmentIssues(FragmentModel fm, AnnotatedElement ae){

		//TO-DO
		return null;
	}

	@Override
	public void updateConstraints() {
		if(!(element instanceof metabup.metamodel.Reference)) return;
		List<ParamValue> params = new ArrayList<ParamValue>();
		int j = 1;
		
		Annotation ann = null;
		
		for(Annotation a : element.getAnnotations()){
			if(a.getName().toLowerCase().equals(this.getName().toLowerCase())){
				ann = a;
				break;
			}
		}
		
		ParamValue pv;
		
		while((pv = ann.getParamValueByParamName("ref" + j)) != null){
			params.add(pv);		
			j++;
		}
		
		if(params == null || params.size() < 1) return;
		
		String REFSRC = ((MetaClass)element.eContainer()).getName();
		//String ELEMENT = ((metabup.metaModel.Reference)element).getName();
		String REFTAR = ((metabup.metamodel.Reference)element).getReference().getName();
		
		String constraint = "context " + REFTAR + " inv " + REFSRC + "_covering: ";
		
		List<metabup.metamodel.Reference> refs = new ArrayList<metabup.metamodel.Reference>();
		
		for(int i = 0; i<params.size(); i++){		
			if(params.get(i) instanceof ElementValue){
				ElementValue ev = (ElementValue)params.get(i);
				
				if(ev.getValue() instanceof metabup.metamodel.Reference){
					metabup.metamodel.Reference r = (metabup.metamodel.Reference)ev.getValue();
					refs.add(r);
				}
			}
		}
		
		refs.add((metabup.metamodel.Reference)element);
		
		for(int i = 0; i<refs.size(); i++){
			MetaClass mcsrc = (MetaClass)refs.get(i).eContainer();
			if(i > 0) constraint += " or";
			constraint += "\n\t" + mcsrc.getName() + ".allInstances() -> exists ( o | o." + refs.get(i).getName() + " -> includes(self))";
			
		}
			
		super.addConstraint("covering", constraint);
	}

	/*private List<metabup.fragments.Reference> getReferences(Object o){
		List<metabup.fragments.Reference> references = new ArrayList<metabup.fragments.Reference>();
		
		for(metabup.fragments.Feature f : o.getFeatures())
			if(f instanceof metabup.fragments.Reference)
				references.add((metabup.fragments.Reference)f);
		
		return references;
	}*/
}
