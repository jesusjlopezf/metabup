package metabup.metamodel.edit.actions;

import java.util.List;

import org.apache.commons.lang.WordUtils;

import metabup.annotations.catalog.constraint.Root;
import metabup.annotations.catalog.design.Composition;
import metabup.extensionpoints.MetaBupMetaModelEditActionContribution;
import metabup.extensionpoints.MetaBupMetaModelExportActionContribution;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.Reference;
import metabup.metamodel.edit.common.MetaModelUtils;
import metabup.metamodel.impl.ReferenceImpl;

public class RemoveSelectedElement extends MetaBupMetaModelEditActionContribution {

	@Override
	public boolean checkConditions(MetaModel mm) {
		if(super.getSelection() != null) return true;
		return false;
	}

	@Override
	public MetaModel editMetaModel(MetaModel mm) {		
		if(super.getSelection() instanceof MetaClass){	
			MetaModelUtils.removeMetaClass(mm, (MetaClass) super.getSelection());
		}
		
		return mm;
	}
	
	
}
