/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.annotations.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import metabup.annotations.Activator;

/**
 * Class used to initialize default preference values.
 */
public class AnnotationsPluginPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();		
		store.setDefault(AnnotationsPluginPreferenceConstants.P_ANNOTATIONS_PACKAGE, "annotations");
		store.setDefault(AnnotationsPluginPreferenceConstants.P_ANNOTATIONS_LIB_FILES, "data/metabup.annotations.lib.jar");
	}

}
