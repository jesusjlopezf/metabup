/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.popup.actions;

import org.eclipse.jface.dialogs.MessageDialog;

public class GenerateCollaboroHistory extends MetaBupEditorAction {


	public void run() {
		/*
		XtextEditor x = editor.getXtextEditor();
		TextSelection txt = (TextSelection) x.getSelectionProvider().getSelection();
		String fragmentName = txt.getText();
		*/
		
		if ( ! editor.getSession().generateCollaboroHistory()) {
			MessageDialog.openError(null, "No changes", "No changes to generate");
			// MessageDialog.openInformation(null, "Editor", "Sample Action Executed");			
		} else {
			MessageDialog.openInformation(null, "collaboro.history generated", "Collaboro history file successfully generated");
		}
		
	}

}
