/**
 */
package graphicProperties.impl;

import org.eclipse.emf.ecore.EClass;

import graphicProperties.GraphicPropertiesPackage;
import graphicProperties.NodeProperties;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NodePropertiesImpl extends GraphicPropertiesImpl implements NodeProperties {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodePropertiesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GraphicPropertiesPackage.Literals.NODE_PROPERTIES;
	}

} //NodePropertiesImpl
