/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks;

import test.checks.expressions.ObjectTypeExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Type Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.checks.AbstractTypeInstance#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see test.checks.ChecksPackage#getAbstractTypeInstance()
 * @model
 * @generated
 */
public interface AbstractTypeInstance extends Check {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' containment reference.
	 * @see #setType(ObjectTypeExpression)
	 * @see test.checks.ChecksPackage#getAbstractTypeInstance_Type()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ObjectTypeExpression getType();

	/**
	 * Sets the value of the '{@link test.checks.AbstractTypeInstance#getType <em>Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' containment reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ObjectTypeExpression value);

} // AbstractTypeInstance
