/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations.models;

import java.util.ArrayList;
import java.util.List;

import fragments.Attribute;
import fragments.Feature;
import fragments.Fragment;
import fragments.Object;
import fragments.Reference;

public class FragmentUtil {
	public static List<Feature> alikeFeatures(Feature feature){
		Object o = (Object)feature.eContainer();
		String type = o.getType();
		Fragment f = (Fragment)o.eContainer();
		
		List<Feature> features = new ArrayList<Feature>();		
		
		for(Object ob : f.getObjects())
			if(ob.getType().equals(type))
				for(Feature fe : ob.getFeatures())
					if(fe.getName().equals(feature.getName()))
						features.add(fe);
		
		return features;
	}
	
	public static List<Object> alikeObjects(Object object){
		Fragment f = (Fragment)object.eContainer();
		String type = object.getType();
		List<Object> objects = new ArrayList<Object>();
		
		for(Object o : f.getObjects()) if(o.getType().equals(type)) objects.add(o);
		
		return objects;
	}
	
	public static int featureMinCardinality(Feature feature){
		int min = -1;
		Object o = (Object) feature.eContainer();
		
		List<Feature> ff = alikeFeatures(feature);
		List<Object> oo = alikeObjects(o);
		
		if(ff.size() < oo.size()) return min = 0;
		
		for(Feature f : ff){
			if(f instanceof Reference){
				Reference r = (Reference)f;
				int size = r.getReference().size();
				if(size < min || min == -1) min = size;
			}else{
				if (f instanceof Attribute){
					Attribute a = (Attribute)f;
					int size = a.getValues().size();
					if(size < min || min == -1) min = size;
				}
			}
		}
		
		return min;
	}
	
	public static int featureMaxCardinality(Feature feature){
		int max = 0;
		
		for(Feature f : alikeFeatures(feature)){
			if(f instanceof Reference){
				Reference r = (Reference)f;
				int size = r.getReference().size();
				if(size > max || max == 0) max = size;
			}else{
				if(f instanceof Attribute){
					Attribute a = (Attribute)f;
					int size = a.getValues().size();
					if(size > max || max == 0) max = size;
				}
			}
		}
		
		return max;
	}
}
