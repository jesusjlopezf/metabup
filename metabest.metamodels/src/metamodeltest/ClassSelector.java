/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Selector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.ClassSelector#getClassConditioner <em>Class Conditioner</em>}</li>
 *   <li>{@link metamodeltest.ClassSelector#getClassFilter <em>Class Filter</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getClassSelector()
 * @model
 * @generated
 */
public interface ClassSelector extends Selector {
	/**
	 * Returns the value of the '<em><b>Class Conditioner</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Conditioner</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Conditioner</em>' containment reference.
	 * @see #setClassConditioner(ClassFilter)
	 * @see metamodeltest.MetamodeltestPackage#getClassSelector_ClassConditioner()
	 * @model containment="true"
	 * @generated
	 */
	ClassFilter getClassConditioner();

	/**
	 * Sets the value of the '{@link metamodeltest.ClassSelector#getClassConditioner <em>Class Conditioner</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Conditioner</em>' containment reference.
	 * @see #getClassConditioner()
	 * @generated
	 */
	void setClassConditioner(ClassFilter value);

	/**
	 * Returns the value of the '<em><b>Class Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Filter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Filter</em>' containment reference.
	 * @see #setClassFilter(ClassFilter)
	 * @see metamodeltest.MetamodeltestPackage#getClassSelector_ClassFilter()
	 * @model containment="true"
	 * @generated
	 */
	ClassFilter getClassFilter();

	/**
	 * Sets the value of the '{@link metamodeltest.ClassSelector#getClassFilter <em>Class Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Filter</em>' containment reference.
	 * @see #getClassFilter()
	 * @generated
	 */
	void setClassFilter(ClassFilter value);

} // ClassSelector
