/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations;

import static metabup.transformations.models.ModelUtil.select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import metabup.annotations.IAnnotation;
import metabup.annotations.IAnnotationFactory;
import metabup.change.trace.ChangeTrace;
import metabup.change.trace.MetaModelChangeChecker;
import metabup.issues.general.IOpenIssue;
import metabup.issues.general.categories.Conflict;
import metabup.issues.lib.RenameMetaClass;
import metabup.issues.lib.UnsuffixMetaClass;
import metabup.issues.lib.UpgradeMaxMultiplicity;
import metabup.lexicon.utils.LexicalInflector;
import metabup.metamodel.AnnotatedElement;
import metabup.metamodel.Annotation;
import metabup.metamodel.Attribute;
import metabup.metamodel.DType;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;
import metabup.metamodel.MetamodelFactory;
import metabup.metamodel.Reference;
import metabup.transformations.models.WFragmentModel;
import metabup.transformations.models.WMetamodelModel;
import metabup.transformations.models.FragmentUtil;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.dialogs.ListSelectionDialog;
import fragments.BooleanValue;
import fragments.FragmentModel;
import fragments.FragmentsPackage;
import fragments.IntegerValue;
import fragments.ObjectValueAnnotationParam;
import fragments.PrimitiveValue;
import fragments.PrimitiveValueAnnotationParam;
import fragments.StringValue;


public class InduceMetaModel extends BaseTransformation {
	private WFragmentModel inModel;
	private WMetamodelModel outModel;

	// Variables of the transformation
	private MetaModel targetMetaModel = null;
	private RefactorMetaModel rm = null;
	private ChangeTrace changeTrace = null;
	private IAnnotationFactory af;
	private List<IOpenIssue> issues = new ArrayList<IOpenIssue>();		

	private HashMap<MetaClass, List<String>> pendingSubs = new HashMap<MetaClass, List<String>>();
	private HashMap<metabup.metamodel.Reference, String> pendingRefs = new HashMap<metabup.metamodel.Reference, String>();

	private boolean activeMode = true;
	
	public InduceMetaModel(WFragmentModel inModel, WMetamodelModel outModel, IAnnotationFactory factory) {				
		this(inModel, outModel, null, factory);		
	}	

	public InduceMetaModel(WFragmentModel inModel, WMetamodelModel outModel, ChangeTrace changeTrace, IAnnotationFactory factory) {		
		this.inModel = inModel;
		this.outModel = outModel;	
		this.changeTrace = changeTrace;
		this.af = factory;		
	}

	public void execute(boolean active) {	
		this.activeMode = active;
		MetaModel src = EcoreUtil.copy(this.outModel.rootMetamodel());		
		ruleFragmentSet2MetaModel();
		ruleObject2Class();		
		resumePending();
		assignAnnotations();
		this.triggerRefactorings(active);
		this.checkFragmentIssues();
		this.updateHistory(src, this.targetMetaModel);
	}	
	
	private void assignAnnotations(){
		for (fragments.Object o : inModel.allFragmentObjects() ) {
			MetaClass mm = outModel.rootMetamodel().getClassByName(o.getType());
			for (fragments.Annotation an : o.getAnnotation()) mm.getAnnotations().add(this.transformAnnotations(an));
		}
	}
	
	private void checkFragmentIssues() {
		TreeIterator<EObject> ti = outModel.rootMetamodel().eAllContents();
		
		while(ti.hasNext()){
			EObject eo = ti.next();
			if(eo instanceof AnnotatedElement){
				AnnotatedElement ae = (AnnotatedElement) eo;
				for(Annotation a : ae.getAnnotations()){
					Class<? extends IAnnotation> annotationClass = af.getAnnotation(a.getName());
					
					if (annotationClass != null){
						try {
							IAnnotation annotation = annotationClass.newInstance();
							List<IOpenIssue> fIssues = annotation.detectFragmentIssues(inModel.rootFragmentModel(), ae);																					
							if(fIssues != null) issues.addAll(fIssues);							
						} catch (InstantiationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}				
			}
		}
	}

	public ChangeTrace getChangeTrace() {
		return changeTrace;
	}
	
	public List<IOpenIssue> getIssues() {
		return issues;
	}
	
	public List<Conflict> getConflictIssues() {
		List<Conflict> conflicts = new ArrayList<Conflict>();
		
		for(IOpenIssue ioi : this.getIssues()) if(ioi instanceof Conflict){
			
			conflicts.add((Conflict)ioi);
		}
		
		return conflicts;
	}
	
	private void updateHistory(MetaModel src, MetaModel tar){
		if(src != null && tar != null && changeTrace != null){
			MetaModelChangeChecker changeChecker = new MetaModelChangeChecker(src, tar);			
			for(metabup.metamodel.AnnotatedElement ae : changeChecker.getNew()) changeTrace.add(ae);
			for(metabup.metamodel.MetaClass[] inhRel : changeChecker.getNewInheritances()) changeTrace.addInheritance(inhRel);
			for(metabup.metamodel.AnnotatedElement ae : changeChecker.getDeleted()) changeTrace.delete(ae);
			for(metabup.metamodel.MetaClass[] inhRel : changeChecker.getDeletedInheritances()) changeTrace.deleteInheritance(inhRel);
			for(metabup.metamodel.AnnotatedElement ae : changeChecker.getUpdated(false)) changeTrace.update(ae);
			
			// renamed reference issue, if necessary
			/*for(metabup.metaModel.AnnotatedElement ae : changeChecker.getRenamed()){
				if(ae instanceof metabup.metaModel.Reference){
					Reference r = (Reference)ae;
					String oldName;
					if(src.getElementById(r.getId()) != null) oldName = ((Reference)src.getElementById(r.getId())).getName();
					else continue;
					RenamedReference rrIssue = new RenamedReference(r, oldName);
					issues.add(rrIssue);					
				}
			}*/
		}
	}
	
	protected void ruleFragmentSet2MetaModel() {
		FragmentModel model = inModel.rootFragmentModel();
		if(model != null){
			// create metamodel if it does not exists yet
			this.targetMetaModel = outModel.rootMetamodel();
			if ( targetMetaModel == null ) {
				this.targetMetaModel = outModel.createMetaModel();
				this.targetMetaModel.setName( model.getName() );
			}
			
			//this.rm = new RefactorMetaModel(this.outModel, this.targetMetaModel, this.issues, this.af);		
			this.rm = new RefactorMetaModel(this.outModel, this.targetMetaModel, this.af);
			this.setTrace(model, this.targetMetaModel);
		}
	}

	// Helper
	protected MetaClass existsMetaClassWithName(String n, Collection<MetaClass> existings) {
		for (MetaClass mc : existings) {
			if (mc.getName().equals(n)) return mc;
		}
		return null;
	}
	
	public List<MetaClass> getCommonSupers(List<MetaClass> mcs){
		if(mcs == null || mcs.isEmpty()) return null;		
		List<MetaClass> supers = new ArrayList<MetaClass>();
		supers.addAll(mcs.get(0).getSupers());
		
		for(MetaClass mc : mcs)
			for(MetaClass superMc : mc.getSupers())
				if(!supers.contains(superMc)) 
					supers.remove(superMc);
		
		if(supers.isEmpty()) return null;
		else return supers;
	}
	
	protected void ruleObject2Class() {
		// var meta : MM!MetaModel := MM!MetaModel.all().first();
		MetaModel meta = this.targetMetaModel;
		
		Collection<MetaClass> existing = meta.getClasses();
		
		for (fragments.Object o : inModel.allFragmentObjects() ) {
			MetaClass mm = null; 

			// guard : not MM!MetaClass.all().exists( c | c.name = o.type )
			if ((mm=this.existsMetaClassWithName(o.getType(), existing))==null ) {
				mm = outModel.createMetaclass();
				mm.setName( o.getType() );
				mm.setIsAbstract( false );
				meta.getClasses().add(mm);
				
				if(mm.getName().endsWith("Type")){
					//System.out.println("got the type");
					UnsuffixMetaClass umc = new UnsuffixMetaClass(mm);
					this.issues.add(umc);
				}
			}						
			
			this.setTrace(o, mm);	
			mm.getFeatures().addAll( (Collection<? extends Feature>) transform2(o.getFeatures(), mm) );
			removeInheritedFeatures(mm, mm.getSupers());
			//for (metabup.fragments.Annotation an : o.getAnnotation()) mm.getAnnotations().add(this.transformAnnotations(an));					
		}
	}
	
	private void removeInheritedFeatures(MetaClass mc, List<MetaClass> supers){		
		if(supers == null || supers.isEmpty() || supers.contains(mc)) return;
		List<Feature> removeff = new ArrayList<Feature>();
		
		for(MetaClass superMC : supers){
			for(Feature f: mc.getFeatures()){								
				for(Feature superf : superMC.getFeatures()){
					if(f.getName().equals(superf.getName())){
						if(f instanceof Attribute && superf instanceof Attribute){
							Attribute a = (Attribute)f;
							Attribute supera = (Attribute)superf;							
							if(a.getPrimitiveType().equals(supera.getPrimitiveType())) removeff.add(f);
						}else{
							if(f instanceof Reference && superf instanceof Reference){
								Reference r = (Reference)f;
								Reference superr = (Reference)superf;							
								if(r.getReference().getId().equals(superr.getReference().getId())) removeff.add(f);
							}
						}
					}
				}
			}
			
			removeInheritedFeatures(mc, superMC.getSupers());
		}
		
		mc.getFeatures().removeAll(removeff);
	}
		
	public metabup.metamodel.Annotation transformAnnotations(fragments.Annotation an) {
		metabup.metamodel.Annotation mmAnn = outModel.createAnnotation();
		mmAnn.setName(an.getName());
		
		// Now copy the parameters...
		for (fragments.AnnotationParam ap : an.getParams()) {
			metabup.metamodel.AnnotationParam param = outModel.createAnnotationParam();
			param.setName(ap.getName());
			
			if(ap.getParamValue() instanceof PrimitiveValueAnnotationParam){
				PrimitiveValueAnnotationParam pvap = (PrimitiveValueAnnotationParam)ap.getParamValue();
				PrimitiveValue pv = pvap.getValue();
				
				if(pv instanceof StringValue){
					metabup.metamodel.StringValue sv = MetamodelFactory.eINSTANCE.createStringValue();
					sv.setValue(((StringValue) pv).getValue());
					param.setValue(sv);
				}
				
				if(pv instanceof IntegerValue){
					//TO-DO
				}
				
				if(pv instanceof BooleanValue){
					//TO-DO
				}
			}

			if(ap.getParamValue() instanceof ObjectValueAnnotationParam){
				metabup.metamodel.ElementValue ev = MetamodelFactory.eINSTANCE.createElementValue();
				String mcname = ((ObjectValueAnnotationParam) ap.getParamValue()).getObject().getType();
				MetaClass mc = targetMetaModel.getClassByName(mcname);
				
				if(mc == null){
					mc = outModel.createMetaclass();
					mc.setName( mcname );
					mc.setIsAbstract( false );
					targetMetaModel.getClasses().add(mc);
				}
				
				// the param value is an object
				if(((ObjectValueAnnotationParam) ap.getParamValue()).getFeature() == null) ev.setValue(mc);
				else{
					// the param is a feature
					String fname = ((ObjectValueAnnotationParam) ap.getParamValue()).getFeature().getName();
					System.out.println("setting feature... " + mc.getFeatureByName(fname, true));
					ev.setValue(mc.getFeatureByName(fname, true));
				}
				
				param.setValue(ev);
			}
			
			mmAnn.getParams().add(param);
			this.setTrace(ap, param);
		}
		
		this.setTrace(an, mmAnn);
		return mmAnn;
	}
	
	public List<metabup.metamodel.Annotation> transformAnnotationsOf (fragments.TypedElement te) {
		List<metabup.metamodel.Annotation> res = new ArrayList<metabup.metamodel.Annotation>();
		for (fragments.Annotation an : te.getAnnotation()) {	//
			res.add(this.transformAnnotations(an));			
		}		
		return res;
	}
	
	public void triggerRefactorings(boolean active) {	
		List<fragments.Annotation> annList = (List<fragments.Annotation>)this.inModel.allObjectsOf(FragmentsPackage.eINSTANCE.getAnnotation());
			
		for (fragments.Annotation a : annList) {				
			metabup.metamodel.Annotation tar = (metabup.metamodel.Annotation)this.getTraceOf(a);			
				
			if (tar != null){								
				if(!this.rm.triggerRefactorByAnnotation(tar, active)) System.out.println("Problems found when applying " + tar.getName() + " annotation");									
			}else System.out.println("tar is null");			
		}	
		
		this.rm.clearNonPersistentAnnotations();
	}		
	
	private int getFeatureMinCardinality(fragments.Feature f, final MetaClass mm, metabup.metamodel.Feature mf, boolean exists){
		//int fMin = FragmentUtil.featureMinCardinality(f);
		int fMin = 0;
		int mmMin = 0;
		
		if ( !exists ) mmMin = fMin;
		else{
			mmMin = mf.getMin();
			if(fMin == 1 && mmMin > 1) mmMin = 1;
			else if(mmMin > 1 && fMin>1 && mmMin>fMin) mmMin = fMin;
		}
		
		return mmMin;
	}
	
	private int getFeatureMaxCardinality(fragments.Feature f, final MetaClass mm, metabup.metamodel.Feature mf, boolean exists){
		int fMax = FragmentUtil.featureMaxCardinality(f);
		int mmMax = 1;
		
		if( !exists ){
			mmMax = -1;						
		}else{
			mmMax = mf.getMax();
			if(fMax > mmMax && mmMax >= 1) mmMax = fMax;
			
			//if(LexicalInflector.isPlural(f.getName())){
			if(mmMax > 1 || (LexicalInflector.isPlural(f.getName()) && mmMax > 0)){
				UpgradeMaxMultiplicity umm = new UpgradeMaxMultiplicity(mf);
				umm.setDescription("The max multiplicity of \'" + ((metabup.metamodel.MetaClass)mf.eContainer()).getName()+"." + mf.getName() + "\' was inferred \'" + mmMax + "\'. Click to upgrade it to \'*\'");
				
				boolean repeated = false;
				
				for(IOpenIssue oi : this.issues)
					if(oi.getDescription().equals(umm.getDescription())) repeated = true;
				
				if(!repeated) this.issues.add(umm);
			}
		}
		
		return mmMax;
	}
	
	public metabup.metamodel.Attribute transformAttribute(fragments.Attribute ff, MetaClass mm) {
		metabup.metamodel.Attribute mf = null;
		List<metabup.metamodel.Attribute> existingAttributes = select(mm.getAttributes(true), metabup.metamodel.Attribute.class);
		
		for (metabup.metamodel.Attribute existing : existingAttributes) {
			if ( existing.getName().equals(ff.getName() ) ) {
				mf = existing;
				DType type = attributeInduceType(ff);
				
				//TODO : Introduce new conflict and RETURN NULL
				/*if ( type != null && ! type.getLiteral().equals( existing.getPrimitiveType() ) ) {
					AttributeWithIncompatibleType c = issues.createAttributeWithIncompatibleType();
					c.setAttribute(existing);
					c.setAlternativeType(type.getLiteral());
					issues.rootIssueModel().getOpenIssues().add(c);
				}	*/
				
				break;
			}				
		}
		
		int mmMin = this.getFeatureMinCardinality(ff, mm, mf, !(mf == null));
		int mmMax = this.getFeatureMaxCardinality(ff, mm, mf, !(mf == null));
		
		if( mf == null ){
			mf = outModel.createAttribute();
			mf.setName( ff.getName() );
			mf.setPrimitiveType( attributeInduceType(ff).getLiteral() );
		}else{
			mf.setMin(mmMin);
			mf.setMax(mmMax);			
			mf.getAnnotations().addAll(this.transformAnnotationsOf(ff));
			return null;
		}
		
		mf.setMin(mmMin);
		mf.setMax(mmMax);
		
		mf.getAnnotations().addAll(this.transformAnnotationsOf(ff));				
		this.setTrace(ff, mf);
		
		return mf;
	}
	
	public metabup.metamodel.Reference transformReference(final fragments.Reference fr, final MetaClass mmc) {
		metabup.metamodel.Reference mr = null;
		MetaClass mm = mmc;
		
		List<metabup.metamodel.Reference> existingReferences = select(mm.getFeatures(), metabup.metamodel.Reference.class);
				
		for (metabup.metamodel.Reference existing : existingReferences) {
			if ( existing.getName().equals(fr.getName() ) ) {
				mr = existing;
				break;
			}
		}	
		
		int mmMin = this.getFeatureMinCardinality(fr, mm, mr, !(mr == null));
		int mmMax = this.getFeatureMaxCardinality(fr, mm, mr, !(mr == null));
		
		if ( mr == null ) {
			/* if the reference doesn't yet exist */
			/* [in active mode] IN CASE ANOTHER REFERENCE TARGETING THE SAME CLASS OR ONE OF ITS SUBS, we should
				answer whether the user would like to reuse an existing one */
			
			List<Reference> matchingRefs = fr.getAnalogousReferences(mm);
			fragments.Object contObj = ((fragments.Object)fr.eContainer());
			
			if(matchingRefs.size() > 0 && activeMode){
				ListSelectionDialog dialog = new ListSelectionDialog( null, 
						matchingRefs, new ArrayContentProvider(),
						new LabelProvider(), "The following are matching references with \'" + contObj.getName() + "." + fr.getName() + "\'");
				
				// dlg.setInitialSelections(dirtyEditors);
				dialog.setTitle("New reference addition");
				
				if(dialog.open() == Window.OK){
					Object[] result = dialog.getResult();
					
					if (result.length == 1) {
						mr = (Reference)result[0];
					}
				}else{
					mr = outModel.createReference();
					mr.setName( fr.getName() );
				}
			}else{
				mr = outModel.createReference();
				mr.setName( fr.getName() );
			}
		}
								
		try{
			mr.setMin(mmMin);
			mr.setMax(mmMax);
		}catch(NullPointerException e){
			mr.setMin(0);
			mr.setMax(-1);
		}
		
		/*if(LexicalInflector.isPlural(fr.getName())){		
			if(mmMax > -1){
				UpgradeMaxMultiplicity umm = new UpgradeMaxMultiplicity(mr);
				umm.setDescription("The max multiplicity of \'" + mr.getName() + "\' was inferred \'" + mmMax + "\'. Click to upgrade it to \'*\'");
				this.issues.add(umm);
			}
		}*/
		
		for(Annotation ann : InduceMetaModel.this.transformAnnotationsOf(fr)){
			if(!mr.isAnnotated(ann.getName())) mr.getAnnotations().add(ann);
		}		
		
		if(fr.getReference().size() > 1){			
			List<String> types = new ArrayList<String>();
			
			for(fragments.Object o : fr.getReference())
				if(!types.contains(o.getType())) types.add(o.getType());
			
			if(mr.getReference() != null) if(!types.contains(mr.getReference().getName())) types.add(mr.getReference().getName());
			
			if(types.size() > 1){			
				metabup.metamodel.MetaClass newabsmc = null;		
				String name = "General";
				for(String n : types) name += n;
				
				List<MetaClass> mcs = new ArrayList<MetaClass>();				
				
				for(String mcname : types){
					MetaClass mc = this.existsMetaClassWithName(mcname, targetMetaModel.getClasses());
					if(mc != null) mcs.add(mc);
				}
				
				if(types.size() == mcs.size()){
					if(this.getCommonSupers(mcs) != null && !this.getCommonSupers(mcs).isEmpty()) newabsmc = this.getCommonSupers(mcs).get(0);
				}else newabsmc = this.existsMetaClassWithName(name, targetMetaModel.getClasses());
																														
				if(newabsmc == null){							
					//System.out.println("Creating new metaclass");
					newabsmc = outModel.createMetaclass();				
					newabsmc.setName(name);								
					newabsmc.setIsAbstract( true );
					targetMetaModel.getClasses().add(newabsmc);	
					
					RenameMetaClass rmc = new RenameMetaClass(newabsmc);
					rmc.setDescription("The MetaClass name " + name + " was automatically inferred. Please consider changing it for a more descriptive one");
					this.issues.add(rmc);
					System.out.println("added issue: " + this.issues.size());
				}
				
				mr.setReference(newabsmc);
				
				List<String> subs = new ArrayList<String>();
				
				for(String type : types){
					MetaClass mc = this.existsMetaClassWithName(type, targetMetaModel.getClasses());
					
					if(mc != null){
						mc.getSupers().add(newabsmc);
						newabsmc.getSubclasses().add(mc);
					}else subs.add(type);
				}
				
				if(!subs.isEmpty()) this.pendingSubs.put(newabsmc, subs);	
			}else{				
				MetaClass referenceType = this.existsMetaClassWithName(fr.getReference().get(0).getType(), targetMetaModel.getClasses());			
				if(referenceType != null && mr.getReference() != null ) {if(referenceType.isSubTo(mr.getReference())) mr.setReference(mr.getReference());}
				else{
					if(referenceType != null) mr.setReference(referenceType);
					else this.pendingRefs.put(mr, fr.getReference().get(0).getType());		
				}
			}
		}else{
			MetaClass referenceType = this.existsMetaClassWithName(fr.getReference().get(0).getType(), targetMetaModel.getClasses());			
			if(referenceType != null && mr.getReference() != null ) {if(referenceType.isSubTo(mr.getReference())) mr.setReference(mr.getReference());}
			else{
				if(referenceType != null) mr.setReference(referenceType);
				else this.pendingRefs.put(mr, fr.getReference().get(0).getType());		
			}
		}		
		
		//System.out.println("... and of course I get here");
		
		if(fr.getGraphicProperties() != null){
			mr.setGraphicProperties(EcoreUtil.copy(fr.getGraphicProperties()));
			//System.out.println("SEEE THE COLORS IN THE SKY: " + fr.getGraphicProperties().getColor());
		}
		
		this.setTrace(fr, mr);		
		
		return mr;
	}
	
	private void resumePending(){
		/* pending inheritance relationships */
		@SuppressWarnings("rawtypes")
		Iterator it = this.pendingSubs.entrySet().iterator();
		
		while(it.hasNext()){
			@SuppressWarnings("rawtypes")
			Map.Entry e = (Map.Entry)it.next();
			MetaClass supermc = (MetaClass)e.getKey();
			
			@SuppressWarnings("unchecked")
			List<String> subs = (List<String>)e.getValue();
			
			for(String subName : subs){			
				MetaClass submc = this.existsMetaClassWithName(subName, targetMetaModel.getClasses());				
				
				if(submc != null){
					////System.out.println("Setting inheritance relationship for " + submc.getName() + " (sub) and " + supermc.getName() + " (super)");
					if(!supermc.getSubclasses().contains(submc)) supermc.getSubclasses().add(submc);
					if(!submc.getSupers().contains(supermc)) submc.getSupers().add(supermc);
				}
			}
		}
		
		/* pending references */
		it = this.pendingRefs.entrySet().iterator();
		
		while(it.hasNext()){
			Map.Entry e = (Map.Entry)it.next();
			metabup.metamodel.Reference ref = (metabup.metamodel.Reference)e.getKey();
			MetaClass tarmc = this.existsMetaClassWithName((String)e.getValue(), targetMetaModel.getClasses());			
			if(tarmc != null) ref.setReference(tarmc);				
		}
	}	
	
	protected DType attributeInduceType(fragments.Attribute ff) {
		for(PrimitiveValue t : ff.getValues()) {
			if ( t instanceof StringValue ) {
				return DType.STRING_TYPE;
			} else if ( t instanceof IntegerValue ) {
				return DType.INT_TYPE;
			} else if ( t instanceof BooleanValue ) {
				return DType.BOOLEAN_TYPE;
			}
		}
		return null;
	}

	/*protected void updateReferenceType(final metabup.metaModel.Reference reference, MetaClass data, MetaClass currentType) {
		if ( currentType != data ) {				
			// TODO: Get all superclasses!
			boolean existsSuperclass = data.getSupers().contains(currentType);
			// TODO: Check whether the existing superclass has a compatible slots
			/*if ( ! existsSuperclass ) {
				MetaClass superclazz = outModel.createMetaclass();
				String newName = currentType.getName() + "_" + data.getName();
				superclazz.setName(newName);
				currentType.getSupers().add(superclazz);
				data.getSupers().add(superclazz);

				reference.setReference(superclazz);
				outModel.rootMetamodel().getClasses().add(superclazz);
				
				// Create issue				
				IntroduceSuperclass issue = issues.createIntroduceSuperClass();
				issue.setInferredName(newName);
				issue.setSuperclass(superclazz);
				issues.rootIssueModel().getOpenIssues().add(issue);
			}
		}
	}	*/		

}
