/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.preferences;

import org.eclipse.jface.preference.*;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.IWorkbench;

import metabest.preferences.Activator;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class MetaBestPreferencePage
	extends FieldEditorPreferencePage
	implements IWorkbenchPreferencePage {

	public MetaBestPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("MetaBest general Preferences");
	}
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {

		addField(
				new BooleanFieldEditor(
				MetaBestPreferenceConstants.FAST_PERMUTATION_CHECK,
				"Optimize results calculation when launching permutted assertions (unchecking it might affect execution time)",
				getFieldEditorParent()));
		
		addField(
				new StringFieldEditor(
				MetaBestPreferenceConstants.MISSING_ASSERTION_LITERAL,
				"Missing element literal for sketches",
				getFieldEditorParent()));
		
		addField(
				new StringFieldEditor(
				MetaBestPreferenceConstants.UNKNOWN_ASSERTION_LITERAL,
				"Unknown element literal for sketches",
				getFieldEditorParent()));
		}
	

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}