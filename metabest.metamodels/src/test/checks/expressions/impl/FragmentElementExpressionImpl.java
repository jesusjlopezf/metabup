/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.checks.expressions.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import test.checks.expressions.ExpressionsPackage;
import test.checks.expressions.FragmentElementExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fragment Element Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class FragmentElementExpressionImpl extends MinimalEObjectImpl.Container implements FragmentElementExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FragmentElementExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.FRAGMENT_ELEMENT_EXPRESSION;
	}

} //FragmentElementExpressionImpl
