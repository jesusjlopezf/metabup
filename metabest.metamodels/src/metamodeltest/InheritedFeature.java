/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package metamodeltest;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Inherited Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link metamodeltest.InheritedFeature#getClassSel <em>Class Sel</em>}</li>
 * </ul>
 * </p>
 *
 * @see metamodeltest.MetamodeltestPackage#getInheritedFeature()
 * @model
 * @generated
 */
public interface InheritedFeature extends FeatureQualifier {
	/**
	 * Returns the value of the '<em><b>Class Sel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Sel</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Sel</em>' containment reference.
	 * @see #setClassSel(ClassSelector)
	 * @see metamodeltest.MetamodeltestPackage#getInheritedFeature_ClassSel()
	 * @model containment="true"
	 * @generated
	 */
	ClassSelector getClassSel();

	/**
	 * Sets the value of the '{@link metamodeltest.InheritedFeature#getClassSel <em>Class Sel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Sel</em>' containment reference.
	 * @see #getClassSel()
	 * @generated
	 */
	void setClassSel(ClassSelector value);

} // InheritedFeature
