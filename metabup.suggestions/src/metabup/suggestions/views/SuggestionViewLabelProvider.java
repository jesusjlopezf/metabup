package metabup.suggestions.views;

import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import metabup.suggestions.Activator;
import metabup.suggestions.views.model.FolderTreeParent;
import metabup.suggestions.views.model.IssueTreeObject;
import metabup.suggestions.views.model.TipTreeObject;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledString;

public class SuggestionViewLabelProvider extends LabelProvider implements IStyledLabelProvider{
	@Override
	public Image getImage(Object obj) {
		if(obj instanceof IStructuredSelection) obj = ((IStructuredSelection) obj).getFirstElement();
		
		String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
		if(obj instanceof FolderTreeParent) return Activator.getImageDescriptor("icons/folder.png").createImage();
		if(obj instanceof IssueTreeObject) return Activator.getImageDescriptor("icons/issue.png").createImage();
		if(obj instanceof TipTreeObject) return Activator.getImageDescriptor("icons/tip.png").createImage();
		
		return PlatformUI.getWorkbench().getSharedImages().getImage(imageKey);
	}
	
	@Override
	public StyledString getStyledText(Object element) {
		if(element instanceof IStructuredSelection) element = ((IStructuredSelection) element).getFirstElement();
		
		if (element instanceof FolderTreeParent) {
			FolderTreeParent folderParent = (FolderTreeParent) element;
			StyledString styledString = new StyledString(folderParent.getName());
			
			if (folderParent.getChildren() != null) {
				styledString.append(" (" + folderParent.getChildren().length + ") ", StyledString.COUNTER_STYLER);
			}
			return styledString;
		}
		
		return null;
	}

}
