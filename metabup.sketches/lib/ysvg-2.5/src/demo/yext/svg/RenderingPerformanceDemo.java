/****************************************************************************
 * This demo file is part of the yFiles extension package ySVG-2.5.
 * Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 * 72070 Tuebingen, Germany. All rights reserved.
 * 
 * ySVG demo files exhibit ySVG functionalities. Any redistribution
 * of demo files in source code or binary form, with or without
 * modification, is not permitted.
 * 
 * Owners of a valid software license for a ySVG version that this
 * demo is shipped with are allowed to use the demo source code as basis
 * for their own ySVG powered applications. Use of such programs is
 * governed by the rights and conditions as set out in the ySVG
 * license agreement.
 * 
 * THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 * NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ***************************************************************************/
package demo.yext.svg;

import yext.svg.io.SVGNodeRealizerSerializer;
import yext.svg.view.SVGModel;
import yext.svg.view.SVGNodeRealizer;

import y.io.GraphMLIOHandler;
import y.option.ComponentOptionItem;
import y.option.DefaultEditorFactory;
import y.option.Editor;
import y.option.EditorFactory;
import y.option.ItemEditor;
import y.option.OptionGroup;
import y.option.OptionHandler;
import y.option.OptionItem;
import y.util.D;
import y.view.EditMode;
import y.view.Graph2D;
import y.view.NodeRealizer;
import y.base.Node;
import y.base.NodeCursor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.net.URL;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

/**
 * Demonstrates how caching intermediate images improves performance of the
 * graph viewer component when rendering <em>Scalable Vector Graphics</em>.
 * To experience performance changes, use the mouse to move preselected
 * graph elements around before and after changing cache settings.
 * The following cache settings are available:
 * <ul>
 *   <li><b>Image Cache Enabled</b><br>
 *       allows for activation/deactivation of the caching mechanism</li>
 *   <li><b>Image Cache Size</b><br>
 *       determines the number of images to be cached</li>
 *   <li><b>Maximum Image Size</b><br>
 *       determines the maximum size (in bytes) of images to be cached</li>
 *   <li><b>Minimum Relative Image Size</b><br>
 *       determines the minimum relative size for which precached images
 *       may be reused when rendering duplicates of smaller sizes</li>
 * </ul>
 *
 * @see yext.svg.view.SVGModel#setImageCacheEnabled(boolean)
 * @see yext.svg.view.SVGModel#setImageCacheSize(int)
 * @see yext.svg.view.SVGModel#setMaximumImageSize(int)
 * @see yext.svg.view.SVGModel#setMinimumRelativeImageSize(double)
 */
public class RenderingPerformanceDemo extends SVGExportDemo {
  private static final String GRAPH =
          "Graph";
  private static final String IMAGE_CACHE_ENABLED =
          "Image Cache Enabled";
  private static final String IMAGE_CACHE_SIZE =
          "Image Cache Size";
  private static final String MAXIMUM_IMAGE_SIZE =
          "Maximum Image Size (in bytes)";
  private static final String MINIMUM_RELATIVE_IMAGE_SIZE =
          "Minimum Relative Image Size";

  private Map nameToUrl;

  public RenderingPerformanceDemo() {
    createNameToUrlMap();

    if (nameToUrl != null && !nameToUrl.isEmpty()) {
      readGraph((URL)nameToUrl.values().iterator().next());
    }

    createContent();
    createListeners();
    updateView();
  }

  private void createNameToUrlMap() {
    nameToUrl = new LinkedHashMap();

    final String[] names = {
            "100Earths",
            "mimeTypes",
            "mixedSizes"
    };
    URL url;
    for (int i = 0; i < names.length; ++i) {
      final String resource = "resource/" + names[i] + ".graphml";
      url = RenderingPerformanceDemo.class.getResource(resource);
      if (url != null) {
        nameToUrl.put(names[i], url);
      } else {
        System.err.println("Could not resolve " + resource + "!");
      }
    }
  }

  private void createListeners() {
    final Dimension oldSize = view.getPreferredSize();
    view.addComponentListener(new ComponentAdapter() {
      public void componentResized( final ComponentEvent e ) {
        final Dimension newSize = e.getComponent().getSize();
        if (oldSize.width < newSize.width || oldSize.height < newSize.height) {
          updateView();
          view.removeComponentListener(this);
        }
      }
    });
  }

  /**
   * Creates the GUI components.
   */
  private void createContent() {
    final String[] names;
    if (nameToUrl != null && !nameToUrl.isEmpty()) {
      names = new String[nameToUrl.size()];
      int i = 0;
      for (Iterator it = nameToUrl.keySet().iterator(); it.hasNext(); ++i) {
        names[i] = (String)it.next();
      }
    } else {
      names = new String[0];
    }

    OptionGroup group;

    final OptionHandler oh = new OptionHandler("CacheOptions");
    OptionItem item;

    if (names.length > 0) {
      group = new OptionGroup();
      group.setAttribute(OptionGroup.ATTRIBUTE_TITLE, "Sample SVG Graph");
      item = oh.addEnum(GRAPH, names, 0);
      item.setAttribute(ItemEditor.ATTRIBUTE_AUTO_COMMIT, Boolean.TRUE);
      group.addItem(item);
    } else {
      group = new OptionGroup();
      group.setAttribute(OptionGroup.ATTRIBUTE_TITLE, "Sample SVG Graph");

      final JLabel label = new JLabel("No graphs available.");
      label.setForeground(Color.RED.darker());
      item = new ComponentOptionItem("_COMPONENT", label) {
        public boolean wantsVisibleName() {
          return false;
        }
      };
      oh.addItem(item);
      group.addItem(item);
    }

    group = new OptionGroup();
    group.setAttribute(OptionGroup.ATTRIBUTE_TITLE, "SVG Image Cache Options");

    item = oh.addBool(IMAGE_CACHE_ENABLED, SVGModel.isImageCacheEnabled());
    item.setTipText(
            "<html><body><table width=\"240\"><tr><td>" +
            "Enables or disables image caching. " +
            "Image caching is a performance optimization that tries to " +
            "minimize expensive vector graphics rendering to a cached " +
            "intermediate image that will be displayed instead. To achieve " +
            "nice rendering quality images are created and cached for " +
            "multiple zoom levels." +
            "</td></tr></table></body></html>"
    );
    item.setAttribute(ItemEditor.ATTRIBUTE_AUTO_COMMIT, Boolean.TRUE);
    group.addItem(item);
    item = oh.addInt(IMAGE_CACHE_SIZE, SVGModel.getImageCacheSize());
    item.setTipText(
            "<html><body><table width=\"240\"><tr><td>" +
            "The maximum number of images that will be cached." +
            "</td></tr></table></body></html>"
    );
    item.setAttribute(ItemEditor.ATTRIBUTE_AUTO_COMMIT, Boolean.TRUE);
    group.addItem(item);
    item = oh.addInt(MAXIMUM_IMAGE_SIZE, SVGModel.getMaximumImageSize());
    item.setTipText(
            "<html><body><table width=\"240\"><tr><td>" +
            "The maximum size of images that will be cached. " +
            "Image size is measured in bytes necessary to store it. " +
            "For an image with size <code>W x H</code> the image size " +
            "is <code>W * H * 4</code>. " +
            "The width and height of an image is calulated by " +
            "multiplying the width and height of the SVG by the scale " +
            "factor of the graphics context it is rendered on." +
            "</td></tr></table></body></html>"
    );
    item.setAttribute(ItemEditor.ATTRIBUTE_AUTO_COMMIT, Boolean.TRUE);
    group.addItem(item);
    item = oh.addDouble(MINIMUM_RELATIVE_IMAGE_SIZE, SVGModel.getMinimumRelativeImageSize());
    item.setTipText(
            "<html><body><table width=\"240\"><tr><td>" +
            "The minimum relative image size for which precached image " +
            "duplicates are reused. I.e. if the image cache is queried for " +
            "an intermediate image for a given SVG definition and image " +
            "size, a cache miss is avoided if there is a cached image for " +
            "the given SVG definition and <code>query width/cached width " +
            "&gt;= minimumRelativeImageSize</code> as well as " +
            "<code>query height/cached height &gt;= minimumRelativeImageSize</code> " +
            "hold." +
            "</td></tr></table></body></html>"
    );
    item.setAttribute(ItemEditor.ATTRIBUTE_AUTO_COMMIT, Boolean.TRUE);
    group.addItem(item);


    final JTextArea jta = new JTextArea();
    jta.setText(info());
    jta.setEditable(false);
    jta.setLineWrap(true);
    jta.setWrapStyleWord(true);
    final JScrollPane jsp = new JScrollPane(jta);
    jsp.setPreferredSize(new Dimension(200, 100));
    item = new ComponentOptionItem("Comment", jsp) {
      public boolean wantsVisibleName() {
        return false;
      }
    };
    oh.addItem(item);

    oh.addChildPropertyChangeListener(new PropertyChangeListener() {
      public void propertyChange( PropertyChangeEvent evt ) {
        if (!OptionItem.PROPERTY_VALUE.equals(evt.getPropertyName())) {
          return;
        }

        final OptionItem src = (OptionItem)evt.getSource();
        final String name = src.getName();
        if (GRAPH.equals(name)) {
          SVGModel.clearImageCache();
          readGraph((URL)nameToUrl.get(evt.getNewValue()));
          jta.setText(info());
        } else if (IMAGE_CACHE_ENABLED.equals(name)) {
          SVGModel.setImageCacheEnabled(((Boolean)evt.getNewValue()).booleanValue());
        } else if (IMAGE_CACHE_SIZE.equals(name)) {
          SVGModel.setImageCacheSize(((Number)evt.getNewValue()).intValue());
        } else if (MAXIMUM_IMAGE_SIZE.equals(name)) {
          SVGModel.setMaximumImageSize(((Number)evt.getNewValue()).intValue());
        } else if (MINIMUM_RELATIVE_IMAGE_SIZE.equals(name)) {
          SVGModel.setMinimumRelativeImageSize(((Number)evt.getNewValue()).doubleValue());
          view.repaint();
        }
      }
    });


    final EditorFactory ef = new DefaultEditorFactory();//new TableEditorFactory();
    final Editor editor = ef.createEditor(oh);

    final GridBagConstraints gbc = new GridBagConstraints();
    final JPanel editorPane = new JPanel(new GridBagLayout());
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.weightx = 1.0;
    gbc.weighty = 0.0;
    editorPane.add(editor.getComponent(), gbc);
    gbc.fill = GridBagConstraints.BOTH;
    ++gbc.gridy;
    gbc.weighty = 1.0;
    editorPane.add(new JPanel(), gbc);

    remove(view);
    add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true,
            editorPane, view),
            BorderLayout.CENTER);
  }


  protected JMenuBar createMenuBar() {
    JMenuBar jmb = new JMenuBar();
    JMenu menu = new JMenu("File");
    menu.add(new ExitAction());
    jmb.add(menu);
    return jmb;
  }

  protected EditMode createEditMode() {
    final EditMode em = new EditMode() {
      protected String getNodeTip( final Node v ) {
        final NodeRealizer nr = getGraph2D().getRealizer(v);
        final double zoom = view.getZoom();
        final int bytes = (int)Math.ceil(nr.getWidth() *zoom) *
                          (int)Math.ceil(nr.getHeight()*zoom) * 4;
        return "Corresponding image size: " + bytes + " bytes";
      }
    };
    em.showNodeTips(true);
    em.allowBendCreation(false);
    em.allowEdgeCreation(false);
    em.allowNodeCreation(false);
    return em;
  }

  public void start(final String title) {
    JFrame frame = new JFrame(title);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    addContentTo(frame.getRootPane());
    frame.pack();
    frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }

  private void readGraph( final URL url ) {
    if (url == null) {
      return;
    }

    final Graph2D graph = view.getGraph2D();

    final GraphMLIOHandler ioh = new GraphMLIOHandler();
    ioh.addNodeRealizerSerializer(new SVGNodeRealizerSerializer());
    try {
      graph.clear();
      ioh.read(view.getGraph2D(), url);
    } catch (IOException ioe) {
      D.show(ioe);
    }

    graph.setSelected(graph.nodes(), true);
    graph.setBendsSelected(graph.edges(), true);

    updateView();
  }

  private void updateView() {
    view.fitContent();
    view.setPaintDetailThreshold(0);
    view.updateView();
  }

  private String info() {
    final Graph2D graph = view.getGraph2D();

    final StringBuffer sb = new StringBuffer();
    sb.append(graph.N()).append(" node(s),\n");
    sb.append(graph.E()).append(" edge(s),\n");

    final Set documents = new HashSet();
    for (NodeCursor nc = graph.nodes(); nc.ok(); nc.next()) {
      final NodeRealizer nr = graph.getRealizer(nc.node());
      if (nr instanceof SVGNodeRealizer) {
        try {
          final StringWriter w = new StringWriter();
          ((SVGNodeRealizer)nr).getModel().serializeSVGContent(w);
          documents.add(w.toString());
        } catch (IOException e) {
          // the count displayed below will not be accurate 
        }
      }
    }
    sb.append(documents.size()).append(" SVG image(s)");
    return sb.toString();
  }


  /**
   * Launches this demo.
   * @param args ignored.
   */
  public static void main( final String[] args ) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        initLnF();
        (new RenderingPerformanceDemo()).start();
      }
    });
  }
}
