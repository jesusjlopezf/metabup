/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test;

import fragments.Fragment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Testable Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link test.TestableFragment#getAssertionSet <em>Assertion Set</em>}</li>
 *   <li>{@link test.TestableFragment#getCompletion <em>Completion</em>}</li>
 * </ul>
 *
 * @see test.TestPackage#getTestableFragment()
 * @model
 * @generated
 */
public interface TestableFragment extends Fragment {
	/**
	 * Returns the value of the '<em><b>Assertion Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assertion Set</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assertion Set</em>' containment reference.
	 * @see #setAssertionSet(ComplementaryBlock)
	 * @see test.TestPackage#getTestableFragment_AssertionSet()
	 * @model containment="true"
	 * @generated
	 */
	ComplementaryBlock getAssertionSet();

	/**
	 * Sets the value of the '{@link test.TestableFragment#getAssertionSet <em>Assertion Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assertion Set</em>' containment reference.
	 * @see #getAssertionSet()
	 * @generated
	 */
	void setAssertionSet(ComplementaryBlock value);

	/**
	 * Returns the value of the '<em><b>Completion</b></em>' attribute.
	 * The literals are from the enumeration {@link test.CompletionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Completion</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Completion</em>' attribute.
	 * @see test.CompletionType
	 * @see #setCompletion(CompletionType)
	 * @see test.TestPackage#getTestableFragment_Completion()
	 * @model required="true"
	 * @generated
	 */
	CompletionType getCompletion();

	/**
	 * Sets the value of the '{@link test.TestableFragment#getCompletion <em>Completion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Completion</em>' attribute.
	 * @see test.CompletionType
	 * @see #getCompletion()
	 * @generated
	 */
	void setCompletion(CompletionType value);
	public String toString();

} // TestableFragment
