/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package fragments;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fragments.FragmentsPackage#getFeature()
 * @model abstract="true"
 * @generated
 */
public interface Feature extends TypedElement {
} // Feature
