/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.sketches;

import org.eclipse.emf.common.util.EList;

import metabup.sketches.Container;
import metabup.sketches.Line;
import metabup.sketches.SingleElement;
import metabup.sketches.Sketch;
import metabup.sketches.SketchElement;
import metabup.sketches.legend.Legend;
import metabup.sketches.legend.LegendElement;
import metabup.sketches.legend.LegendFactory;

public class Sketch2Legend {
	
	private Legend legend;
	private Sketch sketch;

	public Sketch2Legend(Sketch sketch) {
		this.sketch = sketch;				
	}
	
	public Legend execute() {
		legend = LegendFactory.eINSTANCE.createLegend();
		// metabup.sketches.legend.setName(sketch.getName());

		EList<SketchElement> elements = sketch.getElements();
		for (SketchElement sketchElement : elements) {
			if ( sketchElement instanceof SingleElement || sketchElement instanceof Container ) {
				
				if ( sketchElement.getSrcOf().isEmpty() ) {
					continue;
				}
				
				String text = sketchElement.getSrcOf().get(0).getTgt().getText();
				if ( text == null ) {
					throw new IllegalArgumentException("Text required in symbol " + sketchElement.getToolSymbolName() );
				}
				
				LegendElement element = LegendFactory.eINSTANCE.createLegendElement();
				element.setOriginalSymbolName( sketchElement.getToolSymbolName() );
				element.setDefaultHeight(sketchElement.getHeight());
				element.setDefaultWidth(sketchElement.getWidth());
				element.setColor(sketchElement.getColor());
				element.setTransparent(sketchElement.isTransparent());
				element.setNewSymbolName(text);
				
				legend.getElements().add(element);
			} else if ( sketchElement instanceof Line ){
				// That's fine, skip
			} else { 
				//System.out.println("Warning: Element " + sketchElement + " not supported in metabup.sketches.legend");
			}
		}
			
		return legend;
	}
	
	/*
	private String inferPossibleName(SketchElement sketchElement) {
		if ( sketchElement.getText() != null ) {
			return sketchElement.getText().replaceAll(" ", "_").replaceAll("\n", "_");
		} else {
			String type = sketchElement.getSkechTypeInfo();
			if ( ! uniqueIds.containsKey(type) ) {
				uniqueIds.put(type, 0);
			}
			
			Integer id = uniqueIds.get(type) + 1; 			
			uniqueIds.put(type, id);
			return type + "_" + id.toString();
		}
	}
	*/
}
