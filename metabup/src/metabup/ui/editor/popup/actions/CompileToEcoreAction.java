/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.ui.editor.popup.actions;

import java.io.IOException;

import metabup.SessionState;
import metabup.annotations.general.AnnotationFactory;
import metabup.compilers.xtext.XTextCompiler;
import metabup.refactorings.IntroduceRootClassRefactoring;
import metabup.refactorings.IntroduceXTextImportRefactoring;
import metabup.transformations.models.WEcoreModel;
import metabup.transformations.models.WMetamodelModel;

import org.eclipse.jface.dialogs.MessageDialog;

public class CompileToEcoreAction extends MetaBupEditorAction {
	public static final String ID = "ECore";

	public void run() {
		SessionState session = editor.getSession();
		
		session.deriveCompiledMetamodel(ID);
		WMetamodelModel copy = session.getDerivedMetamodel(ID);
		// Somehow, I should encapsulate the two steps in a single compiler, but
		// still allowing manual refactorings on the xtext-neutral-based representation
		// that has to be propagated to the ecore metamodel. Maybe it it not worth the effort, and 
		// generating an ecore file that can be manually modified is enough.
		IntroduceRootClassRefactoring refactoring = new IntroduceRootClassRefactoring(copy);
		if ( ! refactoring.execute() ) {
			MessageDialog.openError(null, "Refactoring", "Cannot apply introduce root class refactoring");			
		} else {	
			IntroduceXTextImportRefactoring refactoring2 = new IntroduceXTextImportRefactoring(copy, refactoring.getRootMetaclass());
			refactoring2.execute();

			editor.addCompiledMetamodel(ID);
		}
		
		WEcoreModel model = session.createEcoreMetamodel();
		AnnotationFactory af = new AnnotationFactory();
		af.initializeFactory();
		XTextCompiler compiler = new XTextCompiler(copy, model, af);
		compiler.execute();
		try {
			model.serialize();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
