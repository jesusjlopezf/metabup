/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.lexicon.de.colibri.io.lattice;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import metabup.lexicon.de.colibri.lib.Edge;
import metabup.lexicon.de.colibri.lib.Lattice;
import metabup.lexicon.de.colibri.lib.Traversal;


/**
 * Class for exporting the edges of a concept lattice to an .xml file.
 * @author Daniel N. Goetzmann
 * @version 1.0
 */
public class LatticeWriterXmlEdges {
	public void write (Lattice lattice, File file) throws IOException {
		write(lattice, file, Traversal.TOP_ATTR);
	}
	
	
	public void write (Lattice lattice, File file, Traversal traversal) throws IOException {
		FileWriter writer = new FileWriter(file);
		
		try {
			Iterator<Edge> edgeIterator = lattice.edgeIterator(traversal);
			
			writer.write("<?xml version=\"1.0\" ?>\n<lattice type=\"edges_only\">\n");
			
			while(edgeIterator.hasNext()) {
				Edge edge = edgeIterator.next();
				
				writer.write("<edge>\n<upper_concept>\n");
				
				Iterator<Comparable> it;
				it = edge.getUpper().getObjects().iterator();
				while (it.hasNext()) {
					writer.write("<object name=\"" + it.next() + "\"></object>\n");
				}
				
				it = edge.getUpper().getAttributes().iterator();
				while (it.hasNext()) {
					writer.write("<attribute name=\"" + it.next() + "\"></attribute>\n");
				}
				
				writer.write("</upper_concept>\n<lower_concept>\n");
				
				it = edge.getLower().getObjects().iterator();
				while (it.hasNext()) {
					writer.write("<object name=\"" + it.next() + "\"></object>\n");
				}
				
				it = edge.getLower().getAttributes().iterator();
				while (it.hasNext()) {
					writer.write("<attribute name=\"" + it.next() + "\"></attribute>\n");
				}
				
				writer.write("</lower_concept>\n</edge>\n");
			}
			
			writer.write("</lattice>");
		} finally {
			writer.close();			
		}
	}
}
