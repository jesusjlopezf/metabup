/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabest.solver;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.preference.IPreferenceStore;

import metabest.solver.ui.preferences.PreferenceConstants;
import metabest.test.fragments.MBFStandaloneSetupGenerated;
import metabest.transformations.Fragment2GraphML;
import metabest.transformations.MetaModel2Use;
import metabest.validation.ValidateFragment;
import metabup.annotations.catalog.design.Composition;
import metabup.annotations.general.AnnotationFactory;
import metabup.metamodel.Attribute;
import metabup.metamodel.ElementValue;
import metabup.metamodel.Feature;
import metabup.metamodel.MetaClass;
import metabup.metamodel.MetaModel;

import org.tzi.use.main.Session;
import org.tzi.use.main.shell.Shell;
import org.tzi.use.main.shell.runtime.IPluginShellCmd;
import org.tzi.use.runtime.impl.PluginRuntime;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.sys.MLink;
import org.tzi.use.uml.sys.MObject;
import org.tzi.use.uml.sys.MSystem;
import org.tzi.use.uml.sys.MSystemState;
import org.tzi.use.kodkod.plugin.KodkodValidateCmd;
import org.xml.sax.SAXException;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;
import com.google.inject.Injector;
import com.jesusjlopezf.utils.eclipse.resources.IFileUtils;

import fragments.Annotation;
import fragments.AnnotationParam;
import fragments.Fragment;
import fragments.FragmentsFactory;
import test.CompletionType;
import test.TestFactory;
import test.TestModel;
import test.TestableFragment;

public class ConstraintSolver {
	int ATTRIBUTE_UPPER_BOUND = 100;
	MetaModel mm;
	Fragment baseFragment;
	IProject project;
	IFolder destinationFolder;
	File useModel;
	
	private int timeOut = 60;
	
	HashMap<String, String> nameSwitcher;
	
	private MSystemState useResult = null;
	
	static private ByteArrayOutputStream errorTrace = new ByteArrayOutputStream();
	
	IPreferenceStore store = Activator.getDefault().getPreferenceStore();
	
	public ConstraintSolver(IProject project, IFolder folder, MetaModel metamodel, Fragment baseFragment) throws IOException{
		this.mm = metamodel;
		this.project = project;
		this.baseFragment = baseFragment;		
		this.useModel = metamodel2Use(mm, baseFragment);	
		this.destinationFolder = folder;
	}
	
	protected ConstraintSolver(){
		
	}
	
	public void setTimeOut(int time){
		this.timeOut = time;
	}
	
	protected File metamodel2Use(MetaModel metamodel, Fragment baseFragment) throws IOException{
		AnnotationFactory af = new AnnotationFactory();
		af.initializeFactory();
		MetaModel2Use transformation = new MetaModel2Use(mm, project, af, null);
		
		File f = transformation.execute(baseFragment, 
										store.getInt(PreferenceConstants.P_MIN_DEFAULT_REFERENCE_CARDINALITY), 
										store.getInt(PreferenceConstants.P_MAX_DEFAULT_REFERENCE_CARDINALITY),
										store.getBoolean(PreferenceConstants.P_ADD_ATTRIBUTES),
										store.getBoolean(PreferenceConstants.P_ADD_ASSOCIATIONS),
										store.getBoolean(PreferenceConstants.P_ADD_CONTAINEES),
										store.getBoolean(PreferenceConstants.P_MAKE_UNREACHABLE));
		
		this.nameSwitcher = transformation.getNameSwitch();
		
		return f;
	}
	
	public IFile solve(boolean removeUseFile, String outputFileName, boolean activeMode){
		useResult = generateUseInstance(removeUseFile, activeMode);
		if (useResult == null || useResult.allObjects().size() < 1) return null;
		
		TestModel fm = TestFactory.eINSTANCE.createTestModel();
		fm.setImportURI("/" + project.getName() + "/" + mm.getName().substring(0, mm.getName().indexOf(".")) + ".mbup");
		TestableFragment f = TestFactory.eINSTANCE.createTestableFragment();
		f.setCompletion(CompletionType.IS_EXAMPLE);
		f.setName("metabup_fragment");				
		
		for (MObject useObject : useResult.allObjects()) {
			if(useObject.cls().name().equals(MetaModel2Use.DUMMY_CLASS_NAME)) continue;
			fragments.Object object = FragmentsFactory.eINSTANCE.createObject();
			object.setName(useObject.name());
			object.setType(useObject.cls().name());
						
			for (MAttribute attribute : useObject.state(useResult).attributeValueMap().keySet()) {
				fragments.Attribute a = FragmentsFactory.eINSTANCE.createAttribute();
				a.setName(attribute.name());				
				String value = trim( useObject.state(useResult).attributeValueMap().get(attribute).toString() );
				
				if (!value.equals("Undefined")) {
					String values[] = {value};
					if (value.startsWith("Set{")) values = value.substring(4,value.length()-1).split(",");
					
					for(String s : values){
						if(s.matches("-?\\d+(\\.\\d+)?")){
							fragments.IntegerValue iv = FragmentsFactory.eINSTANCE.createIntegerValue();
							iv.setValue(Integer.parseInt(s));
							a.getValues().add(iv);
						}else if(s.equals("true") || s.equals("false")){
							fragments.BooleanValue bv = FragmentsFactory.eINSTANCE.createBooleanValue();
							//System.out.println("LOOK HERE: " + useObject.name() + "." + attribute.name() + " = " + s);
							if(s.equals("true")) bv.setValue(true);
							else bv.setValue(false);
							a.getValues().add(bv);
						}else{
							fragments.StringValue sv = FragmentsFactory.eINSTANCE.createStringValue();
							sv.setValue(s);
							a.getValues().add(sv);
						}
					}
				
					object.getFeatures().add(a);
				}				
			}
			
			f.getObjects().add(object);
		}
		
		for (MLink useLink : useResult.allLinks()) {
			String src = useLink.linkedObjects().get(0).name();
			String tgt = useLink.linkedObjects().get(1).name();
			
			String refName = useLink.association().roleNames().get(1);
			//System.out.println("checking: " + refName + " from " + src + " to " + tgt);
			
			for(fragments.Object so : f.getObjects()){
				if(so.getName().equals(src)){
					fragments.Reference r = so.getReferenceByName(refName);
					
					if(r == null){
						r = FragmentsFactory.eINSTANCE.createReference();	 
						r.setName(refName);
					}					
					
					if(useLink.association().aggregationKind() == 2) r.annotate(Composition.NAME);
					
					for(fragments.Object to : f.getObjects()){
						if(to.getName().equals(tgt)){
							r.getReference().add(to);
							so.getFeatures().add(r);
							
							// add opposite reference in case there's one
							if(mm.getClassByName(so.getType()) != null && 
									mm.getClassByName(so.getType()).getFeatureByName(refName, true) != null && 
									mm.getClassByName(so.getType()).getFeatureByName(refName, true).isAnnotated("opposite")){
								
								//System.out.println("found the ref ");
								metabup.metamodel.Reference mmsr = (metabup.metamodel.Reference)mm.getClassByName(so.getType()).getFeatureByName(refName, true);
								
								metabup.metamodel.Annotation oppAnn = mmsr.getAnnotationByName("opposite");
								
								if(oppAnn != null){
									//System.out.println("found the annotation");
									
									if(oppAnn.getParamValueByParamName("opp") instanceof ElementValue){
										ElementValue ev = (ElementValue)oppAnn.getParamValueByParamName("opp");
										
										if(ev.getValue() instanceof metabup.metamodel.Reference){
											metabup.metamodel.Reference mmtr = (metabup.metamodel.Reference) ev.getValue();
											//System.out.println("the opposite of " + mmsr.getName() + " is " + mmtr.getName());
											if(to.getReferenceByName(mmtr.getName()) != null){
												fragments.Reference tr = to.getReferenceByName(mmtr.getName());
												tr.getReference().add(so);
											}else{
												//System.out.println("creating it...");
												fragments.Reference tr = FragmentsFactory.eINSTANCE.createReference();	 
												tr.setName(mmtr.getName());
												tr.getReference().add(so);
												to.getFeatures().add(tr);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		annotateFragment(f, mm);
		fm.getTestableFragments().add(f);
		
		Diagnostic d = Diagnostician.INSTANCE.validate(fm);
		/*System.out.println("Diagnostic = " + d.getMessage() + " " + d.getSource());*/
				
		// generate file
		String fileName;
		if(outputFileName == null) fileName = EcoreUtil.generateUUID();
		else fileName = outputFileName;
		IFile file = destinationFolder.getFile(fileName + ".mbf");
		Injector injector = new MBFStandaloneSetupGenerated().createInjectorAndDoEMFRegistration();
		ResourceSet rs = injector.getInstance(ResourceSet.class);
		Resource r = rs.createResource(URI.createURI(file.getLocationURI().toString()));
		r.getContents().add(fm);
		try {
			r.save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		/*if(removeUseFile){
			try {
				String propFile = mm.getName().substring(0, mm.getName().length()-7) + ".properties";
				String useFile = mm.getName().substring(0, mm.getName().length()-7) + ".use";
				
				if(project.getFile(propFile) != null) project.getFile(propFile).delete(true, null);
				if(project.getFile(useFile) != null) project.getFile(useFile).delete(true, null);
			} catch (CoreException e) {
				// TODO Auto-generated catch block
			}
			
			if(useModel.exists()) useModel.delete();
		}*/
		
		try {
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return file;
	}
	
	private void annotateFragment(TestableFragment fragment, MetaModel metamodel) {		
		for(MetaClass mc : metamodel.getClasses()){
			for(fragments.Object o : fragment.getObjects(mc.getName())){
				for(metabup.metamodel.Annotation mann : mc.getAnnotations()){
					Annotation annotation = FragmentsFactory.eINSTANCE.createAnnotation();
					annotation.setName(mann.getName());	
					o.getAnnotation().add(annotation);
				}
				
				for(Feature mf : mc.getFeatures(true)){
					for(fragments.Feature ff : o.getFeatures()){
						if(mf.getName().equals(ff.getName())){
							for(metabup.metamodel.Annotation mann : mf.getAnnotations()){
								Annotation annotation = FragmentsFactory.eINSTANCE.createAnnotation();
								annotation.setName(mann.getName());	
								ff.getAnnotation().add(annotation);
							}
						}
					}					
				}
			}
		}
	}
	
	private String trim (String phrase) {
		while (phrase.startsWith("'")) phrase = phrase.substring(1);
		while (phrase.endsWith("'"))   phrase = phrase.substring(0, phrase.length()-1);
		return phrase;
	}
	
	private MSystemState generateUseInstance(boolean removeAuxFiles, boolean activeMode){
		if(baseFragment != null){
			ValidateFragment vf = new ValidateFragment(baseFragment);
			
			if(!vf.validate(mm)){
				System.out.println("The base fragment is not valid because:");
				System.out.println(vf.getExplanation());
				return null;
			}
		}
		
		final Session fSession = new Session();
		fSession.reset();
		Shell.createInstance(fSession, PluginRuntime.getInstance());		
		WrapperErrorConsole.start();  // wrapper for standard error console
		Shell.getInstance().processLineSafely("open " + useModel.getAbsolutePath());
		String error = WrapperErrorConsole.read();
		WrapperErrorConsole.finish(); // restore standard error console
		
		Status status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Invalid USE file", null);
		if (!fSession.hasSystem()) ErrorDialog.openError(null, "Invalid USE file", "USE file contains errors: " + error, status);
		
		String root = null;
		try {
			FileReader fr = new FileReader (new File(useModel.getAbsolutePath()));
			BufferedReader br = new BufferedReader(fr);
			root = br.readLine().split(" ")[1];
			br.close();
		} catch (FileNotFoundException e1) {
			status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "File not found", null);
			ErrorDialog.openError(null, "File not found", "USE file not found", status);
		} catch (IOException e) {}
				
		final String properties_file = useModel.getAbsolutePath().substring(0, useModel.getAbsolutePath().length()-4)+".properties";				
		
		try {
			FileWriter fw = new FileWriter(new File(properties_file));
			
			fw.write(MetaModel2Use.DUMMY_CLASS_NAME + "_min = 1\n");
			fw.write(MetaModel2Use.DUMMY_CLASS_NAME + "_max = 1\n");
			
			for(MetaClass mc : mm.getClasses()){
				if(!mc.getIsAbstract()){
					fw.write(nameSwitcher.get(mc.getName()) + "_min = " + store.getInt(PreferenceConstants.P_MIN_OBJECT_NUMBER) + "\n");
					fw.write(nameSwitcher.get(mc.getName()) + "_max = " + ((mc.isAnnotated("unique"))? "1":store.getInt(PreferenceConstants.P_MAX_OBJECT_NUMBER)) + "\n");
				}
				
				for(Attribute a : mc.getAttributes()){
					fw.write(nameSwitcher.get(mc.getName()) + "_" + nameSwitcher.get(a.getName()) + "_min = 0\n");
					fw.write(nameSwitcher.get(mc.getName()) + "_" + nameSwitcher.get(a.getName()) + "_max = " + ATTRIBUTE_UPPER_BOUND + "\n");
				}
			}			
			
			for (MAssociation assoc : fSession.system().model().associations()) {
				//if(store.getBoolean(PreferenceConstants.P_FORZE_ZERO_TO_ONE)) fw.write(assoc.name() + "_min = 1\n");
				fw.write(assoc.roleNames().get(0) + "_" + assoc.roleNames().get(1) + "_min = " + store.getInt(PreferenceConstants.P_MIN_DEFAULT_REFERENCE_CARDINALITY) + "\n");
				fw.write(assoc.roleNames().get(0) + "_" + assoc.roleNames().get(1) + "_max = " + store.getInt(PreferenceConstants.P_MAX_DEFAULT_REFERENCE_CARDINALITY) + "\n");
			}
			
			fw.write(
					"Real_min = -2000\n" +
					"Real_max = 2000\n" +
					"Real_step = 0.5\n" +
					"String_min = 1\n" +
					"String_max = 50\n" +
					"Integer_min = 0\n" +
					"Integer_max = 10\n"
					);
			
			fw.close();
		} catch (IOException e) {
			//
		}
		
		errorTrace.reset();
		
		final KodkodValidateCmd c = new KodkodValidateCmd() {
			@Override
			public void performCommand(IPluginShellCmd pluginCommand) {
				mSystem = fSession.system();
				mModel = mSystem.model();
				String arguments = " "+properties_file;
				handleArguments(arguments);
				System.out.println("current operation: " + mSystem.getCurrentOperation());
			}
		};
		
		
		ExecutorService executor = Executors.newCachedThreadPool();
		Callable<Object> task = new Callable<Object>() {
		   public Object call() {
		      c.performCommand(null);
		      return null;
		   }
		};
		
		Future<Object> future = executor.submit(task);
		try {
		   Object result = future.get(timeOut, TimeUnit.SECONDS); 
		} catch (TimeoutException ex) {
		   // handle the timeout
		} catch (InterruptedException e) {
		   // handle the interrupts
		} catch (ExecutionException e) {
		   // handle other exceptions
		} finally {
		   future.cancel(true); // may or may not desire this
		}
		
		
		//c.performCommand(null);
		
		if (errorTrace.size()>0 && activeMode) {
			status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, 0, "Model generaton returned errors", null);
			ErrorDialog.openError(null, "USE model generation error", errorTrace.toString(), status);
		}
				
		fSession.system().registerPPCHandlerOverride(Shell.getInstance());
		MSystem system = fSession.system();
		
		if(removeAuxFiles){
			File propertyFile = new File(properties_file);
			propertyFile.delete();
			
			File useFile = new File(useModel.getAbsolutePath());
			useFile.delete();
		}
		
		return system.state();
	}
	
	static class WrapperErrorConsole extends PrintStream {

	    private static StringBuilder       buffer;
	    private static PrintStream         standardConsole;
	    private static WrapperErrorConsole wrapperConsole = null;
	    
	    private WrapperErrorConsole(StringBuilder sb, OutputStream os, PrintStream ul) {
	        super(os);
	        buffer          = sb;
	        standardConsole = ul;
	    }
	    
	    // redirect system.err to buffer
	    public static WrapperErrorConsole start() {
	        try {
	            final StringBuilder sb = new StringBuilder();
	            Field f = FilterOutputStream.class.getDeclaredField("out");
	            f.setAccessible(true);
	            OutputStream psout = (OutputStream) f.get(System.err);
	            wrapperConsole = new WrapperErrorConsole(
	            		    sb, 
	            			new FilterOutputStream(psout) {
	            				public void write(int b) throws IOException {
	            					super.write(b);
	            					sb.append((char) b);
	            				}
	            			}, 
	            			System.err);
	            System.setErr(wrapperConsole);
	        } 
	        catch (NoSuchFieldException shouldNotHappen)     {} 
	        catch (IllegalArgumentException shouldNotHappen) {} 
	        catch (IllegalAccessException shouldNotHappen)   {}
	        return null;
	    }
	    
	    // get content of buffer
	    public static String read() {
	    	if (wrapperConsole!=null) {
	    		System.err.flush();
	    		return buffer.toString();
	    	}
	    	else return "";
	    }
	    
	    // restore system.err
	    public static void finish() {
	    	if (standardConsole!=null) {
	    		System.setErr(standardConsole);
	    		standardConsole = null;
	    		wrapperConsole  = null;
	    	}
	    }
	}
}
