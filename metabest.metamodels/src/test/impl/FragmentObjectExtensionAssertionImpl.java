/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 */
package test.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import test.FragmentObjectExtensionAssertion;
import test.TestPackage;
import test.extensions.FragmentObjectQualifier;
import test.extensions.FragmentObjectSelector;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fragment Object Extension Assertion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link test.impl.FragmentObjectExtensionAssertionImpl#getSelector <em>Selector</em>}</li>
 *   <li>{@link test.impl.FragmentObjectExtensionAssertionImpl#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FragmentObjectExtensionAssertionImpl extends ExtensionAssertionImpl implements FragmentObjectExtensionAssertion {
	/**
	 * The cached value of the '{@link #getSelector() <em>Selector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelector()
	 * @generated
	 * @ordered
	 */
	protected FragmentObjectSelector selector;

	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected FragmentObjectQualifier condition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FragmentObjectExtensionAssertionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestPackage.Literals.FRAGMENT_OBJECT_EXTENSION_ASSERTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FragmentObjectSelector getSelector() {
		return selector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelector(FragmentObjectSelector newSelector, NotificationChain msgs) {
		FragmentObjectSelector oldSelector = selector;
		selector = newSelector;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR, oldSelector, newSelector);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelector(FragmentObjectSelector newSelector) {
		if (newSelector != selector) {
			NotificationChain msgs = null;
			if (selector != null)
				msgs = ((InternalEObject)selector).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR, null, msgs);
			if (newSelector != null)
				msgs = ((InternalEObject)newSelector).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR, null, msgs);
			msgs = basicSetSelector(newSelector, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR, newSelector, newSelector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FragmentObjectQualifier getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(FragmentObjectQualifier newCondition, NotificationChain msgs) {
		FragmentObjectQualifier oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION, oldCondition, newCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(FragmentObjectQualifier newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION, newCondition, newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR:
				return basicSetSelector(null, msgs);
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION:
				return basicSetCondition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR:
				return getSelector();
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION:
				return getCondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR:
				setSelector((FragmentObjectSelector)newValue);
				return;
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION:
				setCondition((FragmentObjectQualifier)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR:
				setSelector((FragmentObjectSelector)null);
				return;
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION:
				setCondition((FragmentObjectQualifier)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__SELECTOR:
				return selector != null;
			case TestPackage.FRAGMENT_OBJECT_EXTENSION_ASSERTION__CONDITION:
				return condition != null;
		}
		return super.eIsSet(featureID);
	}

} //FragmentObjectExtensionAssertionImpl
