/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package metabup.transformations.models;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

public abstract class BaseModel {

	protected Resource resource;

	public BaseModel(Resource resource) {
		this.resource = resource;
	}
	
	public Resource getResource() {
		return this.resource;
	}
	
	public Collection<? extends EObject> allObjectsOf(EClass c) {
		LinkedList<EObject> result = new LinkedList<EObject>();
		TreeIterator<EObject> iterator = resource.getAllContents();
		while ( iterator.hasNext() ) {
			EObject obj = (EObject) iterator.next();
			if ( c.isInstance(obj) ) result.add(obj);
		}
		return result;
	}

	protected EObject addToResource(EObject obj) {
		resource.getContents().add(obj);
		return obj;
	}

	public void serialize() throws IOException {
		resource.save(null);		
	}

}
