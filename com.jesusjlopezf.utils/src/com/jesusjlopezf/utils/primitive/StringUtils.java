/*******************************************************************************
 * Copyright (c) 2015 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package com.jesusjlopezf.utils.primitive;

import java.util.ArrayList;

public class StringUtils {
	public static boolean startsUpperCase(String str){
		return str.substring(0,1).toUpperCase().equals(str.substring(0,1));
	}
	
	public static boolean startsLowerCase(String str){
		return str.substring(0,1).toLowerCase().equals(str.substring(0,1));
	}
	
	/* 
	 * this is for removing the first occurrence in a sequence of UpperCase-starting words, such as:
	 * DuckCaliforniaFatherNothing. In this case, it'd return "CaliforniaFatherNothing"
	*/
	public static String removeFirstWordToUpper(String str){		
		for(int i = 1 ; i < str.length()-1; i++)
			if(startsUpperCase(str.substring(i, str.length()-1)))
				return str.substring(i, str.length());

		return null;
	}
	
	/*
	 * this is for getting the first occurrence in a sequence of UpperCase-starting words, such as:
	 * EyesHeadPainInsane. In this case, it'd return "Eyes"
	 */
	public static String getFirstUpperCaseWord(String str){
		if(startsUpperCase(str)){
			for(int i = 1; i<str.length()-1; i++)
				if(startsUpperCase(str.substring(i, str.length()-1))) 
					return str.substring(0, i);
		}else return null;
		
		return str;
	}
	
	public static String getFirstLowerCaseWord(String str){
		if(startsLowerCase(str)){
			for(int i = 1; i<str.length()-1; i++)
				if(startsUpperCase(str.substring(i, str.length()-1))) 
					return str.substring(0, i);
		}else return null;
		
		return str;
	}
	
	/** @return an array of adjacent letter pairs contained in the input string */
	private static String[] letterPairs(String str) {
		if(str.length() <= 1) return null;				
		int numPairs = str.length()-1;
	    String[] pairs = new String[numPairs];
	    for (int i=0; i<numPairs; i++) pairs[i] = str.substring(i,i+2);
	    return pairs;
	}
	
	/** @return an ArrayList of 2-character Strings. */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static ArrayList wordLetterPairs(String str) {
		ArrayList allPairs = new ArrayList();
	    // Tokenize the string and put the tokens/words into an array
	    String[] words = str.split("\\s");
	    
	    // For each word
	    for (int w=0; w < words.length; w++) {
	    	// Find the pairs of characters
	    	String[] pairsInWord = letterPairs(words[w]);
	    	
	    	if(pairsInWord != null){	    	
		    	for (int p=0; p < pairsInWord.length; p++) {
		    		allPairs.add(pairsInWord[p]);
		        }
	    	}
	    }
	    
	    return allPairs;
	}
	
	/** @return lexical similarity value in the range [0,1] */
	@SuppressWarnings("rawtypes")
	public static double compareStrings(String str1, String str2) {
		//System.out.println("STRING1: " + str1);
		//System.out.println("STRING2: " + str2);
		ArrayList pairs1 = wordLetterPairs(str1.toUpperCase());
		ArrayList pairs2 = wordLetterPairs(str2.toUpperCase());
		int intersection = 0;
		int union = pairs1.size() + pairs2.size();
		
		for (int i=0; i<pairs1.size(); i++) {
			Object pair1=pairs1.get(i);
			
			for(int j=0; j<pairs2.size(); j++) {
				Object pair2=pairs2.get(j);
				if (pair1.equals(pair2)) {
					intersection++;
					pairs2.remove(j);
					break;
				}
			}
		}
		
		return (2.0*intersection)/union;
	}
}
